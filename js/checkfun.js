function CheckPIC(obj)
{
   tmpstr = obj.value ;
   if (tmpstr.length) {
      picname = basename(tmpstr) ;
      if (picname) {
         if (illegalName(picname)) {   // 不合法的檔名
            alert ("檔案名稱「"+picname+"」錯誤!\n檔案名稱開頭第一個字不可為句點'.'") ;
            obj.focus() ;
            obj.select() ;
            return 0 ;
         }
         tmpstr = picname.toLowerCase() ;
         tmpxxx = tmpstr.substring(tmpstr.lastIndexOf(".")+1, tmpstr.length) ;
         if (tmpxxx == "gif" ||tmpxxx == "jpg" ||tmpxxx == "GIF" ||tmpxxx == "JPG" ||tmpxxx == "doc"||tmpxxx == "swf"||tmpxxx == "pdf"||tmpxxx == "ppt"||tmpxxx == "xls"||tmpxxx == "zip"||tmpxxx == "ZIP" ||tmpxxx == "avi"|| tmpxxx=="mpg") {
            return 2 ;
         }
         else {
            alert ("檔案必須為 gif 或 jpg 或 doc 或 swf 或 pdf 或 gif \n 或 ppt 或 xls 或 zip 或 avi 或 mpg 的格式 ") ;
            obj.focus() ;
            obj.select() ;
            return 0 ;
         }
      }
      else {
         alert ("檔案格式不合!!") ;
         obj.focus() ;
         obj.select() ;
         return 0 ;
      }
   }
   else {
      return 1 ;
   }
}
//--------------------------------------------------------------------------------------
function illegalName(str) 
{
	strlen = str.length;
	var c='';

	if(strlen > 0)
	{
		c = str.charAt(0);	

		if( c == '.')
			return true;
	}
	else
	{
		return true;
	}
	return false;    
}
function basename(fPath)
{

	var fileA = fPath.split('\\');
	var cnt = fileA.length;
	if(cnt > 0)
	{

		var fName1 = fileA[cnt-1];

		fileA = fName1.split('/');
		cnt = fileA.length;
		if(cnt > 0)
		{

			return fileA[cnt-1];
		}
	}
}
function IsEmpty(str){
  var s;
  
  s = new String(str);
  s = s.replace(/ /g,"");
  if (s == "")
    return true;
  else
    return false; 
}

function getLength(str){
  var i , cnt = 0;
  for(i=0;i<str.length;i++){
    if(escape(str.charAt(i)).length >= 4) cnt+=2;
    else cnt++;
  }
  return cnt;
}

function Trim(str){
  var location,i,j,k;
  var rt;

  location=str.length;
  i=0;
  while(i<=location){
    if ((str.charAt(i)!=' ')&&(str.charAt(i)!='\n')&&(str.charAt(i)!='\t')&&(str.charAt(i)!='\r'))
      break;
    i++;
    if (i==location){  //none ......
      rt='';
      return rt;
    }
  }
  j=location-1;
  while(j>=0){
    if ((str.charAt(j)!=' ')&&(str.charAt(j)!='\n')&&(str.charAt(j)!='\t')&&(str.charAt(i)!='\r'))
      break;
    j--;
  }
  rt = '';
  for(k=0;(k<=(j-i));k++)
    rt = rt + str.charAt(i+k);
    
  return rt;
}

function add_zero(st,c){
  var i,l,s,str;
  
  s= new String(st);
  l = s.length;
  if (l >= c){
    return s;
  }else{
    str = '';
    for (i=1;i<=(c-l);i++){
      str = str + '0';
    }
    str = str + s;
    return str;
  }
}

function IsStringMode(str,mode){
  var i;
  /*
  mode :
  ac : All Chinese
  ne : No English
  nn : No Number
  ae : All English
  nc : No Chinese
  */
  if (mode=="ac"){  
    for (i=0 ; i<str.length ; i++){
      if ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".indexOf(str.substr(i,1),0)>-1){
        return false;
        break;
      }
    }
    return true;
  }else if(mode=="ne"){  
    for (i=0 ; i<str.length ; i++){
      if ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(str.substr(i,1),0)>-1){
        return false;
        break;
      }
    }
    return true;
  }else if(mode=="nn"){  
    for (i=0 ; i<str.length ; i++){
      if ("0123456789".indexOf(str.substr(i,1),0)>-1){
        return false;
        break;
      }
    }
    return true;
  }else if(mode=="ae"){  
    for (i=0 ; i<str.length ; i++){
      if (!("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(str.substr(i,1),0)>-1)){
        return false;
        break;
      }
    }
    return true;
  }else if (mode=="nc"){  
    for (i=0 ; i<str.length ; i++){
      if (!("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".indexOf(str.substr(i,1),0)>-1)){
        return false;
        break;
      }
    }
    return true;
  }
}
/*
統一證號檢核
計算規則︰
1.第一碼英文字母轉換為數字取二位，第二碼轉換為數字取個位。(請先了解身份證檢核之字母轉換方式)
2.將轉換後之數字依原來順序排列，再除去最後一碼。(最末碼為檢查碼)
3.將此數與1987654321做相對位置的乘法運算，僅取相乘後之個位數。
4.將所得之數依序相加。
5.取個位數，若為0，檢查碼為0，否則檢查碼為10減去該個位數。
Exp:
1.A C 12345660 轉換成 10 2 12345660
2.得到1021234566，檢查碼為 0
3.1021234566
 *1987654321
 -----------(由左至右為1*1,0*9,2*8,1*7,2*6,3*5,4*4,5*3,6*2,6*1)
  1067256526
4.1+0+6+7+2+5+6+5+2+6=40
5.40 取個位數 0 = 0(檢查碼)
*/
function checkForeignId(str){
  var c1, c2, n, i ,s;
  var t = "ABCDEFGHJKLMNPQRSTUVXYWZIO";

  s = new String(str);
  c1 = s.substring(0,1);
  c2 = s.substring(1,2);

  //檢查第二碼。
  if(c2.toUpperCase()!="A" && c2.toUpperCase()!="B" && c2.toUpperCase()!="C" && c2.toUpperCase()!="D" && c2.toUpperCase()!="E" && c2.toUpperCase()!="F"){
    //alert('第二碼有誤。');
    return false;
  }

  c1 = t.indexOf(c1.toUpperCase());
  c2 = t.indexOf(c2.toUpperCase());
  
  //檢核前兩個字是否為英文字母及字串長度是否為10碼。
  if((s.length != 10) || (c1<0) || (c2<0)){
    //alert('前兩碼 或 長度有誤。');
    return false;
  }
  
  n = Math.floor(c1/10) + (c1%10*9)%10 + 1;    //對第一碼作運算。
  n = n + (c2*8)%10;    //對第二碼作運算。
  
  //對流號作運算。
  for(i=2; i<9; i++){ 
    n = n + (parseInt(s.substring(i,i+1))* (9-i))%10;
  }
  
  //結果與檢查碼比對。
  n = (10- (n% 10))% 10;
  if(n != parseInt(s.substring(9,10))){
    return false;
  }

  return true;
}

function check_email(s){
  if ((s.indexOf("@")==-1)||(s.indexOf(".")==-1)||((s.charAt(0)) == "@"))
    return false;
  else
    return true;
}


