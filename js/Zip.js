function update_phone( Name, mForm, nForm )
{

	update_zip( Name, nForm);	
	mForm.value="";	
	switch( Name ){
		case "":
			mForm.value="";
			break;
		case "基隆市":
			mForm.value="02";
			break;
		case "臺北市":
			mForm.value="02";
			break;
		case "新北市":
			mForm.value="02";
			break;
		case "桃園市":
			mForm.value="03";
			break;
		case "新竹市":
			mForm.value="03";
			break;
		case "新竹縣":
			mForm.value="03";
			break;
		case "苗栗縣":
			mForm.value="037";
			break;
		case "臺中市":
			mForm.value="04";
			break;
		case "彰化縣":
			mForm.value="04";
			break;
                case "南投縣":
			mForm.value="049";
			break;
		case "雲林縣":
			mForm.value="05";
			break;
		case "嘉義市":
			mForm.value="05";
			break;
		case "嘉義縣":
			mForm.value="05";
			break;
		case "臺南市":
			mForm.value="06";
			break;
		case "高雄市":
			mForm.value="07";
			break;
		case "屏東縣":
			mForm.value="08";
			break;
		case "宜蘭縣":
			mForm.value="03";
			break;
		case "花蓮縣":
			mForm.value="03";
			break;
		case "臺東縣":
			mForm.value="089";
			break;
		case "馬祖":
			mForm.value="0836";
			break;
		case "金門":
			mForm.value="0823";
			break;
		case "澎湖縣":
			mForm.value="06";
			break;
		case "國外地區":
			mForm.value="00";
			break;
		default:
			mForm.value="";
			
	}
}

function update_zip( Name, nForm)
{
	Zip=nForm.value ;
	nForm.length = 0;
	switch( Name ){
		case "臺北市":
			nForm.options[0] = new Option("請選擇...","請選擇...");			
			nForm.options[1] = new Option("中正區,100");
			if(Zip=="中正區,100") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("大同區,103");
			if(Zip=="大同區,103") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("中山區,104");
			if(Zip=="中山區,104") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("松山區,105");
			if(Zip=="松山區,105") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("大安區,106");
			if(Zip=="大安區,106") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("萬華區,108");
			if(Zip=="萬華區,108") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("信義區,110");
			if(Zip=="信義區,110") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("士林區,111");
			if(Zip=="士林區,111") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("北投區,112");
			if(Zip=="北投區,112") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("內湖區,114");
			if(Zip=="內湖區,114") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("南港區,115");
			if(Zip=="南港區,115") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("文山區,116");
			if(Zip=="文山區,116") {nForm.options[12].selected=true;}
			break;

		case "基隆市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("仁愛區,200");
			if(Zip=="仁愛區,200") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("信義區,201");
			if(Zip=="信義區,201") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("中正區,202");
			if(Zip=="中正區,202") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("中山區,203");
			if(Zip=="中山區,203") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("安樂區,204");
			if(Zip=="安樂區,204") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("暖暖區,205");
			if(Zip=="暖暖區,205") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("七堵區,206");
			if(Zip=="七堵區,206") {nForm.options[7].selected=true;}
			break;

		case "新北市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("萬里,207");
			if(Zip=="萬里,207") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("金山,208");
			if(Zip=="金山,208") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("板橋,220");
			if(Zip=="板橋,220") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("汐止,221");
			if(Zip=="汐止,221") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("深坑,222");
			if(Zip=="深坑,222") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("石碇,223");
			if(Zip=="石碇,223") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("瑞芳,224");
			if(Zip=="瑞芳,224") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("平溪,226");
			if(Zip=="平溪,226") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("雙溪,227");
			if(Zip=="雙溪,227") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("貢寮,228");
			if(Zip=="貢寮,228") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("新店,231");
			if(Zip=="新店,231") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("坪林,232");
			if(Zip=="坪林,232") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("烏來,233");
			if(Zip=="烏來,233") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("永和,234");
			if(Zip=="永和,234") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("中和,235");
			if(Zip=="中和,235") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("土城,236");
			if(Zip=="土城,236") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("三峽,237");
			if(Zip=="三峽,237") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("樹林,238");
			if(Zip=="樹林,238") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("鶯歌,239");
			if(Zip=="鶯歌,239") {nForm.options[19].selected=true;}
			nForm.options[20] = new Option("三重,241");
			if(Zip=="三重,241") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("新莊,242");
			if(Zip=="新莊,242") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("泰山,243");
			if(Zip=="泰山,243") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("林口,244");
			if(Zip=="林口,244") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("蘆洲,247");
			if(Zip=="蘆洲,247") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("五股,248");
			if(Zip=="五股,248") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("八里,249");
			if(Zip=="八里,249") {nForm.options[26].selected=true;}
			nForm.options[27] = new Option("淡水,251");
			if(Zip=="淡水,251") {nForm.options[27].selected=true;}
			nForm.options[28] = new Option("三芝,252");
			if(Zip=="三芝,252") {nForm.options[28].selected=true;}
			nForm.options[29] = new Option("石門,253");
			if(Zip=="石門,253") {nForm.options[29].selected=true;}
			break;
		case "宜蘭縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("宜蘭,260");
			if(Zip=="宜蘭,260") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("頭城,261");
			if(Zip=="頭城,261") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("礁溪,262");
			if(Zip=="礁溪,262") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("壯圍,263");
			if(Zip=="壯圍,263") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("員山,264");
			if(Zip=="員山,264") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("羅東,265");
			if(Zip=="羅東,265") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("三星,266");
			if(Zip=="三星,266") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("大同,267");
			if(Zip=="大同,267") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("五結,268");
			if(Zip=="五結,268") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("冬山,269");
			if(Zip=="冬山,269") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("蘇澳,270");
			if(Zip=="蘇澳,270") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("南澳,272");
			if(Zip=="南澳,272") {nForm.options[12].selected=true;}
			break;
		case "新竹市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("新竹市,300");
			if(Zip=="新竹市,300") {nForm.options[1].selected=true;}
			break;
		case "新竹縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("竹北,302");
			if(Zip=="竹北,302") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("湖口,303");
			if(Zip=="湖口,303") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("新豐,304");
			if(Zip=="新豐,304") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("新埔,305");
			if(Zip=="新埔,305") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("關西,306");
			if(Zip=="關西,306") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("芎林,307");
			if(Zip=="芎林,307") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("寶山,308");
			if(Zip=="寶山,308") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("竹東,310");
			if(Zip=="竹東,310") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("五峰,311");
			if(Zip=="五峰,311") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("橫山,312");
			if(Zip=="橫山,312") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("尖石,313");
			if(Zip=="尖石,313") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("北埔,314");
			if(Zip=="北埔,314") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("峨眉,315");
			if(Zip=="峨眉,315") {nForm.options[13].selected=true;}
  			break;
		case "桃園市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("中壢,320");
			if(Zip=="中壢,320") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("平鎮,324");
			if(Zip=="平鎮,324") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("龍潭,325");
			if(Zip=="龍潭,325") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("楊梅,326");
			if(Zip=="楊梅,326") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("新屋,327");
			if(Zip=="新屋,327") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("觀音,328");
			if(Zip=="觀音,328") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("桃園,330");
			if(Zip=="桃園,330") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("龜山,333");
			if(Zip=="龜山,333") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("八德,334");
			if(Zip=="八德,334") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("大溪,335");
			if(Zip=="大溪,335") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("復興,336");
			if(Zip=="復興,336") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("大園,337");
			if(Zip=="大園,337") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("蘆竹,338");
			if(Zip=="蘆竹,338") {nForm.options[13].selected=true;}
  			break;
		case "苗栗縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("竹南,350");
			if(Zip=="竹南,350") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("頭份,351");
			if(Zip=="頭份,351") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("三灣,352");
			if(Zip=="三灣,352") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("南庄,353");
			if(Zip=="南庄,353") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("獅潭,354");
			if(Zip=="獅潭,354") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("後龍,356");
			if(Zip=="後龍,356") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("通霄,357");
			if(Zip=="通霄,357") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("苑裡,358");
			if(Zip=="苑裡,358") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("苗栗,360");
			if(Zip=="苗栗,360") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("造橋,361");
			if(Zip=="造橋,361") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("頭屋,362");
			if(Zip=="頭屋,362") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("公館,363");
			if(Zip=="公館,363") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("大湖,364");
			if(Zip=="大湖,364") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("泰安,365");
			if(Zip=="泰安,365") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("銅鑼,366");
			if(Zip=="銅鑼,366") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("三義,367");
			if(Zip=="三義,367") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("西湖,368");
			if(Zip=="西湖,368") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("卓蘭,369");
			if(Zip=="卓蘭,369") {nForm.options[18].selected=true;}
  			break;
		case "臺中市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("中區,400");
			if(Zip=="中區,400") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("東區,401");
			if(Zip=="東區,401") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("南區,402");
			if(Zip=="南區,402") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("西區,403");
			if(Zip=="西區,403") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("北區,404");
			if(Zip=="北區,404") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("北屯區,406");
			if(Zip=="北屯區,406") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("西屯區,407");
			if(Zip=="西屯區,407") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("南屯區,408");
			if(Zip=="南屯區,408") {nForm.options[8].selected=true;}
		        nForm.options[9] = new Option("太平區,411");
			if(Zip=="太平區,411") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("大里區,412");
			if(Zip=="大里區,412") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("霧峰區,413");
			if(Zip=="霧峰區,413") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("烏日區,414");
			if(Zip=="烏日區,414") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("豐原區,420");
			if(Zip=="豐原區,420") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("后里區,421");
			if(Zip=="后里區,421") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("石岡區,422");
			if(Zip=="石岡區,422") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("東勢區,423");
			if(Zip=="東勢區,423") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("和平區,424");
			if(Zip=="和平區,424") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("新社區,426");
			if(Zip=="新社區,426") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("潭子區,427");
			if(Zip=="潭子區,427") {nForm.options[19].selected=true;}
			nForm.options[20] = new Option("大雅區,428");
			if(Zip=="大雅區,428") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("神岡區,429");
			if(Zip=="神岡區,429") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("大肚區,432");
			if(Zip=="大肚區,432") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("沙鹿區,433");
			if(Zip=="沙鹿區,433") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("龍井區,434");
			if(Zip=="龍井區,434") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("梧棲區,435");
			if(Zip=="梧棲區,435") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("清水區,436");
			if(Zip=="清水區,436") {nForm.options[26].selected=true;}
			nForm.options[27] = new Option("大甲區,437");
			if(Zip=="大甲區,437") {nForm.options[27].selected=true;}
                        nForm.options[28] = new Option("外埔區,438");			
                        if(Zip=="外埔區,438") {nForm.options[28].selected=true;}	
			nForm.options[29] = new Option("大安區,439");  		
			if(Zip=="大安區,439") {nForm.options[29].selected=true;}	
                        break;
	       case "彰化縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("彰化,500");
			if(Zip=="彰化,500") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("芬園,502");
			if(Zip=="芬園,502") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("花壇,503");
			if(Zip=="花壇,503") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("秀水,504");
			if(Zip=="秀水,504") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("鹿港,505");
			if(Zip=="鹿港,505") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("福興,506");
			if(Zip=="福興,506") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("線西,507");
			if(Zip=="線西,507") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("和美,508");
			if(Zip=="和美,508") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("伸港,509");
			if(Zip=="伸港,509") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("員林,510");
			if(Zip=="員林,510") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("社頭,511");
			if(Zip=="社頭,511") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("永靖,512");
			if(Zip=="永靖,512") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("埔心,513");
			if(Zip=="埔心,513") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("溪湖,514");
			if(Zip=="溪湖,514") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("大村,515");
			if(Zip=="大村,515") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("埔鹽,516");
			if(Zip=="埔鹽,516") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("田中,520");
			if(Zip=="田中,520") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("北斗,521");
			if(Zip=="北斗,521") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("田尾,522");
			if(Zip=="田尾,522") {nForm.options[19].selected=true;}
                        nForm.options[20] = new Option("埤頭,523");
                        if(Zip=="埤頭,523") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("溪洲,524");
			if(Zip=="溪洲,524") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("竹塘,525");
			if(Zip=="竹塘,525") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("二林,526");
			if(Zip=="二林,526") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("大城,527");
			if(Zip=="大城,527") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("芳苑,528");
			if(Zip=="芳苑,528") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("二水,530");
			if(Zip=="二水,530") {nForm.options[26].selected=true;}
			break;
               case "南投縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("南投,540");
			if(Zip=="南投,540") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("中寮,541");
			if(Zip=="中寮,541") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("草屯,542");
			if(Zip=="草屯,542") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("國姓,544");
			if(Zip=="國姓,544") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("埔里,545");
			if(Zip=="埔里,545") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("仁愛,546");
			if(Zip=="仁愛,546") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("名間,551");
			if(Zip=="名間,551") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("集集,552");
			if(Zip=="集集,552") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("水里,553");
			if(Zip=="水里,553") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("魚池,555");
			if(Zip=="魚池,555") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("信義,556");
			if(Zip=="信義,556") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("竹山,557");
			if(Zip=="竹山,557") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("鹿谷,558");
			if(Zip=="鹿谷,558") {nForm.options[13].selected=true;}
			break;
		case "嘉義市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("嘉義市,600");
			if(Zip=="嘉義市,600") {nForm.options[1].selected=true;}
			break;
		case "嘉義縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("番路,602");
			if(Zip=="番路,602") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("梅山,603");
			if(Zip=="梅山,603") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("竹崎,604");
			if(Zip=="竹崎,604") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("阿里山,605");
			if(Zip=="阿里山,605") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("中埔,606");
			if(Zip=="中埔,606") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("大埔,607");
			if(Zip=="大埔,607") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("水上,608");
			if(Zip=="水上,608") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("鹿草,611");
			if(Zip=="鹿草,611") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("太保,612");
			if(Zip=="太保,612") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("朴子,613");
			if(Zip=="朴子,613") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("東石,614");
			if(Zip=="東石,614") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("六腳,615");
			if(Zip=="六腳,615") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("新港,616");
			if(Zip=="新港,616") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("民雄,621");
			if(Zip=="民雄,621") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("大林,622");
			if(Zip=="大林,622") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("溪口,623");
			if(Zip=="溪口,623") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("義竹,624");
			if(Zip=="義竹,624") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("布袋,625");
			if(Zip=="布袋,625") {nForm.options[18].selected=true;}
  			break;
		case "雲林縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("斗南,630");
			if(Zip=="斗南,630") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("大埤,631");
			if(Zip=="大埤,631") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("虎尾,632");
			if(Zip=="虎尾,632") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("土庫,633");
			if(Zip=="土庫,633") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("褒忠,634");
			if(Zip=="褒忠,634") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("東勢,635");
			if(Zip=="東勢,635") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("臺西,636");
			if(Zip=="臺西,636") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("崙背,637");
			if(Zip=="崙背,637") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("麥寮,638");
			if(Zip=="麥寮,638") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("斗六,640");
			if(Zip=="斗六,640") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("林內,643");
			if(Zip=="林內,643") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("古坑,646");
			if(Zip=="古坑,646") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("莿桐,647");
			if(Zip=="莿桐,647") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("西螺,648");
			if(Zip=="西螺,648") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("二崙,649");
			if(Zip=="二崙,649") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("北港,651");
			if(Zip=="北港,651") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("水林,652");
			if(Zip=="水林,652") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("口湖,653");
			if(Zip=="口湖,653") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("四湖,654");
			if(Zip=="四湖,654") {nForm.options[19].selected=true;}
                        nForm.options[20] = new Option("元長,655");
                        if(Zip=="元長,655") {nForm.options[20].selected=true;}
			break;
	        case "臺南市": 
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("中區,700");
			if(Zip=="中區,700") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("東區,701");
			if(Zip=="東區,701") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("南區,702");
			if(Zip=="南區,702") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("西區,703");
			if(Zip=="西區,703") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("北區,704");
			if(Zip=="北區,704") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("安平區,708");
			if(Zip=="安平區,708") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("安南區,709");
			if(Zip=="安南區,709") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("永康區,710");
			if(Zip=="永康區,710") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("歸仁區,711");
			if(Zip=="歸仁區,711") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("新化區,712");
			if(Zip=="新化區,712") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("左鎮區,713");
			if(Zip=="左鎮區,713") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("玉井區,714");
			if(Zip=="玉井區,714") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("楠西區,715");
			if(Zip=="楠西區,715") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("南化區,716");
			if(Zip=="南化區,716") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("仁德區,717");
			if(Zip=="仁德區,717") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("關廟區,718");
			if(Zip=="關廟區,718") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("龍崎區,719");
			if(Zip=="龍崎區,719") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("官田區,720");
			if(Zip=="官田區,720") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("麻豆區,721");
			if(Zip=="麻豆區,721") {nForm.options[19].selected=true;}
			nForm.options[20] = new Option("佳里區,722");
			if(Zip=="佳里區,722") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("西港區,723");
			if(Zip=="西港區,723") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("七股區,724");
			if(Zip=="七股區,724") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("將軍區,725");
			if(Zip=="將軍區,725") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("學甲區,726");
			if(Zip=="學甲區,726") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("北門區,727");
			if(Zip=="北門區,727") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("新營區,730");
			if(Zip=="新營區,730") {nForm.options[26].selected=true;}
                        nForm.options[27] = new Option("後壁區,731");			
                        if(Zip=="後壁區,731") {nForm.options[27].selected=true;}	
			nForm.options[28] = new Option("白河區,732");
			if(Zip=="白河區,732") {nForm.options[28].selected=true;}
			nForm.options[29] = new Option("東山區,733");
			if(Zip=="東山區,733") {nForm.options[29].selected=true;}
			nForm.options[30] = new Option("六甲區,734");
			if(Zip=="六甲區,734") {nForm.options[30].selected=true;}
			nForm.options[31] = new Option("下營區,735");
			if(Zip=="下營區,735") {nForm.options[31].selected=true;}
			nForm.options[32] = new Option("柳營區,736");
			if(Zip=="柳營區,736") {nForm.options[32].selected=true;}
			nForm.options[33] = new Option("鹽水區,737");
			if(Zip=="鹽水區,737") {nForm.options[33].selected=true;}
			nForm.options[34] = new Option("善化區,741");
			if(Zip=="善化區,741") {nForm.options[34].selected=true;}
			nForm.options[35] = new Option("大內區,742");
			if(Zip=="大內區,742") {nForm.options[35].selected=true;}
			nForm.options[36] = new Option("山上區,743");
			if(Zip=="山上區,743") {nForm.options[36].selected=true;}
			nForm.options[37] = new Option("新市區,744");
			if(Zip=="新市區,744") {nForm.options[37].selected=true;}
			nForm.options[38] = new Option("安定區,745");
			if(Zip=="安定區,745") {nForm.options[38].selected=true;}
			break;
	     case "高雄市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("新興區,800");
			if(Zip=="新興區,800") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("前金區,801");
			if(Zip=="前金區,801") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("苓雅區,802");
			if(Zip=="苓雅區,802") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("鹽埕區,803");
			if(Zip=="鹽埕區,803") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("鼓山區,804");
			if(Zip=="鼓山區,804") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("旗津區,805");
			if(Zip=="旗津區,805") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("前鎮區,806");
			if(Zip=="前鎮區,806") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("三民區,807");
			if(Zip=="三民區,807") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("楠梓區,811");
			if(Zip=="楠梓區,811") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("小港區,812");
			if(Zip=="小港區,812") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("左營區,813");
			if(Zip=="左營區,813") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("仁武區,814");
			if(Zip=="仁武區,814") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("大社區,815");
			if(Zip=="大社區,815") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("岡山區,820");
			if(Zip=="岡山區,820") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("路竹區,821");
			if(Zip=="路竹區,821") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("阿蓮區,822");
			if(Zip=="阿蓮區,822") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("田寮區,823");
			if(Zip=="田寮區,823") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("燕巢區,824");
			if(Zip=="燕巢區,824") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("橋頭區,825");
			if(Zip=="橋頭區,825") {nForm.options[19].selected=true;}
			nForm.options[20] = new Option("梓官區,826");
			if(Zip=="梓官區,826") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("彌陀區,827");
			if(Zip=="彌陀區,827") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("永安區,828");
			if(Zip=="永安區,828") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("湖內區,829");
			if(Zip=="湖內區,829") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("鳳山區,830");
			if(Zip=="鳳山區,830") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("大寮區,831");
			if(Zip=="大寮區,831") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("林園區,832");
			if(Zip=="林園區,832") {nForm.options[26].selected=true;}
			nForm.options[27] = new Option("鳥松區,833");
			if(Zip=="鳥松區,833") {nForm.options[27].selected=true;}
			nForm.options[28] = new Option("大樹區,840");
			if(Zip=="大樹區,840") {nForm.options[28].selected=true;}
			nForm.options[29] = new Option("旗山區,842");
			if(Zip=="旗山區,842") {nForm.options[29].selected=true;}
			nForm.options[30] = new Option("美濃區,843");
			if(Zip=="美濃區,843") {nForm.options[30].selected=true;}
                        nForm.options[31] = new Option("六龜區,844");			
                        if(Zip=="六龜區,844") {nForm.options[31].selected=true;}	
			nForm.options[32] = new Option("內門區,845");
			if(Zip=="內門區,845") {nForm.options[32].selected=true;}
			nForm.options[33] = new Option("杉林區,846");
			if(Zip=="杉林區,846") {nForm.options[33].selected=true;}
			nForm.options[34] = new Option("甲仙區,847");
			if(Zip=="甲仙區,847") {nForm.options[34].selected=true;}
			nForm.options[35] = new Option("桃源區,848");
			if(Zip=="桃源區,848") {nForm.options[35].selected=true;}
			nForm.options[36] = new Option("那瑪夏區,849");
			if(Zip=="那瑪夏區,849") {nForm.options[36].selected=true;}
			nForm.options[37] = new Option("茂林區,851");
			if(Zip=="茂林區,851") {nForm.options[37].selected=true;}
			nForm.options[38] = new Option("茄萣區,852");
			if(Zip=="茄萣區,852") {nForm.options[38].selected=true;}
			break;
                case "屏東縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("屏東,900");
			if(Zip=="屏東,900") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("三地門,901");
			if(Zip=="三地門,901") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("霧臺,902");
			if(Zip=="霧臺,902") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("瑪家,903");
			if(Zip=="瑪家,903") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("九如,904");
			if(Zip=="九如,904") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("里港,905");
			if(Zip=="里港,905") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("高樹,906");
			if(Zip=="高樹,906") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("鹽埔,907");
			if(Zip=="鹽埔,907") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("長治,908");
			if(Zip=="長治,908") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("麟洛,909");
			if(Zip=="麟洛,909") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("竹田,911");
			if(Zip=="竹田,911") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("內埔,912");
			if(Zip=="內埔,912") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("萬丹,913");
			if(Zip=="萬丹,913") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("潮州,920");
			if(Zip=="潮州,920") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("泰武,921");
			if(Zip=="泰武,921") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("來義,922");
			if(Zip=="來義,922") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("萬巒,923");
			if(Zip=="萬巒,923") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("崁頂,924");
			if(Zip=="崁頂,924") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("新埤,925");
			if(Zip=="新埤,925") {nForm.options[19].selected=true;}
                        nForm.options[20] = new Option("南州,926");
                        if(Zip=="南州,926") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("林邊,927");
			if(Zip=="林邊,927") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("東港,928");
			if(Zip=="東港,928") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("琉球,929");
			if(Zip=="琉球,929") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("佳冬,931");
			if(Zip=="佳冬,931") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("新園,932");
			if(Zip=="新園,932") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("枋寮,940");
			if(Zip=="枋寮,940") {nForm.options[26].selected=true;}
			nForm.options[27] = new Option("枋山,941");
			if(Zip=="枋山,941") {nForm.options[27].selected=true;}
			nForm.options[28] = new Option("春日,942");
			if(Zip=="春日,942") {nForm.options[28].selected=true;}
			nForm.options[29] = new Option("獅子,943");
			if(Zip=="獅子,943") {nForm.options[29].selected=true;}
			nForm.options[30] = new Option("車城,944");
			if(Zip=="車城,944") {nForm.options[30].selected=true;}
			nForm.options[31] = new Option("牡丹,945");
			if(Zip=="牡丹,945") {nForm.options[31].selected=true;}
			nForm.options[32] = new Option("恆春,946");
			if(Zip=="恆春,946") {nForm.options[32].selected=true;}
			nForm.options[33] = new Option("滿州,947");
			if(Zip=="滿州,947") {nForm.options[33].selected=true;}
			break;
		case "花蓮縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("花蓮,970");
			if(Zip=="花蓮,970") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("新城,971");
			if(Zip=="新城,971") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("吉安,973");
			if(Zip=="吉安,973") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("壽豐,974");
			if(Zip=="壽豐,974") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("鳳林,975");
			if(Zip=="鳳林,975") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("光復,976");
			if(Zip=="光復,976") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("豐濱,977");
			if(Zip=="豐濱,977") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("瑞穗,978");
			if(Zip=="瑞穗,978") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("萬榮,979");
			if(Zip=="萬榮,979") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("玉里,981");
			if(Zip=="玉里,981") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("卓溪,982");
			if(Zip=="卓溪,982") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("富里,983");
			if(Zip=="富里,983") {nForm.options[12].selected=true;}
                        nForm.options[13] = new Option("秀林,972");
                        if(Zip=="秀林,972") {nForm.options[13].selected=true;}
			break;
		case "澎湖縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("馬公,880");
			if(Zip=="馬公,880") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("西嶼,881");
			if(Zip=="西嶼,881") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("望安,882");
			if(Zip=="望安,882") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("七美,883");
			if(Zip=="七美,883") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("白沙,884");
			if(Zip=="白沙,884") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("湖西,885");
			if(Zip=="湖西,885") {nForm.options[6].selected=true;}
			break;
			
		case "金門":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("金沙,890");
			if(Zip=="金沙,890") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("金湖,891");
			if(Zip=="金湖,891") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("金寧,892");
			if(Zip=="金寧,892") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("金城,893");
			if(Zip=="金城,893") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("烈嶼,894");
			if(Zip=="烈嶼,894") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("烏坵,896");
			if(Zip=="烏坵,896") {nForm.options[6].selected=true;}
			break;	
		case "馬祖":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("南竿,209");
			if(Zip=="南竿,209") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("北竿,210");
			if(Zip=="北竿,210") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("莒光,211");
			if(Zip=="莒光,211") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("東引,212");
			if(Zip=="東引,212") {nForm.options[4].selected=true;}
			break;
		case "臺東縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("臺東,950");
			if(Zip=="臺東,950") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("綠島,951");
			if(Zip=="綠島,951") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("蘭嶼,952");
			if(Zip=="蘭嶼,952") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("延平,953");
			if(Zip=="延平,953") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("卑南,954");
			if(Zip=="卑南,954") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("鹿野,955");
			if(Zip=="鹿野,955") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("關山,956");
			if(Zip=="關山,956") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("海端,957");
			if(Zip=="海端,957") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("池上,958");
			if(Zip=="池上,958") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("東河,959");
			if(Zip=="東河,959") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("成功,961");
			if(Zip=="成功,961") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("長賓,962");
			if(Zip=="長賓,962") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("太麻里,963");
			if(Zip=="太麻里,963") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("金峰,964");
			if(Zip=="金峰,964") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("大武,965");
			if(Zip=="大武,965") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("達仁,966");
			if(Zip=="達仁,966") {nForm.options[16].selected=true;}
			break;
		case "國外地區":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("亞洲","亞洲,000");
			nForm.options[2] = new Option("歐洲","歐洲,000");
			nForm.options[3] = new Option("美洲","美洲,000");
			nForm.options[4] = new Option("非洲","非洲,000");
			nForm.options[5] = new Option("澳洲","澳洲,000");
			break;
		default:
			nForm.options[0] = new Option("請選擇...","請選擇...");
			break;
	}
}

function Buildkey(num) {
	var ctr=1;
	document.form1.h11.selectedIndex=0;
	//document.form1.h26.value="";  
	document.form1.h11.options[0]=new Option("請選擇區域...","");
	/*
	定義二階選單內容
	if(num=="第一階下拉選單的值") {	document.form1.h07.options[ctr]=new Option("第二階下拉選單的顯示名稱","第二階下拉選單的值");	ctr=ctr+1;	}
	*/	
	/*臺北市*/  
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("中正區","100");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("大同區","103");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("中山區","104");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("松山區","105");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("大安區","106");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("萬華區","108");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("信義區","110");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("士林區","111");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("北投區","112");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("內湖區","114");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("南港區","115");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("文山區","116");	ctr=ctr+1;	}
	/*基隆市*/                                                                         
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("仁愛區","200");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("信義區","201");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("中正區","202");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("中山區","203");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("安樂區","204");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("暖暖區","205");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("七堵區","206");	ctr=ctr+1;	}
	/*新北市*/                                                                         
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("萬里區","207");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("金山區","208");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("板橋區","220");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("汐止區","221");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("深坑區","222");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("石碇區","223");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("瑞芳區","224");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("平溪區","226");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("雙溪區","227");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("貢寮區","228");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("新店區","231");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("坪林區","232");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("烏來區","233");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("永和區","234");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("中和區","235");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("土城區","236");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("三峽區","237");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("樹林區","238");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("鶯歌區","239");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("三重區","241");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("新莊區","242");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("泰山區","243");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("林口區","244");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("蘆洲區","247");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("五股區","248");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("八里區","249");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("淡水區","251");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("三芝區","252");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("石門區","253");	ctr=ctr+1;	} 
	/*宜蘭縣*/                                                                         
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("宜蘭市","260");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("頭城鎮","261");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("礁溪鄉","262");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("壯圍鄉","263");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("員山鄉","264");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("羅東鎮","265");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("三星鄉","266");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("大同鄉","267");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("五結鄉","268");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("冬山鄉","269");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("蘇澳鎮","270");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("南澳鄉","272");	ctr=ctr+1;	} 
	/*新竹縣市*/                                                                       
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("新竹市","300");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("竹北市","302");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("湖口鄉","303");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("新豐鄉","304");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("新埔鎮","305");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("關西鎮","306");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("芎林鄉","307");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("寶山鄉","308");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("竹東鎮","310");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("五峰鄉","311");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("橫山鄉","312");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("尖石鄉","313");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("北埔鄉","314");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("峨眉鄉","315");	ctr=ctr+1;	} 
	/*桃園市*/                                                                         
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("中壢區","320");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("平鎮區","324");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("龍潭區","325");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("楊梅區","326");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("新屋區","327");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("觀音區","328");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("桃園區","330");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("龜山區","333");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("八德區","334");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("大溪區","335");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("復興區","336");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("大園區","337");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("蘆竹區","338");	ctr=ctr+1;	} 
	/*苗栗縣*/                                                                         
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("竹南鎮","350");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("頭份鎮","351");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("三灣鄉","352");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("南庄鄉","353");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("獅潭鄉","354");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("後龍鎮","356");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("通霄鎮","357");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("苑裡鎮","358");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("苗栗市","360");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("造橋鄉","361");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("頭屋鄉","362");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("公館鄉","363");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("大湖鄉","364");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("泰安鄉","365");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("銅鑼鄉","366");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("三義鄉","367");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("西湖鄉","368");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("卓蘭鎮","369");	ctr=ctr+1;	}
	/*臺中市*/                                                                         
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("中　區","400");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("東　區","401");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("南　區","402");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("西　區","403");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("北　區","404");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("北屯區","406");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("西屯區","407");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("南屯區","408");    ctr=ctr+1;	}                                                                          
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("太平區","411");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大里區","412");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("霧峰區","413");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("烏日區","414");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("豐原區","420");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("后里區","421");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("石岡區","422");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("東勢區","423");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("和平區","424");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("新社區","426");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("潭子區","427");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大雅區","428");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("神岡區","429");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大肚區","432");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("沙鹿區","433");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("龍井區","434");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("梧棲區","435");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("清水區","436");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大甲區","437");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("外埔區","438");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大安區","439");	ctr=ctr+1;	}
	/*彰化縣*/                                                                         
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("彰化市","500");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("芬園鄉","502");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("花壇鄉","503");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("秀水鄉","504");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("鹿港鎮","505");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("福興鄉","506");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("線西鄉","507");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("和美鎮","508");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("伸港鄉","509");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("員林鎮","510");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("社頭鄉","511");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("永靖鄉","512");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("埔心鄉","513");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("溪湖鎮","514");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("大村鄉","515");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("埔鹽鄉","516");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("田中鎮","520");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("北斗鎮","521");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("田尾鄉","522");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("埤頭鄉","523");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("溪州鄉","524");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("竹塘鄉","525");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("二林鎮","526");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("大城鄉","527");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("芳苑鄉","528");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("二水鄉","530");	ctr=ctr+1;	} 
	/*南投縣*/                                                                         
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("南投市","540");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("中寮鄉","541");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("草屯鎮","542");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("國姓鄉","544");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("埔里鎮","545");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("仁愛鄉","546");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("名間鄉","551");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("集集鎮","552");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("水里鄉","553");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("魚池鄉","555");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("信義鄉","556");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("竹山鎮","557");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("鹿谷鄉","558");	ctr=ctr+1;	} 
	/*嘉義縣市*/                                                                       
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("嘉義市","600");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("番路鄉","602");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("梅山鄉","603");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("竹崎鄉","604");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("阿里山","605");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("中埔鄉","606");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("大埔鄉","607");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("水上鄉","608");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("鹿草鄉","611");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("太保市","612");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("朴子市","613");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("東石鄉","614");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("六腳鄉","615");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("新港鄉","616");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("民雄鄉","621");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("大林鎮","622");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("溪口鄉","623");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("義竹鄉","624");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("布袋鎮","625");	ctr=ctr+1;	} 
	/*雲林縣*/                                                                         
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("斗南鎮","630");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("大埤鄉","631");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("虎尾鎮","632");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("土庫鎮","633");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("褒忠鄉","634");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("東勢鄉","635");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("臺西鄉","636");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("崙背鄉","637");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("麥寮鄉","638");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("斗六市","640");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("林內鄉","643");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("古坑鄉","646");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("莿桐鄉","647");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("西螺鎮","648");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("二崙鄉","649");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("北港鎮","651");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("水林鄉","652");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("口湖鄉","653");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("四湖鄉","654");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("元長鄉","655");	ctr=ctr+1;	} 
	/*臺南市*/                                                                         
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("中西區","700");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("東　區","701");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("南　區","702");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("北　區","704");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("安平區","708");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("安南區","709");	ctr=ctr+1;	}                                                                         
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("永康區","710");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("歸仁區","711");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("新化區","712");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("左鎮區","713");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("玉井區","714");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("楠西區","715");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("南化區","716");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("仁德區","717");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("關廟區","718");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("龍崎區","719");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("官田區","720");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("麻豆區","721");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("佳里區","722");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("西港區","723");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("七股區","724");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("將軍區","725");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("學甲區","726");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("北門區","727");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("新營區","730");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("後壁區","731");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("白河區","732");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("東山區","733");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("六甲區","734");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("下營區","735");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("柳營區","736");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("鹽水區","737");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("善化區","741");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("新市區","741");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("大內區","742");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("山上區","743");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("新市區","744");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("安定區","745");	ctr=ctr+1;	}
	/*高雄市*/                                                                         
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("新興區","800");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("前金區","801");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("苓雅區","802");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("鹽埕區","803");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("鼓山區","804");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("旗津區","805");	ctr=ctr+1;	}  
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("前鎮區","806");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("三民區","807");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("楠梓區","811");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("小港區","812");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("左營區","813");	ctr=ctr+1;	} 	                                                                         
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("仁武區","814");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("大社區","815");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("岡山區","820");	ctr=ctr+1;	}  
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("路竹區","821");	ctr=ctr+1;	}  
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("阿蓮區","822");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("田寮區","823");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("燕巢區","824");	ctr=ctr+1;	}  
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("橋頭區","825");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("梓官區","826");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("彌陀區","827");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("永安區","828");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("湖內區","829");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("鳳山區","830");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("大寮區","831");	ctr=ctr+1;	}  
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("林園區","832");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("鳥松區","833");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("大樹區","840");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("旗山區","842");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("美濃區","843");	ctr=ctr+1;	}  
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("六龜區","844");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("內門區","845");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("杉林區","846");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("甲仙區","847");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("桃源區","848");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("那瑪夏區","849");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("茂林區","851");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("茄萣區","852");	ctr=ctr+1;	} 
	/*澎湖縣*/                                                                         
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("馬公市","880");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("西嶼鄉","881");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("望安鄉","882");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("七美鄉","883");	ctr=ctr+1;	}  
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("白沙鄉","884");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("湖西鄉","885");	ctr=ctr+1;	} 
	/*屏東縣*/                                                                         
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("屏東市","900");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("三地門","901");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("霧臺鄉","902");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("瑪家鄉","903");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("九如鄉","904");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("里港鄉","905");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("高樹鄉","906");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("鹽埔鄉","907");	ctr=ctr+1;	}  
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("長治鄉","908");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("麟洛鄉","909");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("竹田鄉","911");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("內埔鄉","912");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("萬丹鄉","913");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("潮州鎮","920");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("泰武鄉","921");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("來義鄉","922");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("萬巒鄉","923");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("崁頂鄉","924");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("新埤鄉","925");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("南州鄉","926");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("林邊鄉","927");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("東港鎮","928");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("琉球鄉","929");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("佳冬鄉","931");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("新園鄉","932");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("枋寮鄉","940");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("枋山鄉","941");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("春日鄉","942");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("獅子鄉","943");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("車城鄉","944");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("牡丹鄉","945");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("恆春鎮","946");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("滿州鄉","947");	ctr=ctr+1;	} 
	/*臺東縣*/                                                                         
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("臺東市","950");	ctr=ctr+1;	}  
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("綠島鄉","951");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("蘭嶼鄉","952");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("延平鄉","953");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("卑南鄉","954");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("鹿野鄉","955");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("關山鎮","956");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("海端鄉","957");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("池上鄉","958");	ctr=ctr+1;	} 

	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("東河鄉","959");	ctr=ctr+1;	}  
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("成功鎮","961");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("長濱鄉","962");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("太麻里","963");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("金峰鄉","964");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("大武鄉","965");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("達仁鄉","966");	ctr=ctr+1;	} 
	/*花蓮縣*/                                                                         
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("花蓮市","970");	ctr=ctr+1;	}  
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("新城鄉","971");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("秀林鄉","972");	ctr=ctr+1;	}  
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("吉安鄉","973");	ctr=ctr+1;	}  
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("壽豐鄉","974");	ctr=ctr+1;	}  
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("鳳林鎮","975");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("光復鄉","976");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("豐濱鄉","977");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("瑞穗鄉","978");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("萬榮鄉","979");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("玉里鎮","981");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("卓溪鄉","982");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("富里鄉","983");	ctr=ctr+1;	} 
	/*金門縣*/                                                                         
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("金沙鎮","890");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("金湖鎮","891");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("金寧鄉","892");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("金城鎮","893");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("烈嶼鄉","894");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("烏坵鄉","896");	ctr=ctr+1;	} 
	/*連江縣*/                                                                         
	if(num=="連江縣") {	document.form1.h11.options[ctr]=new Option("南竿鄉","209");	ctr=ctr+1;	} 
	if(num=="連江縣") {	document.form1.h11.options[ctr]=new Option("北竿鄉","210");	ctr=ctr+1;	} 
	if(num=="連江縣") {	document.form1.h11.options[ctr]=new Option("莒光鄉","211");	ctr=ctr+1;	} 
	if(num=="連江縣") {	document.form1.h11.options[ctr]=new Option("東引鄉","212");	ctr=ctr+1;	} 
	/*南海諸島*/                                                               
	if(num=="南海諸島") {	document.form1.h11.options[ctr]=new Option("東　沙","817");	ctr=ctr+1;	}
	if(num=="南海諸島") {	document.form1.h11.options[ctr]=new Option("南　沙","819");	ctr=ctr+1;	}
	/*釣魚臺列嶼*/
	if(num=="釣魚臺列嶼") {	document.form1.h11.options[ctr]=new Option("釣魚臺列嶼","290");	ctr=ctr+1;	}

	document.form1.h11.length=ctr;
	document.form1.h11.options[0].selected=true;
} 

function Buildkey1(num) {
	var ctr=1;
	document.form1.h11.selectedIndex=0;
	//document.form1.h26.value="";  
	document.form1.h11.options[0]=new Option("請選擇區域...","");
	/*
	定義二階選單內容
	if(num=="第一階下拉選單的值") {	document.form1.h07.options[ctr]=new Option("第二階下拉選單的顯示名稱","第二階下拉選單的值");	ctr=ctr+1;	}
	*/	
	/*臺北市*/  
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("中正區","100 中正區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("大同區","103 大同區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("中山區","104 中山區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("松山區","105 松山區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("大安區","106 大安區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("萬華區","108 萬華區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("信義區","110 信義區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("士林區","111 士林區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("北投區","112 北投區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("內湖區","114 內湖區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("南港區","115 南港區");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h11.options[ctr]=new Option("文山區","116 文山區");	ctr=ctr+1;	}
	/*基隆市*/                                                                         
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("仁愛區","200 仁愛區");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("信義區","201 信義區");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("中正區","202 中正區");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("中山區","203 中山區");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("安樂區","204 安樂區");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("暖暖區","205 暖暖區");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h11.options[ctr]=new Option("七堵區","206 七堵區");	ctr=ctr+1;	}
	/*新北市*/                                                                         
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("萬里區","207 萬里區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("金山區","208 金山區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("板橋區","220 板橋區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("汐止區","221 汐止區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("深坑區","222 深坑區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("石碇區","223 石碇區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("瑞芳區","224 瑞芳區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("平溪區","226 平溪區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("雙溪區","227 雙溪區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("貢寮區","228 貢寮區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("新店區","231 新店區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("坪林區","232 坪林區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("烏來區","233 烏來區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("永和區","234 永和區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("中和區","235 中和區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("土城區","236 土城區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("三峽區","237 三峽區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("樹林區","238 樹林區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("鶯歌區","239 鶯歌區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("三重區","241 三重區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("新莊區","242 新莊區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("泰山區","243 泰山區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("林口區","244 林口區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("蘆洲區","247 蘆洲區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("五股區","248 五股區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("八里區","249 八里區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("淡水區","251 淡水區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("三芝區","252 三芝區");	ctr=ctr+1;	} 
	if(num=="新北市") {	document.form1.h11.options[ctr]=new Option("石門區","253 石門區");	ctr=ctr+1;	} 
	/*宜蘭縣*/                                                                         
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("宜蘭市","260 宜蘭市");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("頭城鎮","261 頭城鎮");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("礁溪鄉","262 礁溪鄉");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("壯圍鄉","263 壯圍鄉");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("員山鄉","264 員山鄉");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("羅東鎮","265 羅東鎮");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("三星鄉","266 三星鄉");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("大同鄉","267 大同鄉");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("五結鄉","268 五結鄉");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("冬山鄉","269 冬山鄉");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("蘇澳鎮","270 蘇澳鎮");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h11.options[ctr]=new Option("南澳鄉","272 南澳鄉");	ctr=ctr+1;	} 
	/*新竹縣市*/                                                                       
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("新竹市","300 新竹市");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("竹北市","302 竹北市");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("湖口鄉","303 湖口鄉");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("新豐鄉","304 新豐鄉");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("新埔鎮","305 新埔鎮");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("關西鎮","306 關西鎮");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("芎林鄉","307 芎林鄉");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("寶山鄉","308 寶山鄉");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("竹東鎮","310 竹東鎮");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("五峰鄉","311 五峰鄉");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("橫山鄉","312 橫山鄉");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("尖石鄉","313 尖石鄉");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("北埔鄉","314 北埔鄉");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h11.options[ctr]=new Option("峨眉鄉","315 峨眉鄉");	ctr=ctr+1;	} 
	/*桃園市*/                                                                         
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("中壢區","320 中壢區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("平鎮區","324 平鎮區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("龍潭區","325 龍潭區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("楊梅區","326 楊梅區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("新屋區","327 新屋區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("觀音區","328 觀音區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("桃園區","330 桃園區");	ctr=ctr+1;	} 
	
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("龜山區","333 龜山區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("八德區","334 八德區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("大溪區","335 大溪區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("復興區","336 復興區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("大園區","337 大園區");	ctr=ctr+1;	} 
	if(num=="桃園市") {	document.form1.h11.options[ctr]=new Option("蘆竹區","338 蘆竹區");	ctr=ctr+1;	} 
	/*苗栗縣*/                                                                         
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("竹南鎮","350 竹南鎮");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("頭份鎮","351 頭份鎮");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("三灣鄉","352 三灣鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("南庄鄉","353 南庄鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("獅潭鄉","354 獅潭鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("後龍鎮","356 後龍鎮");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("通霄鎮","357 通霄鎮");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("苑裡鎮","358 苑裡鎮");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("苗栗市","360 苗栗市");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("造橋鄉","361 造橋鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("頭屋鄉","362 頭屋鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("公館鄉","363 公館鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("大湖鄉","364 大湖鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("泰安鄉","365 泰安鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("銅鑼鄉","366 銅鑼鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("三義鄉","367 三義鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("西湖鄉","368 西湖鄉");	ctr=ctr+1;	} 
	if(num=="苗栗縣") {	document.form1.h11.options[ctr]=new Option("卓蘭鎮","369 卓蘭鎮");	ctr=ctr+1;	}
	/*臺中市*/                                                                         
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("中　區","400 中　區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("東　區","401 東　區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("南　區","402 南　區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("西　區","403 西　區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("北　區","404 北　區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("北屯區","406 北屯區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("西屯區","407 西屯區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("南屯區","408 南屯區");      ctr=ctr+1;	}                                                                         
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("太平區","411 太平區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大里區","412 大里區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("霧峰區","413 霧峰區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("烏日區","414 烏日區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("豐原區","420 豐原區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("后里區","421 后里區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("石岡區","422 石岡區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("東勢區","423 東勢區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("和平區","424 和平區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("新社區","426 新社區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("潭子區","427 潭子區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大雅區","428 大雅區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("神岡區","429 神岡區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大肚區","432 大肚區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("沙鹿區","433 沙鹿區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("龍井區","434 龍井區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("梧棲區","435 梧棲區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("清水區","436 清水區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大甲區","437 大甲區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("外埔區","438 外埔區");	ctr=ctr+1;	} 
	if(num=="臺中市") {	document.form1.h11.options[ctr]=new Option("大安區","439 大安區");	ctr=ctr+1;	}
	/*彰化縣*/                                                                         
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("彰化市","500 彰化市");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("芬園鄉","502 芬園鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("花壇鄉","503 花壇鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("秀水鄉","504 秀水鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("鹿港鎮","505 鹿港鎮");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("福興鄉","506 福興鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("線西鄉","507 線西鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("和美鎮","508 和美鎮");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("伸港鄉","509 伸港鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("員林鎮","510 員林鎮");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("社頭鄉","511 社頭鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("永靖鄉","512 永靖鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("埔心鄉","513 埔心鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("溪湖鎮","514 溪湖鎮");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("大村鄉","515 大村鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("埔鹽鄉","516 埔鹽鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("田中鎮","520 田中鎮");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("北斗鎮","521 北斗鎮");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("田尾鄉","522 田尾鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("埤頭鄉","523 埤頭鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("溪州鄉","524 溪州鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("竹塘鄉","525 竹塘鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("二林鎮","526 二林鎮");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("大城鄉","527 大城鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("芳苑鄉","528 芳苑鄉");	ctr=ctr+1;	} 
	if(num=="彰化縣") {	document.form1.h11.options[ctr]=new Option("二水鄉","530 二水鄉");	ctr=ctr+1;	} 
	/*南投縣*/                                                                         
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("南投市","540 南投市");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("中寮鄉","541 中寮鄉");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("草屯鎮","542 草屯鎮");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("國姓鄉","544 國姓鄉");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("埔里鎮","545 埔里鎮");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("仁愛鄉","546 仁愛鄉");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("名間鄉","551 名間鄉");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("集集鎮","552 集集鎮");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("水里鄉","553 水里鄉");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("魚池鄉","555 魚池鄉");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("信義鄉","556 信義鄉");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("竹山鎮","557 竹山鎮");	ctr=ctr+1;	} 
	if(num=="南投縣") {	document.form1.h11.options[ctr]=new Option("鹿谷鄉","558 鹿谷鄉");	ctr=ctr+1;	} 
	/*嘉義縣市*/                                                                      
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("嘉義市","600 嘉義市");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("番路鄉","602 番路鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("梅山鄉","603 梅山鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("竹崎鄉","604 竹崎鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("阿里山","605 阿里山");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("中埔鄉","606 中埔鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("大埔鄉","607 大埔鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("水上鄉","608 水上鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("鹿草鄉","611 鹿草鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("太保市","612 太保市");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("朴子市","613 朴子市");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("東石鄉","614 東石鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("六腳鄉","615 六腳鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("新港鄉","616 新港鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("民雄鄉","621 民雄鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("大林鎮","622 大林鎮");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("溪口鄉","623 溪口鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("義竹鄉","624 義竹鄉");	ctr=ctr+1;	} 
	if(num=="嘉義縣市") {	document.form1.h11.options[ctr]=new Option("布袋鎮","625 布袋鎮");	ctr=ctr+1;	} 
	/*雲林縣*/                                                                        
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("斗南鎮","630 斗南鎮");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("大埤鄉","631 大埤鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("虎尾鎮","632 虎尾鎮");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("土庫鎮","633 土庫鎮");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("褒忠鄉","634 褒忠鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("東勢鄉","635 東勢鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("臺西鄉","636 臺西鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("崙背鄉","637 崙背鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("麥寮鄉","638 麥寮鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("斗六市","640 斗六市");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("林內鄉","643 林內鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("古坑鄉","646 古坑鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("莿桐鄉","647 莿桐鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("西螺鎮","648 西螺鎮");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("二崙鄉","649 二崙鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("北港鎮","651 北港鎮");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("水林鄉","652 水林鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("口湖鄉","653 口湖鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("四湖鄉","654 四湖鄉");	ctr=ctr+1;	} 
	if(num=="雲林縣") {	document.form1.h11.options[ctr]=new Option("元長鄉","655 元長鄉");	ctr=ctr+1;	} 
	/*臺南市*/                                                                         
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("中西區","700 中西區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("東　區","701 東　區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("南　區","702 南　區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("北　區","704 北　區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("安平區","708 安平區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("安南區","709 安南區");	ctr=ctr+1;	}                                                                      
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("永康區","710 永康區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("歸仁區","711 歸仁區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("新化區","712 新化區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("左鎮區","713 左鎮區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("玉井區","714 玉井區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("楠西區","715 楠西區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("南化區","716 南化區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("仁德區","717 仁德區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("關廟區","718 關廟區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("龍崎區","719 龍崎區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("官田區","720 官田區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("麻豆區","721 麻豆區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("佳里區","722 佳里區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("西港區","723 西港區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("七股區","724 七股區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("將軍區","725 將軍區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("學甲區","726 學甲區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("北門區","727 北門區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("新營區","730 新營區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("後壁區","731 後壁區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("白河區","732 白河區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("東山區","733 東山區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("六甲區","734 六甲區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("下營區","735 下營區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("柳營區","736 柳營區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("鹽水區","737 鹽水區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("善化區","741 善化區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("新市區","741 新市區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("大內區","742 大內區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("山上區","743 山上區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("新市區","744 新市區");	ctr=ctr+1;	} 
	if(num=="臺南市") {	document.form1.h11.options[ctr]=new Option("安定區","745 安定區");	ctr=ctr+1;	}
	/*高雄市*/                                                                       
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("新興區","800 新興區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("前金區","801 前金區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("苓雅區","802 苓雅區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("鹽埕區","803 鹽埕區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("鼓山區","804 鼓山區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("旗津區","805 旗津區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("前鎮區","806 前鎮區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("三民區","807 三民區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("楠梓區","811 楠梓區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("小港區","812 小港區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("左營區","813 左營區");	ctr=ctr+1;	} 	                                                                       
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("仁武區","814 仁武區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("大社區","815 大社區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("岡山區","820 岡山區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("路竹區","821 路竹區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("阿蓮區","822 阿蓮區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("田寮區","823 田寮區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("燕巢區","824 燕巢區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("橋頭區","825 橋頭區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("梓官區","826 梓官區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("彌陀區","827 彌陀區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("永安區","828 永安區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("湖內區","829 湖內區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("鳳山區","830 鳳山區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("大寮區","831 大寮區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("林園區","832 林園區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("鳥松區","833 鳥松區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("大樹區","840 大樹區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("旗山區","842 旗山區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("美濃區","843 美濃區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("六龜區","844 六龜區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("內門區","845 內門區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("杉林區","846 杉林區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("甲仙區","847 甲仙區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("桃源區","848 桃源區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("那瑪夏區","849 那瑪夏區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("茂林區","851 茂林區");	ctr=ctr+1;	} 
	if(num=="高雄市") {	document.form1.h11.options[ctr]=new Option("茄萣區","852 茄萣區");	ctr=ctr+1;	} 
	/*澎湖縣*/                                                                         
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("馬公市","880 馬公市");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("西嶼鄉","881 西嶼鄉");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("望安鄉","882 望安鄉");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("七美鄉","883 七美鄉");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("白沙鄉","884 白沙鄉");	ctr=ctr+1;	} 
	if(num=="澎湖縣") {	document.form1.h11.options[ctr]=new Option("湖西鄉","885 湖西鄉");	ctr=ctr+1;	} 
	/*屏東縣*/                                                                        
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("屏東市","900 屏東市");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("三地門","901 三地門");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("霧臺鄉","902 霧臺鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("瑪家鄉","903 瑪家鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("九如鄉","904 九如鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("里港鄉","905 里港鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("高樹鄉","906 高樹鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("鹽埔鄉","907 鹽埔鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("長治鄉","908 長治鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("麟洛鄉","909 麟洛鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("竹田鄉","911 竹田鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("內埔鄉","912 內埔鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("萬丹鄉","913 萬丹鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("潮州鎮","920 潮州鎮");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("泰武鄉","921 泰武鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("來義鄉","922 來義鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("萬巒鄉","923 萬巒鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("崁頂鄉","924 崁頂鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("新埤鄉","925 新埤鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("南州鄉","926 南州鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("林邊鄉","927 林邊鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("東港鎮","928 東港鎮");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("琉球鄉","929 琉球鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("佳冬鄉","931 佳冬鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("新園鄉","932 新園鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("枋寮鄉","940 枋寮鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("枋山鄉","941 枋山鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("春日鄉","942 春日鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("獅子鄉","943 獅子鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("車城鄉","944 車城鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("牡丹鄉","945 牡丹鄉");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("恆春鎮","946 恆春鎮");	ctr=ctr+1;	} 
	if(num=="屏東縣") {	document.form1.h11.options[ctr]=new Option("滿州鄉","947 滿州鄉");	ctr=ctr+1;	} 
	/*臺東縣*/                                                                        
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("臺東市","950 臺東市");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("綠島鄉","951 綠島鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("蘭嶼鄉","952 蘭嶼鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("延平鄉","953 延平鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("卑南鄉","954 卑南鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("鹿野鄉","955 鹿野鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("關山鎮","956 關山鎮");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("海端鄉","957 海端鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("池上鄉","958 池上鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("東河鄉","959 東河鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("成功鎮","961 成功鎮");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("長濱鄉","962 長濱鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("太麻里","963 太麻里");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("金峰鄉","964 金峰鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("大武鄉","965 大武鄉");	ctr=ctr+1;	} 
	if(num=="臺東縣") {	document.form1.h11.options[ctr]=new Option("達仁鄉","966 達仁鄉");	ctr=ctr+1;	} 
	/*花蓮縣*/                                                                        
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("花蓮市","970 花蓮市");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("新城鄉","971 新城鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("秀林鄉","972 秀林鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("吉安鄉","973 吉安鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("壽豐鄉","974 壽豐鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("鳳林鎮","975 鳳林鎮");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("光復鄉","976 光復鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("豐濱鄉","977 豐濱鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("瑞穗鄉","978 瑞穗鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("萬榮鄉","979 萬榮鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("玉里鎮","981 玉里鎮");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("卓溪鄉","982 卓溪鄉");	ctr=ctr+1;	} 
	if(num=="花蓮縣") {	document.form1.h11.options[ctr]=new Option("富里鄉","983 富里鄉");	ctr=ctr+1;	} 
	/*金門縣*/                                                                        
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("金沙鎮","890 金沙鎮");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("金湖鎮","891 金湖鎮");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("金寧鄉","892 金寧鄉");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("金城鎮","893 金城鎮");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("烈嶼鄉","894 烈嶼鄉");	ctr=ctr+1;	} 
	if(num=="金門縣") {	document.form1.h11.options[ctr]=new Option("烏坵鄉","896 烏坵鄉");	ctr=ctr+1;	}
	/*連江縣*/                                                                        
	if(num=="連江縣") {	document.form1.h11.options[ctr]=new Option("南竿鄉","209 南竿鄉");	ctr=ctr+1;	} 
	if(num=="連江縣") {	document.form1.h11.options[ctr]=new Option("北竿鄉","210 北竿鄉");	ctr=ctr+1;	} 
	if(num=="連江縣") {	document.form1.h11.options[ctr]=new Option("莒光鄉","211 莒光鄉");	ctr=ctr+1;	} 
	if(num=="連江縣") {	document.form1.h11.options[ctr]=new Option("東引鄉","212 東引鄉");	ctr=ctr+1;	} 
	/*南海諸島*/                                                               
	if(num=="南海諸島") {	document.form1.h11.options[ctr]=new Option("東　沙","東　沙817");	ctr=ctr+1;	}
	if(num=="南海諸島") {	document.form1.h11.options[ctr]=new Option("南　沙","南　沙819");	ctr=ctr+1;	}
	/*釣魚臺列嶼*/
	if(num=="釣魚臺列嶼") {	document.form1.h11.options[ctr]=new Option("釣魚臺列嶼","290");	ctr=ctr+1;	}

	document.form1.h11.length=ctr;
	document.form1.h11.options[0].selected=true;
	
	
} 

