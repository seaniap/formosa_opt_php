<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

  if($_SESSION['UserName']<>"system"){
  $query_sitemap = "SELECT * FROM ".$Table_ID."_AdminSiteMap WHERE Display='Y' AND LEFT(Page,1)<>'Z' ".$WhereSQL." ORDER BY Page ASC , Sq ASC";}
  else {
  $query_sitemap = "SELECT * FROM ".$Table_ID."_AdminSiteMap WHERE Display='Y' ".$WhereSQL." ORDER BY Page ASC , Sq ASC";}

  //echo $query_sitemap ;
  $sitemap = mysqli_query($MySQL,$query_sitemap) or die(mysqli_error($MySQL));
  $row_sitemap = mysqli_fetch_assoc($sitemap);

  if($_GET['GoPage']=="") {
  $query_template = sprintf("SELECT * FROM %s_AdminSiteMap WHERE Page=%s  ",$Table_ID, GetSQLValueString($_GET['Page'], "text") );
  } 
  else{
  $query_template = sprintf("SELECT * FROM %s_AdminSiteMap WHERE Page=%s  ",$Table_ID, GetSQLValueString($_GET['GoPage'], "text") );
  }
  //echo $query_template;
  $template = mysqli_query($MySQL,$query_template) or die(mysqli_error($MySQL));
  $row_template = mysqli_fetch_assoc($template);
  //echo $row_template['Template'];
  $totalRows_template = mysqli_num_rows($template);
  if($row_template['Template']==""){ //如果抓不到template show news
  $row_template['Template']="News.php";
  }
?>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>網站管理系統</title>  
    <link rel="shortcut icon" type="image/x-icon" href="../images/icon/tgifridaysIco.ico" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="plugins/fontawesome5/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="menu.css" rel="stylesheet" type="text/css"/>
    <!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
    <link href="css/table.css" rel="stylesheet" type="text/css">
    <link href="css/form.css" rel="stylesheet" type="text/css" />
    <?php require_once('../lib/BBlib.php'); ?>  
    <style type="text/css">
      * { margin:0;padding:0;}
      /*body { background:rgb(74,81,85); }*/
      /* div#menu { margin:20px auto; } */
      /* div#copyright {
        font:11px 'Trebuchet MS';
        color:#222;
        text-indent:30px;
        padding:140px 0 0 0;
      }
      div#copyright a { color:#eee; }
      div#copyright a:hover { color:#222; } */
</style>   
</head>
<body>
<!-- body -->
<?php
  if(1==2){ 
  ?>
<div id="menu">
 <a href="index.php" class="logo"><img src="img/logo-w.svg" alt="Logo"></a>
 <ul class="menu">
 <?php 
  $level=strlen($row_sitemap['Page']);
  $n=1;
  while ($row_sitemap_Next = mysqli_fetch_assoc($sitemap)) {  
    if(strlen($row_sitemap['Page'])==strlen($row_sitemap_Next['Page']))
    { 
      echo "<li".$last."><a href='main.php?Page=".$row_sitemap['Page']."' ><span>".$row_sitemap['Page']." ".$row_sitemap['Topic']."</span></a></li>";
    }
    elseif(strlen($row_sitemap['Page'])<strlen($row_sitemap_Next['Page'])) //往下走
    {
      if($row_sitemap['Page']=="6"){
        $mypage="2";
      }else{
        $mypage=$row_sitemap['Page'];	
      }
      echo "<li".$last."><a href='main.php?Page=".$row_sitemap['Page']."' ><span>".$mypage." ".$row_sitemap['Topic']."</span></a>";
      echo "<div> <ul>";
      $level=strlen($row_sitemap['Page']);
    }
    elseif (strlen($row_sitemap['Page'])>strlen($row_sitemap_Next['Page'])) //往上走
    {
      echo "<li".$last."><a href='main.php?Page=".$row_sitemap['Page']."' ><span>".$row_sitemap['Page']." ".$row_sitemap['Topic']."</span></a></li>";
      for ($i=strlen($row_sitemap['Page']);$i>strlen($row_sitemap_Next['Page']);$i-=2){
        echo "</ul></div></li>";
      }
    }
    $row_sitemap=$row_sitemap_Next;
  }
  echo "<li><a href='main.php?Page=".$row_sitemap['Page']."' ><span>".$row_sitemap['Page']." ".$row_sitemap['Topic']."</span></a></li>";
  for ($i=strlen($row_sitemap['Page']);$i>1;$i-=2){
    echo "</ul></div></li>";
  }
  ?>
  </ul>
</div>
<div>
<?php } ?>
<div id="menu">
<a href="main.php?Page=2-1" class="logo"><img src="img/logo-w.svg" alt="Logo"></a>
  <ul class="menu">
<li><a href='javascript:void(0);' ><span>1 系統管理 </span></a><div> <ul>
<li><a href='main.php?Page=1-1' ><span>1-1 使用者設定</span></a></li>
<!--
<li><a href='main.php?Page=1-2' ><span>1-2 新增使用者</span></a></li>
-->
<li><a href='main.php?Page=2-1' ><span>1-2 網站地圖</span></a></li>
<li><a href='../Logout.php' ><span>1-3 登出</span></a></li></ul></div></li>
<!--
<li><a href='main.php?Page=2' ><span>2 網站管理</span></a><div> 
<ul><li><a href='main.php?Page=2-1' ><span>2-1 網站地圖 </span></a></li>
<li><a href='main.php?Page=2-2' ><span>2-2 參數設定</span></a></li>
<li><a href='main.php?Page=2-3' ><span>2-3 產生網站地圖 SEO</span></a></li>
<li><a href='main.php?Page=2-4' ><span>2-4 線上更新</span></a></li></ul></div></li>
-->
<li><a href='main.php?Page=6-1' ><span>2 歷史紀錄</span></a></li>
<!-- <li><a href='main.php?Page=8-4&Template=5754858805ab3a3d7b8231' ><span>3 聯絡我們</span></a></li> -->
<?php if($_SESSION['UserName']=="system"){ ?>
<li><a href='main.php?Page=Z' ><span>Z 系統管理者</span></a><div> 
<ul><li><a href='main.php?Page=Z-1' ><span>Z-1 網站地圖管理</span></a></li>
<li><a href='main.php?Page=Z-2' ><span>Z-2 參數設定</span></a></li>
<li><a href='main.php?Page=Z-3' ><span>Z-3 管理介面設定</span></a></li>
<li><a href='main.php?Page=Z-4' ><span>Z-4 版型管理</span></a></li></ul></div></li>
<?php } ?>
</ul></div>
<div class="container">
<?php //呼叫版型
 include($row_template['Template']);
?>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<!--end  body -->
</body>
</html>