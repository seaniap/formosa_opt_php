<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php
$colname_UserLogin = "1";
if (isset($_GET['KeyID'])) {
  $colname_UserLogin = (get_magic_quotes_gpc()) ? $_GET['KeyID'] : addslashes($_GET['KeyID']);
}

$query_UserLogin = sprintf("SELECT * FROM " . $Table_ID . "_UserLogin WHERE KeyID = '%s'", $colname_UserLogin);
$UserLogin = mysqli_query($MySQL, $query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
?>

<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <div class="jumbotron mt-5">
        <h1 class="display-4"><?php echo "確定要刪除使用者 " . $row_UserLogin['UserName'] . " !!"; ?></h1>
        <p class="lead">請注意，使用者刪除之後就無法回復使用者</p>
        <hr class="my-4">
        <form action="UserDelF.php" method="post">
        <div align="center">
          <input name="KeyID" type="hidden" value="<?php echo $row_UserLogin['KeyID']; ?>">
          <input name="Confirm" type="submit" class="btn btn-primary btn-lg" value="Yes">
          <input name="Confirm" type="submit" class="btn btn-primary btn-lg" value="No">
        </div>
      </form>
      </div>    
    </div>
  </div>
</div>
<!-- end -->