<?php require_once('../../Connections/AdminMySQL.php'); ?>
<?
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE ".$Table_ID."_Tag SET Tag=%s,Total=%s, D02=%s WHERE KeyID=%s ",
                       GetSQLValueString($_POST['Tag'], "text"),
                       GetSQLValueString($_POST['Total'], "text"),
					   GetSQLValueString(date("Y-m-d H:i:s"), "date"),
                       GetSQLValueString($_POST['KeyID'], "text")
					  );
					  
//echo $updateSQL;					   

$Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));
  $updateGoTo = "TagView.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_UserLogin = "1";
if (isset($_GET['KeyID'])) {
  $colname_UserLogin = (get_magic_quotes_gpc()) ? $_GET['KeyID'] : addslashes($_GET['KeyID']);
}

$query_UserLogin = sprintf("SELECT * FROM ".$Table_ID."_Tag WHERE KeyID = '%s'", $colname_UserLogin);
$UserLogin = mysqli_query($MySQL,$query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>無標題文件</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="MainHeader">
    <div class="PageNumberRight"><img src="../images/ICOedit.gif" width="14" height="12" align="absmiddle">修改標籤</div>
    <div class="whereYouAre">系統管理 &gt; 修改標籤</div>
</div>
<div class="clear"></div>
<!-- 開始 -->
  
  <div id="Box">
	<div class="indexhead"></div>
    <div class="indexContent">
    <form method="post" name="form1" action="<?php echo $editFormAction; ?>">
    <table width="85%" border="0" align="center" cellpadding="2" cellspacing="1" class="general">
   
	 <tr> 
      <th align="right" valign="middle" nowrap><label>標籤名稱</label></th>
      <td> <label><input name="Tag" type="text" class="textbox" value="<?php echo $row_UserLogin['Tag']; ?>" size="32"></label>
      </td>
    </tr>
    <tr> 
      <th align="right" valign="middle" nowrap><label>權重</label></th>
      <td> <label><input name="Total" type="text" class="textbox" value="<?php echo $row_UserLogin['Total']; ?>" size="32"></label>
      </td>
    </tr>
		
    <tr valign="baseline"> 
      <td align="right" nowrap>&nbsp;</td>
      <td> <input type="submit" class="button" value="更新記錄"></td>
    </tr>
  </table>
    <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="KeyID" value="<? echo $_GET[KeyID]; ?>">
</form>
</div>
    <div class="indexbottom"></div>
	</div>
<!-- 結束 -->
</body>
</html>
<?php
mysqli_free_result($UserLogin);
?>

