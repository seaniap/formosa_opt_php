<?
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO ".$Table_ID."_Tag (KeyID, Tag, Total, D01, D02) VALUES (%s, %s, %s, %s,%s)",
                       GetSQLValueString(uniqid(mt_rand()), "text"),
                       GetSQLValueString($_POST['Tag'], "text"),
                       GetSQLValueString($_POST['Total'], "text"),
                       GetSQLValueString(date("Y-m-d H:i:s"), "date"),
					   GetSQLValueString(date("Y-m-d H:i:s"), "date")
					   );
//echo $insertSQL;

  $Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
  $insertGoTo = "TagView.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
 ?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>新增標籤</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<?php //require_once('../../lib/JavaLib.php'); ?>
</head>

<body>

<!-- 開始 -->
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
<div id="MainHeader">
    <div class="PageNumberRight"><img src="../images/ICOedit.gif" width="14" height="12" align="absmiddle">新增標籤</div>
    <div class="whereYouAre">系統管理 &gt; 新增標籤</div>
</div>
<div class="clear"></div>


<div id="Box">
	<div class="indexhead"></div>
    <div class="indexContent"> 
  <table width="85%" border="0" align="center" cellpadding="2" cellspacing="1" class="general">
    <tr> 
      <th width="115" height="28" align="right" nowrap>標籤名稱</th>
      <td width="787"> 
          <input name="Tag" type="text" class="textbox" value="" size="32">
      </td>
    </tr>
    <tr> 
      <th height="28" align="right" >標籤權重</th>
      <td> 
          <input name="Total" type="text" class="textbox" value="" size="32">
       </td>
    </tr>
    	
      <td height="29" align="right" nowrap>&nbsp;</td>
      <td> <input type="button" class="button" value="新增標籤" onClick="document.form1.submit();"></td>
    </tr>
  </table></div>
    <div class="indexbottom"></div>
	</div>
  
  <input type="hidden" name="ClientID" value="<? Echo $_SESSION['ClientID'] ; ?>">
  <input type="hidden" name="MM_insert" value="form1">
</form><!-- 結束 -->
      
</body>
</html>
