<?php
$colname_UserLogin = "1";
if (isset($_GET['ClientID'])) {
  $colname_UserLogin = (get_magic_quotes_gpc()) ? $_GET['ClientID'] : addslashes($_GET['ClientID']);
}
if($_GET[Sort]<>"")
{ $Sort=" order by ".$_GET[Sort]." ";}
else 
{ $Sort=" order by Total DESC " ;}



// 檢查是否有Admin 權限
if ($_SESSION['Mode']<>"Admin")
{
echo "需有管理員權限才可管理使用者";
exit;
}


$query_UserLogin = sprintf("SELECT * FROM ".$Table_ID."_Tag  ");
$query_UserLogin =$query_UserLogin.$Sort; 
//echo $query_UserLogin;
$UserLogin = mysqli_query($MySQL,$query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);
?>
<div id="MainHeader">
    <div class="PageNumberRight"><img src="../images/ICOedit.gif" width="14" height="12" align="absmiddle">標籤設定</div>
    <div class="whereYouAre">系統管理 &gt; 標籤設定</div>
</div>
<div class="datatable">
    <div class="headtext2">
    <div class="RecordData"> &nbsp; </div>
    </div>

<table class="datatableInside" summary="標籤設定">
<caption>標籤設定</caption>
<thead>
  <tr> 
    <th>
      <span>標籤名稱</span>
	  <span><a href="<? echo "TagView.php?Sort=Tag " ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></span>
	  <span><a href="<? echo "TagView.php?Sort=Tag Desc " ;?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></span>
    </th>
    <th>
    <span>權重</span>
    <span><a href="<? echo "TagView.php?Sort=Total " ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></span>
	<span><a href="<? echo "TagView.php?Sort=Total Desc " ;?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></span>
	</th>
    <th>
    <span>首次產生時間</span>
	<span><a href="<? echo "TagView.php?Sort=D01 " ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></span>
	<span><a href="<? echo "TagView.php?Sort=D01 Desc " ;?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></span></th>
	<th><span>
	最後更新時間</span>
	<span><a href="<? echo "TagView.php?Sort=D02 " ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></span>
	<span><a href="<? echo "TagView.php?Sort=D02 Desc " ;?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></span></th>
	
	<th class="func">功能</th>
  </tr>
  </thead>
  <tbody>
  <?php do { ?>
  <tr> 
    <td><?php echo $row_UserLogin['Tag']; ?></td>
    <td><?php echo $row_UserLogin['Total']; ?></td>
	<td><?php echo substr($row_UserLogin['D01'],0,10); ?></td>
	<td><?php echo substr($row_UserLogin['D02'],0,10); ?></td>
	
    <td><a href="TagUpdate.php?<? echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."KeyID=".$row_UserLogin['KeyID'] ?>"><img src="../images/ICOedit.gif" alt="編輯" width="14" height="12" border="0"></a>　
	    <a href="TagDelWarn.php?<? echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."KeyID=".$row_UserLogin['KeyID']; ?>"><img src="../images/ICOdelete.gif" alt="刪除" width="15" height="12" border="0"></a>    </td>
  </tr>
  <?php } while ($row_UserLogin = mysqli_fetch_assoc($UserLogin)); ?>
  </tbody>
</table>
</div>
    <div class="bottomtext2"><div class="clear"></div>
   </div>
</div>

<?php
mysqli_free_result($UserLogin);
?>

