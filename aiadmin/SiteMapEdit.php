<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ($_POST['Security'][0] <> "") {
  $Security = implode("-", $_POST['Security']);
} else {
  $Security = "";
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf(
    "UPDATE " . $Table_ID . "_SiteMap SET   Topic=%s,  Title=%s, Security=%s WHERE KeyID=%s",

    GetSQLValueString($_POST['Topic'], "text"),
    GetSQLValueString($_POST['Title'], "text"),
    GetSQLValueString($Security, "text"),
    GetSQLValueString($_POST['KeyID'], "text")
  );

  //echo $updateSQL; 
  
  $Result1 = mysqli_query($MySQL, $updateSQL) or die(mysqli_error($MySQL));
  
  
   //權限編輯可以直接開通或關閉 page 0、0-1、0-2、0-3 

  
  if($_POST['Page']=="0"){
	$sql="  update GoWeb_SiteMap set Security='".$Security."' where (Page='0-1' or Page='0-2' or Page='0-3') ";
	  $Result1 = mysqli_query($MySQL, $sql) or die(mysqli_error($MySQL));  
	  }
  
  // 權限編輯可以直接開通或關閉 page 4 跟 4-1
  if($_POST['Page']=="4"){
	$sql="  update GoWeb_SiteMap set Security='".$Security."' where (Page='4-1' ) "; 
	  $Result1 = mysqli_query($MySQL, $sql) or die(mysqli_error($MySQL));   
	  }
  

  $updateGoTo = "main.php?Page=2-1";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_SiteMap = "-1";
if (isset($_GET['Page'])) {
  $colname_SiteMap = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$query_SiteMap = sprintf("SELECT * FROM " . $Table_ID . "_SiteMap WHERE Page = %s", GetSQLValueString($colname_SiteMap, "text"));
$SiteMap = mysqli_query($MySQL, $query_SiteMap) or die(mysqli_error($MySQL));
$row_SiteMap = mysqli_fetch_assoc($SiteMap);
$totalRows_SiteMap = mysqli_num_rows($SiteMap);


$query_UserLogin = "SELECT * FROM " . $Table_ID . "_UserLogin where UserName<>'system' ORDER BY UserName ASC";
$UserLogin = mysqli_query($MySQL, $query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);
?>

<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">編輯網站地圖</h3>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="main.php?Page=2">網站管理</a></li>
          <li class="breadcrumb-item"><a href="main.php?Page=2-1">網站地圖</a></li>
          <li class="breadcrumb-item active" aria-current="page">編輯網站地圖</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <!-- form start -->
      <form method="post" name="form1" action="SiteMapEdit.php">

        <div class="form-group row">
          <label for="Page" class="col-sm-2 col-form-label">Page 頁碼</label>
          <div class="col-sm-10">
            <?php if ($_GET['Action'] == "ins") { ?>
              <input type="text" name="Page" value="" class="form-control">
            <?php } else {  ?>
              <input type="text" name="Page" value="<?php echo $row_SiteMap['Page'];  ?>" class="form-control" readonly>
              <input type="hidden" name="Page" value="<?php echo $row_SiteMap['Page']; ?>">
            <?php } ?>
          </div>
        </div>

        <div class="form-group row">
          <label for="Template" class="col-sm-2 col-form-label">Template 版型</label>
          <div class="col-sm-10">
            <?php //echo $row_SiteMap['Template']; 
            ?>
            <input id="SiteMapTemplate" type="text" name="Template" value="<?php echo $row_SiteMap['Template']; ?>" class="form-control" readonly>
          </div>
        </div>

        <div class="form-group row">
          <label for="Topic" class="col-sm-2 col-form-label">Topic 內容主旨</label>
          <div class="col-sm-10">
            <!-- <input id="SiteMapTopic" type="text" name="Topic" value="<?php // echo $row_SiteMap['Topic']; ?>" class="form-control"> -->
            <textarea id="SiteMapTopic" class="form-control" name="Topic" rows="5"><?php echo $row_SiteMap['Topic']; ?></textarea>
          </div>
        </div>

        <div class="form-group row">
          <label for="Title" class="col-sm-2 col-form-label">網頁標題</label>
          <div class="col-sm-10">
            <input id="SiteMapTitle" type="text" name="Title" value="<?php echo $row_SiteMap['Title']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="SiteMapSecurity" class="col-sm-2 col-form-label">編輯權限<br>
            <small class="form-text text-muted">(不選代表不設限)</small></label>
          <div class="col-sm-10">
            <?php do { ?>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" name="Security[]" class="custom-control-input" id="ID<?php echo $row_UserLogin['UserName']; ?>" value="<?php echo $row_UserLogin['UserName']; ?>" <?php $Security = "-" . $row_SiteMap['Security'] . "-";
                                                                                                                                                                                          if ((strchr($Security, "-" . $row_UserLogin['UserName'] . "-"))) {
                                                                                                                                                                                            echo "checked";
                                                                                                                                                                                          }
                                                                                                                                                                                          ?>>
                <label class="custom-control-label" for="ID<?php echo $row_UserLogin['UserName']; ?>"><?php echo $row_UserLogin['UserName']; ?></label>
              </div>
            <?php } while ($row_UserLogin = mysqli_fetch_assoc($UserLogin)); ?>
          </div>
        </div>


        <div class="form-group d-flex justify-content-center">
          <input name="sbutton" type="submit" class="btn btn-primary" value="更新記錄">
          <input type="hidden" name="MM_update" value="form1">
          <input type="hidden" name="KeyID" value="<?php echo $row_SiteMap['KeyID']; ?>">
          <input type="hidden" name="Page" value="<?php echo $row_SiteMap['Page']; ?>">
        </div>
      </form>
      <!-- form end -->
    </div>
  </div>
</div>
<!-- end -->
<?php
mysqli_free_result($SiteMap);
mysqli_free_result($UserLogin);
?>