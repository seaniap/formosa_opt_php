<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if($_POST['Security'][0]<>"")
{$Security=implode("-",$_POST['Security']);}
else
{$Security="";}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE ".$Table_ID."_AdminSiteMap SET Page=%s, Sq=%s, Template=%s, Extend=%s, MaxRow=%s, Topic=%s, Topic_bg=%s, Topic_bgColor=%s, MainIcon=%s, Title=%s, Display=%s, Security=%s WHERE KeyID=%s",
                       GetSQLValueString($_POST['Page'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['Extend'], "text"),
					   GetSQLValueString($_POST['MaxRow'], "text"),
                 
                       GetSQLValueString($_POST['Topic'], "text"),
                       GetSQLValueString($_POST['Topic_bg'], "text"),
                       GetSQLValueString($_POST['Topic_bgColor'], "text"),
                       GetSQLValueString($_POST['MainIcon'], "text"),
                       GetSQLValueString($_POST['Title'], "text"),
	   			       GetSQLValueString($_POST['Display'], "text"),
                      
					   GetSQLValueString($Security, "text"),
                       GetSQLValueString($_POST['KeyID'], "text"));
  
  //echo $updateSQL;
  
  $Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));

  $updateGoTo = "SiteMapView.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_SiteMap = "-1";
if (isset($_GET['Page'])) {
  $colname_SiteMap = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$query_SiteMap = sprintf("SELECT * FROM ".$Table_ID."_AdminSiteMap WHERE Page = %s", GetSQLValueString($colname_SiteMap, "text"));
$SiteMap = mysqli_query($MySQL,$query_SiteMap) or die(mysqli_error($MySQL));
$row_SiteMap = mysqli_fetch_assoc($SiteMap);
$totalRows_SiteMap = mysqli_num_rows($SiteMap);


$query_UserLogin = "SELECT * FROM ".$Table_ID."_UserLogin ORDER BY UserName ASC";
$UserLogin = mysqli_query($MySQL,$query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);
?>






<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>編輯網站地圖</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/table.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="MainHeader">
    <div class="PageNumberRight"><img src="images/ICOedit.gif" width="14" height="12" align="absmiddle">編輯網站地圖</div>
    <div class="whereYouAre">網站管理 &gt; 編輯網站地圖</div>
</div>
<div class="clear"></div>
<div id="Box">
	<div class="indexhead"></div>
    <div class="indexContent">
      <table width="85%" border="0" align="center" cellpadding="2" cellspacing="1" class="general">
    <form method="post" name="form1" action="<?php echo $editFormAction; ?>">   
      <tr>
      <th nowrap align="right"><label>Page</label></th>
      <td><?php echo $row_SiteMap['Page']; ?></td>
      </tr>
      <tr>
      <th nowrap align="right"><label>順序</label></th>
      <td><input type="text" name="Sq" value="<?php echo $row_SiteMap['Sq']; ?>" size="32"></td>
      </tr>
	  <tr>
      <th nowrap align="right"><label>版型</label></th>
      <td><input type="text" name="Template" value="<?php echo $row_SiteMap['Template']; ?>" size="32"></td>
      </tr>
	
      <tr>
      <th nowrap align="right"><label>延伸版型</label></th>
      <td>
	   <input type="radio" name="Extend" value="N"  <? if($row_SiteMap['Extend']=="N"){ echo "checked";} ?>/> 無延伸 <br />
	   <input type="radio" name="Extend" value="Y"  <? if($row_SiteMap['Extend']=="Y"){ echo "checked";} ?>/> 有延伸<br />
	  
	  每頁最多顯示 <input type="text" name="MaxRow" value="<?php echo $row_SiteMap['MaxRow']; ?>" size="12">筆資料</td>
    </tr>
      <tr>
      <th nowrap align="right"><label>內容主旨</label></th>
      <td><input type="text" name="Topic" value="<?php echo $row_SiteMap['Topic']; ?>" size="32"></td>
      </tr>
      <tr>
      <th nowrap align="right"><label>網頁標題</label></th>
      <td><input type="text" name="Title" value="<?php echo $row_SiteMap['Title']; ?>" size="32"></td>
      </tr>
	  <tr>
      <th nowrap align="right"><label>顯示</label></th>
      <td>目前設定<?php echo $row_SiteMap['Display']; ?><br>
      <input type="radio" name="Display" value="Y"  <? if($row_SiteMap['Display']=="Y"){ echo "checked";} ?>/>顯示<br>
      <input type="radio" name="Display" value="N"  <? if($row_SiteMap['Display']=="N"){ echo "checked";} ?>/>不顯示
      </td>
      </tr>
	  <tr>
      <th nowrap align="right"><label>編輯權限 (不選代表不設限)</label></th>
      <td><?php do { ?>
          <input name="Security[]" type="checkbox" value="<? echo $row_UserLogin['UserName']; ?>" 
		  <? $Security= "-".$row_SiteMap['Security']."-";
		   if((strchr($Security,"-".$row_UserLogin['UserName']."-")))
             {echo "checked";}
		   ?>
		   /> 
          <? echo $row_UserLogin['UserName']."<br>"; ?>
          <?php } while ($row_UserLogin = mysqli_fetch_assoc($UserLogin)); ?>	  </td>
    </tr>
	
    <tr valign="baseline">
      <th nowrap align="right">&nbsp;</th>
      <td><input type="submit" class="button b-btn" value="更新記錄"></td>
    </tr>
  
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="KeyID" value="<?php echo $row_SiteMap['KeyID']; ?>"> 
  <input type="hidden" name="Page" value="<?php echo $row_SiteMap['Page']; ?>">
 <!-- <input type="hidden" name="Template" value="<?php //echo $row_SiteMap['Template']; ?>"> -->
</form></table>
    </div>
    <div class="indexbottom"></div>
	</div>


</body>
</html>
<?php
mysqli_free_result($SiteMap);

mysqli_free_result($UserLogin);
?>