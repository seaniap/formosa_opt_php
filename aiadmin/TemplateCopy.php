<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php
echo 'TemplateCopy';
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

  $insertSQL = sprintf("INSERT INTO ".$Table_ID."_Template (T_ID, T_Name, T_FileName, T_Category, T_Type, T_Page, T_Icon, T_Remark) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['T_ID'], "text"),
                       GetSQLValueString($_POST['T_Name'], "text"),
                       GetSQLValueString($_POST['T_FileName'], "text"),
                       GetSQLValueString($_POST['T_Category'], "text"),
                       GetSQLValueString($_POST['T_Type'], "text"),
                       GetSQLValueString($_POST['T_Page'], "text"),
                       GetSQLValueString($_POST['T_Icon'], "text"),
                       GetSQLValueString($_POST['T_Remark'], "text"));

  $updateSQL = sprintf("UPDATE ".$Table_ID."_Template SET T_Name=%s,T_FileName=%s, T_Category=%s, T_Type=%s, T_Page=%s, T_Icon=%s, T_Remark=%s  WHERE T_ID=%s",
                       GetSQLValueString($_POST['T_Name'], "text"),
					   GetSQLValueString($_POST['T_FileName'], "text"),
                       GetSQLValueString($_POST['T_Category'], "text"),
                       GetSQLValueString($_POST['T_Type'], "text"),
                       GetSQLValueString($_POST['T_Page'], "text"),
                       GetSQLValueString($_POST['T_Icon'], "text"),
                       GetSQLValueString($_POST['T_Remark'], "text"),
                       GetSQLValueString($_POST['T_ID'], "text"));

  
  if ($_POST['Action']=="ins"){

   
   //$Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL)); 
  }
  else if($_POST['Action']=="del") {
  
  $delSQL="DELETE FROM ".$Table_ID."_Template WHERE T_ID='".$_POST['T_ID']."'";
   $Result1 = mysqli_query($MySQL,$delSQL) or die(mysqli_error($MySQL)); 
  }
  else {
  
   //echo $insertSQL;
   echo "多重新增";
   foreach ($_POST['ClientID'] as $value)
   { echo $value;}
  $Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));
}
  $updateGoTo = "TemplateViewA.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Tempalte = "-1";
if (isset($_GET['T_ID'])) {
  $colname_Tempalte = (get_magic_quotes_gpc()) ? $_GET['T_ID'] : addslashes($_GET['T_ID']);
}

$query_Tempalte = sprintf("SELECT * FROM ".$Table_ID."_Template WHERE T_ID = '%s'", $colname_Tempalte);
$Tempalte = mysqli_query($MySQL,$query_Tempalte) or die(mysqli_error($MySQL));
$row_Tempalte = mysqli_fetch_assoc($Tempalte);
$totalRows_Tempalte = mysqli_num_rows($Tempalte);


if(  $_GET['Action']=="ins") //新增資料
{
 $row_Tempalte['T_ID']=uniqid(mt_rand());
 
}

?>
<!-- 暫時放置 -->
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>網站管理系統</title>  
<link rel="shortcut icon" type="image/x-icon" href="../images/icon/tgifridaysIco.ico" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<link rel="stylesheet" href="plugins/fontawesome5/css/fontawesome-all.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>  
<!-- 暫時放置 -->
<body>
<!-- start 複製版型到其他帳號-->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">複製版型到其他帳號</h3>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">系統管理</a></li>
        <li class="breadcrumb-item"><a href="main.php?Page=z-4">版型管理</a></li>
        <li class="breadcrumb-item active" aria-current="page">複製版型到其他帳號</li>
        </ol>
      </nav>
    </div>
  </div>


  <div class="row">
    <div class="col">
    <!-- form start-->
    <form method="post" name="form1" action="<?php echo $editFormAction; ?>">

    <div class="form-group d-flex justify-content-center">  
      <?php if($_GET['Action']=='ins'){ ?>
      <input class="btn btn-primary" type="submit" value="新增">
      <?php  } else if ($_GET['Action']=='del') {  ?> 
      <input class="btn btn-danger" type="submit" value="刪除">
      <input class="btn btn-primary" type="button" value="取消" onclick="history.go(-1);"> 
      <?php  } else {  ?> 
      <input class="btn btn-primary" type="submit" value="更新">
      <?php } ?>
    </div>

      <div class="form-group row">
            <label for="Sq" class="col-sm-2 col-form-label">T_Name</label>
            <div class="col-sm-10">
              <input type="text" name="T_Name" value="<?php echo $row_Tempalte['T_Name']; ?>" class="form-control">
            </div>
      </div>

      <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_FileName</label>
          <div class="col-sm-10">
            <input type="text" name="T_FileName" value="<?php echo $row_Tempalte['T_FileName']; ?>" class="form-control">
          </div>
      </div>

      <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Category</label>
          <div class="col-sm-10">
            <input type="text" name="T_Category" value="<?php echo $row_Tempalte['T_Category']; ?>" class="form-control">
          </div>
      </div>

      <div class="form-group row">
          <label for="Display" class="col-sm-2 col-form-label">T_Type</label>
          <div class="col-sm-5">目前設定: <?php echo $row_Tempalte['T_Type']; ?></div>

          <div class="col-sm-5">
            <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="T_Type" id="T_Type1" class="custom-control-input" <?php if($row_Tempalte['T_Type']=="電子報") {echo "checked";} ?> value="電子報">
              <label class="custom-control-label" for="T_Type1">電子報</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="T_Type" id="T_Type2" class="custom-control-input" <?php if($row_Tempalte['T_Type']=="內頁") {echo "checked";} ?> value="內頁">
              <label class="custom-control-label" for="T_Type2">內頁</label>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Page</label>
          <div class="col-sm-10">
            <input type="text" name="T_Page" value="<?php echo $row_Tempalte['T_Page']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Icon</label>
          <div class="col-sm-10">
            <input type="text" name="T_Icon" value="<?php echo $row_Tempalte['T_Icon']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Remark</label>
          <div class="col-sm-10">
            <input type="text" name="T_Remark" value="<?php echo $row_Tempalte['T_Remark']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
        <label for="Sq" class="col-sm-2 col-form-label">複製到</label>
        <div class="col-sm-10">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID01" value="HQ" checked>
              <label class="form-check-label" for="ClientID01">總公司</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID02" value="FA" checked>
              <label class="form-check-label" for="ClientID02">敦化店</label>
            </div>


            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID03" value="FB" checked>
              <label class="form-check-label" for="ClientID03">忠孝店</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID04" value="FC" checked>
              <label class="form-check-label" for="ClientID04">世貿店</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID05" value="FD" checked>
              <label class="form-check-label" for="ClientID05">重慶店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID06" value="FE" checked>
              <label class="form-check-label" for="ClientID06">西門店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID07" value="FF" checked>
              <label class="form-check-label" for="ClientID07">美麗華店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID08" value="FG" checked>
              <label class="form-check-label" for="ClientID08">永春店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID09" value="FH" checked>
              <label class="form-check-label" for="ClientID09">環球店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID10" value="FI" checked>
              <label class="form-check-label" for="ClientID10">中壢店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID11" value="FO" checked>
              <label class="form-check-label" for="ClientID11">桃園店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID12" value="FJ" checked>
              <label class="form-check-label" for="ClientID12">新竹店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID13" value="FK" checked>
              <label class="form-check-label" for="ClientID13">台中英才店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID14" value="FL" checked>
              <label class="form-check-label" for="ClientID14">台中三民店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID15" value="FM" checked>
              <label class="form-check-label" for="ClientID15">台中市政店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID16" value="FQ" checked>
              <label class="form-check-label" for="ClientID16">台南安平店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID17" value="FN" checked>
              <label class="form-check-label" for="ClientID17">高雄五福店</label>
            </div>

            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" name="ClientID[]" id="ClientID18" value="FP" checked>
              <label class="form-check-label" for="ClientID18">高雄夢時代</label>
            </div>
          </div>
      </div>
    <input type="hidden" name="MM_update" value="form1">
    <input type="hidden" name="T_ID" value="<?php echo $row_Tempalte['T_ID']; ?>">
    <input type="hidden" name="Action" value="<?php echo $_GET['Action']; ?>">
  </form>
  <!-- form end -->
      
    </div>
  </div>

  
</div>
<!-- end -->
</body>
</html>
<?php
mysqli_free_result($Tempalte);
?>
