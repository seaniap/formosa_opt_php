<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php
$colname_UserLogin = "1";
if (isset($_GET['ClientID'])) {
    $colname_UserLogin = (get_magic_quotes_gpc()) ? $_GET['ClientID'] : addslashes($_GET['ClientID']);
}
if ($_GET[Sort] <> "") {
    $Sort = " order by " . $_GET[Sort] . " ";
}


// 檢查是否有Admin 權限
if ($_SESSION['Mode'] <> "Admin") {
    echo "需有管理員權限才可管理使用者";
    exit;
}


$query_UserLogin = sprintf("SELECT * FROM " . $Table_ID . "_UserLogin where KeyID<>'system' ");
$query_UserLogin = $query_UserLogin . $Sort;
//echo $query_UserLogin;
$UserLogin = mysqli_query($MySQL, $query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);

$MM_paramName = "";

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&" . strtolower($MM_paramName) . "=";
$MM_keepURL = "";
$MM_keepForm = "";
$MM_keepBoth = "";
$MM_keepNone = "";
// add the URL parameters to the MM_keepURL string
reset($_GET);
while (list($key, $val) = each($_GET)) {
    $nextItem = "&" . strtolower($key) . "=";
    if (!stristr($MM_removeList, $nextItem)) {
        $MM_keepURL .= "&" . $key . "=" . urlencode($val);
    }
}
// add the URL parameters to the MM_keepURL string
if (isset($_POST)) {
    reset($_POST);
    while (list($key, $val) = each($_POST)) {
        $nextItem = "&" . strtolower($key) . "=";
        if (!stristr($MM_removeList, $nextItem)) {
            $MM_keepForm .= "&" . $key . "=" . urlencode($val);
        }
    }
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL . "&" . $MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);
?>

<!-- start -->
<style>
    i.bi {
        /*set icon color*/
        color: #fff;
        font-size: 18px;
        letter-spacing: 2px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col">
            <h3 class="mt-3 caption">使用者設定</h3>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="main.php?Page=1">系統管理</a></li>
                    <li class="breadcrumb-item active" aria-current="page">使用者設定</li>
                </ol>
            </nav>
        </div>
    </div>
    <!-- form start -->
    <div class="row">
        <div class="col my-3">
            <h5>新增使用者</h5>
            <form method="post" name="form1" action="UserAdd.php">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="AdminUserName">使用者名稱</label>
                        <input type="text" id="AdminUserName" class="form-control" name="UserName" value="" tabindex="1">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="AdminPassword">使用者密碼</label>
                        <input type="text" id="AdminPassword" class="form-control" name="Password" value="" tabindex="2">
                    </div>
                </div>

                <div class="form-group row col-12">
                    <label for="AdminLevel" class="col-sm-3 col-form-label">使用者類型</label>
                    <div class="col-sm-9">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" name="Level" id="AdminLevel" value="Admin" tabindex="3">
                            <label class="custom-control-label" for="AdminLevel">Admin系統管理者</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="Level" id="AdminLevel2" class="custom-control-input" value="User" tabindex="4" checked="checked">
                            <label class="custom-control-label" for="AdminLevel2">Userㄧ般使用者</label>
                        </div>
                    </div>
                </div>

                <div class="form-group d-flex justify-content-center">
                    <input name="Remark" type="hidden" class="textbox" id="AdminRemark" value="Created by <?php echo $_SESSION['UserName']; ?>" size="32" tabindex="5">
                    <input type="button" class="btn btn-primary" value="新增使用者" tabindex="6" onClick="check1();">
                    <input type="hidden" name="ClientID" value="<?php echo $_SESSION['ClientID']; ?>">
                    <input type="hidden" name="MM_insert" value="form1">
                </div>

            </form>
        </div>
    </div>
    <!-- end form -->

    <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table table-striped" summary="使用者設定">
                    <thead>
                        <tr>
                            <th nowrap>
                                <div class="d-flex justify-content-around">
                                    <span>使用者名稱</span>
                                    <span>
                                        <a href="<?php echo "main.php?Page=1-1&Sort=UserName ASC"; ?>%20">
                                            <i class="bi bi-arrow-up-circle"></i>
                                        </a>
                                        <a href="<?php echo "main.php?Page=1-1&Sort=UserName Desc "; ?>%20">
                                            <i class="bi bi-arrow-down-circle"></i>
                                        </a>
                                    </span>
                                </div>
                            </th>
                            <th nowrap>
                                <span>使用者密碼</span>
                            </th>
                            <th nowrap>
                                <div class="d-flex justify-content-around">
                                    <span>使用者層級</span>
                                    <span>
                                        <a href="<?php echo "main.php?Page=1-1&Sort=Level ASC"; ?>%20">
                                            <i class="bi bi-arrow-up-circle"></i>
                                        </a>
                                        <a href="<?php echo "main.php?Page=1-1&Sort=Level Desc "; ?>%20">
                                            <i class="bi bi-arrow-down-circle"></i>
                                        </a>
                                    </span>
                                </div>
                            </th>
                            <th nowrap>
                                <div class="d-flex justify-content-around">
                                    <span>上次更新時間</span>
                                    <span>
                                        <a href="<?php echo "main.php?Page=1-1&Sort=CreatedTime ASC"; ?>%20">
                                            <i class="bi bi-arrow-up-circle"></i>
                                        </a>
                                        <a href="<?php echo "main.php?Page=1-1&Sort=CreatedTime Desc "; ?>%20">
                                            <i class="bi bi-arrow-down-circle"></i>
                                        </a>
                                    </span>
                                </div>
                            </th>
                            <th nowrap>
                                <div class="d-flex justify-content-around">
                                    <span>說明</span>
                                    <span>
                                        <a href="<?php echo "main.php?Page=1-1&Sort=Remark ASC"; ?>%20">
                                            <i class="bi bi-arrow-up-circle"></i>
                                        </a>
                                        <a href="<?php echo "main.php?Page=1-1&Sort=Remark Desc "; ?>%20">
                                            <i class="bi bi-arrow-down-circle"></i>
                                        </a>
                                    </span>
                                </div>
                            </th>
                            <th nowrap class="func">
                                <span>功能</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php do { ?>
                            <tr>
                                <td><?php echo $row_UserLogin['UserName']; ?></td>
                                <td><?php echo $row_UserLogin['Password']; ?></td>
                                <td><?php echo $row_UserLogin['Level']; ?></td>
                                <td><?php echo substr($row_UserLogin['CreatedTime'], 0, 10); ?></td>
                                <td><?php echo $row_UserLogin['Remark']; ?></td>
                                <td>
                                    <a class="fun-edit" href="main.php?Page=1-1-1&<?php echo "KeyID=" . $row_UserLogin['KeyID'] ?>" title="編輯">
                                        <i class="far fa-edit"></i>
                                    </a>　
                                    <!-- <a class="fun-delete" href="main.php?Page=1-1-2&<?php echo "KeyID=" . $row_UserLogin['KeyID']; ?>" title="刪除">
                                        <i class="far fa-trash-alt"></i>
                                    </a> -->
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#Modal<?php echo $row_UserLogin['KeyID']; ?>"><i class="far fa-trash-alt"></i> 刪除</button>
                                </td>
                            </tr>
                            <!--Start Modal -->
                            <div class="modal fade" id="Modal<?php echo $row_UserLogin['KeyID']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="UserDelF.php" method="post">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">刪除使用者<?php echo $row_UserLogin['UserName']; ?></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>您是否確定要刪除使用者 <?php echo $row_UserLogin['UserName']; ?>？</p>
                                                <p><?php echo $row_UserLogin['UserName']; ?>被刪除後即不可以回復。</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" name="Confirm" class="btn btn-secondary" data-dismiss="modal" value="No">取消</button>
                                                <button type="submit" name="Confirm" class="btn btn-danger" value="Yes">確定刪除</button>
                                                <input name="KeyID" type="hidden" value="<?php echo $row_UserLogin['KeyID']; ?>">
                                                <!-- <input name="Confirm" type="submit" class="btn btn-primary btn-lg" value="Yes">
                                    <input name="Confirm" type="submit" class="btn btn-primary btn-lg" value="No"> -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End Modal -->
                        <?php } while ($row_UserLogin = mysqli_fetch_assoc($UserLogin)); ?>
                    </tbody>
                </table>


            </div>
        </div>
    </div>

</div>




<script>
    function check1() {
        if (document.form1.UserName.value == "") {
            alert('請填寫使用者名稱');
            return false;
        }

        if (document.form1.Password.value == "") {
            alert('請填寫使用者密碼');
            return false;
        }

        document.form1.submit();
    }
</script>
<!-- end -->
<?php
mysqli_free_result($UserLogin);
?>