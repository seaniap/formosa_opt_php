<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php
//抓網址
$path = substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], '/', 1) + 1);
$path = "http://" . $_SERVER['HTTP_HOST'] . $path;
//echo $path;


$query_template = sprintf("SELECT * FROM %s_SiteMap WHERE Display='Y' and left(Page,1)<>'B'  and left(Page,1)<>'P'  and left(Page,1)<>'S' and left(Page,1)<>'T'  order by Sq  ", $Table_ID);
$template = mysqli_query($MySQL, $query_template) or die(mysqli_error($MySQL));
$row_template = mysqli_fetch_assoc($template);
$xml_header = "<" . "?xml version=\"1.0\" encoding=\"UTF-8\"?" . "> \n <urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"> \n";
$xml_footer = "</urlset>";
$result = "";
$last_modify = " <lastmod>" . date("Y-m-d") . "</lastmod> ";
$changefreq = " <changefreq>monthly</changefreq> ";
//echo $last_modify;
do {

    if ($row_template['Extend'] == "Y") {

        $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = '%s' ", $Table_ID, $row_template['Page']);
        //echo $query_content01;
        $content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);

        do {

            $len = strlen($row_content01['Template']) - 4;
            $Template = substr($row_content01['Template'], 0, $len);
            $result .= "<url> <loc>" . $path . "include/index.php?Page=" . $row_template['Page'] . "&amp;" . $Template . "=" . $row_content01['KeyID'] . "</loc>" . $last_modify . $changefreq . " </url> \n";
        } while ($row_content01 = mysqli_fetch_assoc($content01));
    }



    $result .= "<url> <loc>" . $path . "include/index.php?Page=" . $row_template['Page'] . "</loc> " . $last_modify . $changefreq . "</url> \n";
} while ($row_template = mysqli_fetch_assoc($template));

$ss = $xml_header . $result . $xml_footer;
//echo $ss;
//$fp = fopen("../../sitemap.xml", "w");
$fp = fopen("../sitemap.xml", "w");
$rr = fputs($fp, $ss);

// if ($rr == true) {
//     echo "Siteamp 已成功建立";

// } else {
//     echo "Siteamp建立失敗";
// }

?>
<!-- start -->
<div class="container">
    <div class="row">
        <div class="col mt-2">
            <?php if ($rr == true) { ?>
                <div class="alert alert-primary" role="alert">
                    <a href="../../sitemap.xml"><?php echo "Siteamp 已成功建立"; ?></a>
                </div>
            <?php } else { ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "Siteamp建立失敗"; ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- detail -->
    <div class="row">
        <div class="col mt-3">
            <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">顯示細節</a>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <div class="card card-body" style="height: 200px; overflow:scroll">
                    <?php echo $ss; ?>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- end -->