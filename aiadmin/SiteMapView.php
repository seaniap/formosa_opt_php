<?php

$currentPage = $_SERVER["PHP_SELF"];
//控制一頁顯示筆數
$maxRows_sitemap = 10;
$pageNum_sitemap = 0;
if (isset($_GET['pageNum_sitemap'])) {
  $pageNum_sitemap = $_GET['pageNum_sitemap'];
}
$startRow_sitemap = $pageNum_sitemap * $maxRows_sitemap;

// Sort排序
if ($_GET['Sort'] <> "") {
  $Sort = " ORDER BY " . $_GET['Sort'] . " ";
  $sort = $_GET['Sort'];
} else {
  $Sort = " ORDER BY Page ASC ";
}

// $query_sitemap = "SELECT * FROM ".$Table_ID."_SiteMap WHERE `Display`='Y' and Page<>'5-1' and Page<>'5-2' and Page<>'5-3'  and Page<>'1' and Page<>'3' and Page<>'9' and Page<>'6' and Page<>'6-1' ORDER BY Page ASC , Sq ASC";
//$query_sitemap = "SELECT * FROM ".$Table_ID."_SiteMap WHERE Display='Y' ORDER BY Page ASC , Sq ASC";
$query_sitemap = "SELECT * FROM ".$Table_ID."_SiteMap WHERE Page='0' or Page='2' or Page='4' or Page='B-8'  ";
$query_sitemap = $query_sitemap . $Sort;

//echo 'Debug--------';
//echo "Sort: ".$_GET[Sort]."<br>";
//echo $query_sitemap."<br>";
//echo $sort."<br>";

$query_limit_sitemap = sprintf("%s LIMIT %d, %d", $query_sitemap, $startRow_sitemap, $maxRows_sitemap);


$sitemap = mysqli_query($MySQL,$query_limit_sitemap) or die(mysqli_error($MySQL));
$row_sitemap = mysqli_fetch_assoc($sitemap);

if (isset($_GET['totalRows_sitemap'])) {
  $totalRows_sitemap = $_GET['totalRows_sitemap'];
} else {
  $all_sitemap = mysqli_query($MySQL,$query_sitemap);
  $totalRows_sitemap = mysqli_num_rows($all_sitemap);
}
$totalPages_sitemap = ceil($totalRows_sitemap/$maxRows_sitemap)-1;

$queryString_sitemap = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_sitemap") == false && 
        stristr($param, "totalRows_sitemap") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_sitemap = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_sitemap = sprintf("&totalRows_sitemap=%d%s", $totalRows_sitemap, $queryString_sitemap);
?>

<!-- start content -->
<div class="container">
  <div class="row">
      <div class="col">
        <h3 class="mt-3 caption">網站地圖</h3>
      </div>
    </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="main.php?Page=2">系統管理</a></li>
          <!-- <li class="breadcrumb-item active" aria-current="page">網站地圖(依照：<?php //echo $_GET[Sort];?>排序)</li> -->
          <li class="breadcrumb-item active" aria-current="page">網站地圖</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col record-data">  
        記錄 <?php echo ($startRow_sitemap + 1) ?> 到 <?php echo min($startRow_sitemap + $maxRows_sitemap, $totalRows_sitemap) ?> 共 <?php echo $totalRows_sitemap ?>
    </div>
  </div>

  <div class="row">
    <div class="col">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
          <th nowrap scope="col">
              <div class="d-flex justify-content-around">
                <span>頁碼</span>
                <!-- <span>
                  <a href="<?php echo "main.php?Page=2-1&Sort=Page ASC"; ?>">
                    <i class="bi bi-arrow-up-circle"></i>
                  </a>
                  <a href="<?php echo "main.php?Page=2-1&Sort=Page Desc "; ?>">
                    <i class="bi bi-arrow-down-circle"></i>
                  </a>
                </span> -->
              </div>
            </th>  
          <th scope="col">
            <div class="d-flex justify-content-around">
                  <span>版型</span>
                  <span>
                    <a href="<?php echo "main.php?Page=2-1&Sort=Template ASC"; ?>">
                      <i class="bi bi-arrow-up-circle"></i>
                    </a>
                    <a href="<?php echo "main.php?Page=2-1&Sort=Template Desc "; ?>">
                      <i class="bi bi-arrow-down-circle"></i>
                    </a>
                  </span>
                </div>
          </th>
          <th scope="col">
            <div class="d-flex justify-content-around">
                    <span>內容筆數</span>
                    <span>
                      <a href="<?php echo "main.php?Page=2-1&Sort=Template ASC"; ?>">
                        <i class="bi bi-arrow-up-circle"></i>
                      </a>
                      <a href="<?php echo "main.php?Page=2-1&Sort=Template Desc "; ?>">
                        <i class="bi bi-arrow-down-circle"></i>
                      </a>
                    </span>
                  </div>
          </th>
          <!-- <th scope="col">
            <div class="d-flex justify-content-around">
                    <span>內容主旨</span>
                    <span>
                      <a href="<?php // echo "main.php?Page=2-1&Sort=Topic ASC"; ?>">
                        <i class="bi bi-arrow-up-circle"></i>
                      </a>
                      <a href="<?php // echo "main.php?Page=2-1&Sort=Topic Desc "; ?>">
                        <i class="bi bi-arrow-down-circle"></i>
                      </a>
                    </span>
                  </div>
          </th> -->
          <th scope="col">
            <div class="d-flex justify-content-around">
                      <span>網頁標題</span>
                      <span>
                        <a href="<?php echo "main.php?Page=2-1&Sort=Title ASC"; ?>">
                          <i class="bi bi-arrow-up-circle"></i>
                        </a>
                        <a href="<?php echo "main.php?Page=2-1&Sort=Title Desc"; ?>">
                          <i class="bi bi-arrow-down-circle"></i>
                        </a>
                      </span>
                    </div>
          </th>
          <th scope="col">
            <div class="d-flex justify-content-around">
                      <span>編輯權限</span>
                      <span>
                        <a href="<?php echo "main.php?Page=2-1&Sort=Security ASC"; ?>">
                          <i class="bi bi-arrow-up-circle"></i>
                        </a>
                        <a href="<?php echo "main.php?Page=2-1&Sort=Security Desc"; ?>">
                          <i class="bi bi-arrow-down-circle"></i>
                        </a>
                      </span>
                    </div>
          </th>
          <th class="func">功能</th>
          </tr>
        </thead>
        <tbody>
          <?php do { ?> 
          <tr> 
            <th scope="row">&nbsp;<?php echo $row_sitemap['Page']; ?></th>
            <td><?php echo $row_sitemap['Template']; ?></td>
            <td>
            <?php 
            if($row_sitemap['Page']=="5-1"){
              $Page='5';
            }else{$Page=$row_sitemap['Page'];}
              $query_contentT01 = sprintf("SELECT count(*) as cc FROM %s_BigTable WHERE Page = %s  ",$Table_ID, GetSQLValueString($Page, "text"));
              $contentT01 = mysqli_query($MySQL,$query_contentT01) or die(mysqli_error($MySQL));
              $row_contentT01 = mysqli_fetch_assoc($contentT01);
              echo $row_contentT01['cc']; ?></td>
              <!-- <td><?php // echo $row_sitemap['Topic']; ?></td> -->
              <td><?php echo $row_sitemap['Title']; ?></td>
              <td><?php echo $row_sitemap['Security']; ?></td>
              <td> 
              <?php if ($_SESSION['Mode']=="Admin" ){ ?>
              <span><a class="fun-edit" href="main.php?GoPage=2-1-1&Page=<?php echo $row_sitemap['Page']; ?>" title="編輯">
                    <i class="far fa-edit"></i></a></span>
              <?php } ?>
              <?php if($row_sitemap['Page']<>'1' and $row_sitemap['Page']<>'3' and $row_sitemap['Page']<>'9' ){ ?>
              <span>
                  <a class="fun-upload" href="<?php echo $RootLevel; ?>/include/index.php?Page=<?php if($row_sitemap['Page']=="5"){ echo "5-1";}else{ echo $row_sitemap['Page'];} ?>"  target="_blank"  title="線上更新2">
                  <i class="fas fa-cloud-upload-alt"></i></a>
              </span>
              
              <?php } ?>
              </td>
            </tr>
          <?php } while ($row_sitemap = mysqli_fetch_assoc($sitemap)); ?> 
        </tbody>
      </table>
    </div>

    </div>
  </div>

  <div class="row">
    <div class="col d-flex justify-content-center">
      <!-- paginations -->
      <nav aria-label="Page navigation example">
        <ul class="pagination">
          <?php if ($pageNum_sitemap > 0) { // Show if not first page ?>
          <li class="page-item">
            <a class="page-link" aria-label="Previous" href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, 0, $queryString_sitemap); ?>">
            第一頁</a>
          </li>
          <?php } // Show if not first page ?>

          <?php if ($pageNum_sitemap > 0) { // Show if not first page ?>
            <li class="page-item">
                <a class="page-link" href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, max(0, $pageNum_sitemap - 1), $queryString_sitemap); ?>">上一頁</a>
            </li>
          <?php } // Show if not first page ?>

          <?php if ($pageNum_sitemap < $totalPages_sitemap) { // Show if not last page ?>
            <li class="page-item">
                <a class="page-link" href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, min($totalPages_sitemap, $pageNum_sitemap + 1), $queryString_sitemap); ?>">下一頁</a>
            </li>
          <?php } // Show if not last page ?>
          <?php if ($pageNum_sitemap < $totalPages_sitemap) { // Show if not last page ?>
            <li class="page-item">
                <a class="page-link" href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, $totalPages_sitemap, $queryString_sitemap); ?>">最後一頁</a>
            </li>
          <?php } // Show if not last page ?>
        </ul>
      </nav>
      <!--end paginations -->
    </div>
  </div>

   
  </div>
<!-- end content -->
<?php
mysqli_free_result($sitemap);
?>
