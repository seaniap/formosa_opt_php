<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php
// 檢查是否有Admin 權限
if ($_SESSION['Mode'] <> "Admin") {
  echo "需有管理員權限才可管理使用者";
  exit;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf(
    "INSERT INTO " . $Table_ID . "_UserLogin (KeyID, UserName, Password, `Level`,CreatedTime,Remark) VALUES (%s, %s, %s, %s,%s,%s)",
    GetSQLValueString(uniqid(mt_rand()), "text"),
    GetSQLValueString($_POST['UserName'], "text"),
    GetSQLValueString($_POST['Password'], "text"),
    GetSQLValueString($_POST['Level'], "text"),
    GetSQLValueString(date("Y-m-d H:i:s"), "date"),
    GetSQLValueString($_POST['Remark'], "text")
  );
  //echo $insertSQL;

  $Result1 = mysqli_query($MySQL, $insertSQL) or die(mysqli_error($MySQL));
  $insertGoTo = "main.php?Page=1-1";


  //寫更新 log
  $userIP = $_SERVER['REMOTE_ADDR']; //收集瀏覽者的IP
  //$Domain=gethostbyaddr($userIP);
  $LogDate = Date("Y-m-j H:i:s");
  $sql = "insert into " . $Table_ID . "_WebLog Values ('',";
  $sql = $sql . "'$LogDate','" . $_POST['KeyID'] . "  " . $_POST['UserName'] . "-" . $_POST['Level'] . "','" . '新增使用者' . "','" . $_SESSION['UserName'] . "-" . $_SESSION['Mode'] . "','$userIP','線上更新','" . $_SESSION['GuestName'] . $_POST['Template'] . "-" . 'ins' . "',0)";
  //echo $sql;
  mysqli_query($MySQL, $sql);





  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>

<?php require_once('../lib/JavaLib.php'); ?>


<!-- start -->
<div class="container"></div>
<!-- end -->


<!-- 開始 -->
<div id="MainHeader">
  <div class="PageNumberRight"><img src="images/ICOedit.gif" width="14" height="12" align="absmiddle">新增使用者</div>
  <div class="whereYouAre">系統管理 &gt; 新增使用者</div>
</div>
<div class="datatable">
  <div class="headtext2">
    <div class="RecordData"> &nbsp; </div>
  </div>
  <!--Form begin -->
  <div class="UserFrom">
    <form method="post" name="form1" action="UserAdd.php">
      <fieldset>
        <!--第一項目 -->
        <legend>新增使用者</legend>
        <dl>
          <dt><label for="AdminUserName">使用者名稱</label></dt>
          <dd><input type="text" id="AdminUserName" class="" name="UserName" value="" tabindex="1" />
          </dd>
        </dl>
        <dl>
          <dt><label for="AdminPassword">使用者密碼</label></dt>
          <dd><input type="text" id="AdminPassword" class="textbox" name="Password" value="" tabindex="2" />
          </dd>
        </dl>
        <dl>
          <dt><label for="AdminLevel">使用者類型</label></dt>
          <dd><input type="radio" name="Level" value="Admin" id="AdminLevel" tabindex="3" class="styled" />Admin系統管理者</dd>
          <dd><input type="radio" name="Level" value="User" id="AdminLevel" tabindex="4" checked="checked" class="styled" />Userㄧ般使用者</dd>
        </dl>
        <dl>
          <dt><label for="AdminRemark">使用者建立來源</label></dt>
          <dd><input name="Remark" type="text" class="textbox" id="AdminRemark" value="Created by <?php echo $_SESSION['UserName']; ?>" size="32" tabindex="5"></dd>
        </dl>

        <!-- -->
        <div class="submit">
          <input type="button" class="button d-btn" value="新增使用者" tabindex="6" onClick="document.form1.submit();">
          <input type="hidden" name="ClientID" value="<?php echo $_SESSION['ClientID']; ?>">
          <input type="hidden" name="MM_insert" value="form1">
        </div>
      </fieldset>
    </form><!-- 結束 -->
  </div>
</div>