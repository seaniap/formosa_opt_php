<?php
//權限檢查不合格就 Logout 
if($_SESSION['Mode']=="") {
$FF_authFailedURL="../../Logout.php target=_TOP";
header("Location: $FF_authFailedURL");
exit;} 
?>
<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_Template = 10;
$pageNum_Template = 0;
if (isset($_GET['pageNum_Template'])) {
  $pageNum_Template = $_GET['pageNum_Template'];
}
$startRow_Template = $pageNum_Template * $maxRows_Template;

if($_GET['Where']<>"")
{ $WhereSQL=" AND T_Category='".$_GET['Where']."' ";}
else
{ $WhereSQL=" ";}


$query_Template = "SELECT * FROM ".$Table_ID."_Template WHERE 1=1 ".$WhereSQL;
$query_limit_Template = sprintf("%s LIMIT %d, %d", $query_Template, $startRow_Template, $maxRows_Template);
//echo $query_limit_Template;
$Template = mysqli_query($MySQL,$query_limit_Template) or die(mysqli_error($MySQL));
$row_Template = mysqli_fetch_assoc($Template);

if (isset($_GET['totalRows_Template'])) {
  $totalRows_Template = $_GET['totalRows_Template'];
} else {
  $all_Template = mysqli_query($MySQL,$query_Template);
  $totalRows_Template = mysqli_num_rows($all_Template);
}
$totalPages_Template = ceil($totalRows_Template/$maxRows_Template)-1;


$query_TGroup = "Select T_Category as Category from ".$Table_ID."_Template group by T_Category order by T_Category";
$TGroup = mysqli_query($MySQL,$query_TGroup) or die(mysqli_error($MySQL));
$row_TGroup = mysqli_fetch_assoc($TGroup);
$totalRows_TGroup = mysqli_num_rows($TGroup);

$queryString_Template = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Template") == false && 
        stristr($param, "totalRows_Template") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Template = "&" . implode("&", $newParams);
  }
}
$queryString_Template = sprintf("&totalRows_Template=%d%s", $totalRows_Template, $queryString_Template);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);
?>



 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>版型瀏覽</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/table.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(selObj,restore){ //v3.0
  eval("location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
</head>

<body class="TempBG"></p> 
<div id="MainHeader">
    <div class="PageNumberRight"><img src="images/ICOedit.gif" width="14" height="12" align="absmiddle">版型瀏覽</div>
    <div class="whereYouAre">網站管理 &gt; 版型瀏覽</div>
</div>
<div class="clear"></div>

<!--22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222 -->
<div class="datatable">
     <div class="headtext2">
            <div class="CheckBoxSelect">
              <form name="form1"> 請選擇分類 
			      <select name="menu1" onChange="MM_jumpMenu(this,0)"> 
				   <option value="TemplateViewC.php" <? if($_GET['Where']=="") {echo "selected";}?> >全部顯示  </option>     
                <?php do {  ?>
				
				<option value="TemplateViewC.php?Where=<? echo $row_TGroup['Category']; ?>" <? if ($_GET['Where']==$row_TGroup['Category']) {echo "selected"; }       ?>><? echo $row_TGroup['Category']; ?></option>
                  
                <?php } while ($row_TGroup = mysqli_fetch_assoc($TGroup)); ?>
                  </select>             
              </form>
            </div>
            <div class="RecordData">紀錄<?php echo ($startRow_Template + 1) ?>到<?php echo $totalRows_Template ?>共 <?php echo $totalRows_Template ?>筆
            </div>
     </div>
     
     <div>
     <!--版型表列開始 --><?php do { ?>
     <div id="templateList">
     <div id="templateImg">
        <p><a href="TemplateDetail.php?Template=<?php echo $row_Template['T_FileName']; ?>"><img src="<?php echo "TemplateIcon/".$row_Template['T_Icon']; ?>" width="200" height="150" border="0"></a></p>
        <p><a href="TemplateDetail.php?Template=<?php echo $row_Template['T_FileName']; ?>"><?php echo $row_Template['T_Name']; ?></a></p>
       </div>
     <div id="teplateDiscription"><table width="100%" border="0" cellpadding="5" cellspacing="0">
                          <tr> 
                            <td align="left"> <table width="50%" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tr> 
                                  
										
										
 <td width="20%">
									<table width="100%" border="0" cellpadding="2" cellspacing="0">
                                      <tr> 
                                        <td width="3%"><div align="center"><a href="#.htm"><img src="images/ICOadd.gif" alt="發送紀錄" width="14" height="15" border="0"></a></div></td>
                                        <td width="97%" > <div align="left"><a href="TemplateAdd.php?<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."Action=ins&T_ID=".$row_Template['T_ID'] ?>">新增版型</a></div></td>
                                      </tr>
                                    </table></td>										
										
										
										
<td width="20%">
									<table width="100%" border="0" cellpadding="2" cellspacing="0">
                                      <tr> 
                                        <td width="3%"><div align="center"><a href="#.htm"><img src="images/ICOadd.gif" alt="發送紀錄" width="14" height="15" border="0"></a></div></td>
                                        <td width="97%" > <div align="left"><a href="TemplateAdd.php?<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."Action=del&T_ID=".$row_Template['T_ID'] ?>">刪除版型</a></div></td>
                                      </tr>
                                    </table></td>												
										
										
                                      </tr>
                                    </table></td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr> 
                            <td valign="top" class="TextArea"><div align="left"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr valign="top"> 
                                    <td width="14%" class="keyword"><div align="left">說明</div></td>
                                    <td width="86%"> <div align="left"><?php echo $row_Template['T_Remark']; ?>
									
<? 
$query_TemplateDetail = sprintf("SELECT * FROM ".$Table_ID."_TemplateDetail WHERE Template = '%s' ORDER BY Sq ASC", $row_Template['T_FileName']);
$TemplateDetail = mysqli_query($MySQL,$query_TemplateDetail) or die(mysqli_error($MySQL));	
 do { ?>          
     <?php echo $row_TemplateDetail['Sq'],"   ".$row_TemplateDetail['FieldID']."   ".$row_TemplateDetail['Type']."   ".$row_TemplateDetail['FieldTitle']; ?>   	 
    <br>
	  <?php } while ($row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail)); ?>												
																		
									</div></td>
                                  </tr>
                                </table>
                              </div></td>
                          </tr>
                        </table></div>
     <div class="clear"></div>
     </div><?php } while ($row_Template = mysqli_fetch_assoc($Template)); ?>
     <!--版型表列結束 -->
     </div>
    <div class="bottomtext2">
            <table border="0" width="30%" align="right">
              <tr> 
                <td width="23%" align="center"> <?php if ($pageNum_Template > 0) { // Show if not first page ?>
                  <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage, 0, $queryString_Template); ?>"><img src="images/First.gif" border=0></a> 
                  <?php } // Show if not first page ?> </td>
                <td width="15%" align="center"> <?php if ($pageNum_Template > 0) { // Show if not first page ?>
                  <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage, max(0, $pageNum_Template - 1), $queryString_Template); ?>"><img src="images/Previous.gif" border=0></a> 
                  <?php } // Show if not first page ?> </td>
                <td width="16%" align="center">
				
<?php if($totalPages_Template>0) { // 如果總頁數大於1
	for($i=-4;$i<=5;$i++) {   
	     if(($_GET['pageNum_Template']+$i)>=0 and ($_GET['pageNum_Template']+$i)<= $totalPages_Template)
    {
	?>
	      <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage,$_GET['pageNum_Template']+$i , $queryString_Template); ?>">
		  <? printf("%d ",$_GET['pageNum_Template']+$i+1); ?></a>
          <?php
		   }
		  } 
} ?>									
				
				</td>
                <td width="23%" align="center"> <?php if ($pageNum_Template < $totalPages_Template) { // Show if not last page ?>
                  <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage, min($totalPages_Template, $pageNum_Template + 1), $queryString_Template); ?>"><img src="images/Next.gif" border=0></a> 
                  <?php } // Show if not last page ?> </td>
                <td width="23%" align="center"> <?php if ($pageNum_Template < $totalPages_Template) { // Show if not last page ?>
                  <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage, $totalPages_Template, $queryString_Template); ?>"><img src="images/Last.gif" border=0></a> 
                  <?php } // Show if not last page ?> </td>
              </tr>
            </table>
            <div class="clear"></div>
    </div>
</div>
<!--22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222 -->


</body>
</html>
<?php
mysqli_free_result($Template);

mysqli_free_result($TGroup);
?>


