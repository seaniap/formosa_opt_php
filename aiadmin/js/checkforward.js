function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function trim(str) {
	while (str.indexOf(" ")==0) {
		str = str.substring(1, str.length);
	}
	while ((str.length>0) && (str.indexOf(" ")==(str.length-1))) {
		str = str.substring(0, str.length-1);
	}
	return str;
}

function clearForm() {
    window.location.reload();
	 
	}
	
function chk_form(myform) {

             

 if( myform.Name.value == '' ) {
         myform.Name.focus();
         alert("請填寫姓名!");
         return false;
      }
  
		if (!check_input_name(myform.Name)){
				 myform.Name.focus();
				 return false;
			  }

/*
if( myform.h01.value == '' ) {
         myform.h01.focus();
         alert("請填寫暱稱!");
         return false;
      }	  
	 

 if (!check_input_idcard_no(myform.MemberID)){
         //myform.MemberID.focus();
         return false;
      }              
  
*/	 
	  
  if (myform.h03.value == '') {
         alert ('請填寫您的電話號碼!!');
         myform.h03.focus();
         return false;
      }           
          
      var p2 = myform.h03.value.length;
      if (p2>15) {
         myform.h03.focus();
         alert("電話號碼請勿超過15個字!");
         return false;
      }   
	  var p2 = myform.h03.value.length;
	  if (p2<7) {
         myform.h03.focus();
         alert("電話號碼請勿少於7個字!");
         return false;
      }

/*
 if (myform.h04.value == '') {
         alert ('請填寫您的手機號碼!!');
         myform.h04.focus();
         return false;
      }           
          
      var p4 = myform.h04.value.length;
      if (p4!=10) {
         myform.h04.focus();
         alert("請填寫手機號碼共10碼!");
         return false;
      }   	 
	 
	 
	   
      if( myform.h06.value == '' ) {
         myform.h06.focus();
         alert("請選擇縣市!");
         return false;
      }

      if( myform.h07.value == '' ) {
         myform.h07.focus();
         alert("請選擇鄉鎮市區!");
         return false;
      }

      if( myform.h08.value == '' ) {
         myform.h08.focus();
         alert("請填寫連絡地址!");
         return false;
      }   
*/	
      if (myform.Email.value == ''){
         alert ('請填寫您的電子郵件信箱!!');
         myform.Email.focus();
         return false;
      }
     
      if (!check_email(myform.Email.value)){
         alert ('您填寫的電子郵件信箱格式有誤, 請檢查!!');
         myform.Email.focus();
         return false;
      }  
	  
	if( myform.h09.value == '' ) {
	myform.h09.focus();
	alert("請填寫您的發票號碼!");
	return false;
	}
	
	if((myform.h09.value != '') && (myform.h09.value.length != 10)) {
		alert('統一發票開獎號碼格式錯誤，請您檢查一下！'); 
		return false;
		}
                              
	  answer = confirm("確認送出?");
   if (answer)
      myform.submit();	
	  
	  
	   }
/*	   
function checkNum(tel_obj)
 // 立刻檢查輸入的電話是否不是數字
	{
		txt = document.tel_obj.value;
		for(i=0; i<txt.length;i++)
		{
			c = txt.charAt(i);
			if("0123456789".indexOf(c,0)<0)
			{alert("請輸入數字");return;}
			}
		}	
*/		
		
  function check_input_name( name_obj ) {
   // 立刻檢查輸入的姓名是否有英數字
   name_str = name_obj.value;
   
   var len = name_str.length;
   if(len==0)
      return false;
      
   for(var i=0;i<len;i++){
     var c= name_str.charAt(i);
      if(((c>="A"&&c<="Z")||(c>="a"&&c<="z")||(c>="0"&&c<="9")||(c=="-")||(c=="_")||(c==".")||(c=="@")||(c=="~")||(c==" ") )) {
      
         alert ('配合作業, 請輸入中文姓名, 請勿輸入英文稱呼, 或空白!!');
         name_obj.focus();
         return false;
      }
   }
   return true;
}

function check_email ( email )	//檢查email信箱的正確性
{
   var len = email.length;
   if(len==0)
      return false;
   for(var i=0;i<len;i++)
   {  var c= email.charAt(i);
      if(!((c>="A"&&c<="Z")||(c>="a"&&c<="z")||(c>="0"&&c<="9")||(c=="-")||(c=="_")||(c==".")||(c=="@")))
         return false;
   }
   if((email.indexOf("@")==-1)||(email.indexOf("@")==0)||(email.indexOf("@")==(len-1)))
      return false;
   if((email.indexOf("@")!=-1)&&(email.substring(email.indexOf("@")+1,len).indexOf("@")!=-1))
      return false;
   if((email.indexOf(".")==-1)||(email.indexOf(".")==0)||(email.lastIndexOf(".")==(len-1)))
      return false;
     
   ///////////////////////////////////////   
   
   var tail1,tail2;
   len = email.length;
   tail1 = email.substring(email.indexOf("@")+1,len)
   var posi = tail1.indexOf(".");
   while( posi > 0 ) {
      tail2 = tail1.substring(0, tail1.indexOf(".") )
      len = tail2.length;
      
      if (len < 2)
         return false; // 不可過短

      len = tail1.length;
      tail1 = tail1.substring(tail1.indexOf(".")+1,len )

      posi = tail1.indexOf(".");
      
   }

   len = tail1.length;
   if (len < 2)
      return false; // 不可過短
      
   if ( pure_deci(tail1) )
      return false; // 最後一段 不可全為數字
    
      
   ///////////////////////////////////////  
     
   if( (email.indexOf('@.')) >= 0 ) // 不可 為 abc@.com
      return false;
   //alert(email);   
   return true;
}

// 判斷傳入字串 是否全為數字
function pure_deci( val_str ) {
   // 假設全為數字
   var all_deci = true ;
   len = val_str.length;
   for(var i=0;i<len;i++)
   {  var c= val_str.charAt(i);
      if (! (c>="0"&&c<="9") )
         all_deci = false;
   }
   return all_deci;
}

function check_input_idcard_no( idcard_no_obj ) {
   // 立刻檢查輸入的身分證全碼是否合理 
   if (!CheckPID(idcard_no_obj.value)) {
      alert ('請正確填寫您的身分證字號!!');
      idcard_no_obj.focus();
      return false;
   }
   else{
	return true;   
   }
}

function CheckPID(sPID) {
        var bChk = true;
        var sMsg = "正確";
        sPID = trim(sPID.toUpperCase());
        var iPIDLen = String(sPID).length;

        var iChkNum=0;

        if (iPIDLen!=10) {
                sMsg = "這個身分證字號長度不合法！";
                bChk = false;
        } else {
                var sChk = sPID;
                for(i=0;i<iPIDLen;i++) {
                        if (sChk.indexOf(sPID.substr(i,1))==-1) {
                                sMsg = "這個身分證字號含有不正確的字元！";
                                bChk = false;
                                break;
                        }
                }
        }

        if (bChk) {
                iChkNum = sPID.indexOf(sPID.substr(0,1));
				
                if (iChkNum==-1) {
                        sMsg = "身分證字號第一個字應為英文字母！";
						
                        bChk = false;
                } else {
                        iChkNum += 10;
                        if ((sPID.indexOf("1")!=1) && (sPID.indexOf("2")!=1)) {
                                sMsg = "身分證字號第二個字應為 1 或 2！";
                                bChk = false;
                        }
                }
        }

        if (bChk) {
                iChkNum = Math.floor(iChkNum/10) + (iChkNum%10*9);
                for(var i=1; i<iPIDLen-1; i++) {
                        iChkNum += sPID.substr(i,1) * (9-i);
                }

                var iLastNum = sPID.substr(9,1)*1;
				
				var ss = sPID.substr(0,1);
			//	ss = toUpperCase(ss);
			//	alert(ss);
			//	if (sPID.substr(0,1)=='A' or sPID.substr(0,1)=='a' )
			//	{nn=10;}
				if (ss == "A")
				{nn=0;}
				if (ss == "B")
				{nn=9;}
				if (ss == "C")
				{nn=8;}
				if (ss == "D")
				{nn=7;}
				if (ss == "E")
				{nn=6;}
				if (ss == "F")
				{nn=5;}
				if (ss == "G")
				{nn=4;}
				if (ss == "H")
				{nn=3;}
				if (ss == "I")
				{nn=8;}
				if (ss == "J")
				{nn=2;}
				if (ss == "K")
				{nn=1;}
				if (ss == "L")
				{nn=1;}
				if (ss == "M")
				{nn=0;}
				if (ss == "N")
				{nn=9;}
				if (ss == "O")
				{nn=7;}
				if (ss == "P")
				{nn=8;}
				if (ss == "Q")
				{nn=7;}
				if (ss == "R")
				{nn=6;}
				if (ss == "S")
				{nn=5;}
				if (ss == "T")
				{nn=4;}
				if (ss == "U")
				{nn=3;}
				if (ss == "V")
				{nn=2;}
				if (ss == "W")
				{nn=0;}
				if (ss == "X")
				{nn=2;}
				if (ss == "Y")
				{nn=1;}
				if (ss == "Z")
				{nn=9;}
				
			//	alert(ss);
				
				//alert(nn);
                iChkNum += iLastNum;
            //    alert(iChkNum);
				iChkNum += nn;
			//	alert(iChkNum);
                if ((iChkNum % 10) !=0) {
                        sMsg = "這個身分證字號不合法！";
                        bChk = false;
                        for (i=0;i<10;i++) {
                                var xRightAlpNum = iChkNum - iLastNum + i;
                                if ((xRightAlpNum % 10) ==0) {
                                        sMsg += "最後一個數應為：" + i;
                                }
                        }
                }
        }
  // alert( sMsg);
   return bChk;    
} 
