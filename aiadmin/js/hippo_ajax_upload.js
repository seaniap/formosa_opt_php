var XMLHttpRequestObject = createXMLHttpRequestObject();

function createXMLHttpRequestObject()
{
  var XMLHttpRequestObject = false;
  
  try
  {
    XMLHttpRequestObject = new XMLHttpRequest();
  }
  catch(e)
  {
    var aryXmlHttp = new Array(
                               "MSXML2.XMLHTTP",
                               "Microsoft.XMLHTTP",
                               "MSXML2.XMLHTTP.6.0",
                               "MSXML2.XMLHTTP.5.0",
                               "MSXML2.XMLHTTP.4.0",
                               "MSXML2.XMLHTTP.3.0"
                               );
    for (var i=0; i<aryXmlHttp.length && !XMLHttpRequestObject; i++)
    {
      try
      {
        XMLHttpRequestObject = new ActiveXObject(aryXmlHttp[i]);
      } 
      catch (e) {}
    }
  }
  
  if (!XMLHttpRequestObject)
  {
    alert("Error: failed to create the XMLHttpRequest object.");
  }
  else 
  {
    return XMLHttpRequestObject;
  }
}

function UploadFormData(obj, idForm, dataSource, divID)
{
  document.getElementById(idForm).submit();
  var uploadDir = obj.value;
  var uploader = obj.name;
  try
  {
    var objDiv = parent.document.getElementById(divID);
    objDiv.innerHTML = "<img src=../images/loading.gif>";
  }
  catch(e){}
}

function ProcessData(uploadDir, dataSource, divID, widthThumb, heightThumb)
{
  if(XMLHttpRequestObject)
  {
    dataSource += '&uploadDir='+uploadDir;
    dataSource += '&widthThumb='+widthThumb;
    dataSource += '&heightThumb='+heightThumb;
    dataSource += '&parm='+new Date().getTime();
    
    XMLHttpRequestObject.open("GET", dataSource);
    XMLHttpRequestObject.onreadystatechange = function()
    {
      try
      {
        if (XMLHttpRequestObject.readyState == 4 &&
            XMLHttpRequestObject.status == 200)
        {
		 var objDiv = parent.document.getElementById(divID);
          objDiv.innerHTML = "<p>檔案上傳已完成...... </p>";
			
			
			
			TdivID="show"+divID;
          var objDiv = parent.opener.document.getElementById(TdivID);
        //  objDiv.innerHTML = XMLHttpRequestObject.responseText;
		    objDiv.innerHTML ="<img src='"+XMLHttpRequestObject.responseText+"' width="+widthThumb+" >";
		  //alert(objDiv.innerHTML);
		  TdivID="t"+divID;
		  objDiv=parent.opener.document.getElementById(TdivID);
		  objDiv.value=XMLHttpRequestObject.responseText;
		  	 
		  top.window.close();
		 
		  parent.window.close(); // 上傳結束後關閉視窗 
		//  parent.opener.document.form1.toString(divID).value=XMLHttpRequestObject.responseText;
		
		 // window.opener.objDiv.innerHTML = XMLHttpRequestObject.responseText;
		//  window.opener.showDiv1.innerHTML = XMLHttpRequestObject.responseText;
		
		 // alert("OK");
		//  form2.showDiv1.innerHTML = XMLHttpRequestObject.responseText;
		  
          /*eval(XMLHttpRequestObject.responseText);*/
        }
        else
        {
          var objDiv = parent.document.getElementById(divID);
          objDiv.innerHTML = "<p>檔案上傳中......   <img src=../images/loading.gif></p>";
        }
      }
      catch(e){}
    }
    try
    {
      XMLHttpRequestObject.send(null);
    }
    catch(e){}
  }
}

function errUpload(errMsg, divID)
{
  try
  {
    var objDiv = parent.document.getElementById(divID);
    objDiv.innerHTML = "<font color=red>"+errMsg+"</font>";
  }
  catch(e){}
}