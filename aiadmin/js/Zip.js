function update_phone( Name, mForm, nForm )
{

	update_zip( Name, nForm);	
	mForm.value="";	
	switch( Name ){
		case "":
			mForm.value="";
			break;
		case "基隆市":
			mForm.value="02";
			break;
		case "台北市":
			mForm.value="02";
			break;
		case "台北縣":
			mForm.value="02";
			break;
		case "桃園縣":
			mForm.value="03";
			break;
		case "新竹市":
			mForm.value="03";
			break;
		case "新竹縣":
			mForm.value="03";
			break;
		case "苗栗縣":
			mForm.value="037";
			break;
		case "台中市":
			mForm.value="04";
			break;
		case "台中縣":
			mForm.value="04";
			break;
		case "彰化縣":
			mForm.value="04";
			break;
        case "南投縣":
			mForm.value="049";
			break;
		case "雲林縣":
			mForm.value="05";
			break;
		case "嘉義市":
			mForm.value="05";
			break;
		case "嘉義縣":
			mForm.value="05";
			break;
		case "台南市":
			mForm.value="06";
			break;
		case "台南縣":
			mForm.value="06";
			break;
		case "高雄市":
			mForm.value="07";
			break;
		case "高雄縣":
			mForm.value="07";
			break;
		case "屏東縣":
			mForm.value="08";
			break;
		case "宜蘭縣":
			mForm.value="03";
			break;
		case "花蓮縣":
			mForm.value="03";
			break;
		case "台東縣":
			mForm.value="089";
			break;
		case "馬祖":
			mForm.value="0836";
			break;
		case "金門":
			mForm.value="0823";
			break;
		case "澎湖縣":
			mForm.value="06";
			break;
		case "國外地區":
			mForm.value="00";
			break;
		default:
			mForm.value="";
			
	}
}

function update_zip( Name, nForm)
{
	Zip=nForm.value ;
	nForm.length = 0;
	switch( Name ){
		case "台北市":
			nForm.options[0] = new Option("請選擇...","請選擇...");			
			nForm.options[1] = new Option("中正區,100");
			if(Zip=="中正區,100") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("大同區,103");
			if(Zip=="大同區,103") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("中山區,104");
			if(Zip=="中山區,104") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("松山區,105");
			if(Zip=="松山區,105") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("大安區,106");
			if(Zip=="大安區,106") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("萬華區,108");
			if(Zip=="萬華區,108") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("信義區,110");
			if(Zip=="信義區,110") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("士林區,111");
			if(Zip=="士林區,111") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("北投區,112");
			if(Zip=="北投區,112") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("內湖區,114");
			if(Zip=="內湖區,114") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("南港區,115");
			if(Zip=="南港區,115") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("文山區,116");
			if(Zip=="文山區,116") {nForm.options[12].selected=true;}
			break;

		case "基隆市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("仁愛區,200");
			if(Zip=="仁愛區,200") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("信義區,201");
			if(Zip=="信義區,201") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("中正區,202");
			if(Zip=="中正區,202") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("中山區,203");
			if(Zip=="中山區,203") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("安樂區,204");
			if(Zip=="安樂區,204") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("暖暖區,205");
			if(Zip=="暖暖區,205") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("七堵區,206");
			if(Zip=="七堵區,206") {nForm.options[7].selected=true;}
			break;

		case "台北縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("萬里,207");
			if(Zip=="萬里,207") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("金山,208");
			if(Zip=="金山,208") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("板橋,220");
			if(Zip=="板橋,220") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("汐止,221");
			if(Zip=="汐止,221") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("深坑,222");
			if(Zip=="深坑,222") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("石碇,223");
			if(Zip=="石碇,223") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("瑞芳,224");
			if(Zip=="瑞芳,224") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("平溪,226");
			if(Zip=="平溪,226") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("雙溪,227");
			if(Zip=="雙溪,227") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("貢寮,228");
			if(Zip=="貢寮,228") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("新店,231");
			if(Zip=="新店,231") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("坪林,232");
			if(Zip=="坪林,232") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("烏來,233");
			if(Zip=="烏來,233") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("永和,234");
			if(Zip=="永和,234") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("中和,235");
			if(Zip=="中和,235") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("土城,236");
			if(Zip=="土城,236") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("三峽,237");
			if(Zip=="三峽,237") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("樹林,238");
			if(Zip=="樹林,238") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("鶯歌,239");
			if(Zip=="鶯歌,239") {nForm.options[19].selected=true;}
			nForm.options[20] = new Option("三重,241");
			if(Zip=="三重,241") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("新莊,242");
			if(Zip=="新莊,242") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("泰山,243");
			if(Zip=="泰山,243") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("林口,244");
			if(Zip=="林口,244") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("蘆洲,247");
			if(Zip=="蘆洲,247") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("五股,248");
			if(Zip=="五股,248") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("八里,249");
			if(Zip=="八里,249") {nForm.options[26].selected=true;}
			nForm.options[27] = new Option("淡水,251");
			if(Zip=="淡水,251") {nForm.options[27].selected=true;}
			nForm.options[28] = new Option("三芝,252");
			if(Zip=="三芝,252") {nForm.options[28].selected=true;}
			nForm.options[29] = new Option("石門,253");
			if(Zip=="石門,253") {nForm.options[29].selected=true;}
			break;
		case "宜蘭縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("宜蘭,260");
			if(Zip=="宜蘭,260") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("頭城,261");
			if(Zip=="頭城,261") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("礁溪,262");
			if(Zip=="礁溪,262") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("壯圍,263");
			if(Zip=="壯圍,263") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("員山,264");
			if(Zip=="員山,264") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("羅東,265");
			if(Zip=="羅東,265") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("三星,266");
			if(Zip=="三星,266") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("大同,267");
			if(Zip=="大同,267") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("五結,268");
			if(Zip=="五結,268") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("冬山,269");
			if(Zip=="冬山,269") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("蘇澳,270");
			if(Zip=="蘇澳,270") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("南澳,272");
			if(Zip=="南澳,272") {nForm.options[12].selected=true;}
			break;
		case "新竹市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("新竹市,300");
			if(Zip=="新竹市,300") {nForm.options[1].selected=true;}
			break;
		case "新竹縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("竹北,302");
			if(Zip=="竹北,302") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("湖口,303");
			if(Zip=="湖口,303") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("新豐,304");
			if(Zip=="新豐,304") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("新埔,305");
			if(Zip=="新埔,305") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("關西,306");
			if(Zip=="關西,306") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("芎林,307");
			if(Zip=="芎林,307") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("寶山,308");
			if(Zip=="寶山,308") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("竹東,310");
			if(Zip=="竹東,310") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("五峰,311");
			if(Zip=="五峰,311") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("橫山,312");
			if(Zip=="橫山,312") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("尖石,313");
			if(Zip=="尖石,313") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("北埔,314");
			if(Zip=="北埔,314") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("峨眉,315");
			if(Zip=="峨眉,315") {nForm.options[13].selected=true;}
  			break;
		case "桃園縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("中壢,320");
			if(Zip=="中壢,320") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("平鎮,324");
			if(Zip=="平鎮,324") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("龍潭,325");
			if(Zip=="龍潭,325") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("楊梅,326");
			if(Zip=="楊梅,326") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("新屋,327");
			if(Zip=="新屋,327") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("觀音,328");
			if(Zip=="觀音,328") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("桃園,330");
			if(Zip=="桃園,330") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("龜山,333");
			if(Zip=="龜山,333") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("八德,334");
			if(Zip=="八德,334") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("大溪,335");
			if(Zip=="大溪,335") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("復興,336");
			if(Zip=="復興,336") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("大園,337");
			if(Zip=="大園,337") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("蘆竹,338");
			if(Zip=="蘆竹,338") {nForm.options[13].selected=true;}
  			break;
		case "苗栗縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("竹南,350");
			if(Zip=="竹南,350") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("頭份,351");
			if(Zip=="頭份,351") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("三灣,352");
			if(Zip=="三灣,352") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("南庄,353");
			if(Zip=="南庄,353") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("獅潭,354");
			if(Zip=="獅潭,354") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("後龍,356");
			if(Zip=="後龍,356") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("通霄,357");
			if(Zip=="通霄,357") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("苑裡,358");
			if(Zip=="苑裡,358") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("苗栗,360");
			if(Zip=="苗栗,360") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("造橋,361");
			if(Zip=="造橋,361") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("頭屋,362");
			if(Zip=="頭屋,362") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("公館,363");
			if(Zip=="公館,363") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("大湖,364");
			if(Zip=="大湖,364") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("泰安,365");
			if(Zip=="泰安,365") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("銅鑼,366");
			if(Zip=="銅鑼,366") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("三義,367");
			if(Zip=="三義,367") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("西湖,368");
			if(Zip=="西湖,368") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("卓蘭,369");
			if(Zip=="卓蘭,369") {nForm.options[18].selected=true;}
  			break;
		case "台中市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("中區,400");
			if(Zip=="中區,400") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("東區,401");
			if(Zip=="東區,401") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("南區,402");
			if(Zip=="南區,402") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("西區,403");
			if(Zip=="西區,403") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("北區,404");
			if(Zip=="北區,404") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("北屯區,406");
			if(Zip=="北屯區,406") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("西屯區,407");
			if(Zip=="西屯區,407") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("南屯區,408");
			if(Zip=="南屯區,408") {nForm.options[8].selected=true;}
			break;
		case "台中縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("太平,411");
			if(Zip=="太平,411") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("大里,412");
			if(Zip=="大里,412") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("霧峰,413");
			if(Zip=="霧峰,413") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("烏日,414");
			if(Zip=="烏日,414") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("豐原,420");
			if(Zip=="豐原,420") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("后里,421");
			if(Zip=="后里,421") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("石岡,422");
			if(Zip=="石岡,422") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("東勢,423");
			if(Zip=="東勢,423") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("和平,424");
			if(Zip=="和平,424") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("新社,426");
			if(Zip=="新社,426") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("潭子,427");
			if(Zip=="潭子,427") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("大雅,428");
			if(Zip=="大雅,428") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("神岡,429");
			if(Zip=="神岡,429") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("大肚,432");
			if(Zip=="大肚,432") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("沙鹿,433");
			if(Zip=="沙鹿,433") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("龍井,434");
			if(Zip=="龍井,434") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("梧棲,435");
			if(Zip=="梧棲,435") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("清水,436");
			if(Zip=="清水,436") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("大甲,437");
			if(Zip=="大甲,437") {nForm.options[19].selected=true;}
            nForm.options[20] = new Option("外埔,438");			
            if(Zip=="外埔,438") {nForm.options[20].selected=true;}	
			nForm.options[21] = new Option("大安,439");  		
			if(Zip=="大安,439") {nForm.options[21].selected=true;}	
            break;
		case "彰化縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("彰化,500");
			if(Zip=="彰化,500") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("芬園,502");
			if(Zip=="芬園,502") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("花壇,503");
			if(Zip=="花壇,503") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("秀水,504");
			if(Zip=="秀水,504") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("鹿港,505");
			if(Zip=="鹿港,505") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("福興,506");
			if(Zip=="福興,506") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("線西,507");
			if(Zip=="線西,507") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("和美,508");
			if(Zip=="和美,508") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("伸港,509");
			if(Zip=="伸港,509") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("員林,510");
			if(Zip=="員林,510") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("社頭,511");
			if(Zip=="社頭,511") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("永靖,512");
			if(Zip=="永靖,512") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("埔心,513");
			if(Zip=="埔心,513") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("溪湖,514");
			if(Zip=="溪湖,514") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("大村,515");
			if(Zip=="大村,515") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("埔鹽,516");
			if(Zip=="埔鹽,516") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("田中,520");
			if(Zip=="田中,520") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("北斗,521");
			if(Zip=="北斗,521") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("田尾,522");
			if(Zip=="田尾,522") {nForm.options[19].selected=true;}
            nForm.options[20] = new Option("埤頭,523");
            if(Zip=="埤頭,523") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("溪洲,524");
			if(Zip=="溪洲,524") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("竹塘,525");
			if(Zip=="竹塘,525") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("二林,526");
			if(Zip=="二林,526") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("大城,527");
			if(Zip=="大城,527") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("芳苑,528");
			if(Zip=="芳苑,528") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("二水,530");
			if(Zip=="二水,530") {nForm.options[26].selected=true;}
			break;
		case "南投縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("南投,540");
			if(Zip=="南投,540") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("中寮,541");
			if(Zip=="中寮,541") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("草屯,542");
			if(Zip=="草屯,542") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("國姓,544");
			if(Zip=="國姓,544") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("埔里,545");
			if(Zip=="埔里,545") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("仁愛,546");
			if(Zip=="仁愛,546") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("名間,551");
			if(Zip=="名間,551") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("集集,552");
			if(Zip=="集集,552") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("水里,553");
			if(Zip=="水里,553") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("魚池,555");
			if(Zip=="魚池,555") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("信義,556");
			if(Zip=="信義,556") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("竹山,557");
			if(Zip=="竹山,557") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("鹿谷,558");
			if(Zip=="鹿谷,558") {nForm.options[13].selected=true;}
			break;
		case "嘉義市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("嘉義市,600");
			if(Zip=="嘉義市,600") {nForm.options[1].selected=true;}
			break;
		case "嘉義縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("番路,602");
			if(Zip=="番路,602") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("梅山,603");
			if(Zip=="梅山,603") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("竹崎,604");
			if(Zip=="竹崎,604") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("阿里山,605");
			if(Zip=="阿里山,605") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("中埔,606");
			if(Zip=="中埔,606") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("大埔,607");
			if(Zip=="大埔,607") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("水上,608");
			if(Zip=="水上,608") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("鹿草,611");
			if(Zip=="鹿草,611") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("太保,612");
			if(Zip=="太保,612") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("朴子,613");
			if(Zip=="朴子,613") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("東石,614");
			if(Zip=="東石,614") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("六腳,615");
			if(Zip=="六腳,615") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("新港,616");
			if(Zip=="新港,616") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("民雄,621");
			if(Zip=="民雄,621") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("大林,622");
			if(Zip=="大林,622") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("溪口,623");
			if(Zip=="溪口,623") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("義竹,624");
			if(Zip=="義竹,624") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("布袋,625");
			if(Zip=="布袋,625") {nForm.options[18].selected=true;}
  			break;
		case "雲林縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("斗南,630");
			if(Zip=="斗南,630") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("大埤,631");
			if(Zip=="大埤,631") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("虎尾,632");
			if(Zip=="虎尾,632") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("土庫,633");
			if(Zip=="土庫,633") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("褒忠,634");
			if(Zip=="褒忠,634") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("東勢,635");
			if(Zip=="東勢,635") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("臺西,636");
			if(Zip=="臺西,636") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("崙背,637");
			if(Zip=="崙背,637") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("麥寮,638");
			if(Zip=="麥寮,638") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("斗六,640");
			if(Zip=="斗六,640") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("林內,643");
			if(Zip=="林內,643") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("古坑,646");
			if(Zip=="古坑,646") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("莿桐,647");
			if(Zip=="莿桐,647") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("西螺,648");
			if(Zip=="西螺,648") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("二崙,649");
			if(Zip=="二崙,649") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("北港,651");
			if(Zip=="北港,651") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("水林,652");
			if(Zip=="水林,652") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("口湖,653");
			if(Zip=="口湖,653") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("四湖,654");
			if(Zip=="四湖,654") {nForm.options[19].selected=true;}
            nForm.options[20] = new Option("元長,655");
            if(Zip=="元長,655") {nForm.options[20].selected=true;}
			break;
		case "台南市": 
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("中區,700");
			if(Zip=="中區,700") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("東區,701");
			if(Zip=="東區,701") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("南區,702");
			if(Zip=="南區,702") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("西區,703");
			if(Zip=="西區,703") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("北區,704");
			if(Zip=="北區,704") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("安平區,708");
			if(Zip=="安平區,708") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("安南區,709");
			if(Zip=="安南區,709") {nForm.options[7].selected=true;}
			break;
		case "台南縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("永康,710");
			if(Zip=="永康,710") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("歸仁,711");
			if(Zip=="歸仁,711") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("新化,712");
			if(Zip=="新化,712") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("左鎮,713");
			if(Zip=="左鎮,713") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("玉井,714");
			if(Zip=="玉井,714") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("楠西,715");
			if(Zip=="楠西,715") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("南化,716");
			if(Zip=="南化,716") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("仁德,717");
			if(Zip=="仁德,717") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("關廟,718");
			if(Zip=="關廟,718") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("龍崎,719");
			if(Zip=="龍崎,719") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("官田,720");
			if(Zip=="官田,720") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("麻豆,721");
			if(Zip=="麻豆,721") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("佳里,722");
			if(Zip=="佳里,722") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("西港,723");
			if(Zip=="西港,723") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("七股,724");
			if(Zip=="七股,724") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("將軍,725");
			if(Zip=="將軍,725") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("學甲,726");
			if(Zip=="學甲,726") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("北門,727");
			if(Zip=="北門,727") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("新營,730");
			if(Zip=="新營,730") {nForm.options[19].selected=true;}
            nForm.options[20] = new Option("後壁,731");			
            if(Zip=="後壁,731") {nForm.options[20].selected=true;}	
			nForm.options[21] = new Option("白河,732");
			if(Zip=="白河,732") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("東山,733");
			if(Zip=="東山,733") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("六甲,734");
			if(Zip=="六甲,734") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("下營,735");
			if(Zip=="下營,735") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("柳營,736");
			if(Zip=="柳營,736") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("鹽水,737");
			if(Zip=="鹽水,737") {nForm.options[26].selected=true;}
			nForm.options[27] = new Option("善化,741");
			if(Zip=="善化,741") {nForm.options[27].selected=true;}
			nForm.options[28] = new Option("大內,742");
			if(Zip=="大內,742") {nForm.options[28].selected=true;}
			nForm.options[29] = new Option("山上,743");
			if(Zip=="山上,743") {nForm.options[29].selected=true;}
			nForm.options[30] = new Option("新市,744");
			if(Zip=="新市,744") {nForm.options[30].selected=true;}
			nForm.options[31] = new Option("安定,745");
			if(Zip=="安定,745") {nForm.options[31].selected=true;}
			break;
		case "高雄市":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("新興區,800");
			if(Zip=="新興區,800") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("前金區,801");
			if(Zip=="前金區,801") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("苓雅區,802");
			if(Zip=="苓雅區,802") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("鹽埕區,803");
			if(Zip=="鹽埕區,803") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("鼓山區,804");
			if(Zip=="鼓山區,804") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("旗津區,805");
			if(Zip=="旗津區,805") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("前鎮區,806");
			if(Zip=="前鎮區,806") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("三民區,807");
			if(Zip=="三民區,807") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("楠梓區,811");
			if(Zip=="楠梓區,811") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("小港區,812");
			if(Zip=="小港區,812") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("左營區,813");
			if(Zip=="左營區,813") {nForm.options[11].selected=true;}
			break;
		case "高雄縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("仁武,814");
			if(Zip=="仁武,814") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("大社,815");
			if(Zip=="大社,815") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("岡山,820");
			if(Zip=="岡山,820") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("路竹,821");
			if(Zip=="路竹,821") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("阿蓮,822");
			if(Zip=="阿蓮,822") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("田寮,823");
			if(Zip=="田寮,823") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("燕巢,824");
			if(Zip=="燕巢,824") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("橋頭,825");
			if(Zip=="橋頭,825") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("梓官,826");
			if(Zip=="梓官,826") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("彌陀,827");
			if(Zip=="彌陀,827") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("永安,828");
			if(Zip=="永安,828") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("湖內,829");
			if(Zip=="湖內,829") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("鳳山,830");
			if(Zip=="鳳山,830") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("大寮,831");
			if(Zip=="大寮,831") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("林園,832");
			if(Zip=="林園,832") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("鳥松,833");
			if(Zip=="鳥松,833") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("大樹,840");
			if(Zip=="大樹,840") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("旗山,842");
			if(Zip=="旗山,842") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("美濃,843");
			if(Zip=="美濃,843") {nForm.options[19].selected=true;}
            nForm.options[20] = new Option("六龜,844");			
            if(Zip=="六龜,844") {nForm.options[20].selected=true;}	
			nForm.options[21] = new Option("內門,845");
			if(Zip=="內門,845") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("杉林,846");
			if(Zip=="杉林,846") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("甲仙,847");
			if(Zip=="甲仙,847") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("桃源,848");
			if(Zip=="桃源,848") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("三民,849");
			if(Zip=="三民,849") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("茂林,851");
			if(Zip=="茂林,851") {nForm.options[26].selected=true;}
			nForm.options[27] = new Option("茄萣,852");
			if(Zip=="茄萣,852") {nForm.options[27].selected=true;}
			break;
		case "屏東縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("屏東,900");
			if(Zip=="屏東,900") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("三地門,901");
			if(Zip=="三地門,901") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("霧臺,902");
			if(Zip=="霧臺,902") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("瑪家,903");
			if(Zip=="瑪家,903") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("九如,904");
			if(Zip=="九如,904") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("里港,905");
			if(Zip=="里港,905") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("高樹,906");
			if(Zip=="高樹,906") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("鹽埔,907");
			if(Zip=="鹽埔,907") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("長治,908");
			if(Zip=="長治,908") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("麟洛,909");
			if(Zip=="麟洛,909") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("竹田,911");
			if(Zip=="竹田,911") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("內埔,912");
			if(Zip=="內埔,912") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("萬丹,913");
			if(Zip=="萬丹,913") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("潮州,920");
			if(Zip=="潮州,920") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("泰武,921");
			if(Zip=="泰武,921") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("來義,922");
			if(Zip=="來義,922") {nForm.options[16].selected=true;}
			nForm.options[17] = new Option("萬巒,923");
			if(Zip=="萬巒,923") {nForm.options[17].selected=true;}
			nForm.options[18] = new Option("崁頂,924");
			if(Zip=="崁頂,924") {nForm.options[18].selected=true;}
			nForm.options[19] = new Option("新埤,925");
			if(Zip=="新埤,925") {nForm.options[19].selected=true;}
            nForm.options[20] = new Option("南州,926");
            if(Zip=="南州,926") {nForm.options[20].selected=true;}
			nForm.options[21] = new Option("林邊,927");
			if(Zip=="林邊,927") {nForm.options[21].selected=true;}
			nForm.options[22] = new Option("東港,928");
			if(Zip=="東港,928") {nForm.options[22].selected=true;}
			nForm.options[23] = new Option("琉球,929");
			if(Zip=="琉球,929") {nForm.options[23].selected=true;}
			nForm.options[24] = new Option("佳冬,931");
			if(Zip=="佳冬,931") {nForm.options[24].selected=true;}
			nForm.options[25] = new Option("新園,932");
			if(Zip=="新園,932") {nForm.options[25].selected=true;}
			nForm.options[26] = new Option("枋寮,940");
			if(Zip=="枋寮,940") {nForm.options[26].selected=true;}
			nForm.options[27] = new Option("枋山,941");
			if(Zip=="枋山,941") {nForm.options[27].selected=true;}
			nForm.options[28] = new Option("春日,942");
			if(Zip=="春日,942") {nForm.options[28].selected=true;}
			nForm.options[29] = new Option("獅子,943");
			if(Zip=="獅子,943") {nForm.options[29].selected=true;}
			nForm.options[30] = new Option("車城,944");
			if(Zip=="車城,944") {nForm.options[30].selected=true;}
			nForm.options[31] = new Option("牡丹,945");
			if(Zip=="牡丹,945") {nForm.options[31].selected=true;}
			nForm.options[32] = new Option("恆春,946");
			if(Zip=="恆春,946") {nForm.options[32].selected=true;}
			nForm.options[33] = new Option("滿州,947");
			if(Zip=="滿州,947") {nForm.options[33].selected=true;}
			break;
		case "花蓮縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("花蓮,970");
			if(Zip=="花蓮,970") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("新城,971");
			if(Zip=="新城,971") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("吉安,973");
			if(Zip=="吉安,973") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("壽豐,974");
			if(Zip=="壽豐,974") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("鳳林,975");
			if(Zip=="鳳林,975") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("光復,976");
			if(Zip=="光復,976") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("豐濱,977");
			if(Zip=="豐濱,977") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("瑞穗,978");
			if(Zip=="瑞穗,978") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("萬榮,979");
			if(Zip=="萬榮,979") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("玉里,981");
			if(Zip=="玉里,981") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("卓溪,982");
			if(Zip=="卓溪,982") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("富里,983");
			if(Zip=="富里,983") {nForm.options[12].selected=true;}
            nForm.options[13] = new Option("秀林,972");
            if(Zip=="秀林,972") {nForm.options[13].selected=true;}
			break;
		case "澎湖縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("馬公,880");
			if(Zip=="馬公,880") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("西嶼,881");
			if(Zip=="西嶼,881") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("望安,882");
			if(Zip=="望安,882") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("七美,883");
			if(Zip=="七美,883") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("白沙,884");
			if(Zip=="白沙,884") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("湖西,885");
			if(Zip=="湖西,885") {nForm.options[6].selected=true;}
			break;
			
		case "金門":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("金沙,890");
			if(Zip=="金沙,890") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("金湖,891");
			if(Zip=="金湖,891") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("金寧,892");
			if(Zip=="金寧,892") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("金城,893");
			if(Zip=="金城,893") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("烈嶼,894");
			if(Zip=="烈嶼,894") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("烏坵,896");
			if(Zip=="烏坵,896") {nForm.options[6].selected=true;}
			break;	
		case "馬祖":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("南竿,209");
			if(Zip=="南竿,209") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("北竿,210");
			if(Zip=="北竿,210") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("莒光,211");
			if(Zip=="莒光,211") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("東引,212");
			if(Zip=="東引,212") {nForm.options[4].selected=true;}
			break;
		case "台東縣":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("台東,950");
			if(Zip=="台東,950") {nForm.options[1].selected=true;}
			nForm.options[2] = new Option("綠島,951");
			if(Zip=="綠島,951") {nForm.options[2].selected=true;}
			nForm.options[3] = new Option("蘭嶼,952");
			if(Zip=="蘭嶼,952") {nForm.options[3].selected=true;}
			nForm.options[4] = new Option("延平,953");
			if(Zip=="延平,953") {nForm.options[4].selected=true;}
			nForm.options[5] = new Option("卑南,954");
			if(Zip=="卑南,954") {nForm.options[5].selected=true;}
			nForm.options[6] = new Option("鹿野,955");
			if(Zip=="鹿野,955") {nForm.options[6].selected=true;}
			nForm.options[7] = new Option("關山,956");
			if(Zip=="關山,956") {nForm.options[7].selected=true;}
			nForm.options[8] = new Option("海端,957");
			if(Zip=="海端,957") {nForm.options[8].selected=true;}
			nForm.options[9] = new Option("池上,958");
			if(Zip=="池上,958") {nForm.options[9].selected=true;}
			nForm.options[10] = new Option("東河,959");
			if(Zip=="東河,959") {nForm.options[10].selected=true;}
			nForm.options[11] = new Option("成功,961");
			if(Zip=="成功,961") {nForm.options[11].selected=true;}
			nForm.options[12] = new Option("長賓,962");
			if(Zip=="長賓,962") {nForm.options[12].selected=true;}
			nForm.options[13] = new Option("太麻里,963");
			if(Zip=="太麻里,963") {nForm.options[13].selected=true;}
			nForm.options[14] = new Option("金峰,964");
			if(Zip=="金峰,964") {nForm.options[14].selected=true;}
			nForm.options[15] = new Option("大武,965");
			if(Zip=="大武,965") {nForm.options[15].selected=true;}
			nForm.options[16] = new Option("達仁,966");
			if(Zip=="達仁,966") {nForm.options[16].selected=true;}
			break;
		case "國外地區":
			nForm.options[0] = new Option("請選擇...","請選擇...");
			nForm.options[1] = new Option("亞洲","亞洲,000");
			nForm.options[2] = new Option("歐洲","歐洲,000");
			nForm.options[3] = new Option("美洲","美洲,000");
			nForm.options[4] = new Option("非洲","非洲,000");
			nForm.options[5] = new Option("澳洲","澳洲,000");
			break;
		default:
			nForm.options[0] = new Option("請選擇...","請選擇...");
			break;
	}
}

function Buildkey(num) {
	var ctr=1;
	document.form1.h07.selectedIndex=0;
	//document.form1.h26.value="";  
	document.form1.h07.options[0]=new Option("請選擇區域...","");
	/*
	定義二階選單內容
	if(num=="第一階下拉選單的值") {	document.form1.h07.options[ctr]=new Option("第二階下拉選單的顯示名稱","第二階下拉選單的值");	ctr=ctr+1;	}
	*/	
	/*臺北市*/  
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("中正區","100");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("大同區","103");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("中山區","104");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("松山區","105");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("大安區","106");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("萬華區","108");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("信義區","110");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("士林區","111");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("北投區","112");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("內湖區","114");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("南港區","115");	ctr=ctr+1;	}
	if(num=="臺北市") {	document.form1.h07.options[ctr]=new Option("文山區","116");	ctr=ctr+1;	}
	/*基隆市*/                                                                         
	if(num=="基隆市") {	document.form1.h07.options[ctr]=new Option("仁愛區","200");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h07.options[ctr]=new Option("信義區","201");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h07.options[ctr]=new Option("中正區","202");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h07.options[ctr]=new Option("中山區","203");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h07.options[ctr]=new Option("安樂區","204");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h07.options[ctr]=new Option("暖暖區","205");	ctr=ctr+1;	}
	if(num=="基隆市") {	document.form1.h07.options[ctr]=new Option("七堵區","206");	ctr=ctr+1;	}
	/*臺北縣*/                                                                         
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("萬里鄉","207");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("金山鄉","208");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("板橋市","220");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("汐止市","221");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("深坑鄉","222");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("石碇鄉","223");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("瑞芳鎮","224");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("平溪鄉","226");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("雙溪鄉","227");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("貢寮鄉","228");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("新店市","231");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("坪林鄉","232");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("烏來鄉","233");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("永和市","234");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("中和市","235");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("土城市","236");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("三峽鎮","237");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("樹林市","238");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("鶯歌鎮","239");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("三重市","241");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("新莊市","242");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("泰山鄉","243");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("林口鄉","244");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("蘆洲市","247");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("五股鄉","248");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("八里鄉","249");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("淡水鎮","251");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("三芝鄉","252");	ctr=ctr+1;	} 
	if(num=="臺北縣") {	document.form1.h07.options[ctr]=new Option("石門鄉","253");	ctr=ctr+1;	} 
	/*宜蘭縣*/                                                                         
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("宜蘭市","260");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("頭城鎮","261");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("礁溪鄉","262");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("壯圍鄉","263");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("員山鄉","264");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("羅東鎮","265");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("三星鄉","266");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("大同鄉","267");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("五結鄉","268");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("冬山鄉","269");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("蘇澳鎮","270");	ctr=ctr+1;	} 
	if(num=="宜蘭縣") {	document.form1.h07.options[ctr]=new Option("南澳鄉","272");	ctr=ctr+1;	} 
	/*新竹縣市*/                                                                       
	if(num=="新竹縣市") {	document.form1.h07.options[ctr]=new Option("新竹市","300");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h07.options[ctr]=new Option("竹北市","302");	ctr=ctr+1;	} 
	if(num=="新竹縣市") {	document.form1.h07.options[ctr]=new Option("湖口鄉","303");	ctr