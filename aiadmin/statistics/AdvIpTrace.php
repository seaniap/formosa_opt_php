<?php require_once('../../Connections/MySQL.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_WebLog = 20;
$pageNum_WebLog = 0;
if (isset($_GET['pageNum_WebLog'])) {
  $pageNum_WebLog = $_GET['pageNum_WebLog'];
}
$startRow_WebLog = $pageNum_WebLog * $maxRows_WebLog;

$colname_WebLog = "1";
if (isset($_GET['FromIP'])) {
  $colname_WebLog = (get_magic_quotes_gpc()) ? $_GET['FromIP'] : addslashes($_GET['FromIP']);
}

$query_WebLog = sprintf("SELECT WebLog.*,.Topic FROM ".$Table_ID."_WebLog left join  on .Page=WebLog.Page WHERE FromIP = '%s' ORDER BY LogDate", $colname_WebLog);
$query_limit_WebLog = sprintf("%s LIMIT %d, %d", $query_WebLog, $startRow_WebLog, $maxRows_WebLog);
$WebLog = mysqli_query($MySQL,$query_limit_WebLog) or die(mysqli_error($MySQL));
$row_WebLog = mysqli_fetch_assoc($WebLog);

if (isset($_GET['totalRows_WebLog'])) {
  $totalRows_WebLog = $_GET['totalRows_WebLog'];
} else {
  $all_WebLog = mysqli_query($MySQL,$query_WebLog);
  $totalRows_WebLog = mysqli_num_rows($all_WebLog);
}
$totalPages_WebLog = ceil($totalRows_WebLog/$maxRows_WebLog)-1;

$queryString_WebLog = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_WebLog") == false && 
        stristr($param, "totalRows_WebLog") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_WebLog = "&" . implode("&", $newParams);
  }
}
$queryString_WebLog = sprintf("&totalRows_WebLog=%d%s", $totalRows_WebLog, $queryString_WebLog);
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>無標題文件</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {font-size: 12px}
-->
</style>
</head>

<body>
<table width="71%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="83"><p align="left">來自網域 <?php echo $row_WebLog['Domain']; ?> 的來賓<span class="style1">　瀏覽記錄 <?php echo ($startRow_WebLog + 1) ?> 到 <?php echo min($startRow_WebLog + $maxRows_WebLog, $totalRows_WebLog) ?> 共 <?php echo $totalRows_WebLog ?>筆</span></p>
      <table width="482" border="0" cellpadding="2" cellspacing="1" class="general">
      <tr>
        <td width="143" class="headtext">時間</td>
        <td width="116" background="拖曳到一個檔案以選取它。" class="headtext">瀏覽網頁</td>
        <td width="103" class="headtext">網頁代碼</td>
      </tr>
      <?php do { ?>
      <tr class="FirstRow">
        <td><?php echo $row_WebLog['LogDate']; ?></td>
        <td background="拖曳到一個檔案以選取它。"><font color="#0000FF"><?php echo $row_WebLog['Topic']; ?></font></td>
        <td><?php echo $row_WebLog['Page']; ?></td>
      </tr>
      <?php } while ($row_WebLog = mysqli_fetch_assoc($WebLog)); ?>
    </table>
      <BR>
      <table width="482" border="0" align="center" bgcolor="#CCCCCC">
        <tr>
          <td width="23%" align="center">
            <?php if ($pageNum_WebLog > 0) { // Show if not first page ?>
            <a href="<?php printf("%s?pageNum_WebLog=%d%s", $currentPage, 0, $queryString_WebLog); ?>"><img src="First.gif" border=0>第一頁</a>
            <?php } // Show if not first page ?>
          </td>
          <td width="31%" align="center">
            <?php if ($pageNum_WebLog > 0) { // Show if not first page ?>
            <a href="<?php printf("%s?pageNum_WebLog=%d%s", $currentPage, max(0, $pageNum_WebLog - 1), $queryString_WebLog); ?>"><img src="Previous.gif" border=0>上一頁</a>
            <?php } // Show if not first page ?>
          </td>
          <td width="23%" align="center">
            <?php if ($pageNum_WebLog < $totalPages_WebLog) { // Show if not last page ?>
            <a href="<?php printf("%s?pageNum_WebLog=%d%s", $currentPage, min($totalPages_WebLog, $pageNum_WebLog + 1), $queryString_WebLog); ?>">下一頁<img src="Next.gif" border=0></a>
            <?php } // Show if not last page ?>
          </td>
          <td width="23%" align="center">
            <?php if ($pageNum_WebLog < $totalPages_WebLog) { // Show if not last page ?>
            <a href="<?php printf("%s?pageNum_WebLog=%d%s", $currentPage, $totalPages_WebLog, $queryString_WebLog); ?>">最終頁<img src="Last.gif" border=0></a>
            <?php } // Show if not last page ?>
          </td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
<?php
mysqli_free_result($WebLog);
?>

