<?php

$query_MonthTotal = "SELECT " . $Table_ID . "_SiteMap.Topic," . $Table_ID . "_WebLog.Page as Page,year( LogDate ) AS Year, month( LogDate ) AS  Month , sum( click ) AS total FROM `" . $Table_ID . "_WebLog`,  " . $Table_ID . "_SiteMap ";
$query_MonthTotal = $query_MonthTotal . "where  " . $Table_ID . "_WebLog.Page=" . $Table_ID . "_SiteMap.Page GROUP BY page ORDER BY total DESC  ";
//echo $query_MonthTotal;
$MonthTotal = mysqli_query($MySQL, $query_MonthTotal) or die(mysqli_error($MySQL));
$row_MonthTotal = mysqli_fetch_assoc($MonthTotal);
$totalRows_MonthTotal = mysqli_num_rows($MonthTotal);
?>

<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">歷史記錄</h3>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="main.php?Page=1">系統管理</a></li>
          <li class="breadcrumb-item"><a href="main.php?Page=6-1">歷史記錄</a></li>
          <li class="breadcrumb-item active" aria-current="page">各版面點擊數</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col table-responsive mt-5">
      <h5>網頁點擊統計：各版面點擊數</h5>
      <table class="table" summary="網頁點擊統計依版面">
        <thead>
          <tr>
            <th nowrap>網頁</th>
            <th nowrap>細節</th>
            <th nowrap>點擊數</th>
            <th width="50%">&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php do { ?>
            <tr>
              <td>
                <a href="<?php echo $RootLevel; ?>/aiadmin/main.php?Page=6-2-1&Topic=<? echo rawurlencode($row_MonthTotal['Topic']); ?>">
                  <?php echo $row_MonthTotal['Topic']; ?></a>
              </td>
              <td>
                <?php if (substr($row_MonthTotal['Page'], 0, 1) == 'S') {
                  echo "<a href=" . $RootLevel . "/aiadmin/main.php?Page=6-2-2&Topic=" . $row_MonthTotal['Topic'] . "&Page=" . $row_MonthTotal['Page'] . ">細節" . "</a>";
                } ?>&nbsp;
              </td>
              <td><?php echo $row_MonthTotal['total']; ?></td>
              <td>
                <div align="left">
                  <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?php echo $row_MonthTotal['total'] / 10; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $row_MonthTotal['total'] / 10; ?>%"><?php echo $row_MonthTotal['total']; ?></div>
                  </div>
                  <!-- <img src="statistics/bar_g_h.gif" width="<?php //echo $row_MonthTotal['total'] / 20; 
                                                                ?>" height="10"> -->
                </div>
              </td>
            </tr>
          <?php } while ($row_MonthTotal = mysqli_fetch_assoc($MonthTotal)); ?>
        </tbody>
      </table>
    </div>
  </div>
  <!-- end -->
  <?php
  mysqli_free_result($MonthTotal);
  ?>