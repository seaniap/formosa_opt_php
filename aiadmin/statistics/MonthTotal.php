<?php

$query_MonthTotal = "SELECT year( LogDate ) AS Year, month( LogDate ) AS  Month , sum( click ) AS total FROM `" . $Table_ID . "_WebLog` GROUP BY Month,Year ORDER BY Year DESC ,  Month DESC  ";
$MonthTotal = mysqli_query($MySQL, $query_MonthTotal) or die(mysqli_error($MySQL));
$row_MonthTotal = mysqli_fetch_assoc($MonthTotal);
$totalRows_MonthTotal = mysqli_num_rows($MonthTotal);
?>
<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">歷史記錄</h3>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="main.php?Page=1">系統管理</a></li>
          <li class="breadcrumb-item active" aria-current="page">歷史記錄</li>
        </ol>
      </nav>
    </div>
  </div>


  <div class="row">
    <div class="col table-responsive mt-5">
      <h5>網頁點擊統計：依月份</h5>
      <table class="table" summary="網頁點擊統計依月份">
        <thead>
          <tr>
            <th nowrap>月份</th>
            <th nowrap>點擊數</th>
            <th width="70%">&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php do { ?>
            <tr>
              <td>
                <a href="<?php echo $RootLevel; ?>/aiadmin/main.php?Page=6-1-1&Year=<?php echo $row_MonthTotal['Year'] . "&Month=" . $row_MonthTotal['Month']; ?>">
                  <?php echo $row_MonthTotal['Year'] . "年" . $row_MonthTotal['Month'] . "月"; ?></a></td>
              <td><?php echo $row_MonthTotal['total']; ?></td>
              <td>
                <div align="left">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?php echo $row_MonthTotal['total'] / 100; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $row_MonthTotal['total'] / 50; ?>%"><?php echo $row_MonthTotal['total']; ?></div>
                  </div>
                  <!-- <img src="statistics/bar_g_h.gif" width="<?php //echo $row_MonthTotal['total'] / 100; ?>" height="10"> -->
                </div>
              </td>
            </tr>
          <?php } while ($row_MonthTotal = mysqli_fetch_assoc($MonthTotal)); ?>
        </tbody>
      </table>

    </div>
  </div>

</div>
<!-- end -->
<?php
mysqli_free_result($MonthTotal);
?>