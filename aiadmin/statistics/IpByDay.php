<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_WebLog = 10;
$pageNum_WebLog = 0;
if (isset($_GET['pageNum_WebLog'])) {
  $pageNum_WebLog = $_GET['pageNum_WebLog'];
}
$startRow_WebLog = $pageNum_WebLog * $maxRows_WebLog;


$query_WebLog = "SELECT DATE_FORMAT(LogDate,'%y-%m-%e') as DD, " . $Table_ID . "_WebLog.Page, " . $Table_ID . "_WebLog.FromIP," . $Table_ID . "_WebLog.`Domain`,count(1) FROM " . $Table_ID . "_WebLog GROUP BY DATE_FORMAT(LogDate,'%y-%m-%e'),Domain ORDER BY " . $Table_ID . "_WebLog.LogDate Desc";
$query_limit_WebLog = sprintf("%s LIMIT %d, %d", $query_WebLog, $startRow_WebLog, $maxRows_WebLog);
$WebLog = mysqli_query($MySQL, $query_limit_WebLog) or die(mysqli_error($MySQL));
$row_WebLog = mysqli_fetch_assoc($WebLog);

if (isset($_GET['totalRows_WebLog'])) {
  $totalRows_WebLog = $_GET['totalRows_WebLog'];
} else {
  $all_WebLog = mysqli_query($MySQL, $query_WebLog);
  $totalRows_WebLog = mysqli_num_rows($all_WebLog);
}
$totalPages_WebLog = ceil($totalRows_WebLog / $maxRows_WebLog) - 1;

$queryString_WebLog = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (
      stristr($param, "pageNum_WebLog") == false &&
      stristr($param, "totalRows_WebLog") == false
    ) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_WebLog = "&" . implode("&", $newParams);
  }
}
$queryString_WebLog = sprintf("&totalRows_WebLog=%d%s", $totalRows_WebLog, $queryString_WebLog);
?>
<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">歷史記錄</h3>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="main.php?Page=1">系統管理</a></li>
          <li class="breadcrumb-item"><a href="main.php?Page=6-1">歷史記錄</a></li>
          <li class="breadcrumb-item active" aria-current="page">IP點擊數</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <div class="RecordData">記錄 <?php echo ($startRow_WebLog + 1) ?> 到 <?php echo min($startRow_WebLog + $maxRows_WebLog, $totalRows_WebLog) ?> 共 <?php echo $totalRows_WebLog ?></div>
    </div>
  </div>

  <div class="row">
    <div class="col table-responsive mt-5">
    <h5>網頁點擊統計：IP點擊數</h5>
      <table class="table">
        <thead>
          <tr>
            <th nowrap>日期</th>
            <th nowrap>IP位置</th>
            <th nowrap>IP來自</th>
            <th width="50%">點擊數</th>
          </tr>
        </thead>
        <tbody>
          <?php do { ?>
            <tr>
              <td><?php echo $row_WebLog['DD']; ?></td>
              <td><?php echo $row_WebLog['FromIP']; ?></td>
              <td>
                <div align="left"><a href="main.php?Page=6-3-1&FromIP=<?php echo $row_WebLog['FromIP']; ?>"><?php echo $row_WebLog['Domain']; ?></a></div>
              </td>
              <td>
                <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?php echo $row_WebLog['count(1)'] / 5; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $row_WebLog['count(1)'] / 5; ?>%"><?php echo $row_WebLog['count(1)']; ?></div>
                </div>
                <?php //echo $row_WebLog['count(1)']; 
                ?>
              </td>
            </tr>
          <?php } while ($row_WebLog = mysqli_fetch_assoc($WebLog)); ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col d-flex justify-content-center">
      <!-- paginations -->
      <nav aria-label="Page navigation example">
        <ul class="pagination">
          <?php if ($pageNum_WebLog > 0) { // Show if not first page 
          ?>
            <li class="page-item">
              <a class="page-link" aria-label="Previous" href="<?php printf("%s?pageNum_WebLog=%d%s", $currentPage, 0, $queryString_WebLog); ?>">
                第一頁</a>
            </li>
          <?php } // Show if not first page 
          ?>


          <?php if ($pageNum_WebLog > 0) { // Show if not first page 
          ?>
            <li class="page-item">
              <a class="page-link" href="<?php printf("%s?pageNum_WebLog=%d%s", $currentPage, max(0, $pageNum_WebLog - 1), $queryString_WebLog); ?>">上一頁</a>
            </li>
          <?php } // Show if not first page 
          ?>

          <?php if ($pageNum_WebLog < $totalPages_WebLog) { // Show if not last page 
          ?>
            <li class="page-item">
              <a class="page-link" href="<?php printf("%s?pageNum_WebLog=%d%s", $currentPage, min($totalPages_WebLog, $pageNum_WebLog + 1), $queryString_WebLog); ?>">下一頁</a>
            </li>
          <?php } // Show if not last page 
          ?>
          <?php if ($pageNum_WebLog < $totalPages_WebLog) { // Show if not last page 
          ?>
            <li class="page-item">
              <a class="page-link" href="<?php printf("%s?pageNum_WebLog=%d%s", $currentPage, $totalPages_WebLog, $queryString_WebLog); ?>">最後一頁</a>
            </li>
          <?php } // Show if not last page 
          ?>
        </ul>
      </nav>
      <!--end paginations -->
    </div>
  </div>
</div>
<!-- end -->
<?php
mysqli_free_result($WebLog);
?>