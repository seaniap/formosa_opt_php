<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if($_POST['Security'][0]<>"")
{$Security=implode("-",$_POST['Security']);}
else
{$Security="";}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE ".$Table_ID."_SiteMap SET Page=%s, Sq=%s, Template=%s, Extend=%s, MaxRow=%s, TP1=%s, TP2=%s, TP3=%s, TP4=%s, TP5=%s, TP6=%s, TP7=%s, TP8=%s, TP9=%s, Topic=%s, Topic_bg=%s, Topic_bgColor=%s, MainIcon=%s, Title=%s, Display=%s, A_FImg=%s, A_BImg=%s, B_FImg=%s, B_BImg=%s, C_FImg=%s, C_BImg=%s, D_FImg=%s, D_BImg=%s, E_FImg=%s, E_BImg=%s, U1=%s, U2=%s, U3=%s, U4=%s, Security=%s WHERE KeyID=%s",
                       GetSQLValueString($_POST['Page'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['Extend'], "text"),
					   GetSQLValueString($_POST['MaxRow'], "text"),
                       GetSQLValueString($_POST['TP1'], "text"),
                       GetSQLValueString($_POST['TP2'], "text"),
                       GetSQLValueString($_POST['TP3'], "text"),
                       GetSQLValueString($_POST['TP4'], "text"),
                       GetSQLValueString($_POST['TP5'], "text"),
                       GetSQLValueString($_POST['TP6'], "text"),
                       GetSQLValueString($_POST['TP7'], "text"),
                       GetSQLValueString($_POST['TP8'], "text"),
                       GetSQLValueString($_POST['TP9'], "text"),
                       GetSQLValueString($_POST['Topic'], "text"),
                       GetSQLValueString($_POST['Topic_bg'], "text"),
                       GetSQLValueString($_POST['Topic_bgColor'], "text"),
                       GetSQLValueString($_POST['MainIcon'], "text"),
                       GetSQLValueString($_POST['Title'], "text"),
	   			       GetSQLValueString($_POST['Display'], "text"),
                       GetSQLValueString($_POST['A_FImg'], "text"),
                       GetSQLValueString($_POST['A_BImg'], "text"),
                       GetSQLValueString($_POST['B_FImg'], "text"),
                       GetSQLValueString($_POST['B_BImg'], "text"),
                       GetSQLValueString($_POST['C_FImg'], "text"),
                       GetSQLValueString($_POST['C_BImg'], "text"),
                       GetSQLValueString($_POST['D_FImg'], "text"),
                       GetSQLValueString($_POST['D_BImg'], "text"),
                       GetSQLValueString($_POST['E_FImg'], "text"),
                       GetSQLValueString($_POST['E_BImg'], "text"),
                       GetSQLValueString($_POST['U1'], "text"),
                       GetSQLValueString($_POST['U2'], "text"),
                       GetSQLValueString($_POST['U3'], "text"),
                       GetSQLValueString($_POST['U4'], "text"),
					   GetSQLValueString($Security, "text"),
                       GetSQLValueString($_POST['KeyID'], "text"));
  
  //echo $updateSQL;
    $insertSQL = sprintf("INSERT INTO ".$Table_ID."_SiteMap (KeyID, Page, Sq, Template, Extend, MaxRow, TP1, TP2, TP3, TP4, TP5, TP6, TP7, TP8, TP9, Topic, Topic_bg, Topic_bgColor, MainIcon, Title, Display, A_FImg, A_BImg, B_FImg, B_BImg, C_FImg, C_BImg, D_FImg, D_BImg, E_FImg, E_BImg, U1, U2, U3, U4, Security) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['KeyID'], "text"),
                       GetSQLValueString($_POST['Page'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['Extend'], "text"),
                       GetSQLValueString($_POST['MaxRow'], "int"),
                       GetSQLValueString($_POST['TP1'], "text"),
                       GetSQLValueString($_POST['TP2'], "text"),
                       GetSQLValueString($_POST['TP3'], "text"),
                       GetSQLValueString($_POST['TP4'], "text"),
                       GetSQLValueString($_POST['TP5'], "text"),
                       GetSQLValueString($_POST['TP6'], "text"),
                       GetSQLValueString($_POST['TP7'], "text"),
                       GetSQLValueString($_POST['TP8'], "text"),
                       GetSQLValueString($_POST['TP9'], "text"),
                       GetSQLValueString($_POST['Topic'], "text"),
                       GetSQLValueString($_POST['Topic_bg'], "text"),
                       GetSQLValueString($_POST['Topic_bgColor'], "text"),
                       GetSQLValueString($_POST['MainIcon'], "text"),
                       GetSQLValueString($_POST['Title'], "text"),
                       GetSQLValueString($_POST['Display'], "text"),
                       GetSQLValueString($_POST['A_FImg'], "text"),
                       GetSQLValueString($_POST['A_BImg'], "text"),
                       GetSQLValueString($_POST['B_FImg'], "text"),
                       GetSQLValueString($_POST['B_BImg'], "text"),
                       GetSQLValueString($_POST['C_FImg'], "text"),
                       GetSQLValueString($_POST['C_BImg'], "text"),
                       GetSQLValueString($_POST['D_FImg'], "text"),
                       GetSQLValueString($_POST['D_BImg'], "text"),
                       GetSQLValueString($_POST['E_FImg'], "text"),
                       GetSQLValueString($_POST['E_BImg'], "text"),
                       GetSQLValueString($_POST['U1'], "text"),
                       GetSQLValueString($_POST['U2'], "text"),
                       GetSQLValueString($_POST['U3'], "text"),
                       GetSQLValueString($_POST['U4'], "text"),
                       GetSQLValueString($Security, "text"));
  
  
   //echo $insertSQL;
   
   
  if ( $_POST[Action]=="edit")
  {$Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));
  //echo $updateSQL;
  }
  else
  {$Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
  //echo $insertSQL;
  }

  $updateGoTo = "main.php";
  
 $queryString_sitemap = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "Page")== false && stristr($param, "Action") == false )  {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_sitemap = "?".implode("&", $newParams);
  }
  }
   //echo $queryString_sitemap;
   if ($queryString_sitemap<>"") {
    $updateGoTo .=  $queryString_sitemap."&Page=Z-1";}
	else {
	 $updateGoTo .=  $queryString_sitemap."?Page=Z-1";
	}

  //echo $updateGoTo;
  header("Location:".$updateGoTo);
}

$colname_SiteMap = "-1";
if (isset($_GET['KeyPage'])) {
  $colname_SiteMap = (get_magic_quotes_gpc()) ? $_GET['KeyPage'] : addslashes($_GET['KeyPage']);
}

$query_SiteMap = sprintf("SELECT * FROM ".$Table_ID."_SiteMap WHERE Page = %s", GetSQLValueString($colname_SiteMap, "text"));
$SiteMap = mysqli_query($MySQL,$query_SiteMap) or die(mysqli_error($MySQL));
$row_SiteMap = mysqli_fetch_assoc($SiteMap);
$totalRows_SiteMap = mysqli_num_rows($SiteMap);
//echo $query_SiteMap;


$query_UserLogin = "SELECT * FROM ".$Table_ID."_UserLogin WHERE UserName<>'system'  ORDER BY UserName ASC";
$UserLogin = mysqli_query($MySQL,$query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);


//新增或編輯
if($_GET['Action']=="ins")
{$KeyID=uniqid(mt_rand());}
else
{ $KeyID=$row_SiteMap['KeyID'];}
?>
<div class="container">
  <div class="row">
    <div class="col">
    <h3 class="mt-3 caption">編輯網站地圖</h3>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="main.php?Page=Z">網站管理</a></li>
        <li class="breadcrumb-item"><a href="main.php?Page=Z-1">網站地圖</a></li>
        <li class="breadcrumb-item active" aria-current="page">編輯網站地圖</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <!-- form start -->
    <form method="post" name="form1" action="SiteMapEditAdv.php?<?php echo $_SERVER['QUERY_STRING']; ?>">
    <div class="form-group d-flex justify-content-center">
      <input name="sbutton" type="submit" class="btn btn-primary" value="<?php if($_GET['Action']=="ins") {echo "插入記錄";}else{echo "更新";} ?>">
    </div>

    <div class="form-group row">
      <label for="Page" class="col-sm-2 col-form-label">Page</label>
      <div class="col-sm-10">
        <?php if($_GET['Action']=="ins") { ?>
        <input type="text" name="Page" value="" class="form-control">
        <?php } else {  ?>
          <input type="text" name="Page" value="<?php echo $row_SiteMap['Page'];  ?>" class="form-control" readonly> 
          <input type="hidden" name="Page" value="<?php echo $row_SiteMap['Page']; ?>">
        <?php } ?>
      </div>
    </div>

    <div class="form-group row">
      <label for="Sq" class="col-sm-2 col-form-label">順序</label>
      <div class="col-sm-10">
      <input type="text" name="Sq" value="<?php echo $row_SiteMap['Sq']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="Template" class="col-sm-2 col-form-label">版型</label>
      <div class="col-sm-10">
      <input type="text" name="Template" value="<?php echo $row_SiteMap['Template']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="Topic" class="col-sm-2 col-form-label">內容主旨</label>
      <div class="col-sm-10">
      <input type="text" name="Topic" value="<?php echo $row_SiteMap['Topic']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="Title" class="col-sm-2 col-form-label">網頁標題</label>
      <div class="col-sm-10">
      <input type="text" name="Title" value="<?php echo $row_SiteMap['Title']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="Display" class="col-sm-2 col-form-label">顯示</label>
      <div class="col-sm-5">目前設定: <?php echo $row_SiteMap['Display']; ?></div>
      <div class="col-sm-5">
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" name="Display" id="Display1" class="custom-control-input" checked="<?php if($row_SiteMap['Display']=="Y"){ echo "true";} ?>" value="Y" <?php if($row_SiteMap['Display']=="Y"){ echo "checked";} ?>>
        <label class="custom-control-label" for="Display1">顯示</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" name="Display" id="Display2" class="custom-control-input" value="N" <?php if($row_SiteMap['Display']=="N"){ echo "checked";} ?>>
        <label class="custom-control-label" for="Display2">不顯示</label>
      </div>
      </div>
    </div>

    <div class="form-group row">
      <label for="TP1" class="col-sm-2 col-form-label">TP1</label>
      <div class="col-sm-10">
      <input type="text" name="TP1" value="<?php echo $row_SiteMap['TP1']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="TP2" class="col-sm-2 col-form-label">TP2</label>
      <div class="col-sm-10">
      <input type="text" name="TP2" value="<?php echo $row_SiteMap['TP2']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="TP3" class="col-sm-2 col-form-label">TP3</label>
      <div class="col-sm-10">
      <input type="text" name="TP3" value="<?php echo $row_SiteMap['TP3']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="TP4" class="col-sm-2 col-form-label">TP4</label>
      <div class="col-sm-10">
      <input type="text" name="TP4" value="<?php echo $row_SiteMap['TP4']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="TP5" class="col-sm-2 col-form-label">TP5</label>
      <div class="col-sm-10">
      <input type="text" name="TP5" value="<?php echo $row_SiteMap['TP5']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="TP6" class="col-sm-2 col-form-label">TP6</label>
      <div class="col-sm-10">
      <input type="text" name="TP6" value="<?php echo $row_SiteMap['TP6']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="TP7" class="col-sm-2 col-form-label">TP7</label>
      <div class="col-sm-10">
      <input type="text" name="TP7" value="<?php echo $row_SiteMap['TP7']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="TP8" class="col-sm-2 col-form-label">TP8</label>
      <div class="col-sm-10">
      <input type="text" name="TP8" value="<?php echo $row_SiteMap['TP8']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="TP9" class="col-sm-2 col-form-label">轉址<br>
      <small class="form-text text-muted">(直接填頁碼 ex:1-1)</small></label>
      <div class="col-sm-10">
      <input type="text" name="TP9" value="<?php echo $row_SiteMap['TP9']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="A_FImg" class="col-sm-2 col-form-label">A_FImg</label>
      <div class="col-sm-10">
      <input type="text" name="A_FImg" value="<?php echo $row_SiteMap['A_FImg']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="A_BImg" class="col-sm-2 col-form-label">A_BImg</label>
      <div class="col-sm-10">
      <input type="text" name="A_BImg" value="<?php echo $row_SiteMap['A_BImg']; ?>" class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="B_FImg" class="col-sm-2 col-form-label">B_FImg</label>
      <div class="col-sm-10">
      <input type="text" name="B_FImg" value="<?php echo $row_SiteMap['B_FImg']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="B_BImg" class="col-sm-2 col-form-label">B_BImg</label>
      <div class="col-sm-10">
      <input type="text" name="B_BImg" value="<?php echo $row_SiteMap['B_BImg']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="C_FImg" class="col-sm-2 col-form-label">C_FImg</label>
      <div class="col-sm-10">
      <input type="text" name="C_FImg" value="<?php echo $row_SiteMap['C_FImg']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="C_BImg" class="col-sm-2 col-form-label">C_BImg</label>
      <div class="col-sm-10">
      <input type="text" name="C_BImg" value="<?php echo $row_SiteMap['C_BImg']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="D_FImg" class="col-sm-2 col-form-label">D_FImg</label>
      <div class="col-sm-10">
      <input type="text" name="D_FImg" value="<?php echo $row_SiteMap['D_FImg']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="D_BImg" class="col-sm-2 col-form-label">D_BImg</label>
      <div class="col-sm-10">
      <input type="text" name="D_BImg" value="<?php echo $row_SiteMap['D_BImg']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="E_FImg" class="col-sm-2 col-form-label">E_FImg</label>
      <div class="col-sm-10">
      <input type="text" name="E_FImg" value="<?php echo $row_SiteMap['E_FImg']; ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row">
      <label for="E_BImg" class="col-sm-2 col-form-label">E_BImg</label>
      <div class="col-sm-10">
      <input type="text" name="E_BImg" value="<?php echo $row_SiteMap['E_BImg']; ?>" class="form-control">
      </div>
    </div>


    <div class="form-group row">
      <label for="Display" class="col-sm-2 col-form-label">延伸版型</label>
      <!-- <div class="col-sm-12">目前設定: <?php // echo $row_SiteMap['Extend']; ?></div> -->
        <div class="col-lg-5 mb-3 mb-lg-0">
          <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="Extend" id="Extend1" class="custom-control-input" checked="<?php if($row_SiteMap['Extend']=="N"){ echo "true";} ?>" value="N" <?php if($row_SiteMap['Display']=="N"){ echo "checked";} ?>>
          <label class="custom-control-label" for="Extend1">無延伸</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" name="Extend" id="Extend2" class="custom-control-input" value="Y" <?php if($row_SiteMap['Extend']=="Y"){ echo "checked";} ?>>
          <label class="custom-control-label" for="Extend2">有延伸</label>
        </div>    
      </div>
      <div class="col-lg-5">
        <div class="row">
          <div class="col input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">每頁最多顯示</span>
            </div>
            <input type="text" name="MaxRow" value="<?php echo $row_SiteMap['MaxRow']; ?>" class="form-control">
            <div class="input-group-append">
              <span class="input-group-text">筆</span>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="form-group row">      
      <label for="auth" class="col-sm-2 col-form-label">編輯權限<br>
      <small class="form-text text-muted">(不選代表不設限)</small></label>
      <div class="col-sm-10">
        <?php do { ?>
        <div class="custom-control custom-checkbox">
          <input type="checkbox" name="Security[]" class="custom-control-input" id="Security1" value="<? echo $row_UserLogin['UserName']; ?>" checked="true" >
          <label class="custom-control-label" for="Security1"><?php echo $row_UserLogin['UserName']; ?></label>
        </div>
        <?php } while ($row_UserLogin = mysqli_fetch_assoc($UserLogin)); ?>	 
      </div>    
    </div>
    
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="KeyID" value="<?php echo $KeyID; ?>"> 
  <input type="hidden" name="Action" value="<?php echo $_GET['Action']; ?>">
 <!-- <input type="hidden" name="Template" value="<?php //echo $row_SiteMap['Template']; ?>"> -->
    </form>
    <!-- form end -->
    </div>
  </div>

</div>
<?php
mysqli_free_result($SiteMap);
mysqli_free_result($UserLogin);
?>