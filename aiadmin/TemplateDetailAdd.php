<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO ".$_Table_ID."_TemplateDetail (KeyID, Template, FieldID, FieldTitle, Sq, Type, `Value`, MappingValue) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['KeyID'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['FieldID'], "text"),
                       GetSQLValueString($_POST['FieldTitle'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Type'], "text"),
                       GetSQLValueString($_POST['Value'], "text"),
                       GetSQLValueString($_POST['MappingValue'], "text"));

  
  $Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));

  $insertGoTo = "win_close_re.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE ".$Table_ID."_TemplateDetail SET Template=%s, FieldID=%s, FieldTitle=%s, Sq=%s, Type=%s, `Value`=%s, MappingValue=%s WHERE KeyID=%s",
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['FieldID'], "text"),
                       GetSQLValueString($_POST['FieldTitle'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Type'], "text"),
                       GetSQLValueString($_POST['Value'], "text"),
                       GetSQLValueString($_POST['MappingValue'], "text"),
                       GetSQLValueString($_POST['KeyID'], "text"));
					   
					   
 $insertSQL = sprintf("INSERT INTO ".$Table_ID."_TemplateDetail (KeyID, Template, FieldID, FieldTitle, Sq, Type, `Value`, MappingValue) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['KeyID'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['FieldID'], "text"),
                       GetSQLValueString($_POST['FieldTitle'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Type'], "text"),
                       GetSQLValueString($_POST['Value'], "text"),
                       GetSQLValueString($_POST['MappingValue'], "text"));					   

  
  if ( $_POST[Action]=="edit")
  {$Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));
  //echo $updateSQL;
  }
  else
  {$Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
  //echo $insertSQL;
  }
  
 

  $updateGoTo = "win_close_re.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_TemplateDetail = "-1";
if (isset($_GET['KeyID'])) {
  $colname_TemplateDetail = (get_magic_quotes_gpc()) ? $_GET['KeyID'] : addslashes($_GET['KeyID']);
}

$query_TemplateDetail = sprintf("SELECT * FROM ".$Table_ID."_TemplateDetail WHERE KeyID = %s", GetSQLValueString($colname_TemplateDetail, "text"));
$TemplateDetail = mysqli_query($MySQL,$query_TemplateDetail) or die(mysqli_error($MySQL));
$row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail);
$totalRows_TemplateDetail = mysqli_num_rows($TemplateDetail);


if(  $_GET[Action]=="ins") //新增資料
{
 $row_TemplateDetail['KeyID']=uniqid(mt_rand());
 $row_TemplateDetail['Template']= $_GET['Template'];
 $row_TemplateDetail['Sq']=$row_TemplateDetail['Sq']+100;
 
}

?>
<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">版型管理</h3>
    </div>
  </div>
  
  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">系統管理</a></li>
          <li class="breadcrumb-item"><a href="main.php?Page=z-4">版型管理</a></li>
          <li class="breadcrumb-item"><a href="main.php?Page=z-4-2&Template=<?php echo $_GET['Template']; ?>">版型欄位內容</a></li>
          <li class="breadcrumb-item active" aria-current="page">插入新欄位</li>
        </ol>
      </nav>
    </div>
  </div>

  <!-- start form -->
  <form method="post" name="form1" action="TemplateDetailAdd.php">
    <div class="row">
      <div class="col">
      <h5 class="my-3">版型: <?php echo $row_TemplateDetail['Template']; ?></h5>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="form-group d-flex justify-content-center">
          <input type="submit" class="btn btn-primary" value="更新">
        </div>
      </div>
    </div>
    <!-- 欄位代碼 -->
    <div class="row">
      <div class="col">
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">欄位代碼</label>
            <div class="col-sm-10">
            <select class="custom-select" name="FieldID">
              <option  value="Sq" <?php if($row_TemplateDetail['FieldID']=="Sq") {echo "selected";} ?> size="32"> Sq </option>
              <option  value="ProductID" <?php if($row_TemplateDetail['FieldID']=="ProductID") {echo "selected";} ?> size="32"> ProductID </option>
              <option  value="Pname" <?php if($row_TemplateDetail['FieldID']=="Pname") {echo "selected";} ?> size="32"> Pname </option>	  
              <option  value="Cate01" <?php if($row_TemplateDetail['FieldID']=="Cate01") {echo "selected";} ?> size="32"> Cate01 </option>
              <option  value="Cate02" <?php if($row_TemplateDetail['FieldID']=="Cate02") {echo "selected";} ?> size="32"> Cate02 </option>
              <option  value="Cate03" <?php if($row_TemplateDetail['FieldID']=="Cate03") {echo "selected";} ?> size="32"> Cate03 </option>
              <option  value="Price01" <?php if($row_TemplateDetail['FieldID']=="Price01") {echo "selected";} ?> size="32"> Price01 </option>
              <option  value="h01" <?php if($row_TemplateDetail['FieldID']=="h01") {echo "selected";} ?> size="32"> h01 </option>
              <option  value="h02" <?php if($row_TemplateDetail['FieldID']=="h02") {echo "selected";} ?> size="32"> h02 </option>
              <option  value="h03" <?php if($row_TemplateDetail['FieldID']=="h03") {echo "selected";} ?> size="32"> h03 </option>
              <option  value="h04" <?php if($row_TemplateDetail['FieldID']=="h04") {echo "selected";} ?> size="32"> h04 </option>
              <option  value="h05" <?php if($row_TemplateDetail['FieldID']=="h05") {echo "selected";} ?> size="32"> h05 </option>
              <option  value="h06" <?php if($row_TemplateDetail['FieldID']=="h06") {echo "selected";} ?> size="32"> h06 </option>
              <option  value="h07" <?php if($row_TemplateDetail['FieldID']=="h07") {echo "selected";} ?> size="32"> h07 </option>
              <option  value="h08" <?php if($row_TemplateDetail['FieldID']=="h08") {echo "selected";} ?> size="32"> h08 </option>
              <option  value="h09" <?php if($row_TemplateDetail['FieldID']=="h09") {echo "selected";} ?> size="32"> h09 </option>
              <option  value="h10" <?php if($row_TemplateDetail['FieldID']=="h10") {echo "selected";} ?> size="32"> h10 </option>
              <option  value="h11" <?php if($row_TemplateDetail['FieldID']=="h11") {echo "selected";} ?> size="32"> h11 </option>
              <option  value="h12" <?php if($row_TemplateDetail['FieldID']=="h12") {echo "selected";} ?> size="32"> h12 </option>
              <option  value="h13" <?php if($row_TemplateDetail['FieldID']=="h13") {echo "selected";} ?> size="32"> h13 </option>
              <option  value="h14" <?php if($row_TemplateDetail['FieldID']=="h14") {echo "selected";} ?> size="32"> h14 </option>
              <option  value="h15" <?php if($row_TemplateDetail['FieldID']=="h15") {echo "selected";} ?> size="32"> h15 </option>
              <option  value="h16" <?php if($row_TemplateDetail['FieldID']=="h16") {echo "selected";} ?> size="32"> h16 </option>
              <option  value="h17" <?php if($row_TemplateDetail['FieldID']=="h17") {echo "selected";} ?> size="32"> h17 </option>
              <option  value="h18" <?php if($row_TemplateDetail['FieldID']=="h18") {echo "selected";} ?> size="32"> h18 </option>
              <option  value="h19" <?php if($row_TemplateDetail['FieldID']=="h19") {echo "selected";} ?> size="32"> h19 </option>
              <option  value="h20" <?php if($row_TemplateDetail['FieldID']=="h20") {echo "selected";} ?> size="32"> h20 </option>
              <option  value="c01" <?php if($row_TemplateDetail['FieldID']=="c01") {echo "selected";} ?> size="32"> c01 </option>
              <option  value="c02" <?php if($row_TemplateDetail['FieldID']=="c02") {echo "selected";} ?> size="32"> c02 </option>
              <option  value="c03" <?php if($row_TemplateDetail['FieldID']=="c03") {echo "selected";} ?> size="32"> c03 </option>
              <option  value="c04" <?php if($row_TemplateDetail['FieldID']=="c04") {echo "selected";} ?> size="32"> c04 </option>
              <option  value="c05" <?php if($row_TemplateDetail['FieldID']=="c05") {echo "selected";} ?> size="32"> c05 </option>
              <option  value="c06" <?php if($row_TemplateDetail['FieldID']=="c06") {echo "selected";} ?> size="32"> c06 </option>
              <option  value="c07" <?php if($row_TemplateDetail['FieldID']=="c07") {echo "selected";} ?> size="32"> c07 </option>
              <option  value="c08" <?php if($row_TemplateDetail['FieldID']=="c08") {echo "selected";} ?> size="32"> c08 </option>
              <option  value="c09" <?php if($row_TemplateDetail['FieldID']=="c09") {echo "selected";} ?> size="32"> c09 </option>
              <option  value="c10" <?php if($row_TemplateDetail['FieldID']=="c10") {echo "selected";} ?> size="32"> c10 </option>

              <option  value="d01" <?php if($row_TemplateDetail['FieldID']=="d01") {echo "selected";} ?> size="32"> d01 </option>
              <option  value="d02" <?php if($row_TemplateDetail['FieldID']=="d02") {echo "selected";} ?> size="32"> d02 </option>
              <option  value="d03" <?php if($row_TemplateDetail['FieldID']=="d03") {echo "selected";} ?> size="32"> d03 </option>

              <option  value="f01" <?php if($row_TemplateDetail['FieldID']=="f01") {echo "selected";} ?> size="32"> f01 </option>
              <option  value="f02" <?php if($row_TemplateDetail['FieldID']=="f02") {echo "selected";} ?> size="32"> f02 </option>
              <option  value="f03" <?php if($row_TemplateDetail['FieldID']=="f03") {echo "selected";} ?> size="32"> f03 </option>

              <option  value="l01" <?php if($row_TemplateDetail['FieldID']=="l01") {echo "selected";} ?> size="32"> l01 </option>
              <option  value="l02" <?php if($row_TemplateDetail['FieldID']=="l02") {echo "selected";} ?> size="32"> l02 </option>
              <option  value="l03" <?php if($row_TemplateDetail['FieldID']=="l03") {echo "selected";} ?> size="32"> l03 </option>
              <option  value="l04" <?php if($row_TemplateDetail['FieldID']=="l04") {echo "selected";} ?> size="32"> l04 </option>
              <option  value="l05" <?php if($row_TemplateDetail['FieldID']=="l05") {echo "selected";} ?> size="32"> l05 </option>
              <option  value="l06" <?php if($row_TemplateDetail['FieldID']=="l06") {echo "selected";} ?> size="32"> l06 </option>

              <option  value="p01" <?php if($row_TemplateDetail['FieldID']=="p01") {echo "selected";} ?> size="32"> p01 </option>
              <option  value="p02" <?php if($row_TemplateDetail['FieldID']=="p02") {echo "selected";} ?> size="32"> p02 </option>
              <option  value="p03" <?php if($row_TemplateDetail['FieldID']=="p03") {echo "selected";} ?> size="32"> p03 </option>
              <option  value="p04" <?php if($row_TemplateDetail['FieldID']=="p04") {echo "selected";} ?> size="32"> p04 </option>
              <option  value="p05" <?php if($row_TemplateDetail['FieldID']=="p05") {echo "selected";} ?> size="32"> p05 </option>
              <option  value="p06" <?php if($row_TemplateDetail['FieldID']=="p06") {echo "selected";} ?> size="32"> p06 </option>  
            </select>
            </div>
          </div>
        </div>
    </div>

    <!-- 欄位標題 -->
    <div class="form-group row">
        <label for="Sq" class="col-sm-2 col-form-label">欄位標題</label>
        <div class="col-sm-10">
        <input type="text" name="FieldTitle" value="<?php echo $row_TemplateDetail['FieldTitle']; ?>" class="form-control">
        </div>
    </div>

    <!-- 順序 -->
    <div class="form-group row">
        <label for="Sq" class="col-sm-2 col-form-label">順序</label>
        <div class="col-sm-10">
        <input type="text" name="Sq" value="<?php echo $row_TemplateDetail['Sq']; ?>" class="form-control">
      </div>
    </div>

    <!-- 種類 -->
    <div class="row">
      <div class="col">
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">種類</label>
            <div class="col-sm-10">
            <select class="custom-select" name="Type" >
              <option  value="Text" <?php if($row_TemplateDetail['Type']=="Text") { echo "selected";} ?> > 文字 </option>
              <option  value="Radio" <?php if($row_TemplateDetail['Type']=="Radio") { echo "selected";} ?> > 圓鈕</option>
              <option  value="Select" <?php if($row_TemplateDetail['Type']=="Select") { echo "selected";} ?> > 清單 </option>
              <option  value="CheckBox" <?php if($row_TemplateDetail['Type']=="CheckBox") { echo "selected";} ?> > 多重選 </option>
              <!-- <option  value="C" <?php //if($row_TemplateDetail['Type']=="C") { echo "selected";} ?> > HTML編輯器 </option> -->
              <option  value="D" <?php if($row_TemplateDetail['Type']=="D") { echo "selected";} ?> > ckeditor </option>
              <option  value="A" <?php if($row_TemplateDetail['Type']=="A") { echo "selected";} ?> > 文字區域 </option>
              <option  value="P" <?php if($row_TemplateDetail['Type']=="P") { echo "selected";} ?> > 壓縮圖片 (限 p01-p06欄位) </option>	 
              <option  value="Q" <?php if($row_TemplateDetail['Type']=="Q") { echo "selected";} ?> > 中文圖片(限 p01-p06欄位) </option> 
              <option  value="F" <?php if($row_TemplateDetail['Type']=="F") { echo "selected";} ?> > PDF上傳(限 f01-f03欄位) </option>	
              <option  value="FileDB" <?php if($row_TemplateDetail['Type']=="FileDB") { echo "selected";} ?> > 檔案進資料庫 (限 f01-f03欄位) </option>	  
              <option  value="Calendar" <?php if($row_TemplateDetail['Type']=="Calendar") { echo "selected";} ?> > 日曆 </option>
              <option  value="Key" <?php if($row_TemplateDetail['Type']=="Key") { echo "selected";} ?> > Key </option> 
            </select>
            </div>
          </div>
        </div>
    </div>

    <!-- 儲存值(請用"-"分開) -->
    <div class="form-group row">
        <label for="Sq" class="col-sm-2 col-form-label">儲存值</label>
        <div class="col-sm-10">
        <input type="text" name="Value" value="<?php echo $row_TemplateDetail['Value']; ?>" class="form-control">
        <small>(請用"-"分開)</small>
      </div>
    </div>

    <!-- 說明對照值(請用"-"分開) -->
    <div class="form-group row">
        <label for="Sq" class="col-sm-2 col-form-label">說明對照值</label>
        <div class="col-sm-10">
        <input type="text" name="MappingValue" value="<?php echo $row_TemplateDetail['MappingValue']; ?>" class="form-control">
        <small>(請用"-"分開)</small>
      </div>
    </div>

    <input type="hidden" name="Action" value="<?php echo $_GET['Action']; ?>">
    <input type="hidden" name="MM_update" value="form1">
    <input type="hidden" name="KeyID" value="<?php echo $row_TemplateDetail['KeyID']; ?>">
    <input type="hidden" name="Template" value="<?php echo $row_TemplateDetail['Template']; ?>">
  </form>
  <!-- end form -->
</div>
<!-- end -->
<?php
mysqli_free_result($TemplateDetail);
?>
