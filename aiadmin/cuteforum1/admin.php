<?php require_once('../../Connections/AdminMySQL.php'); ?>
<?php
// *** Restrict Access To Page: Grant or deny access to this page
if($_SESSION['Mode']=="") {
$FF_authFailedURL="../../Login.php";
//header("Location: $FF_authFailedURL");
exit;}






$currentPage = $_SERVER["PHP_SELF"];

$maxRows_RecMain = 10;
$pageNum_RecMain = 0;
if (isset($_GET['pageNum_RecMain'])) {
  $pageNum_RecMain = $_GET['pageNum_RecMain'];
}
$startRow_RecMain = $pageNum_RecMain * $maxRows_RecMain;


$query_RecMain = "SELECT ".$Table_ID."_postMain1.main_id, ".$Table_ID."_postMain1.main_important, ".$Table_ID."_postMain1.main_subject, ".$Table_ID."_postMain1.main_name, ".$Table_ID."_postMain1.main_email, ".$Table_ID."_postMain1.num_hits, Count(postRe.main_id) AS num_re, Max(If(postRe.re_time ,postRe.re_time,".$Table_ID."_postMain1.main_time)) AS NewUpdate FROM ".$Table_ID."_postMain1 LEFT JOIN postRe ON ".$Table_ID."_postMain1.main_id = postRe.main_id GROUP BY ".$Table_ID."_postMain1.main_id, ".$Table_ID."_postMain1.main_important, ".$Table_ID."_postMain1.main_subject, ".$Table_ID."_postMain1.main_name, ".$Table_ID."_postMain1.main_email, ".$Table_ID."_postMain1.num_hits ORDER BY NewUpdate DESC";
$query_limit_RecMain = sprintf("%s LIMIT %d, %d", $query_RecMain, $startRow_RecMain, $maxRows_RecMain);
$RecMain = mysqli_query($MySQL,$query_limit_RecMain) or die(mysqli_error($MySQL));
$row_RecMain = mysqli_fetch_assoc($RecMain);

if (isset($_GET['totalRows_RecMain'])) {
  $totalRows_RecMain = $_GET['totalRows_RecMain'];
} else {
  $all_RecMain = mysqli_query($MySQL,$query_RecMain);
  $totalRows_RecMain = mysqli_num_rows($all_RecMain);
}
$totalPages_RecMain = ceil($totalRows_RecMain/$maxRows_RecMain)-1;

$queryString_RecMain = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_RecMain") == false && 
        stristr($param, "totalRows_RecMain") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_RecMain = "&" . implode("&", $newParams);
  }
}
$queryString_RecMain = sprintf("&totalRows_RecMain=%d%s", $totalRows_RecMain, $queryString_RecMain);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);
?>
<html>
<head>
<title>超可愛討論區</title>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_nbGroup(event, grpName) { //v6.0
  var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])? args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) {
      img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr)
      for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function centerWindow(theURL,winName,width,height,features) {
//織夢平台 http://www.e-dreamer.idv.tw
//分享是成長的開始
    var window_width = width;
    var window_height = height;
    var edfeatures= features;
    var window_top = (screen.height-window_height)/2;
    var window_left = (screen.width-window_width)/2;
    newWindow=window.open(''+ theURL + '',''+ winName + '','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + edfeatures + '');
    newWindow.focus();
}
//-->
</script>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" text="#000000" link="#FFCC00" vlink="#CC6600" onLoad="MM_preloadImages('../../CuteForumImages/ub_r2_c2_f3.gif','../../CuteForumImages/ub_r2_c2_f2.gif','../../CuteForumImages/ub_r2_c2_f4.gif','../../CuteForumImages/ub1_1_f3.gif','../../CuteForumImages/ub1_1_f2.gif','../../CuteForumImages/ub1_1_f4.gif')">
<div align="center"> 
  <table border="0" cellpadding="0" cellspacing="0" width="760" align="center">
    <!-- fwtable fwsrc="back.png" fwbase="ub.gif" fwstyle="Dreamweaver" fwdocid = "218474546" fwnested="0" -->
    <tr> 
      <td><img src="../../CuteForumImages/spacer.gif" width="121" height="1" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="113" height="1" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="11" height="1" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="113" height="1" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="10" height="1" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="113" height="1" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="279" height="1" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="1" height="1" border="0"></td>
    </tr>
    <tr> 
      <td colspan="7"><img name="ub_r1_c1" src="../../CuteForumImages/ub_r1_c1.gif" width="760" height="139" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="1" height="139" border="0"></td>
    </tr>
    <tr> 
      <td><img name="ub_r2_c1" src="../../CuteForumImages/ub_r2_c1.gif" width="121" height="27" border="0"></td>
      <td><a href="#" target="_top"  onClick="MM_nbGroup('down','navbar1','ub_r2_c2','../../CuteForumImages/ub_r2_c2_f3.gif',1);"  onMouseOver="MM_nbGroup('over','ub_r2_c2','../../CuteForumImages/ub_r2_c2_f2.gif','../../CuteForumImages/ub_r2_c2_f4.gif',1);" onMouseOut="MM_nbGroup('out');" ><img name="ub_r2_c2" src="../../CuteForumImages/ub_r2_c2.gif" width="113" height="27" border="0"></a></td>
      <td><img name="ub_r2_c3" src="../../CuteForumImages/ub_r2_c3.gif" width="11" height="27" border="0"></td>
      
      <td><img name="ub_r2_c5" src="../../CuteForumImages/ub_r2_c5.gif" width="10" height="27" border="0"></td>
     
      <td><img name="ub_r2_c7" src="../../CuteForumImages/ub_r2_c7.gif" width="279" height="27" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="1" height="27" border="0"></td>
    </tr>
    <tr> 
      <td rowspan="2" colspan="7" background="../../CuteForumImages/ub_r3_c1.gif">
        <?php if ($totalRows_RecMain > 0) { // Show if recordset not empty ?>
        <table width="98%" border="0" cellspacing="0" align="center">
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0">
                <tr>
                  <td>&nbsp; </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="1" align="center">
                <tr valign="middle" bgcolor="#990000">
                  <th width="26" height="30">
                    <div align="center"><font color="#666666"><font face="Verdana"><font face="Verdana"><font size="2"><font color="#FFFFFF"></font></font></font></font></font></div>
                  </th>
                  <th width="408" height="30">
                    <div align="left"><font color="#FFFFFF" size="2" face="Verdana">發表主題</font></div>
                  </th>
                  <th width="150" height="30">
                    <div align="left"><font color="#FFFFFF" size="2" face="Verdana">留言人</font></div>
                  </th>
                  <th width="93" height="30"><font color="#FFFFFF" size="2" face="Verdana">點選</font></th>
                  <th width="93" height="30"><font color="#FFFFFF" size="2" face="Verdana">回應</font></th>
                  <th width="169"><font color="#FFFFFF" size="2" face="Verdana">最新回應時間</font></th>
                  <th width="100" height="30">&nbsp;</th>
                </tr>
                <?php do { ?>
                <tr bgcolor="#f9f8ee">
                  <td width="26" height="25">
                    <div align="center"><img src="../../CuteForumImages/<?php echo $row_RecMain['main_important']; ?>"></div>
                  </td>
                  <td width="408" height="25" bgcolor="#f9f8ee"><font size="2"><a href="admin_detail.php?<?php echo $MM_keepNone.(($MM_keepNone!="")?"&":"")."main_id=".$row_RecMain['main_id'] ?>"><?php echo $row_RecMain['main_subject']; ?></a>&nbsp;</font> </td>
                  <td width="150" height="25"><font face="Verdana, 新細明體" size="-1"><a href="mailto:<?php echo $row_RecMain['main_email']; ?>"><?php echo $row_RecMain['main_name']; ?></a>&nbsp;</font></td>
                  <td width="93" height="25">
                    <div align="center"><font face="Verdana" size="1"> <?php echo $row_RecMain['num_hits']; ?>次</font></div>
                  </td>
                  <td width="93" height="25" bgcolor="#f9f8ee">
                    <div align="center"><font face="Verdana" size="1"><?php echo $row_RecMain['num_re']; ?>篇</font></div>
                  </td>
                  <td width="169" height="25" valign="middle">
                    <div align="center"><font size="1" face="Times New Roman, Times, serif"><?php echo $row_RecMain['NewUpdate']; ?></font></div>
                  </td>
                  <td width="100" height="25" valign="middle">
                    <div align="center"><font size="2" face="Times New Roman, Times, serif"><a href="#" onClick="centerWindow('delfunction1.php?main_id=<?php echo $row_RecMain['main_id']; ?>','delfunction','300','200','')">刪除</a></font></div>
                  </td>
                </tr>
                <?php } while ($row_RecMain = mysqli_fetch_assoc($RecMain)); ?>
              </table>
              <table width="100%" border="0" cellspacing="0" height="56" align="center" cellpadding="4">
                <tr>
                  <td height="20" width="50%" bgcolor="#f9f8ee"><font size="-1" color="#FFFFFF"> <font color="#666666"> 記錄 <?php echo ($startRow_RecMain + 1) ?> 到 <?php echo min($startRow_RecMain + $maxRows_RecMain, $totalRows_RecMain) ?> 共 <?php echo $totalRows_RecMain ?> </font></font></td>
                  <td align="right" bgcolor="#f9f8ee"> <font size="-1">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr align="right">
                        <td><font size="2">
                          <?php if ($pageNum_RecMain > 0) { // Show if not first page ?>
                          <a href="<?php printf("%s?pageNum_RecMain=%d%s", $currentPage, 0, $queryString_RecMain); ?>">第一頁</a>
                          <?php } // Show if not first page ?>
                          <?php if ($pageNum_RecMain > 0) { // Show if not first page ?>
                          <a href="<?php printf("%s?pageNum_RecMain=%d%s", $currentPage, max(0, $pageNum_RecMain - 1), $queryString_RecMain); ?>">上一頁</a>
                          <?php } // Show if not first page ?>
                          <?php if ($pageNum_RecMain < $totalPages_RecMain) { // Show if not last page ?>
                          <a href="<?php printf("%s?pageNum_RecMain=%d%s", $currentPage, min($totalPages_RecMain, $pageNum_RecMain + 1), $queryString_RecMain); ?>">下一頁</a>
                          <?php } // Show if not last page ?>
                          <?php if ($pageNum_RecMain < $totalPages_RecMain) { // Show if not last page ?>
                          <a href="<?php printf("%s?pageNum_RecMain=%d%s", $currentPage, $totalPages_RecMain, $queryString_RecMain); ?>">最後一頁</a>
                          <?php } // Show if not last page ?>
                        </font></td>
                      </tr>
                    </table>
                  </font></td>
                </tr>
                <tr>
                  <td height="20" colspan="2">
                    <div align="center"><font size="2" color="#FF9900"><b><font face="Verdana, Arial, Helvetica, sans-serif">[織夢平台版權所有
                            www.e-dreamer.idv.tw 2002&copy; All Rights Reserved]</font></b></font></div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <?php } // Show if recordset not empty ?>
        <?php if ($totalRows_RecMain == 0) { // Show if recordset empty ?>
        <table width="95%" border="0" align="center" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF">
          <tr>
            <td align="center">資料庫中沒有任何資料！</td>
          </tr>
        </table>
        <?php } // Show if recordset empty ?></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="1" height="1" border="0"></td>
    </tr>
    <tr> 
      <td><img src="../../CuteForumImages/spacer.gif" width="1" height="1" border="0"></td>
    </tr>
    <tr> 
      <td colspan="7"><img name="ub_r5_c1" src="../../CuteForumImages/ub_r5_c1.gif" width="760" height="10" border="0"></td>
      <td><img src="../../CuteForumImages/spacer.gif" width="1" height="10" border="0"></td>
    </tr>
  </table>
</div>
</body>
</html>
<?php
mysqli_free_result($RecMain);
?>
