<?php require_once('../../Connections/MySQL.php'); ?>
<?php
// *** Restrict Access To Page: Grant or deny access to this page
if($_SESSION['Mode']=="") {
$FF_authFailedURL="../Login.php";
header("Location: $FF_authFailedURL");
exit;}

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE ".$Table_ID."_postMain1 SET main_subject=%s, main_content=%s WHERE main_id=%s",
                       GetSQLValueString($_POST['main_subject'], "text"),
                       GetSQLValueString($_POST['main_content'], "text"),
                       GetSQLValueString($_POST['main_id'], "int"));

  
  $Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));

  $updateGoTo = "admin_detail.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
  $updateSQL = sprintf("UPDATE postRe SET re_subject=%s, re_content=%s WHERE re_id=%s",
                       GetSQLValueString($_POST['re_subject'], "text"),
                       GetSQLValueString($_POST['re_content'], "text"),
                       GetSQLValueString($_POST['re_id'], "int"));

  
  $Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));

  $updateGoTo = "admin_detail.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_RecMain = "1";
if (isset($_GET['main_id'])) {
  $colname_RecMain = (get_magic_quotes_gpc()) ? $_GET['main_id'] : addslashes($_GET['main_id']);
}

$query_RecMain = sprintf("SELECT *,concat(main_sex,main_face) as faceImg FROM ".$Table_ID."_postMain1 WHERE main_id = %s", $colname_RecMain);
$RecMain = mysqli_query($MySQL,$query_RecMain) or die(mysqli_error($MySQL));
$row_RecMain = mysqli_fetch_assoc($RecMain);
$totalRows_RecMain = mysqli_num_rows($RecMain);

$maxRows_RecRe = 10;
$pageNum_RecRe = 0;
if (isset($_GET['pageNum_RecRe'])) {
  $pageNum_RecRe = $_GET['pageNum_RecRe'];
}
$startRow_RecRe = $pageNum_RecRe * $maxRows_RecRe;

$colname_RecRe = "1";
if (isset($_GET['main_id'])) {
  $colname_RecRe = (get_magic_quotes_gpc()) ? $_GET['main_id'] : addslashes($_GET['main_id']);
}

$query_RecRe = sprintf("SELECT *,concat(re_sex,re_face) as faceImg FROM postRe WHERE main_id = %s ORDER BY re_time DESC", $colname_RecRe);
$query_limit_RecRe = sprintf("%s LIMIT %d, %d", $query_RecRe, $startRow_RecRe, $maxRows_RecRe);
$RecRe = mysqli_query($MySQL,$query_limit_RecRe) or die(mysqli_error($MySQL));
$row_RecRe = mysqli_fetch_assoc($RecRe);

if (isset($_GET['totalRows_RecRe'])) {
  $totalRows_RecRe = $_GET['totalRows_RecRe'];
} else {
  $all_RecRe = mysqli_query($MySQL,$query_RecRe);
  $totalRows_RecRe = mysqli_num_rows($all_RecRe);
}
$totalPages_RecRe = ceil($totalRows_RecRe/$maxRows_RecRe)-1;
?>
<html>
<head>
<title>超可愛討論區</title>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<link href="style.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function centerWindow(theURL,winName,width,height,features) {
//織夢平台 http://www.e-dreamer.idv.tw
//分享是成長的開始
    var window_width = width;
    var window_height = height;
    var edfeatures= features;
    var window_top = (screen.height-window_height)/2;
    var window_left = (screen.width-window_width)/2;
    newWindow=window.open(''+ theURL + '',''+ winName + '','width=' + window_width + ',height=' + window_height + ',top=' + window_top + ',left=' + window_left + ',features=' + edfeatures + '');
    newWindow.focus();
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000">
<table width="760" border="0" cellspacing="0" cellpadding="0" align="center" background="../../CuteForumImages/button02.gif" height="35">
  <tr> 
    <td width="124"><font face="Geneva, Arial, Helvetica, san-serif" size="3"><b><font color="#FF6600"></font></b></font></td>
    <td valign="middle"><font face="Geneva, Arial, Helvetica, san-serif" size="3"><b><font color="#FF6600"><font size="4"><?php echo $row_RecMain['main_subject']; ?></font></font></b></font></td>
    <td width="80" valign="middle"><font face="Verdana, 新細明體" size="1" color="#FF0000">&nbsp;<?php echo $totalRows_RecRe ?> </font></td>
  </tr>
</table>
<hr size="1" width="760" noshade>
<table width="760" border="0" cellspacing="0" cellpadding="0" align="center">
  
  <tr> 
    <td><img src="../../CuteForumImages/ub_r5_c2.gif" width="760" height="10"></td>
  </tr>
  <tr> 
    <td background="../../CuteForumImages/ub_r3_c1.gif"> <form method="POST" action="<?php echo $editFormAction; ?>" name="form1">
        
        <table width="95%" border="0" align="center" cellpadding="4" cellspacing="0" bgcolor="#FFFFFF" class="boxRed">
          
          <tr> 
            <td width="150" valign="top"> <div align="center"><img src="../../CuteForumImages/face/<?php echo $row_RecMain['faceImg']; ?>"><br>
                <font face="Verdana, 新細明體" size="2"></font> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td><img src="../../CuteForumImages/spacer.gif" width="1" height="2"></td>
                  </tr>
                </table>
                <font size="2"><?php echo $row_RecMain['main_name']; ?></font> </div></td>
            <td valign="top"> <table width="98%" border="0" cellspacing="0" cellpadding="5" align="center"  bgcolor="#FFFFFF">
                <tr> 
                  <td height="25"> <font color="#003399"><strong></strong></font><font size="1" face="Arial, Helvetica, sans-serif"> 
                    <input name="main_subject" type="text" id="main_subject" value="<?php echo $row_RecMain['main_subject']; ?>">
(time:<?php echo $row_RecMain['main_time']; ?> ip:<?php echo $row_RecMain['main_ip']; ?>) </font> 
                    <hr noshade size="1"> <font color="#0066CC" size="-1"> 
                    <textarea name="main_content" cols="74" rows="5" id="main_content"><?php echo $row_RecMain['main_content']; ?></textarea>
                    <input name="main_id" type="hidden" id="main_id" value="<?php echo $row_RecMain['main_id']; ?>">
                  </font> </td>
                </tr>
              </table></td>
          </tr>
          <tr> 
            <td width="150" valign="middle" height="30"> <table border="0" cellspacing="0" cellpadding="0" width="142" align="center">
                <tr> 
                  <td><img src="../../CuteForumImages/spacer.gif" width="142" height="1"></td>
                </tr>
                <tr> 
                  <td background="../../CuteForumImages/button01.gif" height="20"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="10">&nbsp;</td>
                        <td valign="baseline"> <div align="center"><font face="新細明體" size="2"><a href="admin.php">回管理主畫面</a></font></div></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
            <td valign="middle" height="30"> <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr> 
                  <td><input type="submit" name="Submit" value="更新"> <input type="reset" name="Submit2" value="重設"> 
                  </td>
                </tr>
              </table></td>
          </tr>
        </table>
        
        
        
        <input type="hidden" name="MM_update" value="form1">
    </form> 
      <hr noshade size="1" width="95%">
      <form method="POST" action="<?php echo $editFormAction; ?>" name="form2" id="form2">
        <?php do { ?>
        <?php if ($totalRows_RecRe > 0) { // Show if recordset not empty ?>
        <table width="95%" border="0" align="center" cellpadding="4" cellspacing="0" bgcolor="#FFFFFF" class="boxGray">
          <tr>
            <td width="150" valign="top">
              <div align="center"><img src="../../CuteForumImages/face/<?php echo $row_RecRe['faceImg']; ?>"><br>
                  <font face="Verdana, 新細明體" size="2"></font>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="../../CuteForumImages/spacer.gif" width="1" height="2"></td>
                    </tr>
                  </table>
              <font size="2"><?php echo $row_RecRe['re_name']; ?></font> </div>
            </td>
            <td valign="top">
              <table width="98%" border="0" cellspacing="0" cellpadding="5" align="center"  bgcolor="#FFFFFF">
                <tr>
                  <td height="25"> <font color="#003399"><strong></strong></font><font size="1" face="Arial, Helvetica, sans-serif">
                    <input name="re_subject" type="text" id="re_subject" value="<?php echo $row_RecRe['re_subject']; ?>">
              (time:<?php echo $row_RecRe['re_time']; ?> ip:<?php echo $row_RecRe['re_ip']; ?>) </font>
                      <hr noshade size="1">
                      <font color="#0066CC" size="-1">
                      <textarea name="re_content" cols="74" rows="5" id="re_content"><?php echo $row_RecRe['re_content']; ?></textarea>
                      <input name="re_id" type="hidden" id="re_id" value="<?php echo $row_RecRe['re_id']; ?>">
                  </font> </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td width="150" valign="middle" height="30">
              <table border="0" cellspacing="0" cellpadding="0" width="142" align="center">
                <tr>
                  <td><img src="../../CuteForumImages/spacer.gif" width="142" height="1"></td>
                </tr>
                <tr>
                  <td background="../../CuteForumImages/button01.gif" height="20">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="15">&nbsp;</td>
                        <td valign="baseline">
                          <div align="center"><font size="2" face="新細明體"><a href="#" onClick="centerWindow('delfunction2.php?re_id=<?php echo $row_RecRe['re_id']; ?>','delfunction','420','300','status=yes,scrollbars=yes')">刪除留言</a> | <a href="admin.php">回主畫面</a></font></div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td valign="middle" height="30">
              <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><input type="submit" name="Submit3" value="更新">
                      <input type="reset" name="Submit22" value="重設">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <hr noshade size="1" width="95%">
        <?php } // Show if recordset not empty ?>
        <?php } while ($row_RecRe = mysqli_fetch_assoc($RecRe)); ?>
        <input type="hidden" name="MM_update" value="form2">
      </form></td>
  </tr>
  <tr> 
    <td><img src="../../CuteForumImages/ub_r5_c1.gif" width="760" height="10"></td>
  </tr>
</table>



</body>
</html>
<?php
mysqli_free_result($RecMain);

mysqli_free_result($RecRe);
?>
