<?php require_once('../../Connections/MySQL.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ((isset($_GET['re_id'])) && ($_GET['re_id'] != "") && (isset($_GET['delsure']))) {
  $deleteSQL = sprintf("DELETE FROM postRe WHERE re_id=%s",
                       GetSQLValueString($_GET['re_id'], "int"));

  
  $Result1 = mysqli_query($MySQL,$deleteSQL) or die(mysqli_error($MySQL));

  $deleteGoTo = "win_close_re.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_RecRe = "1";
if (isset($_GET['re_id'])) {
  $colname_RecRe = (get_magic_quotes_gpc()) ? $_GET['re_id'] : addslashes($_GET['re_id']);
}

$query_RecRe = sprintf("SELECT *,concat(re_sex,re_face) as faceImg FROM postRe WHERE re_id = %s", $colname_RecRe);
$RecRe = mysqli_query($MySQL,$query_RecRe) or die(mysqli_error($MySQL));
$row_RecRe = mysqli_fetch_assoc($RecRe);
$totalRows_RecRe = mysqli_num_rows($RecRe);
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>您確定要刪除此筆資料嗎？</title>
</head>

<body>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><table width="95%" border="0" align="center" cellpadding="4" cellspacing="0" bgcolor="#FFFFFF" class="boxGray">
        <tr> 
          <td width="100" valign="top"> <div align="center"><img src="../../CuteForumImages/face/<?php echo $row_RecRe['faceImg']; ?>"><br>
              <font face="Verdana, 新細明體" size="2"></font> 
              <?php echo $row_RecRe['re_name']; ?>
              <form name="form1">
                <input type="submit" name="Submit" value="刪除">
                <input name="button" type=button onClick="javascript:self.close();" value="關閉">
                <input name="delsure" type="hidden" id="delsure" value="1">
                <input name="re_id" type="hidden" id="re_id" value="<?php echo $row_RecRe['re_id']; ?>">
              </form>
              <font size="2"></font></div></td>
          <td valign="top"> <table width="98%" border="0" cellspacing="0" cellpadding="5" align="center"  bgcolor="#FFFFFF">
              <tr> 
                <td height="25"> <font size="1" face="Arial, Helvetica, sans-serif">                  <font color="#FF0000" size="2"><strong><?php echo $row_RecRe['re_subject']; ?></strong></font>&nbsp;(<?php echo $row_RecRe['re_time']; ?>                ) </font> <hr noshade size="1"><font size="2">&nbsp;</font><?php echo nl2br(htmlspecialchars($row_RecRe['re_content'])); ?> </td>
              </tr>
            </table></td>
        </tr>
      </table>
      
      
    </td>
  </tr>
</table>
</body>
</html>
<?php
mysqli_free_result($RecRe);
?>
