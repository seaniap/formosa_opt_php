<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$colname_TemplateDetail = "-1";
if (isset($_GET['Template'])) {
  $colname_TemplateDetail = (get_magic_quotes_gpc()) ? $_GET['Template'] : addslashes($_GET['Template']);
}

$query_TemplateDetail = sprintf("SELECT * FROM ".$Table_ID."_TemplateDetail WHERE Template = %s ORDER BY Sq ASC", GetSQLValueString($colname_TemplateDetail, "text"));
//echo $query_TemplateDetail ;
$TemplateDetail = mysqli_query($MySQL,$query_TemplateDetail) or die(mysqli_error($MySQL));
$row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail);
$totalRows_TemplateDetail = mysqli_num_rows($TemplateDetail);

$query_Template = "SELECT * FROM ".$Table_ID."_Template WHERE T_ID = '".$colname_TemplateDetail."'";
//echo $query_Template;
$Template = mysqli_query($MySQL,$query_Template) or die(mysqli_error($MySQL));
$row_Template = mysqli_fetch_assoc($Template);


?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/table.css" rel="stylesheet" type="text/css">

<title>版型欄位設定</title>
<script language="javascript" >
function ConfirmDel(item1,KeyID,Template){
 answer = confirm("確認刪除參數"+item1);
   if (answer){
     location='survey/SurveyTemplateDetailDelF.php?KeyID='+KeyID+'&Template='+Template; }	
}	  
</script> 
</head>

<body>


<div id="WraperArea">
<div id="MainHeader">
    <div class="PageNumberRight"><img src="images/ICOsystem.gif" width="20" height="15" align="absmiddle">問卷管理</div>
    <div class="whereYouAre">問卷系統 &gt;問卷清單&gt;個別問卷</div>
    <div class="clear"></div>
<!-- -->
<div id="Box">
<div class="indexhead"></div>
    <div class="indexContent">  
    <div class="indexContentText">
        <ul>
            <li><span class="BoldText">版　　型：</span><?php  if(isset($_GET['Title'])){ echo $_GET['Title']; }else{echo $row_TemplateDetail['Template'];} ?></li>
            <li><span class="BoldText">修改日期：</span><?php echo $row_Template['CreateDate']; ?></li>
            <li><span class="BoldText">問卷主題：</span><?php echo $row_Template['T_Name']; ?></li>
            <li><span class="BoldText">問卷說明：</span><br/><span class="BoldText2"><?php echo $row_Template['T_Remark']; ?></span></li>
        </ul>
    </div>
</div>
    <div class="indexbottom"></div>
</div>
<!-- -->
</div>
<div class="clear"></div>
<div class="datatable">
<div class="headtext2"></div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style=" border:#CCCCCC 1px solid;">
    <tr>
      <th>代碼</th>
	  <th>題號</th>
      <th>問題說明</th>
      <th>題目順序</th>
      <th>問題類型</th>
      <th>問題選項</th>
      <!--<th>說明對照值</th> -->
	  <th>必填、非必填</th>
	  <th>   
	  功能	   </th>
     </tr>
   
   
   
   
   <?php  $n=0; ?>
	<?php do { $n=$n+1; ?>
   <tr <?php if($nn%2==0) { ?>style="background-color:#EFEFEF"<?php } else { ?>style="background-color:#FFFFFF"
  <?php } $nn=$nn+1; ?>
  >
      <td><?php echo $row_TemplateDetail['FieldID']; ?></td>
	  <td><?php echo $nn; ?></td>
      <td><?php echo $row_TemplateDetail['FieldTitle']; ?></td>
      <td><?php echo $row_TemplateDetail['Sq']; ?></td>
      <td align="left">
	  <?php
	  switch($row_TemplateDetail['Type']){
	  case "Text" :
	      echo "文字-單行欄位 "; 
	       break; 
	  case "Radio" :
	      echo "單選-圓鈕"; 
	       break;
	  case "RadioText" :
	      echo "單選-圓鈕+1個其他(文字)"; 
	       break;	  
	  case "RadioReason" :
	      echo "單選-圓鈕+原因欄位(文字)"; 
	       break;
	  case "Select" :
	      echo "單選-下拉選單"; 
	       break;
	  case "CheckBox" :
	      echo "複選-核取方塊鈕"; 
	       break;
	  case "C" :
	      echo "其他-html編輯器"; 
	       break;
	  case "A" :
	      echo "文字-多行欄位"; 
	       break;
	  case "P" :
	      echo "其他-上傳圖片功能"; 
	       break;
	  }
	  
	  
	  
	  ?>	  </td>
      <td align="left">
	  <span style="text-align:left"><?php //echo $row_TemplateDetail['Value']; ?>
	  <?php 
	  $ss=explode("-", $row_TemplateDetail['Value']);
      $n=1;
	  foreach ($ss as $arr){
	  echo $n.".".$arr."</br>";
	  $n++;
	  }
	  
	  ?></span>
	  </td>
      <!--<td><?php 
	   $ss=explode("-",  $row_TemplateDetail['MappingValue']);
      $n=1;
	  foreach ($ss as $arr){
	  echo $n.".".$arr."</br>";
	  $n++;
	  }
	 ?> 
	  
	  
	  </td> -->
	  <td><?php 
	  switch($row_TemplateDetail['FieldCheck']){
	  case "0" :
	      echo "非必填"; 
	       break;
	  
	  case "1" :
	      echo "必填"; 
	       break;
	  }
	  ?></td>
	  <td nowrap="nowrap">   
	  <a href="main.php?Page=8-3-1&Action=ins&Template=<?php echo $_GET['Template']; ?>&KeyID=<?php echo $row_TemplateDetail['KeyID']; ?>&Title=<?php echo rawurlencode($_GET['Title']); ?>" target="_blank" >插入</a><br />
	  <img src="images/ICOdelete.gif" alt="刪除" width="15" height="12" border="0" onClick="ConfirmDel('<?php echo $row_TemplateDetail['FieldID']; ?>','<?php echo $row_TemplateDetail['KeyID']; ?>','<? echo $row_TemplateDetail['Template']; ?>');"><br />
	  <a href="main.php?Page=8-3-1&Action=edit&Template=<?php echo $row_TemplateDetail['Template']; ?>&KeyID=<?php echo $row_TemplateDetail['KeyID']; ?>&Title=<? echo rawurlencode($_GET['Title']); ?>" target="_blank" >修改</a>	   </td>
     </tr>
	  <?php } while ($row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail)); ?>
</table>
</div>
<?php // } ?>
</div>
</body>
</html>
<?php
mysqli_free_result($TemplateDetail);
?>
