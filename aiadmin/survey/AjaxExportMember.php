<?
//權限檢查不合格就 Logout 
if($_SESSION['Mode']=="") {
$FF_authFailedURL="../Logout.php";
header("Location: $FF_authFailedURL");
exit;} 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="http://www.outlet.idv.tw/mli/aiadmin/AA/css/style.css" rel="stylesheet" type="text/css">
<link href="http://www.outlet.idv.tw/mli/aiadmin/AA/css/table.css" rel="stylesheet" type="text/css">

<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<script language=JavaScript >
var XMLHttpRequestObject = createXMLHttpRequestObject();

function createXMLHttpRequestObject()
{
  var XMLHttpRequestObject = false;
  
  try
  {
    XMLHttpRequestObject = new XMLHttpRequest();
  }
  catch(e)
  {
    var aryXmlHttp = new Array(
                               "MSXML2.XMLHTTP",
                               "Microsoft.XMLHTTP",
                               "MSXML2.XMLHTTP.6.0",
                               "MSXML2.XMLHTTP.5.0",
                               "MSXML2.XMLHTTP.4.0",
                               "MSXML2.XMLHTTP.3.0"
                               );
    for (var i=0; i<aryXmlHttp.length && !XMLHttpRequestObject; i++)
    {
      try
      {
        XMLHttpRequestObject = new ActiveXObject(aryXmlHttp[i]);
      } 
      catch (e) {}
    }
  }
  
  if (!XMLHttpRequestObject)
  {
    alert("Error: failed to create the XMLHttpRequest object.");
  }
  else 
  {
    return XMLHttpRequestObject;
  }
}

function getData(dataSource, divID)
{
  if(XMLHttpRequestObject)
  {
    dataSource += "&parm="+new Date().getTime();
    var objDiv = document.getElementById(divID);
    
    XMLHttpRequestObject.open("GET", dataSource);
    XMLHttpRequestObject.onreadystatechange = function()
    {
      try
      {
        switch(XMLHttpRequestObject.readyState)
        {
          case 1:
            objDiv.innerHTML = '  <img src=../images/loading.gif alt=Loading...>  請稍待片刻 ...  ';
            break;
          case 4:
            if(XMLHttpRequestObject.status == 200)
            {
              objDiv.innerHTML = XMLHttpRequestObject.responseText;
            }
            else
            {
              objDiv.innerHTML = 'Error getting from php result...';
            }
            break;
        }
      }
      catch(e){}
    }
    try
    {
      XMLHttpRequestObject.send(null);
    }
    catch(e){}
  }
}

function postData(dataSource, divID)
{
  if(XMLHttpRequestObject)
  {
    var objDiv = document.getElementById(divID);
    
    XMLHttpRequestObject.open("POST", dataSource);
    XMLHttpRequestObject.setRequestHeader("Method", "POST " + dataSource + " HTTP/1.1");
	  XMLHttpRequestObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
    XMLHttpRequestObject.onreadystatechange = function()
    {
      try
      {
        switch(XMLHttpRequestObject.readyState)
        {
          case 1:
             objDiv.innerHTML = '  <img src=../images/loading.gif alt=Loading...>  請稍待片刻 ...  ';
			
            break;
          case 4:
            if(XMLHttpRequestObject.status == 200)
            {
              objDiv.innerHTML = XMLHttpRequestObject.responseText;
			  alert(XMLHttpRequestObject.responseText);
			  document.getElementById("resultsA").innerHTML = xmlHttp.responseText;  
            }
            else
            {
              objDiv.innerHTML = 'Error getting from php result...';
            }
            break;
        }
      }
      catch(e){}
    }
    
    dataSource += "&parm="+new Date().getTime();
    try
    {
      XMLHttpRequestObject.send(dataSource);
    }
    catch(e){}
  }
}

</script>
<?php
/* hippo_ajax class by DavidLanz
  
Function
========

Class Member Function
========

*/


class hippo_ajax
{
  var $err_str;
  
  /* Constructor */
  function hippo_ajax()
  {
    $this->init();
  }
  
  function init()
  {
    return true;
  }
  
  function trimStr($str)
  {
    return trim($str);
  }
  
  function add_get_link($id, $prefix, $urlProcess, $targetDiv)
  {
    if($id=='' || $urlProcess=='' || $targetDiv=='')
    {
      $this->err_str = 'add_get_link() method need valid variable.';
      return false;
    }
    else
    {
      $urlProcess = $this->trimStr($urlProcess);
      $targetDiv = $this->trimStr($targetDiv);
      $aryTmp = split('\?', $urlProcess);
      $aryParse[$id][0]=$aryTmp[1];
      $urlProcess = $aryTmp[0].'?idParse='.$id.'&'.$aryTmp[1];
      echo '<a href=javascript:getData('.'\''.$urlProcess.'\''.','.'\''.$targetDiv.'\''.')>'.$prefix.'</a>';
      
      return true;
    }
  }
  
  function add_post_link($id, $prefix, $urlProcess, $targetDiv)
  {
    if($urlProcess=='' || $targetDiv=='')
    {
      $this->err_str = 'add_post_link() method need valid variable.';
      return false;
    }
    else
    {
      $urlProcess = $this->trimStr($urlProcess);
      $targetDiv = $this->trimStr($targetDiv);
      $aryTmp = split('\?', $urlProcess);
      $aryParse[$id][0]=$aryTmp[1];
      $urlProcess = $aryTmp[0].'?&idParse='.$id.'&'.$aryTmp[1];
      echo '<a href=javascript:postData('.'\''.$urlProcess.'\''.','.'\''.$targetDiv.'\''.')>'.$prefix.'</a>';
      
      return true;
    }
  }
  
  function show_error()
  {
    return $this->err_str;
  } 
}

?>

<title>匯出名單</title>
</head>

<body>

<?php
  $fobj = new hippo_ajax();
  
  $dataSource1 =$_GET['Export']."?".$_SERVER['QUERY_STRING']; 
  //$dataSource2 ='hippo_ajax_exec.php?test2=HippoEmily';
  
  $fobj->add_get_link('link1', '按這裡開始匯出名單   請記得刪除檔案', $dataSource1, 'showDiv1');
  echo '<br>';
  //$fobj->add_post_link('link2', 'Loading Image Demo(POST)', $dataSource2, 'showDiv2');
?>

<div id="showDiv1"></div>
<div id="showDiv2"></div>
<div style="border:#999999 1px groove; width:80%; padding:10px;">
<p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#990000;">請務必刪除資料，一但資料忘記刪除，不小心流入他人手中，</br>
依據行政院公佈的電腦處理個人資料保護法、電腦處理個人資料保護法施行細則，</br>
將處以刑責與罰鍰，後果非常嚴重！</br>
請各位使用者在下載檔案後，務必記得刪除檔案。
</p>
<label><a href="http://www.dgbas.gov.tw/ct.asp?xItem=10116&ctNode=2286" target="_blank">電腦處理個人資料保護法</a><span> | </span></label></br>
<label><a href="http://law.moj.gov.tw/Scripts/Query4A.asp?FullDoc=all&Fcode=I0050022" target="_blank">電腦處理個人資料保護法施行細則</a></label>
</p>
</div>

</body>
</html>
