<?php 
if (substr($_SERVER["PHP_SELF"],-8,8)<>"main.php") {
require_once('../../Connections/AdminMySQL.php');  }

//權限檢查不合格就 Logout 
if($_SESSION['Mode']=="") {
$FF_authFailedURL="../../Logout.php target=_TOP";
header("Location: $FF_authFailedURL");
exit;} 

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_Template = 10;
$pageNum_Template = 0;
if (isset($_GET['pageNum_Template'])) {
  $pageNum_Template = $_GET['pageNum_Template'];
}
$startRow_Template = $pageNum_Template * $maxRows_Template;

if($_GET['Where']<>"")
{ $WhereSQL=" AND a.T_Category='".$_GET['Where']."' ";}
else
{ $WhereSQL=" ";}

if($_GET['T_Type']<>"")
{ $WhereSQL=$WhereSQL." AND a.T_Type='".$_GET['T_Type']."' ";}



if($_POST['search']=="") {
$query_Template = "SELECT * FROM ".$Table_ID."_Template a WHERE  a.T_Type='問卷' ".$WhereSQL." order by a.CreateDate DESC";
}
else {
$query_Template = " SELECT * FROM ".$Table_ID."_Template WHERE  T_ID in (SELECT a.T_ID  FROM ".$Table_ID."_Template a,".$Table_ID."_TemplateDetail b WHERE  a.T_Type='問卷' and a.T_FileName=b.Template AND ( b.FieldTitle like '%".$_POST['search']."%'  OR  b.Value  like '%".$_POST['search']."%'  OR  b.MappingValue like '%".$_POST['search']."%'  OR  a.T_Name like '%".$_POST['search']."%'   OR  a.T_Remark like '%".$_POST['search']."%' )  group by a.T_ID )".$WhereSQL." order by CreateDate DESC";
}



$query_limit_Template = sprintf("%s LIMIT %d, %d", $query_Template, $startRow_Template, $maxRows_Template);
//echo $query_limit_Template;
$Template = mysqli_query($MySQL,$query_limit_Template) or die(mysqli_error($MySQL));
$row_Template = mysqli_fetch_assoc($Template);

if (isset($_GET['totalRows_Template'])) {
  $totalRows_Template = $_GET['totalRows_Template'];
} else {
  $all_Template = mysqli_query($MySQL,$query_Template);
  $totalRows_Template = mysqli_num_rows($all_Template);
}
$totalPages_Template = ceil($totalRows_Template/$maxRows_Template)-1;

$query_TGroup = "Select T_Category as Category from ".$Table_ID."_Template where T_Type='問卷'   group by T_Category order by T_Category";
$TGroup = mysqli_query($MySQL,$query_TGroup) or die(mysqli_error($MySQL));
$row_TGroup = mysqli_fetch_assoc($TGroup);
//$totalRows_TGroup = mysqli_num_rows($TGroup);

$query_T_Type = "Select DISTINCT T_Type as T_Type from ".$Table_ID."_Template WHERE  T_Type='問卷' order by T_Type";
$T_Type = mysqli_query($MySQL,$query_T_Type) or die(mysqli_error($MySQL));
$row_T_Type = mysqli_fetch_assoc($T_Type);
//$totalRows_T_Type = mysqli_num_rows($T_Type);


$queryString_Template = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Template") == false && 
        stristr($param, "totalRows_Template") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Template = "&" . implode("&", $newParams);
  }
}
$queryString_Template = sprintf("&totalRows_Template=%d%s", $totalRows_Template, $queryString_Template);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);
?>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>版型瀏覽</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/table.css" rel="stylesheet" type="text/css">
<link href="../../script/link.css" rel="stylesheet" type="text/css">
<link href="../../script/txt.css" rel="stylesheet" type="text/css">
<link href="../../script/all.css" rel="stylesheet" type="text/css">
<LINK href="../../script/css.css" type="text/css" rel="stylesheet"> 

<SCRIPT language=JavaScript src="../../script/preview_templates.js" type="text/javascript"></SCRIPT>
<SCRIPT language=JavaScript src="../../script/loader.js" type="text/javascript"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">

function MM_jumpMenu(selObj,restore){ //v3.0
  eval("location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!--開始 -->
<div id="Box">
	<div class="indexhead"></div>
    <div class="indexContent"><table width="80%" border="0" align="center" cellpadding="2" cellspacing="1">
  <form name="form2">
  <tr>
  <td width="20%" nowrap><label class="RecordData2" style="text-align:right;font-family:Arial, Helvetica, sans-serif;font-size:12px;color:#6699FF;padding-left:2em;">請選擇分類</label>  </td>
  <td width="15%" align="left">
	<select name="menu2" onChange="MM_jumpMenu(this,0)">  
				   <option value="SurveyViewC.php" selected><?php if ($_GET['Where']<>''){echo $_GET['Where'];}else{ echo "全部顯示";} ?> </option>      
				    <?php if ($_GET['Where']<>''){echo " <option value=SurveyViewC.php>全部顯示</option>";} ?>
				         
                <?php do {  
				  if ($_GET['Where']<>$row_TGroup['Category'])             
                     { echo "<option value=SurveyViewC.php?Where=".$row_TGroup['Category'].">".$row_TGroup['Category']."</option>";}?>
                          
         <?php } while ($row_TGroup = mysqli_fetch_assoc($TGroup)); ?>
                  </select>      
   </form>
  </td>
  <td width="65%" align="left">
  
  <form method="POST">
  全文檢索
  <input type="text" width="100" name="search"  > 
  <input type="submit" value="查詢">
  </form>
  
  
  </td>
  </tr>
</table>
    </div>
    <div class="indexbottom"></div>
</div>
<!--開始 -->
<div id="WraperAreaLarge">
<div class="PageNumberRight"><img src="../images/ICOsystem.gif" width="20" height="15" align="absmiddle">問卷管理</div>
<div class="whereYouAre">問卷系統 &gt;問卷清單</div>
<div class="clear"></div>


<table border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
  <tr> 
    <td class="headtext2">
    <div class="RecordData">紀錄<?php echo ($startRow_Template + 1) ?>到<?php echo $totalRows_Template ?>共 <?php echo $totalRows_Template ?>筆</div>
    </td>
  </tr>
  <tr> 
    <td valign="top" >
    
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr> 
          <td valign="top">&nbsp;</td>
        </tr>
        <!--do --><?php  $n=0; ?>
	<?php do { $n=$n+1; ?>
        <tr> 
          <td valign="top">
          
          <!--start-->
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" border:#CCCCCC 1px solid;">
  <tr <?php if($nn%2==0) { ?>style="background-color:#EFEFEF"<?php } else { ?>style="background-color:#FFFFFF"
  <?php } $nn=$nn+1; ?>
  >
    <td width="13%" nowrap>
	<div align="left" style="margin-left:10px; margin-right:10px;">
    No. <?php echo $n+$startRow_Template; ?></div>
    <div align="left" style="margin-left:10px; margin-right:10px;">
	<img src="<?php echo "images/ICOlog.gif"; ?>" width="48" height="51" vspace="5" border="0">
	<br>
	
	修改日期  <?php echo substr($row_Template['CreateDate'],0,10); ?> <br>
	修改時間  <?php echo substr($row_Template['CreateDate'],11,8); ?><br>
	開始日期  <?php echo substr($row_Template['StartDate'],0,10); ?> <br>
	結束日期  <?php echo substr($row_Template['EndDate'],0,10); ?> <br>
    <a href="SurveyDetail.php?Template=<?php echo $row_Template['T_FileName']; ?>">
	   </div>
    <div></div>    </td>
    <td width="61%">
    <div align="left" style="padding:5px;font-size:15px; color:#006699; font-weight:bold;">問卷主題:
    <a href="main.php?Page=8-3&Title=<?php echo $row_Template['T_Name']; ?>&Template=<?php echo $row_Template['T_FileName']; ?>"  style="font-size:15px; color:#006699;"><?php echo $row_Template['T_Name']; ?></a></div>
    <div align="left" style="padding:5px; border-top:solid 1px #CCCCCC;"><span style="font-weight:bold;">問卷說明：</span><?php echo $row_Template['T_Remark']; ?></div>    </td>
    <td width="26%" colspan="3" nowrap><div align="left">
    </div>
     <div align="left" style=" margin-top:5px">
<span style=" padding:5px">	 
<a href="../include/index.php?Page=S-1-1&Action=ins&Template=<?php echo $row_Template['T_FileName']; ?>&PostPage=S-3" target="_blank">預覽</a>	 
</span>
<span style=" padding:5px"><a href="main.php?Page=8-2&Action=ins&T_ID=<?php echo $row_Template['T_ID']; ?>">新增</a></span>
<span style=" padding:5px">
<img src="images/ICOedit.gif" alt="編輯題目" width="14" height="12" hspace="2" border="0" align="absmiddle"><a href="main.php?Page=8-3&Title=<?php echo $row_Template['T_Name']; ?>&Template=<?php echo $row_Template['T_FileName']; ?>">編輯題目</a><br></span>
<span style=" padding:5px"><img src="images/ICOdelete.gif" alt="刪除問卷含結果" width="15" height="12" hspace="2" border="0" align="absmiddle"><a href="main.php?Page=8-2&Action=del&T_ID=<? echo $row_Template['T_ID']; ?>">刪除</a></span> </div>
            <span style=" padding:5px"><img src="images/ICOedit.gif" alt="編輯問卷說明" width="14" height="12" hspace="2" border="0" align="absmiddle"><a href="main.php?Page=8-2&Action=edit&T_ID=<? echo $row_Template['T_ID']; ?>"> 編輯說明</a>
			<br> </span>
			
<span style=" padding:5px"><a href="main.php?Page=8-4&Template=<?php echo $row_Template['T_FileName']; ?>"><img src="images/ICOadd.gif" alt="問卷統計" width="14" height="15" border="0" align="absmiddle">問卷統計</a>
</span>			
			
            </td>
    </tr>
</table>
           <!--end-->
           </td>
           </tr><?php } while ($row_Template = mysqli_fetch_assoc($Template)); ?>
         <!--while -->
         <tr> 
          <td valign="top" class="bottomtext2">
		  <!--<div id="bottomSelect"> --><div style="width:90%; height:40px; margin:10px;">
		   <div style="float:left"><span><form name="form1">
              跳至第 
              <select name="menu1" onChange="MM_jumpMenu(this,0)">
                <option value='' selected></option>
                <?php for($I=0;$I<=$totalRows_Template/$maxRows_Template;$I++)
			  
			  { echo "<option value='";
			  printf("%s?pageNum_Template=%d%s", $currentPage,$I,$queryString_Template);
			   echo "'>".($I+1)." </option>"; 
			}
		?>
              </select>
              頁 </form></span></div>
		   <div style="float:right">
		   
		   <?php if ($pageNum_Template > 0) { // Show if not first page ?><span>
                  <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage, 0, $queryString_Template); ?>"><img src="../images/PageHome.gif" border=0></a> </span>
                  <?php } // Show if not first page ?>
			<?php if ($pageNum_Template > 0) { // Show if not first page ?><span>
                  <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage, max(0, $pageNum_Template - 1), $queryString_Template); ?>"><img src="../images/PagePrevious.gif" border=0></a> </span>
                  <?php } // Show if not first page ?> 
            <?php if ($pageNum_Template < $totalPages_Template) { // Show if not last page ?><span>
                  <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage, min($totalPages_Template, $pageNum_Template + 1), $queryString_Template); ?>"><img src="../images/PageNext.gif" border=0></a> </span>
                  <?php } // Show if not last page ?> 
                <?php if ($pageNum_Template < $totalPages_Template) { // Show if not last page ?><span>
                  <a href="<?php printf("%s?pageNum_Template=%d%s", $currentPage, $totalPages_Template, $queryString_Template); ?>"><img src="../images/PageEnd.gif" border=0></a> </span>
                <?php } // Show if not last page ?></div>
		  </div></td>
        </tr>    
            </table></td>
        </tr>
      </table>
</div>
<!--結束 -->
<DIV id="preview_div" style="DISPLAY: none; Z-INDEX: 110; POSITION: absolute"></DIV>

</body>
</html>
<?php
mysqli_free_result($Template);
mysqli_free_result($TGroup);
?>


