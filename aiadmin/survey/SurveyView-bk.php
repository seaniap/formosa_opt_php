<?php
//權限檢查不合格就 Logout 
if($_SESSION['Mode']=="") {
$FF_authFailedURL="../../Logout.php target=_TOP";
header("Location: $FF_authFailedURL");
exit;} 
?>
<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];


// 查詢 群組
$query_TGroup = "Select distinct `Group` as Category FROM ".$Table_ID."_Survey  WHERE Type<>'Field' OR Type is NULL ";
//echo $query_TGroup;
$TGroup = mysqli_query($MySQL,$query_TGroup) or die(mysqli_error($MySQL));
$row_TGroup = mysqli_fetch_assoc($TGroup);
$totalRows_TGroup = mysqli_num_rows($TGroup);

$maxRows_Member = 10;
$pageNum_Member = 0;
if (isset($_GET['pageNum_Member'])) {
  $pageNum_Member = $_GET['pageNum_Member'];
}
$startRow_Member = $pageNum_Member * $maxRows_Member;
$Where=$_GET['Where'];
$Sort=$_GET['Sort'];
$Group=$_GET['Group'];

if($Where<>"")
{ $WhereSQL="WHERE ".$Table_ID."_Survey.MemberID like '%$Where%' OR ".$Table_ID."_Survey.Name like '%$Where%' OR ".$Table_ID."_Survey.Email like '%$Where%' OR ".$Table_ID."_Survey.Group like '%$Where%' ";}
else
{$WhereSQL=" WHERE 1=1 ";}

if($_GET['h08']<>""){
$WhereSQL=$WhereSQL." and h08='".$_GET['h08']."'";	
}

if($_GET['h16']<>""){
$WhereSQL=$WhereSQL." and h16='".$_GET['h16']."'";	
}

if($_GET['h30']<>""){
$WhereSQL=$WhereSQL." and h30='".$_GET['h30']."'";	
}

if($Sort<>"")
{ $Sort=" order by ".$Sort." ";}
else
{ $Sort=" order by CreatedTime DESC ";}


if($_GET['Group']<>"")
{ $WhereSQL=$WhereSQL." AND `Group`='".$_GET['Group']."'" ;  }



$query_Member = "SELECT * FROM  ".$Table_ID."_Survey ".$WhereSQL." AND Name='".$_GET['Template']."'";
$query_Member =$query_Member.$Sort;
$query_limit_Member = sprintf("%s LIMIT %d, %d", $query_Member, $startRow_Member, $maxRows_Member);
//echo $query_limit_Member ;
$Member = mysqli_query($MySQL,$query_limit_Member) or die(mysqli_error($MySQL));
$row_Member = mysqli_fetch_assoc($Member);

if (isset($_GET['totalRows_Member'])) {
  $totalRows_Member = $_GET['totalRows_Member'];
} else {
  $all_Member = mysqli_query($MySQL,$query_Member);
  $totalRows_Member = mysqli_num_rows($all_Member);
}
$totalPages_Member = ceil($totalRows_Member/$maxRows_Member)-1;

$queryString_Member = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Member") == false && 
        stristr($param, "totalRows_Member") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Member = "&" . implode("&", $newParams);
  }
}
$queryString_Member = sprintf("&totalRows_Member=%d%s", $totalRows_Member, $queryString_Member);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);


//抓欄位名稱
$query_TemplateDetail = sprintf("SELECT * FROM ".$Table_ID."_TemplateDetail WHERE Template = %s AND FieldCheck<>'' ORDER BY Sq ASC", GetSQLValueString($_GET['Template'], "text"));
//echo $query_TemplateDetail;
$TemplateDetail = mysqli_query($MySQL,$query_TemplateDetail) or die(mysqli_error($MySQL));

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>聯絡我們</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/table.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(selObj,restore){ //v3.0
  eval("location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->


function ConfirmWindow(ss,ff){

answer = confirm(ss);
   if (answer){ 
      document.myform.ff.value=ff;
      document.myform.submit();		
}
}


</script>
<script type="text/javascript" src="../css/checkbox.js"></script>
</head>

<body>
<div id="WraperArea">
<div id="MainHeader">
    <div class="PageNumberRight"><img src="images/ICOgirl.gif" width="14" height="12" align="absmiddle">問卷統計</div>
    <div class="whereYouAre">聯絡我們</div>
</div>
<div class="clear"></div>
<a href="survey/SurveyExcelExport.php?Template=<? echo $_GET['Template']; ?>" target="_blank" class="d-btn mb-20">匯出名單</a> 
<div>

    <div>
  <form name="form1" action="main.php" method="post"> 
   
     <select class="form-control" id="h08" name="h08">
                    <option value="">詢問項目</option>
                    <option value="產品資訊" <? if($_GET['h08']=="產品資訊"){ echo "selected"; }   ?>>產品資訊</option>
                    <option value="專案規劃" <? if($_GET['h08']=="專案規劃"){ echo "selected"; }   ?>>專案規劃</option>
                    <option value="優惠活動" <? if($_GET['h08']=="優惠活動"){ echo "selected"; }   ?>>優惠活動</option>
                    <option value="售後服務" <? if($_GET['h08']=="售後服務"){ echo "selected"; }   ?>>售後服務</option>
                    <option value="其他問題" <? if($_GET['h08']=="其他問題"){ echo "selected"; }   ?>>其他問題</option>
                  </select>
                  <i></i>
                  
                  
         <select class="form-control" name="h16" id="h16" >
                     <option value="">設備類型</option>
                    <option value="家用空調/一對一" <? if($_GET['h16']=="家用空調/一對一"){ echo "selected"; }   ?>>家用空調/一對一</option>
                    <option value="家用空調/多聯式" <? if($_GET['h16']=="家用空調/多聯式"){ echo "selected"; }   ?>>家用空調/多聯式</option>
                    <option value="其他產品/空氣清淨機" <? if($_GET['h16']=="其他產品/空氣清淨機"){ echo "selected"; }   ?>>其他產品/空氣清淨機</option>
                   
                  </select>
                  <i></i>          
                  
                  
                  <select class="form-control" name="h30" id="h30" >
                     <option value="">狀態</option>
                    <option value="新增" <? if($_GET['h30']=="新增"){ echo "selected"; }   ?>>新增</option>
                    <option value="聯繫中" <? if($_GET['h30']=="聯繫中"){ echo "selected"; }   ?>>聯繫中</option>
                    <option value="結案" <? if($_GET['h30']=="結案"){ echo "selected"; }   ?>>結案</option>
                    
                   
                  </select> 
                  <i></i>      
                  
                  
                  
          <input type="button" class="b-btn" value="搜尋" onclick="form1.submit();" />       
                  
   <input type="hidden" name="Page" value="8-4" />
   <input type="hidden" name="Template" value="5754858805ab3a3d7b8231"  />
   </form>
   
    <div class="RecordData"> <?php echo ($startRow_Member + 1) ?>到<?php echo min($startRow_Member + $maxRows_Member, $totalRows_Member) ?> 共 <?php echo $totalRows_Member ?></div>
  <!--主要資料區 -->
  <form action="survey/SurveyFunction.php" name="myform" method="post">
  
  <table width="100%" border="0" cellpadding="2" cellspacing="1" class="datatable2">
    <tr> 
      <!-- <td colspan="30">&nbsp;</td> -->
    </tr>
    <tr> 
        <th width="60">&nbsp;日期 </th>
       <!-- <th>群組</th> -->
        <?  //顯示欄位名稱 
            while ($row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail)) { 	  
            ?>
            <th> 
              <? //echo $row_TemplateDetail['FieldID'].
               echo $row_TemplateDetail['FieldTitle']; ?>
                        <a style="display:none;" href="<? echo "SurveyView.php?Where=$Where&Sort=".$row_TemplateDetail['FieldID']." Desc" ;?> ">
                        <img style="display:none;" src="../images/SortIncrease.gif" width="12" height="10" border="0"></a>
                      
                        <a style="display:none;" href="<? echo "SurveyView.php?Where=$Where&Sort=".$row_TemplateDetail['FieldID']; ?> ">
                        <img style="display:none;" src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a>
                    </th>
            <? }  ?>		
          <th>操作</th>
    </tr>
    <?php do { ?>
    <tr class="FirstRow"> 
      <td>
      <!--
        <input name="SelectMember[]" type="checkbox" value="<? //echo $row_Member['KeyID']; ?>">
        -->
	 <? echo substr($row_Member['CreatedTime'],0,10); ?></td>
    <!--
      <td>
      <a href="../include/index.php?Page=S-1-1&Action=edit&Template=<? //echo $row_Member['Name']; ?>&KeyID=<? //echo $row_Member['KeyID']; ?>" target="_blank">  <? //echo $row_Member['Group']; ?></a>   
	</td> -->
	<? //顯示欄位內容
    $TemplateDetail = mysqli_query($MySQL,$query_TemplateDetail) or die(mysqli_error($MySQL));
    while ($row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail)) { 	 ?> 

    <td>
	
	
	<?php 
	
	if($row_TemplateDetail['FieldID']=="f01"){ ?> 
    <? if($row_Member[$row_TemplateDetail['FieldID']]<>""){ ?>
      <a href="../include/GetDBfile.php?KeyID=<? echo $row_Member[$row_TemplateDetail['FieldID']]; ?>">檔案下載</a>
    <? } else { ?>
 無檔案
    <? } ?>	
<?	}else{
	echo $row_Member[$row_TemplateDetail['FieldID']];
	}
	  ?>
    
    
    </td>
	<? }?>		
    <td> 
   <!--
    <a href="survey/SurveyFunction.php?ff=Del&SelectMember=<?php //echo $row_Member['KeyID']."&".$MM_keepURL.(($MM_keepURL!="")?"&":"") ?>">
	   <img src="images/ICOdelete.gif" alt="刪除" width="15" height="12" border="0"></a>
       -->
    
    
     <a class="border-btn btn-block" href="survey/SurveyFunction.php?ff=contact&SelectMember=<?php echo $row_Member['KeyID']."&".$MM_keepURL.(($MM_keepURL!="")?"&":"") ?>">
     聯繫中</a>
     <a class="r-btn btn-block" href="survey/SurveyFunction.php?ff=close&SelectMember=<?php echo $row_Member['KeyID']."&".$MM_keepURL.(($MM_keepURL!="")?"&":"") ?>">
      結案</a></td>
    
    
    
  
       
       
    </tr>    
    <?php } while ($row_Member = mysqli_fetch_assoc($Member)); ?>
  </table>
  
  
  <input name="ff" type="hidden" value="Del">
  <input name="KeepURL" type="hidden" value="<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":""); ?>">
<!--  
  <input name="SubmitMult" type="submit" class="button" value="刪除"   onclick="ConfirmWindow('確定要刪除會員?','Del');" >
-->
</form>
  
    <div class="bottomtext2">
    <!--下方跳業區 -->
     <div style="float:left;padding-left:2em;">
		<span>
        <form name="form2">跳至第
  <select class="form-control" name="menu1" onChange="MM_jumpMenu(this,0)">
  	<option value="" selected></option>
     <? for($I=0;$I<=$totalRows_Member/$maxRows_Member;$I++)
			  
			  { echo "<option value='";
			  printf("%s?pageNum_Member=%d%s", $currentPage,$I,$queryString_Member);
			   echo "'>".($I+1)." </option>"; 
			}
		?>	
  </select><i></i>頁
</form></span></div>
            <div class="page-mark">
            <?php if ($pageNum_Member > 0) { // Show if not first page ?><span>
            <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, 0, $queryString_Member); ?>"><!--<img src="images/PageHome.gif" alt="最前頁" border=0>--><i class="fas fa-angle-double-left"></i></a> 
            </span>
			<?php } // Show if not first page ?>
            <?php if ($pageNum_Member > 0) { // Show if not first page ?><span>
            <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, max(0, $pageNum_Member - 1), $queryString_Member); ?>"><!--<img src="images/PagePrevious.gif" alt="上一頁" border=0>--><i class="fas fa-angle-left"></i></a></span> 
            <?php } // Show if not first page ?>
            <?php if ($pageNum_Member < $totalPages_Member) { // Show if not last page ?><span>
            <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, min($totalPages_Member, $pageNum_Member + 1), $queryString_Member); ?>"><!--<img src="images/PageNext.gif" alt="下一頁" border=0>--><i class="fas fa-angle-right"></i></a></span> 
            <?php } // Show if not last page ?>
            <?php if ($pageNum_Member < $totalPages_Member) { // Show if not last page ?><span>
            <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, $totalPages_Member, $queryString_Member); ?>"><!--<img src="images/PageEnd.gif" alt="最終頁" border=0>--><i class="fas fa-angle-double-right"></i></a></span> 
            <?php } // Show if not last page ?>
            </div>
    <div>
            </div>


</div>
<!--=========================================================================================================== -->
</body>
</html>
<?php
mysqli_free_result($Member);
?>

