<?php
if (substr($_SERVER["PHP_SELF"],-8,8)<>"main.php") {
require_once('../../Connections/AdminMySQL.php'); //不使用 main.php
}


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO ".$_Table_ID."_TemplateDetail (KeyID, Template, FieldID, FieldTitle, Sq, Type, `Value`, MappingValue) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['KeyID'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['FieldID'], "text"),
                       GetSQLValueString($_POST['FieldTitle'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Type'], "text"),
                       GetSQLValueString($_POST['Value'], "text"),
                       GetSQLValueString($_POST['MappingValue'], "text"));

  
  $Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));

  $insertGoTo = "../win_close_re.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE ".$Table_ID."_TemplateDetail SET Template=%s, FieldID=%s, FieldTitle=%s, Sq=%s, Type=%s, `Value`=%s, MappingValue=%s, FieldCheck=%s, FieldCheckValue=%s   WHERE KeyID=%s",
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['FieldID'], "text"),
                       GetSQLValueString($_POST['FieldTitle'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Type'], "text"),
                       GetSQLValueString($_POST['Value'], "text"),
                       GetSQLValueString($_POST['MappingValue'], "text"),
                       GetSQLValueString($_POST['FieldCheck'], "text"),
					   GetSQLValueString($_POST['FieldCheckValue'], "text"),
					   GetSQLValueString($_POST['KeyID'], "text")
					   
					   );
					   
					   
 $insertSQL = sprintf("INSERT INTO ".$Table_ID."_TemplateDetail (KeyID, Template, FieldID, FieldTitle, Sq, Type, `Value`, MappingValue,FieldCheck,FieldCheckValue) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['KeyID'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['FieldID'], "text"),
                       GetSQLValueString($_POST['FieldTitle'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Type'], "text"),
                       GetSQLValueString($_POST['Value'], "text"),
                       GetSQLValueString($_POST['MappingValue'], "text"),
					   GetSQLValueString($_POST['FieldCheck'], "text"),
					   GetSQLValueString($_POST['FieldCheckValue'], "text")
					   );					   

  
  if ( $_POST[Action]=="edit")
  {$Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));
  //echo $updateSQL;
  }
  else
  {$Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
  //echo $insertSQL;
  }
  
 

  $updateGoTo = "../win_close_re.htm";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_TemplateDetail = "-1";
if (isset($_GET['KeyID'])) {
  $colname_TemplateDetail = (get_magic_quotes_gpc()) ? $_GET['KeyID'] : addslashes($_GET['KeyID']);
}

$query_TemplateDetail = sprintf("SELECT * FROM ".$Table_ID."_TemplateDetail WHERE KeyID = %s", GetSQLValueString($colname_TemplateDetail, "text"));
$TemplateDetail = mysqli_query($MySQL,$query_TemplateDetail) or die(mysqli_error($MySQL));
$row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail);
$totalRows_TemplateDetail = mysqli_num_rows($TemplateDetail);




if(  $_GET[Action]=="ins") //新增資料
{
 $row_TemplateDetail['KeyID']=uniqid(mt_rand());
 $row_TemplateDetail['Template']= $_GET['Template'];
 $row_TemplateDetail['Sq']=$row_TemplateDetail['Sq']+100;
}

// 抓目前已有欄位
$query_sql = sprintf("SELECT * FROM ".$Table_ID."_TemplateDetail WHERE Template = %s", GetSQLValueString( $row_TemplateDetail['Template'], "text"));
//echo $query_sql;
$data = mysqli_query($MySQL,$query_sql) or die(mysqli_error($MySQL));
$row_data = mysqli_fetch_assoc($data);

do{
if($row_data['FieldID']<>$row_TemplateDetail['FieldID'])
{$AllField=$AllField."-".$row_data['FieldID'];}
} while ($row_data = mysqli_fetch_assoc($data));
if($_GET[Action]=="ins"){$AllField=$AllField."-".$row_TemplateDetail['FieldID'];}
//echo $AllField;



?>



<p></p>
<div id="Box">
	<div class="indexhead"></div>
    <div class="indexContent"> 
<form method="post" name="form1" action="survey/SurveyDetailAdd.php">
   <table border="0" align="center" cellpadding="2" cellspacing="1" class="general">
        <tr valign="baseline">
      <th nowrap align="right">版型:</th>
      <td> <?php if(isset($_GET['Title'])){ echo rawurldecode($_GET['Title']); }else{echo $row_TemplateDetail['Template'];} ?></td>
    </tr>
    <tr valign="baseline">
      <th nowrap align="right">欄位代碼:</th>
      <td><select name="FieldID">
	  

<?php  if(!strchr($AllField,"h01"))	{ ?>
<option  value="h01" <?php if($row_TemplateDetail['FieldID']=="h01") {echo "selected";} ?> size="32"> h01  </option>
<?php } if(!strchr($AllField,"h02"))	{ ?>
<option  value="h02" <?php if($row_TemplateDetail['FieldID']=="h02") {echo "selected";} ?> size="32"> h02 </option>
<?php } if(!strchr($AllField,"h03"))	{ ?>
<option  value="h03" <?php if($row_TemplateDetail['FieldID']=="h03") {echo "selected";} ?> size="32"> h03 </option>
<?php } if(!strchr($AllField,"h04"))	{ ?>
<option  value="h04" <?php if($row_TemplateDetail['FieldID']=="h04") {echo "selected";} ?> size="32"> h04 </option>
<?php } if(!strchr($AllField,"h05"))	{ ?>
<option  value="h05" <?php if($row_TemplateDetail['FieldID']=="h05") {echo "selected";} ?> size="32"> h05 </option>
<?php } if(!strchr($AllField,"h06"))	{ ?>
<option  value="h06" <?php if($row_TemplateDetail['FieldID']=="h06") {echo "selected";} ?> size="32"> h06 </option>
<?php } if(!strchr($AllField,"h07"))	{ ?>
<option  value="h07" <?php if($row_TemplateDetail['FieldID']=="h07") {echo "selected";} ?> size="32"> h07 </option>
<?php } if(!strchr($AllField,"h08"))	{ ?>
<option  value="h08" <?php if($row_TemplateDetail['FieldID']=="h08") {echo "selected";} ?> size="32"> h08 </option>
<?php } if(!strchr($AllField,"h09"))	{ ?>
<option  value="h09" <?php if($row_TemplateDetail['FieldID']=="h09") {echo "selected";} ?> size="32"> h09 </option>
<?php } if(!strchr($AllField,"h10"))	{ ?>
<option  value="h10" <?php if($row_TemplateDetail['FieldID']=="h10") {echo "selected";} ?> size="32"> h10 </option>

<?php } if(!strchr($AllField,"h11"))	{ ?>
<option  value="h11" <?php if($row_TemplateDetail['FieldID']=="h11") {echo "selected";} ?> size="32"> h11  </option>
<?php } if(!strchr($AllField,"h12"))	{ ?>
<option  value="h12" <?php if($row_TemplateDetail['FieldID']=="h12") {echo "selected";} ?> size="32"> h12 </option>
<?php } if(!strchr($AllField,"h13"))	{ ?>
<option  value="h13" <?php if($row_TemplateDetail['FieldID']=="h13") {echo "selected";} ?> size="32"> h13 </option>
<?php } if(!strchr($AllField,"h14"))	{ ?>
<option  value="h14" <?php if($row_TemplateDetail['FieldID']=="h14") {echo "selected";} ?> size="32"> h14  </option>
<?php } if(!strchr($AllField,"h15"))	{ ?>
<option  value="h15" <?php if($row_TemplateDetail['FieldID']=="h15") {echo "selected";} ?> size="32"> h15 </option>
<?php } if(!strchr($AllField,"h16"))	{ ?>
<option  value="h16" <?php if($row_TemplateDetail['FieldID']=="h16") {echo "selected";} ?> size="32"> h16  </option>
<?php } if(!strchr($AllField,"h17"))	{ ?>
<option  value="h17" <?php if($row_TemplateDetail['FieldID']=="h17") {echo "selected";} ?> size="32"> h17  </option>
<?php } if(!strchr($AllField,"h18"))	{ ?>
<option  value="h18" <?php if($row_TemplateDetail['FieldID']=="h18") {echo "selected";} ?> size="32"> h18  </option>
<?php } if(!strchr($AllField,"h19"))	{ ?>
<option  value="h19" <?php if($row_TemplateDetail['FieldID']=="h19") {echo "selected";} ?> size="32"> h19 </option>
<?php } if(!strchr($AllField,"h20"))	{ ?>
<option  value="h20" <?php if($row_TemplateDetail['FieldID']=="h20") {echo "selected";} ?> size="32"> h20 </option>

<?php } if(!strchr($AllField,"h21"))	{ ?>
<option  value="h21" <?php if($row_TemplateDetail['FieldID']=="h21") {echo "selected";} ?> size="32"> h21 </option>
<?php } if(!strchr($AllField,"h22"))	{ ?>
<option  value="h22" <?php if($row_TemplateDetail['FieldID']=="h22") {echo "selected";} ?> size="32"> h22 </option>
<?php } if(!strchr($AllField,"h23"))	{ ?>
<option  value="h23" <?php if($row_TemplateDetail['FieldID']=="h23") {echo "selected";} ?> size="32"> h23 </option>
<?php } if(!strchr($AllField,"h24"))	{ ?>
<option  value="h24" <?php if($row_TemplateDetail['FieldID']=="h24") {echo "selected";} ?> size="32"> h24 </option>
<?php } if(!strchr($AllField,"h25"))	{ ?>
<option  value="h25" <?php if($row_TemplateDetail['FieldID']=="h25") {echo "selected";} ?> size="32"> h25 </option>
<?php } if(!strchr($AllField,"h26"))	{ ?>
<option  value="h26" <?php if($row_TemplateDetail['FieldID']=="h26") {echo "selected";} ?> size="32"> h26 </option>
<?php } if(!strchr($AllField,"h27"))	{ ?>
<option  value="h27" <?php if($row_TemplateDetail['FieldID']=="h27") {echo "selected";} ?> size="32"> h27 </option>
<?php } if(!strchr($AllField,"h28"))	{ ?>
<option  value="h28" <?php if($row_TemplateDetail['FieldID']=="h28") {echo "selected";} ?> size="32"> h28 </option>
<?php } if(!strchr($AllField,"h29"))	{ ?>
<option  value="h29" <?php if($row_TemplateDetail['FieldID']=="h29") {echo "selected";} ?> size="32"> h29 </option>
<?php } if(!strchr($AllField,"h30"))	{ ?>
<option  value="h30" <?php if($row_TemplateDetail['FieldID']=="h30") {echo "selected";} ?> size="32"> h30 </option>
<?php } if(!strchr($AllField,"h31"))	{ ?>
<option  value="h31" <?php if($row_TemplateDetail['FieldID']=="h31") {echo "selected";} ?> size="32"> h31 </option>
<?php } if(!strchr($AllField,"h32"))	{ ?>
<option  value="h32" <?php if($row_TemplateDetail['FieldID']=="h32") {echo "selected";} ?> size="32"> h32 </option>
<?php } if(!strchr($AllField,"h33"))	{ ?>
<option  value="h33" <?php if($row_TemplateDetail['FieldID']=="h33") {echo "selected";} ?> size="32"> h33 </option>
<?php } if(!strchr($AllField,"h34"))	{ ?>
<option  value="h34" <?php if($row_TemplateDetail['FieldID']=="h34") {echo "selected";} ?> size="32"> h34 </option>
<?php } if(!strchr($AllField,"h35"))	{ ?>
<option  value="h35" <?php if($row_TemplateDetail['FieldID']=="h35") {echo "selected";} ?> size="32"> h35 </option>
<?php  } if(!strchr($AllField,"h36"))	{ ?>
<option  value="h36" <?php if($row_TemplateDetail['FieldID']=="h36") {echo "selected";} ?> size="32"> h36 </option>
<?php  } if(!strchr($AllField,"h37"))	{ ?>
<option  value="h37" <?php if($row_TemplateDetail['FieldID']=="h37") {echo "selected";} ?> size="32"> h37 </option>
<?php } if(!strchr($AllField,"h38"))	{ ?>
<option  value="h38" <?php if($row_TemplateDetail['FieldID']=="h38") {echo "selected";} ?> size="32"> h38 </option>
<?php  } if(!strchr($AllField,"h39"))	{ ?>
<option  value="h39" <?php if($row_TemplateDetail['FieldID']=="h39") {echo "selected";} ?> size="32"> h39 </option>
<?php  } if(!strchr($AllField,"h40"))	{ ?>
<option  value="h40" <?php if($row_TemplateDetail['FieldID']=="h40") {echo "selected";} ?> size="32"> h40 </option>
<?php  } if(!strchr($AllField,"h41"))	{ ?>
<option  value="h41" <?php if($row_TemplateDetail['FieldID']=="h41") {echo "selected";} ?> size="32"> h41 </option>
<?php  } if(!strchr($AllField,"h42"))	{ ?>
<option  value="h42" <?php if($row_TemplateDetail['FieldID']=="h42") {echo "selected";} ?> size="32"> h42 </option>
<?php  } if(!strchr($AllField,"h43"))	{ ?>
<option  value="h43" <?php if($row_TemplateDetail['FieldID']=="h43") {echo "selected";} ?> size="32"> h43 </option>
<?php  } if(!strchr($AllField,"h44"))	{ ?>
<option  value="h44" <?php if($row_TemplateDetail['FieldID']=="h44") {echo "selected";} ?> size="32"> h44 </option>
<?php  } if(!strchr($AllField,"h45"))	{ ?>
<option  value="h45" <?php if($row_TemplateDetail['FieldID']=="h45") {echo "selected";} ?> size="32"> h45 </option>
<?php  } if(!strchr($AllField,"h46"))	{ ?>
<option  value="h46" <?php if($row_TemplateDetail['FieldID']=="h46") {echo "selected";} ?> size="32"> h46 </option>
<?php  } if(!strchr($AllField,"h47"))	{ ?>
<option  value="h47" <?php if($row_TemplateDetail['FieldID']=="h47") {echo "selected";} ?> size="32"> h47 </option>
<?php  } if(!strchr($AllField,"h48"))	{ ?>
<option  value="h48" <?php if($row_TemplateDetail['FieldID']=="h48") {echo "selected";} ?> size="32"> h48 </option>
<?php  } if(!strchr($AllField,"h49"))	{ ?>
<option  value="h49" <?php if($row_TemplateDetail['FieldID']=="h49") {echo "selected";} ?> size="32"> h49 </option>
<?php  } if(!strchr($AllField,"h50"))	{ ?>
<option  value="h50" <?php if($row_TemplateDetail['FieldID']=="h50") {echo "selected";} ?> size="32"> h50 </option>



<?php  } if(!strchr($AllField,"h51"))	{ ?>                                                                       
<option  value="h51" <?php if($row_TemplateDetail['FieldID']=="h51") {echo "selected";} ?> size="32"> h51 </option>
<?php  } if(!strchr($AllField,"h52"))	{ ?>                                                                       
<option  value="h52" <?php if($row_TemplateDetail['FieldID']=="h52") {echo "selected";} ?> size="32"> h52 </option>
<?php  } if(!strchr($AllField,"h53"))	{ ?>                                                                       
<option  value="h53" <?php if($row_TemplateDetail['FieldID']=="h53") {echo "selected";} ?> size="32"> h53 </option>
<?php  } if(!strchr($AllField,"h54"))	{ ?>                                                                       
<option  value="h54" <?php if($row_TemplateDetail['FieldID']=="h54") {echo "selected";} ?> size="32"> h54 </option>
<?php  } if(!strchr($AllField,"h55"))	{ ?>                                                                       
<option  value="h55" <?php if($row_TemplateDetail['FieldID']=="h55") {echo "selected";} ?> size="32"> h55 </option>
<?php  } if(!strchr($AllField,"h56"))	{ ?>                                                                       
<option  value="h56" <?php if($row_TemplateDetail['FieldID']=="h56") {echo "selected";} ?> size="32"> h56 </option>
<?php  } if(!strchr($AllField,"h57"))	{ ?>                                                                       
<option  value="h57" <?php if($row_TemplateDetail['FieldID']=="h57") {echo "selected";} ?> size="32"> h57 </option>
<?php  } if(!strchr($AllField,"h58"))	{ ?>                                                                       
<option  value="h58" <?php if($row_TemplateDetail['FieldID']=="h58") {echo "selected";} ?> size="32"> h58 </option>
<?php  } if(!strchr($AllField,"h59"))	{ ?>       
<option  value="h59" <?php if($row_TemplateDetail['FieldID']=="h59") {echo "selected";} ?> size="32"> h59 </option>

<?php  } if(!strchr($AllField,"h60"))	{ ?>
<option  value="h60" <?php if($row_TemplateDetail['FieldID']=="h60") {echo "selected";} ?> size="32"> h60 </option>
<?php  } if(!strchr($AllField,"h61"))	{ ?>
<option  value="h61" <?php if($row_TemplateDetail['FieldID']=="h61") {echo "selected";} ?> size="32"> h61 </option>
<?php  } if(!strchr($AllField,"h62"))	{ ?>
<option  value="h62" <?php if($row_TemplateDetail['FieldID']=="h62") {echo "selected";} ?> size="32"> h62 </option>
<?php  } if(!strchr($AllField,"h63"))	{ ?>
<option  value="h63" <?php if($row_TemplateDetail['FieldID']=="h63") {echo "selected";} ?> size="32"> h63 </option>
<?php  } if(!strchr($AllField,"h64"))	{ ?>
<option  value="h64" <?php if($row_TemplateDetail['FieldID']=="h64") {echo "selected";} ?> size="32"> h64 </option>
<?php  } if(!strchr($AllField,"h65"))	{ ?>
<option  value="h65" <?php if($row_TemplateDetail['FieldID']=="h65") {echo "selected";} ?> size="32"> h65 </option>
<?php  } if(!strchr($AllField,"h66"))	{ ?>
<option  value="h66" <?php if($row_TemplateDetail['FieldID']=="h66") {echo "selected";} ?> size="32"> h66 </option>
<?php  } if(!strchr($AllField,"h67"))	{ ?>
<option  value="h67" <?php if($row_TemplateDetail['FieldID']=="h67") {echo "selected";} ?> size="32"> h67 </option>
<?php  } if(!strchr($AllField,"h68"))	{ ?>
<option  value="h68" <?php if($row_TemplateDetail['FieldID']=="h68") {echo "selected";} ?> size="32"> h68 </option>
<?php  } if(!strchr($AllField,"h69"))	{ ?>           
<option  value="h69" <?php if($row_TemplateDetail['FieldID']=="h69") {echo "selected";} ?> size="32"> h69 </option>

<?php  } if(!strchr($AllField,"h70"))	{ ?>
<option  value="h70" <?php if($row_TemplateDetail['FieldID']=="h70") {echo "selected";} ?> size="32"> h70 </option>
<?php  } if(!strchr($AllField,"h71"))	{ ?>
<option  value="h71" <?php if($row_TemplateDetail['FieldID']=="h71") {echo "selected";} ?> size="32"> h71 </option>
<?php  } if(!strchr($AllField,"h72"))	{ ?>
<option  value="h72" <?php if($row_TemplateDetail['FieldID']=="h72") {echo "selected";} ?> size="32"> h72 </option>
<?php  } if(!strchr($AllField,"h73"))	{ ?>
<option  value="h73" <?php if($row_TemplateDetail['FieldID']=="h73") {echo "selected";} ?> size="32"> h73 </option>
<?php  } if(!strchr($AllField,"h74"))	{ ?>
<option  value="h74" <?php if($row_TemplateDetail['FieldID']=="h74") {echo "selected";} ?> size="32"> h74 </option>
<?php  } if(!strchr($AllField,"h75"))	{ ?>
<option  value="h75" <?php if($row_TemplateDetail['FieldID']=="h75") {echo "selected";} ?> size="32"> h75 </option>
<?php  } if(!strchr($AllField,"h76"))	{ ?>
<option  value="h76" <?php if($row_TemplateDetail['FieldID']=="h76") {echo "selected";} ?> size="32"> h76 </option>
<?php  } if(!strchr($AllField,"h77"))	{ ?>
<option  value="h77" <?php if($row_TemplateDetail['FieldID']=="h77") {echo "selected";} ?> size="32"> h77 </option>
<?php  } if(!strchr($AllField,"h78"))	{ ?>
<option  value="h78" <?php if($row_TemplateDetail['FieldID']=="h78") {echo "selected";} ?> size="32"> h78 </option>
<?php  } if(!strchr($AllField,"h79"))	{ ?>
<option  value="h79" <?php if($row_TemplateDetail['FieldID']=="h79") {echo "selected";} ?> size="32"> h79 </option>                                                     
<?php  } if(!strchr($AllField,"h80"))	{ ?>
<option  value="h80" <?php if($row_TemplateDetail['FieldID']=="h80") {echo "selected";} ?> size="32"> h80 </option>
<?php  } if(!strchr($AllField,"h81"))	{ ?>
<option  value="h81" <?php if($row_TemplateDetail['FieldID']=="h81") {echo "selected";} ?> size="32"> h81 </option>
<?php  } if(!strchr($AllField,"h82"))	{ ?>
<option  value="h82" <?php if($row_TemplateDetail['FieldID']=="h82") {echo "selected";} ?> size="32"> h82 </option>
<?php  } if(!strchr($AllField,"h83"))	{ ?>
<option  value="h83" <?php if($row_TemplateDetail['FieldID']=="h83") {echo "selected";} ?> size="32"> h83 </option>
<?php  } if(!strchr($AllField,"h84"))	{ ?>
<option  value="h84" <?php if($row_TemplateDetail['FieldID']=="h84") {echo "selected";} ?> size="32"> h84 </option>
<?php  } if(!strchr($AllField,"h85"))	{ ?>
<option  value="h85" <?php if($row_TemplateDetail['FieldID']=="h85") {echo "selected";} ?> size="32"> h85 </option>
<?php  } if(!strchr($AllField,"h86"))	{ ?>
<option  value="h86" <?php if($row_TemplateDetail['FieldID']=="h86") {echo "selected";} ?> size="32"> h86 </option>
<?php  } if(!strchr($AllField,"h87"))	{ ?>
<option  value="h87" <?php if($row_TemplateDetail['FieldID']=="h87") {echo "selected";} ?> size="32"> h87 </option>
<?php  } if(!strchr($AllField,"h88"))	{ ?>
<option  value="h88" <?php if($row_TemplateDetail['FieldID']=="h88") {echo "selected";} ?> size="32"> h88 </option>
<?php  } if(!strchr($AllField,"h89"))	{ ?>
<option  value="h89" <?php if($row_TemplateDetail['FieldID']=="h89") {echo "selected";} ?> size="32"> h89 </option>                                                     
<?php  } if(!strchr($AllField,"h90"))	{ ?>
<option  value="h90" <?php if($row_TemplateDetail['FieldID']=="h90") {echo "selected";} ?> size="32"> h90 </option>
<?php  } if(!strchr($AllField,"h91"))	{ ?>
<option  value="h91" <?php if($row_TemplateDetail['FieldID']=="h91") {echo "selected";} ?> size="32"> h91 </option>
<?php  } if(!strchr($AllField,"h92"))	{ ?>
<option  value="h92" <?php if($row_TemplateDetail['FieldID']=="h92") {echo "selected";} ?> size="32"> h92 </option>
<?php  } if(!strchr($AllField,"h93"))	{ ?>
<option  value="h93" <?php if($row_TemplateDetail['FieldID']=="h93") {echo "selected";} ?> size="32"> h93 </option>
<?php  } if(!strchr($AllField,"h94"))	{ ?>
<option  value="h94" <?php if($row_TemplateDetail['FieldID']=="h94") {echo "selected";} ?> size="32"> h94 </option>
<?php  } if(!strchr($AllField,"h95"))	{ ?>
<option  value="h95" <?php if($row_TemplateDetail['FieldID']=="h95") {echo "selected";} ?> size="32"> h95 </option>
<?php  } if(!strchr($AllField,"h96"))	{ ?>
<option  value="h96" <?php if($row_TemplateDetail['FieldID']=="h96") {echo "selected";} ?> size="32"> h96 </option>
<?php  } if(!strchr($AllField,"h97"))	{ ?>
<option  value="h97" <?php if($row_TemplateDetail['FieldID']=="h97") {echo "selected";} ?> size="32"> h97 </option>
<?php } if(!strchr($AllField,"h98"))	{ ?>
<option  value="h98" <?php if($row_TemplateDetail['FieldID']=="h98") {echo "selected";} ?> size="32"> h98 </option>
<?php } if(!strchr($AllField,"h99"))	{ ?>
<option  value="h99" <?php if($row_TemplateDetail['FieldID']=="h99") {echo "selected";} ?> size="32"> h99 </option>                                                     
<?php } if(!strchr($AllField,"h100"))	{ ?>
<option  value="h100" <?php if($row_TemplateDetail['FieldID']=="h100") {echo "selected";} ?> size="32"> h100 </option>
<?php } if(!strchr($AllField,"c01"))	{ ?>
<option  value="c01" <?php if($row_TemplateDetail['FieldID']=="c01") {echo "selected";} ?> size="32"> c01 </option>

<option  value="p01" <?php if($row_TemplateDetail['FieldID']=="p01") {echo "selected";} ?> size="32"> p01 </option>
<option  value="p02" <?php if($row_TemplateDetail['FieldID']=="p02") {echo "selected";} ?> size="32"> p02 </option>
<option  value="p03" <?php if($row_TemplateDetail['FieldID']=="p03") {echo "selected";} ?> size="32"> p03 </option>
<option  value="p04" <?php if($row_TemplateDetail['FieldID']=="p04") {echo "selected";} ?> size="32"> p04 </option>
<option  value="p05" <?php if($row_TemplateDetail['FieldID']=="p05") {echo "selected";} ?> size="32"> p05 </option>
<option  value="p06" <?php if($row_TemplateDetail['FieldID']=="p06") {echo "selected";} ?> size="32"> p06 </option>









<?php } if(!strchr($AllField,"f01"))	{ ?>
<option  value="f01" <?php if($row_TemplateDetail['FieldID']=="f01") {echo "selected";} ?> size="32"> f01 </option>
<?php } if(!strchr($AllField,"f02"))	{ ?>
<option  value="f02" <?php if($row_TemplateDetail['FieldID']=="f02") {echo "selected";} ?> size="32"> f02 </option>
<?php } if(!strchr($AllField,"f03"))	{ ?>
<option  value="f03" <?php if($row_TemplateDetail['FieldID']=="f03") {echo "selected";} ?> size="32"> f03 </option>
<?php } ?>

  </select>	  </td>
    </tr>
    <tr valign="baseline">
      <th nowrap align="right">欄位標題:</th>
      <td><input type="text" name="FieldTitle" value="<?php echo $row_TemplateDetail['FieldTitle']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <th nowrap align="right">順序:</th>
      <td><input type="text" name="Sq" value="<?php echo $row_TemplateDetail['Sq']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <th nowrap align="right">種類:</th>
      <td>
	  <select name="Type" >
<option  value="Text" <?php if($row_TemplateDetail['Type']=="Text") { echo "selected";} ?> > 文字-單行欄位 </option>
<option  value="TextUnit" <?php if($row_TemplateDetail['Type']=="TextUnit") { echo "selected";} ?> > 文字-單位欄位 </option>

<option  value="Radio" <?php if($row_TemplateDetail['Type']=="Radio") { echo "selected";} ?> > 單選-圓鈕</option>
<option  value="RadioBR" <?php if($row_TemplateDetail['Type']=="RadioBR") { echo "selected";} ?> > 單選-圓鈕換行</option>
<option  value="RadioText" <?php if($row_TemplateDetail['Type']=="RadioText") { echo "selected";} ?> > 單選-圓鈕+1個其他(文字)</option>
<option  value="RadioReason" <?php if($row_TemplateDetail['Type']=="RadioReason") { echo "selected";} ?> > 單選-圓鈕+原因欄位(文字)</option>
<option  value="Select" <?php if($row_TemplateDetail['Type']=="Select") { echo "selected";} ?> > 單選-下拉選單 </option>
<option  value="CheckBox" <?php if($row_TemplateDetail['Type']=="CheckBox") { echo "selected";} ?> > 複選-核取方塊鈕 </option>
<option  value="CheckBoxOther" <?php if($row_TemplateDetail['Type']=="CheckBoxOther") { echo "selected";} ?> > 複選-核取方塊鈕+1個其他(文字) </option>
<!-- <option  value="C" <?php //if($row_TemplateDetail['Type']=="C") { echo "selected";} ?> > 其他-html編輯器 </option> -->
<option  value="D" <?php if($row_TemplateDetail['Type']=="D") { echo "selected";} ?> > ckeditor </option>
<option  value="A" <?php if($row_TemplateDetail['Type']=="A") { echo "selected";} ?> > 文字-多行欄位</option>
<option  value="Country" <?php if($row_TemplateDetail['Type']=="Country") { echo "selected";} ?> >國碼</option>
<option  value="FileDB" <?php if($row_TemplateDetail['Type']=="FileDB") { echo "selected";} ?> > 檔案進資料庫 (限 f01-f03 欄位) </option>	
<option  value="Calendar" <?php if($row_TemplateDetail['Type']=="Calendar") { echo "selected";} ?> > 日曆 </option>
<option  value="D" <?php if($row_TemplateDetail['Type']=="D") { echo "selected";} ?> > 日期 </option> 
<option  value="P" <?php if($row_TemplateDetail['Type']=="P") { echo "selected";} ?> > 壓縮圖片 (限 p01-p06欄位) </option>	 
<option  value="Q" <?php if($row_TemplateDetail['Type']=="Q") { echo "selected";} ?> > 中文圖片(限 p01-p06欄位) </option> 
<option  value="eduTarget" <?php if($row_TemplateDetail['Type']=="eduTarget") { echo "selected";} ?> > 國際教育目標 </option> 
<option  value="eduArea" <?php if($row_TemplateDetail['Type']=="eduArea") { echo "selected";} ?> > 相關領域 </option> 
<option  value="SIEP" <?php if($row_TemplateDetail['Type']=="SIEP") { echo "selected";} ?> > SIEP分類 </option> 

<option  value="2-5" <?php if($row_TemplateDetail['Type']=="2-5") { echo "selected";} ?> > 2-5類別 </option> 




<!--
<option  value="P" <?php // if($row_TemplateDetail['Type']=="P") { echo "selected";} ?> > 其他-上傳圖片功能</option>	  
-->
	  </select>	  
      
      
      
      
      
      
      </td>
    </tr>
    <tr valign="baseline">
      <th nowrap align="right">儲存值(請用"|"分開):</th>
      <td><input type="text" name="Value" value="<?php echo $row_TemplateDetail['Value']; ?>" size="50"></td>
    </tr>
    <!--<tr valign="baseline">
      <th nowrap align="right">說明對照值(請用"-"分開):</th>
      <td><input type="text" name="MappingValue" value="<?php echo $row_TemplateDetail['MappingValue']; ?>" size="32"></td>
    </tr> -->
	 <tr valign="baseline">
      <th nowrap align="right">欄位檢查:</th>
      <td>
	  <input name="FieldCheck" type="radio" value="0" <?php if($row_TemplateDetail['FieldCheck']=="0") {echo "checked";} ?> checked >不檢查
	  <input name="FieldCheck" type="radio" value="1" <?php if($row_TemplateDetail['FieldCheck']=="1") {echo "checked";} ?> > 必填	  </td>
    </tr>
    <tr valign="baseline">
      <th nowrap align="right">欄位型態:</th>
      <td>
	  <input name="FieldCheckValue" type="radio" value="0" <?php if($row_TemplateDetail['FieldCheckValue']=="0") {echo "checked";} ?> checked >一般
	  <input name="FieldCheckValue" type="radio" value="N" <?php if($row_TemplateDetail['FieldCheckValue']=="N") {echo "checked";} ?> > 數字	  
       <input name="FieldCheckValue" type="radio" value="Email" <?php if($row_TemplateDetail['FieldCheckValue']=="Email") {echo "checked";} ?> > Email	  
        <input name="FieldCheckValue" type="radio" value="cellphone" <?php if($row_TemplateDetail['FieldCheckValue']=="cellphone") {echo "checked";} ?> > 手機	   
       
       
       
       
       </td>
    </tr> 
    
    <tr valign="baseline">
      <th nowrap align="right">&nbsp;</th>
      <td><input type="submit" value="更新" class="button"></td>
    </tr>
  </table>
   <input type="hidden" name="Action" value="<?php echo $_GET['Action']; ?>">
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="KeyID" value="<?php echo $row_TemplateDetail['KeyID']; ?>">
  <input type="hidden" name="Template" value="<?php echo $row_TemplateDetail['Template']; ?>" size="32">
</form>
</div>
    <div class="indexbottom"></div>
	</div>

<?php
mysqli_free_result($TemplateDetail);
?>
