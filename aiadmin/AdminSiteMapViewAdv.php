<?php


$currentPage = $_SERVER["PHP_SELF"];

$maxRows_sitemap = 150;
$pageNum_sitemap = 0;
if (isset($_GET['pageNum_sitemap'])) {
  $pageNum_sitemap = $_GET['pageNum_sitemap'];
}
$startRow_sitemap = $pageNum_sitemap * $maxRows_sitemap;

if($_GET['Display']<>"")
{ $WhereSQL=$WhereSQL." AND Display='".$_GET['Display']."'";}


if($_GET['Level']=="L1"){
$WhereSQL=$WhereSQL." AND length(Page)=1 ";}
if($_GET['Level']=="L2"){
$WhereSQL=$WhereSQL." AND length(Page)=3 ";}
if($_GET['Level']=="L3"){
$WhereSQL=$WhereSQL." AND length(Page)=5 ";}
if($_GET['Level']=="L4"){
$WhereSQL=$WhereSQL." AND length(Page)=7 ";}
//頁碼篩選
if($_GET['PP']<>"")
{ $WhereSQL=$WhereSQL." AND left(Page, 1)='".$_GET['PP']."'";}






$query_sitemap = "SELECT * FROM ".$Table_ID."_AdminSiteMap WHERE 1=1 ".$WhereSQL." ORDER BY Page ASC , Sq ASC";
//echo $query_sitemap ;
$query_limit_sitemap = sprintf("%s LIMIT %d, %d", $query_sitemap, $startRow_sitemap, $maxRows_sitemap);
$sitemap = mysqli_query($MySQL,$query_limit_sitemap) or die(mysqli_error($MySQL));
$row_sitemap = mysqli_fetch_assoc($sitemap);

if (isset($_GET['totalRows_sitemap'])) {
  $totalRows_sitemap = $_GET['totalRows_sitemap'];
} else {
  $all_sitemap = mysqli_query($MySQL,$query_sitemap);
  $totalRows_sitemap = mysqli_num_rows($all_sitemap);
}
$totalPages_sitemap = ceil($totalRows_sitemap/$maxRows_sitemap)-1;

$queryString_sitemap = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_sitemap") == false && 
        stristr($param, "totalRows_sitemap") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_sitemap = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_sitemap = sprintf("&totalRows_sitemap=%d%s", $totalRows_sitemap, $queryString_sitemap);


$query_pp = "SELECT * FROM ".$Table_ID."_AdminSiteMap WHERE length(Page)=1 ORDER BY Page ASC";
//echo $query_pp ;
$pp = mysqli_query($MySQL,$query_pp) or die(mysqli_error($MySQL));
$row_pp = mysqli_fetch_assoc($pp);

?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(selObj,restore){ //v3.0
  eval("location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->

function ConfirmDel(item1,KeyID){
 answer = confirm("確認刪除AdminSiteMap Page="+item1);
   if (answer){
     location='AdminSiteMapDelF.php?KeyID='+KeyID+'&<?php echo $_SERVER['QUERY_STRING'];?>'; }
	  	
}	  

</script>
<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">網站地圖</h3>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="main.php?Page=Z">網站管理</a></li>
          <li class="breadcrumb-item active" aria-current="page">網站地圖</li>
        </ol>
      </nav>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="form-group">
        <label>顯示開啟/關閉網頁</label>
        <?php  
          $queryString_sitemap = "";
          if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
              if (stristr($param, "Display") == false ) {
                array_push($newParams, $param);
              }
            }
            if (count($newParams) != 0) {
              $queryString_sitemap =  htmlentities(implode("&", $newParams));
            }
            }
        ?>
        <select class="custom-select" name="menu1" onChange="MM_jumpMenu(this,0)">
          <option value="<?php echo $currentPage."?".$queryString_sitemap; ?>" selected="<?php if($_GET['Display']=="") { echo "true"; }?>">顯示全部網頁</option> 
          <option value="<?php echo $currentPage."?".$queryString_sitemap."&Display=Y"; ?>" <?php if($_GET['Display']=="Y") {echo "selected";}?> >顯示開啟網頁</option>
          <option value="<?php echo $currentPage."?".$queryString_sitemap."&Display=N"; ?>" <?php if($_GET['Display']=="N") {echo "selected";}?> >顯示關閉網頁</option>    
        </select>
      </div>
    </div>
    <div class="col">
      <div class="form-group">
        <label>顯示層別</label>
        <?php 
          $queryString_sitemap = "";
          if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
              if (stristr($param, "Level") == false ) {
                array_push($newParams, $param);
              }
            }
            if (count($newParams) != 0) {
              $queryString_sitemap =  htmlentities(implode("&", $newParams));
            } 
            }
        ?>	 
        <select class="custom-select" name="menu1" onChange="MM_jumpMenu(this,0)"> 
            <option value="<?php echo $currentPage."?".$queryString_sitemap; ?>" <?php if($_GET['Level']=="") {echo "selected";}?> >全部展開  </option>     				      	
            <option value="<?php echo $currentPage."?".$queryString_sitemap."&Level=L1"; ?>" <?php if($_GET['Level']=="L1") {echo "selected";}?> >第一層</option>
            <option value="<?php echo $currentPage."?".$queryString_sitemap."&Level=L2"; ?>" <?php if($_GET['Level']=="L2") {echo "selected";}?> >第二層</option>       	
            <option value="<?php echo $currentPage."?".$queryString_sitemap."&Level=L3"; ?>" <?php if($_GET['Level']=="L3") {echo "selected";}?> >第三層</option>       	
            <option value="<?php echo $currentPage."?".$queryString_sitemap."&Level=L4"; ?>" <?php if($_GET['Level']=="L4") {echo "selected";}?> >第四層</option>                       
          </select> 
      </div>
    </div>
    <div class="col">
      <div class="form-group">
        <label>頁碼</label>
        <?php 
          $queryString_sitemap = "";
          if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
              if (stristr($param, "PP") == false )  {
                array_push($newParams, $param);
              }
            }
            if (count($newParams) != 0) {
              $queryString_sitemap = htmlentities(implode("&", $newParams));
            }
            }
        ?>	 
        <select class="custom-select" name="menu1" onChange="MM_jumpMenu(this,0)">
        <option value="main.php?<?php echo $queryString_sitemap; ?>" <?php if($_GET['PP']=="") {echo "selected";}?> >全部展開</option>
        <?php do { ?>
        <option value="main.php?PP=<?php echo $row_pp['Page']."&".$queryString_sitemap; ?>" <?php if($_GET['PP']==$row_pp['Page']) {echo "selected";}?> >
        <?php echo $row_pp['Page']; ?> </option>     	
        <?php } while ($row_pp = mysqli_fetch_assoc($pp)); ?>                                      
        </select> 
      </div>     
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col">
    <div class="RecordData">記錄 <?php echo ($startRow_sitemap + 1) ?> 到 <?php echo min($startRow_sitemap + $maxRows_sitemap, $totalRows_sitemap) ?> 共 <?php echo $totalRows_sitemap ?></div>
    </div>
  </div>
  <div class="row">
    <div class="col table-responsive">
    <table class="table" summary="網站地圖">
        <!-- <colgroup id="Page"></colgroup>
        <colgroup id="Sq"></colgroup>
        <colgroup id="Template"></colgroup>
        <colgroup id="Extend"></colgroup>
        <colgroup id="MainIcon"></colgroup>
        <colgroup id="Topic"></colgroup>
        <colgroup id="Title"></colgroup>
        <colgroup id="Security"></colgroup>
        <colgroup id="Display"></colgroup>
        <colgroup id="Function"></colgroup> -->
        <thead class="thead-dark">
          <tr> 
            <th scope="col" nowrap>頁碼</th>
            <th scope="col" nowrap>順序</th>
            <th scope="col" nowrap>版型</th>
            <th scope="col" nowrap>延伸版型</th>
            <th scope="col" nowrap>圖示</th>
            <th scope="col" nowrap>內容主旨</th>
            <th scope="col" nowrap>網頁標題</th>
            <th scope="col" nowrap>編輯權限</th>
            <th scope="col" nowrap>網頁開啟</th>
            <th scope="col" nowrap class="func">功能</th>
        </tr>
        </thead>
        <tbody>
        <?php do { ?> 
        <tr> 
          <td>&nbsp;<?php echo $row_sitemap['Page']; ?></td>
          <td>&nbsp;<?php echo $row_sitemap['Sq']; ?></td>
          <td>&nbsp;<?php echo $row_sitemap['Template']; ?></td>
          <td>&nbsp;<?php echo $row_sitemap['Extend']; ?></td>
          <td>&nbsp;<?php echo $row_sitemap['MainIcon']; ?></td>
          <td>&nbsp;<?php echo $row_sitemap['Topic']; ?></td>
          <td>&nbsp;<?php echo $row_sitemap['Title']; ?></td>
          <td>&nbsp;<?php echo $row_sitemap['Security']; ?></td>
          <td>&nbsp;<?php echo $row_sitemap['Display']; ?></td>
          <td>&nbsp;
              <a class="btn btn-outline-primary btn-sm" href="main.php?GoPage=Z-3-1&Action=edit&<?php echo $_SERVER['QUERY_STRING']."&KeyPage=".$row_sitemap['Page']; ?>">
              <i class="bi bi-check-circle-fill" role="img" aria-label="edit"></i> 編輯</a>

              <a class="btn btn-outline-primary btn-sm" href="main.php?GoPage=Z-3-1&Action=ins&<?php echo $_SERVER['QUERY_STRING']."&KeyPage=".$row_sitemap['Page']; ?>">
              <i class="bi bi-plus-circle-fill" role="img" aria-label="plus"></i> 新增</a>
              <button class="btn btn-outline-danger btn-sm" onClick="ConfirmDel('<?php echo $row_sitemap['Page']; ?>','<?php echo $row_sitemap['KeyID']; ?>');"><i class="bi bi-x-circle-fill"></i> 刪除</button>  
              <!-- <img src="images/ICOdelete.gif" alt="刪除" width="14" height="12" border="0" align="Edit" onClick="ConfirmDel('<?php // echo $row_sitemap['Page']; ?>','<?php // echo $row_sitemap['KeyID']; ?>');"> -->         
          </td>
        </tr>
        <?php } while ($row_sitemap = mysqli_fetch_assoc($sitemap)); ?> 
        </tbody>
      </table>

    </div>
  </div>
  <div class="row">
  <div class="col d-flex justify-content-center">
    <!-- paginations -->
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <?php if ($pageNum_sitemap > 0) { // Show if not first page ?>
        <li class="page-item">
          <a class="page-link" aria-label="Previous" href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, 0, $queryString_sitemap); ?>">
          第一頁</a>
        </li>
        <?php } // Show if not first page ?>

        <?php if ($pageNum_sitemap > 0) { // Show if not first page ?>
          <li class="page-item">
              <a class="page-link" href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, max(0, $pageNum_sitemap - 1), $queryString_sitemap); ?>">上一頁</a>
          </li>
        <?php } // Show if not first page ?>

        <?php if ($pageNum_sitemap < $totalPages_sitemap) { // Show if not last page ?>
          <li class="page-item">
              <a class="page-link" href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, min($totalPages_sitemap, $pageNum_sitemap + 1), $queryString_sitemap); ?>">下一頁</a>
          </li>
        <?php } // Show if not last page ?>
        <?php if ($pageNum_sitemap < $totalPages_sitemap) { // Show if not last page ?>
          <li class="page-item">
              <a class="page-link" href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, $totalPages_sitemap, $queryString_sitemap); ?>">最後一頁</a>
          </li>
        <?php } // Show if not last page ?>
      </ul>
    </nav>
    <!--end paginations -->
  </div>
</div>
</div>
<!-- end -->
<?php
mysqli_free_result($sitemap);
?>
