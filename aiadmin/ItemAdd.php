<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php
echo "Hello<br>";
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
  //echo "editFormAction == ".$editFormAction;
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO ".$Table_ID."_Configure (KeyID, item, value,MappingValue,remark) VALUES (%s, %s, %s,%s,%s)",
                      GetSQLValueString($_POST['KeyID'], "text"),
                      GetSQLValueString($_POST['item'], "text"),
					            GetSQLValueString($_POST['value'], "text"),
					            GetSQLValueString($_POST['MappingValue'], "text"),
                      GetSQLValueString($_POST['remark'], "text"));
  //echo "insertSQL == ".$insertSQL;

  $Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
  $insertGoTo = $RootLevel."/aiadmin/main.php?Page=Z-2";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

 ?>

<!-- 開始 -->
<div class="container">

  <div class="row">
    <div class="col">
      <h3 class="mt-3">參數設定</h3>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="main.php?Page=Z">網站管理</a></li>
        <li class="breadcrumb-item"><a href="main.php?Page=Z-2">參數設定</a></li>
        <li class="breadcrumb-item active" aria-current="page">新增參數</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <!-- form start -->
      <!-- <form method="post" name="form1" action="<?php //echo $RootLevel; ?>/aiadmin/main.php?Page=Z-2-1"> -->
      <form method="post" name="form1" action="<?php echo $RootLevel; ?>/aiadmin/ItemAdd.php">      
        <div class="form-group d-flex justify-content-center">
          <input name="sbutton" type="submit" class="btn btn-primary" value="新增參數">
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">參數</label>
          <div class="col-sm-10">
            <input type="text" name="item" value="" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">設定值</label>
          <div class="col-sm-10">
            <input type="text" name="value" value="" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">轉換值</label>
          <div class="col-sm-10">
            <input type="text" name="MappingValue" value="" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">說明</label>
          <div class="col-sm-10">
            <textarea name="remark" cols="41" class="form-control"></textarea>
          </div>
        </div>               
        <input type="hidden" name="KeyID" value="<?php echo uniqid(mt_rand());  ?>">
        <input type="hidden" name="MM_insert" value="form1">
      </form>
      <!-- form end -->
    </div>
  </div>
</div>
<!-- 結束 -->


<!-- <form method="post" name="form1" action="<?php echo $RootLevel; ?>/aiadmin/ItemAdd.php">
  <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr> 
      <td width="22%" valign="bottom"><div align="left"></div></td>
      <td width="60%">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td> <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr> 
            <td class="PageNumberRight"> 
              <div align="left"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td height="21" class="headtext"> 
                      <div align="left"><img src="images/ICOedit.gif" width="14" height="12" border="0" align="absmiddle">新增使用者 
                      </div></td>
                  </tr>
                </table>
              </div></td>
          </tr>
        </table></td>
      <td>&nbsp;</td>
      <td width="18%" valign="bottom">&nbsp; </td>
    </tr>
  </table>
  <table width="85%" border="0" align="center" cellpadding="2" cellspacing="1" class="general">
    <tr valign="baseline" class="TableTagRight"> 
      <td height="28" colspan="2" align="right" nowrap>&nbsp;</td>
    </tr>
    <tr valign="baseline" class="FirstRow"> 
      <td width="98" height="28" align="right" nowrap>參數</td>
      <td width="530" valign="middle"> <div align="left"> 
          <input name="item" type="text" class="textbox" value="" size="32">
        </div></td>
    </tr>
    <tr valign="baseline" class="SecondRow"> 
      <td height="28" align="right" nowrap>設定值</td>
      <td valign="middle"> <div align="left"> 
          <input name="value" type="text" class="textbox" value="" size="50">
        </div></td>
    </tr>
	<tr valign="baseline" class="SecondRow"> 
      <td height="28" align="right" nowrap>轉換值</td>
      <td valign="middle"> <div align="left"> 
          <input name="MappingValue" type="text" class="textbox" value="" size="50">
        </div></td>
    </tr>
	
    <tr valign="baseline" class="FirstRow"> 
      <td height="53" align="right" valign="middle" nowrap>說明</td>
      <td valign="baseline"><div align="left">
          <textarea name="remark" cols="41"></textarea>
        </div>
        <table width="79%">
          
        </table></tr>
    <tr valign="baseline" class="SecondRow"> 
      <td height="29" align="right" nowrap>&nbsp;</td>
      <td> <input type="submit" class="button" value="新增參數"></td>
    </tr>
    <tr valign="baseline" class="TableTagRight"> 
      <td colspan="2" align="right" nowrap>&nbsp;</td>
    </tr>
  </table>
  <input type="hidden" name="KeyID" value="<? echo uniqid(mt_rand());  ?>">
  <input type="hidden" name="MM_insert" value="form1">
</form> -->

