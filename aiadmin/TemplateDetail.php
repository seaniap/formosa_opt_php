<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$colname_TemplateDetail = "-1";
if (isset($_GET['Template'])) {
  $colname_TemplateDetail = (get_magic_quotes_gpc()) ? $_GET['Template'] : addslashes($_GET['Template']);
}

$query_TemplateDetail = sprintf("SELECT * FROM ".$Table_ID."_TemplateDetail WHERE Template = %s ORDER BY Sq ASC", GetSQLValueString($colname_TemplateDetail, "text"));
$TemplateDetail = mysqli_query($MySQL,$query_TemplateDetail) or die(mysqli_error($MySQL));
$row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail);
$totalRows_TemplateDetail = mysqli_num_rows($TemplateDetail);
?>


<script language="javascript" >
function ConfirmDel(item1,KeyID,Template){
 answer = confirm("確認刪除參數"+item1);
   if (answer){
     location='TemplateDetailDelF.php?KeyID='+KeyID+'&Template='+Template; }	
}	  
</script> 


<?php  //判斷是否使用 MSuper 
//if($totalRows_TemplateDetail==0){
//echo "此版型(".$_GET['Template'].")使用單獨之M版<br>";

//} else {
?>
<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">版型管理</h3>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">系統管理</a></li>
        <li class="breadcrumb-item"><a href="main.php?Page=z-4">版型管理</a></li>
        <li class="breadcrumb-item active" aria-current="page">版型欄位內容</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col">
    <h5 class="my-3">版型: <?php echo $_GET['Template']; ?></h5>
    </div>
  </div>

  <div class="row">
    <div class="col table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th nowrap>欄位</th>
          <th nowrap>標題</th>
          <th nowrap>順序</th>
          <th nowrap>型態</th>
          <th nowrap>儲存值</th>
          <th nowrap>說明對照值</th>
          <th nowrap>功能</th>
        </tr>
      </thead>
      <tbody>
      <?php do { ?>
        <tr>
          <td><?php echo $row_TemplateDetail['FieldID']; ?></td>
          <td><?php echo $row_TemplateDetail['FieldTitle']; ?></td>
          <td><?php echo $row_TemplateDetail['Sq']; ?></td>
          <td><?php echo $row_TemplateDetail['Type']; ?></td>
          <td><?php echo $row_TemplateDetail['Value']; ?></td>
          <td><?php echo $row_TemplateDetail['MappingValue']; ?></td>
          <td>   
            <a class="btn btn-outline-primary" href="main.php?Page=Z-4-2-1&Action=ins&Template=<?php echo $_GET['Template']; ?>&KeyID=<?php echo $row_TemplateDetail['KeyID']; ?>" target="_blank" >插入</a>
            <!-- <img src="images/ICOdelete.gif" alt="刪除" width="15" height="12" border="0" onClick="ConfirmDel('<?php //echo $row_TemplateDetail['FieldID']; ?>','<?php //echo $row_TemplateDetail['KeyID']; ?>','<?php //echo $row_TemplateDetail['Template']; ?>');"> -->      
            <a class="btn btn-outline-primary" href="main.php?Page=Z-4-2-1&Action=edit&Template=<?php echo $row_TemplateDetail['Template']; ?>&KeyID=<?php echo $row_TemplateDetail['KeyID']; ?>" target="_blank" >修改</a>
            <button class="btn btn-outline-danger" onClick="ConfirmDel('<?php echo $row_TemplateDetail['FieldID']; ?>','<?php echo $row_TemplateDetail['KeyID']; ?>','<?php echo $row_TemplateDetail['Template']; ?>');">刪除</button>
          </td>
        </tr>
	    <?php } while ($row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail)); ?>
      </tbody>
    </table>

    </div>
  </div>
</div>
<!-- end -->
<?php
mysqli_free_result($TemplateDetail);
?>
