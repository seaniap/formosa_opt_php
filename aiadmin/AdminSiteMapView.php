<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_sitemap = 20;
$pageNum_sitemap = 0;
if (isset($_GET['pageNum_sitemap'])) {
  $pageNum_sitemap = $_GET['pageNum_sitemap'];
}
$startRow_sitemap = $pageNum_sitemap * $maxRows_sitemap;


$query_sitemap = "SELECT * FROM ".$Table_ID."_AdminSiteMap ORDER BY Page ASC , Sq ASC";
$query_limit_sitemap = sprintf("%s LIMIT %d, %d", $query_sitemap, $startRow_sitemap, $maxRows_sitemap);
$sitemap = mysqli_query($MySQL,$query_limit_sitemap) or die(mysqli_error($MySQL));
$row_sitemap = mysqli_fetch_assoc($sitemap);

if (isset($_GET['totalRows_sitemap'])) {
  $totalRows_sitemap = $_GET['totalRows_sitemap'];
} else {
  $all_sitemap = mysqli_query($MySQL,$query_sitemap);
  $totalRows_sitemap = mysqli_num_rows($all_sitemap);
}
$totalPages_sitemap = ceil($totalRows_sitemap/$maxRows_sitemap)-1;

$queryString_sitemap = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_sitemap") == false && 
        stristr($param, "totalRows_sitemap") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_sitemap = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_sitemap = sprintf("&totalRows_sitemap=%d%s", $totalRows_sitemap, $queryString_sitemap);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>網站地圖</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/table.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="MainHeader">
    <div class="PageNumberRight"><img src="images/ICOedit.gif" width="14" height="12" align="absmiddle">網站地圖</div>
    <div class="whereYouAre">網站管理 &gt; 網站地圖</div>
</div>
<div class="clear"></div>

<!--22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222 -->
<div class="datatable">
    <div class="headtext2">
    <div class="RecordData">記錄 <?php echo ($startRow_sitemap + 1) ?> 到 <?php echo min($startRow_sitemap + $maxRows_sitemap, $totalRows_sitemap) ?> 共 <?php echo $totalRows_sitemap ?></div>
    </div>
    <div>
    <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatableInside">
        <tr> 
        <th class="headtext"> 頁碼</th>
        <th class="headtext"> 順序</th>
        <th class="headtext"> 版型</th>
        <th class="headtext">延伸版型</th>
        <th class="headtext">顯示筆數</th>
        <th class="headtext">內容主旨</th>
        <th class="headtext">網頁標題</th>
        <th class="headtext">編輯權限</th>
        <th class="headtext">功能</th>
      </tr>
	 <?php do { ?> 
      <tr class="FirstRow"> 
        <td>&nbsp;<?php echo $row_sitemap['Page']; ?></td>
        <td>&nbsp;<?php echo $row_sitemap['Sq']; ?></td>
        <td>&nbsp;<?php echo $row_sitemap['Template']; ?></td>
        <td>&nbsp;<?php echo $row_sitemap['Extend']; ?></td>
        <td>&nbsp;<?php echo $row_sitemap['MaxRow']; ?></td>
        <td>&nbsp;<?php echo $row_sitemap['Topic']; ?></td>
        <td>&nbsp;<?php echo $row_sitemap['Title']; ?></td>
        <td>&nbsp;<?php echo $row_sitemap['Security']; ?></td>
        <td>&nbsp;<a href="SiteMapEdit.php?Page=<?php echo $row_sitemap['Page']; ?>"><img src="images/ICOedit.gif" alt="編輯" width="14" height="12" border="0" align="Edit">編輯</a></td>
      </tr>
   <?php } while ($row_sitemap = mysqli_fetch_assoc($sitemap)); ?> 
    </table>
    </div>
    <div class="bottomtext2">
          <span><?php if ($pageNum_sitemap > 0) { // Show if not first page ?>
              <a href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, 0, $queryString_sitemap); ?>">第一頁</a>
              <?php } // Show if not first page ?>
          </span>
          <span><?php if ($pageNum_sitemap > 0) { // Show if not first page ?>
              <a href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, max(0, $pageNum_sitemap - 1), $queryString_sitemap); ?>">上一頁</a>
              <?php } // Show if not first page ?>
        </span>
        <span><?php if ($pageNum_sitemap < $totalPages_sitemap) { // Show if not last page ?>
              <a href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, min($totalPages_sitemap, $pageNum_sitemap + 1), $queryString_sitemap); ?>">下一頁</a>
              <?php } // Show if not last page ?>
        </span>
        <span><?php if ($pageNum_sitemap < $totalPages_sitemap) { // Show if not last page ?>
              <a href="<?php printf("%s?pageNum_sitemap=%d%s", $currentPage, $totalPages_sitemap, $queryString_sitemap); ?>">最後一頁</a>
              <?php } // Show if not last page ?>
        </span>
        <div class="clear"></div>
   </div>
</div>

<!--22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222 -->



</body>
</html>
<?php
mysqli_free_result($sitemap);
?>
