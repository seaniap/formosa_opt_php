<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf(
    "UPDATE " . $Table_ID . "_UserLogin SET UserName=%s,Password=%s, `Level`=%s, CreatedTime=%s, Remark=%s WHERE KeyID=%s ",
    GetSQLValueString($_POST['UserName'], "text"),
    GetSQLValueString($_POST['Password'], "text"),
    GetSQLValueString($_POST['Level'], "text"),
    GetSQLValueString(date("Y-m-d H:i:s"), "date"),
    GetSQLValueString($_POST['Remark'], "text"),
    GetSQLValueString($_POST['KeyID'], "text")
  );

  //echo $updateSQL;					   

  $Result1 = mysqli_query($MySQL, $updateSQL) or die(mysqli_error($MySQL));



  //寫更新 log
  $userIP = $_SERVER['REMOTE_ADDR']; //收集瀏覽者的IP
  //$Domain=gethostbyaddr($userIP);
  $LogDate = Date("Y-m-j H:i:s");
  $sql = "insert into " . $Table_ID . "_WebLog Values ('',";
  $sql = $sql . "'$LogDate','" . $_POST['KeyID'] . "  " . $_POST['UserName'] . "-" . $_POST['Level'] . "','" . '編輯使用者' . "','" . $_SESSION['UserName'] . "-" . $_SESSION['Mode'] . "','$userIP','線上更新','" . $_SESSION['GuestName'] . $_POST['Template'] . "-" . 'edit' . "',0)";
  //echo $sql;
  mysqli_query($MySQL, $sql);



  // $updateGoTo = "UserView.php";
  $updateGoTo = "main.php?Page=1-1";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_UserLogin = "1";
if (isset($_GET['KeyID'])) {
  $colname_UserLogin = (get_magic_quotes_gpc()) ? $_GET['KeyID'] : addslashes($_GET['KeyID']);
}

$query_UserLogin = sprintf("SELECT * FROM " . $Table_ID . "_UserLogin WHERE KeyID = '%s'", $colname_UserLogin);
$UserLogin = mysqli_query($MySQL, $query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);
?>

<!-- start -->
<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="mt-3 caption">編輯使用者設定</h3>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="main.php?Page=1">系統管理</a></li>
          <li class="breadcrumb-item"><a href="main.php?Page=1-1">使用者設定</a></li>
          <li class="breadcrumb-item active" aria-current="page">編輯使用者設定</li>
        </ol>
      </nav>
    </div>
  </div>
  <!-- form start -->
  <div class="row">
    <div class="col mt-3">
      <form method="post" name="form1" action="UserUpdate.php">
        <h5>編輯使用者</h5>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="AdminUserName">使用者名稱</label>
            <input type="text" id="AdminUserName" class="form-control" name="UserName" value="<?php echo $row_UserLogin['UserName']; ?>" tabindex="1">
          </div>
          <div class="form-group col-md-6">
            <label for="AdminPassword">使用者密碼</label>
            <input type="text" id="AdminPassword" class="form-control" name="Password" value="<?php echo $row_UserLogin['Password']; ?>" tabindex="2">
          </div>
        </div>

        <div class="form-group row col-12">
          <label for="AdminLevel" class="col-sm-3 col-form-label">使用者類型</label>
          <div class="col-sm-9">
            <div class="custom-control custom-radio custom-control-inline">
              <input class="custom-control-input" type="radio" name="Level" id="AdminLevel" value="Admin" tabindex="3" <?php if (!(strcmp($row_UserLogin['Level'], "Admin"))) {
                                                                                                                          echo "CHECKED";
                                                                                                                        } ?>>
              <label class="custom-control-label" for="AdminLevel">Admin系統管理者</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="Level" id="AdminLevel2" class="custom-control-input" value="User" tabindex="4" <?php if (!(strcmp($row_UserLogin['Level'], "User"))) {
                                                                                                                          echo "CHECKED";
                                                                                                                        } ?>>
              <label class="custom-control-label" for="AdminLevel2">Userㄧ般使用者</label>
            </div>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="AdminRemark">使用者建立來源</label>
            <input type="text" id="AdminRemark" class="form-control" name="Remark" value="<?php echo $row_UserLogin['Remark']; ?>" tabindex="5">
          </div>
        </div>

        <div class="form-group d-flex justify-content-center">
          <input type="submit" class="btn btn-primary" value="更新記錄">
          <input type="hidden" name="MM_update" value="form1">
          <input type="hidden" name="KeyID" value="<?php echo $_GET[KeyID]; ?>">
        </div>
      </form>
    </div>

  </div>
  <!-- end form -->
</div>
<!-- end -->

<?php
mysqli_free_result($UserLogin);
?>