<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}
//debug
//echo "_POST == ".$_POST["MM_update"];

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE ".$Table_ID."_Configure SET item=%s, value=%s , MappingValue=%s , remark=%s WHERE KeyID=%s",
                       GetSQLValueString($_POST['item'], "text"),
                       GetSQLValueString($_POST['value'], "text"),
					   GetSQLValueString($_POST['MappingValue'], "text"),
					   GetSQLValueString($_POST['remark'], "text"),
                       GetSQLValueString($_POST['KeyID'], "text"));

//echo $updateSQL; 

$Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));

  $updateGoTo = $RootLevel."/aiadmin/main.php?Page=Z-2";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

			   
$colname_UserLogin = "1";
if (isset($_GET['KeyID'])) {
  $colname_UserLogin = (get_magic_quotes_gpc()) ? $_GET['KeyID'] : addslashes($_GET['KeyID']);
}

$query_UserLogin = sprintf("SELECT * FROM ".$Table_ID."_Configure WHERE KeyID = '%s'", $colname_UserLogin);
$UserLogin = mysqli_query($MySQL,$query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);
?>

<div class="container">
  <div class="row">
    <div class="col">
        <h3 class="mt-3 caption">參數設定</h3>
      </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="main.php?Page=Z">網站管理</a></li>
        <li class="breadcrumb-item"><a href="main.php?Page=Z-2">參數設定</a></li>
        <li class="breadcrumb-item active" aria-current="page">編輯參數</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <!-- form start -->
      <form method="post" name="form1" action="<?php echo $RootLevel; ?>/aiadmin/ItemUpdate.php">
        <div class="form-group d-flex justify-content-center">
          <input name="sbutton" type="submit" class="btn btn-primary" value="更新記錄">
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">參數</label>
          <div class="col-sm-10">
            <input type="text" name="item" value="<?php echo $row_UserLogin['item']; ?>" class="form-control" readonly>
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">設定值</label>
          <div class="col-sm-10">
            <input type="text" name="value" value="<?php echo $row_UserLogin['value']; ?>" class="form-control">
            <small class="form-text text-muted">(請用 "-" 分開多重值 )</small>
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">轉換值</label>
          <div class="col-sm-10">
            <input type="text" name="MappingValue" value="<?php echo $row_UserLogin['MappingValue']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">說明</label>
          <div class="col-sm-10">
            <textarea name="remark" cols="41" class="form-control"><?php echo $row_UserLogin['remark']; ?></textarea>
          </div>
        </div> 
        <input type="hidden" name="KeyID" value="<?php echo $row_UserLogin['KeyID']; ?>">
        <input type="hidden" name="item" value="<?php echo $row_UserLogin['item']; ?>">
        <input type="hidden" name="MM_update" value="form1">
      </form>
      <!-- form end -->

      <!-- 開始 -->
        <!-- <form method="post" name="form1" action="<? echo $RootLevel; ?>/aiadmin/ItemUpdate.php"> -->
          <!-- <table width="85%" border="0" align="center" cellpadding="2" cellspacing="1" class="general"> -->
            <!-- <tr valign="baseline" class="FirstRow"> 
              <td width="98" height="28" align="right" nowrap>參數</td>
              <td width="530" valign="middle"> <div align="left"> 
                  <?php //echo $row_UserLogin['item']; ?>
                </div></td>
            </tr> -->
            <!-- <tr valign="baseline" class="SecondRow"> 
              <td height="28" align="right" nowrap>設定值 (請用 "-" 分開多重值 ) </td>
              <td valign="middle"> <div align="left"> 
                  <input name="value" type="text" class="textbox" value="<?php //echo $row_UserLogin['value']; ?>" size="50">
                </div></td>
            </tr> -->
          <!-- <tr valign="baseline" class="SecondRow"> 
              <td height="28" align="right" nowrap>轉換值</td>
              <td valign="middle"> <div align="left"> 
                  <input name="MappingValue" type="text" class="textbox" value="<?php //echo $row_UserLogin['MappingValue']; ?>" size="50">
                </div></td>
            </tr> -->
            <!-- <tr valign="baseline" class="FirstRow"> 
              <td height="53" align="right" valign="middle" nowrap>說明</td>
              <td valign="baseline"><div align="left">
                  <textarea name="remark" cols="41"><?php //echo $row_UserLogin['remark']; ?></textarea>
                </div>
                <table width="79%">
                </table></tr></tr> -->
            <!-- <tr valign="baseline" class="SecondRow"> 
              <td align="right" nowrap>&nbsp;</td>
              <td> <input type="submit" class="button" value="更新記錄"></td>
            </tr> -->
            <!-- <tr valign="baseline" class="TableTagRight"> 
              <td align="right" nowrap>&nbsp;</td>
              <td>&nbsp;</td>
            </tr> -->
          <!-- </table> -->
          <!-- <input type="hidden" name="KeyID" value="<?php //echo $row_UserLogin['KeyID']; ?>">
          <input type="hidden" name="item" value="<?php //echo $row_UserLogin['item']; ?>">
          <input type="hidden" name="MM_update" value="form1"> -->
        <!-- </form> -->
        <!-- 結束 -->
    </div>
  </div>
</div>   
<?php
mysqli_free_result($UserLogin);
?>

