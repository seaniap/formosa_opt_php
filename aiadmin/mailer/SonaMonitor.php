<? header("Content-type: text/html; charset=utf-8");?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 <meta http-equiv="Pragma" content="no-cache">
  <!-- 在IE中這句可能沒作用 -->
  <meta http-equiv="expires" content="0">
  <!-- 在FF或Netscape中可能沒作用 -->

<html>
  <head>
    <title>Ajax Dynamic Update</title>
    <script type="text/javascript">
        var xmlHttp;
		var xmlHttpB;

        function createXMLHttpRequest() {
            if (window.ActiveXObject) {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				
            } 
            else if (window.XMLHttpRequest) {
                xmlHttp = new XMLHttpRequest();   
			         
            }
        }
		
		  function createXMLHttpRequestB() {
            if (window.ActiveXObject) {
				xmlHttpB = new ActiveXObject("Microsoft.XMLHTTP");
            } 
            else if (window.XMLHttpRequest) {
				xmlHttpB = new XMLHttpRequest();                
            }
        }
			
        
        function doStart() {
            createXMLHttpRequest();
    	    createXMLHttpRequestB();
            var url = "EPLogMonitor.php";
			url = url + "?EPLogKeyID=<? echo $_GET['EPLogKeyID'];?>&dummy=" + new Date().getTime();

            xmlHttp.open("GET", url, true);
            xmlHttp.onreadystatechange = startCallback;
            xmlHttp.send(null);
			
			//xmlHttpB.open("GET", url, true);
            //xmlHttpB.onreadystatechange = startCallbackB;
            //xmlHttpB.send(null);
					
        }

        function startCallback() {
            if (xmlHttp.readyState == 4) {
                if (xmlHttp.status == 200) {                    
                    setTimeout("pollServer()", 3000);
                    refreshTime();
                }
				else {
				    setTimeout("pollServer()", 3000);
                    refreshTime();
				}
				
				
				
            }
        }
		
		 function startCallbackB() {
            if (xmlHttpB.readyState == 4) {
                if (xmlHttpB.status == 200) {                    
                    setTimeout("pollServerB()", 3000);
                  //  refreshTime();
                }
				else {
				    setTimeout("pollServerB()", 3000);
                //    refreshTime();
				}
				
				
            }
        }
		

        
        function pollServer() {
            createXMLHttpRequest();
            var url = "EPLogMonitor.php";
			url = url +"?EPLogKeyID=<? echo $_GET['EPLogKeyID'];?>&dummy=" + new Date().getTime();
            xmlHttp.open("GET", url, true);
            xmlHttp.onreadystatechange = pollCallback;
            xmlHttp.send(null);
        }
		
		
		   function pollServerB() {
            createXMLHttpRequestB();
            var url = "../../include/HelpSend.php";
			url = url + "?EPLogKeyID=<? echo $_GET['EPLogKeyID'];?>&dummy=" + new Date().getTime();
            xmlHttpB.open("GET", url, true);
            xmlHttpB.onreadystatechange = pollCallbackB;
            xmlHttpB.send(null);
        }
		
        
        function refreshTime(){
            var time_span = document.getElementById("time");
            var time_val = time_span.innerHTML;

            var int_val = parseInt(time_val);
            var new_int_val = int_val - 1;
            
            if (new_int_val > -1) {
                setTimeout("refreshTime()", 1000);
                time_span.innerHTML = new_int_val;                
            } else {
                time_span.innerHTML = 5;
            }
        }
        
        function pollCallback() {
            if (xmlHttp.readyState == 4) {
                if (xmlHttp.status == 200) {                
                    var message = xmlHttp.responseText;                    
                    var finish=xmlHttp.responseText.search("Finish");					
					
					if(finish == -1) // -1表示未完成要啟動 help send
					{
					pollServerB();               
					document.getElementById("resultsA").innerHTML = xmlHttp.responseText;      
					setTimeout("pollServer()", 3000);
                    refreshTime();
					} 	
					else{
					location.href="EPLog.php";
					
					}
                    
                }
				else {
				    setTimeout("pollServer()", 3000);
                    refreshTime();
				}
				
				
				
				
            }
        }
        
		
		 function pollCallbackB() {
            if (xmlHttpB.readyState == 4) {
                if (xmlHttpB.status == 200) {                
                    var message = xmlHttpB.responseText;                    
                    
                   
                   //     var new_row = createRow(message);
                   //     var table = document.getElementById("dynamicUpdateArea");
                    //    var table_body = table.getElementsByTagName("tbody").item(0);
                    //    var first_row = table_body.getElementsByTagName("tr").item(1);
                    //    table_body.insertBefore(new_row, first_row);                        
                   // 顯示 helpsend 狀態   
				//	   document.getElementById("resultsB").innerHTML = xmlHttpB.responseText;      
					    setTimeout("pollServer()", 3000);
                //        refreshTime();
                    
                }
				
				else {
				    setTimeout("pollServer()", 3000);
                  //  refreshTime();
				}
				
				
            }
        }
		
        function createRow(message) {
            var row = document.createElement("tr");
            var cell = document.createElement("td");
            var cell_data = document.createTextNode(message);
            cell.appendChild(cell_data);
            row.appendChild(cell);
            return row;
        }
    </script>
 
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/table.css" rel="stylesheet" type="text/css">
 </head>
  <body onLoad="doStart();">
  <p>
    網頁在 <span id="time">5</span> 秒內會自動更新
    <p>
	
	<div id="resultsA"></div>
	<p>
	<p>
	
	
	<div id="resultsB"></div>
	
	
    <table id="dynamicUpdateArea" align="left">
        <tbody>
            <tr id="row0"><td></td></tr>
        </tbody>
    </table>
  </body>
</html>
