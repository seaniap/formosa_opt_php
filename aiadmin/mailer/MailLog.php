<?php 
if (substr($_SERVER["PHP_SELF"],-8,8)<>"main.php") {
require_once('../../Connections/AdminMySQL.php'); //不使用 main.php
//require_once('../../lib/BBlib.php');
$LinkSelf="MailLog.php?";
}else{
$LinkSelf="main.php?Page=$_GET[Page]";
//require_once('../lib/BBlib.php'); 
}
?>
<?php
$maxRows_MailLog = 50;
$pageNum_MailLog = 0;
if (isset($_GET['pageNum_MailLog'])) {
  $pageNum_MailLog = $_GET['pageNum_MailLog'];
}
$startRow_MailLog = $pageNum_MailLog * $maxRows_MailLog;

if($_GET['Where']<>"")
{ $WhereSQL=" AND ".$_GET['Where'];}
if($_GET['Sort']<>"")
{ $Sort=" order by ".$_GET['Sort']." ";}


$query_MailLog = "SELECT ".$Table_ID."_MailLog.*,".$Table_ID."_EPLog.Subject,".$Table_ID."_Member.Name,".$Table_ID."_Member.Email,".$Table_ID."_Member.Enable";
$query_MailLog = $query_MailLog." FROM ".$Table_ID."_MailLog,".$Table_ID."_EPLog,".$Table_ID."_Member where ".$Table_ID."_MailLog.EPLogKeyID='".$_GET['EPLogKeyID']."' AND ".$Table_ID."_EPLog.EPLogKeyID='".$_GET['EPLogKeyID']."' AND  ".$Table_ID."_MailLog.MKeyID=".$Table_ID."_Member.MKeyID";
$query_MailLog =$query_MailLog.$WhereSQL.$Sort ;

$query_limit_MailLog = sprintf("%s LIMIT %d, %d", $query_MailLog, $startRow_MailLog, $maxRows_MailLog);
//echo $query_limit_MailLog;
$MailLog = mysqli_query($MySQL,$query_limit_MailLog) or die(mysqli_error($MySQL));
$row_MailLog = mysqli_fetch_assoc($MailLog);

if (isset($_GET['totalRows_MailLog'])) {
  $totalRows_MailLog = $_GET['totalRows_MailLog'];
} else {
  $all_MailLog = mysqli_query($MySQL,$query_MailLog);
  $totalRows_MailLog = mysqli_num_rows($all_MailLog);
}
$totalPages_MailLog = ceil($totalRows_MailLog/$maxRows_MailLog)-1;

$queryString_MailLog = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_MailLog") == false && 
        stristr($param, "totalRows_MailLog") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_MailLog = "&" . implode("&", $newParams);
  }
}
$queryString_MailLog = sprintf("&totalRows_MailLog=%d%s", $totalRows_MailLog, $queryString_MailLog);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);



?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>發送記錄</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<p> 
           <td width="23%" align="center"> 
            <?php if ($pageNum_MailLog > 0) { // Show if not first page ?>
            <a href="<?php printf("%s?pageNum_MailLog=%d%s", $currentPage, 0, $queryString_MailLog); ?>"><img src="../images/First.gif" border=0></a> 
            <?php } // Show if not first page ?>
          </td>
          <td width="31%" align="center"> 
            <?php if ($pageNum_MailLog > 0) { // Show if not first page ?>
            <a href="<?php printf("%s?pageNum_MailLog=%d%s", $currentPage, max(0, $pageNum_MailLog - 1), $queryString_MailLog); ?>"><img src="../images/Previous.gif" border=0></a> 
            <?php } // Show if not first page ?>
          </td>
          <td width="23%" align="center"> 
            <?php if ($pageNum_MailLog < $totalPages_MailLog) { // Show if not last page ?>
            <a href="<?php printf("%s?pageNum_MailLog=%d%s", $currentPage, min($totalPages_MailLog, $pageNum_MailLog + 1), $queryString_MailLog); ?>"><img src="../images/Next.gif" border=0></a> 
            <?php } // Show if not last page ?>
          </td>
          <td width="23%" align="center"> 
            <?php if ($pageNum_MailLog < $totalPages_MailLog) { // Show if not last page ?>
            <a href="<?php printf("%s?pageNum_MailLog=%d%s", $currentPage, $totalPages_MailLog, $queryString_MailLog); ?>"><img src="../images/Last.gif" border=0></a> 
            <?php } // Show if not last page ?></p> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="22%" valign="bottom"><div align="left"><img src="images/TableTopTag.gif" width="77" height="9"></div></td>
          <td width="60%">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td class="TableTagRight"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="headtext">
<div align="left"><img src="images/ICOsystem.gif" width="20" height="15">會員收開信記錄
<br>

  <a href="main.php?GoPage=3-1-4&Export=MailLogExport.php&Page=4-3&EPLogKeyID=<? echo $_GET['EPLogKeyID']; ?>">匯出名單</a>    
</div></td>
              </tr>
            </table></td>
          <td>&nbsp;</td>
          <td width="18%" valign="bottom">  
		     <?php echo ($startRow_MailLog + 1) ?> 
			 到 <?php echo min($startRow_MailLog + $maxRows_MailLog, $totalRows_MailLog) ?> 
             共 <?php echo $totalRows_MailLog ?>
			 
			 </td>
        </tr>
      </table>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="general">
          <tr class="general"> 
          <td class="headtext">主旨</td>
          <td class="headtext">收信人
		  <div align="center"><a href="<?php printf("%s?Sort=%s&Where=%s&EPLogKeyID=%s", $currentPage, "Name Desc", $_GET['Where'],$_GET['EPLogKeyID']); ?> "><img src="images/SortIncrease.gif" width="12" height="10" border="0"></a>
		  <a href="<?php printf("%s?Sort=%s&Where=%s&EPLogKeyID=%s", $currentPage, "Name", $_GET['Where'],$_GET['EPLogKeyID']); ?>"><img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div>
		  
		  </td>
          <td class="headtext"> <p>E-Mail</p></td>
            <td class="headtext">發信</td>
            <td class="headtext">開信</td>
            <td class="headtext">發信時間
			 <div align="center"><a href="<?php printf("%s?Sort=%s&Where=%s&EPLogKeyID=%s", $currentPage, "SendTime Desc", $_GET['Where'],$_GET['EPLogKeyID']); ?> "><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a>
		  <a href="<?php printf("%s?Sort=%s&Where=%s&EPLogKeyID=%s", $currentPage, "SendTime", $_GET['Where'],$_GET['EPLogKeyID']); ?>"><img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div>
			
			</td>
            <td class="headtext">開信時間
			 <div align="center"><a href="<?php printf("%s?Sort=%s&Where=%s&EPLogKeyID=%s", $currentPage, "OpenTime Desc", $_GET['Where'],$_GET['EPLogKeyID']); ?> "><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a>
		  <a href="<?php printf("%s?Sort=%s&Where=%s&EPLogKeyID=%s", $currentPage, "OpenTime", $_GET['Where'],$_GET['EPLogKeyID']); ?>"><img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div>
			
			</td>
            <td class="headtext">功能</td>
            <td class="headtext">備註
			 <div align="center"><a href="<?php printf("%s?Sort=%s&Where=%s&EPLogKeyID=%s", $currentPage, "Remark Desc", $_GET['Where'],$_GET['EPLogKeyID']); ?> "><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a>
		  <a href="<?php printf("%s?Sort=%s&Where=%s&EPLogKeyID=%s", $currentPage, "Remark", $_GET['Where'],$_GET['EPLogKeyID']); ?>"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div>
			</td>
        </tr>
        <?php do { ?>
        <tr class="SecondRow"> 
          <td><?php echo $row_MailLog['Subject']; ?></td>
          <td><a href="MemberDetail.php?<? echo "MKeyID=".$row_MailLog['MKeyID']; ?>"><?php echo $row_MailLog['Name']; ?></a></td>
          <td><?php echo $row_MailLog['Email']; ?></td>
          <td><?php echo $row_MailLog['Send']; ?></td>
          <td><?php echo $row_MailLog['Open']; ?></td>
          <td><?php echo substr($row_MailLog['SendTime'],2,8)."<BR>".substr($row_MailLog['SendTime'],11,8); ?></td>
          <td><?php echo substr($row_MailLog['OpenTime'],2,8)."<BR>".substr($row_MailLog['OpenTime'],11,8); ?></td>
          <td><a href="../main/function/MemberWarn.php?<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."SelectMember[]=".$row_MailLog['MKeyID']."&ClientID=".$Table_ID."&SubmitMult=不發送" ?>">不發送</a></td>
          <td><?php echo $row_MailLog['Remark']; ?></td>
        </tr>
        <?php } while ($row_MailLog = mysqli_fetch_assoc($MailLog)); ?>
      </table></td>
  </tr>
</table>
</body>
</html>
<?php
mysqli_free_result($MailLog);
?>
