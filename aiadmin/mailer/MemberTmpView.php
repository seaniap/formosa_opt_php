<?php 
if (substr($_SERVER["PHP_SELF"],-8,8)<>"main.php") {
require_once('../../Connections/AdminMySQL.php'); //不使用 main.php
//require_once('../../lib/BBlib.php');
$LinkSelf="MemberAdd.php?";
}else{
$LinkSelf="main.php?Page=$_GET[Page]";
//require_once('../lib/BBlib.php'); 
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_Member = 100;
$pageNum_Member = 0;
if (isset($_GET['pageNum_Member'])) {
  $pageNum_Member = $_GET['pageNum_Member'];
}
$startRow_Member = $pageNum_Member * $maxRows_Member;


$query_Member = "SELECT * FROM ".$Table_ID."_MemberTmp ORDER BY CreatedTime ASC";
$query_limit_Member = sprintf("%s LIMIT %d, %d", $query_Member, $startRow_Member, $maxRows_Member);
$Member = mysqli_query($MySQL,$query_limit_Member) or die(mysqli_error($MySQL));
$row_Member = mysqli_fetch_assoc($Member);

if (isset($_GET['totalRows_Member'])) {
  $totalRows_Member = $_GET['totalRows_Member'];
} else {
  $all_Member = mysqli_query($MySQL,$query_Member);
  $totalRows_Member = mysqli_num_rows($all_Member);
}
$totalPages_Member = ceil($totalRows_Member/$maxRows_Member)-1;

$queryString_Member = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Member") == false && 
        stristr($param, "totalRows_Member") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Member = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Member = sprintf("&totalRows_Member=%d%s", $totalRows_Member, $queryString_Member);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>會員名單上傳</title>
</head>

<body>

<p>檔案已上傳成功</p>
<p> 請確認是否要匯入資料庫 </p>

<input type="button" value="是的,請匯入資料庫" onclick="location.href='<? echo $RootLevel; ?>aiadmin/main.php?Page=3-3-2';"> 

<input type="button" value="不,請不要匯入資料庫"  onclick="location.href='<? echo $RootLevel; ?>aiadmin/main.php?Page=3-1';">
<br>

記錄 <?php echo ($startRow_Member + 1) ?> 到 <?php echo min($startRow_Member + $maxRows_Member, $totalRows_Member) ?> 共 <?php echo $totalRows_Member ?>

<table border="0" width="50%" align="center">
  <tr>
    <td width="23%" align="center"><?php if ($pageNum_Member > 0) { // Show if not first page ?>
          <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, 0, $queryString_Member); ?>">第一頁</a>
          <?php } // Show if not first page ?>
    </td>
    <td width="31%" align="center"><?php if ($pageNum_Member > 0) { // Show if not first page ?>
          <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, max(0, $pageNum_Member - 1), $queryString_Member); ?>">上一頁</a>
          <?php } // Show if not first page ?>
    </td>
    <td width="23%" align="center"><?php if ($pageNum_Member < $totalPages_Member) { // Show if not last page ?>
          <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, min($totalPages_Member, $pageNum_Member + 1), $queryString_Member); ?>">下一頁</a>
          <?php } // Show if not last page ?>
    </td>
    <td width="23%" align="center"><?php if ($pageNum_Member < $totalPages_Member) { // Show if not last page ?>
          <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, $totalPages_Member, $queryString_Member); ?>">最後一頁</a>
          <?php } // Show if not last page ?>
    </td>
  </tr>
</table>


<table width="800" border="1">
  <tr>
    <td><table cellspacing="0" cellpadding="0">
      <tr>
        <td height="22" width="64">會員編號</td>
      </tr>
    </table></td>
    <td><table cellspacing="0" cellpadding="0">
      <tr>
        <td height="22" width="64">姓名</td>
      </tr>
    </table></td>
    <td><table cellspacing="0" cellpadding="0">
      <tr>
        <td height="22" width="64">Email</td>
      </tr>
    </table></td>
    <td><table cellspacing="0" cellpadding="0">
      <tr>
        <td height="22" width="64">生日</td>
      </tr>
    </table></td>
    <td><table cellspacing="0" cellpadding="0">
      <tr>
        <td height="22" width="64">性別</td>
      </tr>
    </table></td>
    <td><table cellspacing="0" cellpadding="0">
      <tr>
        <td height="22" width="64">群組</td>
      </tr>
    </table></td>
  </tr>
  <?php do { ?>
  <tr>
   
    <td><?php echo $row_Member['MemberID']; ?></td>
    <td><?php echo $row_Member['Name']; ?></td>
    <td><?php echo $row_Member['Email']; ?></td>
    <td><?php echo $row_Member['Birthday']; ?></td>
    <td><?php echo $row_Member['Sex']; ?></td>
    <td><?php echo $row_Member['Group']; ?></td>
</tr>
  <?php } while ($row_Member = mysqli_fetch_assoc($Member)); ?>
</table>








</body>
</html>
<?php
mysqli_free_result($Member);
?>
