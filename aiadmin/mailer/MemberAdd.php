<?php 
if (substr($_SERVER["PHP_SELF"],-8,8)<>"main.php") {
require_once('../../Connections/AdminMySQL.php'); //不使用 main.php
//require_once('../../lib/BBlib.php');
$LinkSelf="MemberAdd.php?";
}else{
$LinkSelf="main.php?Page=$_GET[Page]";
//require_once('../lib/BBlib.php'); 
}


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO ".$_SESSION['ClientID']."_Member (MKeyID, MemberID, Name, Email, Birthday, Sex, `Group`, Type, Enable, Remark, CreatedTime) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['MKeyID'], "text"),
                       GetSQLValueString($_POST['MemberID'], "text"),
                       GetSQLValueString($_POST['Name'], "text"),
                       GetSQLValueString($_POST['Email'], "text"),
                       GetSQLValueString($_POST['Birthday'], "date"),
                       GetSQLValueString($_POST['Sex'], "text"),
                       GetSQLValueString($_POST['Group'],"text"),
                       GetSQLValueString($_POST['Type'], "text"),
                       GetSQLValueString($_POST['Enable'], "text"),
                       GetSQLValueString($_POST['Remark'], "text"),
                       GetSQLValueString($_POST['CreatedTime'], "date"));

  
  //echo $insertSQL;
  $Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));

  $insertGoTo = $RootLevel."aiadmin/main.php?Page=3-1";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>無標題文件</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/table.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="WraperArea">

<div id="MainHeader">
    <div class="PageNumberRight"><img src="../images/ICOgirl.gif" width="14" height="12" align="absmiddle">新增會員</div>
    <div class="whereYouAre">會員管理 &gt; 新增會員</div>
</div>
<div class="clear"></div>
<!-- 開始 -->
<div id="Box">
	<div class="indexhead"></div>
    <div class="indexContent">
<form method="post" name="form1" action="<? echo $RootLevel; ?>aiadmin/mailer/MemberAdd.php">
  <table width="85%" border="0" align="center" cellpadding="2" cellspacing="1" class="general">
    <tr valign="middle" class="FirstRow"> 
      <th width="139" height="35" align="right" nowrap>密碼</th>
      <td width="427"> <div align="left"> 
          <input name="MemberID" type="text" class="textbox" value="" size="32">
        </div></td>
    </tr>
    <tr valign="middle" class="SecondRow"> 
      <th height="35" align="right" nowrap>會員名稱</th>
      <td> <div align="left"> 
          <input name="Name" type="text" class="textbox" value="" size="32">
        </div></td>
    </tr>
    <tr valign="middle" class="FirstRow"> 
      <th height="35" align="right" nowrap>E mail</th>
      <td> <div align="left"> 
          <input name="Email" type="text" class="textbox" value="" size="32">
        </div></td>
    </tr>
    <tr valign="middle" class="SecondRow"> 
      <th height="35" align="right" nowrap>生　日</th>
      <td> <div align="left"> 
          <input name="Birthday" type="text" class="textbox" value="" size="32">
        </div></td>
    </tr>
    <tr valign="middle" class="FirstRow"> 
      <th height="35" align="right" nowrap>性　別</th>
      <td> <div align="left"> 
          <input type="radio" name="Sex" value="MALE">
          男 
          <input type="radio" name="Sex" value="FEMALE">
          女 </div></td>
    </tr>
    <tr valign="middle" class="SecondRow"> 
      <th height="35" align="right" nowrap>群　組</th>
      <td> <div align="left"> 
          <input name="Group" type="text" class="textbox" value="" size="32">
        </div></td>
    </tr>
    <tr valign="middle" class="SecondRow"> 
      <th height="35" align="right" nowrap>是否寄信</th>
      <td> <div align="left"> 
          <input name="Enable" type="radio" value="Y" checked>
          是 
          <input type="radio" name="Enable" value="N">
          否 </div></td>
    </tr>
    <tr valign="middle" class="FirstRow"> 
      <th height="35" align="right" nowrap>備　註</th>
      <td> <div align="left"> 
          <p> 
            <textarea name="Remark" cols="50" rows="5" class="textbox"></textarea>
          </p>
        </div></td>
    </tr>
    <tr valign="baseline" class="SecondRow"> 
      <td colspan="2" align="right" nowrap> <input type="submit" class="button" value="新增會員"></td>
    </tr>
    <tr valign="baseline" class="general"> 
      <td colspan="2" align="right" nowrap>&nbsp;</td>
    </tr>
  </table>

  <input type="hidden" name="MKeyID" value="<? echo uniqid(mt_rand()); ?>">
  <input type="hidden" name="CreatedTime" value="<? echo Date("Y-m-j H:i:s"); ?>">
  <input type="hidden" name="MM_insert" value="form1"> 
  
</form>
</div>
    <div class="indexbottom"></div>
	</div>
<!-- 結束 --></div>
</body>
</html>