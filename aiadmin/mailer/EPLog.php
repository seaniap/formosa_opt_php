<?php 
if (substr($_SERVER["PHP_SELF"],-8,8)<>"main.php") {
require_once('../../Connections/AdminMySQL.php'); //不使用 main.php
require_once('../../lib/BBlib.php');
$LinkSelf="MemberView.php?";
}else{
$LinkSelf="main.php?Page=$_GET[Page]";
require_once('../lib/BBlib.php'); 
}
?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_EPLog = 10;
$pageNum_EPLog = 0;
if (isset($_GET['pageNum_EPLog'])) {
  $pageNum_EPLog = $_GET['pageNum_EPLog'];
}
$startRow_EPLog = $pageNum_EPLog * $maxRows_EPLog;

if($_GET['Where']<>"")
{ $WhereSQL=" WHERE EPLog.Template='".$_GET['Where']."'";}
if($_GET['Sort']<>"")
{ $Sort=" order by ".$_GET['Sort']." ";}
else 
{$Sort=" order by SendTime Desc";}


$query_EPLog = "SELECT EPLog.EPLogKeyID,EPLog.KeyID,EPLog.SendTime, EPLog.EndTime,EPLog.Subject,EPLog.Template,EPLog.Group,EPLog.Operator,EPLog.TotalCount, EPLog.Status,sum(MailLog.Send) as Send ,sum(MailLog.Open) as Open FROM ".$Table_ID."_EPLog as EPLog left join ".$Table_ID."_MailLog as MailLog on EPLog.EPLogKeyID=MailLog.EPLogKeyID ".$WhereSQL;
$query_EPLog = $query_EPLog." GROUP BY EPLog.EPLogKeyID ".$Sort;
$query_limit_EPLog = sprintf("%s LIMIT %d, %d", $query_EPLog, $startRow_EPLog, $maxRows_EPLog);
//echo $query_limit_EPLog;
$EPLog = mysqli_query($MySQL,$query_limit_EPLog) or die(mysqli_error($MySQL));
$row_EPLog = mysqli_fetch_assoc($EPLog);

if (isset($_GET['totalRows_EPLog'])) {
  $totalRows_EPLog = $_GET['totalRows_EPLog'];
} else {
  $all_EPLog = mysqli_query($MySQL,$query_EPLog);
  $totalRows_EPLog = mysqli_num_rows($all_EPLog);
}
$totalPages_EPLog = ceil($totalRows_EPLog/$maxRows_EPLog)-1;

$queryString_EPLog = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_EPLog") == false && 
        stristr($param, "totalRows_EPLog") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_EPLog = "&" . implode("&", $newParams);
  }
}
$queryString_EPLog = sprintf("&totalRows_EPLog=%d%s", $totalRows_EPLog, $queryString_EPLog);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);



?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<? Java_Script(); // 呼叫java 函式庫  ?>
<title>電子報發送記錄</title>
<link href="<? echo $RootLevel; ?>aiadmin/css/style.css" rel="stylesheet" type="text/css">
<link href="<? echo $RootLevel; ?>aiadmin/css/table.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="WraperArea2">
<div id="MainHeader">
    <div class="PageNumberRight"><img src="images/ICOsystem.gif" width="20" height="15" align="absmiddle">發送記錄</div>
    <div class="whereYouAre">電子報 &gt; 發送記錄</div>
</div>
<div class="clear"></div>
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="datatable">
    <tr class="general">
    <td height="20" colspan="9" class="headtext2">
        <div class="RecordData">
        <?php echo ($startRow_EPLog + 1) ?> 到 <?php echo min($startRow_EPLog + $maxRows_EPLog, $totalRows_EPLog) ?> 共 <?php echo $totalRows_EPLog ?></div>    </td>
    </tr>
    <tr> 
          <th width="8%"><table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="59%" rowspan="2"><div align="right">開始<br>
                時間</div></td>
                <td width="22%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=SendTime Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><a href="<? echo "EPLog.php?Where=$Where&Sort=SendTime"; ?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></td>
              </tr>
            </table>          </th>
          <th width="8%"><table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="63%" rowspan="2"><div align="right">結束<br>
                時間</div></td>
                <td width="17%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=EndTime Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=EndTime"; ?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
            </table>          </th>
      <th>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="46%" rowspan="2"><div align="right">主旨</div></td>
                <td width="54%"><div align="left"><a href="<? echo "EPLog.php?Where=$Where&Sort=Page Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><div align="left"><a href="<? echo "EPLog.php?Where=$Where&Sort=Page"; ?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
        </table>      </th>
          <th width="4%">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="41%" rowspan="2"><div align="right">版型</div></td>
                <td width="39%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=T_Name Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><a href="<? echo "EPLog.php?Where=$Where&Sort=T_Name"; ?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></td>
              </tr>
            </table>          </th>
          <th width="8%">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="53%" rowspan="2"><div align="right">群組</div></td>
                <td width="47%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=Group Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=Group"; ?>%20%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
            </table>          </th>
          <th width="9%">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="53%" rowspan="2"><div align="right">發信</div></td>
                <td width="27%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=Operator Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><a href="<? echo "EPLog.php?Where=$Where&Sort=Operator"; ?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></td>
              </tr>
            </table> </th>
          <th width="7%">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="39%" rowspan="2"><div align="right">預計發送</div></td>
                <td width="61%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=TotalCount Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td height="13"><a href="<? echo "EPLog.php?Where=$Where&Sort=TotalCount"; ?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></td>
              </tr>
            </table>          </th>
          <th width="7%">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="50%" rowspan="2"><div align="right">實發</div></td>
                <td width="29%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=Send Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=Send"; ?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
            </table>          </th>
          <th width="9%"><table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="42%" rowspan="2"><div align="right">開信</div></td>
                <td width="38%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=Open Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><a href="<? echo "EPLog.php?Where=$Where&Sort=Open"; ?>%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></td>
              </tr>
            </table>          </th>
          <th width="8%"><table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td width="42%" rowspan="2"><div align="right">狀態</div></td>
                <td width="39%"><div align="center"><a href="<? echo "EPLog.php?Where=$Where&Sort=Status Desc" ;?>%20"><img src="../images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
              </tr>
              <tr> 
                <td><a href="<? echo "EPLog.php?Where=$Where&Sort=Status"; ?>%20%20"><img src="../images/SortUNDecrease.gif" width="12" height="10" border="0"></a></td>
              </tr>
            </table>          </th>
      <th width="4%">功能</th>
    </tr>
  <?php do { ?>
        <tr class="FirstRow"> 
          <td><?php echo substr($row_EPLog['SendTime'],2,8)."<BR>".substr($row_EPLog['SendTime'],11,8); ?></td>
          <td><?php echo substr($row_EPLog['EndTime'],2,8)."<BR>".substr($row_EPLog['EndTime'],11,8); ?></td>
         <td>
<? 
$templateS=substr($row_EPLog['Template'],0,-4);

 ?>         
         <a href="<? echo $RootLevel."include/".$row_EPLog['Template']."?".$MM_keepURL.(($MM_keepURL!="")?"&":"").$templateS."=".$row_EPLog['KeyID']; ?>" target="_blank"><?php echo $row_EPLog['Subject']; ?></a></td>
          <td><?php echo $row_EPLog['Template']; ?></td>
          <td><?php echo $row_EPLog['Group']; ?></td>
          <td><?php echo $row_EPLog['Operator']; ?></td>
          <td><a href="main.php?GoPage=4-3-1&<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."EPLogKeyID=".$row_EPLog['EPLogKeyID']."&Sort=SendTime"; ?>"> 
            <?php echo $row_EPLog['TotalCount']; ?></a></td>
          <td><a href="main.php?GoPage=4-3-1&<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."EPLogKeyID=".$row_EPLog['EPLogKeyID']."&Where=Send&Sort=SendTime"; ?>"> 
		    <?php echo $row_EPLog['Send'];?></a></td>
          <td><a href="main.php?GoPage=4-3-1&<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."EPLogKeyID=".$row_EPLog['EPLogKeyID']."&Where=Open&Sort=OpenTime"; ?>"> 
		  <?php echo $row_EPLog['Open'];?></a></td>
          <td><?php echo $row_EPLog['Status']; ?></td>
          <td><input type="button" value="急停" onClick="ConfirmWindow('EPStopF.php?EPLogKeyID=<? echo $row_EPLog['EPLogKeyID']; ?>','您確定要停止這封電子報發送嗎?')"></td>
        </tr>
        <?php } while ($row_EPLog = mysqli_fetch_assoc($EPLog)); ?>
    <tr> 
    <td colspan="11">
        <form name="form1">
              跳至第 
              <select name="menu1" onChange="MM_jumpMenu(this,0)">
                <option value='' selected></option>
                <? for($I=0;$I<=$totalRows_BigTable/$maxRows_BigTable;$I++)
			  
			  { echo "<option value='";
			  printf("%s?pageNum_BigTable=%d%s", $currentPage,$I,$queryString_EPLog);
			   echo "'>".($I+1)." </option>"; 
			}
		?>
              </select>
        頁 </form>
        <div style="float:right">
          <?php if ($pageNum_EPLog > 0) { // Show if not first page ?><span>
            <a href="<?php printf("%s?pageNum_EPLog=%d%s", $currentPage, 0, $queryString_EPLog); ?>"><img src="../images/PageHome.gif" alt="最前頁" border=0></a> 
            </span>
			<?php } // Show if not first page ?>
          <?php if ($pageNum_EPLog > 0) { // Show if not first page ?><span>
            <a href="<?php printf("%s?pageNum_EPLog=%d%s", $currentPage, max(0, $pageNum_EPLog - 1), $queryString_EPLog); ?>"><img src="../images/PagePrevious.gif" alt="上一頁" border=0></a></span> 
            <?php } // Show if not first page ?>
          <?php if ($pageNum_EPLog < $totalPages_EPLog) { // Show if not last page ?><span>
            <a href="<?php printf("%s?pageNum_EPLog=%d%s", $currentPage, min($totalPages_EPLog, $pageNum_EPLog + 1), $queryString_EPLog); ?>"><img src="../images/PageNext.gif" alt="下一頁" border=0></a></span> 
            <?php } // Show if not last page ?>
          <?php if ($pageNum_EPLog < $totalPages_EPLog) { // Show if not last page ?><span>
            <a href="<?php printf("%s?pageNum_EPLog=%d%s", $currentPage, $totalPages_EPLog, $queryString_EPLog); ?>"><img src="../images/PageEnd.gif" alt="最終頁" border=0></a></span> 
            <?php } // Show if not last page ?>
        </div>    </td>
    </tr>
  </table>
</div>
</body>
</html>
<?php
mysqli_free_result($EPLog);
?>

