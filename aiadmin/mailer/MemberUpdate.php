<?php 
if (substr($_SERVER["PHP_SELF"],-8,8)<>"main.php") {
require_once('../../Connections/AdminMySQL.php');
$LinkSelf="MemberUpdate.php?";
}else{
$LinkSelf="main.php?Page=$_GET[Page]"; 
}
?>
<?php
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE ".$Table_ID."_Member SET MemberID=%s, Name=%s, Email=%s, Birthday=%s, Sex=%s, `Group`=%s, Type=%s, Enable=%s, Remark=%s, CreatedTime=%s, h01=%s, h02=%s, h03=%s, h04=%s, h05=%s, h06=%s, h07=%s, h08=%s, h09=%s, h10=%s, h11=%s, h12=%s, h13=%s, h14=%s, h15=%s, h16=%s, h17=%s, h18=%s, h19=%s, h20=%s, h21=%s, h22=%s, h23=%s, h24=%s, h25=%s, h26=%s, h27=%s, h28=%s, h29=%s, h30=%s, c01=%s, p01=%s, p02=%s, p03=%s, p04=%s, p05=%s, p06=%s WHERE MKeyID=%s",
                       GetSQLValueString($_POST['MemberID'], "text"),
                       GetSQLValueString($_POST['Name'], "text"),
                       GetSQLValueString($_POST['Email'], "text"),
                       GetSQLValueString($Birthday, "date"),
                       GetSQLValueString($_POST['Sex'], "text"),
                       GetSQLValueString($_POST['Group'], "text"),
                       GetSQLValueString($_POST['Type'], "text"),
                       GetSQLValueString($_POST['Enable'], "text"),
                       GetSQLValueString($_POST['Remark'], "text"),
                       GetSQLValueString($_POST['CreatedTime'], "date"),
                       GetSQLValueString($_POST['h01'], "text"),
                       GetSQLValueString($_POST['h02'], "text"),
                       GetSQLValueString($_POST['h03'], "text"),
                       GetSQLValueString($_POST['h04'], "text"),
                       GetSQLValueString($_POST['h05'], "text"),
                       GetSQLValueString($_POST['h06'], "text"),
                       GetSQLValueString($_POST['h07'], "text"),
                       GetSQLValueString($_POST['h08'], "text"),
                       GetSQLValueString($_POST['h09'], "text"),
                       GetSQLValueString($_POST['h10'], "text"),
                       GetSQLValueString($_POST['h11'], "text"),
                       GetSQLValueString($_POST['h12'], "text"),
                       GetSQLValueString($_POST['h13'], "text"),
                       GetSQLValueString($_POST['h14'], "text"),
                       GetSQLValueString($_POST['h15'], "text"),
                       GetSQLValueString($_POST['h16'], "text"),
                       GetSQLValueString($_POST['h17'], "text"),
                       GetSQLValueString($_POST['h18'], "text"),
                       GetSQLValueString($_POST['h19'], "text"),
                       GetSQLValueString($_POST['h20'], "text"),
                       GetSQLValueString($_POST['h21'], "text"),
                       GetSQLValueString($_POST['h22'], "text"),
                       GetSQLValueString($_POST['h23'], "text"),
                       GetSQLValueString($_POST['h24'], "text"),
                       GetSQLValueString($_POST['h25'], "text"),
                       GetSQLValueString($_POST['h26'], "text"),
                       GetSQLValueString($_POST['h27'], "text"),
                       GetSQLValueString($_POST['h28'], "text"),
                       GetSQLValueString($_POST['h29'], "text"),
                       GetSQLValueString($_POST['h30'], "text"),
                       GetSQLValueString($_POST['c01'], "text"),
                       GetSQLValueString($p01, "text"),
                       GetSQLValueString($p02, "text"),
                       GetSQLValueString($p03, "text"),
                       GetSQLValueString($p04, "text"),
                       GetSQLValueString($p05, "text"),
                       GetSQLValueString($p06, "text"),
                       GetSQLValueString($_POST['MKeyID'], "text"));

  
//  $Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));
   $Result1 = mysqli_query($MySQL,$updateSQL);




  $updateGoTo =$RootLevel."aiadmin/main.php?".str_replace("GoPage","AA",$_POST[KeepURL]);
  
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Member = "1";
if (isset($_GET['MKeyID'])) {
  $colname_Member = (get_magic_quotes_gpc()) ? $_GET['MKeyID'] : addslashes($_GET['MKeyID']);
}

$query_Member = sprintf("SELECT * FROM %s_Member WHERE MKeyID = '%s'",$Table_ID, $colname_Member);
$Member = mysqli_query($MySQL,$query_Member) or die(mysqli_error($MySQL));
$row_Member = mysqli_fetch_assoc($Member);
$totalRows_Member = mysqli_num_rows($Member);
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>無標題文件</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<link href="../css/table.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="WraperArea">
<div id="MainHeader">
    <div class="PageNumberRight"><img src="../images/ICOgirl.gif" width="14" height="12" align="absmiddle">編輯會員資訊</div>
    <div class="whereYouAre">會員管理 &gt; 編輯會員資訊</div>
</div>
<div class="clear"></div>
<!-- 開始 -->
  <div id="Box">
	<div class="indexhead"></div>
    <div class="indexContent">

<form method="post" name="form1" action="<? echo $RootLevel; ?>aiadmin/mailer/MemberUpdate.php">
  <table width="85%" border="0" align="center" cellpadding="2" cellspacing="1" class="general">
    <tr valign="baseline"  class="general"> 
      <td colspan="2" align="right" nowrap>&nbsp;</td>
    </tr>
    <tr valign="middle" class="SecondRow"> 
      <th align="right" nowrap>會員編號</th>
      <td width="238"> <div align="left"> 
          <input name="MemberID" type="text" class="textbox" value="<?php echo $row_Member['MemberID']; ?>" size="32">
        </div></td>
    </tr>
    <tr valign="middle" class="FirstRow"> 
      <th align="right" nowrap>會員名稱</th>
      <td> <div align="left"> 
          <input name="Name" type="text" class="textbox" value="<?php echo $row_Member['Name']; ?>" size="32">
          <font color="#FF0000"> * </font></div></td>
    </tr>
    <tr valign="middle" class="SecondRow"> 
      <th align="right" nowrap>E mail</th>
      <td> <div align="left"> 
          <input name="Email" type="text" class="textbox" value="<?php echo $row_Member['Email']; ?>" size="32">
          <font color="#FF0000"> * </font></div></td>
    </tr>
    <tr valign="middle" class="FirstRow"> 
      <th align="right" nowrap>生　日</th>
      <td> <div align="left"> 
          <input name="Birthday" type="text" class="textbox" value="<?php echo $row_Member['Birthday']; ?>" size="32">
          請輸入年月日(例如1977-01-25) </div></td>
    </tr>
    <tr valign="middle" class="SecondRow"> 
      <th align="right" nowrap>性　別</th>
      <td> <div align="left"> 
          <input name="Sex" type="text" class="textbox" value="<?php echo $row_Member['Sex']; ?>" size="32">
        </div></td>
    </tr>
    <tr valign="middle" class="FirstRow"> 
      <th height="26" align="right" nowrap>群　組</th>
      <td> <div align="left"> 
          <input name="Group" type="text" class="textbox" value="<?php echo $row_Member['Group']; ?>" size="32">
        </div></td>
    </tr>
    <tr valign="middle" class="FirstRow"> 
      <th height="30" align="right" nowrap>是否寄信</th>
      <td> <div align="left"> 
          <input name="Enable" type="text" class="textbox" value="<?php echo $row_Member['Enable']; ?>" size="32">
          <font color="#FF0000"> * </font></div></td>
    </tr>
    <tr valign="middle" class="SecondRow"> 
      <th align="right" nowrap>備　註</th>
      <td> <div align="left"> 
          <textarea name="Remark" cols="45" rows="3" class="textbox"><?php echo $row_Member['Remark']; ?></textarea>
        </div></td>
    </tr>
    <tr valign="baseline" class="FirstRow"> 
      <th align="right" nowrap>&nbsp;</th>
      <td> <input type="submit" class="button" value="更新記錄"></td>
    </tr>
    <tr valign="baseline" class="headtext"> 
      <td colspan="2" align="right" nowrap><font color="#FF0000">*</font> 號者為必填項目</td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="MKeyID" value="<?php echo $row_Member['MKeyID']; ?>">
  <input type="hidden" name="KeepURL" value="<?php echo str_replace($vowels ,"**!!!**",$_SERVER['QUERY_STRING']); ?>">
</form>
</div>
    <div class="indexbottom"></div>
	</div>
<!-- 結束 -->
</div>  </body>
</html>
<?php
mysqli_free_result($Member);
?>

