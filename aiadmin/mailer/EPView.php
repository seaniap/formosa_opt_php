<?php 
if (substr($_SERVER["PHP_SELF"],-8,8)<>"main.php") {
require_once('../../Connections/AdminMySQL.php'); //不使用 main.php
require_once('../../lib/BBlib.php');
$LinkSelf="MemberView.php?";
}else{
$LinkSelf="main.php?Page=$_GET[Page]";
require_once('../lib/BBlib.php'); 
}
?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_BigTable = 10;
$pageNum_BigTable = 0;
if (isset($_GET['pageNum_BigTable'])) {
  $pageNum_BigTable = $_GET['pageNum_BigTable'];
}
$startRow_BigTable = $pageNum_BigTable * $maxRows_BigTable;

if($_GET['Sort']<>"")
{ $Sort=" order by ".$_GET['Sort']." ";}
else
{$Sort=" order by d03 DESC ";}

if($_GET['Where']<>"")
{ $WhereSQL=" AND Cate01='".$_GET['Where']."' ";}
else
{ $WhereSQL=" ";}



$query_BigTable = "SELECT * FROM ".$Table_ID."_BigTable WHERE left(Page,1)=7 ".$WhereSQL.$Sort;
$query_limit_BigTable = sprintf("%s LIMIT %d, %d", $query_BigTable, $startRow_BigTable, $maxRows_BigTable);
//echo $query_limit_BigTable;
$BigTable = mysqli_query($MySQL,$query_limit_BigTable) or die(mysqli_error($MySQL));
$row_BigTable = mysqli_fetch_assoc($BigTable);

if (isset($_GET['totalRows_BigTable'])) {
  $totalRows_BigTable = $_GET['totalRows_BigTable'];
} else {
  $all_BigTable = mysqli_query($MySQL,$query_BigTable);
  $totalRows_BigTable = mysqli_num_rows($all_BigTable);
}
$totalPages_BigTable = ceil($totalRows_BigTable/$maxRows_BigTable)-1;

$queryString_BigTable = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_BigTable") == false && 
        stristr($param, "totalRows_BigTable") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_BigTable = "&" . implode("&", $newParams);
  }
}
$queryString_BigTable = sprintf("&totalRows_BigTable=%d%s", $totalRows_BigTable, $queryString_BigTable);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);


// 分類選單群組查詢
$query_TGroup = "Select distinct Cate01 as Category FROM ".$Table_ID."_BigTable WHERE left(Page,1)=7";
$TGroup = mysqli_query($MySQL,$query_TGroup) or die(mysqli_error($MySQL));
$row_TGroup = mysqli_fetch_assoc($TGroup);
$totalRows_TGroup = mysqli_num_rows($TGroup);


?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<? Java_Script(); // 呼叫java 函式庫  ?>
<title>電子報表列</title>
<link href="<? echo $RootLevel; ?>aiadmin/css/style.css" rel="stylesheet" type="text/css">
<link href="<? echo $RootLevel; ?>aiadmin/css/table.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(selObj,restore){ //v3.0
  eval("location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
</head>
<body>

<div id="WraperArea2">
<div id="MainHeader">
    <div class="PageNumberRight"><img src="images/ICOsystem.gif" width="20" height="15" align="absmiddle">電子報表列</div>
    <div class="whereYouAre">電子報 &gt; 電子報表列</div>
</div>
<div class="clear"></div>
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="datatable">
                    <tr class="general">
                      <td height="20" colspan="9" class="headtext2">
                        <div class="CheckBoxSelect">
                          <form name="form1"> 
                            <p>請選擇分類 
	                          <select name="menu1" onChange="MM_jumpMenu(this,0)"> 
	                             <option value="<? echo $currentPage; ?>" <? if($_GET['Where']=="") {echo "selected";}?> >全部顯示  </option>     
	                            
	                            <?php do {  ?>		
       <option value="<? echo $currentPage."?Where=".$row_TGroup['Category']; ?>"<? if ($_GET['Where']==$row_TGroup['Category']) {echo "selected"; }    ?>>
          <? echo $row_TGroup['Category']; ?></option>
	                            
	                            <?php } while ($row_TGroup = mysqli_fetch_assoc($TGroup)); ?>
                                                        </select>             
                            </p>
                        </form></div>
                        <div class="RecordData"><?php echo ($startRow_BigTable + 1) ?> 到 <?php echo min($startRow_BigTable + $maxRows_BigTable, $totalRows_BigTable) ?> 共 <?php echo $totalRows_BigTable ?></div>
                      </td>
                    </tr>
                    <tr> 
                      <th width="22%" height="20"> <table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td width="46%">最後修改日期</td>
                            <td width="19%">
							<div align="right"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=d03 Desc"><img src="images/SortIncrease.gif" width="12" height="10" border="0"></a></div>
							</td>
                            <td width="35%">
							<div align="left"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=d03"><img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div>
							</td>
                          </tr>
                        </table></th>
                      <th width="33%">
                          <table width="100%" border="0" cellspacing="0" cellpadding="1">
                            <tr> 
                              <td width="45%" height="23">主旨</td>
                              <td width="13%"><div align="right"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=h01 Desc">
							  <img src="images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
                              <td width="42%"><div align="left"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=h01">
							  <img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
                            </tr>
                          </table>
                        </th>
                      <th width="13%"> <table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td width="46%">版型</td>
                            <td width="19%"><div align="right"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=Template Desc">
							<img src="images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
                            <td width="35%"><div align="left"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=Template">
							<img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
                          </tr>
                        </table></th>
						
						 <th width="13%"> <table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td width="46%" nowrap>分類</td>
                            <td width="19%"><div align="right"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=Cate01 Desc">
							<img src="images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
                            <td width="35%"><div align="left"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=Cate01">
							<img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
                          </tr>
                        </table></th>
						
						 <th width="13%"> <table width="100%" border="0" cellspacing="0" cellpadding="1">
                          <tr> 
                            <td width="46%" nowrap>前台顯示</td>
                            <td width="19%"><div align="right"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=Pname Desc">
							<img src="images/SortIncrease.gif" width="12" height="10" border="0"></a></div></td>
                            <td width="35%"><div align="left"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=Pname">
							<img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
                          </tr>
                        </table></th>
						
						
						
						
						
                      <th width="13%">
                          <table width="100%" border="0" cellspacing="0" cellpadding="1">
                            <tr> 
                              <td width="46%" nowrap>作者</td>
                              <td width="19%"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=author Desc"><img src="images/SortIncrease.gif" width="12" height="10" border="0"></a></td>
                              <td width="35%"><div align="left"><a href="../mailer/EPView.php?Where=<? echo $_GET['Where']; ?>&Sort=author"><img src="images/SortUNDecrease.gif" width="12" height="10" border="0"></a></div></td>
                            </tr>
                          </table>
                        </th>
                      <th height="20" colspan="3">功能</th>
                    </tr>
                    <?php do {       
					$templateS=substr($row_BigTable['Template'],0,-4);
					
					?>
                    <tr class="FirstRow"> 
                      <td height="20"><?php echo $row_BigTable['d03']; ?> </td>
                      <td height="20"><a href="../include/<? echo $row_BigTable['Template']."?Page=".$row_BigTable['Page']."&".$templateS."=".$row_BigTable['KeyID']; ?>" target="_blank"> 
                          <?php echo $row_BigTable['h01']; ?></a></td>
                      <td><?php echo $row_BigTable['Template']; ?></td>
					  <td><?php echo $row_BigTable['Cate01']; ?></td>
					  <td><?php echo $row_BigTable['Pname']; ?></td>
                      <td><?php echo $row_BigTable['author']; ?></td>
                      <td width="6%"><a href="<? echo $RootLevel; ?>include/EPSend.php?<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."KeyID=".$row_BigTable['KeyID'] ?>" target="_blank">
					  <img src="<? echo $RootLevel; ?>aiadmin/images/ICOsend.gif" alt="送出" width="17" height="13" border="0"></a></td>
                      <td width="6%">
					  <img src="<? echo $RootLevel; ?>aiadmin/images/ICOedit.gif" alt="編輯" width="14" height="12" border="0" onClick="MM_openBrWindow('../include/MSuper.php?Action=edit&Template=<? echo $row_BigTable['Template'];?>&Page=<? echo $row_BigTable['Page']; ?>&KeyID=<? echo $row_BigTable['KeyID']; ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" style="cursor:hand;" ></td>
                      <td width="6%"><img src="<? echo $RootLevel; ?>aiadmin/images/ICOdelete.gif" alt="刪除" width="15" height="12" border="0" onClick="ConfirmWindow('<? echo $RootLevel; ?>include/DelBigTable.php?KeyID=<? echo $row_BigTable['KeyID']; ?>','你確定要刪除這份電子報?')" ></td>
                    </tr>
                    <?php } while ($row_BigTable = mysqli_fetch_assoc($BigTable)); ?>
                    <tr> 
                      <td height="25" colspan="7"> <form name="form1">
              跳至第 
              <select name="menu1" onChange="MM_jumpMenu(this,0)">
                <option value='' selected></option>
                <? for($I=0;$I<=$totalRows_BigTable/$maxRows_BigTable;$I++)
			  
			  { echo "<option value='";
			  printf("%s?pageNum_BigTable=%d%s", $currentPage,$I,$queryString_BigTable);
			   echo "'>".($I+1)." </option>"; 
			}
		?>
              </select>
              頁 </form>
              
              <div style="float:right">
            <?php if ($pageNum_Member > 0) { // Show if not first page ?><span>
            <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, 0, $queryString_Member); ?>"><img src="../images/PageHome.gif" alt="最前頁" border=0></a> 
            </span>
			<?php } // Show if not first page ?>
            <?php if ($pageNum_Member > 0) { // Show if not first page ?><span>
            <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, max(0, $pageNum_Member - 1), $queryString_Member); ?>"><img src="../images/PagePrevious.gif" alt="上一頁" border=0></a></span> 
            <?php } // Show if not first page ?>
            <?php if ($pageNum_Member < $totalPages_Member) { // Show if not last page ?><span>
            <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, min($totalPages_Member, $pageNum_Member + 1), $queryString_Member); ?>"><img src="../images/PageNext.gif" alt="下一頁" border=0></a></span> 
            <?php } // Show if not last page ?>
            <?php if ($pageNum_Member < $totalPages_Member) { // Show if not last page ?><span>
            <a href="<?php printf("%s?pageNum_Member=%d%s", $currentPage, $totalPages_Member, $queryString_Member); ?>"><img src="../images/PageEnd.gif" alt="最終頁" border=0></a></span> 
            <?php } // Show if not last page ?>
            </div>
                      </td>
                    </tr>
                  </table>
</div>
</body>
</html>
<?php
mysqli_free_result($BigTable);
?>

