<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

  $insertSQL = sprintf("INSERT INTO ".$Table_ID."_Template (T_ID, T_Name, T_FileName, T_Category, T_Type, T_Page, T_Icon, T_Remark) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['T_ID'], "text"),
                       GetSQLValueString($_POST['T_Name'], "text"),
                       GetSQLValueString($_POST['T_FileName'], "text"),
                       GetSQLValueString($_POST['T_Category'], "text"),
                       GetSQLValueString($_POST['T_Type'], "text"),
                       GetSQLValueString($_POST['T_Page'], "text"),
                       GetSQLValueString($_POST['T_Icon'], "text"),
                       GetSQLValueString($_POST['T_Remark'], "text"));

  $updateSQL = sprintf("UPDATE ".$Table_ID."_Template SET T_Name=%s,T_FileName=%s, T_Category=%s, T_Type=%s, T_Page=%s, T_Icon=%s, T_Remark=%s  WHERE T_ID=%s",
                       GetSQLValueString($_POST['T_Name'], "text"),
					   GetSQLValueString($_POST['T_FileName'], "text"),
                       GetSQLValueString($_POST['T_Category'], "text"),
                       GetSQLValueString($_POST['T_Type'], "text"),
                       GetSQLValueString($_POST['T_Page'], "text"),
                       GetSQLValueString($_POST['T_Icon'], "text"),
                       GetSQLValueString($_POST['T_Remark'], "text"),
                       GetSQLValueString($_POST['T_ID'], "text"));

  
  if ($_POST['Action']=="ins"){
   //echo $insertSQL;
   $Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
   
$query_sql = "SELECT * FROM ".$Table_ID."_TemplateDetail where Template='".$_POST['OldTemplate']."'";
//echo $query_sql;
$data = mysqli_query($MySQL,$query_sql) or die(mysqli_error($MySQL));
$row_data = mysqli_fetch_assoc($data);

 do { 
    $insertSQL = sprintf("INSERT INTO ".$Table_ID."_TemplateDetail (KeyID, Template, FieldID, FieldTitle, Sq, Type, `Value`, MappingValue, FieldCheck ,FieldCheckValue) VALUES (%s, %s, %s, %s, %s, %s, %s, %s,%s, %s)",
                       GetSQLValueString(uniqid(mt_rand()), "text"),
                       GetSQLValueString($_POST['T_FileName'], "text"),
                       GetSQLValueString($row_data['FieldID'], "text"),
                       GetSQLValueString($row_data['FieldTitle'], "text"),
                       GetSQLValueString($row_data['Sq'], "text"),
                       GetSQLValueString($row_data['Type'], "text"),
                       GetSQLValueString($row_data['Value'], "text"),
                       GetSQLValueString($row_data['MappingValue'], "text"),
 				       GetSQLValueString($row_data['FieldCheck'], "text"),
					   GetSQLValueString($row_data['FieldCheckValue'], "text")
					   );
  
  //echo $insertSQL."<br>";
  $Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
   } while ($row_data = mysqli_fetch_assoc($data));
     
   
   
   
    
  }
  else if($_POST['Action']=="del") {
  
  $delSQL="DELETE FROM ".$Table_ID."_Template WHERE T_ID='".$_POST['T_ID']."'";
   $Result1 = mysqli_query($MySQL,$delSQL) or die(mysqli_error($MySQL)); 
  $delSQL="DELETE FROM ".$Table_ID."_TemplateDetail WHERE Template='".$_POST['T_FileName']."'";
   $Result1 = mysqli_query($MySQL,$delSQL) or die(mysqli_error($MySQL)); 
   
   
   
  }
  else {
  $Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));
}
 // $updateGoTo = "TemplateViewA.php";
 // if (isset($_SERVER['QUERY_STRING'])) {
 //   $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
 //   $updateGoTo .= $_SERVER['QUERY_STRING'];
 // }
  $updateGoTo=$RootLevel."aiadmin/main.php?Page=Z-4";
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Tempalte = "-1";
if (isset($_GET['T_ID'])) {
  $colname_Tempalte = (get_magic_quotes_gpc()) ? $_GET['T_ID'] : addslashes($_GET['T_ID']);
}

$query_Tempalte = sprintf("SELECT * FROM ".$Table_ID."_Template WHERE T_ID = '%s'", $colname_Tempalte);
$Tempalte = mysqli_query($MySQL,$query_Tempalte) or die(mysqli_error($MySQL));
$row_Tempalte = mysqli_fetch_assoc($Tempalte);
$totalRows_Tempalte = mysqli_num_rows($Tempalte);


if(  $_GET['Action']=="ins") //新增資料
{
 $row_Tempalte['T_ID']=uniqid(mt_rand());
 
}



?>

<!-- delete -->
<?php
if ($_GET['Action']=='del') {
?>
<div class="alert alert-danger mt-3" role="alert">
確定要刪除此一版型?
</div>
<?php } ?>

<!-- start -->
<div class="container">
<div class="row">
    <div class="col">
      <h3 class="mt-3 caption">新增版型</h3>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">系統管理</a></li>
        <li class="breadcrumb-item"><a href="main.php?Page=z-4">版型管理</a></li>
        <li class="breadcrumb-item active" aria-current="page">新增版型</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <!-- form start -->
      <form method="post" name="form1" action="TemplateAdd.php">
        
        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Name</label>
          <div class="col-sm-10">
            <input type="text" name="T_Name" value="<?php echo $row_Tempalte['T_Name']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_FileName</label>
          <div class="col-sm-10">
            <input type="text" name="T_FileName" value="<?php echo $row_Tempalte['T_FileName']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Category</label>
          <div class="col-sm-10">
            <input type="text" name="T_Category" value="<?php echo $row_Tempalte['T_Category']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Display" class="col-sm-2 col-form-label">T_Type</label>
          <div class="col-sm-5">目前設定: <?php echo $row_Tempalte['T_Type']; ?></div>

          <div class="col-sm-5">
            <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="T_Type" id="T_Type1" class="custom-control-input" <?php if($row_Tempalte['T_Type']=="電子報"){ echo "checked";} ?> value="電子報">
              <label class="custom-control-label" for="T_Type1">電子報</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="T_Type" id="T_Type2" class="custom-control-input" <?php if($row_Tempalte['T_Type']=="內頁"){ echo "checked";} ?> value="內頁">
              <label class="custom-control-label" for="T_Type2">內頁</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" name="T_Type" id="T_Type3" class="custom-control-input" <?php if($row_Tempalte['T_Type']=="問卷"){ echo "checked";} ?> value="問卷">
              <label class="custom-control-label" for="T_Type3">問卷</label>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Page</label>
          <div class="col-sm-10">
            <input type="text" name="T_Page" value="<?php echo $row_Tempalte['T_Page']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Icon</label>
          <div class="col-sm-10">
            <input type="text" name="T_Icon" value="<?php echo $row_Tempalte['T_Icon']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          <label for="Sq" class="col-sm-2 col-form-label">T_Remark</label>
          <div class="col-sm-10">
            <input type="text" name="T_Remark" value="<?php echo $row_Tempalte['T_Remark']; ?>" class="form-control">
          </div>
        </div>

        <div class="form-group d-flex justify-content-center">
          <?php if($_GET['Action']=='ins'){ ?>
          <input type="submit" class="btn btn-primary" value="新增">
          <?php  } else if ($_GET['Action']=='del') {  ?> 
          <input type="submit" class="btn btn-primary" value="刪除">
          <input type="button" class="btn btn-primary" value="取消" onclick="history.go(-1);">
          <?php  } else {  ?> 
          <input type="submit" class="btn btn-primary" value="更新">
          <?php } ?>
        </div>



        <input type="hidden" name="MM_update" value="form1">
        <input type="hidden" name="T_ID" value="<?php echo $row_Tempalte['T_ID']; ?>">
        <input type="hidden" name="OldTemplate" value="<?php echo $row_Tempalte['T_FileName']; ?>">
        <input type="hidden" name="Action" value="<?php echo $_GET['Action']; ?>">
      </form>
      <!-- form end -->
    </div>
  </div>  

</div>
<!-- end -->
<?php
mysqli_free_result($Tempalte);
?>
