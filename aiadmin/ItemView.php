<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php

if($Sort<>"")
{ $Sort=" order by ".$Sort." ";}
else
{ $Sort=" order by item ";}


$query_UserLogin = sprintf("SELECT * FROM ".$Table_ID."_Configure");
$query_UserLogin =$query_UserLogin.$Sort; 
//echo $query_UserLogin;
$UserLogin = mysqli_query($MySQL,$query_UserLogin) or die(mysqli_error($MySQL));
$row_UserLogin = mysqli_fetch_assoc($UserLogin);
$totalRows_UserLogin = mysqli_num_rows($UserLogin);

$MM_paramName = ""; 

// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters
// create the list of parameters which should not be maintained
$MM_removeList = "&index=";
if ($MM_paramName != "") $MM_removeList .= "&".strtolower($MM_paramName)."=";
$MM_keepURL="";
$MM_keepForm="";
$MM_keepBoth="";
$MM_keepNone="";
// add the URL parameters to the MM_keepURL string
reset ($_GET);
while (list ($key, $val) = each ($_GET)) {
	$nextItem = "&".strtolower($key)."=";
	if (!stristr($MM_removeList, $nextItem)) {
		$MM_keepURL .= "&".$key."=".urlencode($val);
	}
}
// add the URL parameters to the MM_keepURL string
if(isset($_POST)){
	reset ($_POST);
	while (list ($key, $val) = each ($_POST)) {
		$nextItem = "&".strtolower($key)."=";
		if (!stristr($MM_removeList, $nextItem)) {
			$MM_keepForm .= "&".$key."=".urlencode($val);
		}
	}
}
// create the Form + URL string and remove the intial '&' from each of the strings
$MM_keepBoth = $MM_keepURL."&".$MM_keepForm;
if (strlen($MM_keepBoth) > 0) $MM_keepBoth = substr($MM_keepBoth, 1);
if (strlen($MM_keepURL) > 0)  $MM_keepURL = substr($MM_keepURL, 1);
if (strlen($MM_keepForm) > 0) $MM_keepForm = substr($MM_keepForm, 1);
?>
<script language="javascript" >
function ConfirmDel(item1,KeyID){
 answer = confirm("確認刪除參數"+item1);
   if (answer){
     location='ItemDelF.php?KeyID='+KeyID; }
	  	
}	  

</script> 
<!-- start -->
<div class="container">
  <div class="row"> 
    <div class="col">
      <h3 class="mt-3 caption">參數設定</h3>
    </div>
  </div>

  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">網站管理</a></li>
        <li class="breadcrumb-item active" aria-current="page">參數設定</li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col table-responsive">
    <table class="table" summary="參數設定">
        <!-- <colgroup id="item"></colgroup>
        <colgroup id="value"></colgroup>
        <colgroup id="MappingValue"></colgroup>
        <colgroup id="remark"></colgroup>
        <colgroup id="Function"></colgroup> -->
        <thead class="thead-dark">
        <tr> 
        <th nowrap>
          <div class="d-flex justify-content-around">
            <span>參數名稱</span>
            <span>
              <a href="<?php echo "ItemView.php?Sort=item " ;?>%20">
              <i class="bi bi-arrow-up-circle"></i>
              </a>
              <a href="<?php echo "ItemView.php?Sort=item Desc " ;?>%20">
              <i class="bi bi-arrow-down-circle"></i>
              </a>
            </span>
          </div>
        </th>
        <th nowrap>
          <div class="d-flex justify-content-around">
            <span>設定值</span>
            <span>
              <a href="<?php echo "ItemView.php?Sort=value " ;?>%20">
              <i class="bi bi-arrow-up-circle"></i>
              </a>
              <a href="<?php echo "ItemView.php?Sort=value Desc " ;?>%20">
              <i class="bi bi-arrow-down-circle"></i>
              </a>
            </span>
          </div>
        </th>
        <th nowrap>
          <div class="d-flex justify-content-around">
            <span>轉換值</span>
            <span>
              <a href="<?php echo "ItemView.php?Sort=MappingValue " ;?>%20">
              <i class="bi bi-arrow-up-circle"></i>
              </a>
              <a href="<?php echo "ItemView.php?Sort=MappingValue Desc " ;?>%20">
              <i class="bi bi-arrow-down-circle"></i>
            </a>
            </span>
          </div>
        </th>
        <th nowrap>說明</th>	
        <th nowrap class="func">功能</th>
        </tr>
        </thead>
        <tbody>
        <?php do { ?>
        <tr> 
          <td><?php echo $row_UserLogin['item']; ?></td>
          <td><?php echo $row_UserLogin['value']; ?></td>
          <td><?php echo $row_UserLogin['MappingValue']; ?></td>
          <td><?php echo $row_UserLogin['remark']; ?></td>
          <td>
            <a class="btn btn-outline-primary btn-sm" href="<?php echo $RootLevel; ?>/aiadmin/main.php?GoPage=2-2-1&<?php echo $MM_keepURL.(($MM_keepURL!="")?"&":"")."KeyID=".$row_UserLogin['KeyID'] ?>">
            <i class="bi bi-check-circle-fill" role="img" aria-label="edit"></i> 編輯
            </a>　
          </td>
        </tr>
        <?php } while ($row_UserLogin = mysqli_fetch_assoc($UserLogin)); ?>
      </tbody>
    </table>

    </div>
  </div>

</div>
<!-- end -->
<?php
mysqli_free_result($UserLogin);
?>

