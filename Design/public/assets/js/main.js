/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src@4.0/assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src@4.0/assets/js/main.js":
/*!***********************************!*\
  !*** ./src@4.0/assets/js/main.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(window).on('load', function () {
  $('.p-index-loading').fadeOut();
}); // ajax 配合JQ3 引入設置

$(document).ready(function () {
  var el = document.querySelector('html[lang="zh-tw"]');

  if (el) {
    if ($("#header")) {
      $.ajax({
        url: "ajax/_header.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#header").html(data);
        headerFunction();
        goToAnchor();
      });
    }

    if ($("#sideLink")) {
      $.ajax({
        url: "ajax/_sideLink.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#sideLink").html(data);
        toggleSideLink();
      });
    }

    if ($("#footer")) {
      $.ajax({
        url: "ajax/_footer.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#footer").html(data);
        goTop();
        goTopSticky();
        cookieBar();
      });
    }
  }
}); //英文版的ajax

$(document).ready(function () {
  var el = document.querySelector('html[lang="en"]');

  if (el) {
    if ($("#headerEn")) {
      $.ajax({
        url: "../ajax/_header_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#headerEn").html(data);
        headerFunction();
        goToAnchor();
      });
    }

    if ($("#sideLinkEn")) {
      $.ajax({
        url: "../ajax/_sideLink_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#sideLinkEn").html(data);
        toggleSideLink();
      });
    }

    if ($("#footerEn")) {
      $.ajax({
        url: "../ajax/_footer_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#footerEn").html(data);
        goTop();
        goTopSticky();
      });
    }
  }
});

function toolsListener() {
  window.addEventListener("keydown", function (e) {
    if (e.keyCode === 9) {
      document.body.classList.remove("js-useMouse");
      document.body.classList.add("js-useKeyboard");
    }
  });
  window.addEventListener("mousedown", function (e) {
    document.body.classList.remove("js-useKeyboard");
    document.body.classList.add("js-useMouse");
  });
}

function toggleSideLink() {
  var elTw = document.querySelector('html[lang="zh-tw"]');
  var elEn = document.querySelector('html[lang="en"]');
  document.querySelector("#sideLinkToggleBtn").addEventListener("click", function () {
    this.classList.toggle("js-sideLinkOpened");

    if (elTw) {
      document.querySelector("#sideLink").classList.toggle("js-sideLinkOpened");
    }

    if (elEn) {
      document.querySelector("#sideLinkEn").classList.toggle("js-sideLinkOpened");
    }

    document.querySelector("#page").classList.toggle("js-sideLinkOpened");
    document.querySelector("body").classList.toggle("overflow-hidden"); //打開sideMenu時，把展開的menu關掉

    document.documentElement.classList.remove("js-menuOpened");
    document.querySelector(".l-header-hamburger").classList.remove("js-menuOpened");
    document.querySelector(".l-header-menu").classList.remove("js-menuOpened"); //打開sideMenu時，把gotop推到旁邊去

    document.querySelector("#sideLinkFooter").classList.toggle("js-sideLinkOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= 1200) {
      document.querySelector("#sideLinkToggleBtn").classList.remove("js-sideLinkOpened");

      if (elTw) {
        document.querySelector("#sideLink").classList.remove("js-sideLinkOpened");
      }

      if (elEn) {
        document.querySelector("#sideLinkEn").classList.remove("js-sideLinkOpened");
      }

      document.querySelector("#page").classList.remove("js-sideLinkOpened");
      document.querySelector("body").classList.remove("overflow-hidden"); //[移除]打開sideMenu時，把gotop推到旁邊去

      document.querySelector("#sideLinkFooter").classList.remove("js-sideLinkOpened");
    }
  });
}

function goTop() {
  document.querySelector("#goTop").onclick = function (e) {
    e.preventDefault();
    $("html, body").animate({
      scrollTop: 0
    }, 500);
  };
}

function goTopSticky() {
  var el = document.querySelector("#sideLinkFooter"); //計算footer目前高度

  var footerHeightVar = document.querySelector(".l-footer").clientHeight; // console.log(`footerHeightVar + ${footerHeightVar}`);
  //當下網頁高度(整頁長)

  var scrollHeightVar = document.documentElement.scrollHeight; // console.log(`scrollHeightVar + ${scrollHeightVar}`);
  //當下畫面高度(window height)

  var windowHeightVar = window.innerHeight; // console.log(`windowHeightVar + ${windowHeightVar}`);
  //捲動中的畫面最上端與網頁頂端之間的距離

  var scrollTopVar = document.documentElement.scrollTop; // console.log(`scrollTopVar + ${scrollTopVar}`);
  // console.log(windowHeightVar + scrollTopVar + footerHeightVar);
  //如果整頁長==畫面高度+捲動高度

  if (el) {
    if (scrollHeightVar <= windowHeightVar + scrollTopVar + footerHeightVar) {
      el.classList.remove("js-sticky");
    } else {
      el.classList.add("js-sticky");
    }
  }
}

function goToAnchor() {
  var headerHeight = "";
  window.addEventListener("resize", function () {
    return headerHeight = $(".l-header").height();
  });
  $('.js-goToAnchor').click(function (e) {
    e.preventDefault();
    var target = $(this).attr('href');
    var targetPos = $(target).offset().top;
    var headerHeight = $(".l-header").height();
    $('html,body').animate({
      scrollTop: targetPos - headerHeight
    }, 1000);
    var trigger02 = document.querySelector("#hamburger");
    var target02 = document.querySelector("#menu");
    trigger02.classList.remove("js-menuOpened");
    target02.classList.remove("js-menuOpened");
    document.documentElement.classList.remove("js-menuOpened");
  });
} //風琴用function


function Accordion(el, target, siblings) {
  this.el = el;
  this.target = target;
  this.siblings = siblings;
}

Accordion.prototype.init = function () {
  var triggers = document.querySelectorAll(this.el);
  var target = this.target;
  var siblings = this.siblings;
  Array.prototype.slice.call(triggers).forEach(function (trigger) {
    trigger.addEventListener("click", function () {
      event.preventDefault();

      if (siblings == true) {
        if (this.querySelector(target) !== null) {
          // this.querySelector(target).classList.toggle(
          //   "js-accordionExpended"
          // );
          //為了要能在打開一個時，其他的收合，這邊改用jQuery==>
          $(this).find(target).toggleClass("js-accordionExpended"); // 增加藍線框效果

          $(this).toggleClass("c-accordion-btn-focus");
          $(this).siblings().find(target).removeClass("js-accordionExpended"); // 增加藍線框效果

          $(this).siblings().removeClass("c-accordion-btn-focus");
        } // document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        //為了要能在打開一個時，其他的收合，這邊改用jQuery==>


        var att = $(trigger).attr("data-target");
        $(att).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended"); // $(`${trigger.getAttribute("data-target")}`).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended");
        //用了jQuery之後，這組會被樓上那個綁在一起，所以可以省略
        // trigger.classList.toggle("js-accordionExpended");
      } else {
        if (this.querySelector(target) !== null) {
          this.querySelector(target).classList.toggle("js-accordionExpended");
        }

        document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        trigger.classList.toggle("js-accordionExpended");
      }
    });
  });
};

function nodeListToArray(nodeListCollection) {
  return Array.prototype.slice.call(nodeListCollection);
} // const nodeListToArray = (nodeListCollection) => Array.prototype.slice.call(nodeListCollection);

/**
 * 
 * @param {*} el :class name
 * @param {*} target :被影響到的目標
 * @param {*} mediaQuery :斷點設定
 * @說明 "el"與"target" toggle 一個叫js-active的class要用出什麼效果，端看你怎麼寫js-active的css效果展開或收合什麼東西
 */


function toggleVisiable(el, target, mediaQuery) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);

  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        this.classList.toggle("js-active");
        target.classList.toggle("js-active");
        var hasMediaQuery = mediaQuery;

        if (hasMediaQuery !== "") {
          var isMobile = window.innerWidth < mediaQuery;

          if (isMobile) {
            document.documentElement.classList.toggle("js-functionMenuOpened");
          }
        } else {
          document.documentElement.classList.remove("js-functionMenuOpened");
        }

        window.addEventListener("resize", function () {
          if (window.innerWidth >= mediaQuery) {
            document.documentElement.classList.remove("js-functionMenuOpened");
          }
        });
      });
    });
  }
}
/*el=觸發對象(開關)*/

/*target=被控制的物件(燈)*/


function clickConfirm(el, target) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);

  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        target.classList.remove("js-active");
        document.documentElement.classList.remove("js-functionMenuOpened");
      });
    });
  }
} //↑↑↑通用function---------------------


function createAccordion() {
  if (document.querySelector('.c-accordion-btn')) {
    new Accordion(".c-accordion-btn", ".c-accordion-btn-icon", true).init();
  }
} // TODO: 想辦法這個全域變數要處理一下


var str = 0;

function showAllTab() {
  if (window.innerWidth > 992 && str == 1) {
    $(".tab-pane").removeClass("show active").first().addClass("show active");
    $(".c-tab-linkE").removeClass("active").parent().first().find(".c-tab-linkE").addClass("active");
    str = 0;
  }

  if (window.innerWidth < 992 && str == 0) {
    $(".tab-pane").addClass("show active");
    str = 1;
  } // console.log(str);

}

function togglepFilterSubmenu() {
  // TODO: 有空試試看寫一個手機版可以針對內容高度增加showmore，並且記錄此showmore已經點開過，不會因為rwd就又計算一次
  //手機版顯示全部
  if (document.querySelector('.p-products-search-showMore')) {
    new Accordion(".p-products-search-showMore", undefined, false).init();
  }

  var triggers = document.querySelectorAll(".c-accordion-btn");
  nodeListToArray(triggers).forEach(function (trigger) {
    var el = document.querySelector(trigger.getAttribute("data-target"));
    trigger.addEventListener("click", function () {
      if (el.querySelector(".p-products-search-showMore")) {
        el.querySelector(".p-products-search-showMore").classList.remove("js-accordionExpended");
        el.querySelector(".c-accordion-content-layout").classList.remove("js-accordionExpended");
      }
    });
  }); //手機版開關選單
  //商品專區

  toggleVisiable(".close", ".p-products-search", "");
  toggleVisiable(".p-products-search-btn", ".p-products-search", 992);
  clickConfirm("#js-confirm", ".p-products-search"); //門市查詢

  toggleVisiable(".v-dropdown-btn", ".v-dropdown-menu", 992);
  toggleVisiable(".close", ".v-dropdown-menu", "");
  clickConfirm("#js-confirm", ".v-dropdown-menu"); // if ($(".v-dropdown-btn").hasClass("js-active")) {
  //   document.querySelector("body").addEventListener("click", function () {
  //     document.querySelector(".v-dropdown-btn").classList.remove("js-active");
  //     document.querySelector(".v-dropdown-menu").classList.remove("js-active");
  //   });
  // }
}

function confirmIfDoubleMenuOpened(el, mediaQuery) {
  if (window.innerWidth < mediaQuery && $(el).hasClass("js-active") && document.documentElement.classList == "") {
    document.documentElement.classList.add("js-menuOpened");
  }
} // 輪播相關--------------------------


function Slick(el, slidesToShow, slidesPerRow, rows, responsive) {
  this.el = el;
  this.slidesToShow = slidesToShow;
  this.slidesPerRow = slidesPerRow;
  this.rows = rows;
  this.responsive = responsive;
}

;

Slick.prototype.init = function () {
  $(this.el + " .v-slick").slick({
    arrows: true,
    slidesToShow: this.slidesToShow,
    slidesPerRow: this.slidesPerRow,
    rows: this.rows,
    prevArrow: "<button type=\"button\" class=\"v-slick-arrows-prev\"><i class=\"fal fa-angle-left\"></i></button>",
    nextArrow: "<button type=\"button\" class=\"v-slick-arrows-next\"><i class=\"fal fa-angle-right\"></i></button>",
    // autoplay: true,
    // lazyLoad: "progressive",
    responsive: this.responsive
  });
};

function setStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;

    if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
      trigger.style.height = "".concat(totalNum, "px");
    } else if (window.innerWidth > 1200) {
      trigger.style.height = "";
      trigger.classList.remove('js-active');
      trigger.querySelector("i").classList.remove("js-submenuOpened");
    }

    window.addEventListener("resize", function () {
      if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
        trigger.style.height = "".concat(totalNum, "px");
      } else if (window.innerWidth > 1200) {
        trigger.style.height = "";
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      }
    });
  });
}

function toggleStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;
    trigger.querySelector("td:first-child").addEventListener('click', function () {
      if (window.innerWidth < 1200 && trigger.style.height == "") {
        trigger.style.height = "".concat(totalNum, "px");
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      } else {
        trigger.style.height = "";
        trigger.classList.add("js-active");
        trigger.querySelector("i").classList.add("js-submenuOpened");
      }
    });
  });
} // TODO: 有空看一下為什麼new第三個就出事了


function createSlicks() {
  if (document.querySelector(".v-slick")) {
    var targets = ["#news", "#recommandation"];
    targets.forEach(function (target) {
      new Slick(target, 4, 1, 1, [{
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false
        }
      }]).init();
    });
    new Slick("#video", 1, 2, 2, [{
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1
      }
    }, {
      breakpoint: 576,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1,
        arrows: false
      }
    }]).init();
  }
}

function rionetSlick() {
  var el = ".v-rionetSlick";

  if (document.querySelector(el)) {
    $(el).slick({
      // centerMode: true,
      // centerPadding: '60px',
      slidesToShow: 4,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          dots: true
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false,
          // variableWidth: true,
          dots: true
        }
      }]
    }); //   $('.v-tabBtnSlick-prev').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickPrev');
    //  });
    //   $('.v-tabBtnSlick-next').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickNext');
    //  });
  }
}

function tabBtnSlick() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length; //如果總數大於６就用６， ２０２２０２１５客戶要求多一個....
    //var slidesToShowNum = tabLength > 6 ? 6 : tabLength;

    var slidesToShowNum = tabLength;
    $(el).slick({
      slidesToShow: slidesToShowNum,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          variableWidth: false // slidesToScroll: 5,
          // infinite: true,
          // centerMode: false,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          variableWidth: false // infinite: true,
          // centerMode: false,

        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          // infinite: false,
          arrows: false
        }
      }]
    });
  }
}

function tabBtnSlickFixed() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length; // var slidesToShowNum 
    //原本是小於６就加上致中， ２０２２０２１５客戶要求多一個....

    if (window.innerWidth >= 1200 && tabLength <= 7) {
      $(".v-tabBtnSlick .slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    }

    if (window.innerWidth >= 768 && window.innerWidth < 1200 && tabLength < 5) {
      $(".v-tabBtnSlick .slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    } // if (window.innerWidth < 768) {
    // document.querySelector('.slick-track').classList.remove('text-md-center');
    // }

  }
}

function tabBtnSlickMembers() {
  var el = ".v-tabBtnSlick-members";

  if (document.querySelector(el)) {
    $(el).slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
          variableWidth: false
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          arrows: false
        }
      }]
    });
  }
}

function tabBtnSlickFixedMembers() {
  var el = ".v-tabBtnSlick-members";

  if (document.querySelector(el)) {
    if (window.innerWidth >= 768) {
      $(".v-tabBtnSlick-members .slick-track").addClass("text-md-center");
    }
  }
} // End 輪播相關--------------------------


function warrantyLink() {
  if (document.querySelectorAll(".p-index-warranty-link").length) {
    var triggers = document.querySelectorAll(".p-index-warranty-link");
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".c-btn").classList.add("js-btnHover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".c-btn").classList.remove("js-btnHover");
      });
    });
  }
} //手機版漢堡選單


function toggleMobileMenu(mediaQuery) {
  var trigger = document.querySelector("#hamburger");
  var target = document.querySelector("#menu");
  trigger.addEventListener("click", function () {
    this.classList.toggle("js-menuOpened");
    target.classList.toggle("js-menuOpened");
    document.documentElement.classList.toggle("js-menuOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      trigger.classList.remove("js-menuOpened");
      target.classList.remove("js-menuOpened");
      document.documentElement.classList.remove("js-menuOpened");
    }
  });
} //手機版風琴折疊選單


function toggleMobileSubmenu(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-link");
  var targets = document.querySelectorAll(".l-header-submenu--hidden");

  if (triggers !== null) {
    nodeListToArray(triggers).forEach(function (trigger) {
      if (trigger.nextElementSibling !== null) {
        trigger.addEventListener("click", function (e) {
          e.preventDefault();
          var target = trigger.nextElementSibling;

          if (window.innerWidth < mediaQuery) {
            target.classList.toggle("js-submenuOpened");
            trigger.querySelector("i").classList.toggle("js-submenuOpened"); //為了開一個關一個，用jQuery加寫

            $(target).parents().siblings().find(".l-header-submenu , i").removeClass("js-submenuOpened");
          }
        });
      }
    });
  }

  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      nodeListToArray(triggers).forEach(function (trigger) {
        var target = trigger.nextElementSibling;
        target.classList.remove("js-submenuOpened");
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      });
    }
  });
}

function sibingMobileSubmenu() {
  $(".l-header-menu-item").on("click", function () {
    $(this).siblings().find(".l-header-submenu").removeClass("js-submenuOpened");
  });
}

function togglePcHoverState(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-item");
  nodeListToArray(triggers).forEach(function (trigger) {
    if (trigger.querySelector(".l-header-submenu") !== null) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".l-header-menu-link").classList.add("js-hover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".l-header-menu-link").classList.remove("js-hover");
      });
    }
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth < mediaQuery) {
      Array.prototype.slice.call(document.querySelectorAll(".l-header-menu-link")).forEach(function (item) {
        item.classList.remove("js-hover");
      });
    }
  });
}

function headerFunction() {
  var breakpoint = 1200;
  toggleMobileMenu(breakpoint);
  toggleMobileSubmenu(breakpoint);
  togglePcHoverState(breakpoint);
}

function lazyLoad() {
  if (document.querySelector("img[data-src]")) {
    var observer = lozad();
    observer.observe();

    if (-1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > 0) {
      var iframe = document.querySelector("iframe");
      iframe.setAttribute("src", "https://www.youtube.com/embed/O7pNpR3Py68");
    }
  }
} //計算「最新消息」、「人氣推薦」這類的卡片列表的標題字數，讓大家的高度一樣


function autoFixHeight(con) {
  var el = document.querySelectorAll(con);
  var thisHeight = -1;
  var maxHeight = -1;
  var breakpoint = 768; //為了ie不支援nodelist的forEach修正

  nodeListToArray(el).forEach(function (item) {
    item.style.height = ""; //清空之前的style

    thisHeight = item.clientHeight; //取得已經減過高度的

    maxHeight = maxHeight > thisHeight ? maxHeight : thisHeight;
  });

  if (document.body.clientWidth > breakpoint) {
    nodeListToArray(el).forEach(function (item) {
      item.style.height = "".concat(maxHeight, "px");
    });
  }
}

function textHide(num, con) {
  var el = document.querySelectorAll(con); //ie才執行這個超過字數增加刪節號，其他瀏覽器靠css語法即可
  // if (
  //   navigator.userAgent.indexOf("MSIE") !== -1 ||
  //   navigator.appVersion.indexOf("Trident/") > 0
  // ) {
  //   Array.prototype.slice.call(el).forEach(function (item) {
  //     var txt = item.innerText;
  //     if (txt.length > num) {
  //       txtContent = txt.substring(0, num - 1) + "...";
  //       item.innerHTML = txtContent;
  //     }
  //   });
  // }

  nodeListToArray(el).forEach(function (item) {
    var txt = item.innerText;

    if (txt.length > num) {
      txtContent = txt.substring(0, num - 1) + "...";
      item.innerHTML = txtContent;
    } else {
      txtContent = txt.substring(0, num) + "...";
      item.innerHTML = txtContent;
    }
  });
  autoFixHeight(con);
}

function clearCheckBox(el, target) {
  if (document.querySelector(el)) {
    var trigger = document.querySelector(el);
    var targets = document.querySelectorAll(target);
    trigger.addEventListener("click", function () {
      event.preventDefault();
      trigger.blur();
      Array.prototype.slice.call(targets).forEach(function (trigger) {
        trigger.checked = false;
      });
    });
  }
}

function cardContentFunction() {
  if (document.querySelector(".c-card-body-title") !== null) {
    autoFixHeight(".c-card-body-title");
  } //人氣推薦內文字數限制


  if (document.querySelector(".c-card-body-txt") !== null) {
    textHide(48, ".c-card-body-txt");
  }
}

function toolTips() {
  $('[data-toggle="tooltip"]').tooltip({
    placement: 'right',
    trigger: 'hover',
    offset: 30
  });
  $('[data-toggle="tooltip"]').on('inserted.bs.tooltip', function () {
    var getTootipsId = "#" + $(this).attr("aria-describedby"); // console.log(getTootipsId);
    // console.log($(this).attr("data-original-title") == "德國蔡司數位影像系統");

    if ($(this).attr("data-original-title") == "德國蔡司數位影像系統") {
      $(getTootipsId).find('.tooltip-inner').html("德國蔡司<br>數位影像系統");
    }

    if ($(this).attr("data-original-title") == "法國依視路全方位視覺檢測系統") {
      $(getTootipsId).find('.tooltip-inner').html("法國依視路<br>全方位視覺檢測系統");
    }
  });
}

function aos() {
  if (document.querySelector("[data-aos]")) {
    AOS.init({
      once: true
    });
  }
} // function ytHandler() {
//   if (document.querySelector("[data-url]")) {
//     var triggers = document.querySelectorAll("[data-url]");
//     var target = document.querySelector("iframe");
//     var index = 0;
//     Array.prototype.slice.call(triggers).forEach(function (trigger) {
//       trigger.setAttribute("index", index++);
//       trigger.onclick = function () {
//         var triggerIndex = trigger.getAttribute("index");
//         $('#slick_ad').slick('slickGoTo', triggerIndex);
//         target.setAttribute("src", trigger.getAttribute("data-url"));
//         //alert(+trigger.getAttribute("data-url"));
//         trigger.classList.add("js-active");
//         Array.prototype.slice.call(triggers).filter(function (item) {
//           return item !== trigger;
//         }).forEach(function (item) {
//           item.classList.remove("js-active");
//         });
//       };
//     });
//   }
// }
//sam改寫過的↓↓↓↓↓↓


function ytHandler() {
  if (document.querySelector("[data-url]")) {
    var triggers = document.querySelectorAll("[data-url]");
    var target = document.querySelector("iframe"); //對應的ad key 可以自行塞入你要的key

    var index = 0;
    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      //塞入對應的AD key值
      trigger.setAttribute("index", index++); //ad被點擊事件

      trigger.onclick = function () {
        /*=====此處 為抓DB AD廣告圖片資料=====
        var triggerIndex = trigger.getAttribute("index"); 抓取ad key data
            $.get("url",{}, function(result){
          $("#slick_ad").html("請塞入廣告圖片html資料");
            $('#slick_ad').slick('slickGoTo', 0);
            })
        ========================*/
        var triggerIndex = trigger.getAttribute("index");
        $('#slick_ad').slick('slickGoTo', triggerIndex);
        target.setAttribute("src", trigger.getAttribute("data-url")); //alert(+trigger.getAttribute("data-url"));

        trigger.classList.add("js-active");
        Array.prototype.slice.call(triggers).filter(function (item) {
          return item !== trigger;
        }).forEach(function (item) {
          item.classList.remove("js-active");
        });
      };
    });
  }
}

function cardJustifyContent() {
  var el = $(".js-cardJustifyContent");

  if (el) {
    for (var i = 0; i < el.length; i++) {
      var elChildrenLength = el.eq(i).children('div').length; // console.log(elChildrenLength);

      if (elChildrenLength <= 2) {
        el.eq(i).addClass("justify-content-center");
      } else {
        el.eq(i).removeClass("justify-content-center");
      }
    }
  }
}

function storeFilterNotification(inputContainer, targetEl) {
  var el = document.querySelector(inputContainer);
  var target = document.querySelector(targetEl);

  if (el) {
    // console.log(inputContainer + " + " + target);
    var triggers = el.querySelectorAll("input[type='checkbox']"); // console.log(triggers);

    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        var checkedNum = el.querySelectorAll("input[type=checkbox]:checked").length; // console.log(checkedNum);

        if (checkedNum > 0) {
          target.classList.add("js-inputChecked");
        } else {
          target.classList.remove("js-inputChecked");
        }
      });
    });
    var clearAllBtnEl = document.querySelector("#js-clearCheckBoxes");
    clearAllBtnEl.addEventListener("click", function () {
      target.classList.remove("js-inputChecked");
    });
  }
} //滾軸式日期選單


function dateMobiscroll() {
  var el = document.querySelector("#date");

  if ($(el)) {
    $(el).mobiscroll().date({
      theme: $.mobiscroll.defaults.theme,
      // Specify theme like: theme: 'ios' or omit setting to use default 
      mode: 'mixed',
      // Specify scroller mode like: mode: 'mixed' or omit setting to use default 
      display: 'modal',
      // Specify display mode like: display: 'bottom' or omit setting to use default 
      lang: 'zh' // Specify language like: lang: 'pl' or omit setting to use default 

    });
  }
} //下拉選單跳轉超連結


function selectURL() {
  var el = document.querySelector(".js-selectURL");

  if (el) {
    el.addEventListener("change", function () {
      // window.open(this.options[this.selectedIndex].value);
      location.href = this.options[this.selectedIndex].value;
    });
  }
}

function cookieBar() {
  if (!getCookieValue('cookieConsentRead')) {
    $(".l-footer-cookie-bar").removeClass('d-none');
    $(".l-footer-cookie-btn").click(function () {
      $(".l-footer-cookie-bar").addClass('d-none');
      var expires = new Date(); //放30年才消失，應該夠久了

      expires.setFullYear(expires.getFullYear() + 30);
      setCookie("cookieConsentRead", "true", expires);
    });
  } else {
    $(".l-footer-cookie-bar").addClass('d-none');
  }
} // cookie 基本格式為 name=value [;expires=date] [;path=path] [;domain=domain] [;secure]
// expires=date 設定 cookie 的有效期限至 date 為止。
// path=path 設定可讀取 cookie 的最上層目錄。
// domain=domain 設定可讀取 cookie 的網域。
// secure設定是否用加密的方式來傳輸 cookie。
// 此函式僅用來設定cookie有效期限用，其他的參數沒有做設定。
// 參數 expire 用來設定 cookie 的有效期限。
// 此函式假定 name 不包含分號、逗號、或空白。


function setCookie(name, value, expire) {
  document.cookie = name + "=" + value + "; expires=" + expire.toGMTString();
} //篩選出cookie名稱


function getCookieValue(key) {
  var theCookieEntry = document.cookie.split(';').map(function (data, index) {
    return data.split('=');
  }).filter(function (cookieEntry) {
    return cookieEntry[0].trim() == key;
  })[0];
  return !!theCookieEntry ? theCookieEntry[1] : null;
} // //呼叫function-網頁載入完成後


$(document).ready(function () {
  selectURL();
  toolsListener();
  createSlicks();
  tabBtnSlick();
  tabBtnSlickFixed(); //會員權益專用

  tabBtnSlickMembers();
  tabBtnSlickFixedMembers();
  rionetSlick();
  warrantyLink();
  lazyLoad();
  createAccordion();
  aos();
  ytHandler();
  togglepFilterSubmenu();
  clearCheckBox("#js-clearCheckBoxes", "input[type='checkbox']");
  setStoreTableHeightAtMobile();
  toolTips();
  cardJustifyContent();
  storeFilterNotification(".v-dropdown-menu", ".v-dropdown-btn");
  storeFilterNotification(".p-products-search", ".p-products-search-btn");
  toggleStoreTableHeightAtMobile();
  dateMobiscroll();
  goToAnchor();
  autoFixHeight(".js-titleFixedHeight");
});

window.onload = function () {
  cardContentFunction();
  showAllTab();
}; //呼叫function-視窗大小變更


$(window).resize(function () {
  cardContentFunction();
  showAllTab();
  confirmIfDoubleMenuOpened(".p-products-search", 992);
  tabBtnSlickFixed();
  autoFixHeight(".js-titleFixedHeight");
}); //呼叫function-捲動

$(window).scroll(function () {
  goTopSticky();
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjQDQuMC9hc3NldHMvanMvbWFpbi5qcyJdLCJuYW1lcyI6WyIkIiwid2luZG93Iiwib24iLCJmYWRlT3V0IiwiZG9jdW1lbnQiLCJyZWFkeSIsImVsIiwicXVlcnlTZWxlY3RvciIsImFqYXgiLCJ1cmwiLCJtZXRob2QiLCJkYXRhVHlwZSIsImRvbmUiLCJkYXRhIiwiaHRtbCIsImhlYWRlckZ1bmN0aW9uIiwiZ29Ub0FuY2hvciIsInRvZ2dsZVNpZGVMaW5rIiwiZ29Ub3AiLCJnb1RvcFN0aWNreSIsImNvb2tpZUJhciIsInRvb2xzTGlzdGVuZXIiLCJhZGRFdmVudExpc3RlbmVyIiwiZSIsImtleUNvZGUiLCJib2R5IiwiY2xhc3NMaXN0IiwicmVtb3ZlIiwiYWRkIiwiZWxUdyIsImVsRW4iLCJ0b2dnbGUiLCJkb2N1bWVudEVsZW1lbnQiLCJpbm5lcldpZHRoIiwib25jbGljayIsInByZXZlbnREZWZhdWx0IiwiYW5pbWF0ZSIsInNjcm9sbFRvcCIsImZvb3RlckhlaWdodFZhciIsImNsaWVudEhlaWdodCIsInNjcm9sbEhlaWdodFZhciIsInNjcm9sbEhlaWdodCIsIndpbmRvd0hlaWdodFZhciIsImlubmVySGVpZ2h0Iiwic2Nyb2xsVG9wVmFyIiwiaGVhZGVySGVpZ2h0IiwiaGVpZ2h0IiwiY2xpY2siLCJ0YXJnZXQiLCJhdHRyIiwidGFyZ2V0UG9zIiwib2Zmc2V0IiwidG9wIiwidHJpZ2dlcjAyIiwidGFyZ2V0MDIiLCJBY2NvcmRpb24iLCJzaWJsaW5ncyIsInByb3RvdHlwZSIsImluaXQiLCJ0cmlnZ2VycyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJBcnJheSIsInNsaWNlIiwiY2FsbCIsImZvckVhY2giLCJ0cmlnZ2VyIiwiZXZlbnQiLCJmaW5kIiwidG9nZ2xlQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImF0dCIsImdldEF0dHJpYnV0ZSIsIm5vZGVMaXN0VG9BcnJheSIsIm5vZGVMaXN0Q29sbGVjdGlvbiIsInRvZ2dsZVZpc2lhYmxlIiwibWVkaWFRdWVyeSIsImhhc01lZGlhUXVlcnkiLCJpc01vYmlsZSIsImNsaWNrQ29uZmlybSIsImNyZWF0ZUFjY29yZGlvbiIsInN0ciIsInNob3dBbGxUYWIiLCJmaXJzdCIsImFkZENsYXNzIiwicGFyZW50IiwidG9nZ2xlcEZpbHRlclN1Ym1lbnUiLCJ1bmRlZmluZWQiLCJjb25maXJtSWZEb3VibGVNZW51T3BlbmVkIiwiaGFzQ2xhc3MiLCJTbGljayIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1BlclJvdyIsInJvd3MiLCJyZXNwb25zaXZlIiwic2xpY2siLCJhcnJvd3MiLCJwcmV2QXJyb3ciLCJuZXh0QXJyb3ciLCJzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUiLCJ0cmlnZ2VyVGQiLCJudW0wMSIsIm51bTAyIiwibnVtMDMiLCJ0b3RhbE51bSIsImNvbnRhaW5zIiwic3R5bGUiLCJ0b2dnbGVTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUiLCJjcmVhdGVTbGlja3MiLCJ0YXJnZXRzIiwiYnJlYWtwb2ludCIsInNldHRpbmdzIiwicmlvbmV0U2xpY2siLCJsYXp5TG9hZCIsImRvdHMiLCJ0YWJCdG5TbGljayIsInRhYkxlbmd0aCIsImxlbmd0aCIsInNsaWRlc1RvU2hvd051bSIsInNsaWRlc1RvU2Nyb2xsIiwiaW5maW5pdGUiLCJ2YXJpYWJsZVdpZHRoIiwidGFiQnRuU2xpY2tGaXhlZCIsInRhYkJ0blNsaWNrTWVtYmVycyIsInRhYkJ0blNsaWNrRml4ZWRNZW1iZXJzIiwid2FycmFudHlMaW5rIiwidG9nZ2xlTW9iaWxlTWVudSIsInRvZ2dsZU1vYmlsZVN1Ym1lbnUiLCJuZXh0RWxlbWVudFNpYmxpbmciLCJwYXJlbnRzIiwic2liaW5nTW9iaWxlU3VibWVudSIsInRvZ2dsZVBjSG92ZXJTdGF0ZSIsIml0ZW0iLCJvYnNlcnZlciIsImxvemFkIiwib2JzZXJ2ZSIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsImluZGV4T2YiLCJhcHBWZXJzaW9uIiwiaWZyYW1lIiwic2V0QXR0cmlidXRlIiwiYXV0b0ZpeEhlaWdodCIsImNvbiIsInRoaXNIZWlnaHQiLCJtYXhIZWlnaHQiLCJjbGllbnRXaWR0aCIsInRleHRIaWRlIiwibnVtIiwidHh0IiwiaW5uZXJUZXh0IiwidHh0Q29udGVudCIsInN1YnN0cmluZyIsImlubmVySFRNTCIsImNsZWFyQ2hlY2tCb3giLCJibHVyIiwiY2hlY2tlZCIsImNhcmRDb250ZW50RnVuY3Rpb24iLCJ0b29sVGlwcyIsInRvb2x0aXAiLCJwbGFjZW1lbnQiLCJnZXRUb290aXBzSWQiLCJhb3MiLCJBT1MiLCJvbmNlIiwieXRIYW5kbGVyIiwiaW5kZXgiLCJ0cmlnZ2VySW5kZXgiLCJmaWx0ZXIiLCJjYXJkSnVzdGlmeUNvbnRlbnQiLCJpIiwiZWxDaGlsZHJlbkxlbmd0aCIsImVxIiwiY2hpbGRyZW4iLCJzdG9yZUZpbHRlck5vdGlmaWNhdGlvbiIsImlucHV0Q29udGFpbmVyIiwidGFyZ2V0RWwiLCJjaGVja2VkTnVtIiwiY2xlYXJBbGxCdG5FbCIsImRhdGVNb2Jpc2Nyb2xsIiwibW9iaXNjcm9sbCIsImRhdGUiLCJ0aGVtZSIsImRlZmF1bHRzIiwibW9kZSIsImRpc3BsYXkiLCJsYW5nIiwic2VsZWN0VVJMIiwibG9jYXRpb24iLCJocmVmIiwib3B0aW9ucyIsInNlbGVjdGVkSW5kZXgiLCJ2YWx1ZSIsImdldENvb2tpZVZhbHVlIiwiZXhwaXJlcyIsIkRhdGUiLCJzZXRGdWxsWWVhciIsImdldEZ1bGxZZWFyIiwic2V0Q29va2llIiwibmFtZSIsImV4cGlyZSIsImNvb2tpZSIsInRvR01UU3RyaW5nIiwia2V5IiwidGhlQ29va2llRW50cnkiLCJzcGxpdCIsIm1hcCIsImNvb2tpZUVudHJ5IiwidHJpbSIsIm9ubG9hZCIsInJlc2l6ZSIsInNjcm9sbCJdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBQSxDQUFDLENBQUNDLE1BQUQsQ0FBRCxDQUFVQyxFQUFWLENBQWEsTUFBYixFQUFxQixZQUFZO0FBQy9CRixHQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQkcsT0FBdEI7QUFDRCxDQUZELEUsQ0FHQTs7QUFDQUgsQ0FBQyxDQUFDSSxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzVCLE1BQUlDLEVBQUUsR0FBR0YsUUFBUSxDQUFDRyxhQUFULENBQXVCLG9CQUF2QixDQUFUOztBQUNBLE1BQUlELEVBQUosRUFBUTtBQUNOLFFBQUlOLENBQUMsQ0FBQyxTQUFELENBQUwsRUFBa0I7QUFDaEJBLE9BQUMsQ0FBQ1EsSUFBRixDQUFPO0FBQ0xDLFdBQUcsRUFBRSxtQkFEQTtBQUVMQyxjQUFNLEVBQUUsS0FGSDtBQUdMQyxnQkFBUSxFQUFFO0FBSEwsT0FBUCxFQUlHQyxJQUpILENBSVEsVUFBVUMsSUFBVixFQUFnQjtBQUN0QmIsU0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFhYyxJQUFiLENBQWtCRCxJQUFsQjtBQUNBRSxzQkFBYztBQUNkQyxrQkFBVTtBQUNYLE9BUkQ7QUFTRDs7QUFDRCxRQUFJaEIsQ0FBQyxDQUFDLFdBQUQsQ0FBTCxFQUFvQjtBQUNsQkEsT0FBQyxDQUFDUSxJQUFGLENBQU87QUFDTEMsV0FBRyxFQUFFLHFCQURBO0FBRUxDLGNBQU0sRUFBRSxLQUZIO0FBR0xDLGdCQUFRLEVBQUU7QUFITCxPQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3RCYixTQUFDLENBQUMsV0FBRCxDQUFELENBQWVjLElBQWYsQ0FBb0JELElBQXBCO0FBQ0FJLHNCQUFjO0FBQ2YsT0FQRDtBQVFEOztBQUNELFFBQUlqQixDQUFDLENBQUMsU0FBRCxDQUFMLEVBQWtCO0FBQ2hCQSxPQUFDLENBQUNRLElBQUYsQ0FBTztBQUNMQyxXQUFHLEVBQUUsbUJBREE7QUFFTEMsY0FBTSxFQUFFLEtBRkg7QUFHTEMsZ0JBQVEsRUFBRTtBQUhMLE9BQVAsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLElBQVYsRUFBZ0I7QUFDdEJiLFNBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYWMsSUFBYixDQUFrQkQsSUFBbEI7QUFDQUssYUFBSztBQUNMQyxtQkFBVztBQUNYQyxpQkFBUztBQUNWLE9BVEQ7QUFVRDtBQUNGO0FBRUYsQ0F0Q0QsRSxDQXVDQTs7QUFDQXBCLENBQUMsQ0FBQ0ksUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBWTtBQUM1QixNQUFJQyxFQUFFLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixpQkFBdkIsQ0FBVDs7QUFDQSxNQUFJRCxFQUFKLEVBQVE7QUFDTixRQUFJTixDQUFDLENBQUMsV0FBRCxDQUFMLEVBQW9CO0FBQ2xCQSxPQUFDLENBQUNRLElBQUYsQ0FBTztBQUNMQyxXQUFHLEVBQUUseUJBREE7QUFFTEMsY0FBTSxFQUFFLEtBRkg7QUFHTEMsZ0JBQVEsRUFBRTtBQUhMLE9BQVAsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLElBQVYsRUFBZ0I7QUFDdEJiLFNBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZWMsSUFBZixDQUFvQkQsSUFBcEI7QUFDQUUsc0JBQWM7QUFDZEMsa0JBQVU7QUFDWCxPQVJEO0FBU0Q7O0FBQ0QsUUFBSWhCLENBQUMsQ0FBQyxhQUFELENBQUwsRUFBc0I7QUFDcEJBLE9BQUMsQ0FBQ1EsSUFBRixDQUFPO0FBQ0xDLFdBQUcsRUFBRSwyQkFEQTtBQUVMQyxjQUFNLEVBQUUsS0FGSDtBQUdMQyxnQkFBUSxFQUFFO0FBSEwsT0FBUCxFQUlHQyxJQUpILENBSVEsVUFBVUMsSUFBVixFQUFnQjtBQUN0QmIsU0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQmMsSUFBakIsQ0FBc0JELElBQXRCO0FBQ0FJLHNCQUFjO0FBQ2YsT0FQRDtBQVFEOztBQUNELFFBQUlqQixDQUFDLENBQUMsV0FBRCxDQUFMLEVBQW9CO0FBQ2xCQSxPQUFDLENBQUNRLElBQUYsQ0FBTztBQUNMQyxXQUFHLEVBQUUseUJBREE7QUFFTEMsY0FBTSxFQUFFLEtBRkg7QUFHTEMsZ0JBQVEsRUFBRTtBQUhMLE9BQVAsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLElBQVYsRUFBZ0I7QUFDdEJiLFNBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZWMsSUFBZixDQUFvQkQsSUFBcEI7QUFDQUssYUFBSztBQUNMQyxtQkFBVztBQUNaLE9BUkQ7QUFTRDtBQUNGO0FBQ0YsQ0FwQ0Q7O0FBc0NBLFNBQVNFLGFBQVQsR0FBeUI7QUFDdkJwQixRQUFNLENBQUNxQixnQkFBUCxDQUF3QixTQUF4QixFQUFtQyxVQUFVQyxDQUFWLEVBQWE7QUFDOUMsUUFBSUEsQ0FBQyxDQUFDQyxPQUFGLEtBQWMsQ0FBbEIsRUFBcUI7QUFDbkJwQixjQUFRLENBQUNxQixJQUFULENBQWNDLFNBQWQsQ0FBd0JDLE1BQXhCLENBQStCLGFBQS9CO0FBQ0F2QixjQUFRLENBQUNxQixJQUFULENBQWNDLFNBQWQsQ0FBd0JFLEdBQXhCLENBQTRCLGdCQUE1QjtBQUNEO0FBQ0YsR0FMRDtBQU1BM0IsUUFBTSxDQUFDcUIsZ0JBQVAsQ0FBd0IsV0FBeEIsRUFBcUMsVUFBVUMsQ0FBVixFQUFhO0FBQ2hEbkIsWUFBUSxDQUFDcUIsSUFBVCxDQUFjQyxTQUFkLENBQXdCQyxNQUF4QixDQUErQixnQkFBL0I7QUFDQXZCLFlBQVEsQ0FBQ3FCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkUsR0FBeEIsQ0FBNEIsYUFBNUI7QUFDRCxHQUhEO0FBSUQ7O0FBRUQsU0FBU1gsY0FBVCxHQUEwQjtBQUN4QixNQUFJWSxJQUFJLEdBQUd6QixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsb0JBQXZCLENBQVg7QUFDQSxNQUFJdUIsSUFBSSxHQUFHMUIsUUFBUSxDQUFDRyxhQUFULENBQXVCLGlCQUF2QixDQUFYO0FBQ0FILFVBQVEsQ0FBQ0csYUFBVCxDQUF1QixvQkFBdkIsRUFBNkNlLGdCQUE3QyxDQUE4RCxPQUE5RCxFQUF1RSxZQUFZO0FBQ2pGLFNBQUtJLFNBQUwsQ0FBZUssTUFBZixDQUFzQixtQkFBdEI7O0FBQ0EsUUFBSUYsSUFBSixFQUFVO0FBQ1J6QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsV0FBdkIsRUFBb0NtQixTQUFwQyxDQUE4Q0ssTUFBOUMsQ0FBcUQsbUJBQXJEO0FBQ0Q7O0FBQ0QsUUFBSUQsSUFBSixFQUFVO0FBQ1IxQixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsYUFBdkIsRUFBc0NtQixTQUF0QyxDQUFnREssTUFBaEQsQ0FBdUQsbUJBQXZEO0FBQ0Q7O0FBQ0QzQixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsT0FBdkIsRUFBZ0NtQixTQUFoQyxDQUEwQ0ssTUFBMUMsQ0FBaUQsbUJBQWpEO0FBQ0EzQixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0JtQixTQUEvQixDQUF5Q0ssTUFBekMsQ0FBZ0QsaUJBQWhELEVBVGlGLENBVWpGOztBQUNBM0IsWUFBUSxDQUFDNEIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0F2QixZQUFRLENBQUNHLGFBQVQsQ0FBdUIscUJBQXZCLEVBQThDbUIsU0FBOUMsQ0FBd0RDLE1BQXhELENBQStELGVBQS9EO0FBQ0F2QixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsZ0JBQXZCLEVBQXlDbUIsU0FBekMsQ0FBbURDLE1BQW5ELENBQTBELGVBQTFELEVBYmlGLENBY2pGOztBQUNBdkIsWUFBUSxDQUFDRyxhQUFULENBQXVCLGlCQUF2QixFQUEwQ21CLFNBQTFDLENBQW9ESyxNQUFwRCxDQUEyRCxtQkFBM0Q7QUFDRCxHQWhCRDtBQWlCQTlCLFFBQU0sQ0FBQ3FCLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsUUFBSXJCLE1BQU0sQ0FBQ2dDLFVBQVAsSUFBcUIsSUFBekIsRUFBK0I7QUFDN0I3QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsb0JBQXZCLEVBQTZDbUIsU0FBN0MsQ0FBdURDLE1BQXZELENBQThELG1CQUE5RDs7QUFDQSxVQUFJRSxJQUFKLEVBQVU7QUFDUnpCLGdCQUFRLENBQUNHLGFBQVQsQ0FBdUIsV0FBdkIsRUFBb0NtQixTQUFwQyxDQUE4Q0MsTUFBOUMsQ0FBcUQsbUJBQXJEO0FBQ0Q7O0FBQ0QsVUFBSUcsSUFBSixFQUFVO0FBQ1IxQixnQkFBUSxDQUFDRyxhQUFULENBQXVCLGFBQXZCLEVBQXNDbUIsU0FBdEMsQ0FBZ0RDLE1BQWhELENBQXVELG1CQUF2RDtBQUNEOztBQUNEdkIsY0FBUSxDQUFDRyxhQUFULENBQXVCLE9BQXZCLEVBQWdDbUIsU0FBaEMsQ0FBMENDLE1BQTFDLENBQWlELG1CQUFqRDtBQUNBdkIsY0FBUSxDQUFDRyxhQUFULENBQXVCLE1BQXZCLEVBQStCbUIsU0FBL0IsQ0FBeUNDLE1BQXpDLENBQWdELGlCQUFoRCxFQVQ2QixDQVU3Qjs7QUFDQXZCLGNBQVEsQ0FBQ0csYUFBVCxDQUF1QixpQkFBdkIsRUFBMENtQixTQUExQyxDQUFvREMsTUFBcEQsQ0FBMkQsbUJBQTNEO0FBQ0Q7QUFDRixHQWREO0FBZUQ7O0FBRUQsU0FBU1QsS0FBVCxHQUFpQjtBQUNmZCxVQUFRLENBQUNHLGFBQVQsQ0FBdUIsUUFBdkIsRUFBaUMyQixPQUFqQyxHQUEyQyxVQUFVWCxDQUFWLEVBQWE7QUFDdERBLEtBQUMsQ0FBQ1ksY0FBRjtBQUNBbkMsS0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQm9DLE9BQWhCLENBQXdCO0FBQ3RCQyxlQUFTLEVBQUU7QUFEVyxLQUF4QixFQUdFLEdBSEY7QUFLRCxHQVBEO0FBUUQ7O0FBRUQsU0FBU2xCLFdBQVQsR0FBdUI7QUFDckIsTUFBSWIsRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsaUJBQXZCLENBQVQsQ0FEcUIsQ0FFckI7O0FBQ0EsTUFBSStCLGVBQWUsR0FBR2xDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixXQUF2QixFQUFvQ2dDLFlBQTFELENBSHFCLENBSXJCO0FBQ0E7O0FBQ0EsTUFBSUMsZUFBZSxHQUFHcEMsUUFBUSxDQUFDNEIsZUFBVCxDQUF5QlMsWUFBL0MsQ0FOcUIsQ0FPckI7QUFDQTs7QUFDQSxNQUFJQyxlQUFlLEdBQUd6QyxNQUFNLENBQUMwQyxXQUE3QixDQVRxQixDQVVyQjtBQUNBOztBQUNBLE1BQUlDLFlBQVksR0FBR3hDLFFBQVEsQ0FBQzRCLGVBQVQsQ0FBeUJLLFNBQTVDLENBWnFCLENBYXJCO0FBQ0E7QUFDQTs7QUFDQSxNQUFJL0IsRUFBSixFQUFRO0FBQ04sUUFBSWtDLGVBQWUsSUFBSUUsZUFBZSxHQUFHRSxZQUFsQixHQUFpQ04sZUFBeEQsRUFBeUU7QUFDdkVoQyxRQUFFLENBQUNvQixTQUFILENBQWFDLE1BQWIsQ0FBb0IsV0FBcEI7QUFDRCxLQUZELE1BRU87QUFDTHJCLFFBQUUsQ0FBQ29CLFNBQUgsQ0FBYUUsR0FBYixDQUFpQixXQUFqQjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxTQUFTWixVQUFULEdBQXNCO0FBQ3BCLE1BQUk2QixZQUFZLEdBQUcsRUFBbkI7QUFDQTVDLFFBQU0sQ0FBQ3FCLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsV0FBT3VCLFlBQVksR0FBRzdDLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZThDLE1BQWYsRUFBdEI7QUFDRCxHQUZEO0FBR0E5QyxHQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQitDLEtBQXBCLENBQTBCLFVBQVV4QixDQUFWLEVBQWE7QUFDckNBLEtBQUMsQ0FBQ1ksY0FBRjtBQUNBLFFBQUlhLE1BQU0sR0FBR2hELENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlELElBQVIsQ0FBYSxNQUFiLENBQWI7QUFDQSxRQUFJQyxTQUFTLEdBQUdsRCxDQUFDLENBQUNnRCxNQUFELENBQUQsQ0FBVUcsTUFBVixHQUFtQkMsR0FBbkM7QUFDQSxRQUFJUCxZQUFZLEdBQUc3QyxDQUFDLENBQUMsV0FBRCxDQUFELENBQWU4QyxNQUFmLEVBQW5CO0FBQ0E5QyxLQUFDLENBQUMsV0FBRCxDQUFELENBQWVvQyxPQUFmLENBQXVCO0FBQ3JCQyxlQUFTLEVBQUVhLFNBQVMsR0FBR0w7QUFERixLQUF2QixFQUVHLElBRkg7QUFHQSxRQUFJUSxTQUFTLEdBQUdqRCxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBaEI7QUFDQSxRQUFJK0MsUUFBUSxHQUFHbEQsUUFBUSxDQUFDRyxhQUFULENBQXVCLE9BQXZCLENBQWY7QUFDQThDLGFBQVMsQ0FBQzNCLFNBQVYsQ0FBb0JDLE1BQXBCLENBQTJCLGVBQTNCO0FBQ0EyQixZQUFRLENBQUM1QixTQUFULENBQW1CQyxNQUFuQixDQUEwQixlQUExQjtBQUNBdkIsWUFBUSxDQUFDNEIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0QsR0FiRDtBQWNELEMsQ0FDRDs7O0FBQ0EsU0FBUzRCLFNBQVQsQ0FBbUJqRCxFQUFuQixFQUF1QjBDLE1BQXZCLEVBQStCUSxRQUEvQixFQUF5QztBQUN2QyxPQUFLbEQsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsT0FBSzBDLE1BQUwsR0FBY0EsTUFBZDtBQUNBLE9BQUtRLFFBQUwsR0FBZ0JBLFFBQWhCO0FBQ0Q7O0FBQ0RELFNBQVMsQ0FBQ0UsU0FBVixDQUFvQkMsSUFBcEIsR0FBMkIsWUFBWTtBQUNyQyxNQUFJQyxRQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQixLQUFLdEQsRUFBL0IsQ0FBZjtBQUNBLE1BQUkwQyxNQUFNLEdBQUcsS0FBS0EsTUFBbEI7QUFDQSxNQUFJUSxRQUFRLEdBQUcsS0FBS0EsUUFBcEI7QUFDQUssT0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkJKLFFBQTNCLEVBQXFDSyxPQUFyQyxDQUE2QyxVQUFVQyxPQUFWLEVBQW1CO0FBQzlEQSxXQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDNEMsV0FBSyxDQUFDL0IsY0FBTjs7QUFDQSxVQUFJcUIsUUFBUSxJQUFJLElBQWhCLEVBQXNCO0FBQ3BCLFlBQUksS0FBS2pELGFBQUwsQ0FBbUJ5QyxNQUFuQixNQUErQixJQUFuQyxFQUF5QztBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBaEQsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRbUUsSUFBUixDQUFhbkIsTUFBYixFQUFxQm9CLFdBQXJCLENBQWlDLHNCQUFqQyxFQUx1QyxDQU12Qzs7QUFDQXBFLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW9FLFdBQVIsQ0FBb0IsdUJBQXBCO0FBQ0FwRSxXQUFDLENBQUMsSUFBRCxDQUFELENBQVF3RCxRQUFSLEdBQW1CVyxJQUFuQixDQUF3Qm5CLE1BQXhCLEVBQWdDcUIsV0FBaEMsQ0FBNEMsc0JBQTVDLEVBUnVDLENBU3ZDOztBQUNBckUsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRd0QsUUFBUixHQUFtQmEsV0FBbkIsQ0FBK0IsdUJBQS9CO0FBQ0QsU0FabUIsQ0FhcEI7QUFDQTs7O0FBQ0EsWUFBSUMsR0FBRyxHQUFHdEUsQ0FBQyxDQUFDaUUsT0FBRCxDQUFELENBQVdoQixJQUFYLENBQWdCLGFBQWhCLENBQVY7QUFDQWpELFNBQUMsQ0FBQ3NFLEdBQUQsQ0FBRCxDQUFPRixXQUFQLENBQW1CLHNCQUFuQixFQUEyQ1osUUFBM0MsR0FBc0RhLFdBQXRELENBQWtFLHNCQUFsRSxFQWhCb0IsQ0FpQnBCO0FBQ0E7QUFDQTtBQUNELE9BcEJELE1Bb0JPO0FBQ0wsWUFBSSxLQUFLOUQsYUFBTCxDQUFtQnlDLE1BQW5CLE1BQStCLElBQW5DLEVBQXlDO0FBQ3ZDLGVBQUt6QyxhQUFMLENBQW1CeUMsTUFBbkIsRUFBMkJ0QixTQUEzQixDQUFxQ0ssTUFBckMsQ0FDRSxzQkFERjtBQUdEOztBQUNEM0IsZ0JBQVEsQ0FBQ0csYUFBVCxDQUF1QjBELE9BQU8sQ0FBQ00sWUFBUixDQUFxQixhQUFyQixDQUF2QixFQUE0RDdDLFNBQTVELENBQXNFSyxNQUF0RSxDQUE2RSxzQkFBN0U7QUFDQWtDLGVBQU8sQ0FBQ3ZDLFNBQVIsQ0FBa0JLLE1BQWxCLENBQXlCLHNCQUF6QjtBQUNEO0FBQ0YsS0EvQkQ7QUFnQ0QsR0FqQ0Q7QUFrQ0QsQ0F0Q0Q7O0FBd0NBLFNBQVN5QyxlQUFULENBQXlCQyxrQkFBekIsRUFBNkM7QUFDM0MsU0FBT1osS0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkJVLGtCQUEzQixDQUFQO0FBQ0QsQyxDQUNEOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQSxTQUFTQyxjQUFULENBQXdCcEUsRUFBeEIsRUFBNEIwQyxNQUE1QixFQUFvQzJCLFVBQXBDLEVBQWdEO0FBQzlDLE1BQUloQixRQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQnRELEVBQTFCLENBQWY7QUFDQSxNQUFJMEMsTUFBTSxHQUFHNUMsUUFBUSxDQUFDRyxhQUFULENBQXVCeUMsTUFBdkIsQ0FBYjs7QUFDQSxNQUFJQSxNQUFKLEVBQVk7QUFDVndCLG1CQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuREEsYUFBTyxDQUFDM0MsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1QzRDLGFBQUssQ0FBQy9CLGNBQU47QUFDQSxhQUFLVCxTQUFMLENBQWVLLE1BQWYsQ0FBc0IsV0FBdEI7QUFDQWlCLGNBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJLLE1BQWpCLENBQXdCLFdBQXhCO0FBQ0EsWUFBSTZDLGFBQWEsR0FBR0QsVUFBcEI7O0FBQ0EsWUFBSUMsYUFBYSxLQUFLLEVBQXRCLEVBQTBCO0FBQ3hCLGNBQUlDLFFBQVEsR0FBRzVFLE1BQU0sQ0FBQ2dDLFVBQVAsR0FBb0IwQyxVQUFuQzs7QUFDQSxjQUFJRSxRQUFKLEVBQWM7QUFDWnpFLG9CQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixDQUFtQ0ssTUFBbkMsQ0FBMEMsdUJBQTFDO0FBQ0Q7QUFDRixTQUxELE1BS087QUFDTDNCLGtCQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixDQUFtQ0MsTUFBbkMsQ0FBMEMsdUJBQTFDO0FBQ0Q7O0FBQ0QxQixjQUFNLENBQUNxQixnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLGNBQUlyQixNQUFNLENBQUNnQyxVQUFQLElBQXFCMEMsVUFBekIsRUFBcUM7QUFDbkN2RSxvQkFBUSxDQUFDNEIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLHVCQUExQztBQUNEO0FBQ0YsU0FKRDtBQUtELE9BbEJEO0FBbUJELEtBcEJEO0FBcUJEO0FBQ0Y7QUFDRDs7QUFDQTs7O0FBQ0EsU0FBU21ELFlBQVQsQ0FBc0J4RSxFQUF0QixFQUEwQjBDLE1BQTFCLEVBQWtDO0FBQ2hDLE1BQUlXLFFBQVEsR0FBR3ZELFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCdEQsRUFBMUIsQ0FBZjtBQUNBLE1BQUkwQyxNQUFNLEdBQUc1QyxRQUFRLENBQUNHLGFBQVQsQ0FBdUJ5QyxNQUF2QixDQUFiOztBQUNBLE1BQUlBLE1BQUosRUFBWTtBQUNWd0IsbUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25EQSxhQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDNEMsYUFBSyxDQUFDL0IsY0FBTjtBQUNBYSxjQUFNLENBQUN0QixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixXQUF4QjtBQUNBdkIsZ0JBQVEsQ0FBQzRCLGVBQVQsQ0FBeUJOLFNBQXpCLENBQW1DQyxNQUFuQyxDQUEwQyx1QkFBMUM7QUFDRCxPQUpEO0FBS0QsS0FORDtBQU9EO0FBQ0YsQyxDQUNEOzs7QUFDQSxTQUFTb0QsZUFBVCxHQUEyQjtBQUN6QixNQUFJM0UsUUFBUSxDQUFDRyxhQUFULENBQXVCLGtCQUF2QixDQUFKLEVBQWdEO0FBQzlDLFFBQUlnRCxTQUFKLENBQWMsa0JBQWQsRUFBa0MsdUJBQWxDLEVBQTJELElBQTNELEVBQWlFRyxJQUFqRTtBQUNEO0FBQ0YsQyxDQUNEOzs7QUFDQSxJQUFJc0IsR0FBRyxHQUFHLENBQVY7O0FBRUEsU0FBU0MsVUFBVCxHQUFzQjtBQUNwQixNQUFJaEYsTUFBTSxDQUFDZ0MsVUFBUCxHQUFvQixHQUFwQixJQUEyQitDLEdBQUcsSUFBSSxDQUF0QyxFQUF5QztBQUN2Q2hGLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZXFFLFdBQWYsQ0FBMkIsYUFBM0IsRUFBMENhLEtBQTFDLEdBQWtEQyxRQUFsRCxDQUEyRCxhQUEzRDtBQUNBbkYsS0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQnFFLFdBQWxCLENBQThCLFFBQTlCLEVBQXdDZSxNQUF4QyxHQUFpREYsS0FBakQsR0FBeURmLElBQXpELENBQThELGNBQTlELEVBQThFZ0IsUUFBOUUsQ0FBdUYsUUFBdkY7QUFDQUgsT0FBRyxHQUFHLENBQU47QUFDRDs7QUFDRCxNQUFJL0UsTUFBTSxDQUFDZ0MsVUFBUCxHQUFvQixHQUFwQixJQUEyQitDLEdBQUcsSUFBSSxDQUF0QyxFQUF5QztBQUN2Q2hGLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZW1GLFFBQWYsQ0FBd0IsYUFBeEI7QUFDQUgsT0FBRyxHQUFHLENBQU47QUFDRCxHQVRtQixDQVVwQjs7QUFDRDs7QUFFRCxTQUFTSyxvQkFBVCxHQUFnQztBQUM5QjtBQUNBO0FBQ0EsTUFBSWpGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1Qiw2QkFBdkIsQ0FBSixFQUEyRDtBQUN6RCxRQUFJZ0QsU0FBSixDQUFjLDZCQUFkLEVBQTZDK0IsU0FBN0MsRUFBd0QsS0FBeEQsRUFBK0Q1QixJQUEvRDtBQUNEOztBQUNELE1BQUlDLFFBQVEsR0FBR3ZELFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCLGtCQUExQixDQUFmO0FBQ0FZLGlCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxRQUFJM0QsRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUIwRCxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsYUFBckIsQ0FBdkIsQ0FBVDtBQUNBTixXQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDLFVBQUloQixFQUFFLENBQUNDLGFBQUgsQ0FBaUIsNkJBQWpCLENBQUosRUFBcUQ7QUFDbkRELFVBQUUsQ0FBQ0MsYUFBSCxDQUFpQiw2QkFBakIsRUFBZ0RtQixTQUFoRCxDQUEwREMsTUFBMUQsQ0FBaUUsc0JBQWpFO0FBQ0FyQixVQUFFLENBQUNDLGFBQUgsQ0FBaUIsNkJBQWpCLEVBQWdEbUIsU0FBaEQsQ0FBMERDLE1BQTFELENBQWlFLHNCQUFqRTtBQUNEO0FBQ0YsS0FMRDtBQU1ELEdBUkQsRUFQOEIsQ0FnQjlCO0FBQ0E7O0FBQ0ErQyxnQkFBYyxDQUFDLFFBQUQsRUFBVyxvQkFBWCxFQUFpQyxFQUFqQyxDQUFkO0FBQ0FBLGdCQUFjLENBQUMsd0JBQUQsRUFBMkIsb0JBQTNCLEVBQWlELEdBQWpELENBQWQ7QUFDQUksY0FBWSxDQUFDLGFBQUQsRUFBZ0Isb0JBQWhCLENBQVosQ0FwQjhCLENBcUI5Qjs7QUFDQUosZ0JBQWMsQ0FBQyxpQkFBRCxFQUFvQixrQkFBcEIsRUFBd0MsR0FBeEMsQ0FBZDtBQUNBQSxnQkFBYyxDQUFDLFFBQUQsRUFBVyxrQkFBWCxFQUErQixFQUEvQixDQUFkO0FBQ0FJLGNBQVksQ0FBQyxhQUFELEVBQWdCLGtCQUFoQixDQUFaLENBeEI4QixDQXlCOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUQ7O0FBRUQsU0FBU1MseUJBQVQsQ0FBbUNqRixFQUFuQyxFQUF1Q3FFLFVBQXZDLEVBQW1EO0FBQ2pELE1BQUkxRSxNQUFNLENBQUNnQyxVQUFQLEdBQW9CMEMsVUFBcEIsSUFBa0MzRSxDQUFDLENBQUNNLEVBQUQsQ0FBRCxDQUFNa0YsUUFBTixDQUFlLFdBQWYsQ0FBbEMsSUFBaUVwRixRQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixJQUFzQyxFQUEzRyxFQUErRztBQUM3R3RCLFlBQVEsQ0FBQzRCLGVBQVQsQ0FBeUJOLFNBQXpCLENBQW1DRSxHQUFuQyxDQUF1QyxlQUF2QztBQUNEO0FBQ0YsQyxDQUNEOzs7QUFDQSxTQUFTNkQsS0FBVCxDQUFlbkYsRUFBZixFQUFtQm9GLFlBQW5CLEVBQWlDQyxZQUFqQyxFQUErQ0MsSUFBL0MsRUFBcURDLFVBQXJELEVBQWlFO0FBQy9ELE9BQUt2RixFQUFMLEdBQVVBLEVBQVY7QUFDQSxPQUFLb0YsWUFBTCxHQUFvQkEsWUFBcEI7QUFDQSxPQUFLQyxZQUFMLEdBQW9CQSxZQUFwQjtBQUNBLE9BQUtDLElBQUwsR0FBWUEsSUFBWjtBQUNBLE9BQUtDLFVBQUwsR0FBa0JBLFVBQWxCO0FBQ0Q7O0FBQUE7O0FBRURKLEtBQUssQ0FBQ2hDLFNBQU4sQ0FBZ0JDLElBQWhCLEdBQXVCLFlBQVk7QUFDakMxRCxHQUFDLENBQUMsS0FBS00sRUFBTCxHQUFVLFdBQVgsQ0FBRCxDQUF5QndGLEtBQXpCLENBQStCO0FBQzdCQyxVQUFNLEVBQUUsSUFEcUI7QUFFN0JMLGdCQUFZLEVBQUUsS0FBS0EsWUFGVTtBQUc3QkMsZ0JBQVksRUFBRSxLQUFLQSxZQUhVO0FBSTdCQyxRQUFJLEVBQUUsS0FBS0EsSUFKa0I7QUFLN0JJLGFBQVMsc0dBTG9CO0FBTTdCQyxhQUFTLHVHQU5vQjtBQU83QjtBQUNBO0FBQ0FKLGNBQVUsRUFBRSxLQUFLQTtBQVRZLEdBQS9CO0FBV0QsQ0FaRDs7QUFjQSxTQUFTSywyQkFBVCxHQUF1QztBQUNyQ3ZDLFVBQVEsR0FBR3ZELFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCLGdDQUExQixDQUFYO0FBQ0FZLGlCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxRQUFJa0MsU0FBUyxHQUFHbEMsT0FBTyxDQUFDTCxnQkFBUixDQUF5QixJQUF6QixDQUFoQjtBQUNBLFFBQUl3QyxLQUFLLEdBQUdELFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTVELFlBQXpCO0FBQ0EsUUFBSThELEtBQUssR0FBR0YsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhNUQsWUFBekI7QUFDQSxRQUFJK0QsS0FBSyxHQUFHSCxTQUFTLENBQUMsQ0FBRCxDQUFULENBQWE1RCxZQUF6QjtBQUNBLFFBQUlnRSxRQUFRLEdBQUdILEtBQUssR0FBR0MsS0FBUixHQUFnQkMsS0FBL0I7O0FBQ0EsUUFBSXJHLE1BQU0sQ0FBQ2dDLFVBQVAsR0FBb0IsSUFBcEIsSUFBNEJnQyxPQUFPLENBQUN2QyxTQUFSLENBQWtCOEUsUUFBbEIsQ0FBMkIsV0FBM0IsS0FBMkMsS0FBM0UsRUFBa0Y7QUFDaEZ2QyxhQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLGFBQTBCeUQsUUFBMUI7QUFDRCxLQUZELE1BRU8sSUFBSXRHLE1BQU0sQ0FBQ2dDLFVBQVAsR0FBb0IsSUFBeEIsRUFBOEI7QUFDbkNnQyxhQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLEdBQXVCLEVBQXZCO0FBQ0FtQixhQUFPLENBQUN2QyxTQUFSLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QjtBQUNBc0MsYUFBTyxDQUFDMUQsYUFBUixDQUFzQixHQUF0QixFQUEyQm1CLFNBQTNCLENBQXFDQyxNQUFyQyxDQUE0QyxrQkFBNUM7QUFDRDs7QUFDRDFCLFVBQU0sQ0FBQ3FCLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsVUFBSXJCLE1BQU0sQ0FBQ2dDLFVBQVAsR0FBb0IsSUFBcEIsSUFBNEJnQyxPQUFPLENBQUN2QyxTQUFSLENBQWtCOEUsUUFBbEIsQ0FBMkIsV0FBM0IsS0FBMkMsS0FBM0UsRUFBa0Y7QUFDaEZ2QyxlQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLGFBQTBCeUQsUUFBMUI7QUFDRCxPQUZELE1BRU8sSUFBSXRHLE1BQU0sQ0FBQ2dDLFVBQVAsR0FBb0IsSUFBeEIsRUFBOEI7QUFDbkNnQyxlQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLEdBQXVCLEVBQXZCO0FBQ0FtQixlQUFPLENBQUN2QyxTQUFSLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QjtBQUNBc0MsZUFBTyxDQUFDMUQsYUFBUixDQUFzQixHQUF0QixFQUEyQm1CLFNBQTNCLENBQXFDQyxNQUFyQyxDQUE0QyxrQkFBNUM7QUFDRDtBQUNGLEtBUkQ7QUFTRCxHQXRCRDtBQXVCRDs7QUFFRCxTQUFTK0UsOEJBQVQsR0FBMEM7QUFDeEMvQyxVQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQixnQ0FBMUIsQ0FBWDtBQUNBWSxpQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkQsUUFBSWtDLFNBQVMsR0FBR2xDLE9BQU8sQ0FBQ0wsZ0JBQVIsQ0FBeUIsSUFBekIsQ0FBaEI7QUFDQSxRQUFJd0MsS0FBSyxHQUFHRCxTQUFTLENBQUMsQ0FBRCxDQUFULENBQWE1RCxZQUF6QjtBQUNBLFFBQUk4RCxLQUFLLEdBQUdGLFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTVELFlBQXpCO0FBQ0EsUUFBSStELEtBQUssR0FBR0gsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhNUQsWUFBekI7QUFDQSxRQUFJZ0UsUUFBUSxHQUFHSCxLQUFLLEdBQUdDLEtBQVIsR0FBZ0JDLEtBQS9CO0FBQ0FyQyxXQUFPLENBQUMxRCxhQUFSLENBQXNCLGdCQUF0QixFQUF3Q2UsZ0JBQXhDLENBQXlELE9BQXpELEVBQWtFLFlBQVk7QUFDNUUsVUFBSXJCLE1BQU0sQ0FBQ2dDLFVBQVAsR0FBb0IsSUFBcEIsSUFBNEJnQyxPQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLElBQXdCLEVBQXhELEVBQTREO0FBQzFEbUIsZUFBTyxDQUFDd0MsS0FBUixDQUFjM0QsTUFBZCxhQUEwQnlELFFBQTFCO0FBQ0F0QyxlQUFPLENBQUN2QyxTQUFSLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QjtBQUNBc0MsZUFBTyxDQUFDMUQsYUFBUixDQUFzQixHQUF0QixFQUEyQm1CLFNBQTNCLENBQXFDQyxNQUFyQyxDQUE0QyxrQkFBNUM7QUFDRCxPQUpELE1BSU87QUFDTHNDLGVBQU8sQ0FBQ3dDLEtBQVIsQ0FBYzNELE1BQWQsR0FBdUIsRUFBdkI7QUFDQW1CLGVBQU8sQ0FBQ3ZDLFNBQVIsQ0FBa0JFLEdBQWxCLENBQXNCLFdBQXRCO0FBQ0FxQyxlQUFPLENBQUMxRCxhQUFSLENBQXNCLEdBQXRCLEVBQTJCbUIsU0FBM0IsQ0FBcUNFLEdBQXJDLENBQXlDLGtCQUF6QztBQUNEO0FBQ0YsS0FWRDtBQVdELEdBakJEO0FBa0JELEMsQ0FFRDs7O0FBQ0EsU0FBUytFLFlBQVQsR0FBd0I7QUFDdEIsTUFBSXZHLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixVQUF2QixDQUFKLEVBQXdDO0FBQ3RDLFFBQUlxRyxPQUFPLEdBQUcsQ0FBQyxPQUFELEVBQVUsaUJBQVYsQ0FBZDtBQUNBQSxXQUFPLENBQUM1QyxPQUFSLENBQWdCLFVBQVVoQixNQUFWLEVBQWtCO0FBQ2hDLFVBQUl5QyxLQUFKLENBQVV6QyxNQUFWLEVBQWtCLENBQWxCLEVBQXFCLENBQXJCLEVBQXdCLENBQXhCLEVBQTJCLENBQUM7QUFDMUI2RCxrQkFBVSxFQUFFLEdBRGM7QUFFMUJDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUU7QUFETjtBQUZnQixPQUFELEVBTTNCO0FBQ0VtQixrQkFBVSxFQUFFLEdBRGQ7QUFFRUMsZ0JBQVEsRUFBRTtBQUNScEIsc0JBQVksRUFBRSxDQUROO0FBRVJLLGdCQUFNLEVBQUU7QUFGQTtBQUZaLE9BTjJCLENBQTNCLEVBYUdyQyxJQWJIO0FBY0QsS0FmRDtBQWdCQSxRQUFJK0IsS0FBSixDQUFVLFFBQVYsRUFBb0IsQ0FBcEIsRUFBdUIsQ0FBdkIsRUFBMEIsQ0FBMUIsRUFBNkIsQ0FBQztBQUM1Qm9CLGdCQUFVLEVBQUUsR0FEZ0I7QUFFNUJDLGNBQVEsRUFBRTtBQUNScEIsb0JBQVksRUFBRSxDQUROO0FBRVJDLG9CQUFZLEVBQUUsQ0FGTjtBQUdSQyxZQUFJLEVBQUU7QUFIRTtBQUZrQixLQUFELEVBUTdCO0FBQ0VpQixnQkFBVSxFQUFFLEdBRGQ7QUFFRUMsY0FBUSxFQUFFO0FBQ1JwQixvQkFBWSxFQUFFLENBRE47QUFFUkMsb0JBQVksRUFBRSxDQUZOO0FBR1JDLFlBQUksRUFBRSxDQUhFO0FBSVJHLGNBQU0sRUFBRTtBQUpBO0FBRlosS0FSNkIsQ0FBN0IsRUFpQkdyQyxJQWpCSDtBQWtCRDtBQUNGOztBQUVELFNBQVNxRCxXQUFULEdBQXVCO0FBQ3JCLE1BQUl6RyxFQUFFLEdBQUcsZ0JBQVQ7O0FBQ0EsTUFBSUYsUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixDQUFKLEVBQWdDO0FBQzlCTixLQUFDLENBQUNNLEVBQUQsQ0FBRCxDQUFNd0YsS0FBTixDQUFZO0FBQ1Y7QUFDQTtBQUNBSixrQkFBWSxFQUFFLENBSEo7QUFJVkssWUFBTSxFQUFFLElBSkU7QUFLVkMsZUFBUyxxR0FMQztBQU1WQyxlQUFTLHNHQU5DO0FBT1ZlLGNBQVEsRUFBRSxhQVBBO0FBUVZuQixnQkFBVSxFQUFFLENBQUM7QUFDWGdCLGtCQUFVLEVBQUUsR0FERDtBQUVYQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUnVCLGNBQUksRUFBRTtBQUZFO0FBRkMsT0FBRCxFQU9aO0FBQ0VKLGtCQUFVLEVBQUUsR0FEZDtBQUVFQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUkssZ0JBQU0sRUFBRSxLQUZBO0FBR1I7QUFDQWtCLGNBQUksRUFBRTtBQUpFO0FBRlosT0FQWTtBQVJGLEtBQVosRUFEOEIsQ0EyQjlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNEO0FBQ0Y7O0FBRUQsU0FBU0MsV0FBVCxHQUF1QjtBQUNyQixNQUFJNUcsRUFBRSxHQUFHLGdCQUFUOztBQUNBLE1BQUlGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBSixFQUFnQztBQUM5QixRQUFJNkcsU0FBUyxHQUFHL0csUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixFQUEyQnNELGdCQUEzQixDQUE0QyxrQkFBNUMsRUFBZ0V3RCxNQUFoRixDQUQ4QixDQUV4QjtBQUNBOztBQUNBLFFBQUlDLGVBQWUsR0FBR0YsU0FBdEI7QUFDTm5ILEtBQUMsQ0FBQ00sRUFBRCxDQUFELENBQU13RixLQUFOLENBQVk7QUFDVkosa0JBQVksRUFBRTJCLGVBREo7QUFFVkMsb0JBQWMsRUFBRSxDQUZOO0FBR1Z2QixZQUFNLEVBQUUsSUFIRTtBQUlWQyxlQUFTLHFHQUpDO0FBS1ZDLGVBQVMsc0dBTEM7QUFNVmUsY0FBUSxFQUFFLGFBTkE7QUFPVk8sY0FBUSxFQUFFLEtBUEE7QUFRVjFCLGdCQUFVLEVBQUUsQ0FBQztBQUNYZ0Isa0JBQVUsRUFBRSxJQUREO0FBRVhDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUUsQ0FETjtBQUVSOEIsdUJBQWEsRUFBRSxLQUZQLENBR1I7QUFDQTtBQUNBOztBQUxRO0FBRkMsT0FBRCxFQVNUO0FBQ0RYLGtCQUFVLEVBQUUsR0FEWDtBQUVEQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUjhCLHVCQUFhLEVBQUUsS0FGUCxDQUdSO0FBQ0E7O0FBSlE7QUFGVCxPQVRTLEVBa0JaO0FBQ0VYLGtCQUFVLEVBQUUsR0FEZDtBQUVFQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUjhCLHVCQUFhLEVBQUUsSUFGUDtBQUdSO0FBQ0F6QixnQkFBTSxFQUFFO0FBSkE7QUFGWixPQWxCWTtBQVJGLEtBQVo7QUFxQ0Q7QUFDRjs7QUFFRCxTQUFTMEIsZ0JBQVQsR0FBNEI7QUFDMUIsTUFBSW5ILEVBQUUsR0FBRyxnQkFBVDs7QUFFQSxNQUFJRixRQUFRLENBQUNHLGFBQVQsQ0FBdUJELEVBQXZCLENBQUosRUFBZ0M7QUFDOUIsUUFBSTZHLFNBQVMsR0FBRy9HLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsRUFBMkJzRCxnQkFBM0IsQ0FBNEMsa0JBQTVDLEVBQWdFd0QsTUFBaEYsQ0FEOEIsQ0FDMEQ7QUFDeEY7O0FBQ0EsUUFBSW5ILE1BQU0sQ0FBQ2dDLFVBQVAsSUFBcUIsSUFBckIsSUFBNkJrRixTQUFTLElBQUksQ0FBOUMsRUFBaUQ7QUFDL0NuSCxPQUFDLENBQUMsNkJBQUQsQ0FBRCxDQUFpQ21GLFFBQWpDLENBQTBDLGdCQUExQyxFQUQrQyxDQUNjO0FBQzlEOztBQUVELFFBQUlsRixNQUFNLENBQUNnQyxVQUFQLElBQXFCLEdBQXJCLElBQTRCaEMsTUFBTSxDQUFDZ0MsVUFBUCxHQUFvQixJQUFoRCxJQUF3RGtGLFNBQVMsR0FBRyxDQUF4RSxFQUEyRTtBQUN6RW5ILE9BQUMsQ0FBQyw2QkFBRCxDQUFELENBQWlDbUYsUUFBakMsQ0FBMEMsZ0JBQTFDLEVBRHlFLENBQ1o7QUFDOUQsS0FUNkIsQ0FTNUI7QUFDRjtBQUNBOztBQUNEO0FBQ0Y7O0FBRUQsU0FBU3VDLGtCQUFULEdBQThCO0FBQzVCLE1BQUlwSCxFQUFFLEdBQUcsd0JBQVQ7O0FBQ0EsTUFBSUYsUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixDQUFKLEVBQWdDO0FBQzlCTixLQUFDLENBQUNNLEVBQUQsQ0FBRCxDQUFNd0YsS0FBTixDQUFZO0FBQ1ZKLGtCQUFZLEVBQUUsQ0FESjtBQUVWNEIsb0JBQWMsRUFBRSxDQUZOO0FBR1Z2QixZQUFNLEVBQUUsSUFIRTtBQUlWQyxlQUFTLHFHQUpDO0FBS1ZDLGVBQVMsc0dBTEM7QUFNVmUsY0FBUSxFQUFFLGFBTkE7QUFPVk8sY0FBUSxFQUFFLEtBUEE7QUFRVjFCLGdCQUFVLEVBQUUsQ0FBQztBQUNYZ0Isa0JBQVUsRUFBRSxHQUREO0FBRVhDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUUsQ0FETjtBQUVSOEIsdUJBQWEsRUFBRTtBQUZQO0FBRkMsT0FBRCxFQU9aO0FBQ0VYLGtCQUFVLEVBQUUsR0FEZDtBQUVFQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUjhCLHVCQUFhLEVBQUUsSUFGUDtBQUdSekIsZ0JBQU0sRUFBRTtBQUhBO0FBRlosT0FQWTtBQVJGLEtBQVo7QUF5QkQ7QUFDRjs7QUFFRCxTQUFTNEIsdUJBQVQsR0FBbUM7QUFDakMsTUFBSXJILEVBQUUsR0FBRyx3QkFBVDs7QUFDQSxNQUFJRixRQUFRLENBQUNHLGFBQVQsQ0FBdUJELEVBQXZCLENBQUosRUFBZ0M7QUFDOUIsUUFBSUwsTUFBTSxDQUFDZ0MsVUFBUCxJQUFxQixHQUF6QixFQUE4QjtBQUM1QmpDLE9BQUMsQ0FBQyxxQ0FBRCxDQUFELENBQXlDbUYsUUFBekMsQ0FBa0QsZ0JBQWxEO0FBQ0Q7QUFDRjtBQUNGLEMsQ0FDRDs7O0FBQ0EsU0FBU3lDLFlBQVQsR0FBd0I7QUFDdEIsTUFBSXhILFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCLHdCQUExQixFQUFvRHdELE1BQXhELEVBQWdFO0FBQzlELFFBQUl6RCxRQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQix3QkFBMUIsQ0FBZjtBQUNBWSxtQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkRBLGFBQU8sQ0FBQzNDLGdCQUFSLENBQXlCLFdBQXpCLEVBQXNDLFlBQVk7QUFDaEQyQyxlQUFPLENBQUMxRCxhQUFSLENBQXNCLFFBQXRCLEVBQWdDbUIsU0FBaEMsQ0FBMENFLEdBQTFDLENBQThDLGFBQTlDO0FBQ0QsT0FGRDtBQUdBcUMsYUFBTyxDQUFDM0MsZ0JBQVIsQ0FBeUIsVUFBekIsRUFBcUMsWUFBWTtBQUMvQzJDLGVBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsUUFBdEIsRUFBZ0NtQixTQUFoQyxDQUEwQ0MsTUFBMUMsQ0FBaUQsYUFBakQ7QUFDRCxPQUZEO0FBR0QsS0FQRDtBQVFEO0FBQ0YsQyxDQUNEOzs7QUFDQSxTQUFTa0csZ0JBQVQsQ0FBMEJsRCxVQUExQixFQUFzQztBQUNwQyxNQUFJVixPQUFPLEdBQUc3RCxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBZDtBQUNBLE1BQUl5QyxNQUFNLEdBQUc1QyxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBYjtBQUVBMEQsU0FBTyxDQUFDM0MsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1QyxTQUFLSSxTQUFMLENBQWVLLE1BQWYsQ0FBc0IsZUFBdEI7QUFDQWlCLFVBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJLLE1BQWpCLENBQXdCLGVBQXhCO0FBQ0EzQixZQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixDQUFtQ0ssTUFBbkMsQ0FBMEMsZUFBMUM7QUFDRCxHQUpEO0FBTUE5QixRQUFNLENBQUNxQixnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFFBQUlyQixNQUFNLENBQUNnQyxVQUFQLElBQXFCMEMsVUFBekIsRUFBcUM7QUFDbkNWLGFBQU8sQ0FBQ3ZDLFNBQVIsQ0FBa0JDLE1BQWxCLENBQXlCLGVBQXpCO0FBQ0FxQixZQUFNLENBQUN0QixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixlQUF4QjtBQUNBdkIsY0FBUSxDQUFDNEIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0Q7QUFDRixHQU5EO0FBT0QsQyxDQUNEOzs7QUFDQSxTQUFTbUcsbUJBQVQsQ0FBNkJuRCxVQUE3QixFQUF5QztBQUN2QyxNQUFJaEIsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIscUJBQTFCLENBQWY7QUFDQSxNQUFJZ0QsT0FBTyxHQUFHeEcsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIsMkJBQTFCLENBQWQ7O0FBQ0EsTUFBR0QsUUFBUSxLQUFLLElBQWhCLEVBQXFCO0FBQ25CYSxtQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkQsVUFBSUEsT0FBTyxDQUFDOEQsa0JBQVIsS0FBK0IsSUFBbkMsRUFBeUM7QUFDdkM5RCxlQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxVQUFVQyxDQUFWLEVBQWE7QUFDN0NBLFdBQUMsQ0FBQ1ksY0FBRjtBQUNBLGNBQUlhLE1BQU0sR0FBR2lCLE9BQU8sQ0FBQzhELGtCQUFyQjs7QUFDQSxjQUFJOUgsTUFBTSxDQUFDZ0MsVUFBUCxHQUFvQjBDLFVBQXhCLEVBQW9DO0FBQ2xDM0Isa0JBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJLLE1BQWpCLENBQXdCLGtCQUF4QjtBQUNBa0MsbUJBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsR0FBdEIsRUFBMkJtQixTQUEzQixDQUFxQ0ssTUFBckMsQ0FBNEMsa0JBQTVDLEVBRmtDLENBR2xDOztBQUNBL0IsYUFBQyxDQUFDZ0QsTUFBRCxDQUFELENBQVVnRixPQUFWLEdBQW9CeEUsUUFBcEIsR0FBK0JXLElBQS9CLENBQW9DLHVCQUFwQyxFQUE2REUsV0FBN0QsQ0FBeUUsa0JBQXpFO0FBQ0Q7QUFDRixTQVREO0FBVUQ7QUFDRixLQWJEO0FBY0Q7O0FBR0RwRSxRQUFNLENBQUNxQixnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFFBQUlyQixNQUFNLENBQUNnQyxVQUFQLElBQXFCMEMsVUFBekIsRUFBcUM7QUFDbkNILHFCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxZQUFJakIsTUFBTSxHQUFHaUIsT0FBTyxDQUFDOEQsa0JBQXJCO0FBQ0EvRSxjQUFNLENBQUN0QixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixrQkFBeEI7QUFDQXNDLGVBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsR0FBdEIsRUFBMkJtQixTQUEzQixDQUFxQ0MsTUFBckMsQ0FBNEMsa0JBQTVDO0FBQ0QsT0FKRDtBQUtEO0FBQ0YsR0FSRDtBQVNEOztBQUVELFNBQVNzRyxtQkFBVCxHQUErQjtBQUM3QmpJLEdBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCRSxFQUF6QixDQUE0QixPQUE1QixFQUFxQyxZQUFZO0FBQy9DRixLQUFDLENBQUMsSUFBRCxDQUFELENBQVF3RCxRQUFSLEdBQW1CVyxJQUFuQixDQUF3QixtQkFBeEIsRUFBNkNFLFdBQTdDLENBQXlELGtCQUF6RDtBQUNELEdBRkQ7QUFHRDs7QUFFRCxTQUFTNkQsa0JBQVQsQ0FBNEJ2RCxVQUE1QixFQUF3QztBQUN0QyxNQUFJaEIsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIscUJBQTFCLENBQWY7QUFDQVksaUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFFBQUlBLE9BQU8sQ0FBQzFELGFBQVIsQ0FBc0IsbUJBQXRCLE1BQStDLElBQW5ELEVBQXlEO0FBQ3ZEMEQsYUFBTyxDQUFDM0MsZ0JBQVIsQ0FBeUIsV0FBekIsRUFBc0MsWUFBWTtBQUNoRDJDLGVBQU8sQ0FBQzFELGFBQVIsQ0FBc0IscUJBQXRCLEVBQTZDbUIsU0FBN0MsQ0FBdURFLEdBQXZELENBQTJELFVBQTNEO0FBQ0QsT0FGRDtBQUdBcUMsYUFBTyxDQUFDM0MsZ0JBQVIsQ0FBeUIsVUFBekIsRUFBcUMsWUFBWTtBQUMvQzJDLGVBQU8sQ0FDSjFELGFBREgsQ0FDaUIscUJBRGpCLEVBRUdtQixTQUZILENBRWFDLE1BRmIsQ0FFb0IsVUFGcEI7QUFHRCxPQUpEO0FBS0Q7QUFDRixHQVhEO0FBYUExQixRQUFNLENBQUNxQixnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFFBQUlyQixNQUFNLENBQUNnQyxVQUFQLEdBQW9CMEMsVUFBeEIsRUFBb0M7QUFDbENkLFdBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FDR0MsSUFESCxDQUNRM0QsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIscUJBQTFCLENBRFIsRUFFR0ksT0FGSCxDQUVXLFVBQVVtRSxJQUFWLEVBQWdCO0FBQ3ZCQSxZQUFJLENBQUN6RyxTQUFMLENBQWVDLE1BQWYsQ0FBc0IsVUFBdEI7QUFDRCxPQUpIO0FBS0Q7QUFDRixHQVJEO0FBU0Q7O0FBRUQsU0FBU1osY0FBVCxHQUEwQjtBQUN4QixNQUFJOEYsVUFBVSxHQUFHLElBQWpCO0FBQ0FnQixrQkFBZ0IsQ0FBQ2hCLFVBQUQsQ0FBaEI7QUFDQWlCLHFCQUFtQixDQUFDakIsVUFBRCxDQUFuQjtBQUNBcUIsb0JBQWtCLENBQUNyQixVQUFELENBQWxCO0FBQ0Q7O0FBRUQsU0FBU0csUUFBVCxHQUFvQjtBQUNsQixNQUFJNUcsUUFBUSxDQUFDRyxhQUFULENBQXVCLGVBQXZCLENBQUosRUFBNkM7QUFDM0MsUUFBSTZILFFBQVEsR0FBR0MsS0FBSyxFQUFwQjtBQUNBRCxZQUFRLENBQUNFLE9BQVQ7O0FBQ0EsUUFDRSxDQUFDLENBQUQsS0FBT0MsU0FBUyxDQUFDQyxTQUFWLENBQW9CQyxPQUFwQixDQUE0QixNQUE1QixDQUFQLElBQ0FGLFNBQVMsQ0FBQ0csVUFBVixDQUFxQkQsT0FBckIsQ0FBNkIsVUFBN0IsSUFBMkMsQ0FGN0MsRUFHRTtBQUNBLFVBQUlFLE1BQU0sR0FBR3ZJLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixRQUF2QixDQUFiO0FBQ0FvSSxZQUFNLENBQUNDLFlBQVAsQ0FBb0IsS0FBcEIsRUFBMkIsMkNBQTNCO0FBQ0Q7QUFDRjtBQUNGLEMsQ0FDRDs7O0FBQ0EsU0FBU0MsYUFBVCxDQUF1QkMsR0FBdkIsRUFBNEI7QUFDMUIsTUFBSXhJLEVBQUUsR0FBR0YsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEJrRixHQUExQixDQUFUO0FBQ0EsTUFBSUMsVUFBVSxHQUFHLENBQUMsQ0FBbEI7QUFDQSxNQUFJQyxTQUFTLEdBQUcsQ0FBQyxDQUFqQjtBQUNBLE1BQUluQyxVQUFVLEdBQUcsR0FBakIsQ0FKMEIsQ0FLMUI7O0FBQ0FyQyxpQkFBZSxDQUFDbEUsRUFBRCxDQUFmLENBQW9CMEQsT0FBcEIsQ0FBNEIsVUFBVW1FLElBQVYsRUFBZ0I7QUFDMUNBLFFBQUksQ0FBQzFCLEtBQUwsQ0FBVzNELE1BQVgsR0FBb0IsRUFBcEIsQ0FEMEMsQ0FDbEI7O0FBQ3hCaUcsY0FBVSxHQUFHWixJQUFJLENBQUM1RixZQUFsQixDQUYwQyxDQUVWOztBQUNoQ3lHLGFBQVMsR0FBR0EsU0FBUyxHQUFHRCxVQUFaLEdBQXlCQyxTQUF6QixHQUFxQ0QsVUFBakQ7QUFDRCxHQUpEOztBQUtBLE1BQUkzSSxRQUFRLENBQUNxQixJQUFULENBQWN3SCxXQUFkLEdBQTRCcEMsVUFBaEMsRUFBNEM7QUFDMUNyQyxtQkFBZSxDQUFDbEUsRUFBRCxDQUFmLENBQW9CMEQsT0FBcEIsQ0FBNEIsVUFBVW1FLElBQVYsRUFBZ0I7QUFDMUNBLFVBQUksQ0FBQzFCLEtBQUwsQ0FBVzNELE1BQVgsYUFBdUJrRyxTQUF2QjtBQUNELEtBRkQ7QUFHRDtBQUNGOztBQUVELFNBQVNFLFFBQVQsQ0FBa0JDLEdBQWxCLEVBQXVCTCxHQUF2QixFQUE0QjtBQUMxQixNQUFJeEksRUFBRSxHQUFHRixRQUFRLENBQUN3RCxnQkFBVCxDQUEwQmtGLEdBQTFCLENBQVQsQ0FEMEIsQ0FFMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0F0RSxpQkFBZSxDQUFDbEUsRUFBRCxDQUFmLENBQW9CMEQsT0FBcEIsQ0FBNEIsVUFBVW1FLElBQVYsRUFBZ0I7QUFDMUMsUUFBSWlCLEdBQUcsR0FBR2pCLElBQUksQ0FBQ2tCLFNBQWY7O0FBQ0EsUUFBSUQsR0FBRyxDQUFDaEMsTUFBSixHQUFhK0IsR0FBakIsRUFBc0I7QUFDcEJHLGdCQUFVLEdBQUdGLEdBQUcsQ0FBQ0csU0FBSixDQUFjLENBQWQsRUFBaUJKLEdBQUcsR0FBRyxDQUF2QixJQUE0QixLQUF6QztBQUNBaEIsVUFBSSxDQUFDcUIsU0FBTCxHQUFpQkYsVUFBakI7QUFDRCxLQUhELE1BR087QUFDTEEsZ0JBQVUsR0FBR0YsR0FBRyxDQUFDRyxTQUFKLENBQWMsQ0FBZCxFQUFpQkosR0FBakIsSUFBd0IsS0FBckM7QUFDQWhCLFVBQUksQ0FBQ3FCLFNBQUwsR0FBaUJGLFVBQWpCO0FBQ0Q7QUFDRixHQVREO0FBVUFULGVBQWEsQ0FBQ0MsR0FBRCxDQUFiO0FBQ0Q7O0FBRUQsU0FBU1csYUFBVCxDQUF1Qm5KLEVBQXZCLEVBQTJCMEMsTUFBM0IsRUFBbUM7QUFDakMsTUFBSTVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBSixFQUFnQztBQUM5QixRQUFJMkQsT0FBTyxHQUFHN0QsUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixDQUFkO0FBQ0EsUUFBSXNHLE9BQU8sR0FBR3hHLFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCWixNQUExQixDQUFkO0FBQ0FpQixXQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDNEMsV0FBSyxDQUFDL0IsY0FBTjtBQUNBOEIsYUFBTyxDQUFDeUYsSUFBUjtBQUNBN0YsV0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkI2QyxPQUEzQixFQUFvQzVDLE9BQXBDLENBQTRDLFVBQVVDLE9BQVYsRUFBbUI7QUFDN0RBLGVBQU8sQ0FBQzBGLE9BQVIsR0FBa0IsS0FBbEI7QUFDRCxPQUZEO0FBR0QsS0FORDtBQU9EO0FBQ0Y7O0FBRUQsU0FBU0MsbUJBQVQsR0FBK0I7QUFDN0IsTUFBSXhKLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixvQkFBdkIsTUFBaUQsSUFBckQsRUFBMkQ7QUFDekRzSSxpQkFBYSxDQUFDLG9CQUFELENBQWI7QUFDRCxHQUg0QixDQUk3Qjs7O0FBQ0EsTUFBSXpJLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixrQkFBdkIsTUFBK0MsSUFBbkQsRUFBeUQ7QUFDdkQySSxZQUFRLENBQUMsRUFBRCxFQUFLLGtCQUFMLENBQVI7QUFDRDtBQUNGOztBQUVELFNBQVNXLFFBQVQsR0FBb0I7QUFDbEI3SixHQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QjhKLE9BQTdCLENBQXFDO0FBQ25DQyxhQUFTLEVBQUUsT0FEd0I7QUFFbkM5RixXQUFPLEVBQUUsT0FGMEI7QUFHbkNkLFVBQU0sRUFBRTtBQUgyQixHQUFyQztBQUtBbkQsR0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJFLEVBQTdCLENBQWdDLHFCQUFoQyxFQUF1RCxZQUFZO0FBQ2pFLFFBQUk4SixZQUFZLEdBQUcsTUFBTWhLLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlELElBQVIsQ0FBYSxrQkFBYixDQUF6QixDQURpRSxDQUVqRTtBQUNBOztBQUNBLFFBQUlqRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxJQUFSLENBQWEscUJBQWIsS0FBdUMsWUFBM0MsRUFBeUQ7QUFDdkRqRCxPQUFDLENBQUNnSyxZQUFELENBQUQsQ0FBZ0I3RixJQUFoQixDQUFxQixnQkFBckIsRUFBdUNyRCxJQUF2QyxDQUE0QyxnQkFBNUM7QUFDRDs7QUFDRCxRQUFJZCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxJQUFSLENBQWEscUJBQWIsS0FBdUMsZ0JBQTNDLEVBQTZEO0FBQzNEakQsT0FBQyxDQUFDZ0ssWUFBRCxDQUFELENBQWdCN0YsSUFBaEIsQ0FBcUIsZ0JBQXJCLEVBQXVDckQsSUFBdkMsQ0FBNEMsb0JBQTVDO0FBQ0Q7QUFDRixHQVZEO0FBV0Q7O0FBRUQsU0FBU21KLEdBQVQsR0FBZTtBQUNiLE1BQUk3SixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBSixFQUEwQztBQUN4QzJKLE9BQUcsQ0FBQ3hHLElBQUosQ0FBUztBQUNQeUcsVUFBSSxFQUFFO0FBREMsS0FBVDtBQUdEO0FBQ0YsQyxDQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFNBQVNDLFNBQVQsR0FBcUI7QUFDbkIsTUFBSWhLLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixZQUF2QixDQUFKLEVBQTBDO0FBQ3hDLFFBQUlvRCxRQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQixZQUExQixDQUFmO0FBQ0EsUUFBSVosTUFBTSxHQUFHNUMsUUFBUSxDQUFDRyxhQUFULENBQXVCLFFBQXZCLENBQWIsQ0FGd0MsQ0FJeEM7O0FBQ0EsUUFBSThKLEtBQUssR0FBRyxDQUFaO0FBRUF4RyxTQUFLLENBQUNKLFNBQU4sQ0FBZ0JLLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQkosUUFBM0IsRUFBcUNLLE9BQXJDLENBQTZDLFVBQVVDLE9BQVYsRUFBbUI7QUFDOUQ7QUFDQUEsYUFBTyxDQUFDMkUsWUFBUixDQUFxQixPQUFyQixFQUE4QnlCLEtBQUssRUFBbkMsRUFGOEQsQ0FJOUQ7O0FBQ0FwRyxhQUFPLENBQUMvQixPQUFSLEdBQWtCLFlBQVk7QUFFNUI7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHUSxZQUFJb0ksWUFBWSxHQUFHckcsT0FBTyxDQUFDTSxZQUFSLENBQXFCLE9BQXJCLENBQW5CO0FBQ0F2RSxTQUFDLENBQUMsV0FBRCxDQUFELENBQWU4RixLQUFmLENBQXFCLFdBQXJCLEVBQWtDd0UsWUFBbEM7QUFFQXRILGNBQU0sQ0FBQzRGLFlBQVAsQ0FBb0IsS0FBcEIsRUFBMkIzRSxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsVUFBckIsQ0FBM0IsRUFkNEIsQ0FlNUI7O0FBQ0FOLGVBQU8sQ0FBQ3ZDLFNBQVIsQ0FBa0JFLEdBQWxCLENBQXNCLFdBQXRCO0FBQ0FpQyxhQUFLLENBQUNKLFNBQU4sQ0FBZ0JLLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQkosUUFBM0IsRUFBcUM0RyxNQUFyQyxDQUE0QyxVQUFVcEMsSUFBVixFQUFnQjtBQUMxRCxpQkFBT0EsSUFBSSxLQUFLbEUsT0FBaEI7QUFDRCxTQUZELEVBRUdELE9BRkgsQ0FFVyxVQUFVbUUsSUFBVixFQUFnQjtBQUN6QkEsY0FBSSxDQUFDekcsU0FBTCxDQUFlQyxNQUFmLENBQXNCLFdBQXRCO0FBQ0QsU0FKRDtBQUtELE9BdEJEO0FBdUJELEtBNUJEO0FBNkJEO0FBQ0Y7O0FBRUQsU0FBUzZJLGtCQUFULEdBQThCO0FBQzVCLE1BQUlsSyxFQUFFLEdBQUdOLENBQUMsQ0FBQyx3QkFBRCxDQUFWOztBQUNBLE1BQUlNLEVBQUosRUFBUTtBQUNOLFNBQUssSUFBSW1LLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUduSyxFQUFFLENBQUM4RyxNQUF2QixFQUErQnFELENBQUMsRUFBaEMsRUFBb0M7QUFDbEMsVUFBSUMsZ0JBQWdCLEdBQUdwSyxFQUFFLENBQUNxSyxFQUFILENBQU1GLENBQU4sRUFBU0csUUFBVCxDQUFrQixLQUFsQixFQUF5QnhELE1BQWhELENBRGtDLENBRWxDOztBQUNBLFVBQUlzRCxnQkFBZ0IsSUFBSSxDQUF4QixFQUEyQjtBQUN6QnBLLFVBQUUsQ0FBQ3FLLEVBQUgsQ0FBTUYsQ0FBTixFQUFTdEYsUUFBVCxDQUFrQix3QkFBbEI7QUFDRCxPQUZELE1BRU87QUFDTDdFLFVBQUUsQ0FBQ3FLLEVBQUgsQ0FBTUYsQ0FBTixFQUFTcEcsV0FBVCxDQUFxQix3QkFBckI7QUFDRDtBQUNGO0FBQ0Y7QUFDRjs7QUFFRCxTQUFTd0csdUJBQVQsQ0FBaUNDLGNBQWpDLEVBQWlEQyxRQUFqRCxFQUEyRDtBQUN6RCxNQUFJekssRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUJ1SyxjQUF2QixDQUFUO0FBQ0EsTUFBSTlILE1BQU0sR0FBRzVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QndLLFFBQXZCLENBQWI7O0FBQ0EsTUFBSXpLLEVBQUosRUFBUTtBQUNOO0FBQ0EsUUFBSXFELFFBQVEsR0FBR3JELEVBQUUsQ0FBQ3NELGdCQUFILENBQW9CLHdCQUFwQixDQUFmLENBRk0sQ0FHTjs7QUFDQUMsU0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkJKLFFBQTNCLEVBQXFDSyxPQUFyQyxDQUE2QyxVQUFVQyxPQUFWLEVBQW1CO0FBQzlEQSxhQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDLFlBQUkwSixVQUFVLEdBQUcxSyxFQUFFLENBQUNzRCxnQkFBSCxDQUFvQiw4QkFBcEIsRUFBb0R3RCxNQUFyRSxDQUQ0QyxDQUU1Qzs7QUFDQSxZQUFJNEQsVUFBVSxHQUFHLENBQWpCLEVBQW9CO0FBQ2xCaEksZ0JBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJFLEdBQWpCLENBQXFCLGlCQUFyQjtBQUNELFNBRkQsTUFFTztBQUNMb0IsZ0JBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJDLE1BQWpCLENBQXdCLGlCQUF4QjtBQUNEO0FBQ0YsT0FSRDtBQVNELEtBVkQ7QUFXQSxRQUFJc0osYUFBYSxHQUFHN0ssUUFBUSxDQUFDRyxhQUFULENBQXVCLHFCQUF2QixDQUFwQjtBQUNBMEssaUJBQWEsQ0FBQzNKLGdCQUFkLENBQStCLE9BQS9CLEVBQXdDLFlBQVk7QUFDbEQwQixZQUFNLENBQUN0QixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixpQkFBeEI7QUFDRCxLQUZEO0FBR0Q7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVN1SixjQUFULEdBQTBCO0FBQ3hCLE1BQUk1SyxFQUFFLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixPQUF2QixDQUFUOztBQUNBLE1BQUlQLENBQUMsQ0FBQ00sRUFBRCxDQUFMLEVBQVc7QUFDVE4sS0FBQyxDQUFDTSxFQUFELENBQUQsQ0FBTTZLLFVBQU4sR0FBbUJDLElBQW5CLENBQXdCO0FBQ3RCQyxXQUFLLEVBQUVyTCxDQUFDLENBQUNtTCxVQUFGLENBQWFHLFFBQWIsQ0FBc0JELEtBRFA7QUFDYztBQUNwQ0UsVUFBSSxFQUFFLE9BRmdCO0FBRVA7QUFDZkMsYUFBTyxFQUFFLE9BSGE7QUFHSjtBQUNsQkMsVUFBSSxFQUFFLElBSmdCLENBSVg7O0FBSlcsS0FBeEI7QUFNRDtBQUVGLEMsQ0FDRDs7O0FBQ0EsU0FBU0MsU0FBVCxHQUFxQjtBQUNuQixNQUFJcEwsRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsZUFBdkIsQ0FBVDs7QUFDQSxNQUFJRCxFQUFKLEVBQVE7QUFDTkEsTUFBRSxDQUFDZ0IsZ0JBQUgsQ0FBb0IsUUFBcEIsRUFBOEIsWUFBWTtBQUN4QztBQUNBcUssY0FBUSxDQUFDQyxJQUFULEdBQWdCLEtBQUtDLE9BQUwsQ0FBYSxLQUFLQyxhQUFsQixFQUFpQ0MsS0FBakQ7QUFDRCxLQUhEO0FBSUQ7QUFDRjs7QUFFRCxTQUFTM0ssU0FBVCxHQUFxQjtBQUNuQixNQUFJLENBQUM0SyxjQUFjLENBQUMsbUJBQUQsQ0FBbkIsRUFBMEM7QUFDeENoTSxLQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQnFFLFdBQTFCLENBQXNDLFFBQXRDO0FBQ0FyRSxLQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQitDLEtBQTFCLENBQWdDLFlBQVk7QUFDMUMvQyxPQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQm1GLFFBQTFCLENBQW1DLFFBQW5DO0FBQ0EsVUFBSThHLE9BQU8sR0FBRyxJQUFJQyxJQUFKLEVBQWQsQ0FGMEMsQ0FHMUM7O0FBQ0FELGFBQU8sQ0FBQ0UsV0FBUixDQUFvQkYsT0FBTyxDQUFDRyxXQUFSLEtBQXdCLEVBQTVDO0FBQ0FDLGVBQVMsQ0FBQyxtQkFBRCxFQUFzQixNQUF0QixFQUE4QkosT0FBOUIsQ0FBVDtBQUNELEtBTkQ7QUFPRCxHQVRELE1BVUs7QUFDSGpNLEtBQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCbUYsUUFBMUIsQ0FBbUMsUUFBbkM7QUFDRDtBQUNGLEMsQ0FDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFTa0gsU0FBVCxDQUFtQkMsSUFBbkIsRUFBeUJQLEtBQXpCLEVBQWdDUSxNQUFoQyxFQUNBO0FBQ0NuTSxVQUFRLENBQUNvTSxNQUFULEdBQWtCRixJQUFJLEdBQUcsR0FBUCxHQUFhUCxLQUFiLEdBQXFCLFlBQXJCLEdBQW9DUSxNQUFNLENBQUNFLFdBQVAsRUFBdEQ7QUFDQSxDLENBQ0Q7OztBQUNBLFNBQVNULGNBQVQsQ0FBd0JVLEdBQXhCLEVBQTZCO0FBQzNCLE1BQUlDLGNBQWMsR0FBR3ZNLFFBQVEsQ0FBQ29NLE1BQVQsQ0FDbEJJLEtBRGtCLENBQ1osR0FEWSxFQUVsQkMsR0FGa0IsQ0FFZCxVQUFVaE0sSUFBVixFQUFnQndKLEtBQWhCLEVBQXVCO0FBQUUsV0FBT3hKLElBQUksQ0FBQytMLEtBQUwsQ0FBVyxHQUFYLENBQVA7QUFBeUIsR0FGcEMsRUFHbEJyQyxNQUhrQixDQUdYLFVBQVV1QyxXQUFWLEVBQXVCO0FBQUUsV0FBT0EsV0FBVyxDQUFDLENBQUQsQ0FBWCxDQUFlQyxJQUFmLE1BQXlCTCxHQUFoQztBQUFzQyxHQUhwRCxFQUdzRCxDQUh0RCxDQUFyQjtBQUlBLFNBQU8sQ0FBQyxDQUFDQyxjQUFGLEdBQW1CQSxjQUFjLENBQUMsQ0FBRCxDQUFqQyxHQUF1QyxJQUE5QztBQUNELEMsQ0FFRDs7O0FBQ0EzTSxDQUFDLENBQUNJLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVk7QUFDNUJxTCxXQUFTO0FBQ1RySyxlQUFhO0FBQ2JzRixjQUFZO0FBQ1pPLGFBQVc7QUFDWE8sa0JBQWdCLEdBTFksQ0FNNUI7O0FBQ0FDLG9CQUFrQjtBQUNsQkMseUJBQXVCO0FBQ3ZCWixhQUFXO0FBQ1hhLGNBQVk7QUFDWlosVUFBUTtBQUNSakMsaUJBQWU7QUFDZmtGLEtBQUc7QUFDSEcsV0FBUztBQUNUL0Usc0JBQW9CO0FBQ3BCb0UsZUFBYSxDQUFDLHFCQUFELEVBQXdCLHdCQUF4QixDQUFiO0FBQ0F2RCw2QkFBMkI7QUFDM0IyRCxVQUFRO0FBQ1JXLG9CQUFrQjtBQUNsQksseUJBQXVCLENBQUMsa0JBQUQsRUFBcUIsaUJBQXJCLENBQXZCO0FBQ0FBLHlCQUF1QixDQUFDLG9CQUFELEVBQXVCLHdCQUF2QixDQUF2QjtBQUNBbkUsZ0NBQThCO0FBQzlCd0UsZ0JBQWM7QUFDZGxLLFlBQVU7QUFDVjZILGVBQWEsQ0FBQyxzQkFBRCxDQUFiO0FBQ0QsQ0ExQkQ7O0FBMkJBNUksTUFBTSxDQUFDK00sTUFBUCxHQUFnQixZQUFZO0FBQzFCcEQscUJBQW1CO0FBQ25CM0UsWUFBVTtBQUNYLENBSEQsQyxDQUlBOzs7QUFDQWpGLENBQUMsQ0FBQ0MsTUFBRCxDQUFELENBQVVnTixNQUFWLENBQWlCLFlBQVk7QUFDM0JyRCxxQkFBbUI7QUFDbkIzRSxZQUFVO0FBQ1ZNLDJCQUF5QixDQUFDLG9CQUFELEVBQXVCLEdBQXZCLENBQXpCO0FBQ0FrQyxrQkFBZ0I7QUFDaEJvQixlQUFhLENBQUMsc0JBQUQsQ0FBYjtBQUNELENBTkQsRSxDQU9BOztBQUNBN0ksQ0FBQyxDQUFDQyxNQUFELENBQUQsQ0FBVWlOLE1BQVYsQ0FBaUIsWUFBWTtBQUMzQi9MLGFBQVc7QUFDWixDQUZELEUiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjQDQuMC9hc3NldHMvanMvbWFpbi5qc1wiKTtcbiIsIiQod2luZG93KS5vbignbG9hZCcsIGZ1bmN0aW9uICgpIHtcclxuICAkKCcucC1pbmRleC1sb2FkaW5nJykuZmFkZU91dCgpO1xyXG59KVxyXG4vLyBhamF4IOmFjeWQiEpRMyDlvJXlhaXoqK3nva5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2h0bWxbbGFuZz1cInpoLXR3XCJdJyk7XHJcbiAgaWYgKGVsKSB7XHJcbiAgICBpZiAoJChcIiNoZWFkZXJcIikpIHtcclxuICAgICAgJC5hamF4KHtcclxuICAgICAgICB1cmw6IFwiYWpheC9faGVhZGVyLmh0bWxcIixcclxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXHJcbiAgICAgICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxyXG4gICAgICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgJChcIiNoZWFkZXJcIikuaHRtbChkYXRhKTtcclxuICAgICAgICBoZWFkZXJGdW5jdGlvbigpO1xyXG4gICAgICAgIGdvVG9BbmNob3IoKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAoJChcIiNzaWRlTGlua1wiKSkge1xyXG4gICAgICAkLmFqYXgoe1xyXG4gICAgICAgIHVybDogXCJhamF4L19zaWRlTGluay5odG1sXCIsXHJcbiAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxyXG4gICAgICAgIGRhdGFUeXBlOiBcImh0bWxcIixcclxuICAgICAgfSkuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICQoXCIjc2lkZUxpbmtcIikuaHRtbChkYXRhKTtcclxuICAgICAgICB0b2dnbGVTaWRlTGluaygpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICgkKFwiI2Zvb3RlclwiKSkge1xyXG4gICAgICAkLmFqYXgoe1xyXG4gICAgICAgIHVybDogXCJhamF4L19mb290ZXIuaHRtbFwiLFxyXG4gICAgICAgIG1ldGhvZDogXCJHRVRcIixcclxuICAgICAgICBkYXRhVHlwZTogXCJodG1sXCIsXHJcbiAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAkKFwiI2Zvb3RlclwiKS5odG1sKGRhdGEpO1xyXG4gICAgICAgIGdvVG9wKCk7XHJcbiAgICAgICAgZ29Ub3BTdGlja3koKTtcclxuICAgICAgICBjb29raWVCYXIoKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufSk7XHJcbi8v6Iux5paH54mI55qEYWpheFxyXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaHRtbFtsYW5nPVwiZW5cIl0nKTtcclxuICBpZiAoZWwpIHtcclxuICAgIGlmICgkKFwiI2hlYWRlckVuXCIpKSB7XHJcbiAgICAgICQuYWpheCh7XHJcbiAgICAgICAgdXJsOiBcIi4uL2FqYXgvX2hlYWRlcl9lbi5odG1sXCIsXHJcbiAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxyXG4gICAgICAgIGRhdGFUeXBlOiBcImh0bWxcIixcclxuICAgICAgfSkuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICQoXCIjaGVhZGVyRW5cIikuaHRtbChkYXRhKTtcclxuICAgICAgICBoZWFkZXJGdW5jdGlvbigpO1xyXG4gICAgICAgIGdvVG9BbmNob3IoKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAoJChcIiNzaWRlTGlua0VuXCIpKSB7XHJcbiAgICAgICQuYWpheCh7XHJcbiAgICAgICAgdXJsOiBcIi4uL2FqYXgvX3NpZGVMaW5rX2VuLmh0bWxcIixcclxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXHJcbiAgICAgICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxyXG4gICAgICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgJChcIiNzaWRlTGlua0VuXCIpLmh0bWwoZGF0YSk7XHJcbiAgICAgICAgdG9nZ2xlU2lkZUxpbmsoKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAoJChcIiNmb290ZXJFblwiKSkge1xyXG4gICAgICAkLmFqYXgoe1xyXG4gICAgICAgIHVybDogXCIuLi9hamF4L19mb290ZXJfZW4uaHRtbFwiLFxyXG4gICAgICAgIG1ldGhvZDogXCJHRVRcIixcclxuICAgICAgICBkYXRhVHlwZTogXCJodG1sXCIsXHJcbiAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAkKFwiI2Zvb3RlckVuXCIpLmh0bWwoZGF0YSk7XHJcbiAgICAgICAgZ29Ub3AoKTtcclxuICAgICAgICBnb1RvcFN0aWNreSgpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcbn0pO1xyXG5cclxuZnVuY3Rpb24gdG9vbHNMaXN0ZW5lcigpIHtcclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgZnVuY3Rpb24gKGUpIHtcclxuICAgIGlmIChlLmtleUNvZGUgPT09IDkpIHtcclxuICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKFwianMtdXNlTW91c2VcIik7XHJcbiAgICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmFkZChcImpzLXVzZUtleWJvYXJkXCIpO1xyXG4gICAgfVxyXG4gIH0pO1xyXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy11c2VLZXlib2FyZFwiKTtcclxuICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmFkZChcImpzLXVzZU1vdXNlXCIpO1xyXG4gIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiB0b2dnbGVTaWRlTGluaygpIHtcclxuICB2YXIgZWxUdyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2h0bWxbbGFuZz1cInpoLXR3XCJdJyk7XHJcbiAgdmFyIGVsRW4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdodG1sW2xhbmc9XCJlblwiXScpO1xyXG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtUb2dnbGVCdG5cIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgIHRoaXMuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xyXG4gICAgaWYgKGVsVHcpIHtcclxuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua1wiKS5jbGFzc0xpc3QudG9nZ2xlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XHJcbiAgICB9XHJcbiAgICBpZiAoZWxFbikge1xyXG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rRW5cIikuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xyXG4gICAgfVxyXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNwYWdlXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJib2R5XCIpLmNsYXNzTGlzdC50b2dnbGUoXCJvdmVyZmxvdy1oaWRkZW5cIik7XHJcbiAgICAvL+aJk+mWi3NpZGVNZW515pmC77yM5oqK5bGV6ZaL55qEbWVudemXnOaOiVxyXG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5sLWhlYWRlci1oYW1idXJnZXJcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XHJcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmwtaGVhZGVyLW1lbnVcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XHJcbiAgICAvL+aJk+mWi3NpZGVNZW515pmC77yM5oqKZ290b3DmjqjliLDml4HpgorljrtcclxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtGb290ZXJcIikuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xyXG4gIH0pO1xyXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+PSAxMjAwKSB7XHJcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtUb2dnbGVCdG5cIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xyXG4gICAgICBpZiAoZWxUdykge1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChlbEVuKSB7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua0VuXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICAgICAgfVxyXG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3BhZ2VcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xyXG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiYm9keVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwib3ZlcmZsb3ctaGlkZGVuXCIpO1xyXG4gICAgICAvL1vnp7vpmaRd5omT6ZaLc2lkZU1lbnXmmYLvvIzmiopnb3RvcOaOqOWIsOaXgemCiuWOu1xyXG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rRm9vdGVyXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICAgIH1cclxuICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gZ29Ub3AoKSB7XHJcbiAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNnb1RvcFwiKS5vbmNsaWNrID0gZnVuY3Rpb24gKGUpIHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICQoXCJodG1sLCBib2R5XCIpLmFuaW1hdGUoe1xyXG4gICAgICBzY3JvbGxUb3A6IDAsXHJcbiAgICB9LFxyXG4gICAgICA1MDBcclxuICAgICk7XHJcbiAgfTtcclxufVxyXG5cclxuZnVuY3Rpb24gZ29Ub3BTdGlja3koKSB7XHJcbiAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua0Zvb3RlclwiKTtcclxuICAvL+ioiOeul2Zvb3RlcuebruWJjemrmOW6plxyXG4gIHZhciBmb290ZXJIZWlnaHRWYXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmwtZm9vdGVyXCIpLmNsaWVudEhlaWdodDtcclxuICAvLyBjb25zb2xlLmxvZyhgZm9vdGVySGVpZ2h0VmFyICsgJHtmb290ZXJIZWlnaHRWYXJ9YCk7XHJcbiAgLy/nlbbkuIvntrLpoIHpq5jluqYo5pW06aCB6ZW3KVxyXG4gIHZhciBzY3JvbGxIZWlnaHRWYXIgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsSGVpZ2h0O1xyXG4gIC8vIGNvbnNvbGUubG9nKGBzY3JvbGxIZWlnaHRWYXIgKyAke3Njcm9sbEhlaWdodFZhcn1gKTtcclxuICAvL+eVtuS4i+eVq+mdoumrmOW6pih3aW5kb3cgaGVpZ2h0KVxyXG4gIHZhciB3aW5kb3dIZWlnaHRWYXIgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XHJcbiAgLy8gY29uc29sZS5sb2coYHdpbmRvd0hlaWdodFZhciArICR7d2luZG93SGVpZ2h0VmFyfWApO1xyXG4gIC8v5o2y5YuV5Lit55qE55Wr6Z2i5pyA5LiK56uv6IiH57ay6aCB6aCC56uv5LmL6ZaT55qE6Led6ZuiXHJcbiAgdmFyIHNjcm9sbFRvcFZhciA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3A7XHJcbiAgLy8gY29uc29sZS5sb2coYHNjcm9sbFRvcFZhciArICR7c2Nyb2xsVG9wVmFyfWApO1xyXG4gIC8vIGNvbnNvbGUubG9nKHdpbmRvd0hlaWdodFZhciArIHNjcm9sbFRvcFZhciArIGZvb3RlckhlaWdodFZhcik7XHJcbiAgLy/lpoLmnpzmlbTpoIHplbc9PeeVq+mdoumrmOW6pivmjbLli5Xpq5jluqZcclxuICBpZiAoZWwpIHtcclxuICAgIGlmIChzY3JvbGxIZWlnaHRWYXIgPD0gd2luZG93SGVpZ2h0VmFyICsgc2Nyb2xsVG9wVmFyICsgZm9vdGVySGVpZ2h0VmFyKSB7XHJcbiAgICAgIGVsLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdGlja3lcIik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBlbC5jbGFzc0xpc3QuYWRkKFwianMtc3RpY2t5XCIpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24gZ29Ub0FuY2hvcigpIHtcclxuICB2YXIgaGVhZGVySGVpZ2h0ID0gXCJcIjtcclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gaGVhZGVySGVpZ2h0ID0gJChcIi5sLWhlYWRlclwiKS5oZWlnaHQoKTtcclxuICB9KTtcclxuICAkKCcuanMtZ29Ub0FuY2hvcicpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB2YXIgdGFyZ2V0ID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XHJcbiAgICB2YXIgdGFyZ2V0UG9zID0gJCh0YXJnZXQpLm9mZnNldCgpLnRvcDtcclxuICAgIHZhciBoZWFkZXJIZWlnaHQgPSAkKFwiLmwtaGVhZGVyXCIpLmhlaWdodCgpO1xyXG4gICAgJCgnaHRtbCxib2R5JykuYW5pbWF0ZSh7XHJcbiAgICAgIHNjcm9sbFRvcDogdGFyZ2V0UG9zIC0gaGVhZGVySGVpZ2h0XHJcbiAgICB9LCAxMDAwKTtcclxuICAgIHZhciB0cmlnZ2VyMDIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhbWJ1cmdlclwiKTtcclxuICAgIHZhciB0YXJnZXQwMiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbWVudVwiKTtcclxuICAgIHRyaWdnZXIwMi5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcclxuICAgIHRhcmdldDAyLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gIH0pO1xyXG59XHJcbi8v6aKo55C055SoZnVuY3Rpb25cclxuZnVuY3Rpb24gQWNjb3JkaW9uKGVsLCB0YXJnZXQsIHNpYmxpbmdzKSB7XHJcbiAgdGhpcy5lbCA9IGVsO1xyXG4gIHRoaXMudGFyZ2V0ID0gdGFyZ2V0O1xyXG4gIHRoaXMuc2libGluZ3MgPSBzaWJsaW5ncztcclxufVxyXG5BY2NvcmRpb24ucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLmVsKTtcclxuICB2YXIgdGFyZ2V0ID0gdGhpcy50YXJnZXQ7XHJcbiAgdmFyIHNpYmxpbmdzID0gdGhpcy5zaWJsaW5ncztcclxuICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4gICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICBpZiAoc2libGluZ3MgPT0gdHJ1ZSkge1xyXG4gICAgICAgIGlmICh0aGlzLnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgLy8gdGhpcy5xdWVyeVNlbGVjdG9yKHRhcmdldCkuY2xhc3NMaXN0LnRvZ2dsZShcclxuICAgICAgICAgIC8vICAgXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiXHJcbiAgICAgICAgICAvLyApO1xyXG4gICAgICAgICAgLy/ngrrkuobopoHog73lnKjmiZPplovkuIDlgIvmmYLvvIzlhbbku5bnmoTmlLblkIjvvIzpgJnpgormlLnnlKhqUXVlcnk9PT5cclxuICAgICAgICAgICQodGhpcykuZmluZCh0YXJnZXQpLnRvZ2dsZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XHJcbiAgICAgICAgICAvLyDlop7liqDol43nt5rmoYbmlYjmnpxcclxuICAgICAgICAgICQodGhpcykudG9nZ2xlQ2xhc3MoXCJjLWFjY29yZGlvbi1idG4tZm9jdXNcIik7XHJcbiAgICAgICAgICAkKHRoaXMpLnNpYmxpbmdzKCkuZmluZCh0YXJnZXQpLnJlbW92ZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XHJcbiAgICAgICAgICAvLyDlop7liqDol43nt5rmoYbmlYjmnpxcclxuICAgICAgICAgICQodGhpcykuc2libGluZ3MoKS5yZW1vdmVDbGFzcyhcImMtYWNjb3JkaW9uLWJ0bi1mb2N1c1wiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdGFyZ2V0XCIpKS5jbGFzc0xpc3QudG9nZ2xlKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XHJcbiAgICAgICAgLy/ngrrkuobopoHog73lnKjmiZPplovkuIDlgIvmmYLvvIzlhbbku5bnmoTmlLblkIjvvIzpgJnpgormlLnnlKhqUXVlcnk9PT5cclxuICAgICAgICB2YXIgYXR0ID0gJCh0cmlnZ2VyKS5hdHRyKFwiZGF0YS10YXJnZXRcIik7XHJcbiAgICAgICAgJChhdHQpLnRvZ2dsZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIikuc2libGluZ3MoKS5yZW1vdmVDbGFzcyhcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xyXG4gICAgICAgIC8vICQoYCR7dHJpZ2dlci5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKX1gKS50b2dnbGVDbGFzcyhcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcclxuICAgICAgICAvL+eUqOS6hmpRdWVyeeS5i+W+jO+8jOmAmee1hOacg+iiq+aok+S4iumCo+WAi+e2geWcqOS4gOi1t++8jOaJgOS7peWPr+S7peecgeeVpVxyXG4gICAgICAgIC8vIHRyaWdnZXIuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmICh0aGlzLnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgdGhpcy5xdWVyeVNlbGVjdG9yKHRhcmdldCkuY2xhc3NMaXN0LnRvZ2dsZShcclxuICAgICAgICAgICAgXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS10YXJnZXRcIikpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcclxuICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIG5vZGVMaXN0VG9BcnJheShub2RlTGlzdENvbGxlY3Rpb24pIHtcclxuICByZXR1cm4gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwobm9kZUxpc3RDb2xsZWN0aW9uKTtcclxufVxyXG4vLyBjb25zdCBub2RlTGlzdFRvQXJyYXkgPSAobm9kZUxpc3RDb2xsZWN0aW9uKSA9PiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChub2RlTGlzdENvbGxlY3Rpb24pO1xyXG4vKipcclxuICogXHJcbiAqIEBwYXJhbSB7Kn0gZWwgOmNsYXNzIG5hbWVcclxuICogQHBhcmFtIHsqfSB0YXJnZXQgOuiiq+W9semfv+WIsOeahOebruaomVxyXG4gKiBAcGFyYW0geyp9IG1lZGlhUXVlcnkgOuaWt+m7nuioreWumlxyXG4gKiBA6Kqq5piOIFwiZWxcIuiIh1widGFyZ2V0XCIgdG9nZ2xlIOS4gOWAi+WPq2pzLWFjdGl2ZeeahGNsYXNz6KaB55So5Ye65LuA6bq85pWI5p6c77yM56uv55yL5L2g5oCO6bq85a+ranMtYWN0aXZl55qEY3Nz5pWI5p6c5bGV6ZaL5oiW5pS25ZCI5LuA6bq85p2x6KW/XHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gdG9nZ2xlVmlzaWFibGUoZWwsIHRhcmdldCwgbWVkaWFRdWVyeSkge1xyXG4gIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoZWwpO1xyXG4gIHZhciB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRhcmdldCk7XHJcbiAgaWYgKHRhcmdldCkge1xyXG4gICAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHRoaXMuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjdGl2ZVwiKTtcclxuICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjdGl2ZVwiKTtcclxuICAgICAgICB2YXIgaGFzTWVkaWFRdWVyeSA9IG1lZGlhUXVlcnk7XHJcbiAgICAgICAgaWYgKGhhc01lZGlhUXVlcnkgIT09IFwiXCIpIHtcclxuICAgICAgICAgIHZhciBpc01vYmlsZSA9IHdpbmRvdy5pbm5lcldpZHRoIDwgbWVkaWFRdWVyeTtcclxuICAgICAgICAgIGlmIChpc01vYmlsZSkge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWZ1bmN0aW9uTWVudU9wZW5lZFwiKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1mdW5jdGlvbk1lbnVPcGVuZWRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+PSBtZWRpYVF1ZXJ5KSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwianMtZnVuY3Rpb25NZW51T3BlbmVkXCIpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4vKmVsPeinuOeZvOWwjeixoSjplovpl5wpKi9cclxuLyp0YXJnZXQ96KKr5o6n5Yi255qE54mp5Lu2KOeHiCkqL1xyXG5mdW5jdGlvbiBjbGlja0NvbmZpcm0oZWwsIHRhcmdldCkge1xyXG4gIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoZWwpO1xyXG4gIHZhciB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRhcmdldCk7XHJcbiAgaWYgKHRhcmdldCkge1xyXG4gICAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWN0aXZlXCIpO1xyXG4gICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwianMtZnVuY3Rpb25NZW51T3BlbmVkXCIpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4vL+KGkeKGkeKGkemAmueUqGZ1bmN0aW9uLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbmZ1bmN0aW9uIGNyZWF0ZUFjY29yZGlvbigpIHtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmMtYWNjb3JkaW9uLWJ0bicpKSB7XHJcbiAgICBuZXcgQWNjb3JkaW9uKFwiLmMtYWNjb3JkaW9uLWJ0blwiLCBcIi5jLWFjY29yZGlvbi1idG4taWNvblwiLCB0cnVlKS5pbml0KCk7XHJcbiAgfVxyXG59XHJcbi8vIFRPRE86IOaDs+i+puazlemAmeWAi+WFqOWfn+iuiuaVuOimgeiZleeQhuS4gOS4i1xyXG52YXIgc3RyID0gMDtcclxuXHJcbmZ1bmN0aW9uIHNob3dBbGxUYWIoKSB7XHJcbiAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gOTkyICYmIHN0ciA9PSAxKSB7XHJcbiAgICAkKFwiLnRhYi1wYW5lXCIpLnJlbW92ZUNsYXNzKFwic2hvdyBhY3RpdmVcIikuZmlyc3QoKS5hZGRDbGFzcyhcInNob3cgYWN0aXZlXCIpO1xyXG4gICAgJChcIi5jLXRhYi1saW5rRVwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKS5wYXJlbnQoKS5maXJzdCgpLmZpbmQoXCIuYy10YWItbGlua0VcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICBzdHIgPSAwO1xyXG4gIH1cclxuICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5OTIgJiYgc3RyID09IDApIHtcclxuICAgICQoXCIudGFiLXBhbmVcIikuYWRkQ2xhc3MoXCJzaG93IGFjdGl2ZVwiKTtcclxuICAgIHN0ciA9IDE7XHJcbiAgfVxyXG4gIC8vIGNvbnNvbGUubG9nKHN0cik7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRvZ2dsZXBGaWx0ZXJTdWJtZW51KCkge1xyXG4gIC8vIFRPRE86IOacieepuuippuippueci+Wvq+S4gOWAi+aJi+apn+eJiOWPr+S7pemHneWwjeWFp+WuuemrmOW6puWinuWKoHNob3dtb3Jl77yM5Lim5LiU6KiY6YyE5q2kc2hvd21vcmXlt7LntpPpu57plovpgY7vvIzkuI3mnIPlm6Dngrpyd2TlsLHlj4joqIjnrpfkuIDmrKFcclxuICAvL+aJi+apn+eJiOmhr+ekuuWFqOmDqFxyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucC1wcm9kdWN0cy1zZWFyY2gtc2hvd01vcmUnKSkge1xyXG4gICAgbmV3IEFjY29yZGlvbihcIi5wLXByb2R1Y3RzLXNlYXJjaC1zaG93TW9yZVwiLCB1bmRlZmluZWQsIGZhbHNlKS5pbml0KCk7XHJcbiAgfVxyXG4gIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIuYy1hY2NvcmRpb24tYnRuXCIpO1xyXG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4gICAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdGFyZ2V0XCIpKTtcclxuICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKGVsLnF1ZXJ5U2VsZWN0b3IoXCIucC1wcm9kdWN0cy1zZWFyY2gtc2hvd01vcmVcIikpIHtcclxuICAgICAgICBlbC5xdWVyeVNlbGVjdG9yKFwiLnAtcHJvZHVjdHMtc2VhcmNoLXNob3dNb3JlXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcclxuICAgICAgICBlbC5xdWVyeVNlbGVjdG9yKFwiLmMtYWNjb3JkaW9uLWNvbnRlbnQtbGF5b3V0XCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfSk7XHJcbiAgLy/miYvmqZ/niYjplovpl5zpgbjllq5cclxuICAvL+WVhuWTgeWwiOWNgFxyXG4gIHRvZ2dsZVZpc2lhYmxlKFwiLmNsb3NlXCIsIFwiLnAtcHJvZHVjdHMtc2VhcmNoXCIsIFwiXCIpO1xyXG4gIHRvZ2dsZVZpc2lhYmxlKFwiLnAtcHJvZHVjdHMtc2VhcmNoLWJ0blwiLCBcIi5wLXByb2R1Y3RzLXNlYXJjaFwiLCA5OTIpO1xyXG4gIGNsaWNrQ29uZmlybShcIiNqcy1jb25maXJtXCIsIFwiLnAtcHJvZHVjdHMtc2VhcmNoXCIpO1xyXG4gIC8v6ZaA5biC5p+l6KmiXHJcbiAgdG9nZ2xlVmlzaWFibGUoXCIudi1kcm9wZG93bi1idG5cIiwgXCIudi1kcm9wZG93bi1tZW51XCIsIDk5Mik7XHJcbiAgdG9nZ2xlVmlzaWFibGUoXCIuY2xvc2VcIiwgXCIudi1kcm9wZG93bi1tZW51XCIsIFwiXCIpO1xyXG4gIGNsaWNrQ29uZmlybShcIiNqcy1jb25maXJtXCIsIFwiLnYtZHJvcGRvd24tbWVudVwiKTtcclxuICAvLyBpZiAoJChcIi52LWRyb3Bkb3duLWJ0blwiKS5oYXNDbGFzcyhcImpzLWFjdGl2ZVwiKSkge1xyXG4gIC8vICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImJvZHlcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAvLyAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi52LWRyb3Bkb3duLWJ0blwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWN0aXZlXCIpO1xyXG4gIC8vICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnYtZHJvcGRvd24tbWVudVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWN0aXZlXCIpO1xyXG4gIC8vICAgfSk7XHJcbiAgLy8gfVxyXG5cclxufVxyXG5cclxuZnVuY3Rpb24gY29uZmlybUlmRG91YmxlTWVudU9wZW5lZChlbCwgbWVkaWFRdWVyeSkge1xyXG4gIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IG1lZGlhUXVlcnkgJiYgJChlbCkuaGFzQ2xhc3MoXCJqcy1hY3RpdmVcIikgJiYgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdCA9PSBcIlwiKSB7XHJcbiAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LmFkZChcImpzLW1lbnVPcGVuZWRcIik7XHJcbiAgfVxyXG59XHJcbi8vIOi8quaSreebuOmXnC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbmZ1bmN0aW9uIFNsaWNrKGVsLCBzbGlkZXNUb1Nob3csIHNsaWRlc1BlclJvdywgcm93cywgcmVzcG9uc2l2ZSkge1xyXG4gIHRoaXMuZWwgPSBlbDtcclxuICB0aGlzLnNsaWRlc1RvU2hvdyA9IHNsaWRlc1RvU2hvdztcclxuICB0aGlzLnNsaWRlc1BlclJvdyA9IHNsaWRlc1BlclJvdztcclxuICB0aGlzLnJvd3MgPSByb3dzO1xyXG4gIHRoaXMucmVzcG9uc2l2ZSA9IHJlc3BvbnNpdmU7XHJcbn07XHJcblxyXG5TbGljay5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcclxuICAkKHRoaXMuZWwgKyBcIiAudi1zbGlja1wiKS5zbGljayh7XHJcbiAgICBhcnJvd3M6IHRydWUsXHJcbiAgICBzbGlkZXNUb1Nob3c6IHRoaXMuc2xpZGVzVG9TaG93LFxyXG4gICAgc2xpZGVzUGVyUm93OiB0aGlzLnNsaWRlc1BlclJvdyxcclxuICAgIHJvd3M6IHRoaXMucm93cyxcclxuICAgIHByZXZBcnJvdzogYDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwidi1zbGljay1hcnJvd3MtcHJldlwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLWxlZnRcIj48L2k+PC9idXR0b24+YCxcclxuICAgIG5leHRBcnJvdzogYDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwidi1zbGljay1hcnJvd3MtbmV4dFwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPjwvYnV0dG9uPmAsXHJcbiAgICAvLyBhdXRvcGxheTogdHJ1ZSxcclxuICAgIC8vIGxhenlMb2FkOiBcInByb2dyZXNzaXZlXCIsXHJcbiAgICByZXNwb25zaXZlOiB0aGlzLnJlc3BvbnNpdmUsXHJcbiAgfSk7XHJcbn07XHJcblxyXG5mdW5jdGlvbiBzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKSB7XHJcbiAgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuYy1zaG9wVGFibGUtcndkLXZlckEgdGJvZHkgdHInKTtcclxuICBub2RlTGlzdFRvQXJyYXkodHJpZ2dlcnMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcclxuICAgIHZhciB0cmlnZ2VyVGQgPSB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3JBbGwoJ3RkJyk7XHJcbiAgICB2YXIgbnVtMDEgPSB0cmlnZ2VyVGRbMF0uY2xpZW50SGVpZ2h0O1xyXG4gICAgdmFyIG51bTAyID0gdHJpZ2dlclRkWzFdLmNsaWVudEhlaWdodDtcclxuICAgIHZhciBudW0wMyA9IHRyaWdnZXJUZFsyXS5jbGllbnRIZWlnaHQ7XHJcbiAgICB2YXIgdG90YWxOdW0gPSBudW0wMSArIG51bTAyICsgbnVtMDM7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCAxMjAwICYmIHRyaWdnZXIuY2xhc3NMaXN0LmNvbnRhaW5zKCdqcy1hY3RpdmUnKSA9PSBmYWxzZSkge1xyXG4gICAgICB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9IGAke3RvdGFsTnVtfXB4YDtcclxuICAgIH0gZWxzZSBpZiAod2luZG93LmlubmVyV2lkdGggPiAxMjAwKSB7XHJcbiAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gXCJcIjtcclxuICAgICAgdHJpZ2dlci5jbGFzc0xpc3QucmVtb3ZlKCdqcy1hY3RpdmUnKTtcclxuICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcclxuICAgIH1cclxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgMTIwMCAmJiB0cmlnZ2VyLmNsYXNzTGlzdC5jb250YWlucygnanMtYWN0aXZlJykgPT0gZmFsc2UpIHtcclxuICAgICAgICB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9IGAke3RvdGFsTnVtfXB4YDtcclxuICAgICAgfSBlbHNlIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDEyMDApIHtcclxuICAgICAgICB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9IFwiXCI7XHJcbiAgICAgICAgdHJpZ2dlci5jbGFzc0xpc3QucmVtb3ZlKCdqcy1hY3RpdmUnKTtcclxuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gdG9nZ2xlU3RvcmVUYWJsZUhlaWdodEF0TW9iaWxlKCkge1xyXG4gIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmMtc2hvcFRhYmxlLXJ3ZC12ZXJBIHRib2R5IHRyJyk7XHJcbiAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICB2YXIgdHJpZ2dlclRkID0gdHJpZ2dlci5xdWVyeVNlbGVjdG9yQWxsKCd0ZCcpO1xyXG4gICAgdmFyIG51bTAxID0gdHJpZ2dlclRkWzBdLmNsaWVudEhlaWdodDtcclxuICAgIHZhciBudW0wMiA9IHRyaWdnZXJUZFsxXS5jbGllbnRIZWlnaHQ7XHJcbiAgICB2YXIgbnVtMDMgPSB0cmlnZ2VyVGRbMl0uY2xpZW50SGVpZ2h0O1xyXG4gICAgdmFyIHRvdGFsTnVtID0gbnVtMDEgKyBudW0wMiArIG51bTAzO1xyXG4gICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwidGQ6Zmlyc3QtY2hpbGRcIikuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEyMDAgJiYgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPT0gXCJcIikge1xyXG4gICAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gYCR7dG90YWxOdW19cHhgO1xyXG4gICAgICAgIHRyaWdnZXIuY2xhc3NMaXN0LnJlbW92ZSgnanMtYWN0aXZlJyk7XHJcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9IFwiXCI7XHJcbiAgICAgICAgdHJpZ2dlci5jbGFzc0xpc3QuYWRkKFwianMtYWN0aXZlXCIpO1xyXG4gICAgICAgIHRyaWdnZXIucXVlcnlTZWxlY3RvcihcImlcIikuY2xhc3NMaXN0LmFkZChcImpzLXN1Ym1lbnVPcGVuZWRcIik7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH0pO1xyXG59XHJcblxyXG4vLyBUT0RPOiDmnInnqbrnnIvkuIDkuIvngrrku4DpurxuZXfnrKzkuInlgIvlsLHlh7rkuovkuoZcclxuZnVuY3Rpb24gY3JlYXRlU2xpY2tzKCkge1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnYtc2xpY2tcIikpIHtcclxuICAgIHZhciB0YXJnZXRzID0gW1wiI25ld3NcIiwgXCIjcmVjb21tYW5kYXRpb25cIl07XHJcbiAgICB0YXJnZXRzLmZvckVhY2goZnVuY3Rpb24gKHRhcmdldCkge1xyXG4gICAgICBuZXcgU2xpY2sodGFyZ2V0LCA0LCAxLCAxLCBbe1xyXG4gICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcclxuICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgc2xpZGVzVG9TaG93OiAyXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgYnJlYWtwb2ludDogNTc2LFxyXG4gICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDIsXHJcbiAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgXSkuaW5pdCgpO1xyXG4gICAgfSk7XHJcbiAgICBuZXcgU2xpY2soXCIjdmlkZW9cIiwgMSwgMiwgMiwgW3tcclxuICAgICAgYnJlYWtwb2ludDogOTkyLFxyXG4gICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMixcclxuICAgICAgICBzbGlkZXNQZXJSb3c6IDEsXHJcbiAgICAgICAgcm93czogMSxcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgYnJlYWtwb2ludDogNTc2LFxyXG4gICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMixcclxuICAgICAgICBzbGlkZXNQZXJSb3c6IDEsXHJcbiAgICAgICAgcm93czogMSxcclxuICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgXSkuaW5pdCgpO1xyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24gcmlvbmV0U2xpY2soKSB7XHJcbiAgdmFyIGVsID0gXCIudi1yaW9uZXRTbGlja1wiO1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xyXG4gICAgJChlbCkuc2xpY2soe1xyXG4gICAgICAvLyBjZW50ZXJNb2RlOiB0cnVlLFxyXG4gICAgICAvLyBjZW50ZXJQYWRkaW5nOiAnNjBweCcsXHJcbiAgICAgIHNsaWRlc1RvU2hvdzogNCxcclxuICAgICAgYXJyb3dzOiB0cnVlLFxyXG4gICAgICBwcmV2QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtdGFiQnRuU2xpY2stcHJldlwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLWxlZnRcIj48L2k+PC9idXR0b24+YCxcclxuICAgICAgbmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLW5leHRcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1yaWdodFwiPjwvaT48L2J1dHRvbj5gLFxyXG4gICAgICBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxyXG4gICAgICByZXNwb25zaXZlOiBbe1xyXG4gICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcclxuICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgc2xpZGVzVG9TaG93OiAzLFxyXG4gICAgICAgICAgZG90czogdHJ1ZSxcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBicmVha3BvaW50OiA1NzYsXHJcbiAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgIHNsaWRlc1RvU2hvdzogMixcclxuICAgICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgICAvLyB2YXJpYWJsZVdpZHRoOiB0cnVlLFxyXG4gICAgICAgICAgZG90czogdHJ1ZSxcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIF0sXHJcbiAgICB9KTtcclxuICAgIC8vICAgJCgnLnYtdGFiQnRuU2xpY2stcHJldicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XHJcbiAgICAvLyAgICAgJCgnLnYtdGFiQnRuU2xpY2snKS5zbGljaygnc2xpY2tQcmV2Jyk7XHJcbiAgICAvLyAgfSk7XHJcbiAgICAvLyAgICQoJy52LXRhYkJ0blNsaWNrLW5leHQnKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xyXG4gICAgLy8gICAgICQoJy52LXRhYkJ0blNsaWNrJykuc2xpY2soJ3NsaWNrTmV4dCcpO1xyXG4gICAgLy8gIH0pO1xyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24gdGFiQnRuU2xpY2soKSB7XHJcbiAgdmFyIGVsID0gXCIudi10YWJCdG5TbGlja1wiO1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xyXG4gICAgdmFyIHRhYkxlbmd0aCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpLnF1ZXJ5U2VsZWN0b3JBbGwoJy5jLXRhYi1jb250YWluZXInKS5sZW5ndGg7XHJcbiAgICAgICAgICAvL+WmguaenOe4veaVuOWkp+aWvO+8luWwseeUqO+8lu+8jCDvvJLvvJDvvJLvvJLvvJDvvJLvvJHvvJXlrqLmiLbopoHmsYLlpJrkuIDlgIsuLi4uXHJcbiAgICAgICAgICAvL3ZhciBzbGlkZXNUb1Nob3dOdW0gPSB0YWJMZW5ndGggPiA2ID8gNiA6IHRhYkxlbmd0aDtcclxuICAgICAgICAgIHZhciBzbGlkZXNUb1Nob3dOdW0gPSB0YWJMZW5ndGg7XHJcbiAgICAkKGVsKS5zbGljayh7XHJcbiAgICAgIHNsaWRlc1RvU2hvdzogc2xpZGVzVG9TaG93TnVtLFxyXG4gICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgYXJyb3dzOiB0cnVlLFxyXG4gICAgICBwcmV2QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtdGFiQnRuU2xpY2stcHJldlwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLWxlZnRcIj48L2k+PC9idXR0b24+YCxcclxuICAgICAgbmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLW5leHRcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1yaWdodFwiPjwvaT48L2J1dHRvbj5gLFxyXG4gICAgICBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxyXG4gICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgIHJlc3BvbnNpdmU6IFt7XHJcbiAgICAgICAgYnJlYWtwb2ludDogMTIwMCxcclxuICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgc2xpZGVzVG9TaG93OiA0LFxyXG4gICAgICAgICAgdmFyaWFibGVXaWR0aDogZmFsc2UsXHJcbiAgICAgICAgICAvLyBzbGlkZXNUb1Njcm9sbDogNSxcclxuICAgICAgICAgIC8vIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgICAgLy8gY2VudGVyTW9kZTogZmFsc2UsXHJcbiAgICAgICAgfVxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgYnJlYWtwb2ludDogOTkyLFxyXG4gICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcclxuICAgICAgICAgIC8vIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgICAgLy8gY2VudGVyTW9kZTogZmFsc2UsXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgYnJlYWtwb2ludDogNzY4LFxyXG4gICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiB0cnVlLFxyXG4gICAgICAgICAgLy8gaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgICAgYXJyb3dzOiBmYWxzZSxcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIF0sXHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRhYkJ0blNsaWNrRml4ZWQoKSB7XHJcbiAgdmFyIGVsID0gXCIudi10YWJCdG5TbGlja1wiO1xyXG5cclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcclxuICAgIHZhciB0YWJMZW5ndGggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKS5xdWVyeVNlbGVjdG9yQWxsKCcuYy10YWItY29udGFpbmVyJykubGVuZ3RoOyAvLyB2YXIgc2xpZGVzVG9TaG93TnVtIFxyXG4gICAgLy/ljp/mnKzmmK/lsI/mlrzvvJblsLHliqDkuIroh7TkuK3vvIwg77yS77yQ77yS77yS77yQ77yS77yR77yV5a6i5oi26KaB5rGC5aSa5LiA5YCLLi4uLlxyXG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDEyMDAgJiYgdGFiTGVuZ3RoIDw9IDcpIHtcclxuICAgICAgJChcIi52LXRhYkJ0blNsaWNrIC5zbGljay10cmFja1wiKS5hZGRDbGFzcyhcInRleHQtbWQtY2VudGVyXCIpOyAvLyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc2xpY2stdHJhY2snKS5jbGFzc0xpc3QuYWRkKCd0ZXh0LW1kLWNlbnRlcicpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+PSA3NjggJiYgd2luZG93LmlubmVyV2lkdGggPCAxMjAwICYmIHRhYkxlbmd0aCA8IDUpIHtcclxuICAgICAgJChcIi52LXRhYkJ0blNsaWNrIC5zbGljay10cmFja1wiKS5hZGRDbGFzcyhcInRleHQtbWQtY2VudGVyXCIpOyAvLyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc2xpY2stdHJhY2snKS5jbGFzc0xpc3QuYWRkKCd0ZXh0LW1kLWNlbnRlcicpO1xyXG4gICAgfSAvLyBpZiAod2luZG93LmlubmVyV2lkdGggPCA3NjgpIHtcclxuICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGljay10cmFjaycpLmNsYXNzTGlzdC5yZW1vdmUoJ3RleHQtbWQtY2VudGVyJyk7XHJcbiAgICAvLyB9XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiB0YWJCdG5TbGlja01lbWJlcnMoKSB7XHJcbiAgdmFyIGVsID0gXCIudi10YWJCdG5TbGljay1tZW1iZXJzXCI7XHJcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpKSB7XHJcbiAgICAkKGVsKS5zbGljayh7XHJcbiAgICAgIHNsaWRlc1RvU2hvdzogNixcclxuICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXHJcbiAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgcHJldkFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLXByZXZcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1sZWZ0XCI+PC9pPjwvYnV0dG9uPmAsXHJcbiAgICAgIG5leHRBcnJvdzogYDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwidi10YWJCdG5TbGljay1uZXh0XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtcmlnaHRcIj48L2k+PC9idXR0b24+YCxcclxuICAgICAgbGF6eUxvYWQ6IFwicHJvZ3Jlc3NpdmVcIixcclxuICAgICAgaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICByZXNwb25zaXZlOiBbe1xyXG4gICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcclxuICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgc2xpZGVzVG9TaG93OiA1LFxyXG4gICAgICAgICAgdmFyaWFibGVXaWR0aDogZmFsc2UsXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgYnJlYWtwb2ludDogNzY4LFxyXG4gICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiB0cnVlLFxyXG4gICAgICAgICAgYXJyb3dzOiBmYWxzZSxcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIF0sXHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRhYkJ0blNsaWNrRml4ZWRNZW1iZXJzKCkge1xyXG4gIHZhciBlbCA9IFwiLnYtdGFiQnRuU2xpY2stbWVtYmVyc1wiO1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xyXG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDc2OCkge1xyXG4gICAgICAkKFwiLnYtdGFiQnRuU2xpY2stbWVtYmVycyAuc2xpY2stdHJhY2tcIikuYWRkQ2xhc3MoXCJ0ZXh0LW1kLWNlbnRlclwiKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLy8gRW5kIOi8quaSreebuOmXnC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbmZ1bmN0aW9uIHdhcnJhbnR5TGluaygpIHtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5wLWluZGV4LXdhcnJhbnR5LWxpbmtcIikubGVuZ3RoKSB7XHJcbiAgICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLnAtaW5kZXgtd2FycmFudHktbGlua1wiKTtcclxuICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4gICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW92ZXJcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRyaWdnZXIucXVlcnlTZWxlY3RvcihcIi5jLWJ0blwiKS5jbGFzc0xpc3QuYWRkKFwianMtYnRuSG92ZXJcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW91dFwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiLmMtYnRuXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1idG5Ib3ZlclwiKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuLy/miYvmqZ/niYjmvKLloKHpgbjllq5cclxuZnVuY3Rpb24gdG9nZ2xlTW9iaWxlTWVudShtZWRpYVF1ZXJ5KSB7XHJcbiAgdmFyIHRyaWdnZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhbWJ1cmdlclwiKTtcclxuICB2YXIgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNtZW51XCIpO1xyXG5cclxuICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICB0aGlzLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgdGFyZ2V0LmNsYXNzTGlzdC50b2dnbGUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC50b2dnbGUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gIH0pO1xyXG5cclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gbWVkaWFRdWVyeSkge1xyXG4gICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XHJcbiAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcclxuICAgIH1cclxuICB9KTtcclxufVxyXG4vL+aJi+apn+eJiOmiqOeQtOaKmOeWiumBuOWWrlxyXG5mdW5jdGlvbiB0b2dnbGVNb2JpbGVTdWJtZW51KG1lZGlhUXVlcnkpIHtcclxuICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmwtaGVhZGVyLW1lbnUtbGlua1wiKTtcclxuICB2YXIgdGFyZ2V0cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIubC1oZWFkZXItc3VibWVudS0taGlkZGVuXCIpO1xyXG4gIGlmKHRyaWdnZXJzICE9PSBudWxsKXtcclxuICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4gICAgICBpZiAodHJpZ2dlci5uZXh0RWxlbWVudFNpYmxpbmcgIT09IG51bGwpIHtcclxuICAgICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgdmFyIHRhcmdldCA9IHRyaWdnZXIubmV4dEVsZW1lbnRTaWJsaW5nO1xyXG4gICAgICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgbWVkaWFRdWVyeSkge1xyXG4gICAgICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XHJcbiAgICAgICAgICAgIHRyaWdnZXIucXVlcnlTZWxlY3RvcihcImlcIikuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XHJcbiAgICAgICAgICAgIC8v54K65LqG6ZaL5LiA5YCL6Zec5LiA5YCL77yM55SoalF1ZXJ55Yqg5a+rXHJcbiAgICAgICAgICAgICQodGFyZ2V0KS5wYXJlbnRzKCkuc2libGluZ3MoKS5maW5kKFwiLmwtaGVhZGVyLXN1Ym1lbnUgLCBpXCIpLnJlbW92ZUNsYXNzKFwianMtc3VibWVudU9wZW5lZFwiKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IG1lZGlhUXVlcnkpIHtcclxuICAgICAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgICAgdmFyIHRhcmdldCA9IHRyaWdnZXIubmV4dEVsZW1lbnRTaWJsaW5nO1xyXG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcclxuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gc2liaW5nTW9iaWxlU3VibWVudSgpIHtcclxuICAkKFwiLmwtaGVhZGVyLW1lbnUtaXRlbVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICQodGhpcykuc2libGluZ3MoKS5maW5kKFwiLmwtaGVhZGVyLXN1Ym1lbnVcIikucmVtb3ZlQ2xhc3MoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xyXG4gIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiB0b2dnbGVQY0hvdmVyU3RhdGUobWVkaWFRdWVyeSkge1xyXG4gIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIubC1oZWFkZXItbWVudS1pdGVtXCIpO1xyXG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4gICAgaWYgKHRyaWdnZXIucXVlcnlTZWxlY3RvcihcIi5sLWhlYWRlci1zdWJtZW51XCIpICE9PSBudWxsKSB7XHJcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlb3ZlclwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiLmwtaGVhZGVyLW1lbnUtbGlua1wiKS5jbGFzc0xpc3QuYWRkKFwianMtaG92ZXJcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW91dFwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdHJpZ2dlclxyXG4gICAgICAgICAgLnF1ZXJ5U2VsZWN0b3IoXCIubC1oZWFkZXItbWVudS1saW5rXCIpXHJcbiAgICAgICAgICAuY2xhc3NMaXN0LnJlbW92ZShcImpzLWhvdmVyXCIpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9KTtcclxuXHJcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgbWVkaWFRdWVyeSkge1xyXG4gICAgICBBcnJheS5wcm90b3R5cGUuc2xpY2VcclxuICAgICAgICAuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmwtaGVhZGVyLW1lbnUtbGlua1wiKSlcclxuICAgICAgICAuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgaXRlbS5jbGFzc0xpc3QucmVtb3ZlKFwianMtaG92ZXJcIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGhlYWRlckZ1bmN0aW9uKCkge1xyXG4gIHZhciBicmVha3BvaW50ID0gMTIwMDtcclxuICB0b2dnbGVNb2JpbGVNZW51KGJyZWFrcG9pbnQpO1xyXG4gIHRvZ2dsZU1vYmlsZVN1Ym1lbnUoYnJlYWtwb2ludCk7XHJcbiAgdG9nZ2xlUGNIb3ZlclN0YXRlKGJyZWFrcG9pbnQpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBsYXp5TG9hZCgpIHtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImltZ1tkYXRhLXNyY11cIikpIHtcclxuICAgIHZhciBvYnNlcnZlciA9IGxvemFkKCk7XHJcbiAgICBvYnNlcnZlci5vYnNlcnZlKCk7XHJcbiAgICBpZiAoXHJcbiAgICAgIC0xICE9PSBuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoXCJNU0lFXCIpIHx8XHJcbiAgICAgIG5hdmlnYXRvci5hcHBWZXJzaW9uLmluZGV4T2YoXCJUcmlkZW50L1wiKSA+IDBcclxuICAgICkge1xyXG4gICAgICB2YXIgaWZyYW1lID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImlmcmFtZVwiKTtcclxuICAgICAgaWZyYW1lLnNldEF0dHJpYnV0ZShcInNyY1wiLCBcImh0dHBzOi8vd3d3LnlvdXR1YmUuY29tL2VtYmVkL083cE5wUjNQeTY4XCIpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4vL+ioiOeul+OAjOacgOaWsOa2iOaBr+OAjeOAgeOAjOS6uuawo+aOqOiWpuOAjemAmemhnueahOWNoeeJh+WIl+ihqOeahOaomemhjOWtl+aVuO+8jOiuk+Wkp+WutueahOmrmOW6puS4gOaoo1xyXG5mdW5jdGlvbiBhdXRvRml4SGVpZ2h0KGNvbikge1xyXG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoY29uKTtcclxuICB2YXIgdGhpc0hlaWdodCA9IC0xO1xyXG4gIHZhciBtYXhIZWlnaHQgPSAtMTtcclxuICB2YXIgYnJlYWtwb2ludCA9IDc2ODtcclxuICAvL+eCuuS6hmll5LiN5pSv5o+0bm9kZWxpc3TnmoRmb3JFYWNo5L+u5q2jXHJcbiAgbm9kZUxpc3RUb0FycmF5KGVsKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICBpdGVtLnN0eWxlLmhlaWdodCA9IFwiXCI7IC8v5riF56m65LmL5YmN55qEc3R5bGVcclxuICAgIHRoaXNIZWlnaHQgPSBpdGVtLmNsaWVudEhlaWdodDsgLy/lj5blvpflt7LntpPmuJvpgY7pq5jluqbnmoRcclxuICAgIG1heEhlaWdodCA9IG1heEhlaWdodCA+IHRoaXNIZWlnaHQgPyBtYXhIZWlnaHQgOiB0aGlzSGVpZ2h0O1xyXG4gIH0pO1xyXG4gIGlmIChkb2N1bWVudC5ib2R5LmNsaWVudFdpZHRoID4gYnJlYWtwb2ludCkge1xyXG4gICAgbm9kZUxpc3RUb0FycmF5KGVsKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGl0ZW0uc3R5bGUuaGVpZ2h0ID0gYCR7bWF4SGVpZ2h0fXB4YDtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24gdGV4dEhpZGUobnVtLCBjb24pIHtcclxuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGNvbik7XHJcbiAgLy9pZeaJjeWft+ihjOmAmeWAi+i2hemBjuWtl+aVuOWinuWKoOWIquevgOiZn++8jOWFtuS7lueAj+imveWZqOmdoGNzc+iqnuazleWNs+WPr1xyXG4gIC8vIGlmIChcclxuICAvLyAgIG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZihcIk1TSUVcIikgIT09IC0xIHx8XHJcbiAgLy8gICBuYXZpZ2F0b3IuYXBwVmVyc2lvbi5pbmRleE9mKFwiVHJpZGVudC9cIikgPiAwXHJcbiAgLy8gKSB7XHJcbiAgLy8gICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChlbCkuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xyXG4gIC8vICAgICB2YXIgdHh0ID0gaXRlbS5pbm5lclRleHQ7XHJcbiAgLy8gICAgIGlmICh0eHQubGVuZ3RoID4gbnVtKSB7XHJcbiAgLy8gICAgICAgdHh0Q29udGVudCA9IHR4dC5zdWJzdHJpbmcoMCwgbnVtIC0gMSkgKyBcIi4uLlwiO1xyXG4gIC8vICAgICAgIGl0ZW0uaW5uZXJIVE1MID0gdHh0Q29udGVudDtcclxuICAvLyAgICAgfVxyXG4gIC8vICAgfSk7XHJcbiAgLy8gfVxyXG4gIG5vZGVMaXN0VG9BcnJheShlbCkuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgdmFyIHR4dCA9IGl0ZW0uaW5uZXJUZXh0O1xyXG4gICAgaWYgKHR4dC5sZW5ndGggPiBudW0pIHtcclxuICAgICAgdHh0Q29udGVudCA9IHR4dC5zdWJzdHJpbmcoMCwgbnVtIC0gMSkgKyBcIi4uLlwiO1xyXG4gICAgICBpdGVtLmlubmVySFRNTCA9IHR4dENvbnRlbnQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0eHRDb250ZW50ID0gdHh0LnN1YnN0cmluZygwLCBudW0pICsgXCIuLi5cIjtcclxuICAgICAgaXRlbS5pbm5lckhUTUwgPSB0eHRDb250ZW50O1xyXG4gICAgfVxyXG4gIH0pO1xyXG4gIGF1dG9GaXhIZWlnaHQoY29uKTtcclxufVxyXG5cclxuZnVuY3Rpb24gY2xlYXJDaGVja0JveChlbCwgdGFyZ2V0KSB7XHJcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpKSB7XHJcbiAgICB2YXIgdHJpZ2dlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpO1xyXG4gICAgdmFyIHRhcmdldHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHRhcmdldCk7XHJcbiAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgIHRyaWdnZXIuYmx1cigpO1xyXG4gICAgICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0YXJnZXRzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgICAgdHJpZ2dlci5jaGVja2VkID0gZmFsc2U7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBjYXJkQ29udGVudEZ1bmN0aW9uKCkge1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmMtY2FyZC1ib2R5LXRpdGxlXCIpICE9PSBudWxsKSB7XHJcbiAgICBhdXRvRml4SGVpZ2h0KFwiLmMtY2FyZC1ib2R5LXRpdGxlXCIpO1xyXG4gIH1cclxuICAvL+S6uuawo+aOqOiWpuWFp+aWh+Wtl+aVuOmZkOWItlxyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmMtY2FyZC1ib2R5LXR4dFwiKSAhPT0gbnVsbCkge1xyXG4gICAgdGV4dEhpZGUoNDgsIFwiLmMtY2FyZC1ib2R5LXR4dFwiKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRvb2xUaXBzKCkge1xyXG4gICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKHtcclxuICAgIHBsYWNlbWVudDogJ3JpZ2h0JyxcclxuICAgIHRyaWdnZXI6ICdob3ZlcicsXHJcbiAgICBvZmZzZXQ6IDMwLFxyXG4gIH0pO1xyXG4gICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS5vbignaW5zZXJ0ZWQuYnMudG9vbHRpcCcsIGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBnZXRUb290aXBzSWQgPSBcIiNcIiArICQodGhpcykuYXR0cihcImFyaWEtZGVzY3JpYmVkYnlcIik7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhnZXRUb290aXBzSWQpO1xyXG4gICAgLy8gY29uc29sZS5sb2coJCh0aGlzKS5hdHRyKFwiZGF0YS1vcmlnaW5hbC10aXRsZVwiKSA9PSBcIuW+t+Wci+iUoeWPuOaVuOS9jeW9seWDj+ezu+e1sVwiKTtcclxuICAgIGlmICgkKHRoaXMpLmF0dHIoXCJkYXRhLW9yaWdpbmFsLXRpdGxlXCIpID09IFwi5b635ZyL6JSh5Y+45pW45L2N5b2x5YOP57O757WxXCIpIHtcclxuICAgICAgJChnZXRUb290aXBzSWQpLmZpbmQoJy50b29sdGlwLWlubmVyJykuaHRtbChcIuW+t+Wci+iUoeWPuDxicj7mlbjkvY3lvbHlg4/ns7vntbFcIik7XHJcbiAgICB9XHJcbiAgICBpZiAoJCh0aGlzKS5hdHRyKFwiZGF0YS1vcmlnaW5hbC10aXRsZVwiKSA9PSBcIuazleWci+S+neimlui3r+WFqOaWueS9jeimluimuuaqoua4rOezu+e1sVwiKSB7XHJcbiAgICAgICQoZ2V0VG9vdGlwc0lkKS5maW5kKCcudG9vbHRpcC1pbm5lcicpLmh0bWwoXCLms5XlnIvkvp3oppbot688YnI+5YWo5pa55L2N6KaW6Ka65qqi5ris57O757WxXCIpO1xyXG4gICAgfVxyXG4gIH0pXHJcbn1cclxuXHJcbmZ1bmN0aW9uIGFvcygpIHtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIltkYXRhLWFvc11cIikpIHtcclxuICAgIEFPUy5pbml0KHtcclxuICAgICAgb25jZTogdHJ1ZSxcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG5cclxuLy8gZnVuY3Rpb24geXRIYW5kbGVyKCkge1xyXG4vLyAgIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiW2RhdGEtdXJsXVwiKSkge1xyXG4vLyAgICAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIltkYXRhLXVybF1cIik7XHJcbi8vICAgICB2YXIgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImlmcmFtZVwiKTtcclxuLy8gICAgIHZhciBpbmRleCA9IDA7XHJcbi8vICAgICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4vLyAgICAgICB0cmlnZ2VyLnNldEF0dHJpYnV0ZShcImluZGV4XCIsIGluZGV4KyspO1xyXG5cclxuLy8gICAgICAgdHJpZ2dlci5vbmNsaWNrID0gZnVuY3Rpb24gKCkge1xyXG4vLyAgICAgICAgIHZhciB0cmlnZ2VySW5kZXggPSB0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImluZGV4XCIpO1xyXG5cclxuLy8gICAgICAgICAkKCcjc2xpY2tfYWQnKS5zbGljaygnc2xpY2tHb1RvJywgdHJpZ2dlckluZGV4KTtcclxuXHJcbi8vICAgICAgICAgdGFyZ2V0LnNldEF0dHJpYnV0ZShcInNyY1wiLCB0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdXJsXCIpKTtcclxuLy8gICAgICAgICAvL2FsZXJ0KCt0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdXJsXCIpKTtcclxuLy8gICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5hZGQoXCJqcy1hY3RpdmVcIik7XHJcbi8vICAgICAgICAgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwodHJpZ2dlcnMpLmZpbHRlcihmdW5jdGlvbiAoaXRlbSkge1xyXG4vLyAgICAgICAgICAgcmV0dXJuIGl0ZW0gIT09IHRyaWdnZXI7XHJcbi8vICAgICAgICAgfSkuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xyXG4vLyAgICAgICAgICAgaXRlbS5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWN0aXZlXCIpO1xyXG4vLyAgICAgICAgIH0pO1xyXG4vLyAgICAgICB9O1xyXG4vLyAgICAgfSk7XHJcbi8vICAgfVxyXG4vLyB9XHJcbi8vc2Ft5pS55a+r6YGO55qE4oaT4oaT4oaT4oaT4oaT4oaTXHJcbmZ1bmN0aW9uIHl0SGFuZGxlcigpIHtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIltkYXRhLXVybF1cIikpIHtcclxuICAgIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbZGF0YS11cmxdXCIpO1xyXG4gICAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJpZnJhbWVcIik7XHJcblxyXG4gICAgLy/lsI3mh4nnmoRhZCBrZXkg5Y+v5Lul6Ieq6KGM5aGe5YWl5L2g6KaB55qEa2V5XHJcbiAgICB2YXIgaW5kZXggPSAwO1xyXG5cclxuICAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgIC8v5aGe5YWl5bCN5oeJ55qEQUQga2V55YC8XHJcbiAgICAgIHRyaWdnZXIuc2V0QXR0cmlidXRlKFwiaW5kZXhcIiwgaW5kZXgrKyk7XHJcblxyXG4gICAgICAvL2Fk6KKr6bue5pOK5LqL5Lu2XHJcbiAgICAgIHRyaWdnZXIub25jbGljayA9IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgLyo9PT09PeatpOiZlSDngrrmipNEQiBBROW7o+WRiuWclueJh+izh+aWmT09PT09XHJcbiAgICAgICAgdmFyIHRyaWdnZXJJbmRleCA9IHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiaW5kZXhcIik7IOaKk+WPlmFkIGtleSBkYXRhXHJcbiAgICAgICAgICAgICQuZ2V0KFwidXJsXCIse30sIGZ1bmN0aW9uKHJlc3VsdCl7XHJcbiAgICAgICAgICAkKFwiI3NsaWNrX2FkXCIpLmh0bWwoXCLoq4vloZ7lhaXlu6PlkYrlnJbniYdodG1s6LOH5paZXCIpO1xyXG4gICAgICAgICAgICAkKCcjc2xpY2tfYWQnKS5zbGljaygnc2xpY2tHb1RvJywgMCk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgPT09PT09PT09PT09PT09PT09PT09PT09Ki9cclxuXHJcblxyXG4gICAgICAgIHZhciB0cmlnZ2VySW5kZXggPSB0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImluZGV4XCIpO1xyXG4gICAgICAgICQoJyNzbGlja19hZCcpLnNsaWNrKCdzbGlja0dvVG8nLCB0cmlnZ2VySW5kZXgpO1xyXG5cclxuICAgICAgICB0YXJnZXQuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS11cmxcIikpO1xyXG4gICAgICAgIC8vYWxlcnQoK3RyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS11cmxcIikpO1xyXG4gICAgICAgIHRyaWdnZXIuY2xhc3NMaXN0LmFkZChcImpzLWFjdGl2ZVwiKTtcclxuICAgICAgICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0cmlnZ2VycykuZmlsdGVyKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICByZXR1cm4gaXRlbSAhPT0gdHJpZ2dlcjtcclxuICAgICAgICB9KS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICBpdGVtLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY3RpdmVcIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH07XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhcmRKdXN0aWZ5Q29udGVudCgpIHtcclxuICB2YXIgZWwgPSAkKFwiLmpzLWNhcmRKdXN0aWZ5Q29udGVudFwiKTtcclxuICBpZiAoZWwpIHtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZWwubGVuZ3RoOyBpKyspIHtcclxuICAgICAgdmFyIGVsQ2hpbGRyZW5MZW5ndGggPSBlbC5lcShpKS5jaGlsZHJlbignZGl2JykubGVuZ3RoO1xyXG4gICAgICAvLyBjb25zb2xlLmxvZyhlbENoaWxkcmVuTGVuZ3RoKTtcclxuICAgICAgaWYgKGVsQ2hpbGRyZW5MZW5ndGggPD0gMikge1xyXG4gICAgICAgIGVsLmVxKGkpLmFkZENsYXNzKFwianVzdGlmeS1jb250ZW50LWNlbnRlclwiKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBlbC5lcShpKS5yZW1vdmVDbGFzcyhcImp1c3RpZnktY29udGVudC1jZW50ZXJcIik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHN0b3JlRmlsdGVyTm90aWZpY2F0aW9uKGlucHV0Q29udGFpbmVyLCB0YXJnZXRFbCkge1xyXG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5wdXRDb250YWluZXIpO1xyXG4gIHZhciB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRhcmdldEVsKTtcclxuICBpZiAoZWwpIHtcclxuICAgIC8vIGNvbnNvbGUubG9nKGlucHV0Q29udGFpbmVyICsgXCIgKyBcIiArIHRhcmdldCk7XHJcbiAgICB2YXIgdHJpZ2dlcnMgPSBlbC5xdWVyeVNlbGVjdG9yQWxsKFwiaW5wdXRbdHlwZT0nY2hlY2tib3gnXVwiKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKHRyaWdnZXJzKTtcclxuICAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgY2hlY2tlZE51bSA9IGVsLnF1ZXJ5U2VsZWN0b3JBbGwoXCJpbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkXCIpLmxlbmd0aDtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhjaGVja2VkTnVtKTtcclxuICAgICAgICBpZiAoY2hlY2tlZE51bSA+IDApIHtcclxuICAgICAgICAgIHRhcmdldC5jbGFzc0xpc3QuYWRkKFwianMtaW5wdXRDaGVja2VkXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWlucHV0Q2hlY2tlZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgICB2YXIgY2xlYXJBbGxCdG5FbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjanMtY2xlYXJDaGVja0JveGVzXCIpO1xyXG4gICAgY2xlYXJBbGxCdG5FbC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWlucHV0Q2hlY2tlZFwiKTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4vL+a7vui7uOW8j+aXpeacn+mBuOWWrlxyXG5mdW5jdGlvbiBkYXRlTW9iaXNjcm9sbCgpIHtcclxuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2RhdGVcIilcclxuICBpZiAoJChlbCkpIHtcclxuICAgICQoZWwpLm1vYmlzY3JvbGwoKS5kYXRlKHtcclxuICAgICAgdGhlbWU6ICQubW9iaXNjcm9sbC5kZWZhdWx0cy50aGVtZSwgLy8gU3BlY2lmeSB0aGVtZSBsaWtlOiB0aGVtZTogJ2lvcycgb3Igb21pdCBzZXR0aW5nIHRvIHVzZSBkZWZhdWx0IFxyXG4gICAgICBtb2RlOiAnbWl4ZWQnLCAvLyBTcGVjaWZ5IHNjcm9sbGVyIG1vZGUgbGlrZTogbW9kZTogJ21peGVkJyBvciBvbWl0IHNldHRpbmcgdG8gdXNlIGRlZmF1bHQgXHJcbiAgICAgIGRpc3BsYXk6ICdtb2RhbCcsIC8vIFNwZWNpZnkgZGlzcGxheSBtb2RlIGxpa2U6IGRpc3BsYXk6ICdib3R0b20nIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcclxuICAgICAgbGFuZzogJ3poJyAvLyBTcGVjaWZ5IGxhbmd1YWdlIGxpa2U6IGxhbmc6ICdwbCcgb3Igb21pdCBzZXR0aW5nIHRvIHVzZSBkZWZhdWx0IFxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxufVxyXG4vL+S4i+aLiemBuOWWrui3s+i9iei2hemAo+e1kFxyXG5mdW5jdGlvbiBzZWxlY3RVUkwoKSB7XHJcbiAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5qcy1zZWxlY3RVUkxcIik7XHJcbiAgaWYgKGVsKSB7XHJcbiAgICBlbC5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgLy8gd2luZG93Lm9wZW4odGhpcy5vcHRpb25zW3RoaXMuc2VsZWN0ZWRJbmRleF0udmFsdWUpO1xyXG4gICAgICBsb2NhdGlvbi5ocmVmID0gdGhpcy5vcHRpb25zW3RoaXMuc2VsZWN0ZWRJbmRleF0udmFsdWU7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNvb2tpZUJhcigpIHtcclxuICBpZiAoIWdldENvb2tpZVZhbHVlKCdjb29raWVDb25zZW50UmVhZCcpKSB7XHJcbiAgICAkKFwiLmwtZm9vdGVyLWNvb2tpZS1iYXJcIikucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xyXG4gICAgJChcIi5sLWZvb3Rlci1jb29raWUtYnRuXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgJChcIi5sLWZvb3Rlci1jb29raWUtYmFyXCIpLmFkZENsYXNzKCdkLW5vbmUnKTtcclxuICAgICAgdmFyIGV4cGlyZXMgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAvL+aUvjMw5bm05omN5raI5aSx77yM5oeJ6Kmy5aSg5LmF5LqGXHJcbiAgICAgIGV4cGlyZXMuc2V0RnVsbFllYXIoZXhwaXJlcy5nZXRGdWxsWWVhcigpICsgMzApO1xyXG4gICAgICBzZXRDb29raWUoXCJjb29raWVDb25zZW50UmVhZFwiLCBcInRydWVcIiwgZXhwaXJlcyk7XHJcbiAgICB9KTtcclxuICB9XHJcbiAgZWxzZSB7XHJcbiAgICAkKFwiLmwtZm9vdGVyLWNvb2tpZS1iYXJcIikuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xyXG4gIH1cclxufVxyXG4vLyBjb29raWUg5Z+65pys5qC85byP54K6IG5hbWU9dmFsdWUgWztleHBpcmVzPWRhdGVdIFs7cGF0aD1wYXRoXSBbO2RvbWFpbj1kb21haW5dIFs7c2VjdXJlXVxyXG4vLyBleHBpcmVzPWRhdGUg6Kit5a6aIGNvb2tpZSDnmoTmnInmlYjmnJ/pmZDoh7MgZGF0ZSDngrrmraLjgIJcclxuLy8gcGF0aD1wYXRoIOioreWumuWPr+iugOWPliBjb29raWUg55qE5pyA5LiK5bGk55uu6YyE44CCXHJcbi8vIGRvbWFpbj1kb21haW4g6Kit5a6a5Y+v6K6A5Y+WIGNvb2tpZSDnmoTntrLln5/jgIJcclxuLy8gc2VjdXJl6Kit5a6a5piv5ZCm55So5Yqg5a+G55qE5pa55byP5L6G5YKz6Ly4IGNvb2tpZeOAglxyXG4vLyDmraTlh73lvI/lg4XnlKjkvoboqK3lrppjb29raWXmnInmlYjmnJ/pmZDnlKjvvIzlhbbku5bnmoTlj4PmlbjmspLmnInlgZroqK3lrprjgIJcclxuLy8g5Y+D5pW4IGV4cGlyZSDnlKjkvoboqK3lrpogY29va2llIOeahOacieaViOacn+mZkOOAglxyXG4vLyDmraTlh73lvI/lgYflrpogbmFtZSDkuI3ljIXlkKvliIbomZ/jgIHpgJfomZ/jgIHmiJbnqbrnmb3jgIJcclxuZnVuY3Rpb24gc2V0Q29va2llKG5hbWUsIHZhbHVlLCBleHBpcmUpXHJcbntcclxuXHRkb2N1bWVudC5jb29raWUgPSBuYW1lICsgXCI9XCIgKyB2YWx1ZSArIFwiOyBleHBpcmVzPVwiICsgZXhwaXJlLnRvR01UU3RyaW5nKCk7XHJcbn1cclxuLy/nr6npgbjlh7pjb29raWXlkI3nqLFcclxuZnVuY3Rpb24gZ2V0Q29va2llVmFsdWUoa2V5KSB7XHJcbiAgdmFyIHRoZUNvb2tpZUVudHJ5ID0gZG9jdW1lbnQuY29va2llXHJcbiAgICAuc3BsaXQoJzsnKVxyXG4gICAgLm1hcChmdW5jdGlvbiAoZGF0YSwgaW5kZXgpIHsgcmV0dXJuIGRhdGEuc3BsaXQoJz0nKTsgfSlcclxuICAgIC5maWx0ZXIoZnVuY3Rpb24gKGNvb2tpZUVudHJ5KSB7IHJldHVybiBjb29raWVFbnRyeVswXS50cmltKCkgPT0ga2V5OyB9KVswXTtcclxuICByZXR1cm4gISF0aGVDb29raWVFbnRyeSA/IHRoZUNvb2tpZUVudHJ5WzFdIDogbnVsbDtcclxufVxyXG5cclxuLy8gLy/lkbzlj6tmdW5jdGlvbi3ntrLpoIHovInlhaXlrozmiJDlvoxcclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gIHNlbGVjdFVSTCgpO1xyXG4gIHRvb2xzTGlzdGVuZXIoKTtcclxuICBjcmVhdGVTbGlja3MoKTtcclxuICB0YWJCdG5TbGljaygpO1xyXG4gIHRhYkJ0blNsaWNrRml4ZWQoKTtcclxuICAvL+acg+WToeasiuebiuWwiOeUqFxyXG4gIHRhYkJ0blNsaWNrTWVtYmVycygpO1xyXG4gIHRhYkJ0blNsaWNrRml4ZWRNZW1iZXJzKCk7XHJcbiAgcmlvbmV0U2xpY2soKTtcclxuICB3YXJyYW50eUxpbmsoKTtcclxuICBsYXp5TG9hZCgpO1xyXG4gIGNyZWF0ZUFjY29yZGlvbigpO1xyXG4gIGFvcygpO1xyXG4gIHl0SGFuZGxlcigpO1xyXG4gIHRvZ2dsZXBGaWx0ZXJTdWJtZW51KCk7XHJcbiAgY2xlYXJDaGVja0JveChcIiNqcy1jbGVhckNoZWNrQm94ZXNcIiwgXCJpbnB1dFt0eXBlPSdjaGVja2JveCddXCIpO1xyXG4gIHNldFN0b3JlVGFibGVIZWlnaHRBdE1vYmlsZSgpO1xyXG4gIHRvb2xUaXBzKCk7XHJcbiAgY2FyZEp1c3RpZnlDb250ZW50KCk7XHJcbiAgc3RvcmVGaWx0ZXJOb3RpZmljYXRpb24oXCIudi1kcm9wZG93bi1tZW51XCIsIFwiLnYtZHJvcGRvd24tYnRuXCIpO1xyXG4gIHN0b3JlRmlsdGVyTm90aWZpY2F0aW9uKFwiLnAtcHJvZHVjdHMtc2VhcmNoXCIsIFwiLnAtcHJvZHVjdHMtc2VhcmNoLWJ0blwiKTtcclxuICB0b2dnbGVTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKTtcclxuICBkYXRlTW9iaXNjcm9sbCgpO1xyXG4gIGdvVG9BbmNob3IoKTtcclxuICBhdXRvRml4SGVpZ2h0KFwiLmpzLXRpdGxlRml4ZWRIZWlnaHRcIik7XHJcbn0pO1xyXG53aW5kb3cub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG4gIGNhcmRDb250ZW50RnVuY3Rpb24oKTtcclxuICBzaG93QWxsVGFiKCk7XHJcbn07XHJcbi8v5ZG85Y+rZnVuY3Rpb24t6KaW56qX5aSn5bCP6K6K5pu0XHJcbiQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKCkge1xyXG4gIGNhcmRDb250ZW50RnVuY3Rpb24oKTtcclxuICBzaG93QWxsVGFiKCk7XHJcbiAgY29uZmlybUlmRG91YmxlTWVudU9wZW5lZChcIi5wLXByb2R1Y3RzLXNlYXJjaFwiLCA5OTIpO1xyXG4gIHRhYkJ0blNsaWNrRml4ZWQoKTtcclxuICBhdXRvRml4SGVpZ2h0KFwiLmpzLXRpdGxlRml4ZWRIZWlnaHRcIik7XHJcbn0pO1xyXG4vL+WRvOWPq2Z1bmN0aW9uLeaNsuWLlVxyXG4kKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uICgpIHtcclxuICBnb1RvcFN0aWNreSgpO1xyXG59KTsiXSwic291cmNlUm9vdCI6IiJ9