## 指令集

---

#### `npm i` 或 `npm install`

- 初始化安裝 node_modules 資料夾

#### `gulp`

- 編譯 src@4.0 資料夾，輸出成開發用資料夾 public

### `gulp build --env unzip`

- 編譯 src@4.0 資料夾，輸出正式環境用資料夾 dist，並提供未壓縮的 main.css 和 main.js 供後端閱讀
- bootstrap.min.css 、 main.css 刪除沒用到的 Class Name

### `gulp build --env zip`

- 編譯 src@4.0 資料夾，輸出正式環境用資料夾 dist
- bootstrap.min.css 、 main.css 刪除沒用到的 Class Name，並壓縮
- main.js 壓縮

### `gulp build --env weak`

- 編譯 src@4.0 資料夾，輸出正式環境用資料夾 dist，並且在 HTML 加入弱掃用 sri-hash 碼
- bootstrap.min.css 、 main.css 刪除沒用到的 Class Name，並壓縮
- main.js 壓縮
