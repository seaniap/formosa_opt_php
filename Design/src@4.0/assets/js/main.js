$(window).on('load', function () {
  $('.p-index-loading').fadeOut();
})
// ajax 配合JQ3 引入設置
$(document).ready(function () {
  var el = document.querySelector('html[lang="zh-tw"]');
  if (el) {
    if ($("#header")) {
      $.ajax({
        url: "ajax/_header.html",
        method: "GET",
        dataType: "html",
      }).done(function (data) {
        $("#header").html(data);
        headerFunction();
        goToAnchor();
      });
    }
    if ($("#sideLink")) {
      $.ajax({
        url: "ajax/_sideLink.html",
        method: "GET",
        dataType: "html",
      }).done(function (data) {
        $("#sideLink").html(data);
        toggleSideLink();
      });
    }
    if ($("#footer")) {
      $.ajax({
        url: "ajax/_footer.html",
        method: "GET",
        dataType: "html",
      }).done(function (data) {
        $("#footer").html(data);
        goTop();
        goTopSticky();
        cookieBar();
      });
    }
  }

});
//英文版的ajax
$(document).ready(function () {
  var el = document.querySelector('html[lang="en"]');
  if (el) {
    if ($("#headerEn")) {
      $.ajax({
        url: "../ajax/_header_en.html",
        method: "GET",
        dataType: "html",
      }).done(function (data) {
        $("#headerEn").html(data);
        headerFunction();
        goToAnchor();
      });
    }
    if ($("#sideLinkEn")) {
      $.ajax({
        url: "../ajax/_sideLink_en.html",
        method: "GET",
        dataType: "html",
      }).done(function (data) {
        $("#sideLinkEn").html(data);
        toggleSideLink();
      });
    }
    if ($("#footerEn")) {
      $.ajax({
        url: "../ajax/_footer_en.html",
        method: "GET",
        dataType: "html",
      }).done(function (data) {
        $("#footerEn").html(data);
        goTop();
        goTopSticky();
      });
    }
  }
});

function toolsListener() {
  window.addEventListener("keydown", function (e) {
    if (e.keyCode === 9) {
      document.body.classList.remove("js-useMouse");
      document.body.classList.add("js-useKeyboard");
    }
  });
  window.addEventListener("mousedown", function (e) {
    document.body.classList.remove("js-useKeyboard");
    document.body.classList.add("js-useMouse");
  });
}

function toggleSideLink() {
  var elTw = document.querySelector('html[lang="zh-tw"]');
  var elEn = document.querySelector('html[lang="en"]');
  document.querySelector("#sideLinkToggleBtn").addEventListener("click", function () {
    this.classList.toggle("js-sideLinkOpened");
    if (elTw) {
      document.querySelector("#sideLink").classList.toggle("js-sideLinkOpened");
    }
    if (elEn) {
      document.querySelector("#sideLinkEn").classList.toggle("js-sideLinkOpened");
    }
    document.querySelector("#page").classList.toggle("js-sideLinkOpened");
    document.querySelector("body").classList.toggle("overflow-hidden");
    //打開sideMenu時，把展開的menu關掉
    document.documentElement.classList.remove("js-menuOpened");
    document.querySelector(".l-header-hamburger").classList.remove("js-menuOpened");
    document.querySelector(".l-header-menu").classList.remove("js-menuOpened");
    //打開sideMenu時，把gotop推到旁邊去
    document.querySelector("#sideLinkFooter").classList.toggle("js-sideLinkOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= 1200) {
      document.querySelector("#sideLinkToggleBtn").classList.remove("js-sideLinkOpened");
      if (elTw) {
        document.querySelector("#sideLink").classList.remove("js-sideLinkOpened");
      }
      if (elEn) {
        document.querySelector("#sideLinkEn").classList.remove("js-sideLinkOpened");
      }
      document.querySelector("#page").classList.remove("js-sideLinkOpened");
      document.querySelector("body").classList.remove("overflow-hidden");
      //[移除]打開sideMenu時，把gotop推到旁邊去
      document.querySelector("#sideLinkFooter").classList.remove("js-sideLinkOpened");
    }
  });
}

function goTop() {
  document.querySelector("#goTop").onclick = function (e) {
    e.preventDefault();
    $("html, body").animate({
      scrollTop: 0,
    },
      500
    );
  };
}

function goTopSticky() {
  var el = document.querySelector("#sideLinkFooter");
  //計算footer目前高度
  var footerHeightVar = document.querySelector(".l-footer").clientHeight;
  // console.log(`footerHeightVar + ${footerHeightVar}`);
  //當下網頁高度(整頁長)
  var scrollHeightVar = document.documentElement.scrollHeight;
  // console.log(`scrollHeightVar + ${scrollHeightVar}`);
  //當下畫面高度(window height)
  var windowHeightVar = window.innerHeight;
  // console.log(`windowHeightVar + ${windowHeightVar}`);
  //捲動中的畫面最上端與網頁頂端之間的距離
  var scrollTopVar = document.documentElement.scrollTop;
  // console.log(`scrollTopVar + ${scrollTopVar}`);
  // console.log(windowHeightVar + scrollTopVar + footerHeightVar);
  //如果整頁長==畫面高度+捲動高度
  if (el) {
    if (scrollHeightVar <= windowHeightVar + scrollTopVar + footerHeightVar) {
      el.classList.remove("js-sticky");
    } else {
      el.classList.add("js-sticky");
    }
  }
}

function goToAnchor() {
  var headerHeight = "";
  window.addEventListener("resize", function () {
    return headerHeight = $(".l-header").height();
  });
  $('.js-goToAnchor').click(function (e) {
    e.preventDefault();
    var target = $(this).attr('href');
    var targetPos = $(target).offset().top;
    var headerHeight = $(".l-header").height();
    $('html,body').animate({
      scrollTop: targetPos - headerHeight
    }, 1000);
    var trigger02 = document.querySelector("#hamburger");
    var target02 = document.querySelector("#menu");
    trigger02.classList.remove("js-menuOpened");
    target02.classList.remove("js-menuOpened");
    document.documentElement.classList.remove("js-menuOpened");
  });
}
//風琴用function
function Accordion(el, target, siblings) {
  this.el = el;
  this.target = target;
  this.siblings = siblings;
}
Accordion.prototype.init = function () {
  var triggers = document.querySelectorAll(this.el);
  var target = this.target;
  var siblings = this.siblings;
  Array.prototype.slice.call(triggers).forEach(function (trigger) {
    trigger.addEventListener("click", function () {
      event.preventDefault();
      if (siblings == true) {
        if (this.querySelector(target) !== null) {
          // this.querySelector(target).classList.toggle(
          //   "js-accordionExpended"
          // );
          //為了要能在打開一個時，其他的收合，這邊改用jQuery==>
          $(this).find(target).toggleClass("js-accordionExpended");
          // 增加藍線框效果
          $(this).toggleClass("c-accordion-btn-focus");
          $(this).siblings().find(target).removeClass("js-accordionExpended");
          // 增加藍線框效果
          $(this).siblings().removeClass("c-accordion-btn-focus");
        }
        // document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        //為了要能在打開一個時，其他的收合，這邊改用jQuery==>
        var att = $(trigger).attr("data-target");
        $(att).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended");
        // $(`${trigger.getAttribute("data-target")}`).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended");
        //用了jQuery之後，這組會被樓上那個綁在一起，所以可以省略
        // trigger.classList.toggle("js-accordionExpended");
      } else {
        if (this.querySelector(target) !== null) {
          this.querySelector(target).classList.toggle(
            "js-accordionExpended"
          );
        }
        document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        trigger.classList.toggle("js-accordionExpended");
      }
    });
  });
}

function nodeListToArray(nodeListCollection) {
  return Array.prototype.slice.call(nodeListCollection);
}
// const nodeListToArray = (nodeListCollection) => Array.prototype.slice.call(nodeListCollection);
/**
 * 
 * @param {*} el :class name
 * @param {*} target :被影響到的目標
 * @param {*} mediaQuery :斷點設定
 * @說明 "el"與"target" toggle 一個叫js-active的class要用出什麼效果，端看你怎麼寫js-active的css效果展開或收合什麼東西
 */

function toggleVisiable(el, target, mediaQuery) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);
  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        this.classList.toggle("js-active");
        target.classList.toggle("js-active");
        var hasMediaQuery = mediaQuery;
        if (hasMediaQuery !== "") {
          var isMobile = window.innerWidth < mediaQuery;
          if (isMobile) {
            document.documentElement.classList.toggle("js-functionMenuOpened");
          }
        } else {
          document.documentElement.classList.remove("js-functionMenuOpened");
        }
        window.addEventListener("resize", function () {
          if (window.innerWidth >= mediaQuery) {
            document.documentElement.classList.remove("js-functionMenuOpened");
          }
        });
      });
    });
  }
}
/*el=觸發對象(開關)*/
/*target=被控制的物件(燈)*/
function clickConfirm(el, target) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);
  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        target.classList.remove("js-active");
        document.documentElement.classList.remove("js-functionMenuOpened");
      });
    });
  }
}
//↑↑↑通用function---------------------
function createAccordion() {
  if (document.querySelector('.c-accordion-btn')) {
    new Accordion(".c-accordion-btn", ".c-accordion-btn-icon", true).init();
  }
}
// TODO: 想辦法這個全域變數要處理一下
var str = 0;

function showAllTab() {
  if (window.innerWidth > 992 && str == 1) {
    $(".tab-pane").removeClass("show active").first().addClass("show active");
    $(".c-tab-linkE").removeClass("active").parent().first().find(".c-tab-linkE").addClass("active");
    str = 0;
  }
  if (window.innerWidth < 992 && str == 0) {
    $(".tab-pane").addClass("show active");
    str = 1;
  }
  // console.log(str);
}

function togglepFilterSubmenu() {
  // TODO: 有空試試看寫一個手機版可以針對內容高度增加showmore，並且記錄此showmore已經點開過，不會因為rwd就又計算一次
  //手機版顯示全部
  if (document.querySelector('.p-products-search-showMore')) {
    new Accordion(".p-products-search-showMore", undefined, false).init();
  }
  var triggers = document.querySelectorAll(".c-accordion-btn");
  nodeListToArray(triggers).forEach(function (trigger) {
    var el = document.querySelector(trigger.getAttribute("data-target"));
    trigger.addEventListener("click", function () {
      if (el.querySelector(".p-products-search-showMore")) {
        el.querySelector(".p-products-search-showMore").classList.remove("js-accordionExpended");
        el.querySelector(".c-accordion-content-layout").classList.remove("js-accordionExpended");
      }
    });
  });
  //手機版開關選單
  //商品專區
  toggleVisiable(".close", ".p-products-search", "");
  toggleVisiable(".p-products-search-btn", ".p-products-search", 992);
  clickConfirm("#js-confirm", ".p-products-search");
  //門市查詢
  toggleVisiable(".v-dropdown-btn", ".v-dropdown-menu", 992);
  toggleVisiable(".close", ".v-dropdown-menu", "");
  clickConfirm("#js-confirm", ".v-dropdown-menu");
  // if ($(".v-dropdown-btn").hasClass("js-active")) {
  //   document.querySelector("body").addEventListener("click", function () {
  //     document.querySelector(".v-dropdown-btn").classList.remove("js-active");
  //     document.querySelector(".v-dropdown-menu").classList.remove("js-active");
  //   });
  // }

}

function confirmIfDoubleMenuOpened(el, mediaQuery) {
  if (window.innerWidth < mediaQuery && $(el).hasClass("js-active") && document.documentElement.classList == "") {
    document.documentElement.classList.add("js-menuOpened");
  }
}
// 輪播相關--------------------------
function Slick(el, slidesToShow, slidesPerRow, rows, responsive) {
  this.el = el;
  this.slidesToShow = slidesToShow;
  this.slidesPerRow = slidesPerRow;
  this.rows = rows;
  this.responsive = responsive;
};

Slick.prototype.init = function () {
  $(this.el + " .v-slick").slick({
    arrows: true,
    slidesToShow: this.slidesToShow,
    slidesPerRow: this.slidesPerRow,
    rows: this.rows,
    prevArrow: `<button type="button" class="v-slick-arrows-prev"><i class="fal fa-angle-left"></i></button>`,
    nextArrow: `<button type="button" class="v-slick-arrows-next"><i class="fal fa-angle-right"></i></button>`,
    // autoplay: true,
    // lazyLoad: "progressive",
    responsive: this.responsive,
  });
};

function setStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;
    if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
      trigger.style.height = `${totalNum}px`;
    } else if (window.innerWidth > 1200) {
      trigger.style.height = "";
      trigger.classList.remove('js-active');
      trigger.querySelector("i").classList.remove("js-submenuOpened");
    }
    window.addEventListener("resize", function () {
      if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
        trigger.style.height = `${totalNum}px`;
      } else if (window.innerWidth > 1200) {
        trigger.style.height = "";
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      }
    });
  });
}

function toggleStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;
    trigger.querySelector("td:first-child").addEventListener('click', function () {
      if (window.innerWidth < 1200 && trigger.style.height == "") {
        trigger.style.height = `${totalNum}px`;
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      } else {
        trigger.style.height = "";
        trigger.classList.add("js-active");
        trigger.querySelector("i").classList.add("js-submenuOpened");
      }
    });
  });
}

// TODO: 有空看一下為什麼new第三個就出事了
function createSlicks() {
  if (document.querySelector(".v-slick")) {
    var targets = ["#news", "#recommandation"];
    targets.forEach(function (target) {
      new Slick(target, 4, 1, 1, [{
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false,
        }
      },
      ]).init();
    });
    new Slick("#video", 1, 2, 2, [{
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1,
        arrows: false,
      }
    },
    ]).init();
  }
}

function rionetSlick() {
  var el = ".v-rionetSlick";
  if (document.querySelector(el)) {
    $(el).slick({
      // centerMode: true,
      // centerPadding: '60px',
      slidesToShow: 4,
      arrows: true,
      prevArrow: `<button type="button" class="v-tabBtnSlick-prev"><i class="fal fa-angle-left"></i></button>`,
      nextArrow: `<button type="button" class="v-tabBtnSlick-next"><i class="fal fa-angle-right"></i></button>`,
      lazyLoad: "progressive",
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          dots: true,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false,
          // variableWidth: true,
          dots: true,
        }
      },
      ],
    });
    //   $('.v-tabBtnSlick-prev').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickPrev');
    //  });
    //   $('.v-tabBtnSlick-next').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickNext');
    //  });
  }
}

function tabBtnSlick() {
  var el = ".v-tabBtnSlick";
  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length;
          //如果總數大於６就用６， ２０２２０２１５客戶要求多一個....
          //var slidesToShowNum = tabLength > 6 ? 6 : tabLength;
          var slidesToShowNum = tabLength;
    $(el).slick({
      slidesToShow: slidesToShowNum,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: `<button type="button" class="v-tabBtnSlick-prev"><i class="fal fa-angle-left"></i></button>`,
      nextArrow: `<button type="button" class="v-tabBtnSlick-next"><i class="fal fa-angle-right"></i></button>`,
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          variableWidth: false,
          // slidesToScroll: 5,
          // infinite: true,
          // centerMode: false,
        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          variableWidth: false,
          // infinite: true,
          // centerMode: false,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          // infinite: false,
          arrows: false,
        }
      },
      ],
    });
  }
}

function tabBtnSlickFixed() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length; // var slidesToShowNum 
    //原本是小於６就加上致中， ２０２２０２１５客戶要求多一個....
    if (window.innerWidth >= 1200 && tabLength <= 7) {
      $(".v-tabBtnSlick .slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    }

    if (window.innerWidth >= 768 && window.innerWidth < 1200 && tabLength < 5) {
      $(".v-tabBtnSlick .slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    } // if (window.innerWidth < 768) {
    // document.querySelector('.slick-track').classList.remove('text-md-center');
    // }
  }
}

function tabBtnSlickMembers() {
  var el = ".v-tabBtnSlick-members";
  if (document.querySelector(el)) {
    $(el).slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: `<button type="button" class="v-tabBtnSlick-prev"><i class="fal fa-angle-left"></i></button>`,
      nextArrow: `<button type="button" class="v-tabBtnSlick-next"><i class="fal fa-angle-right"></i></button>`,
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
          variableWidth: false,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          arrows: false,
        }
      },
      ],
    });
  }
}

function tabBtnSlickFixedMembers() {
  var el = ".v-tabBtnSlick-members";
  if (document.querySelector(el)) {
    if (window.innerWidth >= 768) {
      $(".v-tabBtnSlick-members .slick-track").addClass("text-md-center");
    }
  }
}
// End 輪播相關--------------------------
function warrantyLink() {
  if (document.querySelectorAll(".p-index-warranty-link").length) {
    var triggers = document.querySelectorAll(".p-index-warranty-link");
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".c-btn").classList.add("js-btnHover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".c-btn").classList.remove("js-btnHover");
      });
    });
  }
}
//手機版漢堡選單
function toggleMobileMenu(mediaQuery) {
  var trigger = document.querySelector("#hamburger");
  var target = document.querySelector("#menu");

  trigger.addEventListener("click", function () {
    this.classList.toggle("js-menuOpened");
    target.classList.toggle("js-menuOpened");
    document.documentElement.classList.toggle("js-menuOpened");
  });

  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      trigger.classList.remove("js-menuOpened");
      target.classList.remove("js-menuOpened");
      document.documentElement.classList.remove("js-menuOpened");
    }
  });
}
//手機版風琴折疊選單
function toggleMobileSubmenu(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-link");
  var targets = document.querySelectorAll(".l-header-submenu--hidden");
  if(triggers !== null){
    nodeListToArray(triggers).forEach(function (trigger) {
      if (trigger.nextElementSibling !== null) {
        trigger.addEventListener("click", function (e) {
          e.preventDefault();
          var target = trigger.nextElementSibling;
          if (window.innerWidth < mediaQuery) {
            target.classList.toggle("js-submenuOpened");
            trigger.querySelector("i").classList.toggle("js-submenuOpened");
            //為了開一個關一個，用jQuery加寫
            $(target).parents().siblings().find(".l-header-submenu , i").removeClass("js-submenuOpened");
          }
        });
      }
    });
  }


  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      nodeListToArray(triggers).forEach(function (trigger) {
        var target = trigger.nextElementSibling;
        target.classList.remove("js-submenuOpened");
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      });
    }
  });
}

function sibingMobileSubmenu() {
  $(".l-header-menu-item").on("click", function () {
    $(this).siblings().find(".l-header-submenu").removeClass("js-submenuOpened");
  });
}

function togglePcHoverState(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-item");
  nodeListToArray(triggers).forEach(function (trigger) {
    if (trigger.querySelector(".l-header-submenu") !== null) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".l-header-menu-link").classList.add("js-hover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger
          .querySelector(".l-header-menu-link")
          .classList.remove("js-hover");
      });
    }
  });

  window.addEventListener("resize", function () {
    if (window.innerWidth < mediaQuery) {
      Array.prototype.slice
        .call(document.querySelectorAll(".l-header-menu-link"))
        .forEach(function (item) {
          item.classList.remove("js-hover");
        });
    }
  });
}

function headerFunction() {
  var breakpoint = 1200;
  toggleMobileMenu(breakpoint);
  toggleMobileSubmenu(breakpoint);
  togglePcHoverState(breakpoint);
}

function lazyLoad() {
  if (document.querySelector("img[data-src]")) {
    var observer = lozad();
    observer.observe();
    if (
      -1 !== navigator.userAgent.indexOf("MSIE") ||
      navigator.appVersion.indexOf("Trident/") > 0
    ) {
      var iframe = document.querySelector("iframe");
      iframe.setAttribute("src", "https://www.youtube.com/embed/O7pNpR3Py68");
    }
  }
}
//計算「最新消息」、「人氣推薦」這類的卡片列表的標題字數，讓大家的高度一樣
function autoFixHeight(con) {
  var el = document.querySelectorAll(con);
  var thisHeight = -1;
  var maxHeight = -1;
  var breakpoint = 768;
  //為了ie不支援nodelist的forEach修正
  nodeListToArray(el).forEach(function (item) {
    item.style.height = ""; //清空之前的style
    thisHeight = item.clientHeight; //取得已經減過高度的
    maxHeight = maxHeight > thisHeight ? maxHeight : thisHeight;
  });
  if (document.body.clientWidth > breakpoint) {
    nodeListToArray(el).forEach(function (item) {
      item.style.height = `${maxHeight}px`;
    });
  }
}

function textHide(num, con) {
  var el = document.querySelectorAll(con);
  //ie才執行這個超過字數增加刪節號，其他瀏覽器靠css語法即可
  // if (
  //   navigator.userAgent.indexOf("MSIE") !== -1 ||
  //   navigator.appVersion.indexOf("Trident/") > 0
  // ) {
  //   Array.prototype.slice.call(el).forEach(function (item) {
  //     var txt = item.innerText;
  //     if (txt.length > num) {
  //       txtContent = txt.substring(0, num - 1) + "...";
  //       item.innerHTML = txtContent;
  //     }
  //   });
  // }
  nodeListToArray(el).forEach(function (item) {
    var txt = item.innerText;
    if (txt.length > num) {
      txtContent = txt.substring(0, num - 1) + "...";
      item.innerHTML = txtContent;
    } else {
      txtContent = txt.substring(0, num) + "...";
      item.innerHTML = txtContent;
    }
  });
  autoFixHeight(con);
}

function clearCheckBox(el, target) {
  if (document.querySelector(el)) {
    var trigger = document.querySelector(el);
    var targets = document.querySelectorAll(target);
    trigger.addEventListener("click", function () {
      event.preventDefault();
      trigger.blur();
      Array.prototype.slice.call(targets).forEach(function (trigger) {
        trigger.checked = false;
      });
    });
  }
}

function cardContentFunction() {
  if (document.querySelector(".c-card-body-title") !== null) {
    autoFixHeight(".c-card-body-title");
  }
  //人氣推薦內文字數限制
  if (document.querySelector(".c-card-body-txt") !== null) {
    textHide(48, ".c-card-body-txt");
  }
}

function toolTips() {
  $('[data-toggle="tooltip"]').tooltip({
    placement: 'right',
    trigger: 'hover',
    offset: 30,
  });
  $('[data-toggle="tooltip"]').on('inserted.bs.tooltip', function () {
    var getTootipsId = "#" + $(this).attr("aria-describedby");
    // console.log(getTootipsId);
    // console.log($(this).attr("data-original-title") == "德國蔡司數位影像系統");
    if ($(this).attr("data-original-title") == "德國蔡司數位影像系統") {
      $(getTootipsId).find('.tooltip-inner').html("德國蔡司<br>數位影像系統");
    }
    if ($(this).attr("data-original-title") == "法國依視路全方位視覺檢測系統") {
      $(getTootipsId).find('.tooltip-inner').html("法國依視路<br>全方位視覺檢測系統");
    }
  })
}

function aos() {
  if (document.querySelector("[data-aos]")) {
    AOS.init({
      once: true,
    });
  }
}

// function ytHandler() {
//   if (document.querySelector("[data-url]")) {
//     var triggers = document.querySelectorAll("[data-url]");
//     var target = document.querySelector("iframe");
//     var index = 0;
//     Array.prototype.slice.call(triggers).forEach(function (trigger) {
//       trigger.setAttribute("index", index++);

//       trigger.onclick = function () {
//         var triggerIndex = trigger.getAttribute("index");

//         $('#slick_ad').slick('slickGoTo', triggerIndex);

//         target.setAttribute("src", trigger.getAttribute("data-url"));
//         //alert(+trigger.getAttribute("data-url"));
//         trigger.classList.add("js-active");
//         Array.prototype.slice.call(triggers).filter(function (item) {
//           return item !== trigger;
//         }).forEach(function (item) {
//           item.classList.remove("js-active");
//         });
//       };
//     });
//   }
// }
//sam改寫過的↓↓↓↓↓↓
function ytHandler() {
  if (document.querySelector("[data-url]")) {
    var triggers = document.querySelectorAll("[data-url]");
    var target = document.querySelector("iframe");

    //對應的ad key 可以自行塞入你要的key
    var index = 0;

    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      //塞入對應的AD key值
      trigger.setAttribute("index", index++);

      //ad被點擊事件
      trigger.onclick = function () {

        /*=====此處 為抓DB AD廣告圖片資料=====
        var triggerIndex = trigger.getAttribute("index"); 抓取ad key data
            $.get("url",{}, function(result){
          $("#slick_ad").html("請塞入廣告圖片html資料");
            $('#slick_ad').slick('slickGoTo', 0);
            })
        ========================*/


        var triggerIndex = trigger.getAttribute("index");
        $('#slick_ad').slick('slickGoTo', triggerIndex);

        target.setAttribute("src", trigger.getAttribute("data-url"));
        //alert(+trigger.getAttribute("data-url"));
        trigger.classList.add("js-active");
        Array.prototype.slice.call(triggers).filter(function (item) {
          return item !== trigger;
        }).forEach(function (item) {
          item.classList.remove("js-active");
        });
      };
    });
  }
}

function cardJustifyContent() {
  var el = $(".js-cardJustifyContent");
  if (el) {
    for (var i = 0; i < el.length; i++) {
      var elChildrenLength = el.eq(i).children('div').length;
      // console.log(elChildrenLength);
      if (elChildrenLength <= 2) {
        el.eq(i).addClass("justify-content-center");
      } else {
        el.eq(i).removeClass("justify-content-center");
      }
    }
  }
}

function storeFilterNotification(inputContainer, targetEl) {
  var el = document.querySelector(inputContainer);
  var target = document.querySelector(targetEl);
  if (el) {
    // console.log(inputContainer + " + " + target);
    var triggers = el.querySelectorAll("input[type='checkbox']");
    // console.log(triggers);
    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        var checkedNum = el.querySelectorAll("input[type=checkbox]:checked").length;
        // console.log(checkedNum);
        if (checkedNum > 0) {
          target.classList.add("js-inputChecked");
        } else {
          target.classList.remove("js-inputChecked");
        }
      });
    });
    var clearAllBtnEl = document.querySelector("#js-clearCheckBoxes");
    clearAllBtnEl.addEventListener("click", function () {
      target.classList.remove("js-inputChecked");
    });
  }
}
//滾軸式日期選單
function dateMobiscroll() {
  var el = document.querySelector("#date")
  if ($(el)) {
    $(el).mobiscroll().date({
      theme: $.mobiscroll.defaults.theme, // Specify theme like: theme: 'ios' or omit setting to use default 
      mode: 'mixed', // Specify scroller mode like: mode: 'mixed' or omit setting to use default 
      display: 'modal', // Specify display mode like: display: 'bottom' or omit setting to use default 
      lang: 'zh' // Specify language like: lang: 'pl' or omit setting to use default 
    });
  }

}
//下拉選單跳轉超連結
function selectURL() {
  var el = document.querySelector(".js-selectURL");
  if (el) {
    el.addEventListener("change", function () {
      // window.open(this.options[this.selectedIndex].value);
      location.href = this.options[this.selectedIndex].value;
    });
  }
}

function cookieBar() {
  if (!getCookieValue('cookieConsentRead')) {
    $(".l-footer-cookie-bar").removeClass('d-none');
    $(".l-footer-cookie-btn").click(function () {
      $(".l-footer-cookie-bar").addClass('d-none');
      var expires = new Date();
      //放30年才消失，應該夠久了
      expires.setFullYear(expires.getFullYear() + 30);
      setCookie("cookieConsentRead", "true", expires);
    });
  }
  else {
    $(".l-footer-cookie-bar").addClass('d-none');
  }
}
// cookie 基本格式為 name=value [;expires=date] [;path=path] [;domain=domain] [;secure]
// expires=date 設定 cookie 的有效期限至 date 為止。
// path=path 設定可讀取 cookie 的最上層目錄。
// domain=domain 設定可讀取 cookie 的網域。
// secure設定是否用加密的方式來傳輸 cookie。
// 此函式僅用來設定cookie有效期限用，其他的參數沒有做設定。
// 參數 expire 用來設定 cookie 的有效期限。
// 此函式假定 name 不包含分號、逗號、或空白。
function setCookie(name, value, expire)
{
	document.cookie = name + "=" + value + "; expires=" + expire.toGMTString();
}
//篩選出cookie名稱
function getCookieValue(key) {
  var theCookieEntry = document.cookie
    .split(';')
    .map(function (data, index) { return data.split('='); })
    .filter(function (cookieEntry) { return cookieEntry[0].trim() == key; })[0];
  return !!theCookieEntry ? theCookieEntry[1] : null;
}

// //呼叫function-網頁載入完成後
$(document).ready(function () {
  selectURL();
  toolsListener();
  createSlicks();
  tabBtnSlick();
  tabBtnSlickFixed();
  //會員權益專用
  tabBtnSlickMembers();
  tabBtnSlickFixedMembers();
  rionetSlick();
  warrantyLink();
  lazyLoad();
  createAccordion();
  aos();
  ytHandler();
  togglepFilterSubmenu();
  clearCheckBox("#js-clearCheckBoxes", "input[type='checkbox']");
  setStoreTableHeightAtMobile();
  toolTips();
  cardJustifyContent();
  storeFilterNotification(".v-dropdown-menu", ".v-dropdown-btn");
  storeFilterNotification(".p-products-search", ".p-products-search-btn");
  toggleStoreTableHeightAtMobile();
  dateMobiscroll();
  goToAnchor();
  autoFixHeight(".js-titleFixedHeight");
});
window.onload = function () {
  cardContentFunction();
  showAllTab();
};
//呼叫function-視窗大小變更
$(window).resize(function () {
  cardContentFunction();
  showAllTab();
  confirmIfDoubleMenuOpened(".p-products-search", 992);
  tabBtnSlickFixed();
  autoFixHeight(".js-titleFixedHeight");
});
//呼叫function-捲動
$(window).scroll(function () {
  goTopSticky();
});