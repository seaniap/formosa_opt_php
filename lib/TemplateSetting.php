<?php require_once('../Connections/MySQL.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE AI2_SiteMap SET Template=%s , Extend=%s WHERE Page=%s",
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['Extend'], "text"),
                       GetSQLValueString($_POST['Page'], "text"));

  
  $Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));

  $updateGoTo = "../index.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
 // header(sprintf("Location: %s", $updateGoTo));
}

$colname_SiteMap = "-1";
if (isset($_GET['Page'])) {
  $colname_SiteMap = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$query_SiteMap = sprintf("SELECT * FROM %s_SiteMap WHERE Page = '%s'", $Table_ID, $colname_SiteMap);
$SiteMap = mysqli_query($MySQL,$query_SiteMap) or die(mysqli_error($MySQL));
$row_SiteMap = mysqli_fetch_assoc($SiteMap);
$totalRows_SiteMap = mysqli_num_rows($SiteMap);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>版型設定</title>
<?php require_once('MTemplateJava.php'); ?>
</head>

<body>
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
    <tr valign="baseline">
      <td nowrap align="right">Page:</td>
      <td><?php echo $row_SiteMap['Page']; ?></td>
    </tr>
        <tr valign="baseline">
      <td nowrap align="right">Template:</td>
      <td><input type="text" name="Template" value="<?php echo $row_SiteMap['Template']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">使用子表單:</td>
      <td><input type="text" name="Extend" value="<?php echo $row_SiteMap['Extend']; ?>" size="32"></td>
    </tr>
    
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="更新記錄"></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="Page" value="<?php echo $row_SiteMap['Page']; ?>">
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysqli_free_result($SiteMap);
?>
