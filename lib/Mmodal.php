<!-- Modal -->    
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">新增</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">  
          <div class="container-fluid">
          <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="MSuper.php<?php echo "?Action=ins&Template=" . $Template . "&Page=" . $row_data['Page'] . "&Sq=" . $row_data['Sq'] . "&KeyID=" . $row_data['KeyID'] ?>" frameborder="0"></iframe>
          </div>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    </div>