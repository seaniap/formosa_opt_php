<!-- 標準 script  -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script> -->
<script type="text/JavaScript" src="js/std.js"></script>
<!-- 標準 script End -->
<!--   Google 點擊統計  -->
<?php if ($GoogleAnalytics <> "") { ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $GoogleAnalytics; ?>"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', '<?php echo $GoogleAnalytics; ?>');
  </script>

<?php } ?>
<!--   Google 點擊統計 end  -->
<?php function SortingLink($Name)  //加入排序條件的連結
{ ?>
  <a href="index.php?Page=<?php echo $_GET['Page'] . "&Sort=$Name ASC&Cate01=" . $_GET['Cate01'] . "&Cate02=" . $_GET['Cate02'] . "&Cate03=" . $_GET['Cate03'] ?>">上 </a>

  <a href="index.php?Page=<?php echo $_GET['Page'] . "&Sort=$Name DESC&Cate01=" . $_GET['Cate01'] . "&Cate02=" . $_GET['Cate02'] . "&Cate03=" . $_GET['Cate03'] ?>">下 </a>
  <?php }


//function Category1Filter()

function Template_Button_3($row_data, $Template, $Security)
{
  $Security = "-" . $Security . "-";
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->

    <input name="add" type="button" class="f" onClick="MM_openBrWindow('M<?php echo $Template . "?Action=ins&Template=" . $Template . "&Page=" . $row_data['Page'] . "&Sq=" . $row_data['Sq'] . "&KeyID=" . $row_data['KeyID'] ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="新增">
    <input name="var" type="button" class="f" id="var" onClick="MM_openBrWindow('M<?php echo $Template . "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="編輯">
    <input name="del" type="button" class="f" id="del" onClick="DelConfirmWindow('DelBigTable.php?Page=<?php echo $_GET['Page']; ?>&KeyID=<?php echo $row_data['KeyID'] ?>')" value="刪除">
  <?php }
}


function SuperM_Button_3($row_data, $Template, $Security)
{
  //echo "SuperM_Button_3".$Security;
  //echo "SuperM_Button_3".$Security;
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->

    <div class="col-12 my-1">
      
<?php if($Template!="news01.php" and $Template!="recommended01.php"){  ?>      
      <input name="add" type="button" class="btn btn-outline-secondary" onClick="window.open('MSuper.php<?php echo "?Action=ins&Template=" . $Template . "&Page=" . $row_data['Page'] . "&Sq=" . $row_data['Sq'] . "&KeyID=" . $row_data['KeyID'] ?>','_blank')" value="新增">
      
<?php } ?>      
      
      <input name="var" type="button" class="btn btn-outline-secondary" id="var" onClick="window.open('MSuper.php<?php echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>', '_blank')" value="編輯">
      <input name="del" type="button" class="btn btn-outline-danger" id="del" onClick="DelConfirmWindow('DelBigTable.php?Page=<?php echo $_GET['Page']; ?>&KeyID=<?php echo $row_data['KeyID'] ?>&album02=<?php echo $row_data['l06'] ?>')" value="刪除">
    </div>
    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
      新增A
    </button> -->
    <?php //include('Mmodal.php'); ?>

  <?php }
}


function SurveyM_Button_3($row_data, $Template, $Security)
{
  
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->

    <input name="add" type="button" class="f" onClick="MM_openBrWindow('index.php<?php echo "?Action=ins&Template=" . $Template . "&Page=S-2" ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="新增">
    <input name="var" type="button" class="f" id="var" onClick="MM_openBrWindow('MSurvey.php<?php echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="編輯">
    <input name="del" type="button" class="f" id="del" onClick="DelConfirmWindow('DelSurvey.php?Page=<?php echo $_GET['Page']; ?>&KeyID=<?php echo $row_data['KeyID']; ?>')" value="刪除">
  <?php }
}






function SuperM_Button_1($row_data, $Template, $Security)
{
  //echo "SuperM_Button_3".$Security;
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->

    <input name="del" type="button" class="f" id="del" onClick="DelConfirmWindow('DelBigTable.php?Page=<?php echo $_GET['Page']; ?>&KeyID=<?php echo $row_data['KeyID'] ?>')" value="刪除">
  <?php }
}



function SuperM_ins($row_data, $Template, $Security)
{
  //echo "SuperM_ins: ".$Security;
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if ($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) { ?>
    <!--3botton   -->
    <input name="add" type="button" class="btn btn-outline-secondary" onClick="window.open('MSuper.php<?php echo "?Action=ins&Template=" . $Template . "&Page=" . $row_data['Page'] . "&Sq=" . $row_data['Sq'] . "&KeyID=" . $row_data['KeyID'] ?>','_blank')" value="新增">
    
  <?php }
}

function SurveyM_ins($row_data, $Template, $Security)
{
  //	echo "SurveyM_ins".$Security;
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if ($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) {
  ?>
    <!--3botton   -->

    <input name="add" type="button" class="f" onClick="MM_openBrWindow('index.php<?php echo "?Action=ins&Template=" . $Template . "&Page=S-2"; ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="新增">
  <?php }
}

function ShowSqM($row_data, $Template, $Security)
{
  //echo "ShowSqM".$Security;
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if ($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) {
  ?>
    <!--3botton   -->

    <div class="alert alert-secondary" style="margin-bottom:0">順序編號：<?php echo $row_data['Sq'];  ?></div>
    <?php }
}






//發表新文章按鈕
function Post_ins($row_data, $Template, $Login)
{
  if ($Login == 'Y') { // 需檢查權限
    if ($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) { ?>
      <input name="add" type="button" class="f" onClick="MM_openBrWindow('MPost.php<?php echo "?Action=ins&Template=" . $Template . "&Page=" . $row_data['Page'] . "&Sq=" . $row_data['Sq'] . "&KeyID=" . $row_data['KeyID'] . "&ProductID=1"; ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="發表新文章">
    <?php }
  } else { //不檢查權限    
    ?>
    <!--3botton   -->
    <input name="add" type="button" class="f" onClick="MM_openBrWindow('MPost.php<?php echo "?Action=ins&Template=" . $Template . "&Page=" . $row_data['Page'] . "&Sq=" . $row_data['Sq'] . "&KeyID=" . $row_data['KeyID'] . "&ProductID=1"; ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="發表新文章">
    <?php }
}

//回覆文章按鈕
function Post_insR($row_data, $Template, $Login)
{
  if ($Login == 'Y') { // 需檢查權限
    if ($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) { ?>
      <input name="add" type="button" class="f" onClick="MM_openBrWindow('MPost.php<?php echo "?Action=ins&Template=" . $Template . "&Page=" . $row_data['Page'] . "&Sq=" . $row_data['Sq'] . "&KeyID=" . $row_data['KeyID'] . "&ProductID=2"; ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="回應">
    <?php }
  } else { //不檢查權限    
    ?>
    <!--3botton   -->
    <input name="add" type="button" class="f" onClick="MM_openBrWindow('MPost.php<?php echo "?Action=ins&Template=" . $Template . "&Page=" . $row_data['Page'] . "&Sq=" . $row_data['Sq'] . "&KeyID=" . $row_data['KeyID'] . "&ProductID=2"; ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="回應">
  <?php }
}




function MPost_edit($row_data, $Template, $Security)
{
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }

  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->

    <input name="var" type="button" class="f" id="var" onClick="MM_openBrWindow('MPost.php<?php echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="編輯">

  <?php }
}

//Edit Videos
function EditVideosM_ins($row_data, $Template, $Security)
{ 
  //編輯首頁精選影片 page=0
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-"))) { ?>
    <!--3botton   -->
<!--
    <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#featuredcideos01Modal">
    編輯精選影片
    </button>
    <?php //include("featuredcideos01modal.php"); ?> 
-->

<a  class="btn btn-outline-secondary"  href="index.php?Page=0-2"  target="_blank"> 編輯精選影片</a>






  <?php }
}

//Edit
function EditThisPageM_ins($row_data, $Template, $Security)
{ 
  //編輯首頁商品推薦 page=0
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-"))) { ?>
    <!--3botton   -->

    <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#exampleModal">
    編輯商品推薦
    </button>
    <?php include("suggest01modal.php"); ?> 
  <?php }
}

function EditThisPageM_edit($row_data, $Template, $Security)
{
   //編輯下拉選單 page=4
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->

    <!-- <input name="var" type="button" class="btn btn-outline-secondary" id="var" 
    onClick="MM_openBrWindow('MSuper.php<?php //echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" 
    value="編輯下拉選單"> -->

    <?php //echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>

    <!-- <input name="var" type="button" class="btn btn-outline-secondary" id="var" 
    onClick="MM_openBrWindow('index.php?Page=4-1','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" 
    value="編輯下拉選單"> -->

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#exampleModal">
    編輯下拉選單modal
    </button>


    <?php include("news41modal.php"); ?>


    


  <?php }
}
// end edit
function SelectOptionsM_ins($row_data, $Template, $Security)
{ 
  //輸入下拉選單 page=4
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-"))) { ?>
    <!--3botton   -->

 <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#exampleModal">
    編輯下拉選單modal
    </button>


    <?php include("news41modal.php"); ?> 
  



  <!--  <input name="var" type="button" class="btn btn-outline-secondary" id="var" onClick="MM_openBrWindow('MSuper.php<?php //echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" value="新增下拉選單"> -->

  <?php }
}

function SelectOptionsM_edit($row_data, $Template, $Security)
{
   //編輯下拉選單 page=4
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->

    <!-- <input name="var" type="button" class="btn btn-outline-secondary" id="var" 
    onClick="MM_openBrWindow('MSuper.php<?php //echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" 
    value="編輯下拉選單"> -->

    <?php //echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>

    <!-- <input name="var" type="button" class="btn btn-outline-secondary" id="var" 
    onClick="MM_openBrWindow('index.php?Page=4-1','MTB','scrollbars=yes,resizable=yes,width=680,height=650')" 
    value="編輯下拉選單"> -->

    <!-- Button trigger modal -->
  <!--  
    <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#exampleModal">
    編輯下拉選單modal
    </button>
-->
<button type="button" class="btn btn-outline-secondary" onClick="window.open('index.php?Page=4-1');"   >
    編輯下拉選單modal
    </button>


    <?php //include("news41modal.php"); ?>


    


  <?php }
}



// Edit
function SuperM_edit($row_data, $Template, $Security)
{
  if ($Security <> "") {
    $Security = "-" . $Security . "-";
  }
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->
    <input name="var" type="button" class="btn btn-outline-secondary btn-block" id="var" onClick="window.open('MSuper.php<?php echo "?Action=edit&Template=" . $Template . "&Page=" . $row_data['Page'] . "&KeyID=" . $row_data['KeyID'] ?>','_blank')" value="編輯">
  <?php }
}
function SuperM_del($row_data, $Template, $Security)
{
  $Security = "-" . $Security . "-";
  if (($_SESSION['Mode'] == "Admin" or strchr($Security, "-" . $_SESSION['UserName'] . "-")) and $row_data['KeyID'] != "") { ?>
    <!--3botton   -->
    <input name="del" type="button" class="btn btn-outline-secondary btn-block" id="del" onClick="DelConfirmWindow('DelBigTable.php?Page=<?php echo $_GET['Page']; ?>&KeyID=<?php echo $row_data['KeyID'] ?>')" value="刪除">
  <?php }
}

function more($h01, $len)
{
  // $len 設定擷取長度
  //$len=200;	
  if (strlen($h01) <= $len) {
    $show = $h01;
  } //如果小於擷取長度直接秀值
  else {
    $i = 0;
    while (ord(substr($h01, $len + $i, 1)) < 192 and $i < $len) {
      $i++;
    } //抓到第一個識別碼

    $str_bit = ord(substr($h01, $len + $i, 1));

    if ($str_bit > 191 && $str_bit < 224) {
      $i = $i + 1;
    }
    if ($str_bit > 223 && $str_bit < 240) {
      $i = $i + 3;
    }
    if ($str_bit > 239 && $str_bit < 248) {
      $i = $i + 2;
    }

    $show = substr($h01, 0, $len + $i) . "... ";
  }

  echo $show;
}


function Java_Script()
{
  ?>
  <script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_preloadImages() { //v3.0
      var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_swapImgRestore() { //v3.0
      var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }

    function MM_findObj(n, d) { //v4.01
      var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
      if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
      for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
      if(!x && d.getElementById) x=d.getElementById(n); return x;
    }

    function MM_swapImage() { //v3.0
      var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
      if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }

    function MM_jumpMenu(selObj,restore){ //v3.0
      eval("location='"+selObj.options[selObj.selectedIndex].value+"'");
      if (restore) selObj.selectedIndex=0;
    }
    //-->
  </script>
  <!-- End Preload Script -->


<?php }

?>