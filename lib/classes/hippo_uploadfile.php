<?php

/* hippo_uploadfile class by DavidLanz
  
功能說明
========


類別函數
========
 
*/

class hippo_upload
{
  var $upload_form;
  var $upload_dir;
  var $upfile_name;
  var $upfile_size;
  var $upfile_type;
  var $upfile_ext;  
  var $max_file_size;
  var $extensions;
  var $err_str;
  var $temp_dir;
  var $debug;
  var $save_name; //add by  Bing
  
  function hippo_upload()
  {
    $this->upload_dir = "upload";
    $this->extensions = array(
                        'exe',
                        'php',
                        'php3',
                        'php4',
                        'phtml',
                        'cgi',
                        'pl');

    $this->copy_mode = 0;
    $this->max_file_size = 0;
  }

  function get_ext()
  {
    return strtolower(substr(strrchr($this->upfile_name, '.'), 1));
  }
  
  function check_form()
  {
    if(is_uploaded_file($_FILES[$this->upload_form]['tmp_name']))
    {
      $this->upfile_name = $_FILES[$this->upload_form]['name'];
      $this->upfile_size = $_FILES[$this->upload_form]['size'];
      $this->upfile_type = $_FILES[$this->upload_form]['type'];
      $this->upfile_ext = $this->get_ext();
      /*$this->debug = $this->get_ext();*/
      
      return true;
    }
    else
    {
      $this->err_str = "<font color=red>沒有上傳檔案，表單名稱：".$this->upload_form."</font>";
      return false;
    }
  }

  function check_upload_size()
  {
    if($this->max_file_size == true)
    {
      if($this->upfile_size > $this->max_file_size) 
      {
        $this->err_str="<font color=red>上傳檔案大小限制：".$this->max_file_size." Bytes</font>";
        return false;
      } 
    }
    return true;
  }

  function fix_dir($dir)
  {
    $dir = $this->upload_dir;

    if(is_dir($dir))
    {
      return true;
    }
    else
    {
      $this->err_str = "<font color=red>/".$dir." 資料夾並不存在</font>";
      return false;
    }

  }

  function if_uploadfile_exist()
  {
    if(file_exists($this->upload_dir . $this->upfile_name))
    {
      /*$this->err_str = "<center><font color=red>檔案 ". $this->upfile_name ." 已經存在</font></center>";*/
      @unlink($this->upload_dir . $this->upfile_name);
      return true;
    }
  }

  function check_extensions()
  {
    if(in_array($this->upfile_ext, $this->extensions))
    {
      return true;
    }
    else
    {
      $this->err_str = "<font color=red>本系統無法上傳副檔名為".$this->get_ext()."之檔案</font>";
      return false;
    }
  }
  
  function delete_file($del_file, $dir)
  {
    if(!(is_dir($dir)))
    {
      $this->err_str = "/".$dir." 資料夾並不存在<BR>";
      return false;
    }
    
    if(!@unlink($this->upload_dir.$del_file )) 
    {
      $this->error_msg("<font color=red>檔案：".$del_file." 無法被刪除，或檔案不存在。</font>");
      return false;
    } 
    
    return true;
  }
  
  function check_all()
  {
    if(!$this->check_form())
    {
      return false;
    }
    
    if(!$this->fix_dir($this->upload_dir))
    {
      return false;
    }
    
    $this->check2();
    
    if(!$this->if_uploadfile_exist())
    {
      return false;
    }
    
  }
  
  function check2()
  {
    if(!$this->check_upload_size())
    {
      return false;
    }
    
    $this->get_ext();
    
    if(!$this->check_extensions())
    {
      return false;
    }
  }
  
  function upload($myphoto)
  {
    $this->check_all();
    $this->temp_dir = $this->upload_dir . $this->upfile_name;
    $this->temp_dir = $this->upload_dir . $myphoto;
  //  $this->temp_dir = $this->upload_dir . $this->save_name.".".$this->upfile_ext;   // add by Bing
    if($this->err_str=="")
    {
      copy($_FILES[$this->upload_form]['tmp_name'], $this->temp_dir);
    }
  }
  
  function show_error()
  {
    return $this->err_str;
  }
  
}
?>