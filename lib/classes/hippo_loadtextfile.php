<?php
/* hippo_loadtextfile class by DavidLanz
*/

class hippo_loadtextfile
{
  var $err_str;
  var $num_columes;
  var $ar;
  var $ar2;
  var $res;
  var $total;
  
  function hippo_loadtextfile($aryDefine)
  {
    $this->ar = array();
    $this->res = array();
    $this->init($aryDefine);
  }
  
  function init($aryDefine)
  {
    $this->ar2 = $aryDefine;
    $this->num_columes = count($aryDefine);
  }
  
  function load_file($filename)
  {
    if(!$file=fopen($filename,"r"))
    {
      $this->err_str = "Error to load file:".$filename;
      return false;
    }
    
    $this->ar = file($filename);
    $this->total = count($this->ar);
    
    for($i=0 ; $i< $this->total ; $i++)
    {
      for($j=0 ; $j< $this->num_columes ;$j++)
      {
        $this->res[$i][$j] = trim(substr($this->ar[$i],$this->ar2[$j][0],$this->ar2[$j][1]));
      }
    }
    return true;
  }
  
  function show_error()
  {
    return $this->err_str;
  }
}

?>
