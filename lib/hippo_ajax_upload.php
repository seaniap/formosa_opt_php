<?php
/* hippo_ajax_upload class by DavidLanz
  
Function
========

Class Member Function
========

*/

class hippo_ajax_upload
{
  var $err_str;
  var $id;
  var $urlPHP;
  var $targetDiv;
  var $intDIVCounter;
  var $spliter;
  var $upload_path;
  
  /* Constructor */
  function hippo_ajax_upload($path='../include/UploadTmp/')
  {
    $this->upload_path = $path.'/';
    $this->intDIVCounter=0;
    $this->spliter='_';
    $this->init();
  }
  
  function init()
  {
    echo '<script language=JavaScript src=../js/hippo_ajax_upload.js></script>';
    return true;
  }
  
  function add_thumb_upload($id, $urlPHP, $targetDiv, $width=50, $height=50)
  {
    if($id=='' || $urlPHP=='' || $targetDiv=='')
    {
      $this->err_str='add_thumb_upload() parameter error.';
      return false;
    }
    if($width=='')
    {
      $width=50;
    }
    if($height=='')
    {
      $height=50;
    }
    $urlPHP .= '?upload_path='.$this->upload_path;
    echo '<form name='.$id.$this->spliter.$this->intDIVCounter.' id='.$id.$this->spliter.$this->intDIVCounter.' method=post enctype=multipart/form-data action='.$urlPHP.' target=iframe_'.$id.$this->spliter.$this->intDIVCounter.'>';
    echo '<span id=span_'.$id.$this->spliter.$this->intDIVCounter.' style=font-family:verdana;font-size:10;>';
    echo '<input id='.$id.$this->spliter.$this->intDIVCounter.' name='.$id.$this->spliter.$this->intDIVCounter.' type=file onchange=UploadFormData(this,' . '\'' . $id.$this->spliter.$this->intDIVCounter. '\',' . '\'' . $urlPHP . '\',' . '\'' . $targetDiv . '\'' . ') >';
	  echo '</span>';
    echo "<input type=HIDDEN name=hupload value=". $id.$this->spliter.$this->intDIVCounter.">";
    echo "<input type=HIDDEN name=upload_path value=".$this->upload_path.">";
    echo "<input type=HIDDEN name=urlPHP value=".$urlPHP.">";
    echo "<input type=HIDDEN name=targetDiv value=".$targetDiv.">";
    echo "<input type=HIDDEN name=widthThumb value=".$width.">";
    echo "<input type=HIDDEN name=heightThumb value=".$height.">";
    echo '</form>';
    echo '<iframe name=iframe_'.$id.$this->spliter.$this->intDIVCounter.' src='.$urlPHP.' width=0 height=0 scrolling=no></iframe>';
    $this->intDIVCounter++;
    return true;
  }
  
  function show_error()
  {
    return $this->err_str;
  } 
}

?>
