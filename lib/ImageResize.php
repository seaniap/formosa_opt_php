<?php

function ImageResize($from_filename, $save_filename, $in_width=400, $in_height=300, $quality=100)
{
$allow_format=array('jpeg', 'png', 'gif');
$sub_name=$t= '';

// Get new dimensions
$img_info=getimagesize($from_filename);
$width=$img_info['0'];
$height=$img_info['1'];
$imgtype=$img_info['2'];
$imgtag=$img_info['3'];
$bits=$img_info['bits'];
$channels=$img_info['channels'];
$mime=$img_info['mime'];

list($t, $sub_name)=split('/', $mime);
if ($sub_name=='jpg') {
$sub_name='jpeg';
}

if (!in_array($sub_name, $allow_format)) {
return false;
}

// 取得縮在此範圍內的比例
$percent=getResizePercent($width, $height,$in_width, $in_height);
$new_width=$width * $percent;
$new_height=$height * $percent;

// Resample
$image_new=imagecreatetruecolor($new_width,$new_height);


$image=imagecreatefromjpeg($from_filename);

imagecopyresampled($image_new, $image, 0, 0, 0, 0,$new_width, $new_height, $width, $height);

return imagejpeg($image_new, $save_filename,$quality);
}


function getResizePercent($source_w, $source_h, $inside_w, $inside_h)
{
if ($source_w < $inside_w && $source_h < $inside_h) 
{

return 1; 
}

$w_percent=$inside_w/$source_w;
$h_percent=$inside_h/$source_h;

return($w_percent>$h_percent)?$h_percent:$w_percent;

} 


function ImageCopyResizedTrue($src,$dest,$maxWidth,$maxHeight,$quality=100)
{

if(file_exists($src)&&isset($dest))
{
$destInfo=pathInfo($dest);

$srcSize=getImageSize($src);

$srcRatio=$srcSize[0]/$srcSize[1];
$destRatio=$maxWidth/$maxHeight;
if ($destRatio > $srcRatio) {
$destSize[1]=$maxHeight;
$destSize[0]=$maxHeight*$srcRatio;
}
else {
$destSize[0]=$maxWidth;
$destSize[1]=$maxWidth/$srcRatio;
}

//GIF 檔不支援輸出，因此將GIF轉成JPEG
if ($destInfo['extension']=="gif") 
$dest=substr_replace($dest, 'jpg', -3);

//建立一個 True Color 的影像
$destImage=imageCreateTrueColor($destSize[0],$destSize[1]);

//根據副檔名讀取圖檔
switch ($srcSize[2]) {
case 1: 
$srcImage=imageCreateFromGif($src); 
break;
case 2: 
$srcImage=imageCreateFromJpeg($src); 
break;
case 3: 
$srcImage=imageCreateFromPng($src); 
break;
default: return false; break;
}

//取樣縮圖
ImageCopyResampled($destImage, $srcImage, 0, 0, 0,0,$destSize[0],$destSize[1],$srcSize[0],$srcSize[1]);

//輸出圖檔
switch ($srcSize[2]) {
case 1: case 2: 
imageJpeg($destImage,$dest,$quality); 
break;
case 3: imagePng($destImage,$dest); break;
}
return true;
}
else {
return false;
}
}



?>