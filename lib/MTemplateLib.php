<?php 

function MTempHeader() {
global $MySQL,$Table_ID;
// ================================================
// SPAW PHP WYSIWYG editor control
// ================================================
// Control usage demonstration file
// ================================================
// Developed: Alan Mendelevich, alan@solmetra.lt
// Copyright: Solmetra (c)2003 All rights reserved.
// ------------------------------------------------
//                                www.solmetra.com
// ================================================
// $Revision: 1.6 $, $Date: 2004/12/30 09:38:27 $
// ================================================

// this part determines the physical root of your website
// it's up to you how to do this
if (!ereg('/$', $_SERVER['DOCUMENT_ROOT']))
  $_root = $_SERVER['DOCUMENT_ROOT'].'/';
else
  $_root = $_SERVER['DOCUMENT_ROOT'];
   
 $_root="../"; // bing

define('DR', $_root);
unset($_root);

// set $spaw_root variable to the physical path were control resides
// don't forget to modify other settings in config/spaw_control.config.php
// namely $spaw_dir and $spaw_base_url most likely require your modification
//$spaw_root = DR.'spaw/';
$spaw_root = DR.'spaw2/';


// include the control file
include $spaw_root.'spaw_control.class.php';

// here we add some styles to styles dropdown
$spaw_dropdown_data['style']['default'] = 'No styles';
$spaw_dropdown_data['style']['style1'] = 'Style no. 1';
$spaw_dropdown_data['style']['style2'] = 'Style no. 2';

?>
<?php



$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  
  //  if( $upfile1_size>0)
//   {copy ($upfile1,"../uploadimages/$upfile1_name");
//    $UploadImage1="uploadimages/".$upfile1_name;} 
	
	
 if( $_FILES['upfile1']['size']>0)
   {
    $UploadImage1=$_POST['KeyID']."-p01".strtolower(strrchr($_FILES['upfile1']['name'], "."));
    copy ($_FILES['upfile1']['tmp_name'],"../uploadimages/".$UploadImage1); 
	$p01=$UploadImage1;
	}  
 if( $_FILES['upfile2']['size']>0)
   {
    $UploadImage2=$_POST['KeyID']."-p02".strtolower(strrchr($_FILES['upfile2']['name'], "."));
    copy ($_FILES['upfile2']['tmp_name'],"../uploadimages/".$UploadImage2);
	$p02=$UploadImage2;}  
 if( $_FILES['upfile3']['size']>0)
   {
    $UploadImage3=$_POST['KeyID']."-p03".strtolower(strrchr($_FILES['upfile3']['name'], "."));
   copy ($_FILES['upfile3']['tmp_name'],"../uploadimages/".$UploadImage3);
	$p03=$UploadImage3;}  
 if( $_FILES['upfile4']['size']>0)
   {
    $UploadImage4=$_POST['KeyID']."-p04".strtolower(strrchr($_FILES['upfile4']['name'], "."));
   copy ($_FILES['upfile4']['tmp_name'],"../uploadimages/".$UploadImage4);
	$p04=$UploadImage4;} 
 if( $_FILES['upfile5']['size']>0)
   {
    $UploadImage5=$_POST['KeyID']."-p05".strtolower(strrchr($_FILES['upfile5']['name'], "."));
   copy ($_FILES['upfile5']['tmp_name'],"../uploadimages/".$UploadImage5);
	$p05=$UploadImage5;}  
 if( $_FILES['upfile6']['size']>0)
   {
    $UploadImage6=$_POST['KeyID']."-p06".strtolower(strrchr($_FILES['upfile6']['name'], "."));
   copy ($_FILES['upfile6']['tmp_name'],"../uploadimages/".$UploadImage6);
	$p06=$UploadImage6;}  
  
  
  
  $updateSQL = sprintf("UPDATE %s_BigTable SET Template=%s, Page=%s, Sq=%s, ProductID=%s, Pname=%s, Cate01=%s, Cate02=%s, Cate03=%s, Price01=%s, h01=%s, h02=%s, h03=%s, h04=%s, h05=%s, h06=%s, h07=%s, h08=%s, h09=%s, h10=%s, c01=%s, c02=%s, c03=%s, c04=%s, c05=%s, c06=%s, c07=%s, c08=%s, c09=%s, c10=%s, d01=%s, d02=%s, d03=%s, f01=%s, f02=%s, f03=%s, l01=%s, l02=%s, l03=%s, l04=%s, l05=%s, l06=%s, p01=%s, p02=%s, p03=%s, p04=%s, p05=%s, p06=%s WHERE KeyID=%s",
                       $Table_ID,
					   GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['Page'], "text"),
                       GetSQLValueString($_POST['Sq'], "int"),
                       GetSQLValueString($_POST['ProductID'], "text"),
                       GetSQLValueString($_POST['Pname'], "text"),
                       GetSQLValueString($_POST['Cate01'], "text"),
                       GetSQLValueString($_POST['Cate02'], "text"),
                       GetSQLValueString($_POST['Cate03'], "text"),
                       GetSQLValueString($_POST['Price01'], "int"),
                       GetSQLValueString($_POST['h01'], "text"),
                       GetSQLValueString($_POST['h02'], "text"),
                       GetSQLValueString($_POST['h03'], "text"),
                       GetSQLValueString($_POST['h04'], "text"),
                       GetSQLValueString($_POST['h05'], "text"),
                       GetSQLValueString($_POST['h06'], "text"),
                       GetSQLValueString($_POST['h07'], "text"),
                       GetSQLValueString($_POST['h08'], "text"),
                       GetSQLValueString($_POST['h09'], "text"),
                       GetSQLValueString($_POST['h10'], "text"),
                       GetSQLValueString($_POST['c01'], "text"),
                       GetSQLValueString($_POST['c02'], "text"),
                       GetSQLValueString($_POST['c03'], "text"),
                       GetSQLValueString($_POST['c04'], "text"),
                       GetSQLValueString($_POST['c05'], "text"),
                       GetSQLValueString($_POST['c06'], "text"),
                       GetSQLValueString($_POST['c07'], "text"),
                       GetSQLValueString($_POST['c08'], "text"),
                       GetSQLValueString($_POST['c09'], "text"),
                       GetSQLValueString($_POST['c10'], "text"),
                       GetSQLValueString($_POST['d01'], "date"),
                       GetSQLValueString($_POST['d02'], "date"),
                       GetSQLValueString($_POST['d03'], "date"),
                       GetSQLValueString($_POST['f01'], "text"),
                       GetSQLValueString($_POST['f02'], "text"),
                       GetSQLValueString($_POST['f03'], "text"),
                       GetSQLValueString($_POST['l01'], "text"),
                       GetSQLValueString($_POST['l02'], "text"),
                       GetSQLValueString($_POST['l03'], "text"),
                       GetSQLValueString($_POST['l04'], "text"),
                       GetSQLValueString($_POST['l05'], "text"),
                       GetSQLValueString($_POST['l06'], "text"),
                       GetSQLValueString($p01, "text"),
                       GetSQLValueString($p02, "text"),
                       GetSQLValueString($p03, "text"),
                       GetSQLValueString($p04, "text"),
                       GetSQLValueString($p05, "text"),
                       GetSQLValueString($p06, "text"),
                       GetSQLValueString($_POST['KeyID'], "text"));
					   
	
	$insertSQL = sprintf("INSERT INTO %s_BigTable (KeyID, Template, Page, Sq, ProductID, Pname, Cate01, Cate02, Cate03, Price01, h01, h02, h03, h04, h05, h06, h07, h08, h09, h10, c01, c02, c03, c04, c05, c06, c07, c08, c09, c10, d01, d02, d03, f01, f02, f03, l01, l02, l03, l04, l05, l06, p01, p02, p03, p04, p05, p06) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
	                   $Table_ID, 
                       GetSQLValueString($_POST['KeyID'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['Page'], "text"),
                       GetSQLValueString($_POST['Sq'], "int"),
                       GetSQLValueString($_POST['ProductID'], "text"),
                       GetSQLValueString($_POST['Pname'], "text"),
                       GetSQLValueString($_POST['Cate01'], "text"),
                       GetSQLValueString($_POST['Cate02'], "text"),
                       GetSQLValueString($_POST['Cate03'], "text"),
                       GetSQLValueString($_POST['Price01'], "int"),
                       GetSQLValueString($_POST['h01'], "text"),
                       GetSQLValueString($_POST['h02'], "text"),
                       GetSQLValueString($_POST['h03'], "text"),
                       GetSQLValueString($_POST['h04'], "text"),
                       GetSQLValueString($_POST['h05'], "text"),
                       GetSQLValueString($_POST['h06'], "text"),
                       GetSQLValueString($_POST['h07'], "text"),
                       GetSQLValueString($_POST['h08'], "text"),
                       GetSQLValueString($_POST['h09'], "text"),
                       GetSQLValueString($_POST['h10'], "text"),
                       GetSQLValueString($_POST['c01'], "text"),
                       GetSQLValueString($_POST['c02'], "text"),
                       GetSQLValueString($_POST['c03'], "text"),
                       GetSQLValueString($_POST['c04'], "text"),
                       GetSQLValueString($_POST['c05'], "text"),
                       GetSQLValueString($_POST['c06'], "text"),
                       GetSQLValueString($_POST['c07'], "text"),
                       GetSQLValueString($_POST['c08'], "text"),
                       GetSQLValueString($_POST['c09'], "text"),
                       GetSQLValueString($_POST['c10'], "text"),
                       GetSQLValueString($_POST['d01'], "date"),
                       GetSQLValueString($_POST['d02'], "date"),
                       GetSQLValueString($_POST['d03'], "date"),
                       GetSQLValueString($_POST['f01'], "text"),
                       GetSQLValueString($_POST['f02'], "text"),
                       GetSQLValueString($_POST['f03'], "text"),
                       GetSQLValueString($_POST['l01'], "text"),
                       GetSQLValueString($_POST['l02'], "text"),
                       GetSQLValueString($_POST['l03'], "text"),
                       GetSQLValueString($_POST['l04'], "text"),
                       GetSQLValueString($_POST['l05'], "text"),
                       GetSQLValueString($_POST['l06'], "text"),
                       GetSQLValueString($p01, "text"),
                       GetSQLValueString($p02, "text"),
                       GetSQLValueString($p03, "text"),
                       GetSQLValueString($p04, "text"),
                       GetSQLValueString($p05, "text"),
                       GetSQLValueString($p06, "text"));
					   

  

  if ( $_POST['Action']=="edit")
  {$Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));
  //echo $updateSQL;
  }
  else
  {$Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
  //echo $insertSQL;
  }
 
  
}


$colname_MiddleBigTable = "1";
if (isset($_GET['KeyID'])) {
  $colname_MiddleBigTable = (get_magic_quotes_gpc()) ? $_GET['KeyID'] : addslashes($_GET['KeyID']);
}

$query_MiddleBigTable = sprintf("SELECT * FROM %s_BigTable WHERE KeyID = '%s'",$Table_ID, $colname_MiddleBigTable);
$MiddleBigTable = mysqli_query($MySQL,$query_MiddleBigTable) or die(mysqli_error($MySQL));
$row_MiddleBigTable = mysqli_fetch_assoc($MiddleBigTable);
$totalRows_MiddleBigTable = mysqli_num_rows($MiddleBigTable);

if(  $_GET['Action']=="ins")
{
 $row_MiddleBigTable['KeyID']=uniqid(mt_rand());
 $row_MiddleBigTable['Template']= $_GET[Template]; 
 $row_MiddleBigTable['Sq']= $_GET[Sq]+100;
 $row_MiddleBigTable['d01']= date("Y-m-d");

}
}
?>