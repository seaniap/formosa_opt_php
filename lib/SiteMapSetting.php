<?php require_once('../../Connections/MySQL.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE AI2_SiteMap SET Page=%s, Sq=%s, Template=%s, Extend=%s, TP1=%s, TP2=%s, TP3=%s, TP4=%s, TP5=%s, TP6=%s, TP7=%s, TP8=%s, TP9=%s, Topic=%s, Topic_bg=%s, Topic_bgColor=%s, MainIcon=%s, Title=%s, Display=%s, A_FImg=%s, A_BImg=%s, B_FImg=%s, B_BImg=%s, C_FImg=%s, C_BImg=%s, D_FImg=%s, D_BImg=%s, E_FImg=%s, E_BImg=%s, U1=%s, U2=%s, U3=%s, U4=%s WHERE KeyID=%s",
                       GetSQLValueString($_POST['Page'], "text"),
                       GetSQLValueString($_POST['Sq'], "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['Extend'], "text"),
                       GetSQLValueString($_POST['TP1'], "text"),
                       GetSQLValueString($_POST['TP2'], "text"),
                       GetSQLValueString($_POST['TP3'], "text"),
                       GetSQLValueString($_POST['TP4'], "text"),
                       GetSQLValueString($_POST['TP5'], "text"),
                       GetSQLValueString($_POST['TP6'], "text"),
                       GetSQLValueString($_POST['TP7'], "text"),
                       GetSQLValueString($_POST['TP8'], "text"),
                       GetSQLValueString($_POST['TP9'], "text"),
                       GetSQLValueString($_POST['Topic'], "text"),
                       GetSQLValueString($_POST['Topic_bg'], "text"),
                       GetSQLValueString($_POST['Topic_bgColor'], "text"),
                       GetSQLValueString($_POST['MainIcon'], "text"),
                       GetSQLValueString($_POST['Title'], "text"),
                       GetSQLValueString($_POST['Display'], "text"),
                       GetSQLValueString($_POST['A_FImg'], "text"),
                       GetSQLValueString($_POST['A_BImg'], "text"),
                       GetSQLValueString($_POST['B_FImg'], "text"),
                       GetSQLValueString($_POST['B_BImg'], "text"),
                       GetSQLValueString($_POST['C_FImg'], "text"),
                       GetSQLValueString($_POST['C_BImg'], "text"),
                       GetSQLValueString($_POST['D_FImg'], "text"),
                       GetSQLValueString($_POST['D_BImg'], "text"),
                       GetSQLValueString($_POST['E_FImg'], "text"),
                       GetSQLValueString($_POST['E_BImg'], "text"),
                       GetSQLValueString($_POST['U1'], "text"),
                       GetSQLValueString($_POST['U2'], "text"),
                       GetSQLValueString($_POST['U3'], "text"),
                       GetSQLValueString($_POST['U4'], "text"),
                       GetSQLValueString($_POST['KeyID'], "text"));

  
  $Result1 = mysqli_query($MySQL,$updateSQL) or die(mysqli_error($MySQL));

  $updateGoTo = "../index.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_SiteMap = "-1";
if (isset($_GET['KeyID'])) {
  $colname_SiteMap = (get_magic_quotes_gpc()) ? $_GET['KeyID'] : addslashes($_GET['KeyID']);
}

$query_SiteMap = sprintf("SELECT * FROM AI2_SiteMap WHERE KeyID = '%s'", $colname_SiteMap);
$SiteMap = mysqli_query($MySQL,$query_SiteMap) or die(mysqli_error($MySQL));
$row_SiteMap = mysqli_fetch_assoc($SiteMap);
$totalRows_SiteMap = mysqli_num_rows($SiteMap);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
</head>

<body>
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
    <tr valign="baseline">
      <td nowrap align="right">KeyID:</td>
      <td><?php echo $row_SiteMap['KeyID']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Page:</td>
      <td><input type="text" name="Page" value="<?php echo $row_SiteMap['Page']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Sq:</td>
      <td><input type="text" name="Sq" value="<?php echo $row_SiteMap['Sq']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Template:</td>
      <td><input type="text" name="Template" value="<?php echo $row_SiteMap['Template']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Extend:</td>
      <td><input type="text" name="Extend" value="<?php echo $row_SiteMap['Extend']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP1:</td>
      <td><input type="text" name="TP1" value="<?php echo $row_SiteMap['TP1']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP2:</td>
      <td><input type="text" name="TP2" value="<?php echo $row_SiteMap['TP2']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP3:</td>
      <td><input type="text" name="TP3" value="<?php echo $row_SiteMap['TP3']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP4:</td>
      <td><input type="text" name="TP4" value="<?php echo $row_SiteMap['TP4']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP5:</td>
      <td><input type="text" name="TP5" value="<?php echo $row_SiteMap['TP5']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP6:</td>
      <td><input type="text" name="TP6" value="<?php echo $row_SiteMap['TP6']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP7:</td>
      <td><input type="text" name="TP7" value="<?php echo $row_SiteMap['TP7']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP8:</td>
      <td><input type="text" name="TP8" value="<?php echo $row_SiteMap['TP8']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">TP9:</td>
      <td><input type="text" name="TP9" value="<?php echo $row_SiteMap['TP9']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Topic:</td>
      <td><input type="text" name="Topic" value="<?php echo $row_SiteMap['Topic']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Topic_bg:</td>
      <td><input type="text" name="Topic_bg" value="<?php echo $row_SiteMap['Topic_bg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Topic_bgColor:</td>
      <td><input type="text" name="Topic_bgColor" value="<?php echo $row_SiteMap['Topic_bgColor']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">MainIcon:</td>
      <td><input type="text" name="MainIcon" value="<?php echo $row_SiteMap['MainIcon']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Title:</td>
      <td><input type="text" name="Title" value="<?php echo $row_SiteMap['Title']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Display:</td>
      <td><input type="text" name="Display" value="<?php echo $row_SiteMap['Display']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">A_FImg:</td>
      <td><input type="text" name="A_FImg" value="<?php echo $row_SiteMap['A_FImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">A_BImg:</td>
      <td><input type="text" name="A_BImg" value="<?php echo $row_SiteMap['A_BImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">B_FImg:</td>
      <td><input type="text" name="B_FImg" value="<?php echo $row_SiteMap['B_FImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">B_BImg:</td>
      <td><input type="text" name="B_BImg" value="<?php echo $row_SiteMap['B_BImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">C_FImg:</td>
      <td><input type="text" name="C_FImg" value="<?php echo $row_SiteMap['C_FImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">C_BImg:</td>
      <td><input type="text" name="C_BImg" value="<?php echo $row_SiteMap['C_BImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">D_FImg:</td>
      <td><input type="text" name="D_FImg" value="<?php echo $row_SiteMap['D_FImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">D_BImg:</td>
      <td><input type="text" name="D_BImg" value="<?php echo $row_SiteMap['D_BImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">E_FImg:</td>
      <td><input type="text" name="E_FImg" value="<?php echo $row_SiteMap['E_FImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">E_BImg:</td>
      <td><input type="text" name="E_BImg" value="<?php echo $row_SiteMap['E_BImg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">U1:</td>
      <td><input type="text" name="U1" value="<?php echo $row_SiteMap['U1']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">U2:</td>
      <td><input type="text" name="U2" value="<?php echo $row_SiteMap['U2']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">U3:</td>
      <td><input type="text" name="U3" value="<?php echo $row_SiteMap['U3']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">U4:</td>
      <td><input type="text" name="U4" value="<?php echo $row_SiteMap['U4']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="更新記錄"></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="KeyID" value="<?php echo $row_SiteMap['KeyID']; ?>">
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysqli_free_result($SiteMap);
?>
