<?php  
session_start();
session_destroy();
 
 date_default_timezone_set('Asia/Taipei');
  ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
<META NAME="ROBOTS" CONTENT="NOARCHIVE">
<title>謝謝使用網站管理系統</title>
<link rel="stylesheet" href="aiadmin/plugins/fontawesome5/css/fontawesome-all.min.css">
<link href="aiadmin/css/style.css" rel="stylesheet" type="text/css">
<link href="aiadmin/css/table.css" rel="stylesheet" type="text/css">
<link href="aiadmin/css/form.css" rel="stylesheet" type="text/css">
<style>
    body {background: #eee;height: 100vh;}
</style>
</head>
<body>
 <div id="Box">
 <div class="login-block">
            <img src="aiadmin/img/logo-w.svg" alt="" class="login-logo">
            <form action="Login.php">
            <p class="big text-white mb-30">您已登出 Server !! 謝謝使用<br>現在時間 <? echo Date("Y-m-j H:i:s") ; ?><br></p>
            <div class="mb-30"><input name="Submit" type="submit" class="button w-btn" value="重新登入"></div>
            </form>
        </div>
</div>

</body>
</html>
