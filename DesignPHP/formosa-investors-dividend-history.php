<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="header" class="l-header"></header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">股利及股價資訊</h3>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">一、股價查詢</h4>
                        <div class="row align-items-end">
                            <div class="col-md-2 col-6">
                                <a href="https://www.tpex.org.tw/web/index.php?l=zh-tw" target="_blank" class="c-btn c-btn--contained c-btn-gray-200 u-text-black border border-secondary">
                                    股價查詢
                                </a>
                            </div>
                            <p class="list-inline-item u-text-red-formosa-02 u-mb-000 u-font-14">*個股查詢:輸入股票代碼5312</p> 
                        </div>                       
                    </div>
                </div>
                <div class="u-py-100 u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">二、股利查詢</h4>
                        <div class="l-tablesSroller-wrapper">
                            <table class="table table-bordered l-table">
                                <thead>
                                    <tr>
                                        <th colspan="5">股利(單位：元/股)</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr class="u-bg-gray-300">
                                        <th class="align-middle">年度</th>
                                        <th class="align-middle">現金<br class="d-sm-none">股利</th>
                                        <th class="align-middle">股票<br class="d-sm-none">股利</th>
                                        <th class="align-middle">股利<br class="d-sm-none">合計</th>
                                        <th class="align-middle">除權息<br class="d-sm-none">交易日</th>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">109年度<br>（110年分配）</td>
                                        <td class="align-middle">4.00</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">4.00</td>
                                        <td class="align-middle">9/1</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">108年度<br>（109年分配）</td>
                                        <td class="align-middle">4.00</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">4.00</td>
                                        <td class="align-middle">8/4</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">107年度<br>（108年分配）</td>
                                        <td class="align-middle">3.80</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">3.80</td>
                                        <td class="align-middle">8/1</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">106年度<br>（107年分配）</td>
                                        <td class="align-middle">3.80</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">3.80</td>
                                        <td class="align-middle">7/31</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">105年度<br>（106年分配）</td>
                                        <td class="align-middle">3.60</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">3.60</td>
                                        <td class="align-middle">7/27</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">104年度<br>（105年分配）</td>
                                        <td class="align-middle">3.60</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">3.60</td>
                                        <td class="align-middle">7/28</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">103年度<br>（104年分配）</td>
                                        <td class="align-middle">3.60</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">3.60</td>
                                        <td class="align-middle">7/30</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">102年度<br>（103年分配）</td>
                                        <td class="align-middle">4.20</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">4.20</td>
                                        <td class="align-middle">7/24</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">101年度<br>（102年分配）</td>
                                        <td class="align-middle">3.60</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">3.60</td>
                                        <td class="align-middle">7/29</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">100年度<br>（101年分配）</td>
                                        <td class="align-middle">2.60</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">2.60</td>
                                        <td class="align-middle">7/26</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">99年度<br>（100年分配）</td>
                                        <td class="align-middle">2.20</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">2.20</td>
                                        <td class="align-middle">8/3</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">98年度<br>（99年分配）</td>
                                        <td class="align-middle">2.00</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">2.00</td>
                                        <td class="align-middle">7/29</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">97年度<br>（98年分配）</td>
                                        <td class="align-middle">2.00</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">2.00</td>
                                        <td class="align-middle">7/7</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">96年度<br>（97年分配）</td>
                                        <td class="align-middle">1.60</td>
                                        <td class="align-middle">0.50</td>
                                        <td class="align-middle">2.10</td>
                                        <td class="align-middle">8/14</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">95年度<br>（96年分配）</td>
                                        <td class="align-middle">2.70</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">2.70</td>
                                        <td class="align-middle">7/10</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">94年度<br>（95年分配）</td>
                                        <td class="align-middle">2.30</td>
                                        <td class="align-middle">0.00</td>
                                        <td class="align-middle">2.30</td>
                                        <td class="align-middle">7/17</td>
                                    </tr>
                                    <tr>
                                        <td class="align-middle white-space-nowrap">93年度<br>（94年分配）</td>
                                        <td class="align-middle">0.84</td>
                                        <td class="align-middle">0.90</td>
                                        <td class="align-middle">1.74</td>
                                        <td class="align-middle">8/11</td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="align-middle white-space-nowrap">92年度<br>（93年分配）</td>
                                        <td class="align-middle">0.35</td>
                                        <td class="align-middle">1.00</td>
                                        <td class="align-middle">1.35</td>
                                        <td class="align-middle">8/10</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footer" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>