<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="header" class="l-header"></header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <h3 class="c-title-center u-mb-125">公司治理</h3>
                </div>
                <!-- START TAB -->
                <div class="u-pb-100">
                    <div class="container">
                        <div class="d-md-flex d-none c-tab-verF row justify-content-center">
                            <div class="c-tab-container">
                                <a href="regulation-election-of-independent-directors.html" class="c-tab-linkF">獨立董事選任資訊</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-selection-procedure-of-director.html" class="c-tab-linkF">董事選任程序</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-board-operation.html"
                                    class="c-tab-linkF">董事會</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-articles-of-incorporation.html" class="c-tab-linkF">公司章程</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-financial-regulations.html" class="c-tab-linkF">財務規章</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-organizational-structure-of-the-company.html" class="c-tab-linkF active">公司組織架構與經理人之職稱及職權範圍</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-Internal-Audit-Operation.html" class="c-tab-linkF">內部稽核之組織及運作</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-others-regulations.html" class="c-tab-linkF">其他公司治理規章</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-corporate-fathfull.html" class="c-tab-linkF">公司履行誠信經營情形及採行措施</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-corporate-governance.html" class="c-tab-linkF">公司治理運作情形</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="regulation-functional-committees.html" class="c-tab-linkF">功能性委員會相關資訊</a>
                            </div>
                        </div>
                        <div class="row justify-content-center d-block d-md-none">
                            <div class="col-12">
                                <select name="select" id="" class="l-form-field l-form-select u-bg-gray-200 js-selectURL">
                                    <option value="default" disabled>治理項目</option>
                                    <option value="regulation-election-of-independent-directors.html">獨立董事選任資訊</option>
                                    <option value="regulation-selection-procedure-of-director.html">董事選任程序</option>
                                    <option value="regulation-board-operation.html">董事會</option>
                                    <option value="regulation-articles-of-incorporation.html">公司章程</option>
                                    <option value="regulation-financial-regulations.html">財務規章</option>
                                    <option value="regulation-organizational-structure-of-the-company.html" selected>公司組織架構與經理人之職稱及職權範圍</option>
                                    <option value="regulation-Internal-Audit-Operation.html">內部稽核之組織及運作</option>
                                    <option value="regulation-others-regulations.html">其他公司治理規章</option>
                                    <option value="regulation-corporate-fathfull.html">公司履行誠信經營情形及採行措施</option>
                                    <option value="regulation-corporate-governance.html">公司治理運作情形</option>
                                    <option value="regulation-functional-committees.html">功能性委員會相關資訊</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END TAB -->
                <div class="u-pb-100">
                    <div class="container">
                        <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">公司組織架構與經理人之職稱及職權範圍</p>
                        <!-- <p class="u-mb-000">本公司依照證交法所訂資格條件選任獨立董事之相關訊息</p> -->
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">一、公司之組織結構</h4>
                        <div class="row">
                            <div class="col-12 d-none d-md-block">
                                <img class="w-100" src="./assets/img/regulation/organization-PC.svg" alt="">
                            </div>
                            <div class="col-12 d-md-none">
                                <img class="w-100" src="./assets/img/regulation/organization-mobile.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-py-100 u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">二、經理人之職稱及職權範圍</h4>
                        <table class="table table-bordered l-table">
                            <thead>
                                <tr>
                                    <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-bottom-black l-table-vertical-align-middle">部門</th>
                                    <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle">主管<br class="d-sm-none">姓名</th>
                                    <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle">職稱</th>
                                    <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle" style="min-width: 100px;">主要職掌</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle white-space-nowrap">稽核室</td>
                                    <td class="text-center l-table-vertical-align-middle white-space-nowrap">曾威愷</td>
                                    <td class="text-center l-table-vertical-align-middle">稽核長</td>
                                    <td class="l-table-vertical-align-middle">負責內部控制之改進及內部稽核工作之執行。</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td class="text-center l-table-vertical-align-middle">營管室</td>
                                    <td class="text-center l-table-vertical-align-middle">葉智明</td>
                                    <td class="text-center l-table-vertical-align-middle white-space-nowrap">副總經理</td>
                                    <td class="l-table-vertical-align-middle">協助分公司營運規劃及控制管理、商品採購、倉儲及銷售管理。</td>
                                </tr>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">行銷室</td>
                                    <td class="text-center l-table-vertical-align-middle">楊記生</td>
                                    <td class="text-center l-table-vertical-align-middle">總監</td>
                                    <td class="l-table-vertical-align-middle">廣告企劃及執行、商圈規劃與實地調查、市調及商情蒐集。</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td class="text-center l-table-vertical-align-middle">人資室</td>
                                    <td class="text-center l-table-vertical-align-middle">周克倫</td>
                                    <td class="text-center l-table-vertical-align-middle">人資長</td>
                                    <td class="l-table-vertical-align-middle">負責人事、職教之管理。</td>
                                </tr>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">管理室</td>
                                    <td class="text-center l-table-vertical-align-middle">李正中</td>
                                    <td class="text-center l-table-vertical-align-middle">協理</td>
                                    <td class="l-table-vertical-align-middle">負責總務、行政文書之管理。</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td class="text-center l-table-vertical-align-middle">財會室</td>
                                    <td class="text-center l-table-vertical-align-middle">張立徽</td>
                                    <td class="text-center l-table-vertical-align-middle">副總經理</td>
                                    <td class="l-table-vertical-align-middle">負責資金管理、財務調度、成本與一般帳務處理、會計報表製作與分析、股務作業。</td>
                                </tr>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">資訊室</td>
                                    <td class="text-center l-table-vertical-align-middle">陳世煌</td>
                                    <td class="text-center l-table-vertical-align-middle">資訊長</td>
                                    <td class="l-table-vertical-align-middle">負責電腦主機、網路、資訊系統、資料庫、應用軟體及資訊安全之規劃、建置、開發及維護。</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footer" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>