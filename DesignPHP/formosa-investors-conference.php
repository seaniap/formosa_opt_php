<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="header" class="l-header"></header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">法人說明會訊息</h3>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <div class="c-accordion">
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent1">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">110年度法人說明會</p>
                                        </div>
                                        <div class="c-accordion-btn-icon js-accordionExpended">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent1" class="c-accordion-content js-accordionExpended">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <ul class="mb-0">
                                            <li class="mb-3">法說會時間：110年5月27日14時00分</li>
                                            <li class="mb-3">法說會地點：櫃買中心官網線上直播</li>
                                            <li class="mb-3">法說會擇要訊息：本公司受邀參加櫃買中心舉辦之線上法人說明會，說明110年第一季財務業務相關資訊。</li>
                                            <li class="mb-3">詳細資料：</li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/110/20210527_寶島110Q1法說(中文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1100527法說會中文檔案</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/110/20210527_寶島110Q1法說(英文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1100527法說會英文檔案</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent2">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">109年度法人說明會</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent2" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <ul class="mb-0">
                                            <li class="mb-3">法說會時間：109年12月2日14時00分</li>
                                            <li class="mb-3">法說會地點：台北市敦化南路二段97號11樓 教育訓練室</li>
                                            <li class="mb-3">法說會擇要訊息：本公司受邀參加元富證券舉辦之法人說明會，說明109年第三季財務業務相關資訊。</li>
                                            <li class="mb-3">詳細資料：</li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/109/20200521_寶島109Q1法說(中文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1090521法說會中文檔案</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/109/20200521_寶島109Q1法說(英文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1090521法說會英文檔案</span>
                                                </a>
                                            </div>
                                        </div>
                                        <hr class="w-100 u-bdr-top-gray-300 my-3">
                                        <ul class="mb-0">
                                            <li class="mb-3">法說會時間：109年05月21日14時00分</li>
                                            <li class="mb-3">法說會地點：櫃買中心官網線上直播</li>
                                            <li class="mb-3">法說會擇要訊息：本公司受邀參加櫃買中心舉辦之法人說明會，說明109年第一季財務業務相關資訊。</li>
                                            <li class="mb-3">詳細資料：</li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/109/20200521_寶島109Q1法說(中文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1090527法說會中文檔案</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/109/20200521_寶島109Q1法說(英文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1090527法說會英文檔案</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent3">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">108年度法人說明會</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent3" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <ul class="mb-0">
                                            <li class="mb-3">法說會時間：108年05月22日13時55分</li>
                                            <li class="mb-3">法說會地點：台北市中正區羅斯福路二段100號11樓</li>
                                            <li class="mb-3">法說會擇要訊息：本公司受邀參加櫃買中心舉辦之法人說明會，說明108年第一季財務業務相關資訊。</li>
                                            <li class="mb-3">詳細資料：</li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/108/20190604_寶島108Q1法說(中文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1080522法說會中文檔案</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/108/20190604_寶島108Q1法說(英文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1080522法說會英文檔案</span>
                                                </a>
                                            </div>
                                        </div>
                                        <hr class="w-100 u-bdr-top-gray-300 my-3">
                                        <ul class="mb-0">
                                            <li class="mb-3">法說會時間：108年11月26日14時00分</li>
                                            <li class="mb-3">法說會地點：台北市敦化南路二段97號11樓(敦南摩天大廈-元富證券11樓教育訓練室)</li>
                                            <li class="mb-3">法說會擇要訊息：本公司受邀參加元富證券舉辦之法人說明會，說明108年第三季財務業務相關資訊。</li>
                                            <li class="mb-3">詳細資料：</li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/108/20191126_寶島108Q3法說(中文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1081126法說會中文檔案</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/108/20191126_寶島108Q3法說(英文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1081126法說會英文檔案</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent4">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">107年度法人說明會</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent4" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <ul class="mb-0">
                                            <li class="mb-3">法說會時間：107年05月24日14時30分</li>
                                            <li class="mb-3">法說會地點：台北市中山區明水路700號 2M3會議室</li>
                                            <li class="mb-3">法說會擇要訊息：本公司受邀參加凱基證券舉辦之法人說明會，說明107年第一季財務業務相關資訊。</li>
                                            <li class="mb-3">詳細資料：</li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/107/20180524_寶島107Q1法說(二版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1070524法說會中文檔案</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/107/20180524_寶島107Q1法說(英文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1070524法說會英文檔案</span>
                                                </a>
                                            </div>
                                        </div>
                                        <hr class="w-100 u-bdr-top-gray-300 my-3">
                                        </ul>
                                        <ul class="mb-0">
                                        <li class="mb-3">法說會時間：107年08月31日13時55分</li>
                                        <li class="mb-3">法說會地點：台北市中正區羅斯福路二段100號11樓</li>
                                        <li class="mb-3">法說會擇要訊息：本公司受邀參加櫃買中心舉辦之法人說明會，說明107年上半年財務業務相關資訊。</li>
                                        <li class="mb-3">詳細資料：</li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/107/20180831_寶島107Q2法說(英文版).pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1070831法說會中文檔案</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/legalPersons/107/20180831_寶島107Q2法說講義.pdf"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>1070831法說會英文檔案</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                        </div>
                    </div>
                </div>
                <!-- START 頁籤 -->
                <div class="u-py-100 u-pb-400">
                    <div class="container">
                        <!-- <nav>
                            <ul class="c-pagination justify-content-center align-items-center u-mb-000">
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_to_left"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_left"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item active"><a href="#">1</a></li>
                                <li class="c-pagination-item"><a href="#">2</a></li>
                                <li class="c-pagination-item"><a href="#">3</a></li>
                                <li class="c-pagination-item"><a href="#">4</a></li>
                                <li class="c-pagination-item"><a href="#">5</a></li>
                                <li class="c-pagination-item">...</li>
                                <li class="c-pagination-item"><a href="#">10</a></li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_right"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_to_right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>-->
                    </div>
                </div>
                <!-- END 頁籤 -->
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footer" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>