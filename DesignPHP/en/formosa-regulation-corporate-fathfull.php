<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="The Company's Performance of Ethical Corporate Management and the Measures Taken">
    <meta name="description" content="The Company's Performance of Ethical Corporate Management and the Measures Taken">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="../assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="headerEn" class="l-header"></header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">The Company's Performance of Ethical Corporate Management
                            and the Measures Taken</h3>
                    </div>
                </div>
                <div class="u-pb-300 u-pt-100">
                    <div class="container">
                        <div class="l-tablesSroller-wrapper">
                            <table class="table table-bordered l-table">
                                <thead>
                                    <tr>
                                        <th colspan="1" rowspan="2" class="l-table-vertical-align-middle u-p-100"
                                            style="min-width: 250px;">
                                            Evaluation item
                                        </th>
                                        <th colspan="3" rowspan="1" class="l-table-vertical-align-middle u-p-100"
                                            style="min-width: 500px;">
                                            Implementation status
                                        </th>
                                        <th colspan="1" rowspan="2" class="l-table-vertical-align-middle u-p-100 th-en">
                                            Deviations from the Ethical Corporate Management Best Practice Principles
                                            for TWSE / TPEx Listed Companies and reasons thereof
                                        </th>
                                    </tr>

                                    <tr>
                                        <th colspan="1" rowspan="1"
                                            class="l-table-vertical-align-middle u-bdr-left-white border-left u-p-100"
                                            style="min-width: 50px;">
                                            Yes
                                        </th>
                                        <th colspan="1" rowspan="1" class="l-table-vertical-align-middle u-p-100"
                                            style="min-width: 50px;">
                                            No
                                        </th>
                                        <th colspan="1" rowspan="1" class="l-table-vertical-align-middle u-p-100">
                                            Description
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅰ.&nbsp;Establishment of ethical corporate management policies and
                                                    programs</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;Does the Company establish ethical corporate management
                                                    policies passed by the Board of Directors and declare its ethical
                                                    corporate management policies and procedures in its guidelines and
                                                    external documents, and do the Board of Directors and senior
                                                    management work proactively to implement their commitment to those
                                                    management policies?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle text-center" colspan="1"
                                            rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;The Company has established the "Ethical Corporate
                                                    Management Principles," as well as internal control, internal audit,
                                                    accounting systems, and various management regulations approved by
                                                    the Board of Directors.When purchasing goods from suppliers, we also
                                                    carefully evaluate and select suppliers that comply with ethical
                                                    corporate management to avoid the purchase of counterfeit goods or
                                                    the sale of products that infringe intellectual property rights and
                                                    strive to achieve the standards and norms of ethical corporate
                                                    management in practice.The Board also promotes the operation of
                                                    corporate governance by enhancing information transparency and
                                                    adherence to the principle of good faith, and requires directors,
                                                    supervisors, senior management, and managerial officers to lead by
                                                    example and abide by the principle of good faith to establish a
                                                    sound corporate governance and risk control mechanism, and create a
                                                    sustainable business environment.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;Does the Company establish a risk assessment mechanism for
                                                    unethical conduct, periodically analyze and assess operating
                                                    activities with high-potential unethical conduct in the business
                                                    scope, and formulate precautionary measures against unethical
                                                    conducts, which at least cover the precautionary measures stated in
                                                    Article 7, paragraph 2 of “Ethical Corporate Management Best
                                                    Practice Principles for TWSE/TPEx Listed Companies?”
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle text-center" colspan="1"
                                            rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;The Company has established the "Ethical Corporate
                                                    Management Principles." The Principles clearly define operating
                                                    procedures, conduct guidelines, and punishment and appeal systems
                                                    for violations and implement them following the principles. During
                                                    the training of recruits, the importance of good faith is
                                                    strengthened and advocated. In addition, the corporate governance
                                                    director will instruct a dedicated person to periodically assess the
                                                    risk of unethical conduct and then formulate policies to prevent
                                                    unethical conduct with the management.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;Does the Company clearly define operating procedures,
                                                    conduct guidelines, and punishment and appeal systems for violations
                                                    in precautionary measures against unethical conduct, implement them,
                                                    and regularly review the measures above?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle text-center" colspan="1"
                                            rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;The Company has formulated the "Code of Ethical Conduct" and
                                                    "Ethical Corporate Management Principles" to prevent various
                                                    operating activities with high potential of unethical conduct, and
                                                    all employees are required to follow it. In addition, the Company's
                                                    internal and external websites have set up a special email to
                                                    provide employees and related personnel to report any improper
                                                    practices.The Company always reviews the possible risks of
                                                    dishonesty. It has formulated "Employee Rewards and Punishments" and
                                                    included them in the performance appraisal standards to enhance the
                                                    effectiveness of the Company's ethical corporate management. In
                                                    addition, the Company's corporate governance director will instruct
                                                    a dedicated person to review the implementation of the relevant
                                                    measures periodically.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅱ.&nbsp;Fulfillment of ethical corporate management</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;Does the Company evaluate business partners' ethical records
                                                    and include ethics-related clauses in the business contracts signed
                                                    with the counterparties?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    A.&nbsp;The Company has formulated a “Supplier Management Policy”
                                                    and established an evaluation system with customers or suppliers.
                                                    When entering contracts with them, the rights and obligations of
                                                    both parties are set out in detail to ensure good communication,
                                                    coordination, and integrity partnership between each other.

                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;Does the Company establish an exclusively dedicated unit
                                                    under the Board to implement ethical corporate management, and
                                                    report to the Board on a regular basis (at least once a year) about
                                                    the ethical corporate management policies, precautionary measures
                                                    against unethical conducts, as well as supervision of implementation
                                                    status?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;The Company, under the coordination of the Audit Office and
                                                    the Administration Office, fully advocates ethical corporate
                                                    management at all levels and aspects and makes the Audit Office and
                                                    the Administration Office as the concurrent units to promote ethical
                                                    corporate management and reports to the Board at least once a year
                                                    on the implementation status of ethical corporate management
                                                    policies and precautionary measures against unethical conducts.</li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;Does the Company establish policies to prevent conflicts of
                                                    interest, provide appropriate communication channels, and implement
                                                    them accordingly?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    C.&nbsp;The Company has formulated the "Code of Ethical Conduct" and
                                                    "Ethical Corporate Management Principles" to prevent conflicts of
                                                    interest, requiring directors, managerial officers, employees,
                                                    appointees, or de facto controllers when performing business not to
                                                    provide directly or indirectly, promise, request, or accept any
                                                    improper benefits, or commit other unethical conducts that violate
                                                    good faith, laws, or breach of fiduciary duties to obtain
                                                    benefits.In addition, a special email is set up following the
                                                    Company's "Whistleblowing Measures" to provide an appropriate
                                                    channel for employees and related personnel to report any improper
                                                    practices.

                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>D.&nbsp;Does the Company establish effective accounting systems and
                                                    internal control systems to implement ethical corporate management
                                                    and assign the internal audit unit to draw up relevant audit plans
                                                    based on the assessment results of the unethical conduct risks, and
                                                    verify compliance with the precautionary measures against unethical
                                                    conducts, or entrust CPAs to perform the audit?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    D.&nbsp;The Company has established an internal control system,
                                                    internal audit implementation rules, accounting system, and various
                                                    management regulations and has fulfilled the requirements and norms
                                                    for ethical corporate management in implementation. The internal
                                                    audit has included relevant matters in the audit plan, and the
                                                    related audit matters are carried out either periodically or
                                                    occasionally. No abnormalities are found.

                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>E.&nbsp;Does the Company regularly hold an internal and external
                                                    educational training on ethical corporate management?

                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>E.&nbsp;The Company regularly arranges for directors, supervisors,
                                                    and employees to participate in the educational training and
                                                    advocacy of ethical corporate management.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅲ.&nbsp;Operation of the Company's whistleblowing system</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;Does the Company establish both a reward/ whistleblowing
                                                    system and convenient whistleblowing channels? Are appropriate
                                                    personnel assigned to the accused party?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    A.&nbsp;The Company has established the "Whistleblowing Measures"
                                                    and disclosed the measures on the corporate website. The measures
                                                    have a clear whistle-blowing channel and a dedicated person for
                                                    acceptance.When encountering violations of laws or internal
                                                    regulations, one may report to the managerial officers and internal
                                                    chief auditor to facilitate two-way communication.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;Does the Company establish standard operating procedures for
                                                    the reported matters, follow-up measures to be taken after
                                                    completing the investigation, and the relevant confidential
                                                    mechanism?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    B.&nbsp;The Company encourages the reporting of any illegal or
                                                    unethical conduct. It has established "Whistleblowing Measures,"
                                                    which contains the investigation procedures and related
                                                    confidentiality mechanisms for accepting whistleblowing matters and
                                                    implements them accordingly. After completing the investigation, the
                                                    competent supervisor will decide the following measures to be taken,
                                                    and the task will be tracked and handled by a dedicated person.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;Does the Company provide protection to whistleblowers
                                                    against receiving improper treatment?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    C.&nbsp;The "Whistleblowing Measures" formulated by the Company
                                                    include a whistleblower protection policy, which keeps the
                                                    whistleblower's information confidential and protects the
                                                    whistleblower from any form of retaliation.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅳ.&nbsp;Enhanced disclosure of information</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;Does the Company disclose its ethical corporate management
                                                    principles and the results of its implementation on the Company's
                                                    website and MOPS?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center l-table-vertical-align-middle" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="l-table-vertical-align-middle" colspan="1" rowspan="1">
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;The Company has disclosed the formulated ethical corporate
                                                    management principles in the corporate governance section of the
                                                    website and detailed information about ethical corporate management
                                                    in the annual report at MOPS.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="l-table-vertical-align-middle text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>

                                    <tr class="u-bg-gray-100">
                                        <td colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅴ.&nbsp;If the Company has established its own ethical corporate
                                                    management principles based on the "Ethical Corporate Management
                                                    Best Practice Principles for TWSE/TPEx Listed Companies," please
                                                    describe the implementation and any deviations from the
                                                    Principles:<br>Besides the "Ethical Corporate Management
                                                    Principles," the Company has also formulated the "Code of Ethical
                                                    Conduct" and "Management Measures of the Prevention of Insider
                                                    Trading." Apart from regulating directors, supervisors, and senior
                                                    management, the Company also has an employee handbook for new
                                                    employees to sign the relevant statements and strengthen advocacy.
                                                    Its content and related operations are not materially different from
                                                    the “Ethical Corporate Management Best Practice Principles for
                                                    TWSE/TPEx Listed Companies.”
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅵ.&nbsp;Other important information to facilitate a better
                                                    understanding of the Company's ethical corporate management (e.g.,
                                                    review of and amendments to ethical corporate management principles)
                                                    <br>
                                                    None.
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footerEn" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>