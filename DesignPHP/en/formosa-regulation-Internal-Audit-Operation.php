<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="../assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="headerEn" class="l-header"></header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">Internal Audit Operation</h3>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <!-- <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">Internal Audit
                            Operation</p> -->
                        <p class="u-mb-000 u-font-14 u-text-red-formosa-02 u-font-weight-900">Information Regarding the Selection of Independent Directors by the Company
                            According to the Qualifications Stipulated in the Securities and Exchange Act</p>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">I.&nbsp;Audit Unit and Auditor</h4>
                        <ul class="u-list-style--custom-level02 u-mb-000">
                            <li class="u-mb-100">A.&nbsp;The Company's audit unit is subordinate to the Board of Directors, with one audit manager and three audit commissioners to meet the qualifications required by the Financial Supervisory Commission (FSC).
                                The appointment and dismissal of the Company's audit manager shall be approved by the Board of Directors and submitted to the Board of Directors for resolution.The appointment, dismissal, assessment, and remuneration of the Company's internal auditors shall be signed and submitted by the chief auditor to the Chairman.​​
                            </li>
                            <li class="u-mb-100">B.&nbsp;The auditor's name, age, educational background, experience, years of service,
                                training. etc. shall be reported to the Commission via the internet information system
                                for recordation according to the format prescribed by the FSC before the end of January
                                each year.</li>
                            <li>C.&nbsp;In addition to regularly reporting the audit business to the Audit Committee,
                                the audit manager shall report to the Board of Directors as nonvoting delegates.</li>
                        </ul>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">II.&nbsp;Scope of Internal Audit</h4>
                        <ul class="u-list-style--custom-level02 u-mb-000">
                            <li class="u-mb-100">A.&nbsp;All units of the Company's headquarters and their subordinating branches.</li>
                            <li>B.&nbsp;Reinvestment businesses as defined in the Accounting Bulletin. </li>
                        </ul>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">III.&nbsp;Scope of Internal Audit</h4>
                        <ul class="u-list-style--custom-level02 u-mb-000">
                            <li class="u-mb-100">A.&nbsp;Assess and help improve the design and operation of various internal control
                                systems of the Company and its subsidiaries.</li>
                            <li class="u-mb-100">B.&nbsp;Evaluate the effectiveness of the Company's various plans, organizations, and
                                supervisory procedures in achieving the Company's objectives.</li>
                            <li class="u-mb-100">C.&nbsp;Check the correctness and completeness of financial and operational
                                information, verify the maintenance and control of such information, and the timely and
                                effective reporting of such information.</li>
                            <li class="u-mb-100">D.&nbsp;Examine whether supervisors at all levels have taken appropriate measures to
                                safeguard assets against damage, embezzlement, misappropriation, and tampering of
                                records, resulting in the loss (including theft, fire damage, and loss arising from
                                misconduct or illegal acts).</li>
                            <li class="u-mb-100">E.&nbsp;Verify whether the property inventory procedures are accurate and conduct random inspections to confirm the existence of assets.​​<br>
                            </li>
                            <li class="u-mb-100">F.&nbsp;Assess the appropriateness of the methods and operating procedures (not in
                                violation of laws and regulations) formulated by each unit and whether the competent
                                supervisors duly oversee to prevent violations from occurring and handle them properly.
                            </li>
                            <li class="u-mb-100">G.&nbsp;Verify whether the Company's policies and regulations are followed.</li>
                            <li class="u-mb-100">H.&nbsp;Disclose internal violations.</li>
                            <li class="u-mb-100">I.&nbsp;Review the self-inspection report of each unit and subsidiary of the Company.
                            </li>
                            <li>J.&nbsp;Other matters assigned by superiors.</li>
                        </ul>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">IV.&nbsp;Auditor's Responsibilities</h4>
                        <ul class="u-list-style--custom-level02 u-mb-000">
                            <li class="u-mb-100">A.&nbsp;Auditors are engaged in audit work following the annual audit plan approved by the Board of Directors and prepare reports on a case-by-case basis, detailing the audit findings and recommendations.​​ </li>
                            <li class="u-mb-100">B.&nbsp;When engaged in work, auditors shall be instructed by the audit manager. In case of doubt, they shall put forward handling opinions after obtaining explanation and thorough understanding.</li>
                            <li class="u-mb-100">C.&nbsp;When the audit manager assigning works to auditors, he or she should explain the work items, including the scope, note points, and selection criteria, and provide appropriate supervision.</li>
                            <li class="u-mb-100">D.&nbsp;In the course of their work, if auditors discover any misconduct of employees, they shall immediately report to the managerial officer or Chairman concurrently serving as a director apart from contacting their direct supervisors and shall not deal with it directly.</li>
                            <li class="u-mb-100">E.&nbsp;Auditors may access all files while engaged in their work, and the inspected unit shall not refuse or conceal them. However, highly confidential files shall be submitted to the managerial officer or Chairman concurrently serving as a director for approval before they can be accessed.<br>
                                If the handling staff of the inspected unit refuses to provide or adopts a non-cooperative attitude, the auditor may consult with the direct supervisor of the inspected unit to make corrections and report it to the higher level for approval if necessary.​​
                            </li>
                            <li class="u-mb-100">F.&nbsp;The audit report shall not be processed without the approval of the Chairman.​​</li>
                            <li>G.&nbsp;If auditors discover any major misconduct or likelihood of material impairment to the Company, they should immediately file a report and notify the Audit Committee.​​</li>
                        </ul>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">V.&nbsp;Actual Operation​​</h4>
                        <ul class="u-list-style--custom-level02 u-mb-000">
                            <li class="u-mb-100">A.&nbsp;The audit unit formulates an annual audit plan based on the risk assessment results at the end of each year. Also. management of acquisition or disposal of assets, derivative transactions, fund lending to others, endorsements or guarantees for others, control of material financial business activities such as the management of related party transactions, supervision and management of subsidiaries, management of Board meeting operations, management of applicable international accounting standards, accounting professional judgment procedures, the process of accounting policy and estimation changes, information security check, sales and collection cycle, procurement and payment cycle, the operation and management of the Remuneration Committee, etc. are listed as audit items in the annual audit plan each year.Upon approval of the Board of Directors, the annual audit scheme shall be implemented as planned.​​
                            </li>
                            <li class="u-mb-100">B.&nbsp;Auditors check the implementation status of each operation on the planned audit items, including whether the content of regulations is complete, the actual implementation status, the accuracy of data, and the timeliness and the results of work, to evaluate the integrity of their internal control and submit an audit report, stating the audit results and recommendations for improvement.​​
                            </li>
                            <li class="u-mb-100">C.&nbsp;File the audit plan for the next year before the end of each fiscal year and the implementation of the annual audit plan for the previous year within two months after the end of each fiscal year with the FSC via the internet information system in the prescribed format for recordation.​​</li>
                            <li class="u-mb-100">D.&nbsp;Within five months after the end of each fiscal year, the improvement of deficiencies and abnormalities in the internal control system found in the internal audit of the previous year shall be reported to the FSC via the internet information system in the prescribed format for recordation.​​</li>
                            <li>E.&nbsp;Urge all internal units and subsidiaries to conduct self-inspection at least once a year, then the audit unit will review the self-inspection report of each unit and subsidiary and write a written internal audit report together with the improvement of deficiencies and abnormalities in the internal control found by the audit unit. It is served as the main basis for the Board of Directors and President to evaluate the effectiveness of the overall internal control system and issue the Statement of Internal Control System.After the Statement of Internal Control System is approved by the Board of Directors and signed by Chairman and President, it is filed on the designated website of the FSC via the internet information and published in the annual report and prospectus.​​
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="u-py-100 u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">VI.&nbsp;Download</h4>
                        <div class="d-flex">
                            <div class="col-auto u-pb-100 u-p-000">
                                <a target="_blank"
                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black col-auto u-mb-000"
                                    href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%96%93%E9%97%9C%E4%BF%82%E8%A1%A8.pdf">
                                    <img src="../assets/img/regulation/file-word.svg" alt="" class="u-mr-050"
                                        style="width: 17px;">
                                    <span>Communication between Independent Directors and Internal Audit in 2021</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footerEn" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>