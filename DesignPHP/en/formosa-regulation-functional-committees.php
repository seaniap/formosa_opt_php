<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="headerEn" class="l-header"></header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">Functional Committees</h3>
                    </div>
                </div>
                <div class="u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">I.Download</h4>
                        <div class="row flex-column flex-sm-row align-items-baseline">
                            <div class="col-auto">
                                <a href="../download/Introduction to the Audit Committee.doc" target="_blank"
                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                    <img src="../assets/img/regulation/file-word.svg" alt="" class="u-mr-050" style="width: 17px;">
                                    <span>Introduction to the Remuneration Committee</span>
                                </a>
                            </div>
                            <div class="col-auto">
                                <a href="../download/The Remuneration Committee Charter.docx" target="_blank"
                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                    <img src="../assets/img/regulation/file-word.svg" alt="" class="u-mr-050" style="width: 17px;">
                                    <span>The Remuneration Committee Charter</span>
                                </a>
                            </div>
                            <div class="col-auto">
                                <a href="../download/Proposals and Resolutions of the Remuneration Committee in 2021.docx" target="_blank"
                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                    <img src="../assets/img/regulation/file-word.svg" alt="" class="u-mr-050" style="width: 17px;">
                                    <span>Proposals and Resolutions of the Remuneration Committee in 2021</span>
                                </a>
                            </div>
                            <div class="col-auto">
                                <a href="../download/Introduction to the Audit Committee.doc" target="_blank"
                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                    <img src="../assets/img/regulation/file-word.svg" alt="" class="u-mr-050" style="width: 17px;">
                                    <span>Introduction to the Audit Committee</span>
                                </a>
                            </div>
                            <div class="col-auto">
                                <a href="../download/The Audit Committee Charter.doc" target="_blank"
                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                    <img src="../assets/img/regulation/file-word.svg" alt="" class="u-mr-050" style="width: 17px;">
                                    <span>The Audit Committee Charter</span>
                                </a>
                            </div>
                            <div class="col-auto">
                                <a href="../download/Communication between Independent Directors and Internal Auditors and CPAs.docx" target="_blank"
                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                    <img src="../assets/img/regulation/file-word.svg" alt="" class="u-mr-050" style="width: 17px;">
                                    <span>Communication between Independent Directors and Internal Auditors and CPAs</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
                <!-- =============end 主要內容區 ============= -->
                <!-- ============= footer ============= -->
                <footer id="footerEn" class="l-footer"></footer>
                <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>