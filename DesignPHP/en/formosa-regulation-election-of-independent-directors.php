<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="headerEn" class="l-header"></header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <h3 class="c-title-center u-mb-125">Election of Independent Directors</h3>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <!-- <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">Election of
                            Independent Directors</p> -->
                        <p class="u-mb-000">Information Regarding the Selection of Independent Directors by the Company
                            According to the Qualifications Stipulated in the Securities and Exchange Act</p>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">I.&nbsp;Method of Nomination and Selection
                        </h4>
                        <p class="u-mb-000">As stipulated in Article 14-2, paragraph 1 of the Securities and Exchange
                            Act: “A company that has issued stock in accordance with this Act may appoint independent
                            directors in accordance with its articles of incorporation. The Competent Authority,
                            however, shall as necessary in view of the company's scale, shareholder structure, type of
                            operations, and other essential factors, require it to appoint independent directors, not
                            less than two in number and not less than one-fifth of the total number of
                            directors."Accordingly, the Company passed the amendments to the Company's Articles of
                            Incorporation at the Annual Shareholders' Meeting in 2006, adopted a candidate nomination
                            system for the election of directors, and added Article 18-1 to establish independent
                            directors according to the regulation of the Securities and Exchange Act.
                        </p>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">II.&nbsp;Nomination Process</h4>
                        <p>
                            In accordance with the provisions of Article 172-1, Article 192-1, and
                            Article 216-1 of the Company Act, the Company published at the Market Observation Post
                            System (MOPS) on April 6, 2021, that the "2021 Annual Shareholders' Meeting Received the
                            Slate of Director and Supervisor Candidates Nominated by Shareholders."Shareholders holding
                            more than 1% of the issued shares of the Company may submit a slate of director candidates
                            (including independent directors) to the Company in writing during the nomination period
                            from April 16 to 26, 2021.
                        </p>
                        <p class="u-mb-000">
                            At the expiration of the announcement period, only the Board of Directors nominated
                            Chung-Chi Wen, Yu-Ching Tsai, Jung-Hui Liang, and Meng-Jou Wu as independent director
                            candidates of the Company. On May 10, 2021, the Board of Directors passed the qualification
                            review of Chung-Chi Wen, Yu-Ching Tsai, Jung-Hui Liang, and Meng-Jou Wu.
                        </p>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">III.&nbsp;Candidate Information</h4>
                        <table class="table table-bordered l-table u-mb-000">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th style="min-width: 80px;">Name</th>
                                    <th>Academic background</th>
                                    <th>Experience</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="text-center l-table-vertical-align-middle">1</th>
                                    <td class="text-center l-table-vertical-align-middle">Chung-Chi Wen</td>
                                    <td>Master of Law, Graduate School of Law, Chinese Culture University</td>
                                    <td>Attorney of Wen Chung-Chi Law Firm</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="text-center l-table-vertical-align-middle">2</th>
                                    <td class="text-center l-table-vertical-align-middle">Yu-Ching Tsai</td>
                                    <td>Graduate School of Accounting, National Taiwan University</td>
                                    <td>Partner CPA of Everwell & Co., CPAs.</td>
                                </tr>
                                <tr>
                                    <th class="text-center l-table-vertical-align-middle">3</th>
                                    <td class="text-center l-table-vertical-align-middle">Jung-Hui Liang</td>
                                    <td>Ph.D. in Finance, Graduate School of Business Administration, National Taiwan
                                        University of Science and Technology</td>
                                    <td>Adjunct Professor, Department of Finance, Chinese Culture University</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="text-center l-table-vertical-align-middle">4</th>
                                    <td class="text-center l-table-vertical-align-middle">Meng-Jou Wu</td>
                                    <td>Master of Economics, School of Management, Michigan State University</td>
                                    <td>Attorney of Fach Law Firm</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100 u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">IV.&nbsp;Selection Process and Results</h4>
                        <p>When the Company re-elected directors at the Annual Shareholders' Meeting on July 27, 2021,
                            seven directors were elected, including four independent directors. The number of voting
                            rights for four independent directors is as follows : </p>
                        <table class="table table-bordered l-table u-mb-000">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Name of the candidate</th>
                                    <th>Number of votes won by elected director</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">1</td>
                                    <td class="text-center l-table-vertical-align-middle">Chung-Chi Wen</td>
                                    <td class="text-center l-table-vertical-align-middle">38,582,198</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td class="text-center l-table-vertical-align-middle">2</td>
                                    <td class="text-center l-table-vertical-align-middle">Yu-Ching Tsai</td>
                                    <td class="text-center l-table-vertical-align-middle">38,436,768</td>
                                </tr>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">3</td>
                                    <td class="text-center l-table-vertical-align-middle">Jung-Hui Liang</td>
                                    <td class="text-center l-table-vertical-align-middle">38,397,366</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td class="text-center l-table-vertical-align-middle">4</td>
                                    <td class="text-center l-table-vertical-align-middle">Meng-Jou Wu</td>
                                    <td class="text-center l-table-vertical-align-middle">38,573,058</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footerEn" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>