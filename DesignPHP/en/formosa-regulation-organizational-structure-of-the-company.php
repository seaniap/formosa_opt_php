<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網站描述</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" /> 
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="headerEn" class="l-header"></header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <h3 class="c-title-center u-mb-125">Organizational
                        Structure of the Company</h3>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">I.&nbsp;Organizational</h4>
                        <div class="row">
                            <div class="col-12 d-none d-md-block">
                                <img class="w-100" src="./../assets/img/regulation/organization-en-PC.svg" alt="">
                            </div>
                            <div class="col-12 d-md-none">
                                <img class="w-100" src="./../assets/img/regulation/organization-en-mobile.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-py-100 u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">II.&nbsp;Titles & Duties Of Supervisors</h4>
                        <table class="table table-bordered l-table">
                            <thead>
                                <tr>
                                    <th
                                        class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-bottom-black l-table-vertical-align-middle">
                                        Department</th>
                                    <th
                                        class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle">
                                        Supervisor's name</th>
                                    <th
                                        class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle">
                                        Title</th>
                                    <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle"
                                        style="min-width: 100px;">Main duties</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">Audit Office</td>
                                    <td class="text-center l-table-vertical-align-middle">Wei-Kai Tseng</td>
                                    <td class="text-center l-table-vertical-align-middle">Chief Auditor</td>
                                    <td class="l-table-vertical-align-middle">Responsible for the
                                        improvement of internal control and the implementation of internal audits.</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td class="text-center l-table-vertical-align-middle">Business Management Office
                                    </td>
                                    <td class="text-center l-table-vertical-align-middle">Chih-Ming Yeh</td>
                                    <td class="text-center l-table-vertical-align-middle">Vice President</td>
                                    <td class="l-table-vertical-align-middle">Assisting in branch operation planning and
                                        control management, commodity
                                        procurement, warehousing, and sales management.</td>
                                </tr>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">Marketing Office</td>
                                    <td class="text-center l-table-vertical-align-middle">Ji-Sheng Yang</td>
                                    <td class="text-center l-table-vertical-align-middle">Director</td>
                                    <td class="l-table-vertical-align-middle">Advertising planning and implementation,
                                        business district planning and field
                                        investigation, market survey, and business information collection.</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td class="text-center l-table-vertical-align-middle">Human Resources Office</td>
                                    <td class="text-center l-table-vertical-align-middle">Ke-Lun Chou</td>
                                    <td class="text-center l-table-vertical-align-middle">Chief Human Resources Officer
                                    </td>
                                    <td class="l-table-vertical-align-middle">Responsible for the management of
                                        personnel and vocational education.</td>
                                </tr>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">Administration Office</td>
                                    <td class="text-center l-table-vertical-align-middle">Cheng-Chung Lee</td>
                                    <td class="text-center l-table-vertical-align-middle">Associate Manager</td>
                                    <td class="l-table-vertical-align-middle">Responsible for the management of general
                                        affairs and administrative documents.
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td class="text-center l-table-vertical-align-middle">Finance and Accounting Office
                                    </td>
                                    <td class="text-center l-table-vertical-align-middle">Li-Hui Chang</td>
                                    <td class="text-center l-table-vertical-align-middle">Vice President</td>
                                    <td class="l-table-vertical-align-middle">Responsible for fund management, financial
                                        dispatch, cost and general accounting
                                        processing, preparation and analysis of accounting statements, and stock
                                        affairs.</td>
                                </tr>
                                <tr>
                                    <td class="text-center l-table-vertical-align-middle">IT Office</td>
                                    <td class="text-center l-table-vertical-align-middle">Shih-Huang Chen</td>
                                    <td class="text-center l-table-vertical-align-middle">Chief Information Officer</td>
                                    <td class="l-table-vertical-align-middle">Responsible for the planning,
                                        establishment, development, and maintenance of computer mainframes, networks,
                                        information systems, databases, application software, and information security.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footerEn" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>