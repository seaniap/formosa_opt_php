<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
        <!-- Start dateMobiscroll CSS-->
        <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
        <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="header" class="l-header"></header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section class="p-knowledge">
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">極智焦距定位儀</h3>
                        <p class="u-mb-125">
                            眼睛的構造很複雜，若因為眼睛焦點驗配位置偏差，會影響配戴眼鏡的舒適度及清晰度，所以配眼鏡這件事，不是只有量度數這麼簡單，一定要量身訂製！<br>
                            而您知道嗎?臉的形狀、耳朵高低、眼睛位置、頭部的傾斜角度以及鏡框戴在鼻樑上的位置，都必須經過精密量測後和鏡片相互匹配，才能擁有最精確舒適的視覺。因此全台灣最大眼鏡通路龍頭-寶島眼鏡特別引進配鏡最新科技Eye-ruler2極智焦距定位儀，希望提供消費者更專業且精確的智能配鏡體驗。
                        </p>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">8項配鏡數據測量</h4>
                        <p>
                            Eye-Ruler2極智焦距定位儀是最新一代智慧型驗配定位軟體，能直接用平板電腦進行鏡片與鏡框間之精準眼睛焦點定位的測量，精確量測鏡框在配戴者臉上契合度最好的位置，並在極短時間內、不需花費消費者太多時間，就能精算出所有鏡片製作需要的個人化驗配的8項配鏡參數。
                        </p>
                        <img src="assets/img/education/education_20.jpg" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">近方行為參數測量</h4>
                        <p>
                            Eye-Ruler2極智焦距定位儀能運用平板電腦進行閱讀行為模擬的量測，精算出每個人獨特的近用閱讀姿勢及閱讀習慣，塑造個人化的中近距離視野範圍。
                        </p>
                        <img src="assets/img/education/education_21.jpg" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="u-pt-100 u-pb-400">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">客製化鏡片推薦</h4>
                        <p>
                            身為漸進多焦點專家的寶島眼鏡，除了透過諮詢了解每個人的生活習慣外，最重要的是還會透過Eye-Ruler2極智焦距定位儀，在舒適的服務環境中，以精密專業的光學量測技術，取得消費者個人化數據及閱讀習慣、並配合五官位置精細調整，確認每個人的專屬視覺需求，推薦客製化的鏡片，讓您配戴起來更加舒適、清晰！
                        </p>
                        <img src="assets/img/education/education_22.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footer" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
        <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>