<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="header" class="l-header"></header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">股東會資料</h3>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <div class="c-accordion">
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent1">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">110年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon js-accordionExpended">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent1" class="c-accordion-content js-accordionExpended">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%96%93%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5_%E8%8B%B1%E6%96%87%E7%89%88.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年開會通知_英文版</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99_%E8%8B%B1%E6%96%87%E7%89%88.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年議事手冊及會議補充資料_英文版</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/110%E5%B9%B4%E5%8F%96%E5%BE%97%E6%88%96%E8%99%95%E5%88%86%E8%B3%87%E7%94%A2%E8%99%95%E7%90%86%E7%A8%8B%E5%BA%8F_%E8%A8%82%E5%AE%9A%E6%88%96%E4%BF%AE%E6%AD%A3.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年取得或處分資產處理程序_訂定或修正</span>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent2">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">109年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent2" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/109%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%96%93%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/109%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/109%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/109%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/109%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5_%E8%8B%B1%E6%96%87%E7%89%88.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年開會通知_英文版</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/109%E5%B9%B4%E6%9C%83%E8%AD%B0%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A_%E8%8B%B1%E6%96%87%E7%89%88.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年會議議事手冊_英文版</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/109%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent3">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">108年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent3" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/108%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%96%93%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/108%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/108%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/108%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/108%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/108%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/108%E5%B9%B4%E5%8F%96%E5%BE%97%E6%88%96%E8%99%95%E5%88%86%E8%B3%87%E7%94%A2%E8%99%95%E7%90%86%E7%A8%8B%E5%BA%8F%E4%BF%AE%E6%AD%A3%E6%A2%9D%E6%96%87%E5%B0%8D%E7%85%A7%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年取得或處分資產處理程序修正條文對照表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/108%E5%B9%B4%E8%B3%87%E9%87%91%E8%B2%B8%E8%88%87%E4%BB%96%E4%BA%BA%E6%88%96%E8%83%8C%E6%9B%B8%E4%BF%9D%E8%AD%89%E4%BD%9C%E6%A5%AD%E7%A8%8B%E5%BA%8F%E4%BF%AE%E6%AD%A3%E6%A2%9D%E6%96%87%E5%B0%8D%E7%85%A7%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年資金貸與他人或背書保證作業程序修正條文對照表</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent4">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">107年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent4" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/107%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%96%93%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/107%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/107%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/107%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/107%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/107%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent5">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">106年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent5" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/106%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%96%93%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/106%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/106%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/106%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/106%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/106%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent6">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">105年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent6" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%96%93%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent7">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">105年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent7" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%96%93%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/105%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent8">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">104年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent8" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/104%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年年報前十大股東相互關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/104%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/104%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/104%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/104%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/104%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent9">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">103年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent9" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/103%E5%B9%B4%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年年報前十大股東相互關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/103%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/103%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/103%E5%B9%B4%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/103%E5%B9%B4%E8%83%8C%E6%9B%B8%E4%BF%9D%E8%AD%89%E4%BD%9C%E6%A5%AD%E7%A8%8B%E5%BA%8F%E4%BF%AE%E6%AD%A3%E5%B0%8D%E7%85%A7%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年背書保證作業程序修正對照表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/103%E5%B9%B4%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/103%E5%B9%B4%E8%AD%B0%E4%BA%8B%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent10">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">102年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent10" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/102%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/102%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/102%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/102%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102年報前十大股東相互關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/102%E4%BA%8B%E8%AD%B0%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102事議手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/102%E8%B3%87%E9%87%91%E8%B2%B8%E8%88%87%E4%BB%96%E4%BA%BA%E7%A8%8B%E5%BA%8F.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102資金貸與他人程序</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/102%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102股東會年報</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent11">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">101年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent11" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/101%E9%96%8B%E6%9C%83%E9%80%9A%E7%9F%A5.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/101%E8%82%A1%E6%9D%B1%E6%9C%83%E8%AD%B0%E4%BA%8B%E9%8C%84.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/101%E8%82%A1%E6%9D%B1%E6%9C%83%E5%90%84%E9%A0%85%E8%AD%B0%E6%A1%88%E5%8F%83%E8%80%83%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/101%E5%B9%B4%E5%A0%B1%E5%89%8D%E5%8D%81%E5%A4%A7%E8%82%A1%E6%9D%B1%E7%9B%B8%E4%BA%92%E9%97%9C%E4%BF%82%E8%A1%A8.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101年報前十大股東相互關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/101%E5%8F%96%E5%BE%97%E6%88%96%E8%99%95%E5%88%86%E8%B3%87%E7%94%A2%E8%99%95%E7%90%86%E7%A8%8B%E5%BA%8F(%E8%A8%82%E5%AE%9A%E6%88%96%E4%BF%AE%E8%A8%82).pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101取得或處分資產處理程序(訂定或修訂)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/101%E8%82%A1%E6%9D%B1%E6%9C%83%E5%B9%B4%E5%A0%B1.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="https://www.formosa-optical.com.tw/downloads/101%E4%BA%8B%E8%AD%B0%E6%89%8B%E5%86%8A%E5%8F%8A%E6%9C%83%E8%AD%B0%E8%A3%9C%E5%85%85%E8%B3%87%E6%96%99.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 u-text-black w-100 px-2 my-2">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101事議手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->

                        </div>
                    </div>
                </div>
                <!-- START 頁籤 -->
                <div class="u-py-100 u-pb-400">
                    <div class="container">
                        <nav>
                            <ul class="c-pagination justify-content-center align-items-center u-mb-000">
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_to_left"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_left"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item active"><a href="#">1</a></li>
                                <li class="c-pagination-item"><a href="#">2</a></li>
                                <li class="c-pagination-item"><a href="#">3</a></li>
                                <li class="c-pagination-item"><a href="#">4</a></li>
                                <li class="c-pagination-item"><a href="#">5</a></li>
                                <li class="c-pagination-item">...</li>
                                <li class="c-pagination-item"><a href="#">10</a></li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_right"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_to_right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- END 頁籤 -->
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer id="footer" class="l-footer"></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>