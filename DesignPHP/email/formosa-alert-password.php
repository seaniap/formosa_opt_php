<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <title>密碼通知</title>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <!--[if gte mso 9]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
</head>

<body class="body cLayout--bgColor" style="background-color:#ffffff; margin:0;width:100%;">
    <table class="layout__wrapper" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr class="layout__row">
            <td class="layout__column cLayout--bgColor" align="center" width="100%"
                style="padding-left:10px;padding-right:10px;padding-top:40px;padding-bottom:40px" bgcolor="#ffffff">
                <!--[if !mso]><!---->
                <div style="margin:0 auto;width:100%;max-width:640px;">
                    <!-- <![endif]-->
                    <!--[if mso | IE]>
                  <table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="640" style="margin:0 auto;width:100%;max-width:640px;">
                     <tr>
                        <td>
                           <![endif]-->
                    <!-- Block: Start Image -->
                    <table class="block-inner" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="block-inner__content cBlock--spacingLR " align="left" valign="top" width="100%"
                                bgcolor="#002154"
                                style="padding-left:32px;padding-right:32px;padding-top:16px;padding-bottom:16px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="left" valign="top" width="100%">
                                            <a href="../index.html" target="_blank"
                                                style="padding:0px;text-decoration:none;display:inline-block">
                                                <img class="img_block" border="0" width="240"
                                                    src="../assets/img/logo.png"
                                                    style="display:block;max-width:576px" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- Block: End Image -->
                    <!-- Block: Start Text -->
                    <table class="block-inner" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="block-inner__content cBlock--spacingLR " align="left" valign="top" width="100%"
                                bgcolor="#ffffff"
                                style="padding-left:32px;padding-right:32px;padding-top:16px;padding-bottom:16px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="left">
                                            <p style="font-size: 14px;">※此信件為系統自動發出信件，請勿直接回覆，感謝您的配合。謝謝！</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <p
                                                style="color:#333333;font-size:18px;font-weight:bold;font-family:'PingFang TC','微軟正黑體','Microsoft JhengHei','Helvetica Neue',Helvetica,Arial,sans-serif;padding:0;margin:0;line-height:1.4">
                                                親愛的會員 您好：
                                            </p>
                                            <p>這封帳號/密碼補發信件是由寶島眼鏡官網發出，用以處理您忘記帳號/密碼，當收到本「帳號/密碼補發信件」，密碼是以亂數取代，請用此密碼登入後變更您的新密碼，請勿直接回信。</p>
                                                <p>
                                                    您的帳號：airborne<br/>
                                                    您的密碼：4DBE1A
                                                </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <hr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="margin-bottom: 0;">如果您有任何問題，請您至寶島眼鏡官網信箱來信給我們。</p>
                                            <p style="margin-top: 0;">(<a href="mailto:formosa_service@ms.formosa-opt.com.tw" target="_blank" style="padding:0px;display:inline-block">formosa_service@ms.formosa-opt.com.tw</a>)</p>
                                            <p>寶島眼鏡官網 敬上</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- Block: End Text -->
                    <!--[if mso | IE]>
                        </td>
                     </tr>
                  </table>
                  <![endif]-->
                    <!--[if !mso]><!---->
                </div>
                <!-- <![endif]-->
            </td>
        </tr>
    </table>
</body>

</html>