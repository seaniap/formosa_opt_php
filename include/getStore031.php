<?php
require_once('../Connections/MySQL.php');

$template="store01.php";
$templateS="store01";

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_content01 = 1000;
$pageNum_content01 = 0;
if (isset($_GET['pageNum_content01'])) {
  $pageNum_content01 = $_GET['pageNum_content01'];
}
$startRow_content01 = $pageNum_content01 * $maxRows_content01;

$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = '5' AND Template='$template' and Cate03='1' ",$Table_ID);
if($_GET['area']<>""){
$query_content01=$query_content01."AND h10='".substr($_GET['area'],0,10)."'" ;}

if($_GET['zip']<>""){
$query_content01=$query_content01."AND h09='".substr($_GET['zip'],0,10)."'" ;}

//排序
$query_content01=$query_content01." order by CONVERT(`h01` using big5) asc ";

$query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
//echo $query_limit_content01 ;
$content01 = mysqli_query($MySQL,$query_limit_content01) or die(mysqli_error($MySQL));
//$row_content01 = mysqli_fetch_assoc($content01);

  $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);

$totalPages_content01 = ceil($totalRows_content01/$maxRows_content01)-1;

$queryString_content01 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_content01") == false && 
        stristr($param, "totalRows_content01") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);


?>



<div class="col-12">
          <div class="default-table store-table">
            <table class="table table-rwd">
              <thead>
                <tr class="store-th">
                  <th><i class="icon-shop mr-1"></i>門市據點</th>
                  <th><i class="icon-location1 mr-1"></i>地址</th>
                  <th><i class="icon-time mr-1"></i>營業時間</th>
                  <th><i class="icon-message mr-1"></i>聯絡資訊</th>
                </tr>
              </thead>
              <tbody>
<?php if($totalRows_content01==0){ ?>            
        <tr>
                  <td data-th="門市據點" colspan="4">抱歉，目前此區尚無經銷據點，請選擇其他鄰近區域重新查詢，謝謝。</td>
                  </tr>     
            
<?php } ?>                
   <?php while($row_content01 = mysqli_fetch_assoc($content01)){ ?>                 
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                       <?php echo $row_content01['h01']; ?>
                      </div>
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="https://www.google.com.tw/maps/place/<?php echo $row_content01['h10'].$row_content01['h11'].$row_content01['h12']; ?>" target="_blank">
                        <?php echo $row_content01['h10'].$row_content01['h11'].$row_content01['h12']; ?>
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                  <!--  <div class="table-rwd-content">
                      <div>08:00-20:00</div>
                      <div>(周日公休)</div>
                    </div>
                    -->
                      <?php echo str_replace("(","<br>(",$row_content01['h04']); ?>
                  </td>
                  <td data-th="Line & Email">
                    <div class="table-rwd-content">
                      <?php if($row_content01['h03']<>"" and $row_content01['h02']=="1" ){ ?>
                      <div>
                        <a href="tel:+886-<?php echo $row_content01['h03']; ?>">
                          <i class="icon-phone mr-1"></i> <?php echo $row_content01['h03']; ?>
                        </a>
                      </div>
                      <?php }　 ?>
                     <?php if($row_content01['h06']<>""){ ?>  
                      <div>
                        <a  data-toggle="modal" data-target="#line-modal" onclick="document.getElementById('linepic').src='UploadImages/<? echo $row_content01['p01'];  ?>'">
                          <i class="icon-line-bordered mr-1"></i><?php echo $row_content01['h06']; ?>
                        </a>
                      </div>
                       <?php }　 ?>
                     <?php if($row_content01['h05']<>""){ ?> 
                      <div>
                        <a href="mailto:<?php echo $row_content01['h05']; ?>">
                          <i class="icon-mail mr-1"></i> <?php echo $row_content01['h05']; ?>
                        </a>
                      </div>
                         <?php }　 ?>
                    </div>
                  </td>
                </tr>
                
    <?php } ?>            
                
              
              </tbody>
            </table>
          </div>
<!--          
          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
              <li><a href="#" class="active">1 </a></li>
              <li><a href="#">2 </a>
              </li>
              <li><a href="#">3 </a>
              </li>
              <li><a href="#">4 </a>
              </li>
              <li><a href="#"><i class="fas fa-arrow-right"></i></a></li>
            </ul>
          </nav>
-->          
        </div>