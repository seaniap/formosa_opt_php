
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
<meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<?php require_once '../Connections/MySQL.php';?>
<?php

$query_content01 = sprintf("SELECT * FROM GoWeb_BigTable WHERE KeyID = %s  ", GetSQLValueString($_GET['KeyID'], "text"));
$content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);

?>
<body>
<div class="container">
    <p class="u-text-gray-500">
                <span><?php echo substr($row_content01['d01'], 0, 10); ?></span>
                ~            <span><?php echo substr($row_content01['d02'], 0, 10); ?></span>
            </p>
    <h4 class="c-title-underline u-font-18 u-md-font-24"><?php echo $row_content01['h01']; ?></h4>
</div>

<div class="container formosaResetUl">
        <!-- 預留編輯器空間 -->
        <div class="row">
            <div class="col">
            <?php echo $row_content01['c01']; ?>
            <?php echo $row_content01['c02']; ?>
            <?php echo $row_content01['c03']; ?>
            </div>
        </div>
        <!-- 預留編輯器空間 -->
    </div>
</body>
</html>