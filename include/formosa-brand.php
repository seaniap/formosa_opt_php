
        <main class="wrapper p-brand">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">鏡片品牌</h3>
                        <p class="text-center u-mb-125">寶島眼鏡擁有多樣鏡片品牌，符合您的各種需求</p>
                    </div>
                </div>
                <div class="u-pb-300">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6276/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=NEOS"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/NEOS.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>尼歐<br/>NEOS</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6277/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=ESSEL"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/ESSEL.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>愛視爾<br/>ESSEL</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6278/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=ASAHI"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/ASAHI-LITE.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>朝日<br/>ASAHI-LITE</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6279/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=NIKON"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/NIKON.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>尼康<br/>NIKON</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6280/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=ZIESS"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/ZIESS.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>蔡司<br/>ZIESS</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6281/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=TOKAI"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/TOKAI.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>東海<br/>TOKAI</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6282/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=HOYA"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/HOYA.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>豪雅<br/>HOYA</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6283/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=VARILUX"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/varilux.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>萬里路<br/>VARILUX</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6284/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=CRIZAL"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/CRIZAL.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>鑽潔<br/>CRIZAL</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6285/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=Xion"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/X'ion.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>里昂<br/>X'ion</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6286/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=ESSILOR"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/ESSILOR.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>依視路<br/>ESSILOR</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6288/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=optovision"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/optovision.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>歐特<br/>optovision</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6289/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=SUMMIT"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/SUMMIT.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>高峰<br/>SUMMIT</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6290/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=ROYAL"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/ROYAL.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>蘿亞<br/>ROYAL</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6291/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=ILT"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/ILT.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>愛爾帝<br/>ILT</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <div class="c-card c-card-link c-card-verA">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/GOLDEN.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>高登<br/>GOLDEN</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <div class="c-card c-card-link c-card-verA">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/APOLLO.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>阿波羅<br/>APOLLO</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6293/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=OUTLOOK"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/OUTLOOK.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>趨勢<br/>OUTLOOK</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <a href="https://www.eyesmart.com.tw/link/6294/?utm_source=FormosaOfficialWebsite&utm_medium=product&utm_campaign=lens&utm_term=LIGHTLITE"  target="_blank" class="c-card c-card-link c-card-verA p-brand-card">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/LIGHT LITE.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>徠特<br/>LIGHT LITE</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <div class="c-card c-card-link c-card-verA">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/MAIDO.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>馬麗歐<br/>MAIDO</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <div class="c-card c-card-link c-card-verA">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/CANNEX.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>可妮斯<br/>CANNEX</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-lg-2 col-md-3 col-4">
                                <div class="c-card c-card-link c-card-verA">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio u-mb-100">
                                            <img src="assets/img/brand/FARADAY.jpg" alt="" class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body-layout u-px-050">
                                            <p>法拉蒂<br/>FARADAY</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>
            </section>
        </main>
        