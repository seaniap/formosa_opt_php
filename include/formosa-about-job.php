<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">企業徵才</h3>
            </div>
        </div>
        <div class="u-pb-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">一、人才招募</h4>
                <ul class="mb-0">
                    <li class="mb-3">工作內容：門市基本事務 / 顧客接待與導購、商品銷售 / 專業驗光配鏡 / 顧客眼鏡售後服務
                    </li>
                    <li class="mb-3">休假制度：排休制</li>
                    <li class="mb-3">獎金制度：業績獎金、年終獎金、銷售獎金、商品獎金、介紹獎金</li>
                    <li class="mb-3">人才培育：定期技術檢定、各式培訓課程、輔導考照</li>
                    <li class="mb-3">進修補助：視光科系進修學費補助、視光科系專屬獎學金、無息學費貸款</li>
                </ul>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">二、薪資待遇</h4>
                <p class="u-mb-000">驗光人員：薪資待遇(含獎金)</p>
                <ul class="u-list-style--disc">
                    <li>驗光師 35,000 ~ 50,000元</li>
                    <li>驗光生 33,000 ~ 45,000元</li>
                    <li>視光科系應屆畢業者 31,000 ~ 42,000元</li>
                </ul>
                <ul class="u-mb-000">
                    <li class="mb-3">休假福利：陪產假、產假、年假、育嬰假</li>
                    <li class="mb-3">保險福利：勞保、健保、退休金提撥、意外險、員工及眷屬團保</li>
                    <li class="mb-3">禮金福利：三節禮金、生日禮金</li>
                    <li class="mb-3">其他福利：遠程員工住宿津貼、眷補津貼、績優店、績優員工旅遊、結婚、生育、住院慰問金、兵役、喪葬、子女獎學金等補助
                    </li>
                </ul>
            </div>
        </div>
        <div class="u-pt-100 u-pb-400">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">三、應徵方式</h4>
                <a href="https://www.104.com.tw/company/at5q9kg?jobsource=jolist_c_relevance" class="u-mr-050"
                    target="_blank" title="104人力銀行"><img src="assets/img/about-job/104logo.png" width="100px"
                        height="100px" alt="104"></a>
                <a href="https://www.1111.com.tw/corp/3225300/" target="_blank" title="104人力銀行"><img
                        src="assets/img/about-job/1111logo.png" width="100px" height="100px" alt="104"></a>
            </div>
        </div>
    </section>
</main>