<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">公司治理</h3>
            </div>
        </div>
        <!-- regulation tabs -->
        <?php include('formosa_regulation_tabs.php')?>
        <!-- end regulation tabs -->
        <div class="u-pb-100">
            <div class="container">
                <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">財務規章</p>
                <!-- <p class="u-mb-000">本公司依照證交法所訂資格條件選任獨立董事之相關訊息</p> -->
            </div>
        </div>
        <div class="u-pb-300">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">一、檔案下載</h4>
                <div class="row flex-column flex-sm-row align-items-baseline">
                    <div class="col-auto">
                        <a href="download/pdf/Financial_regulations/取得或處分資產處理程序.pdf" target="_blank" class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                            <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050" style="width: 17px;">
                            <span>取得或處分資產處理程序</span>
                        </a>
                    </div>
                    <div class="col-auto">
                        <a href="download/pdf/Financial_regulations/背書保證作業程序.pdf" target="_blank" class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                            <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050" style="width: 17px;">
                            <span>背書保證作業程序</span>
                        </a>
                    </div>
                    <div class="col-auto">
                        <a href="download/pdf/Financial_regulations/資金貸與他人作業程序.pdf" target="_blank" class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                            <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050" style="width: 17px;">
                            <span>資金貸與他人作業程序</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>