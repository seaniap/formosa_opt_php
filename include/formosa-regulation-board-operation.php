<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">公司治理</h3>
            </div>
        </div>
        <!-- regulation tabs -->
        <?php include('formosa_regulation_tabs.php')?>
        <!-- end regulation tabs -->
        <div class="u-pb-100">
            <div class="container">
                <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">董事會</p>
                <p>
                    寶島光學科技股份有限公司(以下稱本公司)董事會為最高治理機構，且董事會具有高階管理者的選任、提名及評量績效之職責，並負責制定公司企業社會責任、企業公民、及永續發展策略。
                </p>
                <p>本公司董事會是由十一位擁有豐富經營經驗或產業經驗的董事所組成，其中四位為獨立董事。董事會至少每季召開一次常會。</p>
                <p>
                    本公司董事會是一個「認真、有能力、獨立」的機構，在蔡國洲董事長治理理念領導下，董事會肩負監督公司守法、財務透明、即時揭露重要訊息、沒有內部貪污等責任；且為健全監督功能及強化管理機能，本公司董事會已建立了各式組織和管道，例如：薪酬委員會、審計委員會及內部稽核等，並每季定期聽取經營團隊的報告，議題亦包括經濟、環保及企業社會責任（包括相關風險及機會評估、遵循道德規範及誠信經營）等。
                </p>
                <p>
                    本公司經營階層與董事會之間維持著順暢良好的溝通，且經營階層也必須對董事會提出公司經營策略，並透過董事會與會成員討論，評估這些策略成功的可能性，且經常檢視策略的進展，期能在需要時敦促經營團隊作調整，致力於創造全體股東最高利益為優先。
                </p>
                <p class="u-mb-000">
                    本公司本屆「董事」任期於2021年7月26日屆滿，依公司法第195條規定應全面改選新任之董事。本公司於2021年7月27日股東會，依本公司章程規定選出董事十一席（含獨立董事四席）任期三年，股東會後即行就任，自2021年7月27日起至2024年7月26日止，連選連任。本公司董事選舉採候選人提名制度。
                </p>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">一、於2021年7月27日股東會通過董事名單如下：</h4>
                <div class="l-tablesSroller-wrapper">
                    <table class="table table-bordered l-table u-mb-000">
                        <thead>
                            <tr>
                                <th>職稱</th>
                                <th style="width: 180px;">姓名</th>
                                <th style="width: 180px;">學歷</th>
                                <th>經歷</th>
                                <th>現職</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center white-space-nowrap">董事</td>
                                <td>捷富國際股份有限公司代表人：蔡國洲</td>
                                <td>私立東海大學管理碩士</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>寶島光學科技(股)公司董事長</li>
                                        <li>Prosper Link International Limited董事長</li>
                                        <li>Haichang International Limited董事長</li>
                                        <li>Ginko International Co., LTD董事長</li>
                                        <li>海昌隱形眼鏡有限公司董事長</li>
                                        <li>江蘇海倫隱形眼鏡有限公司董事長</li>
                                        <li>江蘇東方光學有限公司董事長</li>
                                        <li>New Path International Co., Ltd.董事長</li>
                                        <li>金可眼鏡實業(股)公司董事長</li>
                                        <li>捷富國際(股)公司董事長</li>
                                        <li>寄生(股)公司董事長</li>
                                        <li>永勝光學(股)公司董事長</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>寶島光學科技(股)公司董事長</li>
                                        <li>Prosper Link International Limited董事長</li>
                                        <li>Haichang International Limited董事長</li>
                                        <li>Ginko International Co., LTD董事長</li>
                                        <li>海昌隱形眼鏡有限公司董事長</li>
                                        <li>江蘇海倫隱形眼鏡有限公司董事長</li>
                                        <li>江蘇東方光學有限公司董事長</li>
                                        <li>New Path International Co., Ltd.董事長</li>
                                        <li>金可眼鏡實業(股)公司董事長</li>
                                        <li>捷富國際(股)公司董事長</li>
                                        <li>寄生(股)公司董事長</li>
                                        <li>永勝光學(股)公司董事長</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">董事</td>
                                <td>蔡國平</td>
                                <td>美國賓州州立大學農業經濟博士</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>寶島光學科技(股)公司副董事長</li>
                                        <li>寶聯光學(股)公司董事長</li>
                                        <li>米蘭眼鏡精品企業(股)公司董事長</li>
                                        <li>寶崴光學(股)公司董事長</li>
                                        <li>寶祥光學(股)公司董事長</li>
                                        <li>金可集團策略投資事業群執行長</li>
                                        <li>元捷國際(股)公司董事長</li>
                                        <li>Silvercoast Investments LTD.董事長</li>
                                        <li>捷富國際(股)公司監察人</li>
                                        <li>寄生(股)公司董事</li>
                                        <li>江蘇東方光學有限公司監察人</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>寶島光學科技(股)公司副董事長</li>
                                        <li>寶聯光學(股)公司董事長</li>
                                        <li>米蘭眼鏡精品企業(股)公司董事長</li>
                                        <li>寶崴光學(股)公司董事長</li>
                                        <li>寶祥光學(股)公司董事長</li>
                                        <li>元捷國際(股)公司董事長</li>
                                        <li>Silvercoast Investments LTD.董事長</li>
                                        <li>捷富國際(股)公司監察人</li>
                                        <li>寄生(股)公司董事</li>
                                        <li>江蘇東方光學有限公司監察人</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">董事</td>
                                <td>陳劉惠鈺</td>
                                <td>銘傳商專</td>
                                <td>寶島投資開發(股)公司董事長</td>
                                <td>寶島投資開發(股)公司董事長</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">董事</td>
                                <td>捷富國際股份有限公司代表人：蔡宜珊</td>
                                <td>歐洲時尚學院米蘭校區品牌管理碩士</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>寶島光學科技(股)公司總經理</li>
                                        <li>寶崴光學(股)公司董事</li>
                                        <li>米蘭眼鏡精品企業(股)公司董事</li>
                                        <li>元捷國際(股)公司董事</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>寶島光學科技(股)公司總經理</li>
                                        <li>寶崴光學(股)公司董事</li>
                                        <li>米蘭眼鏡精品企業(股)公司董事</li>
                                        <li>元捷國際(股)公司董事</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">董事</td>
                                <td>寶島電子股份有限公司代表人：闕子江</td>
                                <td>中國文化大學經濟系</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>寶島鐘錶(股)公司副總經理</li>
                                        <li>寶島光學科技(股)公司監察人</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>寶島鐘錶(股)公司副總經理</li>
                                        <li>寶島光學科技(股)公司監察人</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">董事</td>
                                <td>智偉投資股份有限公司代表人：張智偉</td>
                                <td>美國亞利桑納大學資訊科學系</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>Tension Design LLC.網業工程師</li>
                                        <li>寶島光學科技(股)公司監察人</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>智偉投資(股)公司董事長</li>
                                        <li>寶島光學科技(股)公司監察人</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">董事</td>
                                <td>姚琇碧</td>
                                <td>密蘇里大學哥倫比亞分校企業管理學碩士</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>永勝光學(股)公司策略投資部經理</li>
                                        <li>寶島光學科技(股)公司監察人</li>
                                        <li>裕佳昇股份有限公司董事</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>永勝光學(股)公司策略投資部經理</li>
                                        <li>寶島光學科技(股)公司監察人</li>
                                        <li>裕佳昇股份有限公司董事</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">獨立<br>董事</td>
                                <td>文鍾奇</td>
                                <td>文化大學法律學研究所法學碩士</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>文鍾奇律師事務所律師</li>
                                        <li>寶島光學科技(股)公司獨立董事暨薪酬委員</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>文鍾奇律師事務所律師</li>
                                        <li>寶島光學科技(股)公司獨立董事暨薪酬委員</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">獨立<br>董事</td>
                                <td>吳孟柔</td>
                                <td>密西根州立大學管理學院經濟碩士</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>法赫法律事務所律師</li>
                                        <li>寶島光學科技(股)公司獨立董事暨薪酬委員</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>法赫法律事務所律師</li>
                                        <li>寶島光學科技(股)公司獨立董事暨薪酬委員</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">獨立<br>董事</td>
                                <td>蔡育菁</td>
                                <td>台灣大學會計學研究所</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>永華聯合會計師事務所合夥會計師</li>
                                        <li>耀登科技股份有限公司獨立董事暨薪酬委員</li>
                                        <li>台灣精銳科技(股)公司獨立董事暨薪酬委員</li>
                                        <li>寶島光學科技(股)公司獨立董事暨薪酬委員</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>永華聯合會計師事務所合夥會計師</li>
                                        <li>耀登科技股份有限公司獨立董事暨薪酬委員</li>
                                        <li>台灣精銳科技(股)公司獨立董事暨薪酬委員</li>
                                        <li>寶島光學科技(股)公司獨立董事暨薪酬委員</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">獨立<br>董事</td>
                                <td>梁榮輝</td>
                                <td>國立台灣科技大學企管所財金博士</td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>景文科技大學監察人</li>
                                        <li>中國文化大學財金系兼任教授</li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="u-mb-000">
                                        <li>景文科技大學監察人</li>
                                        <li>中國文化大學財金系兼任教授</li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">二、董事會成員多元化情形如下：</h4>
                <div class="l-tablesSroller-wrapper">
                    <table class="table table-bordered l-table text-center">
                        <thead>
                            <tr>
                                <th class="l-table-vertical-align-middle">&nbsp;</th>
                                <th class="l-table-vertical-align-middle">性別</th>
                                <th class="l-table-vertical-align-middle">營運判斷<br>能力</th>
                                <th class="l-table-vertical-align-middle">會計及財務<br>分析能力</th>
                                <th class="l-table-vertical-align-middle">經營管理<br>能力</th>
                                <th class="l-table-vertical-align-middle">危機處理<br>能力</th>
                                <th class="l-table-vertical-align-middle">產業知識</th>
                                <th class="l-table-vertical-align-middle">國際市場觀</th>
                                <th class="l-table-vertical-align-middle">領導能力</th>
                                <th class="l-table-vertical-align-middle">決策能力</th>
                                <th class="l-table-vertical-align-middle">法律</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="white-space-nowrap">蔡國洲</td>
                                <td>男</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="white-space-nowrap">蔡國平</td>
                                <td>男</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="white-space-nowrap">陳劉惠鈺</td>
                                <td>女</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="white-space-nowrap">蔡宜珊</td>
                                <td>女</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="white-space-nowrap">蔡育菁</td>
                                <td>女</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="white-space-nowrap">文鍾奇</td>
                                <td>男</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                                <td>V</td>
                                <td>V</td>
                            </tr>
                            <tr>
                                <td class="white-space-nowrap">吳孟柔</td>
                                <td>男</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                                <td>V</td>
                                <td>V</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="white-space-nowrap">闕子江</td>
                                <td>男</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="white-space-nowrap">張智偉</td>
                                <td>男</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="white-space-nowrap">姚琇碧</td>
                                <td>女</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="white-space-nowrap">梁榮輝</td>
                                <td>男</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>V</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p class="u-mb-000">本公司具員工身份之董事占比為18%，獨立董事占比為36%、女性董事占比為36%，3位獨立董事任期年資在4~6年，1位獨立董事任期年資在9年以上 ;
                    1位董事年齡在70歲以上，4位董事年齡在60~69歲，6位在60歲以下。本公司注重董事會組成之性別平等，女性董事比率超過30%以上。</p>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">三、董事會運作情形資訊：</h4>
                <p>110年度董事會截至最近一次開會(12月23日)，共開會8次(A)。<br>董事出列席情形如下：</p>
                <p class="text-right">110年12月23日</p>
                <div class="l-tablesSroller-wrapper">
                    <table class="table table-bordered l-table">
                        <thead>
                            <tr>
                                <th class="l-table-vertical-align-middle">職稱</th>
                                <th class="l-table-vertical-align-middle" style="min-width: 120px;">姓名(註1)</th>
                                <th class="l-table-vertical-align-middle" style="min-width: 120px;">
                                    實際出(列)<br>席次數(Ｂ)</th>
                                <th class="l-table-vertical-align-middle" style="min-width: 120px;">委託出席次數</th>
                                <th class="l-table-vertical-align-middle" style="min-width: 120px;">
                                    實際出(列)席率(%)<br>【Ｂ/Ａ】(註2)</th>
                                <th class="l-table-vertical-align-middle" style="min-width: 120px;">備註</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center white-space-nowrap">董事長</td>
                                <td class="text-center">捷富國際(股)公司<br>代表人：蔡國洲</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日連任</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">副董事長</td>
                                <td class="text-center">蔡國平</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日連任</td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">董事</td>
                                <td class="text-center">陳志賢</td>
                                <td class="text-center">2</td>
                                <td class="text-center">0</td>
                                <td class="text-center">67</td>
                                <td class="text-center">舊任</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">董事</td>
                                <td class="text-center">陳劉惠鈺</td>
                                <td class="text-center">5</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日新任</td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">董事</td>
                                <td class="text-center">捷富國際(股)公司<br>代表人：蔡宜珊</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日連任</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">董事</td>
                                <td class="text-center">寶島電子(股)公司<br>代表人：闕子江</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日新任<br>(原監察人)</td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">董事</td>
                                <td class="text-center">智偉投資(股)公司<br>代表人：張智偉</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日新任<br>(原監察人)</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">董事</td>
                                <td class="text-center">姚琇碧</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日新任<br>(原監察人)</td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">獨立<br>董事</td>
                                <td class="text-center">文鍾奇</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日連任</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">獨立<br>董事</td>
                                <td class="text-center">蔡育菁</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日連任</td>
                            </tr>
                            <tr>
                                <td class="text-center white-space-nowrap">獨立<br>董事</td>
                                <td class="text-center">吳孟柔</td>
                                <td class="text-center">8</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日連任</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center white-space-nowrap">獨立<br>董事</td>
                                <td class="text-center">梁榮輝</td>
                                <td class="text-center">5</td>
                                <td class="text-center">0</td>
                                <td class="text-center">100</td>
                                <td class="text-center">110年7月27日新任</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p>110年各次董事會獨立董事出席狀況(◎：親自出席 ☆：委託出席 *：未出席)</p>
                <div class="l-tablesSroller-wrapper">
                    <table class="table table-bordered l-table u-mb-000">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>3月24日</th>
                                <th>5月10日</th>
                                <th>7月6日</th>
                                <th>7月27日</th>
                                <th>8月11日</th>
                                <th>11月10日</th>
                                <th>11月26日</th>
                                <th>12月23日</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center l-table-vertical-align-middle">文鍾奇</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center l-table-vertical-align-middle">蔡育菁</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                            </tr>
                            <tr>
                                <td class="text-center l-table-vertical-align-middle">吳孟柔</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="text-center l-table-vertical-align-middle">梁榮輝</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                                <td class="text-center">◎</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="u-py-100 u-pb-300">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">四、董事會成員之接班規劃</h4>
                <p>公司在規劃董事會接班計劃中，接班人須依本公司訂定之「董事會成員多元化政策」與「董事選任程序」內容要求，具備執行職務之多元能力，且價值觀與人格特質與公司之經營理念相符。</p>
                <p>本公司目前董事共 11 名，其中 2
                    名董事同時身兼集團高階管理階層，未來本公司董事會之整體組成架構及成員經歷背景將延續目前架構，並依公司發展做適當調整。另獨立董事依法需具商務、法務、財務、會計或公司業務所需之工作經驗，並具備五年以上之工作經驗，且依法公開發行公司之獨立董事兼任其他公開發行公司獨立董事不得逾三家。目前國內這部分專業人士之供給不虞匱乏，故獨立董事之接班規劃，本公司仍規劃來自學術界及產業界。
                </p>
                <p>透過集團內之高階管理階層與學術界及產業界之組合，使董事會成員組成符合本公司董事會多元化政策。
                </p>
            </div>
        </div>
    </section>
</main>