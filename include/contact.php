<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-tw" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-tw" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-tw">
<!--<![endif]-->

<head>
  <title>聯絡我們 | 三菱重工空調</title>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta property="og:image" content="assets/img/share_1200x630.jpg" />
  <!-- Favicon -->
  <link rel="shortcut icon" href="../../favicon.ico">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="../../assets/plugins/bootstrap4/css/bootstrap.min.css">
  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="../../assets/plugins/fontawesome5/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../../assets/plugins/icomoon/style.css">
  <!-- CSS Customization -->
  <link rel="stylesheet" href="../../assets/css/main.css">
  <script language="javascript" src="../js/checkform.js"></script>
<script language="javascript" src="../js/Zip.js"></script>
<?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?> 
<? 
$_GET['Template']="5754858805ab3a3d7b8231";
require_once('../lib/MSurveyHeader.php'); ?>
  <header id="header">
  </header>
  <nav class="breadcrumb-nav" aria-label="breadcrumb">
    <div class="container">
      <ol class="breadcrumb">
        <li>
        <a href="../../index.html">
            <i class="fas fa-home"></i>
          </a>
        </li>
        <li class="active">
          <a href="">聯絡我們</a>
        </li>
      </ol>
    </div>
  </nav>
  <div class="wrapper">
    <section class="container">
      <div class="d-title">
        <h3>聯絡我們</h3>
      </div>
      <div class="location">
        <div class="row">
          <div class="col-md-6 px-5 d-flex justify-content-center align-items-center">
            <img src="./assets/img/contact/pro-logo.svg" alt="" class="img-fluid w-100">
          </div>
          <div class="col-md-6 px-5 py-2">
            <ul>
              <li>
                <h5>台北總公司</h5>
                <span>24889 新北市五股區五權六路39號 (新北產業園區)</span>
              </li>
              <li>
                <h5>台中分公司</h5>
                <span>40256 台中市南區大慶街一段62號</span>
              </li>
              <li>
                <h5>嘉義分公司</h5>
                <span>60066 嘉義市東區彌陀路169號</span>
              </li>
              <li>
                <h5>高雄分公司</h5>
                <span>83044 高雄市鳳山區文化西路17-8號</span>
              </li>
            </ul>
            <p class="backtalk pl-2"><i class="fas fa-phone"></i> 消費者專線：0800-3007-66</p>
          </div>
        </div>
      </div>
      <div class="bg-dr-gray my-5 p-5">
        <div class="block-description">
          <h3 class="y-title">填寫表單</h3>
          <p>我們所有的努力，是為了讓您享受更美好的生活與服務！誠摯歡迎您惠予寶貴的批評、建議與需求，請您填寫正確聯絡資料，我們將盡快與您聯繫！</p>
        </div>
       <form method="post" name="form1" action="MSurvey.php" enctype="multipart/form-data">
          <div class="form-row">
            <div class="col-md-6">
              <label for="">姓名*</label>
              <div class="input-group mb-2">
                <div class="input-group-text">
                  <i class="fas fa-user"></i>
                </div>
                <input type="text" class="form-control" name="h01" id="h01" placeholder="">
              </div>
            </div>
            <div class="col-md-6">
              <label for="">聯絡電話</label>
              <div class="input-group mb-2">
                <div class="input-group-text">
                  <i class="fas fa-phone"></i>
                </div>
                <input type="text" class="form-control" name="h02" id="h02" placeholder="">
              </div>
            </div>
            <div class="col-12">
              <label for="">Email*</label>
              <div class="input-group mb-2">
                <div class="input-group-text">
                  <i class="fas fa-envelope"></i>
                </div>
                <input type="text" class="form-control" name="h03" id="h03" placeholder="">
              </div>
            </div>
            <div class="col-12">
              <label for="">聯絡地址*</label>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="select-box">
                       
                       <input name="h09" type="hidden" size="3" id="h09" />
          <select name="h10" size="1" id="h10" onchange="Buildkey1(this.options[this.options.selectedIndex].value);" class="form-control" >
            
            <option value="">請選擇縣市...</option>
            <option value="臺北市">臺北市</option>
            <option value="基隆市">基隆市</option>
            <option value="新北市">新北市</option>
            <option value="宜蘭縣">宜蘭縣</option>
            <option value="新竹市">新竹市</option>
            <option value="新竹縣">新竹縣</option>
            <option value="桃園市">桃園市</option>
            <option value="苗栗縣">苗栗縣</option>
            <option value="臺中市">臺中市</option>
            
            <option value="彰化縣">彰化縣</option>
            <option value="南投縣">南投縣</option>
            <option value="嘉義市">嘉義市</option>
            <option value="嘉義縣">嘉義縣</option>
            <option value="雲林縣">雲林縣</option>
            <option value="臺南市">臺南市</option>
            
            <option value="高雄市">高雄市</option>
            
            <option value="屏東縣">屏東縣</option>
            <option value="臺東縣">臺東縣</option>
            <option value="花蓮縣">花蓮縣</option>
            <option value="澎湖縣">澎湖縣</option>
            <option value="金門縣">金門縣</option>
            <option value="連江縣">連江縣</option>
            <option value="南海諸島">南海諸島</option>
           
            </select>
                      
                      
                      
                      <i></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="select-box">
            
                       <select name="h11" id="h11" size="1" onchange="document.form1.h09.value=this.options[this.options.selectedIndex].value.substring(0,3);" onfocus="Buildkey1(document.form1.h10.options[document.form1.h10.options.selectedIndex].value);" class="form-control" >
            <option value="">請選擇區域..</option>
            </select>
                      <i></i>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-text">
                      <i class="fas fa-home"></i>
                    </div>
                    <input name="h12" type="text" size="40" maxlength="30" id="h12" value="" class="form-control" />
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="form-group">
                <label for="">詢問項目*</label>
                <div class="select-box">
                  <select class="form-control" id="h08" name="h08">
                    <option selected disabled value="">請選擇詢問項目</option>
                    <option value="產品資訊">產品資訊</option>
                    <option value="專案規劃">專案規劃</option>
                    <option value="優惠活動">優惠活動</option>
                    <option value="售後服務">售後服務</option>
                    <option value="其他問題">其他問題</option>
                  </select>
                  <i></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <label for="">設備類型</label>
              <div class="form-group">
                <div class="select-box">
                  <select class="form-control" name="h16" id="h16" >
                    <option selected disabled>請選擇類型</option>
                    <option value="家用空調/一對一">家用空調/一對一</option>
                    <option value="家用空調/多聯式">家用空調/多聯式</option>
                    <option value="商用空調">商用空調</option>
                    <option value="VRF空調">VRF空調</option>
                    <option value="全屋空氣活化機">全屋空氣活化機</option>
                    <option value="其他產品/空氣清淨機">其他產品/空氣清淨機</option>
                   
                  </select>
                  <i></i>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <label for="">設備型號</label>
              <input type="text" class="form-control" name="h21" id="h21"  placeholder="">
            </div>
            <div class="col-12">
              <div class="form-group">
                <label for="exampleFormControlTextarea1">訊息內容*</label>
                <textarea class="form-control" id="h22" name="h22" rows="8"></textarea>
              </div>
            </div>
          </div>
          <div class="d-flex justify-content-center">
            <div class="col-md-3 px-0">
              <button type="button" class="btn btn-red btn-block" onclick="check_post(document.form1);">確認送出</button>
            </div>
          </div>
  <input type="hidden" name="KeyID" value="<? echo uniqid(mt_rand()); ?>">
  <input type="hidden" name="h30" value="新增">
  <input type="hidden" name="MKeyID" value="">
  <input type="hidden" name="Group" value="聯絡我們">
  <input type="hidden" name="Name" value="5754858805ab3a3d7b8231">
  <input type="hidden" name="goto" value="../../contact-final.html">
  <input type="hidden" name="CreatedTime" value="<?php echo  $row_Member['CreatedTime']; ?>" >
  <input type="hidden" name="Action" value="ins" >
  <input type="hidden" name="MM_update" value="form1">


</form>
      </div>
    </section>
    
  </div><footer id="footer"></footer>
  <!-- /container -->
  <!-- JS Global Compulsory -->
  <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
  <script type="text/javascript" src="../../assets/plugins/jquery/jquery.min.js"></script>
  <script type="text/javascript" src="../../assets/plugins/popper.min.js"></script>
  <script type="text/javascript" src="../../assets/plugins/bootstrap4/js/bootstrap.js"></script>
  <!-- JS Customization -->
  <script type="text/javascript" src="../../assets/js/main.js"></script>
  <!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>

</html>