<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">重大訊息</h3>
            </div>
        </div>
        <div class="u-pt-100 u-pb-400">
            <div class="container">
                <!-- <h4 class="c-title-underline u-font-16 u-md-font-22">相關連結</h4> -->
                <div class="row">
                    <div class="col-md-6 col-xl-4 u-mb-100 u-mb-md-000">
                        <img src="assets/img/investors/investors-01.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 col-xl-8">
                        <p>「本公司依「臺灣證券交易所股份有限公司對上市公司重大訊息查證暨公開處理程序」規定， 將不定期重大訊息即時揭露於公開資訊觀測站。」</p>
                        <div class="row align-items-center">
                            <div class="col-auto u-mb-100">
                                <a href="https://mops.twse.com.tw/mops/web/index" target="_blank" class="c-btn c-btn--contained c-btn-gray-200 u-text-black border border-secondary px-3">公開資訊觀測站</a>
                            </div>
                            <a class="list-inline-item u-text-red-formosa-02 u-mb-000 u-font-14">並請輸入寶島科或寶島公司代號(5312)</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>