<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">會員專區</h3>
            </div>
        </div>        
        <!-- member static tabs -->
        <?php include('formosa-member-static_tabs.php')?>
        <!-- end member static tabs -->
        <div class="u-py-100">
                    <div class="container">
                        <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">寶聯光學股份有限公司 隱私保護與Cookie使用政策</p> 
                        <p class="u-text-gray-800 u-mb-000">
                            保護您個人資料的隱私是寶聯光學股份有限公司的責任。<br><br>
                            為了尊重並保護您個人的隱私權，寶聯光學制定了隱私權保護與Cookie使用政策，期望您能知道有關您的權利，進而安心的使用寶聯光學股份有限公司(以下簡稱本公司)為您量身訂做之各項服務。在此，謹依中華民國個人資料保護法第8條規定告知以下事項，並特向您說明本公司之隱私權保護承諾：
                        </p>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">一、個人資料蒐集目的</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            為提昇服務品質及加強個人化服務，本公司個人資料蒐集的目的在於進行會員交易、會員個人及促銷訊息發送、會員調查等各項會員相關服務與管理、其他異常服務處理、行銷、會員行動配送、服務、網路廣告及統計調查與交叉比對分析等運用。<br><br>
                            為保障您的權益，當您使用信用卡在合於前述目的以下列個人資料蒐集方式進行蒐集時，我們會將您所提供的個人資料，與第三方機構進行資訊安全身分檢核，以落實交易安全政策，防止您的信用卡遭到他人利用或利益損失。
                        </p>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">二、個人資料蒐集方式</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            本公司將於您在寶島眼鏡官方網站、寶島眼鏡APP、寶島眼鏡EYESmart會員行動配送網站或寶島眼鏡會員【申請書】填寫基本個人資料，完成本公司申請流程，完成寶島眼鏡會員時，蒐集、處理您向本公司提供之基本個人資料，並透過您使用本服務時，取得您操作偏好或其他相關資料等，作為改善本公司服務流程之參考。本公司僅於符合前述蒐集目的範圍內，處理及利用您所提供之前述個人資料。若您不同意本公司於透過您使用本服務時，取得您操作偏好或其他相關資料等，您應立即停止使用本服務；當您繼續使用本服務時，視為同意依照前述說明辦理。
                        </p>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">三、蒐集之個人資料類別</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            透過上述資料蒐集方式，本公司可能會蒐集到消費者個人資料包括：
                        </p>
                        <ul class="u-list-style--custom-level03 u-mb-000">
                            <li>1.&nbsp;&nbsp;C001辨識個人者：包括但不限於姓名、職稱、住址、工作地址、以前地址、住家電話號碼、行動電話、即時通帳號、網路平臺申請之帳號、通訊及戶籍地址、相片、指紋、電子郵遞地址、電子簽章、憑證卡序號、憑證序號、提供網路身分認證或申辦查詢服務之紀錄、使用者密碼、網際網路協定IP位址、Cookie及裝置唯一識別碼其他任何可辨識資料本人者等。
                            </li>
                            <li>2.&nbsp;&nbsp;C002辨識財務者：包括但不限於金融機構帳戶之號碼與姓名、信用卡或簽帳卡之號碼、保險單號碼、個人之其他號碼或帳戶等。</li>
                            <li>3.&nbsp;&nbsp;C003政府資料中之辨識者：包括但不限於身分證統一編號、統一證號、稅籍編號、保險憑證號碼、殘障手冊號碼、退休證之號碼、證照號碼、護照號碼等。</li>
                            <li>4.&nbsp;&nbsp;C011個人描述：包括但不限於年齡、性別、出生年月日、出生地、國籍、聲音等。</li>
                            <li>5.&nbsp;&nbsp;C023家庭其他成員之細節：包括但不限於子女、受扶養人、家庭其他成員或親屬、父母、同居人及旅居國外及大陸人民親屬等。</li>
                            <li>6.&nbsp;&nbsp;C031住家及設施：包括但不限於住所地址、設備之種類、所有或承租、住用之期間、租金或稅率及其他花費在房屋上之支出、房屋之種類、價值及所有人之姓名等。</li>
                            <li>7.&nbsp;&nbsp;C033 移民情形：例如：護照、工作許可文件、居留證明文件、住居或旅行限制、入境之條件及其他相關細節等。</li>
                            <li>8.&nbsp;&nbsp;C034旅行及其他遷徙細節：包括但不限於過去之遷徙、旅行細節、外國護照、居留證明文件及工作證等相關細節。</li>
                            <li>9.&nbsp;&nbsp;C035 休閒活動及興趣：包括但不限於嗜好、運動及其他興趣等。</li>
                            <li>10.C036 生活格調：例如：使用消費品之種類及服務之細節、個人或家庭之消費模式等。</li>
                            <li>11.C038 職業：包括但不限於學校校長、民意代表或其他各種職業等。</li>
                            <li>12.C051 學校記錄：例如：大學、專科或其他學校等。</li>
                            <li>13.C061 現行之受雇情形：例如：僱主、工作職稱、工作描述、等級、受僱日期、工時、工作地點、產業特性、受僱之條件及期間、與現行僱主有關之以前責任與經驗等。</li>
                            <li>14.C094 賠償：包括但不限於受請求賠償之細節、數額等。</li>
                            <li>15.C111 健康紀錄：包括但不限於醫療報告、治療與診斷紀錄、檢驗結果、身心障礙種類、等級、有效期間、身心障礙手冊證號及聯絡人等。</li>
                            <li>16.C132 未分類之資料：包括但不限於無法歸類之信件、檔案、報告、錄音或電子郵件等。</li>
                        </ul>
                        <br>
                        <p class="u-text-gray-800 u-mb-000">
                            若您不能提供相關服務範圍所需之個人資料，將有可能無法完成部份您所需之服務及交易。
                        </p>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">四、個人資料利用期間、地區、對象及方式</h4>
                        <ul class="u-list-style--custom-level03 u-mb-000">
                            <li>1.&nbsp;&nbsp;期間：上述個人資料之利用期間係指依照蒐集之特定目的、相關法令(例如：台灣民法、個人資料保護法等)所定、因執行業務所須之必要保存期間(較法規規定期間長者)或依個別契約所定之保存年限為利用存續期間。
                            </li>
                            <li>2.&nbsp;&nbsp;地區：個人資料將用於本公司及寶島眼鏡位於台灣各門市及所有網站，以便提供相關服務。</li>
                            <li>3.&nbsp;&nbsp;對象及方式：</li>
                            <li>4.&nbsp;&nbsp;使用本公司之會員行動配送服務以及各類會員服務，將用於下列之用途：</li>
                            <li>5.&nbsp;&nbsp;進行會員行動配送及其他相關資料之建立。</li>
                            <li>6.&nbsp;&nbsp;金融交易及授權：會員使用行動配送或寶島眼鏡EYE+Pay行動支付所提供之財務相關資訊，將於交易過程(如信用卡授權、轉帳)時提交給金融機構以完成交易。</li>
                            <li>7.&nbsp;&nbsp;電子郵件、App通知與簡訊服務：本公司將利用會員所填寫之電子郵件帳號或手機號碼傳送會員訊息及各項提醒、配送訂單相關等資訊。</li>
                            <li>8.&nbsp;&nbsp;由本公司及本公司之簽約商、代理商、附屬公司、集團與合作夥伴提供服務，包括但不限於：統計分析、交叉比對、簡訊、活動及商品訊息推播、郵件、電子郵件、問卷、電話行銷、電話服務、抽獎、一般銷售行為、會員行動配送、社群活動，VIP會員服務等各類活動等，以及為提供服務所為之資訊調查、交換、統計與分析。
                            </li>
                            <li>9.&nbsp;&nbsp;介紹與行銷產品及廣告，如本公司及與本公司有特約合作關係之第三人及本公司之簽約商、代理商、附屬公司、集團與合作夥伴所提供產品和服務。</li>
                            <li>10.透過您所提供之個人資料，進行電腦處理、跨境傳輸必要資訊予本公司人員、代理人員、本公司之簽約商、代理商、附屬公司、集團等，以利提供產品和服務。本公司將盡力確保本公司員工、代理人員、簽約商、代理商、附屬公司、集團及相關服務合作夥伴等遵守隱私政策條款。
                            </li>
                            <li>11.當您透過寶島眼鏡官方網站、寶島眼鏡APP、寶島眼鏡EYESmart會員行動配送網站、，購買非本公司主要銷售之相關產品(包含但不限為EYE嚴選商品專區)或會員點數線上轉點，因其可能為透過其他合作夥伴交易平台，故您所輸入之個人資料或財務相關資料將有可能為合作業者蒐集，以提供後續相關服務使用。
                            </li>
                        </ul>
                        <br>
                        <p class="u-text-gray-800 u-mb-000">
                            我們保證不會將個人資料揭露於與本服務無關之第三人或上述目的以外之用途。但如係為配合司法單位、相關職權機關依職務需要之調查或使用目的時，不在此限。
                        </p>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">五、會員個人資料之權利</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            旅客交付本公司個人資料者，依個人資料保護法第3條之規定得行使以下權利:
                        </p>
                        <ul class="u-list-style--custom-level02 u-mb-000">
                            <li>1.&nbsp;查詢或請求閱覽。</li>
                            <li>2.&nbsp;請求製給複製本。</li>
                            <li>3.&nbsp;請求補充或更正。</li>
                            <li>4.&nbsp;請求停止蒐集、處理或利用。</li>
                            <li>5.&nbsp;請求限制處理。</li>
                            <li>6.&nbsp;限制自動決策。</li>
                            <li>7.&nbsp;請求刪除。但若個人資料處理係屬必要而依相關個資法令，該等資料不適用於刪除權之行使時，本公司可能因此無法進行作業及回覆。</li>
                            <li>8.&nbsp;請求資料攜出。</li>
                        </ul>
                        <br>
                        <p class="u-text-gray-800 u-mb-000">
                            您可以透過寶島眼鏡客服電話(0800-251-257)或寶島眼鏡會員信箱(EYE-CATCH@ms.formosa-opt.com.tw)，使用上述權利。當您的身分通過驗證，我們會立即處理您提出的請求。如要求刪除個人資料，將會由專人提供會員卡註銷申請表。申請表填寫完畢後，請將申請表及應檢附之資料如身分證正反面影本提交給指定窗口。<br><br>
                            基於合法以及服務提供需求，亦或相關法令規定，會員資料有其必要的保存期間需求。<br>
                            我們需保留資料以提供您所需要的會員服務或是之後針對該服務您後續提出客訴抱怨或意見的調查，並將可提升您未來使用本公司相關服務的使用經驗。<br>
                            如果您所提供的個人資料不正確或不完整(如使用暱稱)，致本公司無法確認會員之真實身分，本公司將無法配合會員隱私權主張。
                        </p>
                    </div>
                </div>
                <div class="u-pt-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">六、Cookies的運用</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            Cookies是伺服器端為了區別使用者的不同喜好，並提供個人化功能之服務，經由瀏覽器寫入使用者硬碟的一些簡短資訊。大多數的網站可能會透過Cookies提供使用者相關功能服務。若您選擇不同意Cookies的存取，您可以透過瀏覽器的設定選擇拒絕接受Cookies，但在您拒絕Cookie存取後，將無法正常使用本公司網站。<br><br>
                            為保護您的隱私權，本公司將不會揭露從Cookie取得的資料。本公司將僅依據以下目的及情況，在您的瀏覽器中寫入並讀取Cookies:
                        </p>
                        <ul class="u-list-style--custom-level02">
                            <li>1.&nbsp;為提供更佳、更個人化交易與各類服務，以方便您參與個人化的互動活動。</li>
                            <li>2.&nbsp;為了解使用者網頁瀏覽的習性，做為提昇服務的參考。</li>
                            <li>3.&nbsp;欲取消Cookies運用請參考各瀏覽器說明連結:</li>
                            <li>4.&nbsp;Internet Explorer™-<a
                                    href="https://support.microsoft.com/zh-tw/help/17442/windows-internet-explorer-delete-manage-cookies"
                                    target="_blank">https://support.microsoft.com/zh-tw/help/17442/windows-internet-explorer-delete-manage-cookies</a>
                            </li>
                            <li>5.&nbsp;Chrome™-<a href="https://support.google.com/chrome/answer/95647?hl=zh-Hant&hlrm=en"
                                    target="_blank">https://support.google.com/chrome/answer/95647?hl=zh-Hant&hlrm=en</a>
                            </li>
                        </ul>
                        <br>
                        <p class="u-text-gray-800 u-mb-000">
                            我們所使用的Cookie包含：
                        </p>
                        <ul class="u-list-style--custom-level02">
                            <li>1.&nbsp;系統基本運作所需之 Cookie–為網站運作所需要的基本cookie，讓您可以順利瀏覽我們的網站並使用網站上的功能服務。此類cookie不會收集您任何的個人資料。</li>
                            <li>2.&nbsp;功能cookie–主要用於記憶您在網站上的設定，以改善並提供您個人化的功能服務。</li>
                            <li>3.&nbsp;效能cookie–透過紀錄使用者操作經驗，幫助我們改善網站的設計效能。透過此類Cookie，我們可以了解到網站的哪些頁面或服務功能是最多人使用，以及使用者可能遇到的系統錯誤，並進行修正。
                            </li>
                            <li>4.&nbsp;廣告行銷 Cookie
                                -為適當提供您最適當的本公司最新消息與產品資訊，透過此類cookie，可以讓我們與我們所合作廣告商，提供您感興趣的廣告內容。使您可以在本公司所經營或其他網站看到我們所提供的廣告內容。
                            </li>
                        </ul>
                        <br>
                        <p class="u-text-gray-800 u-mb-000">第三方Cookie的運用：<br>
                            本公司在網站上亦設有第三方追蹤碼，經由瀏覽器寫入使用者硬碟的一些簡短資訊以讀取Cookies，並作以下用途:
                        </p>
                        <ul class="u-list-style--custom-level02">
                            <li>1.&nbsp;確定及追蹤由第三方網站網路廣告連結至本網站的訪客量</li>
                            <li>2.&nbsp;追蹤本公司網站的使用情況</li>
                            <li>3.&nbsp;投放使用者所搜尋產品之促銷資訊或廣告。</li>
                            <li>4.&nbsp;欲取消Cookies運用請參考各瀏覽器說明：
                                <ul class="u-list-style--custom-level02">
                                    <li>◦&nbsp;Internet Explorer™-<a
                                            href="https://support.microsoft.com/zh-tw/help/17442/windows-internet-explorer-delete-manage-cookies"
                                            target="_blank">
                                            https://support.microsoft.com/zh-tw/help/17442/windows-internet-explorer-delete-manage-cookies
                                        </a>
                                    </li>
                                    <li>◦&nbsp;Chrome™-<a
                                            href="https://support.google.com/chrome/answer/95647?hl=zh-Hant&hlrm=en"
                                            target="_blank">https://support.google.com/chrome/answer/95647?hl=zh-Hant&hlrm=en
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <br>
                        <p class="u-text-gray-800 u-mb-000">
                            本公司與Google、LINE、Facebook
                            和Appier有合作關係，在本公司嚴格保密的條件下，其得透過第三方數據(cookies)獲取不含您個人身分的資料，以並提供相關行銷訊息，並設立規範確保適當之行銷。因此，Google、LINE、Facebook
                            和Appier可根據您於本公司及特定網頁上的瀏覽行為，選擇為您呈現自特定第三方網站提供的客製化廣告及相關訊息。<br><br>
                            管理您的Cookie與瀏覽器設定：<br>
                            大多數個人電腦或行動裝置的瀏覽器均有設定自動接受網站之Cookie設定。如果您不接受本公司所運用的Cookie並且希望關閉Cookie，您可以透過所使用的瀏覽器更改Cookie設定。透過您瀏覽器「協助」功能，您可以了解如何調整Cookie設定。<br>
                            如果您選擇不接受或阻擋本公司的Cookie，可能會導致您完全無法使用或僅能使用本公司網站的部分功能。
                        </p>
                    </div>
                </div>
                <div class="u-pt-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">七、IP位址的運用</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            網際網路協定位址（縮寫為IP位址），是由網際網路服務供應商（ISP）自動分配給使用者電腦的標識號碼。此IP位址於使用者到訪本公司網站及行動通訊服務時，會將到訪時間及到訪頁面連同記錄於伺服器日誌檔中。本公司會利用訪客的IP位址以評估網站及行動通訊服務的服務水準，並以此管理、維護本網站及行動通訊服務。本公司亦可使用IP位址進行商品行銷與廣告或阻擋任何未能遵守服務條款的訪客到訪。
                        </p>
                    </div>
                </div>
                <div class="u-pt-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">八、電子郵件的傳送</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            本公司將於前述「蒐集之個人資料類別」中取得會員電子郵件帳號，透過電子郵件帳號提供相關資訊，包含：交易、服務、優惠事項、會員權益與優惠通知(如會員點數到期通知…等各項訊息)，旅客將可以在該電子郵件上看到本公司所註明的發送標誌。該發送標誌為”網路信標(Web
                            Beacons)”的電子影像檔案，透過網路信標可以讓我們了解到寄送給您的信件是否有被開啟、信件打開的次數以及信件上連結的點選次數；當您刪除信件的同時，信件內含的網路信標也將一併被刪除。
                        </p>
                    </div>
                </div>
                <div class="u-pt-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">九、資料剖析與自動化決策</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            本公司及關係企業或服務供應商的行銷活動，針對使用者透過本公司服務所進行的消費來擷取特定資訊。其中包括交易的總金額、系統產生的交易代號、消費產品以及IP位址等。本公司將使用這些資訊來調整行銷活動的有效性與防止交易風險的產生，並可能將這些資訊傳送給本公司的合作夥伴，以用於類似的用途。並藉由第三方服務供應商對蒐集到的資訊整理並做出報告為提供更佳、更個人化交易與各類服務。
                        </p>
                        <br>
                        <ul class="u-list-style--custom-level02">
                            <li>•&nbsp;為了解會員的習性，做為投放相關行銷活動資訊的參考。</li>
                            <li>•&nbsp;欲取消行銷/促銷訊息遞送，您可以前往寶島眼鏡APP會員專區內進行會員資料修改。</li>
                        </ul>
                    </div>
                </div>
                <div class="u-pt-100 u-pb-400">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">十、安全規範及隱私保護與Cookie使用政策修訂</h4>
                        <p class="u-text-gray-800 u-mb-000">
                            請妥善保管您的任何個人資料、附加服務與各類網路服務，不要將任何個人資料提供給任何人。在您完成會員配送、會員聯絡等相關資訊變動等程序後，務必記得登出，若您是與他人共享電腦或使用公共電腦，切記要關閉瀏覽器視窗，以防止他人蒐集並使用您的個人資料。<br><br>
                            交易及服務資料加密<br>
                            本公司的網路交易及個人私密資料均透過高科技的加密安全傳輸方式保護，採用業界標準之256 位元SSL (Secure Sockets Layer)
                            加密技術，於透過網路傳輸前均預先進行資料加密，以防止資料被截取及盜用。<br><br>
                            隱私保護與Cookie使用政策修訂<br>
                            本公司可逕行修訂本隱私保護與Cookie使用政策內容，新的內容將會呈現於此網頁。當本公司在使用個人資料的規定上作出大幅修改時，為維護會員權益，本公司會在網頁上張貼告示，以讓會員知曉相關權益事項。本公司亦將定期檢視隱私保護與Cookie使用政策以符合相關法律規範。<br><br>
                            準據法暨管轄法院<br>
                            因本網站造成之爭訟均以中華民國法律為準據法，並以台灣台北地方法院為第一審管轄法院。
                        </p>
                    </div>
                </div>
    </section>
</main>