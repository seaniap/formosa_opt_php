<!-- START TAB -->
<div class="js-tabPosition"></div>
<div class="u-py-100 u-bg-white js-tabFixed">
    <div class="container">
    <div class="position-relative">
        <!-- <button type="button" class="v-tabBtnSlick-prev"><i class="fal fa-angle-left"></i></button> -->
        <div class="c-tab-verE v-tabBtnSlick">
            <div class="c-tab-container">
                <a href="index.php?Page=B-5-1"
                    class="c-tab-linkE <?php if($_GET['Page']=="B-5-1"){ echo 'active';}?>">會員資料修改</a>
            </div>
            <div class="c-tab-container">
                <a href="index.php?Page=B-6"
                    class="c-tab-linkE <?php if($_GET['Page']=="B-6"){ echo 'active';}?>">點數查詢</a>
            </div>
            <div class="c-tab-container">
                <a href="index.php?Page=B-7"
                    class="c-tab-linkE <?php if($_GET['Page']=="B-7"){ echo 'active';}?>">會員權益</a>
            </div>
            <div class="c-tab-container">
                <a href="index.php?Page=B-8"
                    class="c-tab-linkE <?php if($_GET['Page']=="B-8"){ echo 'active';}?>">會員Q&A</a>
            </div>
            <div class="c-tab-container">
                <a href="index.php?Page=B-9"
                    class="c-tab-linkE <?php if($_GET['Page']=="B-9"){ echo 'active';}?>">隱私權條款</a>
            </div>
            <div class="c-tab-container">
                <a href="index.php?Page=B-0"
                    class="c-tab-linkE <?php if($_GET['Page']=="B-0"){ echo 'active';}?>">四大保固</a>
            </div>
        </div>
        <!-- <button type="button" class="v-tabBtnSlick-next"><i class="fal fa-angle-right"></i></button> -->
    </div>
    </div>
</div>
<!-- END TAB -->
<form name="formsearch" id="formsearch" method="post">
    <div class="l-form">
        <div class="form-row justify-content-end u-pb-075">
            <div class="col-md-6 u-mb-100 u-mb-md-000">
                <div class="form-row flex-nowrap align-items-center">
                    <div class="col">
                        <select name="Cate01" id="Cate01" class="l-form-field l-form-select u-bg-gray-200"
                            onchange="formsearch.submit();">
                            <option value="" <?php if($_GET['Cate01']==""){echo "selected";} ?>>全部顯示</option>
                            <option value="加入會員" <?php if($_GET['Cate01']=="加入會員"){echo "selected";} ?>>加入會員</option>
                            <option value="會員申請規則" <?php if($_GET['Cate01']=="會員申請規則"){echo "selected";} ?>>會員申請規則
                            </option>
                            <option value="會員點數規則" <?php if($_GET['Cate01']=="會員點數規則"){echo "selected";} ?>>會員點數規則
                            </option>
                            <option value="會員等級規則" <?php if($_GET['Cate01']=="會員等級規則"){echo "selected";} ?>>會員等級規則
                            </option>
                            <option value="會員退換貨作業" <?php if($_GET['Cate01']=="會員退換貨作業"){echo "selected";} ?>>會員退換貨作業
                            </option>
                            <option value="會員活動相關" <?php if($_GET['Cate01']=="會員活動相關"){echo "selected";} ?>>會員活動相關
                            </option>
                            <option value="寶島眼鏡APP操作問題" <?php if($_GET['Cate01']=="寶島眼鏡APP操作問題"){echo "selected";} ?>>
                                寶島眼鏡APP操作問題</option>
                            <option value="EYESmart配送相關" <?php if($_GET['Cate01']=="EYESmart配送相關"){echo "selected";} ?>>
                                EYESmart配送相關</option>
                            <option value="寶島眼鏡EYE+Pay支付相關"
                                <?php if($_GET['Cate01']=="寶島眼鏡EYE+Pay支付相關"){echo "selected";} ?>>寶島眼鏡EYE+Pay支付相關
                            </option>
                            <option value="會員終止申請" <?php if($_GET['Cate01']=="會員終止申請"){echo "selected";} ?>>會員終止申請
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-row flex-nowrap align-items-center">
                    <div class="col">
                        <input name="search" type="text" placeholder="請輸入關鍵字" class="l-form-field"
                            value="<?php echo $_GET['search'] ?>">
                    </div>
                    <div class="col-auto">
                        <button type="button" class="c-btn c-btn--contained c-btn-blue-highlight"
                            onClick="formsearch.submit();"><i class="fal fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="Page" value="B-8" />
</form>