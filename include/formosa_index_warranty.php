<section id="warranty" class="u-bg-gray-02 u-pb-400 u-pt-200">
				<div class="container">
					<h3 class="c-title-center u-mb-125">四大保固</h3>
					<div class="row no-gutters">
						<div class="col-lg-3">
							<div class="d-flex flex-lg-column">
								<a href="index.php?Page=B-0#warranty01Id" class="p-index-warranty-wrapper">
									<div class="p-index-warranty-content p-3">
										<img src="assets/img/index/icons/09.svg" alt="" class="img-fluid u-mb-100">
										<h4 class="u-font-22 u-text-blue-500 u-font-weight-700">品質保固</h4>
									</div>
								</a>
								<div class="p-index-warranty-frame">
									<div class="p-index-warranty-content">
										<img data-src="assets/img/index/warranty_01.jpg" alt="" class="img-fluid lozad">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="d-flex flex-lg-column">
								<div class="p-index-warranty-wrapper order-1">
									<a href="index.php?Page=B-0#warranty02Id" class="p-index-warranty-content p-3">
										<img src="assets/img/index/icons/10.svg" alt="" class="img-fluid u-mb-100">
										<h4 class="u-font-22 u-text-blue-500 u-font-weight-700">技術保固</h4>
									</a>
								</div>
								<div class="p-index-warranty-frame">
									<div class="p-index-warranty-content">
										<img data-src="assets/img/index/warranty_02.jpg" alt="" class="img-fluid lozad">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="d-flex flex-lg-column">
								<a href="index.php?Page=B-0#warranty03Id" class="p-index-warranty-wrapper">
									<div class="p-index-warranty-content p-3">
										<img src="assets/img/index/icons/11.svg" alt="" class="img-fluid u-mb-100">
										<h4 class="u-font-22 u-text-blue-500 u-font-weight-700">滿意保固</h4>
									</div>
								</a>
								<div class="p-index-warranty-frame">
									<div class="p-index-warranty-content pic03">
										<img data-src="assets/img/index/warranty_03.jpg" alt="" class="img-fluid lozad">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="d-flex flex-lg-column">
								<a href="index.php?Page=B-0#warranty04Id" class="p-index-warranty-wrapper order-1">
									<div class="p-index-warranty-content p-3">
										<img src="assets/img/index/icons/12.svg" alt="" class="img-fluid u-mb-100">
										<h4 class="u-font-22 u-text-blue-500 u-font-weight-700">服務保固</h4>
									</div>
								</a>
								<div class="p-index-warranty-frame">
									<div class="p-index-warranty-content">
										<img data-src="assets/img/index/warranty_04.jpg" alt="" class="img-fluid lozad">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>