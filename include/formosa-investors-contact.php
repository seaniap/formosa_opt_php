<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">股代聯絡資訊</h3>
            </div>
        </div>
        <div class="u-pb-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">股務代理聯絡資訊</h4>
                <ul class="mb-0">
                    <li class="mb-3">過戶機構：元富證券股份有限公司股務代理部</li>
                    <li class="mb-3">地址：台北市松山區光復北路11巷35號B1</li>
                    <li class="mb-3">網址：<a href="http://www.masterlink.com.tw" target="_blank">http://www.masterlink.com.tw</a></li>
                    <li class="mb-3">電話：(02)2768-6668</li>
                </ul>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">公司發言人</h4>
                <ul class="mb-0">
                    <li class="mb-3">張立徽</li>
                    <li class="mb-3">職稱：財會室 副總</li>
                    <li class="mb-3">電話：(02)2697-2886</li>
                    <li class="mb-3">電子郵件信箱：<a href="mailto:lihui@ms.formosa-opt.com.tw">lihui@ms.formosa-opt.com.tw</a></li>
                </ul>
            </div>
        </div>
        <div class="u-pt-100 u-pb-400">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">公司代理發言人</h4>
                <ul class="mb-0">
                    <li class="mb-3">王曉萍</li>
                    <li class="mb-3">職稱：財會室 協理</li>
                    <li class="mb-3">電話：(02)2697-2886</li>
                    <li>電子郵件信箱：<a href="mailto:ally@ms.formosa-opt.com.tw">ally@ms.formosa-opt.com.tw</a></li>
                </ul>
            </div>
        </div>
    </section>
</main>