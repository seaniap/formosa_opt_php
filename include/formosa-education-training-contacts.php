        <main class="wrapper">
            <section class="p-knowledge">
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">隱形眼鏡</h3>
                    </div>
                </div>
                <!-- banner -->
                <div class="u-py-100 u-mb-200">
                    <div class="container">
                        <img src="./assets/img/education/education_28.jpg" alt="" class="w-100 img-fluid">
                    </div>
                </div>
                <!-- end banner -->
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">認識隱形眼鏡</h4>
                        <div class="row">
                            <!-- <div class="col-md-6 col-lg-5 col-xl-4 u-pb-100 u-pb-md-000">
                                <img src="assets/img/education/education_15.jpg" alt="" class="img-fluid">
                            </div> <div class="col-md-6 col-lg-7 col-xl-8">-->
                            <div class="col-md-12">
                                <p>分為(1)軟式隱形眼鏡、(2)硬式隱形眼鏡
                                </p>
                                <ol class="u-list-style--decimal u-mb-000">
                                    <li class="font-weight-bold">軟式隱形眼鏡</li>
                                <table class="table table-bordered l-table l-table-layout-fixed">
                                    <tbody>
                                        <tr>
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center" width="120">日拋
                                            </th>
                                            <td colspan="2">
                                                每日拋棄，日拋含負離子較容易保水，相對也較容易吸附空氣中的微粒粉塵，難以清除，因此非常忌諱重複配戴，重複配戴會造成眼睛發炎，嚴重則細菌感染角膜潰瘍，造成視力的永久性傷害。
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center">雙週拋</th>
                                            <td class="align-middle u-bg-gray-100">每兩週拋棄，每週去蛋白</td>
                                            <td rowspan="3" class="align-middle">
                                                隱形眼鏡要特別注意清潔保養，只要隱形眼鏡不乾淨，容易引起巨乳突結膜炎及其他相關病症</td>
                                        </tr>
                                        <tr>
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center">月拋</th>
                                            <td class="align-middle">每個月拋棄，每週去蛋白</td>
                                        </tr>
                                        <tr>
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center">年拋</th>
                                            <td class="align-middle u-bg-gray-100">每年拋棄，每週去蛋白</td>
                                        </tr>
                                    </tbody>
                                </table>
                                    <li class="font-weight-bold">硬式隱形眼鏡</li>
                                    <ul>
                                        <li>硬式透氧度佳，因為鏡片滑動使淚液交換，DK值約在45~140之間，依據鏡片的材質有所不同，由於材質的特性，硬式隱形眼鏡較軟式不容易有蛋白質沉積，也大伏降低感染的機率，但是配戴的異物感較重，適應期較長約兩週至一個月，且不適合運動配戴，若騎乘機車建議配戴護目鏡，避免風沙跑入眼睛。
                                        </li>
                                    </ul>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">隱形眼鏡的配戴與保養</h4>
                        <div class="row">
                            <!-- <div class="col-md-6 col-lg-5 col-xl-4 u-pb-100 u-pb-md-000">
                                <img src="assets/img/education/education_16.jpg" alt="" class="img-fluid">
                            </div> <div class="col-md-6 col-lg-7 col-xl-8">-->
                            <div class="col-md-12">
                                <ol class="u-list-style--decimal u-mb-000">
                                    <li class="font-weight-bold">配戴</li>
                                        <ul class="u-list-style--custom u-mb-100">
                                            <li>(1)&nbsp;先將已殺菌隱形眼鏡的水盒打開放好，將生理食鹽水準備好，清洗雙手，不建議用夾子夾隱形眼鏡，夾子清洗消毒不易，細縫髒污易滋生細菌增加感染風險，建議直接以洗淨的雙手拿取。</li>
                                            <li>(2)&nbsp;將隱形眼鏡以正面碗狀立在食指上，依據你的慣用手，面對鏡子看著自己的眼睛，沒有拿鏡片的那隻手中指、無名指扳上眼皮，從睫毛根部往上提，拿隱形眼鏡的那隻手用中指及無名指扳下眼皮，用食指將鏡片放入。</li>
                                            <li>(3)&nbsp;鏡片放入後，向下看，確定鏡片服貼在角膜上後閉起來。</li>
                                        </ul>
                                    </li>
                                    <li class="font-weight-bold">保養</li>
                                        <ul class="u-list-style--custom">
                                            <li>(1)&nbsp;水盒每三個月換一次，每週應以牙刷清洗一次。</li>
                                            <li>(2)&nbsp;不同的藥水有不同的清潔方式，有些藥水是不可以與眼睛接觸的，應在購買前詢問清楚。</li>
                                            <li>(3)&nbsp;隱形眼鏡不論軟硬式都應搓洗，即使藥水上註明免搓洗，仍然建議搓洗</li>
                                            <li>(4)&nbsp;搓洗方式：</li>
                                                <ul class="u-list-style--disc reset-text-indent">
                                                    <li>先將雙手洗淨，清洗雙手建議使用不含脂類清潔劑，以免沉積在鏡片上</li>
                                                    <li>將隱形眼鏡放在掌心，並滴上清潔藥水，以畫十字的方式單一方向搓洗40下(不要來回搓洗)，也可以放射狀由鏡面中心單一方向向外搓洗40下，軟式隱形眼鏡在搓洗時請小心搓洗以免鏡片破裂。</li>
                                                    <li>再以食鹽水沖洗，確實沖洗乾淨，避免清潔藥水殘留造成眼睛不適</li>
                                                    <li>浸泡藥水基本4~6小時(依藥水指示調整時間)</li>
                                                </ul>
                                            <li>(5)&nbsp;目前市售的清潔藥水多含有去蛋白功能，一般定期更換的隱形眼鏡不需另外去蛋白，若為長戴式產品，可視個人各人狀況，每1~2週加強去蛋白。</li>
                                        </ul>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-pt-100 u-pb-400">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">藥水介紹</h4>
                        <div class="row">
                            <!-- <div class="col-md-6 col-lg-5 col-xl-4 u-pb-100 u-pb-md-000">
                                <img src="assets/img/education/education_17.jpg" alt="" class="img-fluid">
                            </div> <div class="col-md-6 col-lg-7 col-xl-8"> -->
                            <div class="col-md-12">
                                <p>現在的藥水多為多功能藥水，一種藥水就包含清潔、殺菌消毒、保養、浸泡、沖洗…多種功能，再購買前應先請楚了解藥水的功能以及使用方法，是否可以接觸眼睛，隱形眼鏡清潔一定要經過清潔、殺菌、每週去蛋白，以下是一些基本藥水功能的介紹，您可以依據藥水的功能選擇適合您的藥水</p>
                                <table class="table table-bordered l-table">
                                    <tbody>
                                        <tr>
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center" width="120">清潔
                                            </th>
                                            <td class="align-middle">
                                                隱形眼鏡戴在眼睛上，跟空氣接觸一整天，空氣中會有一些粉塵會附著在鏡片，造成配戴的不適感，需要清潔的功能將其洗淨，但具有清潔功能的藥水，清潔力還不足以將鏡片上的蛋白質沈積洗淨，需要額外用去蛋白酵素去蛋白。
                                            </td>
                                        </tr>
                                        <tr class="u-bg-gray-100">
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center">殺菌</th>
                                            <td class="align-middle">鏡片與空氣接觸會有細菌附著在鏡片上，殺菌以避免眼睛感染</td>
                                        </tr>
                                        <tr>
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center">去蛋白</th>
                                            <td class="align-middle">我們的淚液是有三層：黏膜層、水層、脂肪層，水層中有免疫球蛋白與溶酶體，有防止細菌感染的功能，因此鏡片與空氣接觸有細菌就會有蛋白的附著，不易清洗，去蛋白的功能就是將這些蛋白質與細菌的殘渣去除，乾淨舒適。</td>
                                        </tr>
                                        <tr class="u-bg-gray-100">
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center">
                                                保濕<br>
                                                浸泡<br>
                                                保存
                                            </th>
                                            <td class="align-middle">隱形眼鏡必須保持濕潤，佩戴才會舒服，維持鏡片功能的穩定性。</td>
                                        </tr>
                                        <tr>
                                            <th class="u-bg-blue-500 u-text-white u-bdr-top-white align-middle text-center">中和</th>
                                            <td class="align-middle">雙氧系列的藥水，在未經過中和步驟，將藥水刺激性中和，若直接佩戴會造成角膜的傷害，刺痛不已，務必確認隱形眼鏡的藥水已中和在進行配戴。</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        