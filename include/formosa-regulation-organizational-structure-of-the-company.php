<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <h3 class="c-title-center u-mb-125">公司治理</h3>
        </div>
        <!-- regulation tabs -->
        <?php include('formosa_regulation_tabs.php')?>
        <!-- end regulation tabs -->
        <div class="u-pb-100">
            <div class="container">
                <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">公司組織架構與經理人之職稱及職權範圍</p>
                <!-- <p class="u-mb-000">本公司依照證交法所訂資格條件選任獨立董事之相關訊息</p> -->
            </div>
        </div>
        <div class="u-pb-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">一、公司之組織結構</h4>
                <div class="row">
                    <div class="col-12 d-none d-md-block">
                        <img class="w-100" src="./assets/img/regulation/organization-PC.svg" alt="">
                    </div>
                    <div class="col-12 d-md-none">
                        <img class="w-100" src="./assets/img/regulation/organization-mobile.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="u-py-100 u-pb-300">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">二、經理人之職稱及職權範圍</h4>
                <table class="table table-bordered l-table">
                    <thead>
                        <tr>
                            <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-bottom-black l-table-vertical-align-middle">部門</th>
                            <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle">主管<br class="d-sm-none">姓名</th>
                            <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle">職稱</th>
                            <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black l-table-vertical-align-middle" style="min-width: 100px;">主要職掌</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center l-table-vertical-align-middle white-space-nowrap">稽核室</td>
                            <td class="text-center l-table-vertical-align-middle white-space-nowrap">曾威愷</td>
                            <td class="text-center l-table-vertical-align-middle">稽核長</td>
                            <td class="l-table-vertical-align-middle">負責內部控制之改進及內部稽核工作之執行。</td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td class="text-center l-table-vertical-align-middle">營管室</td>
                            <td class="text-center l-table-vertical-align-middle">葉智明</td>
                            <td class="text-center l-table-vertical-align-middle white-space-nowrap">副總經理</td>
                            <td class="l-table-vertical-align-middle">協助分公司營運規劃及控制管理、商品採購、倉儲及銷售管理。</td>
                        </tr>
                        <tr>
                            <td class="text-center l-table-vertical-align-middle">行銷室</td>
                            <td class="text-center l-table-vertical-align-middle">楊記生</td>
                            <td class="text-center l-table-vertical-align-middle">總監</td>
                            <td class="l-table-vertical-align-middle">廣告企劃及執行、商圈規劃與實地調查、市調及商情蒐集。</td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td class="text-center l-table-vertical-align-middle">人資室</td>
                            <td class="text-center l-table-vertical-align-middle">周克倫</td>
                            <td class="text-center l-table-vertical-align-middle">人資長</td>
                            <td class="l-table-vertical-align-middle">負責人事、職教之管理。</td>
                        </tr>
                        <tr>
                            <td class="text-center l-table-vertical-align-middle">管理室</td>
                            <td class="text-center l-table-vertical-align-middle">李正中</td>
                            <td class="text-center l-table-vertical-align-middle">協理</td>
                            <td class="l-table-vertical-align-middle">負責總務、行政文書之管理。</td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td class="text-center l-table-vertical-align-middle">財會室</td>
                            <td class="text-center l-table-vertical-align-middle">張立徽</td>
                            <td class="text-center l-table-vertical-align-middle">副總經理</td>
                            <td class="l-table-vertical-align-middle">負責資金管理、財務調度、成本與一般帳務處理、會計報表製作與分析、股務作業。</td>
                        </tr>
                        <tr>
                            <td class="text-center l-table-vertical-align-middle">資訊室</td>
                            <td class="text-center l-table-vertical-align-middle">陳世煌</td>
                            <td class="text-center l-table-vertical-align-middle">資訊長</td>
                            <td class="l-table-vertical-align-middle">負責電腦主機、網路、資訊系統、資料庫、應用軟體及資訊安全之規劃、建置、開發及維護。</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</main>