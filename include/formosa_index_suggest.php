<?php

namespace app\util;

include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";

$query_contentG1 = sprintf("SELECT * FROM GoWeb_BigTable WHERE Page = '0-1' and Template='suggest01.php'");
$contentG1 = mysqli_query($MySQL,$query_contentG1 ) or die(mysqli_error($MySQL));
$row_contentG1 = mysqli_fetch_assoc($contentG1);
?>

<script type="text/javascript">
 
    function genRecommendation(recommendationData) {
        var html = $("#recommandationDiv").html();
    
        recommendationData.forEach(function(item, i) {
            var index = i+3; //預設三個推薦商品
            html += `
            		<a name="link_recommendation" index="${index}" href="${item.url}" class="v-slick-slide p-2" target="_blank">
        	         <div class="v-slick-slide-frame c-card-head c-card-head">
        	             <img src="${item.image}" alt="" class="img-fluid">
        	         </div>
        	         <div class="c-card-body-layout">
        	             <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">${item.title}</p>
        	         </div>
        	   		 </a>
            	`;
        });
    
    
        $("#recommandationDiv").html(html);
    
        $("#recommandation .v-slick").slick({
            arrows: true,
            slidesToShow: 4,
            slidesPerRow: 1,
            rows: 1,
            prevArrow: "<button type=\"button\" class=\"v-slick-arrows-prev\"><i class=\"fal fa-angle-left\"></i></button>",
            nextArrow: "<button type=\"button\" class=\"v-slick-arrows-next\"><i class=\"fal fa-angle-right\"></i></button>",
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            }]
        });

        $("[name=link_recommendation]").click(function(){
			qg('event', "N_recommend_clicked", {"item_number": $(this).attr("index")});
        });
    }
    
    window.addEventListener("load", function(event) {
        var m_id = "";
        var isloign = false;
        var product = [];
    
        <?php 
        	if (isset($_SESSION['user'])) {
        	    echo "m_id ='".$_SESSION['user']->m_id."'\n"; 
        	    echo "isloign = true;\n"; 
        	    echo "product=". json_encode($_SESSION['user']->recommendationData)."\n";
        	} 
        ?>
    
        //如果登入沒有推薦商品 去抓推薦商品
        if (isloign && product.length < 0) {
            qg('login', {
                'user_id': m_id
            });
            qg('event', 'N_login');
    
            qg('getRecommendationByScenario', {
                    scenarioId: '<?php echo Config::get("scenarioId"); ?>',
                    num: 7,
                },
                function(err, recommendationData) {
                    genRecommendation(recommendationData);
                });
        } else if (!isloign) { //尚未登入去抓20項推薦商品
            qg('getRecommendationByScenario', {
                    scenarioId: '<?php echo Config::get("scenarioId"); ?>',
                    num: 7,
                },
                function(err, recommendationData) {
                    genRecommendation(recommendationData);
                });
        } else {
            genRecommendation(product);
        }
    });
</script>
<script type="text/javascript"> 
	$(function(){
        $("#link_tag_id8").click(function(){
			qg('event', "N_icon_clicked", {"clicled_name": "行動配送"});
		});
	})
</script>



<section id="recommandation" class="u-bg-gray-02 u-pb-100">
    <div class="container-md position-relative">
        <h3 class="c-title-center u-mb-125">商品推薦</h3>
        <div class="position-relative">
            <?php include("editThis_button.php"); ?>
            <div id="recommandationDiv" class="v-slick c-card-verA">
                <!-- Slides -->
                <a  name="link_recommendation" index="1" href="<?php echo $row_contentG1['l01']; ?>" class="v-slick-slide p-2" target="_blank">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="UploadImages/<?php echo $row_contentG1['p01'];  ?>" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">
                            <?php echo $row_contentG1['h01']; ?>
                        </p>
                    </div>
                </a>
                <a name="link_recommendation" index="2" href="<?php echo $row_contentG1['l02']; ?>" class="v-slick-slide p-2" target="_blank">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="UploadImages/<?php echo $row_contentG1['p02'];  ?>" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">
                            <?php echo $row_contentG1['h02']; ?></p>
                    </div>
                </a>
                <a  name="link_recommendation" index="3" href="<?php echo $row_contentG1['l03']; ?>" class="v-slick-slide p-2" target="_blank">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="UploadImages/<?php echo $row_contentG1['p03'];  ?>" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">
                            <?php echo $row_contentG1['h03']; ?></p>
                    </div>
                </a>





                <!--
                <a href="" class="v-slick-slide p-2">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="assets/img/index/product_04.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                    </div>
                </a>
                <a href="" class="v-slick-slide p-2">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                    </div>
                </a>
-->
            </div>
        </div>
        <div class="u-pt-000 text-md-right text-right">
            <a id="link_tag_id8" href="javascript:void(0)"class="u-link-blue-500 u-link__hover--blue-highlight">More
                <i class="far fa-angle-right u-mr-025 u-font-14"></i></a>
        </div>
    </div>
</section>