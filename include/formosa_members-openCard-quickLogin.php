<?php

namespace app\util;

include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";
?>

<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>立即開卡 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="若您已於寶島眼鏡門市加入會員，但沒有帳號密碼，請按照以下步驟完成開卡，即可享有寶島眼鏡會員獨享好康！">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />

    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>

    <script src="assets/plugins/platform.js"></script>
    <!-- JS Customization -->


    <script type="text/javascript">
        window.alert = function(msg, icon = "info") {
            Swal.fire({
                text: "" + msg,
                icon: icon,
                confirmButtonColor: '#3085d6',
                confirmButtonText: '確定'
            })
        };


        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        window.fbAsyncInit = function() {
            FB.init({
                appId: '<?php echo Config::get("fb_app_id"); ?>',
                cookie: true,
                xfbml: true,
                version: 'v2.2'
            });

            FB.AppEvents.logPageView();

        };

        function fbLogin() {
            FB.login(function(response) {
                if (response.status === 'connected') {
                    //已登入 Facebook，也登入應用程式
                    FB.api('/me', {
                        fields: 'name,email,picture.type(large)'
                    }, function(response) {
                        var id = response.id;
                        console.log("id=" + id);
                        login(id, "fb");

                    });
                }
            }, {
                scope: 'public_profile,email'
            }); //選擇性 scope 參數是權用來向應用程式用戶要求權限

        }

        //for line login
        var win;
        window.addEventListener('message', function(e) {

            if (window.location.hostname != "localhost" && e.origin.indexOf(window.location.hostname) < 0) return;

            if (win) {
                win.close();
                win = null;

                var id = e.data;
                if (typeof id === "string") {
                    if (id.indexOf("line") < 0) return;

                    id = id.replace("line "," ");

                    if (id != "" && id != "error") {
                        login(id, "line");

                    }
                }
            }
        }, false);

        function lineLogin() {
            var dftWidth = 400;
            var dftHeight = 600;
            var w = window.screen.width / 2 - (dftWidth / 2);
            var t = window.screen.height / 2 - (dftHeight / 2);

            let client_id = '<?php echo Config::get("line_client_id"); ?>';
            let redirect_uri = '<?php echo Config::get("line_redirect_uri"); ?>';
            let link = 'https://access.line.me/oauth2/v2.1/authorize?';
            link += 'response_type=code';
            link += '&client_id=' + client_id;
            link += '&redirect_uri=' + redirect_uri;
            link += '&state=login';
            link += '&scope=openid%20profile';

            win = window.open(link, '_blank', 'location=false,height=400,width=500,scrollbars=yes,status=yes,top=' + t + ",left=" + w);
        }

        function login(social_id, type) {
            var params = {
                "social_id": social_id,
                "type": type,
            };
            $.get("app/controller/Member.php?method=socialBind", params, function(result) {
                result = JSON.parse(result);
                if (result.result == "1") {
                    $("#" + type + "_not_bind").hide();
                    $("#" + type + "_bind").show();
                } else {
                    alert(result.message);
                }
            });
        }

        function googleLogin() {
            auth2.signIn().then(function() {
                if (auth2.isSignedIn.get()) {
                    var profile = auth2.currentUser.get().getBasicProfile();
                    console.log('ID: ' + profile.getId());

                    login(profile.getId(), "google");
                }
            });
        }

        $(function() {
            gapi.load('auth2', function() {
                auth2 = gapi.auth2.init({
                    client_id: '<?php echo Config::get("google_client_id"); ?>',
                    fetch_basic_profile: true,
                    scope: 'profile'
                });
            });
        });
    </script>
   <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php') ?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">立即開卡</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-12">
                                <div class="u-mb-200">
                                    <p class="u-text-gray-800 u-font-weight-700 u-font-18 text-center">綁定社群帳號後，下次登入更為便利</p>
                                </div>

                                <div class="row justify-content-center u-mb-200">
                                    <div class="col-10 col-sm-12">
                                        <div class="row align-items-start">
                                            <!-- item -->
                                            <div class="col-4 px-2 px-md-3" id="fb_not_bind">
                                                <a class="text-center" href="javascript:fbLogin();">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/facebook_bind.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">我要綁定Facebook</p>
                                                </a>
                                            </div>
                                            <div class="col-4 px-2 px-md-3" id="fb_bind" style="display: none">
                                                <a class="text-center" href="">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/facebook.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">已綁定Facebook</p>
                                                </a>
                                            </div>
                                            <div class="col-4 px-2 px-md-3" id="google_not_bind">
                                                <a class="text-center" href="javascript:googleLogin();">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/google_bind.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">我要綁定Gooogle</p>
                                                </a>
                                            </div>
                                            <div class="col-4 px-2 px-md-3" id="google_bind" style="display: none">
                                                <a class="text-center" href="">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/google.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">已綁定Gooogle</p>
                                                </a>
                                            </div>
                                            <div class="col-4 px-2 px-md-3" id="line_not_bind">
                                                <a class="text-center" href="javascript:lineLogin();">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/line_bind.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">我要綁定LINE</p>
                                                </a>
                                            </div>
                                            <div class="col-4 px-2 px-md-3" id="line_bind" style="display: none">
                                                <a class="text-center" href="javascript:alert('帳號已綁定!');">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/line.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">已綁定LINE</p>
                                                </a>
                                            </div>
                                            <!--end item -->
                                        </div>
                                    </div>
                                </div>

                                <button onclick="location.href='index.php?Page=B-2-8'" type="button" class="c-btn c-btn--contained c-btn-blue-highlight col">略過</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>