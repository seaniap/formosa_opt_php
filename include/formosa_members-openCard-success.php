<?php
namespace app\util;
include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";

session_start();

if (!isset($_SESSION['user']))
    header("Location: index.php?Page=B-1")
    
?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>立即開卡 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="若您已於寶島眼鏡門市加入會員，但沒有帳號密碼，請按照以下步驟完成開卡，即可享有寶島眼鏡會員獨享好康！">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <script src="https://apis.google.com/js/platform.js"></script>
    <!-- JS Customization -->
    
     <script type="text/javascript">
      !function(q,g,r,a,p,h,js) {
        if(q.qg)return;
        js = q.qg = function() {
          js.callmethod ? js.callmethod.call(js, arguments) : js.queue.push(arguments);
        };
        js.queue = [];
        p=g.createElement(r);p.async=!0;p.src=a;h=g.getElementsByTagName(r)[0];
        h.parentNode.insertBefore(p,h);
      } (window,document,'script','https://cdn.qgr.ph/qgraph.<?php echo Config::get("appier_id");?>.js');

      qg('login', {'user_id':"<?php echo $_SESSION['user']->m_id?>"});
      qg('event','N_login');
      
      qg('getRecommendationByScenario',
      {
        scenarioId: '<?php echo Config::get("scenarioId"); ?>',
        num: 7,
      },
      function(err, recommendationData) {
    	  console.log("result recommendationData");    
          console.log(recommendationData);
          $.get("app/controller/Member.php?method=updateRecommendationData",{data:recommendationData},function(result){
        	  console.log("server updateRecommendationData:" +result);
          }); 
      });     
    </script>
   <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php')?>
        </header>
		<div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250 u-pb-025">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">完成開卡</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="row align-items-center flex-column u-mb-200 u-mt-200">
                            <div class="p-bigIcon">
                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon21.svg" alt="">
                            </div>
                            <h3 class="u-text-red-formosa u-font-24">看一下您享有的權利吧</h3>
                        </div>

                        <div class="row u-mb-500">
                            <div class="col-lg-6 u-mb-200 u-mb-lg-000">
                                <img src="assets/img/members/opencard-1.jpg" alt="" class="img-fluid">
                            </div>

                            <!-- start -->
                            <div class="col-lg-6 col-12 d-flex flex-column justify-content-between">
                                <div class="u-mb-100 u-mb-lg-000">
                                    <div class="d-flex align-items-center">
                                        <div class="p-titleIcon">
                                            <img class="w-100 img-fluid u-pr-025 u-py-025"
                                                src="assets/img/members/icon22.svg" alt="">
                                        </div>
                                        <h5 class="u-text-gray-800 u-font-weight-700 u-font-18 u-m-000">
                                            會員專屬優惠
                                        </h5>
                                    </div>
                                    <div>
                                        <p class="u-mb-000 u-ml-md-000 u-ml-150">APP隨時掌握會員專屬優惠</p>
                                    </div>
                                </div>

                                <div class="u-mb-100 u-mb-lg-000">
                                    <div class="d-flex align-items-center">
                                        <div class="p-titleIcon">
                                            <img class="w-100 img-fluid u-pr-025 u-py-025"
                                                src="assets/img/members/icon22.svg" alt="">
                                        </div>
                                        <h5 class="u-text-gray-800 u-font-weight-700 u-font-18 u-m-000">
                                            會員日點數5倍
                                        </h5>
                                    </div>
                                    <div>
                                        <p class="u-mb-000 u-ml-md-000 u-ml-150">每月5日指定品牌商品點數5倍狂飆</p>
                                    </div>
                                </div>

                                <div class="u-mb-100 u-mb-lg-000">
                                    <div class="d-flex align-items-center">
                                        <div class="p-titleIcon">
                                            <img class="w-100 img-fluid u-pr-025 u-py-025"
                                                src="assets/img/members/icon22.svg" alt="">
                                        </div>
                                        <h5 class="u-text-gray-800 u-font-weight-700 u-font-18 u-m-000">
                                            點數累積好康
                                        </h5>
                                    </div>
                                    <div>
                                        <p class="u-mb-000 u-ml-md-000 u-ml-150">
                                            拋棄式隱形眼鏡及藥水之單品項50元累1點，鏡框、葉黃素、面膜等其他商品100元累1點</p>
                                    </div>
                                </div>

                                <div class="u-mb-100 u-mb-lg-000">
                                    <div class="d-flex align-items-center">
                                        <div class="p-titleIcon">
                                            <img class="w-100 img-fluid u-pr-025 u-py-025"
                                                src="assets/img/members/icon22.svg" alt="">
                                        </div>
                                        <h5 class="u-text-gray-800 u-font-weight-700 u-font-18 u-m-000">
                                            會員卡跟著走
                                        </h5>
                                    </div>
                                    <div>
                                        <p class="u-mb-000 u-ml-md-000 u-ml-150">APP就是會員卡點數明細即時查</p>
                                    </div>
                                </div>

                                <div class="u-mb-100 u-mb-lg-000">
                                    <div class="d-flex align-items-center">
                                        <div class="p-titleIcon">
                                            <img class="w-100 img-fluid u-pr-025 u-py-025"
                                                src="assets/img/members/icon22.svg" alt="">
                                        </div>
                                        <h5 class="u-text-gray-800 u-font-weight-700 u-font-18 u-m-000">
                                            點點都是現金
                                        </h5>
                                    </div>
                                    <div>
                                        <p class="u-mb-000 u-ml-md-000 u-ml-150">1點可折抵1元</p>
                                    </div>
                                </div>

                                <div class="u-mb-100 u-mb-lg-000">
                                    <div class="d-flex align-items-center">
                                        <div class="p-titleIcon">
                                            <img class="w-100 img-fluid u-pr-025 u-py-025"
                                                src="assets/img/members/icon22.svg" alt="">
                                        </div>
                                        <h5 class="u-text-gray-800 u-font-weight-700 u-font-18 u-m-000">
                                            行動配送服務
                                        </h5>
                                    </div>
                                    <div>
                                        <p class="u-m-000 u-ml-md-000 u-ml-150">會員獨享EYESmart 行動配送及預約試戴服務</p>
                                    </div>
                                </div>

                            </div>
                            <!-- end -->
                        </div>
                        
                        <div class="row justify-content-center">
                           <div class="col-md-3 col-sm-6 u-mb-100">
                                <button type="button"  onclick="location.href='members-benefit.html'"   class="c-btn c-btn--contained c-btn-blue-highlight col">
                                    會員權益
                                </button>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <button type="button" onclick="location.href='index.php'" class="c-btn c-btn--outlined c-btn-gray-800 col">
                                    回首頁
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php')?></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>