<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-tw" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-tw" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-tw">
<!--<![endif]-->

<head>
<? include('std_header.php')?>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta property="og:url" content="https://www.your-domain.com/your-page.html" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="Your Website Title" />
  <meta property="og:description" content="Your description" />
  <meta property="og:image" content="https://www.your-domain.com/path/image.jpg" />
  <!-- Favicon -->
  <link rel="shortcut icon" href="favicon.ico">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="assets/plugins/bootstrap4/css/bootstrap.min.css">
  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="assets/plugins/fontawesome5/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="assets/plugins/icomoon/style.css">
  <!-- CSS Customization -->
  <link rel="stylesheet" href="assets/css/main.css">
  <!-- Facebook Pixel Code -->
   <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
  <header id="header">
  </header>
  <nav class="breadcrumb-nav" aria-label="breadcrumb">
    <div class="container">
      <ol class="breadcrumb">
        <li>
          <a href="index.html">
            <i class="fas fa-home"></i>
          </a>
        </li>
        <li class="active">
          <a href="">經銷據點</a>
        </li>
      </ol>
    </div>
  </nav>
 <? 	if($row_template['TP4']<>""){include($row_template['TP4']); }  ?> 
  <!-- JS Global Compulsory -->
  <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
  <script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
  <script type="text/javascript"
    src="assets/plugins/popper.min.js"></script>
  <script type="text/javascript" src="assets/plugins/bootstrap4/js/bootstrap.js"></script>
  <!-- js plugins -->
  <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
  <!-- JS Customization -->
  <script type="text/javascript" src="assets/js/main.js"></script>
  <!-- <script>
    jQuery(function () {
      var page = location.pathname.split('/').pop();
      page = 'news.html'; //hard coded for testing
      $('#mainmenu li a[href="' + page + '"]').addClass('active')
  });
  </script> -->
  <!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>

</html>