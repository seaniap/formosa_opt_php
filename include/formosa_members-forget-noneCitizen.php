<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title id="metaTitle">忘記密碼｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta id="description" name="description" content="如果您忘記密碼或使用者帳號，請按照這些步驟復原寶島眼鏡帳戶，寶島眼鏡會將使用者帳號及新密碼寄送到您原先設定的e-mail信箱或手機。">
     
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
   
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />

    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- JS Customization -->

    <script type="text/javascript">
        window.alert = function(msg, icon = "info") {
            Swal.fire({
                text: "" + msg,
                icon: icon,
                confirmButtonColor: '#3085d6',
                confirmButtonText: '確定'
            })
        };
        function popupShow(id){
    		$("#"+id).attr("style", "");
    	}
    
    	function popupHide(id){
    		$("#"+id).attr("style", "display: none !important");
    	}
        $(function() {
            $.validator.methods.idcard = function(value, element, params) {
                return $("#idcard_" + $("#idtype").val()).val() != "";
            };

            $("#idtype").change(function() {
                if (this.value == 2) { //國外
                    $("#div_idcard_2").show();
                    $("#div_idcard_3").hide();
                } else if (this.value == 3) { //居留證
                    $("#div_idcard_2").hide();
                    $("#div_idcard_3").show();
                }
            });


            $("#form").validate({
                rules: {
                    idcard_2: {
                        idcard: true
                    },
                    idcard_3: {
                        idcard: true
                    },

                },
                messages: {
                    idcard_2: "請輸入您的該國身分證字號",
                    idcard_3: "請輸入您的居留證號",
                },
                submitHandler: function(form) {
                    var m_mobile = "";
                    var m_idcard = $("#idcard_" + $("#idtype").val()).val();

                    var date = $("#date").val().split('/');

                    var params = {
                        "m_mobile": m_mobile,
                        "m_idcard": m_idcard,
                        "m_birth_year": date[0],
                        "m_birth_month": date[1],
                        "m_birth_day": date[2],


                    };

                    $.get("app/controller/Member.php?method=getForgetMember", params, function(result) {
                        result = JSON.parse(result);
                        
                        if (result.result == 0   ) {
                       	 	popupShow("alertMsg");
                        } else if (result.m_card_no  && !result.m_account) {
                        	popupShow("alertMsg1");  
                        } else if (!result.m_card_no  && !result.m_account) {
                        	popupShow("alertMsg");  
                        } else {
                            window.location = "index.php?Page=B-1-1-3";
                        }
                    });
                }
            })
        });
    </script>

<?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    
     <div id="alertMsg" class="p-pop_up_wrap d-flex align-items-center justify-content-center" style="display: none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="row p-pop_up justify-content-center">
                        <div class="col-lg-3 col-4 u-mb-125">
                            <img src="assets/img/members/icon25.svg" alt="">
                        </div>
                         <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
		         	                	請確認您輸入的資料是否正確
			                        </h3>
			                        <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
		         	                	資料錯誤或未曾加入會員，但現在想加入，請選擇立即註冊
			                        </p>
                        <button onclick="window.location='index.php?Page=B-3'" class="c-btn c-btn--contained c-btn-blue-highlight col-md-10 col-6">
                            我知道了
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div id="alertMsg1" class="p-pop_up_wrap d-flex align-items-center justify-content-center" style="display: none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="row p-pop_up justify-content-center">
                        <div class="col-lg-3 col-4 u-mb-125">
                            <img src="assets/img/members/icon25.svg" alt="">
                        </div>
                         <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
		         	                	請確認您輸入的資料是否正確
			                        </h3>
			                        <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
		         	                	尚未設定帳號密碼，請點選我知道了前往立即開卡頁面
			                        </p>
                        <button onclick="window.location='index.php?Page=B-2-3'" class="c-btn c-btn--contained c-btn-blue-highlight col-md-10 col-6">
                            我知道了
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php') ?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">忘記帳號/密碼</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-12">
                                <form id="form">
                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="select">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon1.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">驗證方式</h5>
                                        </label>
                                        <select name="idtype" id="idtype" class="l-form-field l-form-select">
                                            <option value="3" selected>有居留證號者</option>
                                            <option value="2">國籍</option>
                                        </select>
                                    </div>


                                    <div class="u-mb-150" id="div_idcard_3">
                                        <label class="d-flex align-items-center u-mb-075" for="required">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon3.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">居留證號</h5>
                                        </label>
                                        <input type="text" id="idcard_3" name="idcard_3" class="l-form-field" placeholder="請輸入您的居留證號">
                                    </div>

                                    <div id="div_idcard_2" style="display: none;">
                                        <div class="u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="select">
                                                <!-- <div class="p-titleIcon">
                                                    <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon1.svg" alt="">
                                                </div> -->
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">選擇國籍</h5>
                                            </label>
                                            <select name="select" id="select" class="l-form-field l-form-select">
                                                <option value="default" selected>中國</option>
                                                <option value="香港">香港</option>
                                                <option value="澳門">澳門</option>
                                                <option value="新加坡">新加坡</option>
                                            </select>
                                        </div>

                                        <div class="u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="required">
                                                <!-- <div class="p-titleIcon">
                                                    <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon3.svg" alt="">
                                                </div> -->
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">身分證字號</h5>
                                            </label>
                                            <input type="text" id="idcard_2" name="idcard_2" class="l-form-field" placeholder="請輸入您的該國身分證字號">
                                        </div>
                                    </div>


                                    <div class="u-mb-200">
                                        <label class="d-flex align-items-center u-mb-075" for="date">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon2.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">出生日期</h5>
                                        </label>
                                        <input type="text" readonly id="date" name="date" class="l-form-field" value="1990/01/01">
                                    </div>
                                    <button id="btnSend" type="submit" class="c-btn c-btn--contained c-btn-blue-highlight col">
                                        確認送出
                                    </button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
        <!-- =============end footer ============= -->
    </div>
</body>

</html>