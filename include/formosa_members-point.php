<?php 
session_start();

?>
 
<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>點數查詢 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="登入會員帳號，即可查詢全部點數、即將到期點數及交易紀錄等相關訊息。">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
	<link rel="stylesheet" href="assets/plugins/icomoon/style.css">
	<link rel="stylesheet" href="assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="assets/css/main.css" />
         <!-- <link rel="stylesheet" href="assets/plugins/page/pagination.css" /> -->
  
         <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php')?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">會員專區</h3>
                    </div>
                </div>
                <!-- member static tabs -->
        <?php include('formosa-member-static_tabs.php')?>
        <!-- end member static tabs -->
                <div class="u-py-100">
                    <div class="container">
                        <div class="d-flex justify-content-center">
                            <h2 class="u-text-red-formosa u-font-weight-700 u-font-36 u-mb-000">
                                <span class="u-text-black u-font-18">價值</span>
                                <?php echo isset($_SESSION['user']) ? $_SESSION['user']->m_points:"";?>
                                <span class="u-text-black u-font-18">元</span>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container d-flex justify-content-center">
                        <div class="row col-12 justify-content-center">
                            <div
                                class="col-6 col-md-4 col-lg-3 p-pointFrame d-flex justify-content-center flex-column align-items-center">
                                <p>即將到期時間</p>
                                <p class="u-mb-000"><?php  echo isset($_SESSION['user']) ? $_SESSION['user']->m_exp_date :"";?></p>
                            </div>
                            <div
                                class="col-6 col-md-4 col-lg-3 p-pointFrame d-flex justify-content-center flex-column align-items-center">
                                <p>即將到期點數</p>
                                <p class="u-mb-000"><?php echo   isset($_SESSION['user']) ?  $_SESSION['user']->m_exp_points:"";?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <div class="d-flex justify-content-center align-items-center">
                            <a href="javascript:yearBack()">
                                <i class="icon-arrow_left mr-2 u-text-gray-800"></i>
                            </a>
                            <p class="u-font-24 u-mb-000">
                                <i class="fal fa-calendar-alt mr-2"  ></i>
                                <span id="year">2021</span>
                            </p>
                            <a href="javascript:yearTo()">
                                <i class="icon-arrow_right ml-2 u-text-gray-800"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <div class="l-tablesSroller-wrapper" id="pagination-data">
                        </div>
                        
                        <script type="text/template" id="tpl">
                            <table class="c-shopTable c-shopTable-rwd c-shopTable-rwd-verB c-shopTable--bordered__bottom">
                                <thead>
                                    <tr class="u-bg-gray-100">
                                        <th scope="row" class="u-py-100 u-text-gray-800">交易日期</th>
                                        <th scope="row" class="u-py-100 u-text-gray-800">門市</th>
                                        <th scope="row" class="u-py-100 u-text-gray-800">實付金額</th>
                                        <th scope="row" class="u-py-100 u-text-gray-800">折抵點數</th>
                                        <th scope="row" class="u-py-100 u-text-gray-800">累積點數</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% _.each(datas, function(data){ %>
                                        <tr>
                                            <td data-th="交易日期" class="text-xl-center"><span><%= data.s_date %></span></td>
                                            <td data-th="門市" class="text-xl-center"><span><%= data.s_name %></span></td>
                                            <td data-th="實付金額" class="text-xl-center"><span>＄<%= data.s_amt %></span></td>
                                            <td data-th="折抵點數" class="text-xl-center"><span><%= data.s_points2 %></span></td>
                                            <td data-th="累積點數" class="text-xl-center"><span><%= data.s_points1 %></span></td>
                                        </tr>
                                    <% }) %>
                                </tbody>
                            </table>
                            </script>
                    </div>
                </div>
                <!-- START 頁籤 -->
                <div class="u-py-100 u-pb-400">
                    <div class="container">
                        <nav>
                            <ul class="c-pagination justify-content-center align-items-center u-mb-000">
                                <li class="c-pagination-item">
                                  
                                </li>
                                <div id="pagination_page"></div>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- END 頁籤 -->
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php')?></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="assets/js/main.js"></script>
    <script src="assets/plugins/page/pagination.js"></script>
	<script src="assets/plugins/underscore-umd-min.js" type="text/javascript"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- JS Customization -->
    
    <script type="text/javascript">
		$(function(){
			 var isLogin = "<?php echo isset($_SESSION['user']);?>"
				 
			 if (!isLogin) {
    			 Swal.fire({
 	                 html: `
 	                	 <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
  	                		您尚未登入會員
                        </h3>
                        <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
  	                		若想修改會員資料或查詢會員點數，請先登入會員
                        </p>
	   	                 `,
 	                 imageUrl: 'assets/img/members/icon25.svg',
 	                 imageWidth: 70,
 	                 imageHeight: 70,
  	   	             allowEscapeKey: false,
       	   	         confirmButtonColor: '#3085d6',
                     confirmButtonText: '會員登入',
                     showCancelButton: true,
	         	     cancelButtonText: '我知道了', 
 	               }).then((result) => {
 	            	   if (result.dismiss  == 'cancel') {
	      	               window.location ="index.php?Page=B-7";
		      	       } else {
			      	       window.location ="index.php?Page=B-1";
			      	   }
 	               })
             }

			
			var year = new Date().getFullYear();
			$("#year").html(year);
			getMemberSale(year);
 
		});

		function yearBack(){
			var year = $("#year").html();
			year=parseInt(year) -1;
			$("#year").html(year);
			getMemberSale(year);
		}

		function yearTo(){
			var year = $("#year").html();
			year=parseInt(year) + 1;
			$("#year").html(year);
			getMemberSale(year);
		}

		function getMemberSale(year){
			$.get("app/controller/Member.php?method=getMemberSale", {"year": year} , function(result){
				result = JSON.parse(result);

				if (result.result == 0) {
 					result.Sales = [];
				}
				
				var container = $('#pagination_page');
      	        container.pagination({
      	        	 dataSource : result.Sales,
      	             pageSize: 5,
      	             callback: function (response, pagination) {
	 					  var compiled = _.template($("#tpl").html());
	    	              var html = compiled({"datas":response});
	    	              $("#pagination-data").html(html);
      	            } 
      	        });
				 
			});
	    }
    </script>
</body>

</html>
