
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">教育訓練</h3>
                    </div>
                </div>
                <div class="u-py-100 u-pb-400">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">眼鏡業專業人才培訓的創始者</h4>
                        <p>
                            人才是企業最寶貴的資產，寶島眼鏡每年投資超過千萬元以上的預算用於夥伴的專業及技術培訓；從新進人員學科知識的傳授、經驗的傳承，到各職級人員的技能檢定，均依職級及功能的不同，定期授予不同的課程訓練。寶島眼鏡始終秉持著優良人才是企業唯一長久競爭利基的信念，因此每年投注大成本、大規模的教育訓練，不僅是國內首見，放眼世界華人的眼鏡業者，也難以迄及如此專業專精的培訓計劃。
                        </p>
                        <img src="assets/img/about_edu/about_edu_img.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </section>
        </main>
        