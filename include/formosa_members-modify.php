<?php
session_start();

if (!isset($_SESSION['user']))
    header("Location: index.php?Page=B-1")

?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>忘記密碼 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="如果您忘記密碼或使用者帳號，請按照這些步驟復原寶島眼鏡帳戶，寶島眼鏡會將使用者帳號及新密碼寄送到您原先設定的e-mail信箱或手機。">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />


    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>


    <script type="text/javascript">
        $(function() {
            window.alert = function(msg, icon = "info") {
                Swal.fire({
                    text: "" + msg,
                    icon: icon,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: '確定'
                })
            };

            $.validator.methods.reg = function(value, element, params) {
                return params.test(value);
            };

            $.validator.methods.pw_check = function(value, element, params) {
                return params == value;
            };

            $.validator.methods.new_pw_check = function(value, element, params) {
                if ($("#new_password").val() == "" || $("#re_password").val() == "") return true;

                return $("#new_password").val() == $("#re_password").val();
            };

            $("#form").validate({
                rules: {
                    password: {
                        required: true,
                        reg: /^([a-zA-Z0-9]+)$/,
                        rangelength: [6, 12],
                        pw_check: "<?php echo $_SESSION['user']->m_passwd; ?>"
                    },
                    new_password: {
                        required: true,
                        reg: /^([a-zA-Z0-9]+)$/,
                        rangelength: [6, 12],
                        new_pw_check: true,
                    },

                    re_password: {
                        required: true,
                        reg: /^([a-zA-Z0-9]+)$/,
                        rangelength: [6, 12],
                        new_pw_check: true,
                    },

                },
                messages: {
                    password: {
                        required: "請輸入6-12位英數帳號",
                        reg: "密碼英數字組合",
                        pw_check: "此原密碼不存在",
                    },
                    new_password: {
                        required: "請輸入6-12位英數帳號",
                        reg: "密碼英數字組合",
                        new_pw_check: "與確認新密碼不符",
                    },
                    re_password: {
                        required: "請輸入6-12位英數帳號",
                        reg: "密碼英數字組合",
                        new_pw_check: "與新密碼不符",
                    },
                },

                submitHandler: function(form) {
                    $.get("app/controller/Member.php?method=updatePassword", {
                        "new_password": $("#new_password").val()
                    }, function(result) {
                        result = JSON.parse(result);
                        if (result.result == "1") {
                            Swal.fire({
                                text: "更新成功!",
                                icon: "info",
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: '我知道了'
                            }).then((result) => {
                                window.location = "index.php";
                            })

                        } else {
                            alert(result.message)
                        }
                    });
                }
            });
        });
    </script>
   <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php') ?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">修改密碼</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-12">
                                <form id="form">
                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="password">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon5.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">原密碼</h5>
                                        </label>
                                        <input type="password" id="password" name="password" class="l-form-field" placeholder="請輸入系統提供密碼">
                                    </div>
                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="password">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon6.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">新密碼</h5>
                                        </label>
                                        <input type="password" id="new_password" name="new_password" class="l-form-field" placeholder="6-12英數字組合，不含特殊字元">
                                    </div>
                                    <div class="u-mb-200">
                                        <label class="d-flex align-items-center u-mb-075" for="password">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon6.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">確認新密碼</h5>
                                        </label>
                                        <input type="password" id="re_password" name="re_password" class="l-form-field" placeholder="請輸入您的新密碼">
                                    </div>
                                    <button type="submit" class="c-btn c-btn--contained c-btn-blue-highlight col">修改</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
        <!-- =============end footer ============= -->
    </div>
</body>

</html>