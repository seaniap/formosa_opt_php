<!DOCTYPE html>
<html lang="zh-tw">

<head>
<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>寶島眼鏡官網</title>
	<meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
	<meta name="description" content="描述">
	<meta name="author" content="寶島眼鏡">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
	<meta property="og:image" content="assets/img/share_1200x630.jpg" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
	<link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
	<link rel="stylesheet" href="assets/plugins/aos/aos.css">
	<!-- <link rel="stylesheet" href="assets/plugins/swiper/swiper-bundle.min.css"> -->
	<link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
	<link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
	<link rel="stylesheet" href="assets/css/main.css" />
    <?php require_once('../lib/BBlib.php'); ?>
	<?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 主要內容區 ============= -->
            <!-- start main content -->
            <?php if($row_template['TP1']<>""){include($row_template['TP1']); }  ?> 
            <!-- end main content -->
        <!-- =============end 主要內容區 ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
	<script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
	<!-- <script src="assets/plugins/swiper/swiper-bundle.min.js"></script> -->
	<script src="assets/plugins/aos/aos.js"></script>
	<script src="assets/plugins/slick@1.90/slick.min.js"></script>
	<script src="assets/plugins/lozad/lozad.min.js"></script>
	<script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
	<script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>