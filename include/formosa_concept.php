<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="header" class="l-header"></header>
		<div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main>
            <section>
                <div class="container u-py-200">
                    <h3 class='c-title-center u-mb-225'>經營理念</h3>
                </div>
                <div class="u-pb-200">
                    <div class="container">
                        <h4 class="c-title-underline">誠實服務</h4>
                        <div class="row">
                            <div class="col-md-6 col-12 u-mb-200">
                                <img src="assets/img/concept/style3.jpg" alt="" class="w-100 img-fluid">
                            </div>
                            <p class="col-md-6 col-12 u-text-gray-800 u-mb-000 text-md-left text-center">30餘年來，寶島『誠信』的理念可由眾多消費者的肯定所得之印證，寶島眼鏡自創立之初，即秉持【誠實服務】的中心思想經營企業，因而多年來，不斷締造佳績、擴大經營規模，為業界創造正派誠實的優異典範。誠實服務是寶島眼鏡最珍貴的果實，有形的，提供專業配鏡服務及價格合理、品質可靠的產品；無形的，以專業、親切、可靠的服務，獲得消費者多年的信賴與支持；因為誠實，是寶島眼鏡成功的基石，所以在大步邁向國際的同時，誠實服務仍是我們不變的堅持。</p>
                        </div>
                        
                    </div>
                </div>
                <div class="u-pt-200 u-pb-500">
                    <div class="container">
                        <h4 class="c-title-underline">四大保固 從『心』出發</h4>
                        <p>【 以下保固皆不包含人為損壞，活動詳情請洽門市 】</p>
                        <div
                            class="row no-gutters col-12 u-bg-gray-100 align-items-center justify-content-center flex-md-row u-p-200 u-mb-200">
                            <div class="col-md-2 col-8 u-p-100">
                                <img src="assets/img/index/icons/09.svg" alt="" class="w-100 img-fluid">
                            </div>
                            <div class="col-md-10">
                                <h5 class="u-text-blue-500 u-font-weight-700 u-font-24 u-mb-50 text-md-left text-center">品質保固</h5>
                                <p class="u-text-gray-800 u-font-weight-700 u-font-18 u-mb-50 text-md-left text-center">品質最安心~商品如有瑕疵,半年內免費更換</p>
                                <p class="u-text-gray-800 u-mb-000 u-font-18 text-md-left text-center">商品取件後,有關於任何產品本身的品質瑕疵,可免費更換。 但以第一次取件日起計算,180天內為限。</p>
                            </div>
                        </div>
                        <div
                            class="row no-gutters col-12 u-bg-gray-100 align-items-center justify-content-center flex-md-row u-p-200 u-mb-200">
                            <div class="col-md-2 col-8 u-p-100">
                                <img src="assets/img/index/icons/10.svg" alt="" class="w-100 img-fluid">
                            </div>
                            <div class="col-md-10">
                                <h5 class="u-text-blue-500 u-font-weight-700 u-font-24 u-mb-50 text-md-left text-center">技術保固</h5>
                                <p class="u-text-gray-800 u-font-weight-700 u-font-18 u-mb-50 text-md-left text-center">半年內有任何度數問題,可免費更換同廠牌鏡片</p>
                                <p class="u-text-gray-800 u-mb-000 u-font-18 text-md-left text-center">商品取件後,如有度數改變需更換鏡片者,可以免費更換。但以第一次取件日起計算,180天內為限。<span class="u-font-14">(醫師處方、指定度数,以及任何疾病或手術造成 的度数改變者,不適用此辦法)</span></p>
                            </div>
                        </div>
                        <div
                            class="row no-gutters col-12 u-bg-gray-100 align-items-center justify-content-center flex-md-row u-p-200 u-mb-200">
                            <div class="col-md-2 col-8 u-p-100">
                                <img src="assets/img/index/icons/11.svg" alt="" class="w-100 img-fluid">
                            </div>
                            <div class="col-md-10">
                                <h5 class="u-text-blue-500 u-font-weight-700 u-font-24 u-mb-50 text-md-left text-center">滿意保固</h5>
                                <p class="u-text-gray-800 u-font-weight-700 u-font-18 u-mb-50 text-md-left text-center">配戴不滿意30天內免費更換等值鏡架</p>
                                <p class="u-text-gray-800 u-mb-000 u-font-18 text-md-left text-center">在寶島眼鏡選配的眼鏡,只要對款式不滿意。在取件後30天內可以於原 購買門市免費更換等值鏡架一次,<span class="u-font-14">(但鏡片已加工,不在此限)</span>。如消費者欲升級 商品則貼補差價亦可。<span class="u-font-14">(原配戴眼鏡需保持商品完整性且不得有任何人為損傷方可更換)</span></p>
                            </div>
                        </div>
                        <div
                            class="row no-gutters col-12 u-bg-gray-100 align-items-center justify-content-center flex-md-row u-p-200">
                            <div class="col-md-2 col-8 u-p-100">
                                <img src="assets/img/index/icons/12.svg" alt="" class="w-100 img-fluid">
                            </div>
                            <div class="col-md-10">
                                <h5 class="u-text-blue-500 u-font-weight-700 u-font-24 u-mb-50 text-md-left text-center">服務保固</h5>
                                <p class="u-text-gray-800 u-font-weight-700 u-font-18 u-mb-50 text-md-left text-center">加入會員再享終身維修免費</p>
                                <p class="u-text-gray-800 u-mb-000 u-font-18 text-md-left text-center">在寶島眼鏡所選配的商品,均適用於免費服務之項目,並享有一家配鏡 連鎖專業服務之保固。如再加入會員則可以享受指定項目 <span class="u-font-14">註</span>終身維修一年四次免費之服務<span class="u-font-14">註指定項目包含鏡架調整、眼鏡螺絲、鼻墊、特殊無華、腳套、運動帶、運動掛勾、眼鏡盒、眼鏡布、螺絲起子、隱形眼鏡盒、隱形眼鏡夾子</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer">
            <?php include('formosa_footer.php') ?>
        </footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>