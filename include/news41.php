<?php require_once('../Connections/MySQL.php'); ?>
<?php $template = "news41.php";
$templateS = "news41";
// 判斷是預設是A版還是S版
// 預設 A 版

if ($_GET[$templateS] == "" and $row_template['Extend'] == "Y") {
  //A 版開始
  if ($_GET['demo'] == 'y') { ?>

    <p>4-1</p>

  <?php } else {  ?>
    <?php $currentPage = $_SERVER["PHP_SELF"];

    $maxRows_content01 = $row_template['MaxRow'];
    $pageNum_content01 = 0;
    if (isset($_GET['pageNum_content01'])) {
      $pageNum_content01 = $_GET['pageNum_content01'];
    }
    $startRow_content01 = $pageNum_content01 * $maxRows_content01;

    $colname_content01 = "-1";
    if (isset($_GET['Page'])) {
      $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
    }

    $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
    if ($_GET['Cate01'] <> "") {
      $query_content01 = $query_content01 . "AND Cate01='" . $_GET['Cate01'] . "'";
    }

    if ($_GET['Cate02'] <> "") {
      $query_content01 = $query_content01 . "AND Cate02='" . $_GET['Cate02'] . "'";
    }

if ($_GET['DD'] == "Y") {
      $query_content01 = $query_content01 . " AND d02 >= now()";
    }

if ($_GET['DD'] == "N") {
      $query_content01 = $query_content01 . "AND  d02 < now()";
    }

    //排序
    $query_content01 = $query_content01 . " order by Sq desc ";

    $query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
    //echo $query_limit_content01 ;
    $content01 = mysqli_query($MySQL, $query_limit_content01) or die(mysqli_error($MySQL));
    $row_content01 = mysqli_fetch_assoc($content01);

    $all_content01 = mysqli_query($MySQL, $query_content01);
    $totalRows_content01 = mysqli_num_rows($all_content01);

    $totalPages_content01 = ceil($totalRows_content01 / $maxRows_content01) - 1;

    $queryString_content01 = "";
    if (!empty($_SERVER['QUERY_STRING'])) {
      $params = explode("&", $_SERVER['QUERY_STRING']);
      $newParams = array();
      foreach ($params as $param) {
        if (
          stristr($param, "pageNum_content01") == false &&
          stristr($param, "totalRows_content01") == false &&
          stristr($param, "Cate01") == false &&
          stristr($param, "Cate02") == false &&
          stristr($param, "Cate03") == false
        ) {
          array_push($newParams, $param);
        }
      }
      if (count($newParams) != 0) {
        $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
      }
    }
    $queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
    $queryString_content01 = $queryString_content01 . "&Cate01=" . $_GET['Cate01'] . "&Cate02=" . $_GET['Cate02'] . "&Cate03=" . $_GET['Cate03'];
    ?>
    <!-- start a templates -->
    <div class="container-fluid">
      <div class="row ">
        <div class="col d-flex align-items-center">
          <h2><?php // echo $Title; ?>系列活動</h2>
        </div>
        <div class="col d-flex align-items-center">
          <?php //CategorySelect("AAA ", "", ""); //由此設定是否啟動分類選項以及名稱
          ?>
          
<form action="" method="post" class="form-inline">
<span>選擇 ：</span>
<select class="form-control" name="DD" onChange="MM_jumpMenu(this,0)"   > 
<option value="index.php?Page=4-1"  <?php if($_GET['DD']==""){ echo "selected"; } ?> > 全部系列活動 </option>
<option value="index.php?Page=4-1&DD=Y" <?php if($_GET['DD']=="Y"){ echo "selected"; } ?>  >未過期之系列活動  </option>
<option value="index.php?Page=4-1&DD=N" <?php if($_GET['DD']=="N"){ echo "selected"; } ?>  >已過期之系列活動  </option>
</select>
</form>
          
        </div>
        <div class="col d-flex align-items-center">
          <!-- Start 導覽列  -->
          <span>第 <?php echo ($startRow_content01 + 1) ?> 到 <?php echo min($startRow_content01 + $maxRows_content01, $totalRows_content01) ?> 共 <?php echo $totalRows_content01 ?> 項</span>
          <!-- End 導覽列  -->
        </div>
      </div>
      <div class="row">
        <div class="table-responsive">
          <!-- start inner table -->
          <table class="table table-striped">
            <thead>
              <tr>
                <td>人氣推薦</td>
                <td>順序編號</td>

                <td>開始日期</td>
                <td>結束日期</td>
                <td>操作</td>
              </tr>
            </thead>
            <tbody>
              <?php do { ?>
                <tr>
                  <td nowrap><?php echo $row_content01['h01'];  ?></td>
                  <td nowrap><?php echo $row_content01['Sq'];  ?></td>
                  <td nowrap><?php echo $row_content01['d01'];  ?></td>
                  <td nowrap><?php echo $row_content01['d02'];  ?></td>
                  <td nowrap><?php if ($totalRows_content01 == 0) {  //如果一筆資料都沒有還是可以新增
                                $row_content01['Page'] = $_GET['Page'];
                                $row_content01['Sq'] = 1000;
                                SuperM_ins($row_content01, $template, $row_template['Security']);
                              } else {
                                //	ShowSqM( $row_content01,$template,$row_template['Security']);
                                SuperM_Button_3($row_content01, $template, $row_template['Security']);
                              } ?></td>
                  </td>
                </tr>
              <?php } while ($row_content01 = mysqli_fetch_assoc($content01)); ?>

            </tbody>
          </table>
          <!-- end inner table -->
        </div>

      </div>

      <div class="row">
        <div class="col d-flex justify-content-center">
          <!-- paginations -->
          <nav class="pt-3">
            <ul class="pagination justify-content-center">
              <?php if ($pageNum_content01 > 0) { // Show if not first page 
              ?>
                <li class="page-item">
                  <a class="page-link" href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, 0, $queryString_content01); ?>"><i class="fas fa-angle-double-left"></i></a>
                </li>
              <?php } // Show if not first page 
              ?>
              <?php if ($pageNum_content01 > 0) { // Show if not first page 
              ?>
                <li class="page-item">
                  <a class="page-link" href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, max(0, $pageNum_content01 - 1), $queryString_content01); ?>"><i class="fas fa-angle-left"></i></a>
                </li>
              <?php } // Show if not first page 
              ?>


              <?php if ($totalPages_content01 > 0) { // 如果總頁數大於1	
                for ($i = -4; $i <= 5; $i++) {
                  if (($_GET['pageNum_content01'] + $i) >= 0 and ($_GET['pageNum_content01'] + $i) <= $totalPages_content01) {
              ?>
                    <li class="page-item <?php if ($i == 0) {
                                            echo "active";
                                          } ?>"><a class="page-link" href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, $_GET['pageNum_content01'] + $i, $queryString_content01); ?>"><?php printf("%d ", $_GET['pageNum_content01'] + $i + 1); ?></a></li>

              <?php
                  }
                }
              } ?>

              <?php if ($pageNum_content01 < $totalPages_content01) { // Show if not last page 
              ?>
                <li class="page-item">
                  <a class="page-link" href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, min($totalPages_content01, $pageNum_content01 + 1), $queryString_content01); ?>"><i class="fas fa-angle-right"></i></a>
                </li>
              <?php } // Show if not last page 
              ?>
              <?php if ($pageNum_content01 < $totalPages_content01) { // Show if not last page 
              ?>
                <li class="page-item">
                  <a class="page-link" href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, $totalPages_content01, $queryString_content01); ?>"><i class="fas fa-angle-double-right"></i></a>
                </li>
              <?php } // Show if not last page 
              ?>
            </ul>
          </nav>
          <!--end paginations -->
        </div>
      </div>
    </div>
    <!-- end a templates -->
  <?php mysqli_free_result($content01);
  }

  //A 版結束 
} else {  //S版開始

  if ($_GET['demo'] == 'y') { ?>


  <?php } else {  ?>
    <?php $colname_content01 = "-1";
    if (isset($_GET['Page'])) {
      $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
    }

    $maxRows_content01 = $row_template['MaxRow'];
    $KeyID = str_replace($vowels, "**!!!**", substr($_GET[$templateS], 0, 50));
    if ($row_template['Extend'] == 'Y') {
      $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
    } else {
      $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ", $Table_ID, GetSQLValueString($colname_content01, "text"));
    }

    $content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
    $row_content01 = mysqli_fetch_assoc($content01);
    $totalRows_content01 = mysqli_num_rows($content01);
    //echo "S版啟動".$query_content01;
    ?>



    <div>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <img src="std_images/kl_c02.png" width="699" height="12" />
          </td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">
            <div class="cont">
              <div class="title"><span class="blue"><?php // echo $Title; ?>系列活動</span></div>
              <div class="left">
                <?php if ($row_content01['p01'] <> "") { ?>
                  <img src="UploadImages/<?php echo $row_content01['p01'];  ?>" width="155" title="<?php echo $row_content01['h01'];  ?>" alt="<?php echo $row_content01['h01'];  ?>" />
                <?php }  ?>
              </div>

              <div class="news1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="std_images/kl_icon2.gif" width="15" height="18" />
                      <span style="color:#03F;"> <?php echo $row_content01['h01'];  ?> </span>
                    </td>
                    <td align="right" class="red">
                      <?php if ($row_content01['d01'] <> "") {
                        echo "[" . substr($row_content01['d01'], 0, 10) . "]";
                      }  ?>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <div class="borderline"></div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" class="normal">
                      <?php echo $row_content01['c01'];  ?>

                      <?php if ($row_content01['h11'] <> "") {
                        $myyoutube = strstr($row_content01['h11'], "/watch?v=");
                        $youtube = str_replace("/embed/", $myyoutube, "https://www.youtube.com/embed/?rel=0");
                        $youtube = str_replace("/watch?v=", "/embed/", $youtube);
                      ?>
                        <iframe width="560" height="315" src="<?php echo $youtube; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      <?php } ?>
                      <?php if ($row_content01['h12'] <> "") {
                        $myyoutube = strstr($row_content01['h12'], "/watch?v=");
                        $youtube = str_replace("/embed/", $myyoutube, "https://www.youtube.com/embed/?rel=0");
                        $youtube = str_replace("/watch?v=", "/embed/", $youtube);
                      ?>
                        <iframe width="560" height="315" src="<?php echo $youtube; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      <?php } ?>
                      <?php if ($row_content01['h13'] <> "") {
                        $myyoutube = strstr($row_content01['h13'], "/watch?v=");
                        $youtube = str_replace("/embed/", $myyoutube, "https://www.youtube.com/embed/?rel=0");
                        $youtube = str_replace("/watch?v=", "/embed/", $youtube);
                      ?>
                        <iframe width="560" height="315" src="<?php echo $youtube; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      <?php } ?>



                      <?php if ($row_content01['f01'] <> "") {
                        $query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f01'], "text"));
                        $content0f = mysqli_query($MySQL, $query_content0f) or die(mysqli_error());
                        $row_content0f = mysqli_fetch_assoc($content0f);  ?>
                        <a href="GetDBfile.php?KeyID=<?php echo $row_content01['f01'];  ?> ">文件下載:<?php echo $row_content0f['filename']; ?></a><br>
                      <?php } ?>

                      <?php if ($row_content01['f02'] <> "") {
                        $query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f02'], "text"));
                        $content0f = mysqli_query($MySQL, $query_content0f) or die(mysqli_error());
                        $row_content0f = mysqli_fetch_assoc($content0f);  ?>
                        <a href="GetDBfile.php?KeyID=<?php echo $row_content01['f02'];  ?> ">文件下載:<?php echo $row_content0f['filename']; ?></a><br>
                      <?php } ?>

                      <?php if ($row_content01['f03'] <> "") {
                        $query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f03'], "text"));
                        $content0f = mysqli_query($MySQL, $query_content0f) or die(mysqli_error());
                        $row_content0f = mysqli_fetch_assoc($content0f);  ?>
                        <a href="GetDBfile.php?KeyID=<?php echo $row_content01['f03'];  ?> ">文件下載:<?php echo $row_content0f['filename']; ?></a><br>
                      <?php } ?>

                      <?php include("3button.php"); ?>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td><img src="std_images/kl_c03.png" width="699" height="12" /></td>
        </tr>
      </table>


      <?php //上一篇 下一篇功能 
      $query_sql = "SELECT * FROM GoWeb_BigTable where KeyID='$KeyID'";
      //echo $query_sql;
      $data = mysqli_query($MySQL, $query_sql) or die(mysqli_error($MySQL));
      $row_data = mysqli_fetch_assoc($data);
      $Cate01 = $row_data['Cate01'];
      $h01 = $row_data['h01'];

      $query_sqlU = "SELECT * FROM GoWeb_BigTable where Template='" . $template . "' " . $Pname . " and d01>'" . $row_content01['d01'] . "' order by d01";
      //echo $query_sqlU;
      $dataU = mysqli_query($MySQL, $query_sqlU) or die(mysqli_error($MySQL));
      $row_dataU = mysqli_fetch_assoc($dataU);


      $query_sqlD = "SELECT * FROM GoWeb_BigTable where Template='" . $template . "' " . $Pname . " and d01<'" . $row_content01['d01'] . "' order by d01 desc";
      //echo $query_sqlD;
      $dataD = mysqli_query($MySQL, $query_sqlD) or die(mysqli_error($MySQL));
      $row_dataD = mysqli_fetch_assoc($dataD);
      ?>


      <ul class="pagination page_number">
        <?php if ($row_dataU['KeyID'] <> "") { ?>
          <li><a href="index.php?Page=<?php echo $_GET['Page']; ?>&<?php echo $templateS . "=" . $row_dataU['KeyID']; ?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
        <?php } ?>
        <?php if ($row_dataD['KeyID'] <> "") { ?>
          <li><a href="index.php?Page=<?php echo $_GET['Page']; ?>&<?php echo $templateS . "=" . $row_dataD['KeyID']; ?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
        <?php } ?>
      </ul>


    </div>







<?php mysqli_free_result($content01);
  }
}  //S版結束
?>