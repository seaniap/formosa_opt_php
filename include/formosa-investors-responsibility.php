<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">履行企業社會責任情形</h3>
            </div>
        </div>
        <!-- start table -->
        <div class="u-pb-100">
            <div class="container">
                <!-- <h4 class="c-title-underline u-font-16 u-md-font-22">履行企業社會責任情形</h4>。 -->
                <div class="l-tablesSroller-wrapper">
                    <table class="table table-bordered l-table u-mb-000">
                        <thead>
                            <tr>
                                <th colspan="1" rowspan="2" class="u-p-100 l-table-vertical-align-middle" style="min-width: 250px;">
                                    評估項目
                                </th>
                                <th colspan="3" rowspan="1" class="u-p-100 l-table-vertical-align-middle" style="min-width: 500px;">
                                    運作情形
                                </th>
                                <th colspan="1" rowspan="2" class="u-p-100 l-table-vertical-align-middle">
                                    與上市上櫃公司<br>誠信經營守則差異情形及原因
                                </th>
                            </tr>
                            <tr>
                                <th colspan="1" rowspan="1" class="u-bdr-left-white u-p-100 l-table-vertical-align-middle border-left" style="min-width: 50px;">
                                    是
                                </th>
                                <th colspan="1" rowspan="1" class="u-p-100 l-table-vertical-align-middle" style="min-width: 50px;">
                                    否
                                </th>
                                <th colspan="1" rowspan="1" class="u-p-100 l-table-vertical-align-middle">
                                    摘要說明
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level01">
                                        <li>一、&nbsp;公司是否設置推動企業社會責任專（兼）職單位，依重大性原則，進行與公司營運相關之環境、社會或公司治理議題之風險評估，訂定相關風險管理政策或策略，並揭露於公司網站及年報?
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center text-center" colspan="1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;本公司已設立財會部為公司治理兼職單位，負責公司治理相關之業務，並指定財會主管張立徽副總擔任公司治理主管，負責公司治理、企業社會責任、誠信經營、環境永續、風險管理等議題之政策、管理方針及目標之訂定及監督。各相關部門會依職掌內容進行風險評估及擬定因應措施，並每年定期檢討。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level01">
                                        <li>二、&nbsp;公司是否設置推動企業社會責任專（兼）職單位，並由董事會授權高階管理階層處理，及向董事會報告處理情形？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center text-center" colspan="1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;由稽核室及管理室統籌，推動企業社會責任相關工作。本公司除向員工宣導節能與資源回收外，亦於門市販售環保材質之鏡框，並要求各門市定期參與所在社區之社區打掃服務，回饋社會以盡相關之企業社會責任。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr>
                                <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                    <ul class="u-list-style--custom-level01">
                                        <li>三、&nbsp;環境議題</li>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>1.&nbsp;公司是否依其產業特性建立合適之環境管理制度？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center text-center" colspan="1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>1.&nbsp;本公司主要營業項目為眼鏡零售業，對環境衝擊遠低於一般製造業，故主要友善環境作為著重於門市節電與紙本文書減量作業，為發展永續環境，提升各項資源之利用效率，已建立各項節能管理制度。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>2.&nbsp;公司是否致力於提升各項資源之利用效率，並使用對環境負荷衝擊低之再生物料？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center text-center" colspan="1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>2.&nbsp;為提升各項資源之利用效率，公司成立電子文管中心，採線上簽核文件，減少紙本使用，並持續加強宣導減少非必要列印，如必須列印則應採雙面或使用回收紙列印，新年度希望能達到用紙量減少5%，回收紙張使用比率達20%。用電部分，除建立員工節能習慣外，同時新年度減少冷氣使用時數，提高室內平均溫度，希望能達到用電量較前一年度減少3%，109年度電費較前一年度減少0.6%。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>3.&nbsp;公司是否評估氣候變遷對企業現在及未來的潛在風險與機會，並採取氣候相關議題之因應措施？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center text-center" colspan="1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>3.&nbsp;全球暖化所造成之溫度上升將增加門市冷氣耗電量而提高公司經營成本外，為有效減緩衝擊，本公司積極參與政府各項節能減碳政策，並採取冬夏制服以減少同仁營業時之辛苦。另編制危機應變小組，處理天災對門市之衝擊。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>4.&nbsp;公司是否統計過去兩年溫室氣體排放量、用水量及廢棄物總重量，並制定節能減碳、溫室氣體減量、減少用水或其他廢棄物管理之政策？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center text-center" colspan="1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>4.&nbsp;本公司參與經濟部辦理商業服務業溫室氣體減量示範輔導，推動門市改善能源使用狀況、能耗設備節電潛力、能源費用支出節流之運作並定期檢討，109年已實現節電量373,541度之成績，110年預計持續擴大實施門市，達成節電度數提高一倍之目標。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr>
                                <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                    <ul class="u-list-style--custom-level01">
                                        <li>四、&nbsp;社會議題</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>1.&nbsp;公司是否依照相關法規及國際人權公約，制定相關之管理政策與程序？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan=" 1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            1.&nbsp;公司尊重並支持(聯合國世界人權宣言)、(聯合國全球盟約)與(國際勞工組織公約)之勞動標準，據此訂定本公司相關工作規則，內容包括公平不歧視、營造良好的勞資關係、禁用童工、禁止強迫及強制勞動、工時規範、符合基本薪資、建立安全與健康的工作環境、教育訓練、供應商管理、員工權益溝通管道暢通等，並於每年各級基層、幹部訓練會議持續宣導。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>2.&nbsp;公司是否訂定及實施合理員工福利措施(包括薪酬、休假及其他福利等)，並將經營績效或成果適當反映於員工薪酬？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan=" 1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            2.&nbsp;為充分照顧員工保障其生活條件、提供良好勞動條件，滿足員工需求，除依法提供基本保障外，亦成立職工福利委員會，辦理各項職工福利活動及補助。本公司訂有相關薪酬、各項獎金及考績辦法，將工作績效與個人的薪酬作有效連結。

                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>3.&nbsp;公司是否提供員工安全與健康之工作環境，並對員工定期實施安全與健康教育？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan=" 1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            3.&nbsp;本公司為保障全體員工之安全健康，訂定並於公司網站揭露「員工福利、工作環境與人身安全保護措施」，加上定期講授有關安全衛生概念及安全衛生工作守則、緊急事故應變處理、消防及急救常識、其他與員工作業有關之安全衛生知識等。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>4.&nbsp;公司是否為員工建立有效之職涯能力發展培訓計畫？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan=" 1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            4.&nbsp;本公司設有完整培訓制度，並訂有內部創業辦法，並透過教育訓練協助員工考取工作所需之專業證照。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>5.&nbsp;對產品與服務之顧客健康與安全、客戶隱私、行銷及標示，公司是否遵循相關法規及國際準則，並制定相關保護消費者權益政策及申訴程序？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan=" 1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            5.&nbsp;本公司商品皆按商品標示法進行標示，同時設置0800服務諮詢專線，由專人針對客戶提出問題進行追蹤改善，並於公司網站揭露「檢舉辦法」，設置專門電子信箱提供員工及消費者舉報任何不正當的從業行為，並甫公司指定適當職權之管理階層親自處理。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>6.&nbsp;公司是否訂定供應商管理政策，要求供應商在環保、職業安全衛生或勞動人權等議題遵循相關規範，及其實施情形？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan=" 1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            6.&nbsp;本公司已訂定「供應商管理政策」，並由採購部門對供應商進行相關企業社會責任之評估。本公司在與供應商簽訂合約前皆會進行評估，若其有對環境與社會顯著負面影響之行為時，本公司將主動停止對供應商之相關交易。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>7.&nbsp;公司網站及年報是否揭露員工人身安全與工作環境的保護措施與其實施情形?
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan=" 1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            7.&nbsp;本公司針對災害及搶劫事件，定期實施宣導及演練，並將相關辦法及範例備於公司內部與外部網站，並於各門市及廠房皆設有系統保全，與系統保全公司連線戒備。演練範例詳附註二。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>8.&nbsp;公司是否參考國際人權公約，制訂保障人權政策與具體管理方案，並揭露於公司網站或年報?
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan=" 1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            8.&nbsp;本公司遵守相關企業經營與勞動法規，並依法訂定「工作規則」等相關管理政策與程序，以保障員工之合法權益。召募員工時未限制性別、宗教、種族或政治立場，透過公開招募的方式，用人唯才。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    無重大差異
                                </td>
                            </tr>
                            <tr>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level01">
                                        <li>五、&nbsp;公司是否參考國際通用之報告書編製準則或指引，編製企業社會責任報告書等揭露公司非財務資訊之報告書？前揭報告書是否取得第三方驗證單位之確信或保證意見？
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    V
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                </td>
                                <td class="" colspan="1" rowspan="1">
                                    <ul class="u-list-style--custom-level02">
                                        <li>
                                            &nbsp;本公司尚未編製企業社會責任報告書。
                                        </li>
                                    </ul>
                                </td>
                                <td class="text-center" colspan="1" rowspan="1">
                                    尚在評估中
                                </td>
                            </tr>
                            <tr class="u-bg-gray-100">
                                <td class="" colspan="5" rowspan="1">
                                    <ul class="u-list-style--custom-level01">
                                        <li>六、&nbsp;公司如依據「上市上櫃公司企業社會責任實務守則」定有本身之企業社會責任守則者，請敘明其運作與所定守則之差異情形：<br>
                                            本公司於104年通過「企業社會責任實務守則」，目前適用於環保，社會服務，消費者權益、社會責任活動以及勞工安全衛生等眾多層面，實際運作與
                                            「上市上櫃公司企業社會責任實務守則」並無重大差異。
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="" colspan="5" rowspan="1">
                                    <ul class="u-list-style--custom-level01">
                                        <li>七、&nbsp;其他有助於瞭解企業社會責任運作情形之重要資訊：<br>
                                            本公司歷年活動大記事如下附註一所示。
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">附註一</h4>
                <table class="table table-bordered l-table u-mb-000">
                    <thead>
                        <tr>
                            <th colspan="2">
                                寶島眼鏡公益活動大紀事
                            </th>
                        </tr>
                        <tr>
                            <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-bottom-black">
                                年&nbsp;份
                            </th>
                            <th class="u-bg-gray-300 u-text-gray-800 text-center border-left u-bdr-left-black u-bdr-bottom-black">
                                活&nbsp;動&nbsp;內&nbsp;容
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowspan="2" class="text_fristline">
                                2014
                            </td>
                            <td valign="top" class="text_line">
                                2014年『寶島有eye 視界美好』關懷清寒學子，捐贈2000付眼鏡給衛生福利部，協助清寒學子免費配鏡。
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                協助嘉義長庚醫院台東池上鄉、大武鄉醫療服務-提供視力檢測等義診項目。
                            </td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td rowspan="3" class="text_fristline">
                                2015
                            </td>
                            <td valign="top" class="text_line">
                                連續第五年『寶島有eye視界美好』捐贈衛生福利部2000付眼鏡關懷清寒學子。
                            </td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                與長庚醫院醫療團隊合作至台東偏遠地區提供視力健檢及配鏡服務。
                            </td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                協助家扶基金會宣導關懷守護兒童議題並獲頒感謝狀。
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="text_fristline">
                                2016
                            </td>
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                連續第六年『寶島有eye視界美好』捐贈衛生福利部2000付眼鏡關懷清寒學子。
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                協助家扶基金會宣導關懷守護兒童議題並獲頒感謝狀。
                            </td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td rowspan="2" class="text_fristline">
                                2017
                            </td>
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                連續第七年『寶島有eye視界美好』捐贈衛生福利部2000付眼鏡關懷清寒學子。
                            </td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                協助家扶基金會宣導關懷守護兒童議題並獲頒感謝狀。
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" class="text_fristline">
                                2018
                            </td>
                            <td valign="top" class="text_line">
                                連續第八年『寶島有eye視界美好』捐贈衛生福利部2000付眼鏡關懷清寒學子。
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                與財團法人台灣防盲基金會合作，前往台東卑南鄉服務-提供視力檢測與免費配鏡等義診項目。
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="text_line border-left u-bdr-left-black">
                                寶島61間新北門市共同參與新北市政府舉辦的『新北敬老眼鏡專案』，幫助年滿65歲的新北市民免費視力檢查、配鏡。
                            </td>
                        </tr>
                        <tr class="u-bg-gray-100">
                            <td class="text_fristline">
                                2019
                            </td>
                            <td valign="top" class="text_line">
                                連續第九年『寶島有eye視界美好』捐贈衛生福利部2000付眼鏡關懷清寒學子，今年擴大長者同受惠。
                            </td>
                        </tr>
                        <tr>
                            <td class="text_fristline">
                                2020
                            </td>
                            <td valign="top" class="text_line">
                                連續第十年『寶島有eye視界美好』智慧巡眼公益服務城鄉普及計畫，深入偏鄉服務當地民眾，提供提供視力檢測與免費配鏡等義診項目。
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="u-py-100 u-pb-400">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">檔案下載</h4>
                <div class="row">
                    <div class="col-auto">
                        <a target="_blank" class="c-btn c-btn--download c-btn-gray-800 u-text-black px-3" href="download/pdf/investors/15.履行企業社會責任情形-附註二-防火防搶演練範例.pdf">
                            <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050" style="width: 17px;">
                            <span>防火防搶演練範例</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- end table -->


    </section>
</main>