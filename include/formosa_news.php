<style>
.formosaResetUl ul {
    list-style: disc;
    padding-left: 40px;
}

.formosaResetUl ul ul {
    list-style: circle;
}

.formosaResetUl ul ul ul {
    list-style-type: square;
}

.formosaResetUl table {
    max-width: 100%;
}

.formosaResetUl table img {
    max-width: 100%;
    height: auto !important;
}
</style>
<?php require_once('../Connections/MySQL.php'); ?>
<?php
$template = "news01.php";
$templateS = "news01";
// 判斷是預設是A版還是S版
// 預設 A 版
if ($_GET[$templateS] == "" and $row_template['Extend'] == "Y") {
    //A 版開始 demo page
    if ($_GET['demo'] == 'y') { ?>

<main class="wrapper">
    <section>
        <div class="u-pt-100">
            <div class="container">
                <h3 class="c-title-center u-mb-125">最新消息</h3>
            </div>
        </div>
        <!-- START TAB -->
        <div class="u-pb-100">
            <div class="container">
                <div class="position-relative">
                    <!-- <button type="button" class="v-tabBtnSlick-prev"><i class="fal fa-angle-left"></i></button> -->
                    <div class="c-tab-verE v-tabBtnSlick">
                        <!-- <div class="c-tab-container"><a href="" class="c-tab-linkE">SOUSOU</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">迪士尼</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">MIMA</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">D’eesse</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會員日</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">蔡司</a></div> -->
                        <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島</a></div>
                        <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會員日</a></div>
                        <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會員日</a></div>
                        <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會員日</a></div>
                        <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會0505</a></div>
                        <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會0606</a></div>
                    </div>
                    <!-- <button type="button" class="v-tabBtnSlick-next"><i class="fal fa-angle-right"></i></button> -->
                </div>
            </div>
        </div>
        <!-- END TAB -->
        <div class="u-pt-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_01.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">
                                                【EYE注意】漸進多焦鏡片怎麼挑? 林友祺醫師報你知</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_02.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">
                                                [活動展延通知]會員徵召令-新舊會員更新資料賺點去 千萬獎金大放送</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_03.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">寶島眼鏡會員點數展延通知
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_04.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">全民做防疫，線上安心配！
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_05.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">
                                                【EYE注意】漸進多焦鏡片怎麼挑? 林友祺醫師報你知</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_06.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">
                                                [活動展延通知]會員徵召令-新舊會員更新資料賺點去 千萬獎金大放送</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_07.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">寶島眼鏡會員點數展延通知
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_08.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">全民做防疫，線上安心配！
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_09.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">
                                                【EYE注意】漸進多焦鏡片怎麼挑? 林友祺醫師報你知</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_10.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">
                                                [活動展延通知]會員徵召令-新舊會員更新資料賺點去 千萬獎金大放送</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_11.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">
                                                【嬌生】VITA悅氧月拋2盒現折$250,4盒現折$600、VITA悅氧散光月拋第2盒5折</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/news/news_12.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <span class="u-mb-100 u-text-gray-700">2021/05/27</span>
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">
                                                一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- START 頁籤 -->
        <div class="u-py-100">
            <div class="container">
                <nav>
                    <ul class="c-pagination justify-content-center align-items-center">
                        <li class="c-pagination-item">
                            <a href="#">
                                <i class="icon-arrow_to_left"></i>
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a href="#">
                                <i class="icon-arrow_left"></i>
                            </a>
                        </li>
                        <li class="c-pagination-item active"><a href="#">1</a></li>
                        <li class="c-pagination-item"><a href="#">2</a></li>
                        <li class="c-pagination-item"><a href="#">3</a></li>
                        <li class="c-pagination-item"><a href="#">4</a></li>
                        <li class="c-pagination-item"><a href="#">5</a></li>
                        <li class="c-pagination-item">...</li>
                        <li class="c-pagination-item"><a href="#">10</a></li>
                        <li class="c-pagination-item">
                            <a href="#">
                                <i class="icon-arrow_right"></i>
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a href="#">
                                <i class="icon-arrow_to_right"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- END 頁籤 -->
    </section>
</main>
<?php } else {  ?>
<?php
        $currentPage = $_SERVER["PHP_SELF"];

        $maxRows_content01 = $row_template['MaxRow'];
        $pageNum_content01 = 0;
        if (isset($_GET['pageNum_content01'])) {
            $pageNum_content01 = $_GET['pageNum_content01'];
        }
        $startRow_content01 = $pageNum_content01 * $maxRows_content01;

        $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $maxRows_content01 = $row_template['MaxRow'];
        $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        if ($_GET['Cate01'] <> "") {
            $query_content01 = $query_content01 . "AND Cate01='" . $_GET['Cate01'] . "'";
        }

        if ($_GET['h10'] <> "") {
            $query_content01 = $query_content01 . "AND h10 like '%" . $_GET['h10'] . "%'";
        }
		
		
		 if ($_GET['search'] <> "") {
            $query_content01 = $query_content01 . "AND h01 like '%" . $_GET['search'] . "%'";
        }

			if($_SESSION['Mode']==""){
		 $query_content01 = $query_content01 . "AND d03<=now() and (h20 is NULL or h20='' or h20>=now() ) and ( h15='Y' or h15 is NULL)  ";
				// 
			}
 $query_content01 = $query_content01 ." and (ProductID is NULL  or ProductID!='預覽'  or ProductID is NULL  ) "; 
        //排序
        $query_content01 = $query_content01 . " order by Sq desc, d01 desc ";
	

        $query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
        //echo $query_limit_content01 ;
        $content01 = mysqli_query($MySQL, $query_limit_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);
        if (isset($_GET['totalRows_content01'])) {
            $all_content01 = mysqli_query($MySQL, $query_content01);
            $totalRows_content01 = mysqli_num_rows($all_content01);
        } else {
            $all_content01 = mysqli_query($MySQL, $query_content01);
            $totalRows_content01 = mysqli_num_rows($all_content01);
        }
        $totalPages_content01 = ceil($totalRows_content01 / $maxRows_content01) - 1;

        $queryString_content01 = "";
        if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
                if (
                    stristr($param, "pageNum_content01") == false &&
                    stristr($param, "totalRows_content01") == false
                ) {
                    array_push($newParams, $param);
                }
            }
            if (count($newParams) != 0) {
                $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
            }
        }
        $queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
        // A版開始 real page
        ?>
<!-- ============= 主要內容區 ============= -->
<section>
    <!-- END TAB -->
    <div class="u-pt-100">
        <div class="container">
        
<?php  SuperM_ins($row_content01,$template,$row_template['Security']); ?>        
            <div class="row">
                <!--start loop -->
                

                
                <?php do {  ?>
                <?php if ($row_content01['l01'] == "") {
                                    $newlink = "index.php?Page=" . $row_content01['Page'] . "&" . $templateS . "=" . $row_content01['KeyID'];
                                    $target = "";
                                } else {
                                    $ext = " (" . end(explode('.', $row_content01['l01'])) . ")";
                                    $newlink = $row_content01['l01'];
                                    $target = "_blank";
                                }
                                ?>

                <div class="col-md-4 col-lg-3">
                    <div class="c-card c-card-link">
                        <div class="c-card-layout">
                            <!-- 3 button -->
                            <?php 
											 if($totalRows_content01==0){ echo "相關資訊正在準備中";} 
											include("3buttonA.php"); ?>
                            <div class="c-card-head c-card-head-aspect-ratio">

                                <a href="<?php echo $newlink; ?>">
                                    <img src="UploadImages/<?php echo $row_content01['p01'];  ?>"
                                        class="c-card-head-img" title="<?php echo $row_content01['h01'];  ?>"
                                        alt="<?php echo $row_content01['h01'];  ?>" />
                                </a>
                            </div>
                            <div class="c-card-body c-card-body__height-adjust">
                                <div class="c-card-body-container">
                                    <div class="c-card-body-layout">
                                        <span class="u-mb-100 u-text-gray-700">

                                            <?php if($row_content01['d01']<>""){ echo date("Y/m/d",strtotime($row_content01['d01']));}?>
                                            <?php if($row_content01['d02']<>""){ ?>
                                            ~
                                            <?php } ?>
                                            <?php if($row_content01['d02']<>""){ echo date("Y/m/d",strtotime($row_content01['d02']));}?>
                                        </span>
                                        <a href="<?php echo $newlink; ?>">
                                            <h4 class="c-card-body-title u-mb-000" style="height: 72px;">

                                                <?php echo $row_content01['h01']; ?>

                                            </h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } while($row_content01 = mysqli_fetch_assoc($content01)); ?>
                <!--end loop -->
            </div>
        </div>
    </div>
    <!-- START 頁籤 -->
    <div class="u-py-100">
        <div class="container">
            <?php include('formosa_paginations.php'); ?>
        </div>
    </div>
    <!-- END 頁籤 -->
</section>
<!-- =============end 主要內容區 ============= -->


<?php
        mysqli_free_result($content01);
    }
    //A 版結束 
} else {  //S版開始 
    if ($_GET['demo'] == 'y') { ?>
<main class="wrapper">
    <section class="u-py-200">
        <div class="u-pt-100">
            <div class="container">
                <h3 class="c-title-center u-mb-125">最新消息</h3>
            </div>
        </div>
        <div class="container">
            <p class="u-text-gray-500"><span>2021/10/08</span> ~ <span>2021/12/08</span></p>
            <h4 class="c-title-underline u-font-18 u-md-font-24">【振興樂透包】GUCCI等品牌精選墨鏡▸8折後再現折$1,000(最高現折$4,000)
            </h4>
        </div>
        <div class="container">
            <!-- 預留編輯器空間 -->
            <br>
            <br>
            <br>
            <br>
            <br>
            <!-- 預留編輯器空間 -->
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-3"><button class="c-btn c-btn-blue-highlight c-btn--contained w-100">回列表</button>
                </div>
            </div>
        </div>
    </section>
</main>
<?php } else {  ?>
<?php
        $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $maxRows_content01 = $row_template['MaxRow'];
        $KeyID = str_replace($vowels, "**!!!**", $_GET[$templateS]);
        if ($row_template['Extend'] == 'Y') {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        } else {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        }

	if($_SESSION['Mode']==""){
		 $query_content01 = $query_content01 . "AND d03<=now() and (h20 is NULL or h20='' or h20>=now() ) and (ProductID='' or ProductID!='預覽' or ProductID is NULL   ) ";
			}

  //and (h15='Y' or h15 is NULL) 
        $content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);
        $totalRows_content01 = mysqli_num_rows($content01);
       echo "S版啟動".$query_content01;
		
//如果沒有資料就導回首頁
if($row_content01['KeyID']==""){ ?>
<script>
location.href='index.php';
</script>	
<?php	
}
		
		
		
        ?>
<section class="u-pb-400 u-pt-700 ">
    <div class="u-pt-100">
        <div class="container">
            <h3 class="c-title-center u-mb-125">最新消息</h3>
        </div>
    </div>
    <div class="container">
        <p class="u-text-gray-500">
            <span><?php
		if($row_content01['d01']<>""){
		echo date("Y/m/d",strtotime($row_content01['d01']));}?></span>
            <?php if($row_content01['d02']<>""){ echo "~"; } ?>
            <span><?php 
		if($row_content01['d02']<>""){
		echo date("Y/m/d",strtotime($row_content01['d02']));}?></span>
        </p>

        <h4 class="c-title-underline u-font-18 u-md-font-24">
            <?php echo $row_content01['h01']; ?>
        </h4>
		<?php 
		if($row_content01['ProductID']!="預覽"){
		SuperM_edit( $row_content01,$template,$row_template['Security']); 
		}
		?>
    </div>
    <div class="container formosaResetUl">
        <!-- 預留編輯器空間 -->
        <div class="row">
            <div class="col">
                <?php echo $row_content01['c01']; ?>
                <?php echo $row_content01['c02']; ?>
                <?php echo $row_content01['c03']; ?>
            </div>
        </div>

        <!-- 預留編輯器空間 -->
    </div>
    <div class="container u-mt-400">
        <div class="row justify-content-center">
            <div class="col-sm-3">

                <!--      <img src="UploadImages/<?php //echo $row_content01['p01'];  ?>" width="185"  />       -->

                <a href="index.php?Page=2" class="c-btn c-btn-blue-highlight c-btn--contained w-100">回列表</a>
            </div>
        </div>
    </div>
</section>

<?php mysqli_free_result($content01);
    }
}  //S版結束
?>