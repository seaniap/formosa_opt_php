<div class="l-footer-cookie-bar d-none">
  <div class="container-fluid">
    <div class="mx-auto">
      <div class="d-flex align-items-center justify-content-center">
        <p class="u-mb-000">本網站使用cookies於廣告、社群與分析等用途，藉以增進您的使用體驗，繼續使用本網站即代表您同意使用cookies，請參閱我們的<a href="index.php?Page=B-9">隱私權條款</a>以獲得更多資訊。</p>
        <a href="#" class="ml-3 px-3 l-footer-cookie-btn">同意</a>
      </div>
    </div>
  </div>
</div>
<div class="c-sideLink-list js-sticky" id="sideLinkFooter">
    <a id="link_social_YT" href="https://www.eyesmart.com.tw/link/6262/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=Gotop&utm_term=Youtube" target="_blank" class="c-sideLink-list-link d-none d-xl-block">
        <div class="c-sideLink-list-link-icon">
            <img src="assets/img/index/iconYoutube.svg" alt="">
        </div>
    </a>
    <a  id="link_social_FB" href="https://www.eyesmart.com.tw/link/6263/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=Gotop&utm_term=Facebook" target="_blank" class="c-sideLink-list-link d-none d-xl-block">
        <div class="c-sideLink-list-link-icon">
            <img src="assets/img/index/iconFacebook.svg" alt="">
        </div>
    </a>
    <a  id="link_social_IG" href="https://www.eyesmart.com.tw/link/6264/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=Gotop&utm_term=Instagram" target="_blank" class="c-sideLink-list-link d-none d-xl-block">
        <div class="c-sideLink-list-link-icon">
            <img src="assets/img/index/iconInstagram.svg" alt="">
        </div>
    </a>
    <a id="link_tag_id3" href="javascript:void(0)" class="c-sideLink-list-link d-none d-xl-block">
        <div class="c-sideLink-list-link-icon">
            <img src="assets/img/index/iconEyemarket.svg" alt="">
        </div>
    </a>
    <a href="" id="goTop" class="c-sideLink-list-link">
        <div class="c-sideLink-list-link-icon">
            <img src="assets/img/index/iconTop.svg" alt="">
        </div>
    </a>
</div>
<div class="u-bg-blue-500 u-py-100" id="footerAnchor">
    <div class="container">
        <div class="d-flex w-100 flex-column flex-lg-row justify-content-between">
            <div class="d-flex flex-column justify-content-center">
                <!-- <img src="assets/img/logo.png" alt="" class="img-fluid l-footer-logo"> -->
                <div class="l-footer-logo d-flex w-100 justify-content-between justify-content-lg-start">
                    <img src="assets/img/logo_mb_txt.svg"
                        class="img-fluid l-footer-logo-txt u-mr-100 align-self-center">
                    <img src="assets/img/logo_mb_deco.png" class="img-fluid l-footer-logo-deco align-self-end">
                </div>
                <p class="u-text-white u-font-14 u-mb-050">地址：新北市汐止區新台五路一段97號16樓</p>
                <p class="u-text-white u-font-14 u-mb-050">免付費服務專線：0800-251-257</p>
                <p class="u-text-white u-font-14 u-mb-050">Email：formosa_service@ms.formosa-opt.com.tw</p>
                <div class="">
                    <a href="https://www.eyesmart.com.tw/link/6258/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=Footer&utm_term=Youtube" target="_blank" rel="noreferrer noopener">
                        <img src="assets/img/index/iconYoutube_white.svg" alt=""
                            class="img-fluid l-footer-social u-mr-050">
                    </a>
                    <a href="https://www.eyesmart.com.tw/link/6259/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=Footer&utm_term=Facebook" target="_blank" rel="noreferrer noopener">
                        <img src="assets/img/index/iconFacebook_white.svg" alt=""
                            class="img-fluid l-footer-social u-mr-050">
                    </a>
                    <a href="https://www.eyesmart.com.tw/link/6260/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=Footer&utm_term=Instagram" target="_blank" rel="noreferrer noopener">
                        <img src="assets/img/index/iconInstagram_white.svg" alt=""
                            class="img-fluid l-footer-social u-mr-050">
                    </a>
                    <a id="link_tag_id5" href="javascript:void(0)">
                        <img src="assets/img/index/iconEyemarket_white.svg" alt=""
                            class="img-fluid l-footer-social u-mr-050">
                    </a>
                </div>
            </div>
            <div class="d-none d-lg-flex align-items-center">
                <a href="https://www.eyesmart.com.tw/link/6266/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=Footer&utm_term=iOSdownload" title="下載寶島App" target="_blank"
                    rel="noreferrer noopener">
                    <img src="assets/img/index/ios.svg" alt="" class="img-fluid l-footer-download u-mr-100">
                </a>
                <a href="https://www.eyesmart.com.tw/link/6267/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=Footer&utm_term=Androiddownload" title="下載寶島App" target="_blank"
                    rel="noreferrer noopener">
                    <img src="assets/img/index/android.svg" alt="" class="img-fluid l-footer-download">
                </a>
            </div>
        </div>
    </div>
</div>
<div class="u-bg-white u-py-050">
    <div class="container clearfix">
        <p class="u-mb-000"><a class="u-mb-000 u-font-14 float-lg-left u-text-gray-600"
                href="index.php?Page=B-9">隱私權條款</a></p>
        <p class="u-mb-000 u-font-14 float-lg-right u-text-gray-600">寶島光學科技股份有限公司 | <br class="d-md-none">寶聯光學股份有限公司
            版權所有
            轉載必究</p>
    </div>
</div>
<script type="text/javascript">
$(function(){
	$("#link_social_YT").click(function(){
		qg('event', "N_social_clicked", {"social_name": "YT"});
	});

    $("#link_social_FB").click(function(){
    	qg('event', "N_social_clicked", {"social_name": "FB"});
    });

    $("#link_social_IG").click(function(){
    	qg('event', "N_social_clicked", {"social_name": "IG"});
    });

    $("#link_tag_id3").click(function(){
    	qg('event', "N_social_clicked", {"social_name": "EYESmart"});
    });
});
</script>
