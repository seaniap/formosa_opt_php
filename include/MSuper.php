<?php require_once('../Connections/AdminMySQL.php'); ?>
<?php require_once('../lib/MTemplateHeader.php'); ?>
<?php


$colname_TemplateDetail = "-1";
if (isset($_GET['Template'])) {
	$colname_TemplateDetail = (get_magic_quotes_gpc()) ? $_GET['Template'] : addslashes($_GET['Template']);
}

$query_TemplateDetail = sprintf("SELECT * FROM " . $Table_ID . "_TemplateDetail WHERE Template = %s ORDER BY Sq ASC", GetSQLValueString($colname_TemplateDetail, "text"));
$TemplateDetail = mysqli_query($MySQL, $query_TemplateDetail) or die(mysqli_error($MySQL));
$row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail);
$totalRows_TemplateDetail = mysqli_num_rows($TemplateDetail);
?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<link rel="stylesheet" href="plugins/fontawesome5/css/fontawesome-all.min.css">
	<style type="text/css">
		.style11 {
			color: #2271a9;
			font-family: "微軟正黑體", Arial, Helvetica, sans-serif;
			font-style: normal;
			line-height: 15px;
			list-style-type: square;
			font-size: 16px;
		}

		.style11.red {
			color: #d62121;
		}

		.custom-file-label::after {
			content: "瀏覽檔案";
		}
		#targetp01, #targetp02, #targetp03{
			border: 1px #ccc solid;
		}

	</style>
	<link rel="stylesheet" type="text/css" href="../themes/flora/flora.all.css" media="screen" />
	<script src="src/js/jscal2.js"></script>
	<script src="src/js/lang/cn.js"></script>
	<script src="ckeditor/ckeditor.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="src/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="src/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="src/css/steel/steel.css" />

	<!--
		<script type="text/javascript" src="../js/jquery-1.2.6.pack.js"></script>
		<script type="text/javascript" src="../../main.js"></script>
		<script type="text/javascript" src="../js/ui.core.js"></script>
		<script type="text/javascript" src="../js/ui.datepicker.js"></script>
		<script type="text/javascript" src="../js/i18n/ui.datepicker-zh-TW.js"></script>
		-->
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 my-3">
				<!-- from start -->
				<form method="post" name="form1" action="MSuper.php" enctype="multipart/form-data">
					<?php do { ?>
						<!-- start a group -->
						<div class="form-group">
							<label for="<?php echo $row_TemplateDetail['FieldID']; ?>"><?php echo $row_TemplateDetail['FieldTitle']; ?></label>
                            
<?php
if(strstr($row_TemplateDetail['FieldTitle'],"Tag")){ 

$query_contentT1 = " SELECT * FROM GoWeb_BigTable WHERE Page = '2' AND Template='tab01.php'  order by Sq  LIMIT 0, 10";
$contentT1 = mysqli_query($MySQL,$query_contentT1) or die(mysqli_error($MySQL));
//$row_contentT1 = mysqli_fetch_assoc($contentT1); ?>
<br>
<?php
while ($row_contentT1 = mysqli_fetch_assoc($contentT1)){
?>	


 <span><?php echo $row_contentT1['h01']; ?></span>	

<?php }

}


?>                            
                            
                            
                            
                            
							<?php if ($row_TemplateDetail['Type'] == "Text") {

								//如果是Sq 欄位給最大值+10
								if ($row_TemplateDetail['FieldID'] == "Sq" and $_GET['Action'] == "ins") {

									$query_content01 = sprintf("SELECT max(Sq)+10 max FROM %s_BigTable WHERE ProductID is NULL and Page = %s and Template= %s ", $Table_ID, GetSQLValueString($_GET['Page'], "text"), GetSQLValueString($_GET['Template'], "text"));
									//echo $query_content01;								
									$content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
									$row_content01 = mysqli_fetch_assoc($content01);

									$row_MiddleBigTable[$row_TemplateDetail['FieldID']] = $row_content01['max'];
								}

	
// 	最新消息meta title 預設值
//寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器	
if($_GET['Template']=="news01.php" and $row_TemplateDetail['FieldID']=="h03" and $row_MiddleBigTable[$row_TemplateDetail['FieldID']]=="" ){
	$row_MiddleBigTable[$row_TemplateDetail['FieldID']]="|寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器";

}	
	
if($_GET['Template']=="news01.php" and $row_TemplateDetail['FieldID']=="h02" and $row_MiddleBigTable[$row_TemplateDetail['FieldID']]=="" ){
	$row_MiddleBigTable[$row_TemplateDetail['FieldID']]="寶島眼鏡會員獨享活動、最新聯名鏡框、歐美日韓的鏡框、亞洲最新時尚鏡框、時尚眼鏡、太陽眼鏡、時尚墨鏡、隱形眼鏡、彩片、放大片、葉黃素、面膜、5度C水、助聽器、老花、多焦等各式精彩的最新消息/最新活動";

}		
	
							?>
								<input name="<?php echo $row_TemplateDetail['FieldID']; ?>" type="text" class="form-control" id="<?php echo $row_TemplateDetail['FieldID']; ?>" value="<?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>">
							<?php } else if ($row_TemplateDetail['Type'] == "Key") {
								//隱藏Key型態 
								$query_MiddleBigTableKey = sprintf("SELECT * FROM %s_BigTable WHERE KeyID = '%s'", $Table_ID, $colname_MiddleBigTable);
								$MiddleBigTableKey = mysqli_query($MySQL, $query_MiddleBigTableKey) or die(mysqli_error($MySQL));
								$row_MiddleBigTableKey = mysqli_fetch_assoc($MiddleBigTableKey);
								//echo $query_MiddleBigTableKey;
							?>
								<input name="<?php echo $row_TemplateDetail['FieldID']; ?>" type="hidden" class="f" id="<?php echo $row_TemplateDetail['FieldID']; ?>" value="<?php if ($row_MiddleBigTableKey[$row_TemplateDetail['FieldID']] == "") {
																																													echo $row_MiddleBigTableKey['KeyID'];
																																												} else {
																																													echo $row_MiddleBigTableKey[$row_TemplateDetail['FieldID']];
																																												} ?>" size="35">
							<?php } else if ($row_TemplateDetail['Type'] == "Calendar") {  ?>
								<div class="input-group mb-3">
									<input type="text" class="form-control" name="<?php echo $row_TemplateDetail['FieldID']; ?>" id="<?php echo $row_TemplateDetail['FieldID']; ?>" value="<?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>" >
									<div class="input-group-append">
										<button class="btn btn-outline-secondary" type="button" id="btn_<?php echo $row_TemplateDetail['FieldID']; ?>">...</button>
									</div>
								</div>
								<!-- <input class="form-control" name="<?php //echo $row_TemplateDetail['FieldID']; ?>" id="<?php //echo $row_TemplateDetail['FieldID']; ?>" value="<?php //echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>" readonly />
								<button class="btn btn-outline-success" id="btn_<?php //echo $row_TemplateDetail['FieldID']; ?>">...</button><br /> -->
								<script type="text/javascript">
									//<![CDATA[
									Calendar.setup({
										inputField: "<?php echo $row_TemplateDetail['FieldID']; ?>",
										trigger: "btn_<?php echo $row_TemplateDetail['FieldID']; ?>",
										onSelect: function() {
											this.hide()
										},
										showTime: 24,
										dateFormat: "%Y-%m-%d %H-%M-%S"
									});
									//]]>
								</script>
								<?php } else if ($row_TemplateDetail['Type'] == "Radio") {
								$value = explode("-", $row_TemplateDetail['Value']);
								$MappingValue = explode("-", $row_TemplateDetail['MappingValue']);
								$i = 0;
								while ($value[$i] <> "") {
								?>
									<input name="<?php echo $row_TemplateDetail['FieldID']; ?>" type="radio" class="f" id="<?php echo $row_TemplateDetail['FieldID']; ?>" value="<?php echo $value[$i]; ?>" size="35" <?php if ($row_MiddleBigTable[$row_TemplateDetail['FieldID']] == $value[$i]) echo "checked"; ?>>

									<?php echo $MappingValue[$i];
									$i++; ?>
								<?php } ?>
								<?php } else if ($row_TemplateDetail['Type'] == "CheckBox") { //多重選
								$value = explode("-", $row_TemplateDetail['Value']);
								$MappingValue = explode("-", $row_TemplateDetail['MappingValue']);
								$i = 0;
								while ($value[$i] <> "") {
								?>

									<input name="<?php echo $row_TemplateDetail['FieldID'] . "[]"; ?>" type="checkbox" id="<?php echo $row_TemplateDetail['FieldID'] . "[]"; ?>" value="<?php echo $value[$i]; ?>" size="35" <?php if ($_GET['Action'] == 'edit' and strstr($row_MiddleBigTable[$row_TemplateDetail['FieldID']], $value[$i])) echo "checked";   ?>>

									<?php if ($MappingValue[$i] <> "") {
										echo $MappingValue[$i];
									} else {
										echo $value[$i];
									}
									$i++; ?> </br>
								<?php }
							} else if ($row_TemplateDetail['Type'] == "Select") {  ?>


								<?php // recommended01.php  系列活動  另外寫   

								if ($_GET['Template'] == "recommended01.php" and $row_TemplateDetail['FieldID'] == "h02") {


									$query_content41 = sprintf("SELECT * FROM %s_BigTable WHERE Page = '4-1' AND Template='news41.php' and d02>=now() order  by Sq ", $Table_ID);
									$content41 = mysqli_query($MySQL, $query_content41) or die(mysqli_error($MySQL));
								?>

									<select name="<?php echo $row_TemplateDetail['FieldID']; ?>" class="form-control">
										<?php
										while ($row_content41 = mysqli_fetch_assoc($content41)) { ?>
											<option value="<?php echo $row_content41['h01']; ?>" <?php if ($row_MiddleBigTable['h02'] == $row_content41['h01']) echo "selected";   ?>> <?php echo $row_content41['h01'];
																																														$i++; ?> </option>
										<?php } ?>
									</select>

								<?php } else {  ?>


									<select name="<?php echo $row_TemplateDetail['FieldID']; ?>" class="form-control">
										<?php $value = explode("-", $row_TemplateDetail['Value']);
										$MappingValue = explode("-", $row_TemplateDetail['MappingValue']);
										$i = 0;
										while ($value[$i] <> "") { ?>
											<option value="<?php echo $value[$i]; ?>" <?php if ($row_MiddleBigTable[$row_TemplateDetail['FieldID']] == $value[$i]) echo "selected";   ?>> <?php echo $MappingValue[$i];
																																															$i++; ?> </option>
										<?php } ?>
									</select>

								<?php } ?>


							<?php } else if ($row_TemplateDetail['Type'] == "A") {
								//文字區域 
							?>
								<textarea class="form-control" name="<?php echo $row_TemplateDetail['FieldID']; ?>" rows="10"><?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?></textarea>
							<?php } else if ($row_TemplateDetail['Type'] == "D") {
								//ckeditor 
							?>
								<textarea class="ckeditor" name="<?php echo $row_TemplateDetail['FieldID']; ?>" cols="80" rows="10"><?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>
						</textarea>
							<?php } else if ($row_TemplateDetail['Type'] == "F") {
								//檔案上傳 
							?>
								<input type="file" name="<?php echo "T" . $row_TemplateDetail['FieldID']; ?>">
								目前檔案:<a href="pdf/<?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>"><?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?></a>


							<?php } else if ($row_TemplateDetail['Type'] == "FileDB") {
								//檔案進資料庫 
							?>

								<input type="file" name="<?php echo "T" . $row_TemplateDetail['FieldID']; ?>" id="<?php echo "T" . $row_TemplateDetail['FieldID']; ?>" value="">
								<input type="hidden" name="<?php echo "DB" . $row_TemplateDetail['FieldID']; ?>" id="<?php echo "DB" . $row_TemplateDetail['FieldID']; ?>" value="Y">
								<?php if ($_GET['Action'] <> "ins") { ?>
									<?php if ($row_TemplateDetail['FieldCheck'] == "0") { ?>
										<input type="checkbox" name="<?php echo "DD" . $row_TemplateDetail['FieldID']; ?>" id="<?php echo "DD" . $row_TemplateDetail['FieldID']; ?>" value="Y"> 刪除<?php } ?>目前檔案：
										<a href="GetDBfile.php?KeyID=<?php echo  $row_MiddleBigTable[$row_TemplateDetail['FieldID']];   ?>">
											<?php if ($row_MiddleBigTable[$row_TemplateDetail['FieldID']] <> "") {
												$query_contentA = sprintf("SELECT * FROM %s_FileDB WHERE KeyID = %s ", $Table_ID, GetSQLValueString($row_MiddleBigTable[$row_TemplateDetail['FieldID']], "text"), $Where);
												$contentA = mysqli_query($MySQL, $query_contentA) or die(mysqli_error($MySQL));
												$row_contentA = mysqli_fetch_assoc($contentA);
												echo $row_contentA['filename'];
											}
											?></a>
									<?php } ?>

								<?php } else if ($row_TemplateDetail['Type'] == "C") {
								// Html 編輯器  
								//$spaw1 = new SpawEditor($row_TemplateDetail['FieldID'],$row_MiddleBigTable[$row_TemplateDetail['FieldID']]); 
								//$spaw1->show();
								$offset = 1;
								for ($i = 0; $i <= 1; $i++) {
									$offset = strpos($_SERVER['PHP_SELF'], '/', $offset) + 1;
								}
								$ss = substr($_SERVER['PHP_SELF'], 0, $offset);

								// Path to user files relative to the document root.
								$RootPath = substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], 'include'));
								//echo $RootPath."include/fckeditor/"  ;
								$oFCKeditor = new FCKeditor($row_TemplateDetail['FieldID']);
								$oFCKeditor->BasePath = $RootPath . "include/fckeditor/";
								$oFCKeditor->Value		= $row_MiddleBigTable[$row_TemplateDetail['FieldID']];
								$oFCKeditor->Width = '100%';
								$oFCKeditor->Height = '400';

								$oFCKeditor->Create();
							} else if ($row_TemplateDetail['Type'] == "P") {
								$ID = "show" . $row_TemplateDetail['FieldID'];
								$subb = "sub" . $row_TemplateDetail['FieldID'];
								?>
									<div class="form-group">
										<?php if ($_GET['Action'] <> "view") { ?>
											<div class="input-group mb-3">
												<div class="custom-file">
													<input type="file" class="custom-file-input" id="<?php echo $ID; ?>" name="<?php echo $ID; ?>" accept="image/*">
													<label class="custom-file-label" for="inputGroupFile02">選擇要上傳的檔案</label>
												</div>
												<div class="input-group-append">
													<span class="btn btn-outline-success" type="button" id="<?php echo $subb; ?>">上傳圖片</span>
												</div>
											</div>
											<!-- test   -->
											<!-- <div class="input-group mb-3">
												<input type="file" class="form-control" id="<?php //echo $ID; 
																							?>" name="<?php //echo $ID; 
																						?>" accept="image/*">
												<div class="input-group-append" id="button-addon4">
												<button class="btn btn-outline-success" type="button" id="<?php //echo $subb; 
																											?>">上傳圖片</button>
												</div>
												</div> -->
											<!-- 上傳圖片 -->
											<div id="target<?php echo $row_TemplateDetail['FieldID']; ?>">
												<?php if ($row_MiddleBigTable[$row_TemplateDetail['FieldID']] == "") { ?>
													<div class="showtemp">限用 jpg/jpeg 格式,請勿使用中文檔名</div>
												<?php } ?>
												<!-- <div id="loading_msg">Loading...</div>-->
												<!-- <div id="loading_tp01" style="display: none"><img src="img/loading.gif" alt=""></div>-->
												<!-- <img id="loading_msg" src="img/loading.gif" alt="" style="display: none">-->
												<?php if ($row_MiddleBigTable[$row_TemplateDetail['FieldID']] <> "") { ?>
													<img src="UploadImages/<?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>" style="max-width:300px" alt="" class="img_preview">
												<?php } ?>
												<!-- <input type="hidden" class="<?php echo $row_TemplateDetail['FieldID']; ?>" name="<?php echo $row_TemplateDetail['FieldID']; ?>" id="<?php echo $row_TemplateDetail['FieldID']; ?>" value="<?php echo  $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>"> -->
											</div>
									</div>
								<?php } else { ?>
									<div id="target<?php echo $row_TemplateDetail['FieldID']; ?>">
										<?php if ($row_MiddleBigTable[$row_TemplateDetail['FieldID']] <> "") { ?>
											<img src="UploadImages/<?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>" style="max-width:300px" alt="" class="img_preview">
										<?php } ?>
									</div>
						</div>
					<?php } ?>

					<?php  /*  ?>   				
							<?php if ($row_MiddleBigTable[$row_TemplateDetail['FieldID']] != ""){ ?>
							<div id="<?php echo $ID; ?>">	 
							<img src="UploadImages/<?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>"  width="300">	</div>  
							<?php } else { ?>
							<div id="<?php echo $ID; ?>"></div>  
							<?php  } ?>
							<input type="button"  value="上傳" onClick="window.open('../lib/AjaxUpload.php?showDiv=<?php echo $row_TemplateDetail['FieldID']; ?>&width=2000&height=2000','upload','height=200,width=500,location=0,toolbar=0,menubar=0');">
							<input type="button"  value="清除" onClick="document.getElementById('<?php echo $ID;?>').innerHTML ='';document.form1.<?php echo $row_TemplateDetail['FieldID'];?>.value='';" >  
							限用jpg 格式,請勿使用中文檔名 
							<!-- width代表預覽圖的寬度--> 
							<br>			  
							<?php */ ?>

					<script>
						$('#<?php echo $subb; ?>').on('click', function() {
							var form_data = new FormData();
							var file_data = $('#<?php echo $ID; ?>').prop('files')[0];

							$<?php echo $row_TemplateDetail['FieldID']; ?> = "<?php echo $row_TemplateDetail['FieldID']; ?>";
							form_data.append('file', file_data);
							form_data.append('<?php echo $row_TemplateDetail['FieldID']; ?>', $<?php echo $row_TemplateDetail['FieldID']; ?>);
							<?php if($colname_TemplateDetail=="news01.php" and $row_TemplateDetail['FieldID']=="p02" ){ ?>
							form_data.append('picwidth', 1700 );
							form_data.append('picheigjt', 800 );
							<?php } ?>
							
							
							
							
							$.ajax({
								url: 'uploadpics.php',
								dataType: 'text', // what to expect back from the PHP script, if anything
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,
								type: 'post',
								beforeSend: function() {

								},
								success: function(data) {
									$('#target<?php echo $row_TemplateDetail['FieldID']; ?>').html(data);
								},
								complete: function() {
									//			$('#loading_tp01').hide();
								}
							});

						});
					</script>

				<?php } else if ($row_TemplateDetail['Type'] == "Q") {  ?>
					<div id="show<?php echo $row_TemplateDetail['FieldID']; ?>">
						<?php if ($row_MiddleBigTable[$row_TemplateDetail['FieldID']] != "") { ?>
							<img src="UploadImages/<?php echo $row_MiddleBigTable[$row_TemplateDetail['FieldID']]; ?>">
						<?php } ?>
					</div>
					<input type="button" onClick="window.open('fileupload.php?field=<?php echo $row_TemplateDetail['FieldID']; ?>','upload','height=200,width=500,location=0,toolbar=0,menubar=0');" value="上傳圖片">
					<input type="button" value="清除" onClick="document.getElementById('show<?php echo $row_TemplateDetail['FieldID']; ?>').innerHTML ='';document.form1.<?php echo $row_TemplateDetail['FieldID']; ?>.value='';">
				<?php } ?>

			</div><!-- end a group -->
		<?php } while ($row_TemplateDetail = mysqli_fetch_assoc($TemplateDetail)); ?>
		<div class="form-group">
			<hr size="1" class="Line">
<input type="hidden" name="ProductID" id="ProductID" value="">					
<?php if($row_MiddleBigTable['Template']=="news01.php"){ ?>			
	<input name="sbutton" type="button" class="btn btn-block btn-outline-primary" value="預覽" onClick="document.form1.ProductID.value='預覽';document.form1.target='_blank';chk_form(document.form1)">
	
<?php } ?>			
			<input name="sbutton" type="button" class="btn btn-block btn-outline-primary" value="<?php if ($_GET['Action'] == "ins") {
																										echo "新增";
																									} else {
																										echo "更新";
																									} ?>" onClick="document.form1.ProductID.value='';document.form1.target='';chk_form(document.form1)">
		</div>
		<?php require_once('../lib/MTemplateFooter.php'); ?>
		</form>
		<!-- end form -->
		</div>
	</div>

	</div>
	<?php
	mysqli_free_result($TemplateDetail);
	?>