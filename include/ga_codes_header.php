<meta name="facebook-domain-verification" content="cjz5lww76islzmoklp73808boz02uq" />
<!-- Facebook Pixel Code -->
<!-- <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '951991448506088');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=951991448506088&ev=PageView&noscript=1"
/></noscript> -->
<!-- End Facebook Pixel Code -->
<!-- 20211224 -->
<!-- Clickforce -->
<script src="//cdn.holmesmind.com/js/rtid.js"></script>
<script src="https://cdn.holmesmind.com/dmp/cft/triggerTracker.js"></script>
<script async src="https://cdn.holmesmind.com/dmp/cft/tracker.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-215630172-1"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GTR260230H"></script>
<script>
    clickforce_rtid("9650001");

    var clickforceThat = this;
    window.cft = window.cft || function () { (cft.q = cft.q || []).push([].slice.call(arguments)) };
    function clickForceMyyCFT() {
        cft('setSiteId', 'CF-211200104136');
        cft('setViewPercentage');
    }
    clickForceDelayLoading();

    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-215630172-1');

    gtag('config', 'G-GTR260230H');
</script>
<!-- End Clickforce -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '274691144379784');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=274691144379784&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- LINE Tag Base Code -->
<!-- Do Not Modify -->
<script>
(function(g,d,o){
  g._ltq=g._ltq||[];g._lt=g._lt||function(){g._ltq.push(arguments)};
  var h=location.protocol==='https:'?'https://d.line-scdn.net':'http://d.line-cdn.net';
  var s=d.createElement('script');s.async=1;
  s.src=o||h+'/n/line_tag/public/release/v1/lt.js';
  var t=d.getElementsByTagName('script')[0];t.parentNode.insertBefore(s,t);
    })(window, document);
_lt('init', {
  customerType: 'lap',
  tagId: 'a5d3ca47-c5a1-402c-9fd4-8539f2951f1d'
});
_lt('send', 'pv', ['a5d3ca47-c5a1-402c-9fd4-8539f2951f1d']);
</script>
<noscript>
  <img height="1" width="1" style="display:none"
       src="https://tr.line.me/tag.gif?c_t=lap&t_id=a5d3ca47-c5a1-402c-9fd4-8539f2951f1d&e=pv&noscript=1" />
</noscript>
<!-- End LINE Tag Base Code -->
<!-- Global site tag (gtag.js) - Google Ads: 10812508843 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10812508843"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10812508843');
</script>
<!-- Event snippet for 寶島官網 remarketing page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-10812508843/0xi2CKyE2IUDEKuV56Mo',
      'value': 1.0,
      'currency': 'TWD',
      'aw_remarketing_only': true
  });
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TB2L9HS');</script>
<!-- End Google Tag Manager -->