<?php  
$query_contentV1 = sprintf("SELECT * FROM GoWeb_BigTable WHERE Page = '0-2' and Template='featuredcideos01.php' and Cate03='Y'  ORDER by Sq desc ");
$contentV1 = mysqli_query($MySQL,$query_contentV1 ) or die(mysqli_error($MySQL));
$row_contentV1 = mysqli_fetch_assoc($contentV1);
?>
<section id="video" class="u-bg-white u-py-200">
	<div class="container">
		<h3 class="c-title-center u-mb-125">精選影片</h3>
		<div class="col">
		<?php include("editVideos_button.php"); ?>
		</div>
		
		<div class="row justify-content-between align-items-center">
		
			<div class="col-lg-8 u-mb-100 u-mb-lg-000 u-px-200">
				<div class="embed-responsive embed-responsive-16by9 u-mb-125">
					<iframe id="vidio_play" data-src="<?php echo $row_contentV1['l01']; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" class="embed-responsive-item lozad" allowfullscreen></iframe>
				</div>
				<div class="p-index-video-thumbnails">
					
<?php  
$n=0;					
					do{
$n++;	
if($n==1){$Cate01=$row_contentV1['Cate01']; }						
					?>				
					

					<div data-url="<?php echo $row_contentV1['l01']; ?>" class="p-index-video-thumbnails-frame <?php if($n==1){?> js-active  <?php } ?>">
	<img data-src="UploadImages/<?php echo $row_contentV1['p01'];  ?>" alt="" class="img-fluid lozad"   >
					</div>

<?php }while($row_contentV1 = mysqli_fetch_assoc($contentV1)); ?>						
					

<!--
					<div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame js-active">
						<img data-src="assets/img/index/video/03.jpg" alt="" class="img-fluid lozad">
					</div>
					<div data-url="https://www.youtube.com/embed/U-8MgOi68ig" class="p-index-video-thumbnails-frame">
						<img data-src="assets/img/index/video/02.jpg" alt="" class="img-fluid lozad">
					</div>
					<div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame">
						<img data-src="assets/img/index/video/03.jpg" alt="" class="img-fluid lozad">
					</div>
					<div data-url="https://www.youtube.com/embed/U-8MgOi68ig" class="p-index-video-thumbnails-frame">
						<img data-src="assets/img/index/video/04.jpg" alt="" class="img-fluid lozad">
					</div>
					<div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame">
						<img data-src="assets/img/index/video/05.jpg" alt="" class="img-fluid lozad">
					</div>
-->

				</div>
			</div>
			<div class="col-lg-4">
				<h4 class="text-center u-text-gray-800 u-mb-100 u-mt-100 u-mt-lg-000">精選廣告款</h4>
<!--				<div class="v-slick c-card-verA p-index-video-slick" id="productslide01" >-->
					
<div id="slick_ad" class="v-slick c-card-verA p-index-video-slick">					
					<!-- Slides start -->

<?php  
$query_contentV1 = sprintf("SELECT * FROM GoWeb_BigTable WHERE Page = '0-3' and Template='featuredcideos02.php' and Cate01=%s   ORDER by Sq desc ", GetSQLValueString($Cate01, "text"));
//echo $query_contentV1;
$contentV1 = mysqli_query($MySQL,$query_contentV1 ) or die(mysqli_error($MySQL));
?>
					
	<?php while($row_contentV1 = mysqli_fetch_assoc($contentV1)){ ?>

					<a href="<?php echo $row_contentV1['l01'];  ?>" class="v-slick-slide p-2" target="_blank">
						<div class="v-slick-slide-frame c-card-head">
							<img src="UploadImages/<?php echo $row_contentV1['p01'];  ?>" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100"><?php echo $row_contentV1['h01']; ?></p>
						</div>
					</a>

<?php  } ?>					
					
<!--					 <a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100"><?php echo $row_contentV1['h01']; ?></p>
						</div>
					</a>
		

					<a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/2036413344360884447ccf.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 睫毛彎彎 大方框眼鏡✿玫瑰金</p>
						</div>
					</a>
					<a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/203641420247658483df3f.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 散步の貓 圓框眼鏡✿霧面黑</p>
						</div>
					</a>
					<a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/203641415391370294ae8f.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 花鳥叢林 圓框眼鏡✿玫紅色</p>
						</div>
					</a>
					<a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/203641331188668821bf21.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 心花怒放 圓框眼鏡✿玫瑰金</p>
						</div>
					</a>
					<a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/20364130415111623223cc.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 十數密碼 威靈頓框✿玳瑁棕</p>
						</div>
					</a>
					<a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/20365094851851609e7620.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 跳舞の房子 威靈頓框眼鏡✿透湛藍</p>
						</div>
					</a>
					<a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/20364141336484885f7c48.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 玩味十數 復古圓框✿玫瑰金</p>
						</div>
					</a>
					<a href="" class="v-slick-slide p-2">
						<div class="v-slick-slide-frame c-card-head">
							<img src="assets/img/index/20364131847571290d4f8a.jpg" alt="" class="img-fluid">
						</div>
						<div class="c-card-body-layout">
							<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 錦上添花 貓眼框眼鏡✿霧黑金
</p>
						</div>
					</a> -->
				<!-- Slides end -->

	
	
				</div>
			</div>
		</div>
	</div>
</section>