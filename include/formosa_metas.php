<?php require_once('../Connections/MySQL.php'); ?>
<?php
 //撈configure的SQL
//撈客戶名稱
$query_Configure = "SELECT * FROM ".$Table_ID."_Configure WHERE item = 'CustomerName'";
$Configure = mysqli_query($MySQL,$query_Configure) or die(mysqli_error($MySQL));
$row_Configure = mysqli_fetch_assoc($Configure);
$CustomerName=$row_Configure['value'];
//撈Logo
$query_Configure = "SELECT * FROM ".$Table_ID."_Configure WHERE item = 'Logo'";
$Configure = mysqli_query($MySQL,$query_Configure) or die(mysqli_error($MySQL));
$row_Configure = mysqli_fetch_assoc($Configure);
$Logo=$row_Configure['value'];

?>

<?php
$template = "news01.php";
$templateS = "news01";
// 判斷是預設是A版還是S版
// 預設 A 版
if ($_GET[$templateS] == "" and $row_template['Extend'] == "Y") {
    //A 版開始 demo page
    if ($_GET['demo'] == 'y') { ?>
        <p>DEMO</p>
    <?php } else {  ?>
        <?php
        $currentPage = $_SERVER["PHP_SELF"];

        $maxRows_content01 = $row_template['MaxRow'];
        $pageNum_content01 = 0;
        if (isset($_GET['pageNum_content01'])) {
            $pageNum_content01 = $_GET['pageNum_content01'];
        }
        $startRow_content01 = $pageNum_content01 * $maxRows_content01;

        $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $maxRows_content01 = $row_template['MaxRow'];
        $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        if ($_GET['Cate01'] <> "") {
            $query_content01 = $query_content01 . "AND Cate01='" . $_GET['Cate01'] . "'";
        }

        if ($_GET['h10'] <> "") {
            $query_content01 = $query_content01 . "AND h10 like '%" . $_GET['h10'] . "%'";
        }

        //排序
        $query_content01 = $query_content01 . " order by Sq desc, d01 desc ";


        $query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
        // echo $query_limit_content01;
        $content01 = mysqli_query($MySQL, $query_limit_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);
        if (isset($_GET['totalRows_content01'])) {
            $all_content01 = mysqli_query($MySQL, $query_content01);
            $totalRows_content01 = mysqli_num_rows($all_content01);
        } else {
            $all_content01 = mysqli_query($MySQL, $query_content01);
            $totalRows_content01 = mysqli_num_rows($all_content01);
        }
        $totalPages_content01 = ceil($totalRows_content01 / $maxRows_content01) - 1;

        $queryString_content01 = "";
        if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
                if (
                    stristr($param, "pageNum_content01") == false &&
                    stristr($param, "totalRows_content01") == false
                ) {
                    array_push($newParams, $param);
                }
            }
            if (count($newParams) != 0) {
                $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
            }
        }
        $queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
        // A版開始 real page
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php echo $row_template['Title']; ?> | 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
        <meta name="keywords" content="<?php echo $KeyWord; ?>" />
        <meta name="description" content="<?php echo $row_template['Topic']; ?>">
        <meta name="author" content="<?php echo $CustomerName; ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <?php
        mysqli_free_result($content01);
    }
    //A 版結束 
} else {  //S版開始 
    if ($_GET['demo'] == 'y') { ?>
        <p>S demo</p>
    <?php } else {  ?>
        <?php
        $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $maxRows_content01 = $row_template['MaxRow'];
        $KeyID = str_replace($vowels, "**!!!**", $_GET[$templateS]);
        if ($row_template['Extend'] == 'Y') {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        } else {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        }

        $content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);
        $totalRows_content01 = mysqli_num_rows($content01);
        //echo "S版啟動".$query_content01;
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
        <title><?php if($row_content01['h01']<>""){ echo $row_content01['h01'] ; }  ?> <?php //echo $row_template['Title']; ?> | 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
        <meta name="keywords" content="<?php echo $KeyWord; ?>" />
        <meta name="description" content="<?php echo $row_content01['h02'];  ?>">
        <meta name="author" content="<?php echo $CustomerName; ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
<?php mysqli_free_result($content01);
    }
}  //S版結束
?>

<!-- end -->