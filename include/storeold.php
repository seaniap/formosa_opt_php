<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-tw" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-tw" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-tw">
<!--<![endif]-->

<head>
<? include('std_header.php')?>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta property="og:url" content="https://www.your-domain.com/your-page.html" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="Your Website Title" />
  <meta property="og:description" content="Your description" />
  <meta property="og:image" content="https://www.your-domain.com/path/image.jpg" />
  <!-- Favicon -->
  <link rel="shortcut icon" href="../../favicon.ico">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="../../assets/plugins/bootstrap4/css/bootstrap.min.css">
  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="../../assets/plugins/fontawesome5/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../../assets/plugins/icomoon/style.css">
  <!-- CSS Customization -->
  <link rel="stylesheet" href="../../assets/css/main.css">
</head>

<body>
  <header id="header">
  </header>
  <nav class="breadcrumb-nav" aria-label="breadcrumb">
    <div class="container">
      <ol class="breadcrumb">
        <li>
          <a href="">
            <i class="fas fa-home"></i>
          </a>
        </li>
        <li class="active">
          <a href="index.php?Page=5">經銷據點</a>
        </li>
      </ol>
    </div>
  </nav>
  
    <? 	if($row_template['TP4']<>""){include($row_template['TP4']); }  ?> 
    

  <!-- /container -->
  <!-- JS Global Compulsory -->
  <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
  <script type="text/javascript" src="../../assets/plugins/jquery/jquery.min.js"></script>
  <script type="text/javascript" src="../../assets/plugins/popper.min.js"></script>
  <script type="text/javascript" src="../../assets/plugins/bootstrap4/js/bootstrap.js"></script>
  <!-- js plugins -->
  <script type="text/javascript" src="../../assets/plugins/back-to-top.js"></script>
  <!-- JS Customization -->
  <script type="text/javascript" src="../../assets/js/main.js"></script>
  <!-- <script>
    jQuery(function () {
      var page = location.pathname.split('/').pop();
      page = 'news.html'; //hard coded for testing
      $('#mainmenu li a[href="' + page + '"]').addClass('active')
  });
  </script> -->
  <!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

  
    <script src="assets/js/Zip.js" type="text/javascript"></script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB47X0H478YArS4HTNgBKMC4U_VlT0I1X8"></script>
    <!--<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCDj_2oZ_BAn0WfCO89NosRMto8MBX6Uhw"></script>-->    
    <script src="js/jquery.gmap.js"></script>



<? if($_POST['KeyID']<>"" ){ ?>    
<script>
var geocoder = new google.maps.Geocoder();

 var address = "<? echo $row_contentS1['h10'].$row_contentS1['h11'].$row_contentS1['h12']; ?>";

  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      
	 // alert('AAA');
	 // alert(results[0].geometry.location.lat());
	 // alert(results[0].geometry.location.lng());

	 $("#googlemaps").gMap('centerAt',
	{
		latitude: results[0].geometry.location.lat(),
		longitude: results[0].geometry.location.lng()
	}
);

	 
	// var initLatitude = results[0].geometry.location.lat();
 	// var initLongitude = results[0].geometry.location.lng();
	//  resultsMap.setCenter(results[0].geometry.location);
	  
     // var marker = new google.maps.Marker({
      //  map: resultsMap,
       // position: results[0].geometry.location
      //});
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
	
	 
    }
  });
  
		/*
		Map Settings

			Find the Latitude and Longitude of your address:
				- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
				- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

		*/

		// Map Markers

		var mapMarkers = [
				{
			address: "<? echo $row_contentS1['h10'].$row_contentS1['h11'].$row_contentS1['h12']; ?>",
			html: "<strong><? echo $row_contentS1['h01']; ?></strong><br><? if($row_contentS1['h02']=="1"){  echo $row_contentS1['h03']; ?><br><? } echo $row_contentS1['h10'].$row_contentS1['h11'].$row_contentS1['h12']; ?><br><? if($row_contentS1['h06']<>""){ ?><strong>LINE ID: <? echo $row_contentS1['h06']; ?></strong><br><? } ?><? if($row_contentS1['h05']<>""){ ?><strong>email:</strong> <a href='mailto:<? echo $row_contentS1['h05']; ?>' style='color:#F00'><strong><? echo $row_contentS1['h05']; ?></strong></a><br><? } ?> 	 ",
			
		
			
			
			
			popup: false,
			icon: {
				image: "images/maker.png",
				iconsize: [28, 42],
				iconanchor: [28, 32]
			}
		},
		
				
		];

		// Map Initial Location
		
     var initLatitude = 23.59781;
 	 var initLongitude = 120.960515;


		// Map Extended Settings
		var mapSettings = {
			controls: {
				panControl: true,
				zoomControl: true,
				mapTypeControl: true,
				scaleControl: true,
				streetViewControl: true,
				overviewMapControl: true
			},
			scrollwheel: false,
			markers: mapMarkers,
			latitude: initLatitude,
			longitude: initLongitude,
		
			zoom: 13
		
		
		};





		var map = $("#googlemaps").gMap(mapSettings);

		// Map Center At


		var mapCenterAt = function(options, e) {
			e.preventDefault();
			$("#googlemaps").gMap("centerAt", options);
		
		}
	
	
			
	</script>

<? } else if($_POST['h10']<>""){ ?>
   <script>
var geocoder = new google.maps.Geocoder();

 var address = "<? echo $row_contentS1['h10'].$row_contentS1['h11'].$row_contentS1['h12']; ?>";
 var address = "<? echo $_POST['h10']; ?>";
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      
	 // alert('AAA');
	 // alert(results[0].geometry.location.lat());
	 // alert(results[0].geometry.location.lng());

	 $("#googlemaps").gMap('centerAt',
	{
		latitude: results[0].geometry.location.lat(),
		longitude: results[0].geometry.location.lng()
	}
);

	 
	// var initLatitude = results[0].geometry.location.lat();
 	// var initLongitude = results[0].geometry.location.lng();
	//  resultsMap.setCenter(results[0].geometry.location);
	  
     // var marker = new google.maps.Marker({
      //  map: resultsMap,
       // position: results[0].geometry.location
      //});
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
	
	 
    }
  });
  
		/*
		Map Settings

			Find the Latitude and Longitude of your address:
				- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
				- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

		*/

	
		
     var initLatitude = 23.59781;
 	 var initLongitude = 120.960515;


		// Map Extended Settings
		var mapSettings = {
			controls: {
				panControl: true,
				zoomControl: true,
				mapTypeControl: true,
				scaleControl: true,
				streetViewControl: true,
				overviewMapControl: true
			},
			scrollwheel: false,
		//	markers: mapMarkers,
			latitude: initLatitude,
			longitude: initLongitude,
		
			zoom: 11
		
		
		};





	var map = $("#googlemaps").gMap(mapSettings);

		// Map Center At

		var mapCenterAt = function(options, e) {
			e.preventDefault();
			$("#googlemaps").gMap("centerAt", options);
		
		}

	
			
	</script>

<? } else { ?>
   	<script>

var geocoder = new google.maps.Geocoder();


		// Map Initial Location
		
     var initLatitude = 23.59781;
 	 var initLongitude = 120.960515;


		// Map Extended Settings
		var mapSettings = {
			controls: {
				panControl: true,
				zoomControl: true,
				mapTypeControl: true,
				scaleControl: true,
				streetViewControl: true,
				overviewMapControl: true
			},
			scrollwheel: false,
		//	markers: mapMarkers,
			latitude: initLatitude,
			longitude: initLongitude,
		
			zoom: 7
		
		
		};

		var map = $("#googlemaps").gMap(mapSettings);

		// Map Center At


		var mapCenterAt = function(options, e) {
			e.preventDefault();
			$("#googlemaps").gMap("centerAt", options);
		
		}
				
	</script>
<? } ?>










</body>

</html>