        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section class="p-knowledge">
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">德國蔡司 SL 120 數位光學顯像系統</h3>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">專業提升儀器升級</h4>
                        <div class="row">
                            <div class="col-lg-6 mb-3 mb-lg-0">
                                <p>相較於以往配鏡體驗只能告訴消費者的是，配鏡度數與視力值多少，欠缺視覺照護與預防保健的概念，高階儀器設備導入帶給您不同於以往的配鏡體驗，透過數位即時影像、眼數據分析，讓您更深入了解個人眼生理與眼機能狀況，提供相關衛教訊息及事先的預防措施，提供個人化視覺照護需求的配鏡解決方案，如發現異常現象則轉介眼科醫師做進一步檢查。
                                </p>
                                <p class="u-mb-000">
                                    從問診過程詳細了解個人需求及視覺症狀並記錄完整資料，透過德國蔡司SL120數位光學顯像系統，在驗光之前先進行重要的眼健康篩檢，經驗光人員解析後能預先了解眼睛現狀，再進行自覺式驗光流程，最後彙整檢查數據進行演算，分析推薦適合的鏡片或隱形眼鏡，符合消費者在工作及生活上用眼所需的配鏡方案。
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <div class="d-flex">
                                    <div class="col">
                                        <img src="assets/img/education/education_37.jpg" alt="" class="img-fluid">
                                        <p>SL120全機側面圖</p>
                                    </div>
                                    <div class="col">
                                        <img src="assets/img/education/education_38.jpg" alt="" class="img-fluid">
                                        <p>SL120側面圖</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">德國蔡司 SL 120 數位光學顯像系統</h4>
                        <div class="row">
                            <div class="col-lg-6 mb-3 mb-lg-0">
                                <p>
                                    德國蔡司數位光學顯像系統，可快速觀察出眼睛外觀是否有異常現象。例如眼皮有無紅腫、眼球乾澀、色斑、瞳孔較大及角膜透徹度等，並提供隱形眼鏡驗配評估。透過即時數位影像讓消費者更了解個人眼睛狀況，輔以生活形態與用眼習慣諮詢，給予相關衛教訊息，建議符合需求的配鏡解決方案。
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <div class="d-flex">
                                    <div class="col">
                                        <img src="assets/img/education/education_39.jpg" alt="" class="img-fluid">
                                        <p>SL120側面圖</p>
                                    </div>
                                    <div class="col">
                                        <img src="assets/img/education/education_40.jpg" alt="" class="img-fluid">
                                        <p>SL120全機圖</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->