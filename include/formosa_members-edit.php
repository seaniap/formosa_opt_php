<?php
session_start();
 
 ?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>會員資料修改 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="請先登入帳號，並至會員資料修改變更個人資料，個人資料請填寫正確，以保障您的權益。">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />
    
    
     <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>
     
    <script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>
    
    
    <script type="text/javascript">
        var checkIdNumber = false;
        
    	function initDate() {
    		 var isLogin = "<?php echo isset($_SESSION['user']);?>"
 
    		 if (!isLogin) {
    			 Swal.fire({
 	                 html: `
 	                	 <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
  	                		您尚未登入會員
                        </h3>
                        <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
  	                		若想修改會員資料或查詢會員點數，請先登入會員
                        </p>
	   	                 `,
 	                 imageUrl: 'assets/img/members/icon25.svg',
 	                 imageWidth: 70,
 	                 imageHeight: 70,
  	   	             allowEscapeKey: false,
       	   	         confirmButtonColor: '#3085d6',
                     confirmButtonText: '會員登入',
                     showCancelButton: true,
	         	     cancelButtonText: '我知道了', 
 	               }).then((result) => {
 	            	   if (result.dismiss  == 'cancel') {
	      	               window.location ="index.php?Page=B-7";
		      	       } else {
			      	       window.location ="index.php?Page=B-1";
			      	   }
 	               })
 	             return;
             }
        	
	    	 $.get("app/controller/Member.php?method=toMemberEdit",function(result){
	   			 
	    		 var result = JSON.parse(result);
 
	    		 user = result.user;
				
				 
	     		 $("#m_id").val(user.m_id);
	     		 $("#m_card_no").val(user.m_card_no);
	     		 $("#name").val(user.m_name);
	     		 $("#id_number").val(user.m_idcard);
	     		 $("#date").val(user.m_birth_year + "/" + user.m_birth_month +"/"+ user.m_birth_day );
	     		 $("#phone").val(user.m_mobile);
	     		 $("#email").val(user.m_email);
	     		 $("#addr").val(user.m_addr);
			     $("#m_edm").prop( "checked",  user.m_edm =="Y");

                 //無編輯身分證 開放編輯
				 if (!user.m_idcard) {  
					 $('#id_number').removeAttr("readonly");
					 checkIdNumber = true;
			     }
			     
	      		 $.each(result.city.Citys,function (i,data){
	    			    $('#city').append($('<option>', { 
				        		value: data.city_name,
				        		text : data.city_name 
				    		}
	 			     ));
	             })
	             
	             if (user.m_addr_city) {
	             	$('#city').val(user.m_addr_city);
	             }
	     		 
	      		 if (result.town) {
	      			$.each(result.town.Towns,function (i,data){
	      			    $('#town').append($('<option>', { 
	    		        		value: data.town_name,
	    		        		text : data.town_name 
	    			    	}
	    			    ));
	                });
	      			$('#town').val(user.m_addr_town);
	      		 }
	      		 
	      		 if (result.road){
	      			$.each(result.road.Roads,function (i,data){
	      			    $('#road').append($('<option>', { 
	    		        		value: data.road_name,
	    		        		text : data.road_name 
	    			    	}
	    			    ));
	                })	
	      			$('#road').val(user.m_addr_road);
	      		 } 
	    	 });  
    	}
    	
    	 
    	$(function(){

    		$.validator.methods.idnumber = function (value, element, params) {
        		if (value =="" || checkIdNumber == false) return true;
        		
    		    var userid= value;
    		    var reg=/^[A-Z]{1}[1-2]{1}[0-9]{8}$/;   
    		    if( reg.test( userid ) ) {
    		        var s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";   
    		        var ct = ["10","11","12","13","14","15","16","17","34","18","19","20","21",
    		                       "22","35","23","24","25","26","27","28","29","32","30","31","33"];
    			    var i = s.indexOf(userid.charAt(0));
    			    var tempuserid = ct[i] + userid.substr(1, 9);  
    			    var num = tempuserid.charAt(0)*1;
    			    for( i=1 ; i<=9 ; i++ ) {
    			       num = num + tempuserid.charAt(i)*(10-i);
    			    }
    			    num += tempuserid.charAt(10)*1;
    			    return  num%10 == 0 ;
    		     } else {
    		        return false;
    			 }
    		 };
    		 
    		window.alert = function(msg, icon="info") {
        		Swal.fire({
        		  text: ""+msg,
        		  icon: icon,
        		  confirmButtonColor: '#3085d6',
        		  confirmButtonText: '確定'
        		})
        	};
        	
        	window.showLoading = function(msg) {
            	 Swal.fire({
            		  title: '請稍待...',
            		  html: msg ,
            		  allowEscapeKey: false,
            		  allowOutsideClick: false,
            		  didOpen: () => {
            		    Swal.showLoading()
            		  }
            	 });
        	} 
    		initDate();
    		 
    		 $.validator.methods.reg = function (value, element, params) {
    			 return this.optional(element) || params.test(value);
    		 };
  
    		 $("#form").validate({
    			  rules:  {
    				 name: "required",
    				 email: {
    			        required: true,
    			        email: true
    			     },	

    			     id_number:{
      					 idnumber : true,
      			      },		     
    		      }, 
    		      messages: {
    		    	 name: "請輸入姓名",
    		    	 email:"請輸入正確的Email",
    		    	 id_number:{
    					 idnumber : "身分證錯誤",
    			     },
    			  },

    			  
    			 
    			  submitHandler:function(form){
  
    				  var params =  {
       					   "m_id":$("#m_id").val(),
       					   "m_name":$("#name").val(),
       					   "m_email":$("#email").val(),
       					   "m_idcard" : $("#id_number").val(),
       					   "m_edm":$("#m_edm").prop("checked") ? "Y":"N",
       					   "m_addr_city" : $("#city").val(),
           				   "m_addr_town" : $("#town").val(),
           				   "m_addr_road" : $("#road").val(),
           				   "m_addr": $("#addr").val(),
    				  };
    		  
    				  $.get("app/controller/Member.php?method=editMember",params, function(result){
  		         		  result = JSON.parse(result);
  		         		  if (result.result =="1") {
    		         			Swal.fire({
       		         	    	 text :"更新成功!",
       		         	    	 icon: "info",
   		    	    	         allowEscapeKey: false,
   		    	    	         allowOutsideClick: false,
   		    	      	         confirmButtonColor: '#3085d6',
   		    	        	     confirmButtonText: '我知道了'
   		    	               });
  	  		         	  } else {
							  alert(result.message)
  	  	  		          }
  		    	      }); 
    	          }
    		 });
    
    
    		 
    
    		 $('#city').change(function() {
           			$('#town').children().remove().end().append('<option value="" selected >請選擇鄉鎮區</option>') ;
           			$('#road').children().remove().end().append('<option value="" selected >請選擇道路名</option>') 
           			genTown();
    	     });
    
    		 $('#town').change(function() {
     			$('#road').children().remove().end().append('<option value="" selected >請選擇道路名</option>') 
     			genRoad();
    	     });
    		 
    		 $("#btnCancel").click(function(){
    			initDate(); 
    		 });
    
    	   	
    	})
    	
    	function genTown(){
           	var params = {
             	"city_name" :$('#city').val(),
            };  
        	$.get("app/controller/Member.php?method=getTown",params, function(result){
        		result = JSON.parse(result);
       		     
          		$('#town').children().remove().end()
            		.append('<option value="" selected>請選擇鄉鎮區</option>') ;
       		    
        		$.each(result.Towns,function (i,data){
      			    $('#town').append($('<option>', { 
    		        		value: data.town_name,
    		        		text : data.town_name 
    			    	}
    			    ));
                })	
            });
         }
    
    	function genRoad(){
           	var params = {
             	"city_name" :$('#city').val(),
             	"town_name" :$('#town').val(),
            };  
        	$.get("app/controller/Member.php?method=getRoad",params, function(result){
        		result = JSON.parse(result);
       		     
          		$('#road').children().remove().end()
            		.append('<option value="" selected>請選擇道路名</option>') ;
       		    
        		$.each(result.Roads,function (i,data){
      			    $('#road').append($('<option>', { 
    		        		value: data.road_name,
    		        		text : data.road_name 
    			    	}
    			    ));
                })	
            });
         }
    			
	</script> 
<?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php')?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">會員專區</h3>   
                    </div>
                </div>
                 <!-- member static tabs -->
        <?php include('formosa-member-static_tabs.php')?>
        <!-- end member static tabs -->
                <div class="u-pb-300">
                    <div class="container">
                        <div class="row justify-content-center ">
                            <div class="col-md-8 col-12">
                            	<form id="form">
                                <div class="u-text-gray-800 u-font-weight-700 u-font-18 u-mb-100">
                                    <span class="u-text-red-formosa">＊</span>為必填項目
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12 u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">會員編號</h5>
                                        </label>
                                        <div>
                                            <input type="text" id="m_id" class="l-form-field"   disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">會員卡號</h5>
                                        </label>
                                        <div>
                                            <input type="text" id="m_card_no" class="l-form-field" 
                                                disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span
                                                    class="u-text-red-formosa">＊</span>會員姓名</h5>
                                        </label>
                                        <div>
                                            <input type="text" id="name" name="name" class="l-form-field" placeholder="請輸入您的大名">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span
                                                    class="u-text-red-formosa">＊</span>身分證字號</h5>
                                        </label>
                                        <div>
                                            <input type="text" id="id_number" name="id_number" class="l-form-field" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span
                                                class="u-text-red-formosa">＊</span>出生日期</h5>
                                        </label>
                                        <div>
                                            <input type="text" id="date" name="date" class="l-form-field"  
                                                disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span
                                                class="u-text-red-formosa">＊</span>手機號碼</h5>
                                        </label>
                                        <div>
                                            <input type="text" name="phone" id="phone" class="l-form-field"  
                                                disabled>
                                        </div>
                                    </div>
                                    <div class="col-12 u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span
                                                    class="u-text-red-formosa">＊</span>會員E-Mail</h5>
                                        </label>
                                        <div>
                                            <input type="text" name="email" id="email" class="l-form-field"  >
                                        </div>
                                    </div>
                                    <div class="col-12 u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">聯絡地址</h5>
                                        </label>
                                        <div class="row">
                                            <div class="col-md-3 col-6 u-mb-100">
                                                <select type="select" id="city" class="l-form-field l-form-select">
                                                    <option value="default" selected disabled>請選擇縣市</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <select type="select" id="town" class="l-form-field l-form-select">
                                                    <option value="default" selected disabled>請選擇鄉鎮區</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 col-12 u-mb-100">
                                                <select type="select" id="road" class="l-form-field l-form-select">
                                                    <option value="default" selected disabled>請選擇道路名</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" id="addr" class="l-form-field" placeholder="請輸入地址">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 u-mb-100 u-text-gray-800">
                                        <div class="form-check u-mb-100">
                                            <input class="form-check-input" type="checkbox"  value="Y" id="m_edm">
                                            <label class="form-check-label" for="">
                                                同意收到寶島眼鏡會員電子報
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-sm-6 u-mb-100">
                                             <input id="btnSend" class="c-btn c-btn--contained c-btn-blue-highlight col" type="submit" value="確認送出">
                                            </div>
                                            <div class="col-sm-6">
                                                <button id="btnCancel" type="button" 
                                                    class="c-btn c-btn--outlined c-btn-gray-800 col">
                                                    取消
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php')?></footer>
        <!-- =============end footer ============= -->
    </div>
</body>

</html>