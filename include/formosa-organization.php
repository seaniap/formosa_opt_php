
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">公司組織</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="d-none d-md-block">
                            <img class="w-100" src="./assets/img/regulation/organization-PC.svg" alt="">
                        </div>
                        <div class="d-md-none">
                            <img class="w-100" src="./assets/img/regulation/organization-mobile.svg" alt="">
                        </div>
                    </div>
                </div>
            </section>
        </main>
        