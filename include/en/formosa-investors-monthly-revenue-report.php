<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Monthly Revenue Report ｜ formosa</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="Provide relevant information on the monthly turnover report of Formosa Optical Technology">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="../favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="../assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
    <?php include ("../ga_codes_header.php")?>   
</head>

<body>
<?php include ("../ga_codes_body.php")?>
<?php include ("../formosa_loading.php"); ?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
        <?php include('components/header_en.php')?>
        </header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">Financial Information</h3>
                    </div>
                </div>
                <!-- START TAB -->
                <div class="u-pb-100">
                    <div class="container">
                        <div class="d-md-flex d-none c-tab-verF row justify-content-center">
                            <div class="c-tab-container">
                                <a href="formosa-investors-financial.php" class="c-tab-linkF">Financial statement</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="formosa-investors-monthly-revenue-report.php" class="c-tab-linkF active">Monthly Revenue Report</a>
                            </div>
                        </div>
                        <div class="row justify-content-center d-block d-md-none">
                            <div class="col-12">
                                <select name="select" id="" class="l-form-field l-form-select u-bg-gray-200 js-selectURL">
                                    <option value="formosa-investors-financial.php">Financial statement</option>
                                    <option value="formosa-investors-monthly-revenue-report.php" selected="" disabled="">Monthly Revenue Report</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END TAB -->
                <div class="u-pb-100">
                    <div class="container">
                        <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">Monthly Revenue Report</p>
                        <!-- <p class="u-mb-000">本公司依照證交法所訂資格條件選任獨立董事之相關訊息</p> -->
                    </div>
                </div>
                <div class="u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">I.&nbsp;Related links</h4>
                        <div class="col-md-3 col-lg-2 u-p-000">
                            <a href="http://mops.twse.com.tw/mops/web/index" target="_blank" class="c-btn c-btn--contained c-btn-gray-200 u-text-black border border-secondary u-mb-100">公開資訊觀測站</a>
                        </div>
                        <p class="u-mb-000">營運概況 \ 每月營收 \ 採用IFRS後之月營業收入資訊<br>公司代號 5312</p>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer">
            <?php include('components/footer_en.php') ?>
        </footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>