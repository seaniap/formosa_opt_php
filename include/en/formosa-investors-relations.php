<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Stakeholder ｜ formosa</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="Identify the main stakeholders based on the Formosa Optical Technology operating attributes, and set up corresponding departments and channels for the topics that the key stakeholders care about, and carry out immediate and appropriate communication and response.">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="../favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
    <?php include ("../ga_codes_header.php")?>   
</head>

<body>
<?php include ("../ga_codes_body.php")?>
<?php include ("components/formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
        <?php include('components/header_en.php')?>
        </header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">Stakeholder</h3>
                        <p class="u-mb-125">
                            Based on the Company's operating attributes, the primary stakeholders are identified as shareholders/investors, employees, consumers, communities, directors and main management, competent authorities, and suppliers. Corresponding departments and channels are also set up to promptly and appropriately communicate and respond to key stakeholders' concerns.​​​​
                        </p>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <table class="table table-bordered l-table l-table-layout-fixed u-mb-000">
                            <thead>
                                <tr>
                                    <th colspan="3">Shareholder/Investor</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td colspan="3" class="u-bg-gray-300">
                                        Shareholders/investors' recognition of the Company is the key driving force for the sustainable operation and development of the business.We are committed to providing investors with long-term and high-quality returns and being accountable and responsive to their feedback.
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-text-blue-highlight align-middle">Issues of concern</th>
                                    <th class="u-text-blue-highlight align-middle">Communication channel and method</th>
                                    <th class="u-text-blue-highlight align-middle">Communication frequency</th>
                                </tr>
                                <tr>
                                    <td class="align-middle">Corporate governance</td>
                                    <td class="align-middle">Investor hotline</td>
                                    <td class="align-middle">On occasion</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Risk management</td>
                                    <td class="align-middle">Investor conference</td>
                                    <td class="align-middle">twice / year</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Shareholder participation</td>
                                    <td class="align-middle">Major information release</td>
                                    <td class="align-middle">On occasion</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Operational performance</td>
                                    <td class="align-middle">Company financial report</td>
                                    <td class="align-middle">Once / quarter</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <table class="table table-bordered l-table l-table-layout-fixed u-mb-000">
                            <thead>
                                <tr>
                                    <th colspan="3">Employee</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td colspan="3" class="u-bg-gray-300">
                                        Employees' initiative and willingness have a significant impact on the Company. We hope to create a high-quality working environment with employees in combination with their personal goals and corporate mission and take care of their families.</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-text-blue-highlight align-middle">Issues of concern</th>
                                    <th class="u-text-blue-highlight align-middle">Communication channel and method</th>
                                    <th class="u-text-blue-highlight align-middle">Communication frequency</th>
                                </tr>
                                <tr>
                                    <td class="align-middle">Employee welfare</td>
                                    <td class="align-middle">Internal website and e-mail</td>
                                    <td class="align-middle" rowspan="5">On occasion</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Employee training</td>
                                    <td class="align-middle">Internal announcement</td>
                                </tr>
                                <tr>
                                    <td class="align-middle" rowspan="3">Labor relations</td>
                                    <td class="align-middle">The Employee Welfare Committee meeting</td>
                                </tr>
                                <tr>
                                    <td class="align-middle border-left u-bdr-left-black">Labor-management meeting</td>
                                </tr>
                                <tr>
                                    <td class="align-middle border-left u-bdr-left-black">Discussion between management and employees, emergency relief, retirees care</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <table class="table table-bordered l-table l-table-layout-fixed u-mb-000">
                            <thead>
                                <tr>
                                    <th colspan="3">Consumer</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td colspan="3" class="u-bg-gray-300">Consumer needs and preferences are the keys to the innovative development of our business and business model.</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-text-blue-highlight align-middle">Issues of concern</th>
                                    <th class="u-text-blue-highlight align-middle">Communication channel and method</th>
                                    <th class="u-text-blue-highlight align-middle">Communication frequency</th>
                                </tr>
                                <tr>
                                    <td class="align-middle">Information security</td>
                                    <td class="align-middle" rowspan="3">Customer service hotline</td>
                                    <td class="align-middle" rowspan="3">On occasion</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Customer protection and communication</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Quality of goods and services</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <table class="table table-bordered l-table l-table-layout-fixed u-mb-000">
                            <thead>
                                <tr>
                                    <th colspan="3">Community</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td colspan="3" class="u-bg-gray-300">We look forward to giving back the accumulated achievements of our business to society and create a better future.</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-text-blue-highlight align-middle">Issues of concern</th>
                                    <th class="u-text-blue-highlight align-middle">Communication channel and method</th>
                                    <th class="u-text-blue-highlight align-middle">Communication frequency</th>
                                </tr>
                                <tr>
                                    <td class="align-middle">Corporate image</td>
                                    <td class="align-middle">Charitable activities</td>
                                    <td class="align-middle" rowspan="3">On occasion</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Community care</td>
                                    <td class="align-middle">Information disclosure on the official website</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Social participation</td>
                                    <td class="align-middle">Corporate representative telephone number and e-mail</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <table class="table table-bordered l-table l-table-layout-fixed u-mb-000">
                            <thead>
                                <tr>
                                    <th colspan="3">Directors and Main Management</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td colspan="3" class="u-bg-gray-300">
                                        Directors and management play a crucial role in assisting the Company in complying with relevant laws and corporate governance. The excellent interaction between them facilitates the effective enhancement of corporate competitiveness and risk control.</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-text-blue-highlight align-middle">Issues of concern</th>
                                    <th class="u-text-blue-highlight align-middle">Communication channel and method</th>
                                    <th class="u-text-blue-highlight align-middle">Communication frequency</th>
                                </tr>
                                <tr>
                                    <td class="align-middle">Operational performance</td>
                                    <td class="align-middle">Board of Directors</td>
                                    <td class="align-middle">At least six times / year</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Operational strategy</td>
                                    <td class="align-middle">The Remuneration Committee</td>
                                    <td class="align-middle">At least twice / year</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Corporate governance</td>
                                    <td class="align-middle">Operational meeting</td>
                                    <td class="align-middle">Once / month</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Risk management</td>
                                    <td class="align-middle">Project meeting</td>
                                    <td class="align-middle">On occasion</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <table class="table table-bordered l-table l-table-layout-fixed u-mb-000">
                            <thead>
                                <tr>
                                    <th colspan="3">Competent Authority</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td colspan="3" class="u-bg-gray-300">
                                        All our products, services, and related marketing activities are subject to inspection and supervision by the competent authority.</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-text-blue-highlight align-middle">Issues of concern</th>
                                    <th class="u-text-blue-highlight align-middle">Communication channel and method</th>
                                    <th class="u-text-blue-highlight align-middle">Communication frequency</th>
                                </tr>
                                <tr>
                                    <td class="align-middle">Legal compliance</td>
                                    <td class="align-middle">E-mail</td>
                                    <td class="align-middle" rowspan="4">On occasion</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Corporate governance</td>
                                    <td class="align-middle" rowspan="3">Correspondence</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Risk management</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Communication with competent authorities</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <table class="table table-bordered l-table l-table-layout-fixed u-mb-000">
                            <thead>
                                <tr>
                                    <th colspan="3">Supplier</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td colspan="3" class="u-bg-gray-300">
                                        Our service system relies on the stable and high-quality support of our suppliers.</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-text-blue-highlight align-middle">Issues of concern</th>
                                    <th class="u-text-blue-highlight align-middle">Communication channel and method</th>
                                    <th class="u-text-blue-highlight align-middle">Communication frequency</th>
                                </tr>
                                <tr>
                                    <td class="align-middle">Information security</td>
                                    <td class="align-middle">E-mail</td>
                                    <td class="align-middle" rowspan="4">On occasion</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Sustainable development strategy</td>
                                    <td class="align-middle">Telephone no.</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Corporate image</td>
                                    <td class="align-middle" rowspan="2">Field visit</td>
                                </tr>
                                <tr>
                                    <td class="align-middle">Supplier management</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-pt-100 u-pb-400">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">
                            Stakeholder Contact Information</h4>
                        <ul class="u-mb-000">
                            <li class="mb-3">Spokesperson: Li-Hui Chang</li>
                            <li class="mb-3">Spokesperson Tel. No.: (02)2697-2886</li>
                            <li class="mb-3">Deputy Spokesperson: Hsiao-Ping Wang</li>
                            <li class="mb-3">Deputy Spokesperson Tel. No.: (02)2697-2886</li>
                            <li>E-mail：<a href="mailto:ally@ms.formosa-opt.com.tw">ally@ms.formosa-opt.com.tw</a></li>
                        </ul>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer">
            <?php include('components/footer_en.php') ?>
        </footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>