<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Corporate Social Responsibility  ｜ formosa</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="According to the principle of materiality, Formosa Optical Technology  has conducted regular risk assessments on environmental, social or corporate governance issues related to the company's operations, and formulated the `Code of Practice for Corporate Social Responsibility` to enhance the effectiveness of corporate social responsibility.">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="../favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
    <?php include ("../ga_codes_header.php")?>   
</head>

<body>
<?php include ("../ga_codes_body.php")?>
<?php include ("components/formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
        <?php include('components/header_en.php')?>
        </header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">Corporate Social Responsibility </h3>
                    </div>
                </div>
                <!-- start table -->
                <div class="u-pb-100">
                    <div class="container">
                        <!-- <h4 class="c-title-underline u-font-16 u-md-font-22">Corporate Social Responsibility</h4> -->
                        <div class="l-tablesSroller-wrapper">
                            <table class="table table-bordered l-table u-mb-000">
                                <thead>
                                    <tr>
                                        <th colspan="1" rowspan="2" class="u-p-100 l-table-vertical-align-middle"
                                            style="min-width: 250px;">
                                            Evaluation item
                                        </th>
                                        <th colspan="3" rowspan="1" class="u-p-100 l-table-vertical-align-middle"
                                            style="min-width: 500px;">
                                            Implementation status
                                        </th>
                                        <th colspan="1" rowspan="2" class="u-p-100 l-table-vertical-align-middle th-en">
                                            Deviations from the Ethical Corporate Management Best Practice
                                            Principles for TWSE / TPEx Listed Companies and reasons thereof
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="1" rowspan="1"
                                            class="l-table-vertical-align-middle u-bdr-left-white border-left u-p-100"
                                            style="min-width: 50px;">
                                            Yes
                                        </th>
                                        <th colspan="1" rowspan="1" class="u-p-100 l-table-vertical-align-middle"
                                            style="min-width: 50px;">
                                            No
                                        </th>
                                        <th colspan="1" rowspan="1" class="u-p-100 l-table-vertical-align-middle">
                                            Description
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>Ⅰ.Does the Company establish an exclusively (or concurrently)
                                                    dedicated unit to implement corporate social responsibility (CSR),
                                                    conduct a risk assessment of environmental, social, or corporate
                                                    governance (ESG) issues associated with its operations based on the
                                                    principle of materiality, formulate relevant risk management
                                                    policies or strategies, and disclose them on the Company website and
                                                    annual report?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>&nbsp;&nbsp;&nbsp;The Company has established the Finance and
                                                    Accounting Department as the concurrent unit of corporate governance
                                                    to be responsible for corporate governance-related businesses and
                                                    appointed Chief Finance and Accounting Officer, Vice President
                                                    Li-Hui Chang, as the head of corporate governance to be responsible
                                                    for the formulation and supervision of policies, management
                                                    guidelines, and objectives on issues such as corporate governance,
                                                    corporate social responsibility, corporate ethical management,
                                                    environmental sustainability, and risk management.All relevant
                                                    departments will conduct risk assessments, formulate countermeasures
                                                    according to their duties, and review them regularly every year.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr  class="u-bg-gray-02">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>Ⅱ.Does the Company establish an exclusively (or concurrently)
                                                    dedicated unit to implement CSR, and has the Board of Directors
                                                    appointed senior management with responsibility for CSR and report
                                                    the handling status to the Board of Directors?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>&nbsp;&nbsp;&nbsp;The Audit Office and the Administration Office
                                                    shall coordinate and promote the work related to corporate social
                                                    responsibility.Besides advocating energy-saving and resource
                                                    recycling to employees, the Company also sells eco-friendly
                                                    materials frames in stores and requires the stores to regularly
                                                    participate in community cleaning services in their respective
                                                    communities to give back to society in order to fulfill relevant
                                                    corporate social responsibilities.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅲ.Environmental issues</li>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A. Does the Company establish an appropriate environmental
                                                    management system based on its industrial characteristics?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.The Company is mainly engaged in the optical retail industry,
                                                    which has a much lower impact on the environment than that of the
                                                    general manufacturing industry. Therefore, the focal point
                                                    concerning a friendly environment is to focus on power saving and
                                                    the reduction of paperwork in stores. To develop a sustainable
                                                    environment and utilize all resources more efficiently, various
                                                    energy-saving management systems have been established.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;Does the Company endeavor to utilize all resources more
                                                    efficiently and use renewable materials that have a low impact on
                                                    the environment?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;To improve the utilization efficiency of various resources,
                                                    the Company has established an e-document management center to adopt
                                                    online approval documents, reduce the use of paper, and continuously
                                                    and strongly promote to reduce unnecessary printing. If printing is
                                                    necessary, documents should be printed in double-sided or by
                                                    recycled paper. In the new fiscal year, it is hoped that the paper
                                                    consumption can be reduced by 5% and the utilization rate of
                                                    recycled paper can reach 20%.
                                                    For the power consumption, apart from establishing energy-saving
                                                    habits among employees, meanwhile, the Company also reduces use
                                                    hours of air-conditioning and raises the average indoor temperature
                                                    in the new fiscal year, hoping to achieve a 3% reduction in
                                                    electricity consumption compared with the previous year and a 0.6%
                                                    reduction in electricity bills in 2020 from the previous year."
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;Does the Company evaluate the current and future potential
                                                    risks and opportunities of climate change, and take countermeasures
                                                    to respond to climate-related issues?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;The temperature rise caused by global warming will escalate
                                                    the power consumption of air-conditioning in stores and increase the
                                                    Company's operating costs. To effectively mitigate the impact, the
                                                    Company actively participates in various government energy-saving
                                                    and carbon-reduction policies and adopts winter and summer uniforms
                                                    to reduce the hardship of colleagues during business
                                                    operations.Furthermore, a crisis response team is set up to deal
                                                    with the impact of natural disasters on the stores.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>D.Does the Company conduct statistics on the greenhouse gas
                                                    emissions, water consumption, and total waste weight for the past
                                                    two years, and correspondingly formulate management policies for
                                                    energy saving and carbon reduction, greenhouse gas reduction, water
                                                    use reduction, or other wastes?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>D.The Company takes part in demonstration coaching of greenhouse gas
                                                    reduction in the commercial service industry conducted by the
                                                    Ministry of Economic Affairs, promotes the operation of improving
                                                    energy usage in stores, energy-saving potential of energy-consuming
                                                    equipment, and lessening energy expenditure, and conducts periodic
                                                    reviews. In 2020, 373,541 kWh of electricity has been saved. In
                                                    2021, it is expected to continuously expand the implementation
                                                    stores and achieve the goal of doubling the power-saving kWh.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅳ.Social issues</li>
                                            </ul>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.Does the Company formulate appropriate management policies and
                                                    procedures according to relevant regulations and the International
                                                    Bill of Human Rights?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan=" 1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    A.The Company respects and stands by the labor standards “United
                                                    Nations Universal Declaration of Human Rights,” ”United Nations
                                                    Global Covenant,” and “International Labor Organization Convention,”
                                                    and formulates relevant working rules of the Company accordingly,
                                                    including fairness and non-discrimination, good labor relations, and
                                                    prohibition of child labor, prohibition of forced and compulsory
                                                    labor, working hours specification, compliance with basic salary,
                                                    the establishment of a safe and healthy working environment,
                                                    education and training, supplier management, smooth communication
                                                    channels for employee rights, etc., and continuously disseminates at
                                                    the annual grassroots and cadre training meetings at all levels.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.Does the Company formulate and implement reasonable employee
                                                    benefit measures (including remuneration, vacation, and other
                                                    benefits) and appropriately reflect operating performance or results
                                                    in employee compensation?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan=" 1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    B.To fully take care of employees to ensure their living conditions,
                                                    provide good working conditions, and meet their needs, besides
                                                    providing basic security according to law, the Employee Welfare
                                                    Committee is also set up to handle various employee welfare
                                                    activities and subsidies.The Company has formulated relevant
                                                    remuneration, various bonuses, and performance appraisal schemes to
                                                    effectively link work performance with individual remuneration.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.Does the Company provide a healthy and safe work environment, and
                                                    organize health and safety training for its employees on a regular
                                                    basis?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan=" 1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    C.To ensure the safety and health of all employees, the Company has
                                                    formulated and published "Employee Welfare, Working Environment and
                                                    Personal Safety Protection Measures" on the Company's website,
                                                    together with regular lectures on health and safety concepts, health
                                                    and safety work codes of practice, emergency response, firefighting
                                                    and first aid knowledge, other health and safety knowledge related
                                                    to employee operations, etc.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>D.Does the Company establish effective career development and
                                                    training plans for its employees?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan=" 1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    D.The Company has a complete training system with an in-house
                                                    entrepreneurship approach and assists employees in obtaining
                                                    professional licenses required for their work through educational
                                                    training.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>E.Does the Company comply with relevant regulations and
                                                    international standards in terms of customer health and safety,
                                                    customer privacy, marketing and labeling of products and services,
                                                    and formulate relevant consumer protection policies and complaint
                                                    procedures?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan=" 1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    E.All the Company's products are labeled according to the Commodity
                                                    Labeling Act. Meanwhile, the 0800 service consultation hotline is
                                                    set up, where a dedicated person will track and improve the problems
                                                    raised by customers. Also, the "Whistleblowing Measures" are
                                                    disclosed on the Company's website, and a special email is set up to
                                                    provide employees and consumers to report any improper practices,
                                                    and the Company has designated appropriate authority to handle it
                                                    personally.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>F.Does the Company formulate supplier management policies that
                                                    require suppliers to comply with relevant regulations on
                                                    environmental protection, occupational safety and health, or labor
                                                    rights and its implementation?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan=" 1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    F.The Company has established the “Supplier Management Policy” and
                                                    the procurement department will conduct an assessment on the
                                                    suppliers' relevant corporate social responsibility. <br>
                                                    Before entering contracts with suppliers, the Company assesses that
                                                    if there is any material adverse effect on the environment and
                                                    society, the Company will proactively terminate the relevant
                                                    transactions with suppliers.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>G.Do the Company's website and annual report disclose the protection
                                                    measures and implementation of employee safety and working
                                                    environment?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan=" 1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    G.The Company regularly carries out promotions and drills for
                                                    disasters and robberies, keeps relevant methods and examples on the
                                                    Company's internal and external websites, and installs security
                                                    systems in all stores and plants in connection with the system
                                                    security company.See Note II for drill examples.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>H.Does the Company refer to international human rights conventions,
                                                    formulate policies to protect human rights and specific management
                                                    plans, and disclose them on the Company website or annual report?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan=" 1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    H.The Company abides by relevant business and labor regulations and
                                                    formulates the "work rules" and other relevant management policies
                                                    and procedures according to law to protect the legitimate rights and
                                                    interests of employees.When recruiting employees, there is no
                                                    restraint on gender, religion, race, or political standpoint.
                                                    Through open recruitment, people are employed based on talents.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>Ⅴ.Does the Company compile the corporate social responsibility (CSR)
                                                    report following the internationally accepted standards or
                                                    guidelines for the preparation of reports to disclose non-financial
                                                    information of the Company? Has the aforesaid report received
                                                    assurance or certification opinion of the third-party accreditation
                                                    institution?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    &nbsp;&nbsp;&nbsp;  The Company abides by relevant business and labor
                                                    regulations and formulates the "work rules" and other relevant
                                                    management policies and procedures according to law to protect the
                                                    legitimate rights and interests of employees.When recruiting
                                                    employees, there is no restraint on gender, religion, race, or
                                                    political standpoint. Through open recruitment, people are employed
                                                    based on talents.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            Under evaluation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅵ.If the Company has established corporate social responsibility
                                                    practice principles based on the "Corporate Social Responsibility
                                                    Best Practice Principles for TWSE / TPEx Listed Companies," describe
                                                    the implementation and any deviations from the Principles:</li>
                                                <li style="text-indent:-15px;">The Company has passed the “Corporate
                                                    Social Responsibility Practice Principles” in 2015, which currently
                                                    applies to many aspects such as environmental protection, social
                                                    services, consumer rights, social responsibility activities, and
                                                    labor health and safety. The actual operation is consistent with the
                                                    "Corporate Social Responsibility Best Practice Principles for
                                                    TWSE/TPEx Listed Companies," and there is no material deviation.
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>Ⅶ.Other important information that helps to understand the operation
                                                    of corporate social responsibility: </li>
                                                <li style="text-indent:-15px;">The major events of the Company over the
                                                    years are shown in Note I below. </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">Note I</h4>
                        <table class="table table-bordered l-table u-mb-000">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        The Chronicle of Formosa Optical Charitable Activities​​​​​​​​​​​​​​​​​​​​
                                    </th>
                                </tr>
                                <tr>
                                    <th class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-bottom-black">
                                        Year
                                    </th>
                                    <th
                                        class="u-bg-gray-300 u-text-gray-800 text-center u-bdr-left-black u-bdr-bottom-black">
                                        Activities​​​​​​
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td rowspan="2" class="text_fristline">
                                        2014
                                    </td>
                                    <td valign="top" class="text_line">
                                        In 2014, the "Formosa's eye with beautiful horizon" cared for underprivileged students and donated 2,000 pairs of glasses to the Ministry of Health and Welfare, helping destitute students with the free glasses.​​​​​
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="text_line border-left  u-bdr-left-black">
                                        Assisted Chiayi Chang Gung Memorial Hospital's medical services in Chishang Township and Dawu Township in Taitung County, providing free clinic services such as vision examination.
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="3" class="text_fristline">
                                        2015
                                    </td>
                                    <td valign="top" class="text_line">
                                        For the fifth consecutive year, the "Formosa's eye with beautiful horizon" donated 2,000 pairs of glasses to the Ministry of Health and Welfare to care for underprivileged students.​​​​
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="text_line border-left  u-bdr-left-black">
                                        Cooperated with the medical team of Chang Gung Memorial Hospital to provide vision health examination and optician services in remote areas of Taitung.
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="text_line border-left  u-bdr-left-black">
                                        Assisted the Taiwan Fund for Children and Families to promote the issue of caring and protecting children and was awarded a certificate of appreciation.
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2" class="text_fristline">
                                        2016
                                    </td>
                                    <td valign="top" class="text_line">
                                        For the sixth consecutive year, the "Formosa's eye with beautiful horizon" donated 2,000 pairs of glasses to the Ministry of Health and Welfare to care for underprivileged students.​​​​
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="text_line border-left  u-bdr-left-black">
                                        Assisted the Taiwan Fund for Children and Families to promote the issue of caring and protecting children and was awarded a certificate of appreciation.
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2" class="text_fristline">
                                        2017
                                    </td>
                                    <td valign="top" class="text_line">
                                        For the seventh consecutive year, the "Formosa's eye with beautiful horizon" donated 2,000 pairs of glasses to the Ministry of Health and Welfare to care for underprivileged students.​​​​
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="text_line border-left  u-bdr-left-black">
                                        Assisted the Taiwan Fund for Children and Families to promote the issue of caring and protecting children and was awarded a certificate of appreciation.
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="3" class="text_fristline">
                                        2018
                                    </td>
                                    <td valign="top" class="text_line">
                                        For the eighth consecutive year, the "Formosa's eye with beautiful horizon" donated 2,000 pairs of glasses to the Ministry of Health and Welfare to care for underprivileged students.​​​​
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="text_line border-left  u-bdr-left-black">
                                        Cooperated with the Taiwan Prevent Blindness Foundation and provided free clinic services such as vision examination and free optician in Beinan Township, Taitung County.
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="text_line border-left  u-bdr-left-black">
                                        The Formosa's 61 stores in New Taipei jointly participated in the "New Taipei Glasses Project for the Elderly" organized by the New Taipei City Government to help New Taipei citizens over 65 years of age with free vision examinations and opticians.​​​​
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text_fristline">
                                        2019
                                    </td>
                                    <td valign="top" class="text_line">
                                        For the ninth consecutive year, the "Formosa's eye with beautiful horizon" donated 2,000 pairs of glasses to the Ministry of Health and Welfare to care for underprivileged students.​​​​
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text_fristline">
                                        2020
                                    </td>
                                    <td valign="top" class="text_line">
                                        For the tenth consecutive year, the "Formosa's eye with beautiful horizon" initiated the Smart Eye-Care Tour Public Service Program for the rural areas to serve the local people, providing visual examination and free glasses.​​
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100 u-pb-400">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">Download</h4>
                        <div class="row">
                            <div class="col-auto">
                                <a target="_blank" class="c-btn c-btn--download c-btn-gray-800 u-text-black px-3"
                                    href="../download/pdf/investors/15.履行企業社會責任情形-附註二-防火防搶演練範例.pdf" target=_blank"">
                                    <img src="../assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050"
                                        style="width: 17px;">
                                    <span>Fire and anti-robbing drill examples</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end table -->


            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer">
            <?php include('components/footer_en.php') ?>
        </footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>