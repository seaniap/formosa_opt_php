<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Shareholders Meeting Information ｜ formosa</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="Provide relevant information about the annual shareholders meeting of Formosa Optical Technology">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="../favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="../assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
    <?php include ("../ga_codes_header.php")?>   
</head>

<body>
<?php include ("../ga_codes_body.php")?>
<?php include ("components/formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
        <?php include('components/header_en.php')?>
        </header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">Shareholders Meeting Information</h3>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <div class="c-accordion">
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent11">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2022</p>
                                        </div>
                                        <div class="c-accordion-btn-icon js-accordionExpended">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent11" class="c-accordion-content js-accordionExpended">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/111/111年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2022 報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/111/111年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2022 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/111/111年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2022 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/111/111年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2022 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/111/111年開會通知_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2022 開會通知(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/111/111年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2022 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/111/111年議事手冊及會議補充資料_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2022 議事手冊及會議補充資料(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/111/111年取得或處分資產處理程序_訂定或修正.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2022 取得或處分資產處理程序(訂定或修正)</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent10">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2021</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent10" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021 報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年開會通知_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021 開會通知(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年議事手冊及會議補充資料_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021 議事手冊及會議補充資料(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年取得或處分資產處理程序_訂定或修正.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021 取得或處分資產處理程序(訂定或修正)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/110/110年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2021年 股東會議事錄</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent9">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2020</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent9" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/109/109年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2020 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/109/109年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2020 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/109/109年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2020 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/109/109年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2020 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/109/109年開會通知_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2020 開會通知(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/109/109年會議議事手冊_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2020 會議議事手冊(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/109/109年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2020 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/109/109年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2020年 股東會議事錄</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent8">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2019</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent8" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/108/108年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2019 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/108/108年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2019 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/108/108年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2019 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/108/108年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2019 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/108/108年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2019 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/108/108年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2019 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/108/108年取得或處分資產處理程序修正條文對照表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2019 取得或處分資產處理程序修正條文對照表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/108/108年資金貸與他人或背書保證作業程序修正條文對照表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2019 資金貸與他人或背書保證作業程序修正條文對照表</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent7">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2018</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent7" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/107/107年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2018 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/107/107年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2018 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/107/107年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2018 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/107/107年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2018 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/107/107年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2018 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/107/107年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2018 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent6">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2017</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent6" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/106/106年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2017 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/106/106年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2017 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/106/106年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2017 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/106/106年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2017 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/106/106年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2017 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/106/106年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2017 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent5">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2016</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent5" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/105/105年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2016 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/105/105年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2016 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/105/105年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2016 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/105/105年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2016 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/105/105年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2016 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/105/105年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2016 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent4">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2015</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent4" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/104/104年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2015 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/104/104年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2015 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/104/104年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2015 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/104/104年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2015 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/104/104年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2015 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/104/104年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2015 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent3">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2014
                                            </p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent3" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/103/103年年報前十大股東相互關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2014 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/103/103年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2014 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/103/103年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2014 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/103/103年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2014 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/103/103年背書保證作業程序修正對照表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2014 背書保證作業程序修正對照表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/103/103年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2014 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/103/103年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2014 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent2">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2013
                                            </p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent2" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/102/102開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2013 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/102/102股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2013 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/102/102股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2013 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/102/102年報前十大股東相互關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2013 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/102/102事議手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2013 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/102/102資金貸與他人程序.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2013 資金貸與他人程序</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/102/102股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2013 股東會年報</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent1">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">

                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">2012
                                            </p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent1" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/101/101開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2012 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/101/101股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2012 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/101/101股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2012 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/101/101年報前十大股東相互關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2012 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/101/101取得或處分資產處理程序(訂定或修訂).pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2012 取得或處分資產處理程序(訂定或修正)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/101/101股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2012 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="../download/pdf/investors/shareholder/101/101事議手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="../assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>2012 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                        </div>
                    </div>
                </div>
                <!-- START 頁籤 -->
                <div class="u-py-100 u-pb-400">
                    <div class="container">
                       <!-- <nav>
                            <ul class="c-pagination justify-content-center align-items-center u-mb-000">
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_to_left"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_left"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item active"><a href="#">1</a></li>
                                <li class="c-pagination-item"><a href="#">2</a></li>
                                <li class="c-pagination-item"><a href="#">3</a></li>
                                <li class="c-pagination-item"><a href="#">4</a></li>
                                <li class="c-pagination-item"><a href="#">5</a></li>
                                <li class="c-pagination-item">...</li>
                                <li class="c-pagination-item"><a href="#">10</a></li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_right"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_to_right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>-->
                    </div>
                </div>
                <!-- END 頁籤 -->
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer">
            <?php include('components/footer_en.php') ?>
        </footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>