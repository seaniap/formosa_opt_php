<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Corporate Governance Implementation Status ｜ formosa</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="The content includes information about the governance and operation of Formosa Optical Technology, the independence of accountants and the assessment of competence.">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <link rel="shortcut icon" href="../favicon.ico" />
    <link rel="preload" href="../assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="../assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="../assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="../assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="../assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="../assets/css/main.css" />
    <?php include ("../ga_codes_header.php")?>   
</head>

<body>
<?php include ("../ga_codes_body.php")?>
<?php include ("components/formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
        <?php include('components/header_en.php')?>
        </header>
        <div id="sideLinkEn" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">Corporate Governance Implementation Status</h3>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">I. Implement Status</h4>
                        <div class="l-tablesSroller-wrapper">
                            <table class="table table-bordered l-table">
                                <thead>
                                    <tr>
                                        <th colspan="1" rowspan="2" class="l-table-vertical-align-middle u-p-100"
                                            style="min-width: 120px;">
                                            Evaluation item
                                        </th>
                                        <th colspan="3" rowspan="1" class="l-table-vertical-align-middle u-p-100">
                                            Implementation status
                                        </th>
                                        <th colspan="1" rowspan="2" class="l-table-vertical-align-middle u-p-100">
                                            Deviations from the Ethical Corporate Management Best Practice Principles
                                            for TWSE/TPEx Listed Companies and reasons thereof
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="1" rowspan="1"
                                            class="l-table-vertical-align-middle u-bdr-left-white border-left u-p-100"
                                            style="min-width: 50px;">
                                            Yes
                                        </th>
                                        <th colspan="1" rowspan="1" class="l-table-vertical-align-middle u-p-100"
                                            style="min-width: 50px;">
                                            No
                                        </th>
                                        <th colspan="1" rowspan="1" class="l-table-vertical-align-middle u-p-100">
                                            Description
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>&nbsp;I.&nbsp;Does the Company establish and disclose its
                                                    Corporate Governance Best Practice Principles based on the
                                                    "Corporate Governance Best Practice Principles for TWSE/TPEx Listed
                                                    Companies?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center  text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li class="u-mb-100">A.&nbsp;The Company has formulated the "Corporate Governance Best
                                                    Practice Principles" and disclosed them in the corporate governance
                                                    section of the Company's website. Its composition and operation are
                                                    as follows:
                                                    <ul class="u-list-style--custom-level03">
                                                        <li>(A)&nbsp;Protect the rights and interests of shareholders.
                                                        </li>
                                                        <li>(B)&nbsp;Strengthen the function of the Board of Directors.
                                                        </li>
                                                        <li>(C)&nbsp;Exert the function of the Audit Committee.</li>
                                                        <li>(D)&nbsp;Respect the rights and interests of stakeholders.
                                                        </li>
                                                        <li>(E)&nbsp;Enhance information transparency.</li>
                                                    </ul>
                                                </li>
                                                <li>B.&nbsp;The actual operation of the Company is in accordance with
                                                    the Company's "Corporate Governance Best Practice Principles." The
                                                    Company has successively completed the formulation of "Rules of
                                                    Procedure for Shareholders' Meetings," "Measures for Monitoring
                                                    Subsidiaries," "Procedures for Acquisition or Disposal of Assets,"
                                                    "Rules of Procedures for the Board of Directors' Meetings,"
                                                    "Director Selection Procedures," "Code of Ethical Conduct,"
                                                    "Corporate Governance Best Practice Principles," and "Ethical
                                                    Corporate Management Principles." There is no deviation between its
                                                    operation and the principles above.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class=" u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>&nbsp;II.&nbsp;Shareholding structure & shareholders' rights</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;Does the Company establish internal operating procedures to
                                                    deal with shareholders' suggestions, doubts, disputes, and
                                                    litigations, and does the Company implement them in accordance with
                                                    the procedures?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center  text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;The Company has established a spokesperson system to make
                                                    the unified external speech and handle shareholders' doubts and
                                                    disputes.</li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;Does the Company possess a list of its major shareholders
                                                    with controlling power as well as the ultimate owners of those major
                                                    shareholders?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            &nbsp;
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;The Company and the Stock Affairs Department of MasterLink
                                                    Securities, the stock affairs agency, regularly keep abreast of the
                                                    Company's major shareholders and their controllers.</li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;Does the Company establish and execute a risk management and
                                                    firewall system within its affiliated companies?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center  text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;The Company has formulated the "Operating Procedures for
                                                    Related Party Transactions," "Measures for Monitoring Subsidiaries,"
                                                    "Procedures for Acquisition or Disposal of Assets," "Operating
                                                    Procedures for Lending Funds to Others," and "Operating Procedures
                                                    of Endorsement and Guaranty."The Company conducts business with
                                                    affiliated companies in accordance with relevant regulations,
                                                    internal control systems, and laws.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>D.&nbsp;Does the Company establish internal rules against insiders
                                                    using undisclosed information to trade in securities?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    D.&nbsp;The Company has formulated the "Management Measures of the
                                                    Prevention of Insider Trading," which prohibits corporate insiders
                                                    from using undisclosed information on the market to buy and sell
                                                    securities.The Company also advocates this operating procedure to
                                                    insiders from time to time to avoid violating relevant regulations.
                                                    The Company conducts educational promotion on the "Management
                                                    Measures of the Prevention of Insider Trading" and related laws for
                                                    current directors, managerial officers, and employees at least once
                                                    a year. Educational training will be arranged for new directors and
                                                    managerial officers after assuming the positions and new employees
                                                    during pre-employment training.The content for promoting includes
                                                    the scope of material internal information, confidential operations,
                                                    open operations, and handling of violations.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>III.&nbsp;Composition and responsibilities of the Board of Directors
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;Does the Board develop and implement a diversity policy for
                                                    the composition of its members?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level03 u-mb-100">
                                                <li>(A)&nbsp;The Company has established the "Board Member
                                                    Diversification Policy" and the "Director Election Procedures,"
                                                    which require the overall Board should have various capabilities as
                                                    a diversified policy for the composition of members and be handled
                                                    under the procedures.
                                                </li>
                                                <li>(B)&nbsp;The Board members of the Company are diversified and
                                                    composed of talents with international perspectives, regional
                                                    management capabilities, financial management, copper process
                                                    specialty, and polymer chemical and international business experts
                                                    to enhance the structure of the Company's Board of Directors.
                                                </li>
                                                <li>(C)&nbsp;The Company's implementation of the Board member
                                                    diversification policy:</li>
                                            </ul>
                                            <table class="text-center u-mb-100 table-bordered l-table u-font-12">
                                                <tbody>
                                                    <tr class="u-bg-gray-300">
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="p-0">Gender</td>
                                                        <td class="p-0">Operational judgment capability</td>
                                                        <td class="p-0">Accounting and financial analysis capability</td>
                                                        <td class="p-0">Business management capability</td>
                                                        <td class="p-0">Crisis management capability</td>
                                                        <td class="p-0">Industrial knowledge</td>
                                                        <td class="p-0">Global market insight</td>
                                                        <td class="p-0">Leadership capability</td>
                                                        <td class="p-0">Decision-making capability</td>
                                                        <td class="p-0">Law</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Kuo-Chou<br>Tsai
                                                        </td>
                                                        <td  class="p-0" colspan="1" rowspan="1">
                                                            Male
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="u-bg-gray-100">
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Kuo-Ping<br>Tsai
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Male
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Chen Liu<br>Hui-Yu
                                                        </td>
                                                        <td class="" colspan="1" rowspan="1">
                                                            Female
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="u-bg-gray-100">
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Yi-Shan<br>Tsai
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Female
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Yu-Ching<br>Tsai
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Female
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="u-bg-gray-100">
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Chung-Chi<br>Wen
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Male
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Meng-Jou<br>Wu
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Male
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                    </tr>
                                                    <tr class="u-bg-gray-100">
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Tzu-Chiang<br>Chueh
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Male
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Chih-Wei<br>Chang
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Male
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="u-bg-gray-100">
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Hsiu-Pi<br>Yao
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Female
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="white-space-nowrap p-0" colspan="1" rowspan="1">
                                                            Jung-Hui<br>Liang
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                            Male
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="text-center p-0" colspan="1" rowspan="1">
                                                            V
                                                        </td>
                                                        <td class="p-0" colspan="1" rowspan="1">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <ul class="u-list-style--custom-level03">
                                                <li>(D)&nbsp;The Company's directors with employee status accounted for
                                                    18%, independent directors accounted for 36%, female directors
                                                    accounted for 36%; three independent directors have a tenure of four
                                                    to six years, and one independent director has a tenure of more than
                                                    nine years; one director is over 70 years old, four directors are
                                                    between 60 to 69 years old, and six directors are under 60 years
                                                    old.The Company pays attention to gender equality in the composition
                                                    of the Board of Directors, and the ratio of female directors exceeds
                                                    30%.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;Does the Company voluntarily establish other functional
                                                    committees in addition to the legally required Remuneration
                                                    Committee and Audit Committee?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            &nbsp;
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    B.&nbsp;Currently, the Company has established the Remuneration
                                                    Committee in accordance with the law. Additionally, the Board
                                                    approved amendments to the Articles of Incorporation on May 12,
                                                    2020, added the provisions on the establishment of an Audit
                                                    Committee, and established the Audit Committee after the amendment
                                                    to the articles was passed at the Shareholders' Meeting on July 27,
                                                    2021.<br>
                                                    No other functional committees have been set up yet.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            Under evaluation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;Does the Company establish standards to measure the
                                                    performance of the Board and implement such annually, and submit the
                                                    results of the performance evaluations to the Board of Directors,
                                                    and use them as a reference for individual directors' remuneration
                                                    and the nomination for renewal?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>
                                                    C.&nbsp;The Company has formulated the "Performance Evaluation
                                                    Method of the Board of Directors" to conduct performance evaluations
                                                    for the Board of Directors, the Board members, and the Remuneration
                                                    Committee at least once a year.The internal evaluation shall carry
                                                    out the current year's performance evaluation in line with the
                                                    Method before the end of the first quarter of the following year.The
                                                    performance evaluation items of the Company's Board of Directors
                                                    covers at least the following five aspects:
                                                    <ul class="u-list-style--custom-level03">
                                                        <li>(A)&nbsp;Involvement in the Company's operations</li>
                                                        <li>(B)&nbsp;Enhancement to the Board's decision quality</li>
                                                        <li>(C)&nbsp;Composition and structure of the Board</li>
                                                        <li>(D)&nbsp;Selections and continuing education of directors
                                                        </li>
                                                        <li>(E)&nbsp;Internal control</li>
                                                    </ul>
                                                    The performance evaluation items of the Board members (self or
                                                    peers) covers at least the following six aspects:
                                                    <ul class="u-list-style--custom-level03">
                                                        <li>(A)&nbsp;Keeping abreast of the Company's goals and tasks
                                                        </li>
                                                        <li>(B)&nbsp;Recognition of director duties</li>
                                                        <li>(C)&nbsp;Involvement in the Company's operations</li>
                                                        <li>(D)&nbsp;Internal relationship management and communication
                                                        </li>
                                                        <li>(E)&nbsp;Profession and continuing education of directors
                                                        </li>
                                                        <li>(F)&nbsp;Internal control</li>
                                                    </ul>
                                                    The performance evaluation items of the Remuneration Committee
                                                    covers at least the following five aspects:
                                                    <ul class="u-list-style--custom-level03">
                                                        <li>(A)&nbsp;Involvement in the Company's operations</li>
                                                        <li>(B)&nbsp;Recognition of the functional committee's duties
                                                        </li>
                                                        <li>(C)&nbsp;Enhancement to the functional committee's decision
                                                            quality</li>
                                                        <li>(D)&nbsp;Composition of the functional committee and
                                                            appointment of committee members</li>
                                                        <li>(E)&nbsp;Internal control</li>
                                                    </ul>
                                                    The evaluation is carried out by the Chairman's Office in the form
                                                    of an internal questionnaire. The evaluation consists of three
                                                    parts, including operation of the Board, the directors' involvement,
                                                    and operation of the Remuneration Committee, where directors
                                                    evaluate the Board operations and self-evaluate their involvement,
                                                    while the Remuneration Committee members assess the Committee
                                                    operations.<br>
                                                    The Company has recently conducted a 10-year performance appraisal
                                                    of the Board at the beginning of 2021, and reported the evaluation
                                                    results and the continuous strengthening direction in 2021 at the
                                                    Board meeting held on March 24, 2021, as a reference for individual
                                                    directors' future remuneration and the nomination for
                                                    re-appointment.The performance evaluations of the overall Board of
                                                    Directors, individual Board members, and the Remuneration Committee
                                                    are all excellent. The evaluation results have been disclosed on the
                                                    Company's website.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>D.&nbsp;Does the company regularly evaluate the independence of the
                                                    CPAs?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>D.&nbsp;To implement governance and evaluate the independence and
                                                    competence of CPAs, the Board of Directors approved the formulation
                                                    of "Assessment Measures for the Independence and Competency of CPAs"
                                                    on June 26, 2017.According to these measures, the Board of Directors
                                                    periodically evaluates the independence and competence of CPAs every
                                                    year.The Company's Board of Directors passed the assessment of the
                                                    independence and competence of CPAs on March 24, 2021. For detailed
                                                    independence and competence assessment items, please refer to
                                                    [Explication I].According to the assessment, the personal
                                                    qualifications of two CPAs, Ching-Hsia Chang and Yung-Hsian Chao of
                                                    Deloitte Touche are in line with the accountant practice, and they
                                                    involve no direct or indirect financial interest relationship with
                                                    the Company or directors. The quality and timeliness of the
                                                    auditing, taxation and other services of the CPAs meet the
                                                    standards, and they are more independent and competent in essence,
                                                    which is adequate for them to serve as the Company's CPA.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>IV.&nbsp;Does the TWSE/TPEx listed company establish a qualified and appropriate number of corporate governance personnel and appoint a corporate governance director responsible for matters related to corporate governance  (including but not limited to providing directors with the information required for business execution, assisting directors to comply with laws, handling matters related to Board meetings and the Shareholders' Meetings following the regulations, producing minutes of Board meetings and the Shareholders' Meetings, etc.)?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            The Company has established the Finance and Accounting Department as the concurrent unit of corporate governance to be responsible for corporate governance-related businesses. The Board of Directors resolved and approved on May 14, 2019 to appoint Chief Finance and Accounting Officer, Vice President Li-Hui Chang, as the head of corporate governance to safeguard shareholders' rights and strengthen the functions of the Board of Directors.Vice President Li-Hui Chang has over three years of experience as a financial supervisor in public offering companies.The main responsibilities of the head of corporate governance are to handle matters related to the Board meeting and Shareholders' Meeting under the law, produce the minutes of the Board meeting Directors and Shareholders' Meeting, assist directors in taking office and continuing education, provide the information required by the directors to conduct business, and assist directors in complying with laws.<br>
                                            The continuing education in 2020 is as follows:
                                            <ul class="u-list-style--custom-level02 u-my-100">
                                                <li class="u-mb-100">A.&nbsp;Training date: Aug. 6, 2020, Aug. 17, 2020 to Aug. 18, 2020</li>
                                                <li class="u-mb-100">B.&nbsp;Organizer: Taiwan Corporate Governance Association, Accounting Research and Development Foundation</li>
                                                <li class="u-mb-100">C.&nbsp;Course Title: Industry 4.0 and How Enterprises Leading Innovation and Transformation, Impact of the China-US Trade War on Taiwan-funded Enterprises and the Countermeasures, Continuing Education Course for Principal Accounting Officers of Issuers, Securities Firms, and Securities Exchanges
                                                </li>
                                                <li class="u-mb-100">D.&nbsp;Training hours: Three hours, three hours, three hours for corporate governance, three hours for professional ethics and legal responsibility</li>
                                                <li>E.&nbsp;Total training hours in 2020: 12 hours</li>
                                            </ul>
                                            The business performance in 2021 is as follows:
                                            <ul class="u-list-style--custom-level02">
                                                <li class="u-mb-100">A.&nbsp;Assisted independent directors and general directors in performing their duties, provided the required information, and arranged continuing education for directors：
                                                    <ul class="u-list-style--custom-level03">
                                                        <li>(A)&nbsp;Provided the Board members with the latest amendments and development to laws and regulations relating to the Company's businesses and corporate governance at the time of appointment and regular updates.</li>
                                                        <li>(B)&nbsp;Reviewed the confidentiality level of the relevant information and provided corporate information required by the directors to maintain smooth communication and interaction between directors and business managers.</li>
                                                        <li>(C)&nbsp;Assisted in arranging relevant meetings with the internal chief auditor or CPAs for independent directors who need to understand the Company's financial business operation under the Corporate Governance Best Practice Principles.
                                                        </li>
                                                        <li>(D)&nbsp;Assisted independent directors and general directors in drawing up annual training plans and arranging courses according to the Company's industry characteristics and the directors' academic and professional background.</li>
                                                    </ul>
                                                </li>
                                                <li class="u-mb-100">B.&nbsp;Assisted in the matters related to the rules of procedures for Board of Directors' and Shareholders' Meeting as well as legal compliance with resolutions：
                                                    <ul class="u-list-style--custom-level03">
                                                        <li>(A)&nbsp;Reported the Company's corporate governance operation status to the Board of Directors and independent directors and confirmed whether the convening of the Company's Shareholders' Meeting and the Board meeting comply with relevant laws and the Corporate Governance Best-Practice Principles.
                                                        </li>
                                                        <li>(B)&nbsp;Assisted and reminded directors of the regulations to be followed when performing business or making formal Board resolutions, and put forward suggestions when the Board of Directors was about to make illegal resolutions.
                                                        </li>
                                                        <li>(C)&nbsp;After the meeting, be responsible for reviewing the major information release of significant resolutions of the Board of Directors, ensuring the legality and correctness of the content of the vital information to guarantee investor transaction information equivalence.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="u-mb-100">C.&nbsp;Drew up the agenda of Board meetings and notified the directors seven days in advance, convened the meeting and provided meeting materials, reminded beforehand if the issues requiring recusal of interest, and completed the Board meeting minutes within 20 days after the meeting.
                                                </li>
                                                <li>D.&nbsp;Conducted the pre-registration of the date of the Shareholders' Meeting under the law, prepared meeting notices, meeting handbooks, and minutes of proceedings within the statutory period, and handled the registration of changes when amending the Articles of Incorporation or re-electing directors.</li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>&nbsp;V.&nbsp;Does the Company establish communication channels and a dedicated section on its website for stakeholders (including but not limited to shareholders, employees, customers, and suppliers) to respond to material corporate social responsibility issues in a proper manner?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level03">
                                                <li class="u-mb-100">A.&nbsp;The Company maintains smooth communication channels with banks, other creditors, employees, consumers, suppliers, communities, or the Company stakeholders, responds to all issues promptly, as well as respects and safeguards their due legitimate rights and interests.
                                                </li>
                                                <li class="u-mb-100">B.&nbsp;Stakeholders can immediately obtain the Company's operational information through the Market Observation Post System (MOPS).</li>
                                                <li>C.&nbsp;The Company has set up a special stakeholder section on the corporate website and has dedicated personnel and service hotlines to deal with related issues.</li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>&nbsp;VI.&nbsp;Does the Company appoint a professional stock affairs agency to deal with the affairs of the Shareholders' Meeting?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            It has appointed MasterLink Securities Corporation to handle.
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="u-bg-gray-300" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>VII.&nbsp;Information disclosure</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;Does the Company have a website to disclose the financial operations and corporate governance status?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>A.&nbsp;The Company has established a website and discloses financial, operation, and corporate governance information on the Company website and MOPS.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;Does the Company have other information disclosure channels (e.g., setting up an English website, appointing designated people to handle information collection and disclosure, creating a spokesperson system, and webcasting investor conferences)?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>B.&nbsp;The Company has established a spokesperson system, and the dedicated personnel is responsible for the collection and disclosure of Company information, as well as periodically or timely disclosing relevant information to shareholders at MOPS.</li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;Does the Company announce and file the annual financial report within two months after the end of the fiscal year, and announce and file the financial reports for the first, second, and third quarter and the operating conditions of each month before the specified period?
                                                </li>
                                            </ul>
                                        </td>
                                        <td colspan="1" rowspan="1">
                                            &nbsp;
                                        </td>
                                        <td class="text-center" colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li>C.&nbsp;The Company did not announce and file the annual financial report within two months after the end of the year, but the quarterly financial report and monthly operating conditions were announced and filed within the prescribed period.
                                                </li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr class="u-bg-gray-100">
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>VIII&nbsp;Is there any other important information to facilitate a better understanding of the Company's corporate governance practices (including but not limited to employee rights, employee wellness, investor relations, supplier relations, stakeholder rights, directors' continuing education, implementation status of risk management policies and risk evaluation measures, implementation status of customer policies, and take out a liability insurance policy for directors)?
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center " colspan="1" rowspan="1">
                                            V
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                        </td>
                                        <td class="" colspan="1" rowspan="1">
                                            <ul class="u-list-style--custom-level02">
                                                <li class="u-mb-100">A.&nbsp;Employee rights: We have set up a Welfare Committee and conducted various welfare measures such as children's education scholarships, annual tours, travel allowances, wedding and funeral subsidies, etc., to ensure the rights and interests of employees so that employees can contribute to their jobs with peace of mind.	
                                                </li>
                                                <li class="u-mb-100">B.&nbsp;Employee wellness: The Company has managed various welfare measures such as childbirth and marriage subsidies, annual travel subsidies for excellent employees, and children's scholarships, etc., promoted resource integration, knowledge sharing, and self-learning in the workplace, and took care of employees' education and growth issues in many aspects with love.
                                                </li>
                                                <li class="u-mb-100">C.&nbsp;Investor relations: The Company attaches great importance to the rights and interests of investors. In addition to promptly publishing relevant information on the MOPS website designated by the competent authority under related regulations, it concurrently posts the relevant information on the corporate website. Also, the Company's spokesperson is responsible for responding to questions raised by shareholders.	
                                                </li>
                                                <li class="u-mb-100">D.&nbsp;Supplier relations: The Company has formulated a "Supplier Management Policy," which values long-term relationships with partners and corporate social responsibility and establishes a relationship of mutual trust and mutual benefit with suppliers.	
                                                </li>
                                                <li class="u-mb-100">E.&nbsp;Stakeholders' rights and interests: Stakeholders have no obstacles in communication with the Company and can safeguard their due legitimate rights and interests.	</li>
                                                <li class="u-mb-100">F.&nbsp;Directors' continuing education: The directors of the Company take external courses related to corporate governance topics according to regulations.</li>
                                                <li class="u-mb-100">G.&nbsp;Implementation status of risk management policy and risk evaluation: The Company's major operational policies, investments, bank financing, endorsement guarantees, fund lending, and other major proposals have been evaluated and analyzed by the authority and responsibility department and the Risk Management Committee and implemented in line with the resolutions of the Board of Directors. The Audit Office also draws up an annual audit plan according to the results of the risk assessment.
                                                </li>
                                                <li class="u-mb-100">H.&nbsp;Implementation status of consumer policy: We have set up a 0800 service consultation hotline, and a dedicated person will track and improve the problems raised by customers.		</li>
                                                <li>I.&nbsp;Purchase of liability insurance for directors: The Company began to take out liability insurance for directors in 2018 and announced it at the MOPS.</li>
                                            </ul>
                                        </td>
                                        <td class=" text-center" colspan="1" rowspan="1">
                                            No material deviation
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="" colspan="5" rowspan="1">
                                            <ul class="u-list-style--custom-level01">
                                                <li>XI&nbsp;Please explain the improvements made in response to the Corporate Governance Evaluation results released by the Taiwan Stock Exchange's Corporate Governance Center and provide the priorities and plans for improvement with items yet to be improved.(Leave blank if the Company is not included in the evaluation)
                                                    <ul class="u-list-style--custom-level02 u-mt-100">
                                                        <li class="u-mb-100">A.&nbsp;The Company has fully disclosed the reason for discussion and resolution of the Remuneration Committee in its annual report.</li>
                                                        <li class="u-mb-100">B.&nbsp;In 2020, the Company began to disclose the communication status between independent directors, the internal chief auditor, and CPAs on the corporate website.</li>
                                                        <li class="u-mb-100">C.&nbsp;The Company set up a corporate governance supervisor in 2019 to be responsible for matters related to corporate governance.</li>
                                                        <li>D.&nbsp;The Company has amended the Articles of Incorporation in 2020 and established the Audit Committee upon re-election of directors and supervisors in 2021.</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">[Explication I]</h4>
                        <table class="table table-bordered l-table">
                            <thead>
                                <tr>
                                    <th colspan="2" rowspan="1">
                                        Independence
                                        </td>
                                    <th colspan="2" rowspan="1">
                                        Qualified
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="u-bg-gray-300">
                                    <td colspan="1" rowspan="1" class="text-center white-space-nowrap"
                                        style="width: 57px;">
                                        Item no.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        Description
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center" style="width: 50px;">
                                        Yes
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center" style="width: 50px;">
                                        No
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        1
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The term of appointment of the CPA is less than seven years.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        2
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        Whether the members of the audit service team, other co-practicing CPAs, or shareholders of legal-person CPA firms, CPA firms, firm-affiliated companies, and alliance firms maintain their independence vis-a-vis the Company.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        3
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA or member of the audit service team has not served as a director, managerial officer, or another position that has had a significant impact on the audit case of the Company in the most recent two years.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        4
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA or member of the audit service team has no kinship with the Company's directors, supervisors, managerial officers, or persons with positions that have a significant influence on the audit case.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        5
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA who has been rescinded within one year does not hold any position as a director, supervisor, or managerial officer of the Company or has a significant influence on the audit case.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        6
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA has no direct or indirect significant financial relationship with the Company.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        7
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA firm does not rely excessively on a single customer (the Company) for its source of remuneration.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        8
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        There is no significant close business relationship between the CPA and the Company.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        9
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        There is no potential employment relationship between the CPA and the Company.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        10
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA has no contingent expenses related to the audit case.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        11
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The non-audit service provided by the CPA to the Company does not have a direct impact on the important items of the audit case.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        12
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA has not represented the Company in third-party legal cases or other disputes.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        13
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA does not promote or act as an intermediary for shares or other securities issued by the Company.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        14
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA has not received gifts or special offers of value from the Company, or its directors, supervisors, managerial officers, and major shareholders.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        15
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        The CPA or member of the audit service team does not keep money on behalf of the Company.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered l-table">
                            <thead>
                                <tr>
                                    <th colspan="2" rowspan="1">
                                        Competence
                                        </td>
                                    <th colspan="2" rowspan="1">
                                        Qualified
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="u-bg-gray-300">
                                    <td colspan="1" rowspan="1" class="text-center white-space-nowrap"
                                        style="width: 57px;">
                                        Item no.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        Description
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center" style="width: 50px;">
                                        Yes
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center" style="width: 50px;">
                                        No
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        1
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        Whether they are qualified as CPAs to practice the CPA business.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        2
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        Whether the CPA has not been disciplined by the competent authority or the CPA Associations or sanctioned according to the provisions of Article 37, paragraph 3 of the Securities and Exchange Act.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        3
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        Whether the CPA has knowledge of the Company's related industries.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <td colspan="1" rowspan="1" class="text-center">
                                        4
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        Whether the CPA performs the auditing of financial statements in accordance with Generally Accepted Auditing Standards and Regulations Governing Auditing and Attestation of Financial Statements by Certified Public Accountant.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        V
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" rowspan="1" class="text-center">
                                        5
                                    </td>
                                    <td colspan="1" rowspan="1">
                                        Whether the CPA status is not used as unfair commercial competition.
                                    </td>
                                    <td colspan="1" rowspan="1" class="text-center">V</td>
                                    <td colspan="1" rowspan="1" class="text-center">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer">
            <?php include('components/footer_en.php') ?>
        </footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="../assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="../assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="../assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="../assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="../assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="../assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="../assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="../assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>