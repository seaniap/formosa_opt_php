<div class=" u-bg-white d-none d-xl-block">
  <div class="container">
    <div class="d-flex justify-content-center justify-content-xl-end">
      <div class="d-flex">
        <a href="https://www.eyesmart.com.tw/" class="u-link-gray-800 u-link__hover--blue-highlight d-inline-block u-p-050">
          <img src="../assets/img/index/icons/03.svg" alt="" class="l-header-icon">
          <span>EYESmart</span>
        </a>
        <div class="l-header-seperator">
        </div>
        <a href="#footerAnchor" class="u-link-gray-800 u-link__hover--blue-highlight d-inline-block u-p-050 js-goToAnchor">
          <img src="../assets/img/index/icons/08.svg" alt="" class="l-header-icon">
          <span>APP</span>
        </a>
        <div class="l-header-seperator">
        </div>
        <a href="../index.php" class="u-link-gray-800 u-link__hover--blue-highlight d-inline-block u-p-050">
          <span>繁體中文</span>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="l-header-layout">
    <button id="hamburger" class="l-header-hamburger">
      <div class="l-header-hamburger-bar l-header-hamburger-bar__top"></div>
      <div class="l-header-hamburger-bar l-header-hamburger-bar__middle"></div>
      <div class="l-header-hamburger-bar l-header-hamburger-bar__bottom"></div>
    </button>
    <a href="formosa-investors-financial.php" class="l-header-logo">
      <img src="../assets/img/logo.png" alt="" class="l-header-logo-img">
    </a>
    <button id="sideLinkToggleBtn" type="button" class="c-sideLink-toggleBtn">
      <div class="l-header-hamburger js-menuOpened">
        <div class="l-header-hamburger-bar l-header-hamburger-bar__top"></div>
        <div class="l-header-hamburger-bar l-header-hamburger-bar__middle"></div>
        <div class="l-header-hamburger-bar l-header-hamburger-bar__bottom"></div>
      </div>
    </button>
    <nav id="menu" class="l-header-menu">
      <div class="container-xl u-px-000">
        <ul class="l-header-menu-layout">
          <li class="l-header-menu-item">
            <a href="#" class="l-header-menu-link l-header-menu-link__hover--fill">
              CSR
              <i class="far fa-angle-down u-ml-025"></i>
            </a>
            <!-- [START]下拉Mega menu -->
            <div class="l-header-submenu l-header-submenu-wrapper l-header-submenu--hidden u-px-150 u-px-xl-000">
              <div class="container-fluid">
                <ul class="l-header-submenu-layout row justify-content-center u-mb-xl-200 u-mb-100 u-pt-xl-100">
                  <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                    <a href="formosa-investors-responsibility.php" class="l-header-submenu-link">
                      corporate social responsibility
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                    <a href="formosa-regulation-corporate-fathfull.php" class="l-header-submenu-link">
                      The Company's Performance of<br class="d-none d-xl-inline">
                      Ethical Corporate Management<br class="d-none d-xl-inline">
                      and the Measures Taken
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                    <a href="formosa-investors-relations.php" class="l-header-submenu-link">
                      Stakeholder
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- [END]下拉Mega menu  -->
          </li>
          <li class="l-header-menu-item">
            <a href="#" class="l-header-menu-link l-header-menu-link__hover--fill">
              Investor Relations
              <i class="far fa-angle-down u-ml-025"></i>
            </a>
            <!-- [START]下拉Mega menu -->
            <div class="l-header-submenu l-header-submenu-wrapper l-header-submenu--hidden u-px-150 u-px-xl-000">
              <div class="container-fluid">
                <ul class="l-header-submenu-layout row justify-content-center u-mb-xl-200 u-mb-100 u-pt-xl-100">
                  <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                    <a href="formosa-investors-financial.php" class="l-header-submenu-link">
                      Financial Information
                    </a>
                    <!-- <ul class="">
                      <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                        <a href="formosa-investors-financial.php" class="l-header-submenu-link">Financial statement</a>
                      </li>
                      <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                        <a href="formosa-investors-monthly-revenue-report.php" class="l-header-submenu-link">Monthly Revenue Report</a>
                      </li>
                    </ul> -->
                  </li>
                  <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                    <a href="formosa-investors-shareholders.php" class="l-header-submenu-link">
                      Shareholders Meeting<br class="d-none d-xl-inline">
                      Information
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                    <a href="formosa-investors-dividend-history.php" class="l-header-submenu-link">
                      Dividend History
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                    <a href="formosa-investors-contact.php" class="l-header-submenu-link">
                      Contact Information of<br class="d-none d-xl-inline">
                      Stock Affairs Agency
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                    <a href="formosa-investors-conference.php" class="l-header-submenu-link">
                      Investor Conference
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- [END]下拉Mega menu  -->
          </li>
          <li class="l-header-menu-item">
            <a href="#" class="l-header-menu-link l-header-menu-link__hover--fill">
              Corporate Governance
              <i class="far fa-angle-down u-ml-025"></i>
            </a>
            <!-- [START]下拉Mega menu -->
            <div class="l-header-submenu l-header-submenu-wrapper l-header-submenu--hidden u-px-150 u-px-xl-000">
              <div class="container-fluid">
                <ul class="l-header-submenu-layout row u-mb-xl-200 u-mb-100 u-pt-xl-100 u-px-xl-800">
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-election-of-independent-directors.php" class="l-header-submenu-link">
                      Election of Independent<br>Directors
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-selection-procedure-of-director.php" class="l-header-submenu-link">
                      Selection Procedure of Director
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-articles-of-incorporation.php" class="l-header-submenu-link">
                      Articles of Incorporation
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-financial-regulations.php" class="l-header-submenu-link">
                      Financial regulations
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-organizational-structure-of-the-company.php" class="l-header-submenu-link">
                      Organizational Structure of the Company
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-Internal-Audit-Operation.php" class="l-header-submenu-link">
                      Internal Audit Operation
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-others-regulations.php" class="l-header-submenu-link">
                      Others regulations
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-corporate-governance.php" class="l-header-submenu-link">
                      Corporate Governance<br>Implementation Status
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-board-operation.php" class="l-header-submenu-link">
                      Board Operation
                    </a>
                  </li>
                  <li class="l-header-submenu-item col-xl-2 mx-xl-3 mt-3">
                    <a href="formosa-regulation-functional-committees.php" class="l-header-submenu-link">
                      Functional Committees
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- [END]下拉Mega menu  -->
          </li>
        </ul>
      </div>
      <div class="l-header-bottom container-fluid">
        <div class="l-header-bottom-layout">
          <a href="https://www.eyesmart.com.tw/" class="l-header-bottom-link">
            <img src="../assets/img/index/icons/03.svg" alt="" class="l-header-icon">
            EYESmart
          </a>
          <div class="l-header-seperator"></div>
          <a href="../index.php?Page=B-7#appId" class="l-header-bottom-link">
            <img src="../assets/img/index/icons/08.svg" alt="" class="l-header-icon">
            APP
          </a>
        </div>
        <div class="l-header-bottom-layout">
          <div class="d-flex justify-content-center u-mt-100">
            <a href="../../index.php"
              class="c-btn c-btn--outlined c-btn-gray-800 u-py-050 u-px-200 u-mr-100 rounded-pill">繁體中文</a>
            <a href="formosa-investors-financial.php"
              class="c-btn c-btn--outlined c-btn-gray-800 u-py-050 u-px-200 rounded-pill">English</a>
          </div>
        </div>
      </div>
    </nav>
  </div>
</div>