<?php
session_start();

if (!isset($_SESSION['forgetMember']))
    header("Location:formosa_members-forget-citizen.php")

?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <title id="metaTitle">忘記密碼｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta id="description" name="description" content="如果您忘記密碼或使用者帳號，請按照這些步驟復原寶島眼鏡帳戶，寶島眼鏡會將使用者帳號及新密碼寄送到您原先設定的e-mail信箱或手機。">
    
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />

    <link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />


    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript">
        function popupShow(id){
        	$("#"+id).attr("style", "");
        }
        
        function popupHide(id){
        	$("#"+id).attr("style", "display: none !important");
        }
    
        $(function() {
            var phone = "<?php echo $_SESSION['forgetMember']->m_mobile; ?>";
            var email = "<?php echo $_SESSION['forgetMember']->m_email; ?>";

            $("#btnSms").click(function() {

                $.post("app/controller/Member.php?method=smsSendForget", function(result) {
                    result = JSON.parse(result);

                    if (result.ErrorCode == 0) {
                        $("#tempPhone").html(phone);
                        popupShow("alert_sms");
                    } else {
                        alert("簡訊發送失敗!", "error");
                    }
                })
            })

            $("#btnEmail").click(function() {
                $.post("app/controller/Member.php?method=sendEmailForget", function(result) {
                    result = JSON.parse(result);

                    if (result == 0) {
                    	$("#tempEmail").html(email);
                    	popupShow("alert_email");
                    } else {
                        alert("Email發送失敗!", "error");
                    }
                })
            })

        });
    </script>
<?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
     <div id="alert_sms" class="p-pop_up_wrap d-flex align-items-center justify-content-center" style="display: none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="row p-pop_up justify-content-center">
                        <div class="col-lg-3 col-4 u-mb-125">
                            <img src="assets/img/members/icon25.svg" alt="">
                        </div>
                       <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">密碼已傳至手機</h3>
                                 <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
                          	            您的帳號/密碼補發信件已經透過<span class="u-text-blue-highlight">簡訊方式</span><br>
                          	            傳送至您登記的手機中<br>
      	                         <span class="u-text-red-formosa" id="tempPhone"></span>
                                   </p>
                        <button onclick="location.href='formosa_members-login.php'" class="c-btn c-btn--contained c-btn-blue-highlight col-md-10 col-6">
                            我知道了
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="alert_email" class="p-pop_up_wrap d-flex align-items-center justify-content-center" style="display: none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="row p-pop_up justify-content-center">
                        <div class="col-lg-3 col-4 u-mb-125">
                            <img src="assets/img/members/icon25.svg" alt="">
                        </div>
                       <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">密碼已傳至手機</h3>
                                <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
                                 		  密碼已傳至E-mail
                               </h3>
                               <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
                                 	您的帳號/密碼補發信件已經透過<span class="u-text-blue-highlight">E-mail</span><br>
                                   	傳送至您登記的電子信箱中<br>
                                   <span class="u-text-red-formosa" id="tempEmail"></span>
                               </p>
                        <button onclick="location.href='formosa_members-login.php'" class="c-btn c-btn--contained c-btn-blue-highlight col-md-10 col-6">
                            我知道了
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
      
   
    
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php') ?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class='c-title-center u-mb-125'>忘記帳號/密碼</h3>
                    </div>
                </div>

                <div class="u-pb-100">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-12">
                                <form>
                                    <p class="u-mb-200 text-center u-text-gray-800 u-font-weight-700 u-font-18">請選擇您想要驗證的方式為</p>
                                    <button id="btnEmail" type="button" class="c-btn c-btn--contained c-btn-blue-highlight col u-mb-100">E-mail</button>
                                    <button id="btnSms" type="button" class="c-btn c-btn--contained c-btn-blue-highlight col u-mb-200">行動電話</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
        <!-- =============end footer ============= -->
    </div>
</body>

</html>