<!DOCTYPE html>
<!--[if IE 8]> <html lang="zh-tw" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-tw" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-tw">
<!--<![endif]-->

<head>
<? include('std_header.php')?>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Favicon -->
  <link rel="shortcut icon" href="../../favicon.ico">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="../../assets/plugins/bootstrap4/css/bootstrap.min.css">
  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="../../assets/plugins/fontawesome5/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="../../assets/plugins/icomoon/style.css">
  <link rel="stylesheet" href="../../assets/plugins/slider-pro/css/slider-pro.min.css">
  <!-- CSS Customization -->
  <link rel="stylesheet" href="../../assets/css/main.css">

  <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?> 
  <header id="header"></header>
  <!-- 麵包屑 -->
  <!-- <nav class="breadcrumb-nav" aria-label="breadcrumb">
    <div class="container">
      <ol class="breadcrumb">
        <li>
          <a href="">
            <i class="fas fa-home"></i>
          </a>
        </li>
      </ol>
    </div>
  </nav> -->
  <!-- 麵包屑 -->
  <div class="wrapper">
             <? 	if($row_template['TP1']<>""){include($row_template['TP1']); }  ?> 
    <section class="index-bg">
      <div class="container">
        <div class="d-title">
          <h3>最新消息</h3>
        </div>
        
<?
$query_contentN1 = sprintf("SELECT * FROM %s_BigTable WHERE Page = '2' and Template='news02.php' and Cate02='Y'  AND (now()>=d01 or d01 is NULL) and (now()<=d02 or d02 is NULL ) ORDER BY d01 desc",$Table_ID);
$contentN1 = mysqli_query($MySQL,$query_contentN1 ) or die(mysqli_error($MySQL));
$row_contentN1 = mysqli_fetch_assoc($contentN1);
?>        
        <div class="tfblock">
            <div class="tilted-img">
              <div class="ins">
                <div class="ins-img">
                  <img src="UploadImages/<? echo $row_contentN1['p01'];  ?>" alt="">
                </div>
              </div>
            </div>
            <div class="tilted-text">
              <div class="ins detail">
                <h5><? echo $row_contentN1['h01']; ?></h5>
  
                <p><? echo $row_contentN1['h02']; ?></p>
                <div class="d-flex justify-content-between mt-lg-5 mr-lg-5">
                  <span>
                    <i class="far fa-clock"></i><? echo substr($row_contentN1['d01'],0,10); ?></span>
                  <a href="index.php?Page=2&news02=<? echo $row_contentN1['KeyID'];  ?>" class="btn-bor-yellow">more</a>
                </div>
              </div>
            </div>
          </div>
          
<? $row_contentN1 = mysqli_fetch_assoc($contentN1);  ?>          
          
        <div class="tfblock">
          <div class="tilted-img">
            <div class="ins">
              <div class="ins-img">
                <img src="UploadImages/<? echo $row_contentN1['p01'];  ?>" alt="">
              </div>
            </div>
          </div>
          <div class="tilted-text">
            <div class="ins detail">
              <h5><? echo $row_contentN1['h01']; ?></h5>
              <p><? echo $row_contentN1['h02']; ?></p>
              <div class="d-flex justify-content-between mt-lg-5 mr-lg-5">
                <span>
                  <i class="far fa-clock"></i><? echo substr($row_contentN1['d01'],0,10); ?></span>
                <a href="index.php?Page=2&news02=<? echo $row_contentN1['KeyID'];  ?>" class="btn-bor-yellow">more</a>
              </div>
            </div>
          </div>
        </div>
        <div class="d-flex justify-content-center my-5">
          <a href="../../news.html" class="btn-bor-yellow px-5">+ more</a>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <div class="main-lnk-list">
          <div class="row">
            <div class="col-md-4">
              <a href="../../product-03.html">
                <div class="main-link">
                  <img src="../../assets/img/index/link-01.jpg" alt="">
                  <span>產品型錄</span>
                </div>
              </a>
            </div>
            <div class="col-md-4">
              <a href="https://warranty.upyoung.com.tw/">
                <div class="main-link">
                  <img src="../../assets/img/index/690028920.png" alt="">
                  <span>保固登錄</span>
                </div>
              </a>
            </div>
            <div class="col-md-4">
              <a href="index.php?Page=5">
                <div class="main-link">
                  <img src="../../assets/img/index/836112606.png" alt="">
                  <span>經銷據點</span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <footer id="footer"></footer>
  <!-- /container -->
  <!-- JS Global Compulsory -->
  <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
  <script type="text/javascript" src="../../assets/plugins/jquery/jquery.min.js"></script>
  <script type="text/javascript" src="../../assets/plugins/popper.min.js"></script>
  <script type="text/javascript" src="../../assets/plugins/bootstrap4/js/bootstrap.js"></script>
  <!-- js plugins -->
  <script type="text/javascript" src="../../assets/plugins/back-to-top.js"></script>
  <script type="text/javascript" src="../../assets/plugins/slider-pro/js/jquery.sliderPro.min.js"></script>
  <!-- JS Customization -->
  <script type="text/javascript" src="../../assets/js/main.js"></script>
  <script type="text/javascript">
    $(document).ready(function ($) {
      $('#example4').sliderPro({
        width: 1920,
        height: 804,
        fade: true,
        slideDistance: 0,
        autoplayOnHover: 'none',
        autoScaleReference: 1,
        arrows: true,
        centerImage: true,
        autoScaleLayers: false,
        playVideoAction: 'stopAutoplay',
        pauseVideoAction:'none',
        endVideoAction: 'nextSlide',
        reachVideoAction: 'playVideo',
        breakpoints: {
          992: {
            autoHeight: true
          }
        }
      });
    });
  </script>
  <!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>

</html>