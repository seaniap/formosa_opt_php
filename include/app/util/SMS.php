<?php
namespace  app\util;
use app\util\Config;


/**
 * 傳訊簡訊
 * @author nicole
 *
 */
class SMS {
    public static function send($type, $phone, $text){
        try {
            
            $DA = $phone;
            $SM = $text;
            $SCHEDULETIME='';
            $STOPTIME='';
            $AParty='';
            
            $opt = array('UID' => Config::get($type."_sms_uid"),
                'Pwd' => base64_encode( Config::get($type."_sms_pwd")),
                'DA' => $DA,
                'SM' => $SM,
                'SCHEDULETIME' => $SCHEDULETIME,
                'STOPTIME' => $STOPTIME,
                'AParty' => $AParty
            );
            /*
             $options = array(
             'http' => array(
             'header' => "Content-type: application/x-www-form-urlencoded\r\n",
             'method' => 'POST',
             'content' => http_build_query($opt)
             )
             );
             $context = stream_context_create($options);
             $result = file_get_contents(Config::get("sms_url"), false, $context);
             */
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, Config::get($type."_sms_url"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($opt));
            
            $result = curl_exec($ch);
            
            curl_close($ch);
            
            return json_decode($result);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }
}



