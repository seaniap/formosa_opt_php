<?php
 
namespace app\util;
 

class IdCard {
    
    /**
    * 身份証判斷
    * @param	string	$IdNo	要判斷的身份証字號
    * @return	boolean		true.格式正確 false.格式錯誤
    */
    public static  function   validate_personid($IdNo)
    {
    	$AB = array(10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33);
    
    	$IdNo = strtoupper(trim($IdNo));
    
    	// 位數不對
    	if ( strlen($IdNo) != 10 )
    	{
    		return false;
    	}
    
    	$Pref = ord(substr($IdNo, 0, 1)) - 65;
    	$Num = substr($IdNo, -9);
    
    	// 第一個非字母
    	if ( ($Pref > 25) || ($Pref < 0) )
    	{
    		return false;
    	}
    
    	// 非男非女
    	if ( (strcmp(substr($Num, 0, 1), '1')) && (strcmp(substr($Num, 0, 1), '2')) )
    	{
    		return false;
    	}
    
    	// 非數字
    	if ( !preg_match('/^[0-9]+$/', $Num) )
    	{
    		return false;
    	}
    
    	$IdNo = $AB[$Pref] . $Num;
    	$Sum = substr($IdNo, 0, 1);
    
    	for ( $i = 9 ; $i >= 1 ; $i-- )
    	{
    		$Sum += substr($IdNo, 10 - $i, 1) * $i;
    	}
    
    	$Sum += substr($IdNo, -1);
    	if ( ($Sum % 10) == 0 )
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
    * 居留證判斷
    * @param	string	$IdNo		要判斷的居留證號
    * @return	boolean			true.格式正確 false.格式錯誤
    */
    public static function validate_resident($IdNo)
    {
    	$AB = array(10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33);
    
    	$IdNo = strtoupper(trim($IdNo));
    
    	// 位數不對
    	if ( strlen($IdNo) != 10 )
    	{
    		return false;
    	}
    
    	if ( preg_match('/^([A-Z])(A|B|C|D|8|9)(\d{8})$/', $IdNo, $aMatch) === 1 )
    	{
    		$aWeight = array(1, 9, 8, 7, 6, 5, 4, 3, 2, 1, 1);
    
    		// 舊版本
    		// 第1碼英文字母(區域碼)+第2碼英文字母(性別碼A~D)+第3~9流水號+第10碼檢查碼
    		if ( ($aMatch[2] == 'A') || ($aMatch[2] == 'B') || ($aMatch[2] == 'C') || ($aMatch[2] == 'D') )
    		{
    			$Pref_1 = ord($aMatch[1]) - 65;
    			$Pref_2 = ord($aMatch[2]) - 65;
    			$Num = $aMatch[3];
    			$IdNo = $AB[$Pref_1] . ($AB[$Pref_2] % 10) . $Num;
    
    			$Sum = 0;
    			foreach ($aWeight as $nIndex => $nWeight)
    			{
    				$Sum += substr($IdNo, $nIndex, 1) * $nWeight;
    			}
    
    			if ( ($Sum % 10) == 0 )
    			{
    				return true;
    			}
    			else
    			{
    				return false;
    			}
    		}
    		// 新版本(2021/1 以後)
    		// 第1碼英文字母(區域碼)+第2碼(性別碼8|9)+第3~9流水號+第10碼檢查碼
    		else
    		{
    			$Pref = ord($aMatch[1]) - 65;
    			$Num = $aMatch[2] . $aMatch[3];
    			$IdNo = $AB[$Pref] . $Num;
    
    			$Sum = 0;
    			foreach ($aWeight as $nIndex => $nWeight)
    			{
    				$Sum += substr($IdNo, $nIndex, 1) * $nWeight % 10;
    			}
    
    			if ( ($Sum % 10) == 0 )
    			{
    				return true;
    			}
    			else
    			{
    				return false;
    			}
    		}
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
    * 中國身份証判斷
    * @param	string	$IdNo	要判斷的身份証字號
    * @return	boolean		true.格式正確 false.格式錯誤
    */
    public static function validate_chinese_personid($IdNo)
    {
    	$IdNo = strtoupper(trim($IdNo));
    
    	// 位數不對
    	if ( strlen($IdNo) != 18 )
    	{
    		return false;
    	}
    
    	// 前6碼為行政區
    	// 中間8碼為生日
    	// 3碼順序碼
    	// 最後1碼為檢查碼(0-9 or X)
    	if ( preg_match('/^(\d{2})(\d{2})(\d{2})(\d{4})(\d{2})(\d{2})(\d{3})([0-9X])$/', $IdNo, $aMatch) === 1 )
    	{
    		$aArea = array(11, 12, 13, 14, 15, 21, 22, 23, 31, 32, 33, 34, 35, 36, 37, 41, 42, 43, 44, 45, 46, 50, 51, 52, 53, 54, 61, 62, 63, 64, 65, 71, 81, 82, 83);
    
    		// 省級
    		if ( !in_array($aMatch[1], $aArea) )
    		{
    			return false;
    		}
    
    		// 地級
    		if ( ($aMatch[2] < 1) || ($aMatch[2] > 90) )
    		{
    			return false;
    		}
    
    		// 縣級
    		if ( ($aMatch[3] < 1) || ($aMatch[3] > 99) )
    		{
    			return false;
    		}
    
    		// 生日
    		if ( !@checkdate($aMatch[5], $aMatch[6], $aMatch[4]) )
    		{
    			return false;
    		}
    
    		// 檢查碼
    		$nSum = 0;
    		$sCheck_Sum = '';
    		$aId = str_split($IdNo);
    
    		for ($i = 0; $i <= 16; $i++)
    		{
    			$nSum += (pow(2, 17 - $i) % 11) * $aId[$i];
    		}
    
    		$sCheck_Sum = (12 - ($nSum % 11)) % 11;
    
    		if ( $sCheck_Sum == 10 )
    		{
    			$sCheck_Sum = 'X';
    		}
    
    		if ( $aMatch[8] == $sCheck_Sum )
    		{
    			return true;
    		}
    		else
    		{
    			return false;
    		}
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
    * 香港身份証判斷
    * @param	string	$IdNo	要判斷的身份証字號
    * @return	boolean		true.格式正確 false.格式錯誤
    */
    public static function validate_hongkong_personid($IdNo)
    {
    	$IdNo = strtoupper(trim($IdNo));
    
    	$nLength = strlen($IdNo);
    
    	// 單字母
    	if ( $nLength == 8 )
    	{
    		if ( preg_match('/^([A-Z])(\d{6})([0-9A])$/', $IdNo, $aMatch) === 1 )
    		{
    			// 轉數字
    			$aChar	= str_split($IdNo);
    			$aNum	= array();
    			foreach ($aChar as $key => $value)
    			{
    				$nNum = strpos('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', $value);
    				if ( $nNum === false )
    				{
    					return false;
    				}
    
    				$aNum[$key] = $nNum;
    			}
    
    			// 檢查碼
    			$nSum = 0;
    			$nSum += (36		% 11 * 9) % 11;
    			$nSum += ($aNum[0]	% 11 * 8) % 11;
    			$nSum += ($aNum[1]	% 11 * 7) % 11;
    			$nSum += ($aNum[2]	% 11 * 6) % 11;
    			$nSum += ($aNum[3]	% 11 * 5) % 11;
    			$nSum += ($aNum[4]	% 11 * 4) % 11;
    			$nSum += ($aNum[5]	% 11 * 3) % 11;
    			$nSum += ($aNum[6]	% 11 * 2) % 11;
    			$nSum += ($aNum[7]	% 11 * 1) % 11;
    
    			if ( ($nSum % 11) == 0 )
    			{
    				return true;
    			}
    			else
    			{
    				return false;
    			}
    		}
    		else
    		{
    			return false;
    		}
    	}
    	// 雙字母
    	elseif ( $nLength == 9 )
    	{
    		if ( preg_match('/^([A-Z]{2})9(\d{5})([0-9A])$/', $IdNo, $aMatch) === 1 )
    		{
    			// 轉數字
    			$aChar	= str_split($IdNo);
    			$aNum	= array();
    			foreach ($aChar as $key => $value)
    			{
    				$nNum = strpos('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', $value);
    				if ( $nNum === false )
    				{
    					return false;
    				}
    
    				$aNum[$key] = $nNum;
    			}
    
    			// 檢查碼
    			$nSum = 0;
    			$nSum += ($aNum[0]	% 11 * 9) % 11;
    			$nSum += ($aNum[1]	% 11 * 8) % 11;
    			$nSum += ($aNum[2]	% 11 * 7) % 11;
    			$nSum += ($aNum[3]	% 11 * 6) % 11;
    			$nSum += ($aNum[4]	% 11 * 5) % 11;
    			$nSum += ($aNum[5]	% 11 * 4) % 11;
    			$nSum += ($aNum[6]	% 11 * 3) % 11;
    			$nSum += ($aNum[7]	% 11 * 2) % 11;
    			$nSum += ($aNum[8]	% 11 * 1) % 11;
    
    			if ( ($nSum % 11) == 0 )
    			{
    				return true;
    			}
    			else
    			{
    				return false;
    			}
    		}
    		else
    		{
    			return false;
    		}
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
    * 澳門身份証判斷
    * @param	string	$IdNo	要判斷的身份証字號
    * @return	boolean		true.格式正確 false.格式錯誤
    */
    public static function validate_macao_personid($IdNo)
    {
    	$IdNo = strtoupper(trim($IdNo));
    
    	// 位數不對
    	if ( strlen($IdNo) != 8 )
    	{
    		return false;
    	}
    
    	if ( preg_match('/^([157])(\d{7})$/', $IdNo, $aMatch) === 1 )
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
    * 新加坡身份証判斷
    * @param	string	$IdNo	要判斷的身份証字號
    * @return	boolean		true.格式正確 false.格式錯誤
    */
    public static function validate_singapore_personid($IdNo)
    {
    	$IdNo = strtoupper(trim($IdNo));
    
    	// 位數不對
    	if ( strlen($IdNo) != 9 )
    	{
    		return false;
    	}
    
    	if ( preg_match('/^([STFG])(\d{8})$/', $IdNo, $aMatch) === 1 )
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

}
?>