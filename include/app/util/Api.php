<?php
namespace app\util;
use app\util\Config;

class Api {
    
    public const CITY = [
        "基隆市"=>0,
        "台北市"=>1,
        "臺北市"=>1,
        "新北市"=>2,
        "宜蘭縣"=>3,
        "桃園市"=>4,
        "新竹市"=>5,
        "新竹縣"=>6,
        "苗栗縣"=>7,
        "臺中市"=>8,
        "台中市"=>8,
        "彰化縣"=>9,
        "南投縣"=>10,
        "雲林縣"=>11,
        "嘉義市"=>12,
        "嘉義縣"=>13,
        "台南市"=>14,
        "臺南市"=>14,
        "高雄市"=>15,
        "屏東縣"=>16,
        "花蓮縣"=>17,
        "臺東縣"=>18,
        "台東縣"=>18,
        "澎湖縣"=>19,
        "金門縣"=>20,
    ];
    
  
    
    
    /**
     * 寶島API
     * @param string $func
     * @return mixed
     */
    public static function fmoApi($func, $params =null){
        $json = json_decode(Api::fmoApiStr($func, $params));
         
        if ($func == "getCity") {
             
            $citys = $json->Citys;
     
            foreach ($citys as $city) {
                if (isset(Api::CITY[$city->city_name])) {
                    $city->order = Api::CITY[$city->city_name];
                } else {
                    $city->order = 999; //不存在給最大值
                }
            }
 
            usort($citys, function($city1, $city2) {
                if ($city1->order < $city2->order)
                    return -1;
                    else
                        return 1;
            });
            
            $json->Citys = $citys;
        } 
            
        
        return $json;
    }
    
    
    
    /**
     * 寶島API
     * @param string $func
     * @return mixed
     */
    public static function fmoApiStr($func, $params =null){
        //測試路徑
        $api_url = Config::get("fmo_api_url");
        $appId=  Config::get("fmo_appId");
        $key =  Config::get("fmo_key");
        
        $time = time();
        $secret=md5($appId.$key.$time);
        $url =  $api_url."?api_id=".$appId."&timestamp=$time&func=$func&secret=$secret";
        
        if ($params){
            $params_str ="";
            foreach ($params as $key=>$val) {
                $params_str .="&$key=".urlencode($val);
            }
            $url.=$params_str;
        }
        //echo "fmoAp url:".$url;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        
      
        
        if ($func == "getCity") {
            
            $json = json_decode($result);
            $citys = $json->Citys;
             
            
            foreach ($citys as $city) {
                if (isset(Api::CITY[$city->city_name])) {
                    $city->order = Api::CITY[$city->city_name];
                } else {
                    $city->order = 999; //不存在給最大值
                }
            }
             
            
            usort($citys, function($city1, $city2) {
                if ($city1->order < $city2->order)
                    return -1;
                    else
                        return 1;
            });
            $json->Citys = $citys;
            $result = json_encode($json);
        } 
        //$result = file_get_contents($url);
      
        return $result;
    }
    
    /**
     * 取得商品種類
     * https://api.eyesmart.com.tw/v2/fo_category_list.php
     category_id  分類編號
     category_father_id 上層編號
     category_name 分類名稱
     category_display 分類狀態(1:顯示 0:不顯示)
     category_show_search 顯示在[進階搜尋]狀態(1:顯示 0:不顯示)
     * @return string
     */
    public static function getProductCategory(){
        $url = "https://api.eyesmart.com.tw/v2/fo_category_list.php";
        //$result = file_get_contents($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result);
        return $json;
    }
    
    /**
     * 取得商品清單
     * https://api.eyesmart.com.tw/v2/fo_product_list.php
     product_id  商品群組編號
     product_image  商品圖網址
     product_name  商品名稱
     product_ar  商品 AR 狀態(1:顯示 0:不顯示)
     product_image_label 商品壓圖圖示(0:不顯示 1:熱銷搶購 2:獨家商品 3:HOT推薦 4:新品上市 5:明星款 6:經典款 7:廣告款 8:JAPAN品牌 9:JAPAN同步 10:JAPAN熱賣 11:BEST Buy 12:HOT Sale 13:超狂推薦 14:限時優惠 15:下殺五折 16:點加金 17:線上專屬)
     product_url  商品網址
     category_ids  商品分類
     * @return string
     */
    public static function getProductList(){
        $url = "https://api.eyesmart.com.tw/v2/fo_product_list.php";
        //$result = file_get_contents($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        
        $json = json_decode($result);
        
        return $json;
    }
}