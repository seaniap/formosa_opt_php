<?php
namespace  app\util;
use app\util\Config;
use app\phpmailer\PHPMailer;

//require_once "phpmailer/class.phpmailer.php";

/**
 * 傳訊簡訊
 * @author nicole
 *
 */
class Email {
    public static function send($to, $subject,$text){
        
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = Config::get("email_host");  // Specify main and backup SMTP servers
            $mail->Username = Config::get("email_user_name");                 // SMTP username
            $mail->Password =Config::get("email_password");  
            $mail->Port = Config::get("email_port");  
            $mail->setFrom(Config::get("email_from"), Config::get("email_from_name"));
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->CharSet = "utf-8";
            $mail->Encoding = 'base64';
            
            $mail->addAddress($to);               // Name is optional
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $text;
             
            
            $mail->send();
            return 0; 
        } catch (\Exception $e) {
            //echo 'Message could not be sent.';
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
            return 1;
        }
    }
    
}