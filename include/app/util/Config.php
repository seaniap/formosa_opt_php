<?php
namespace app\util;

class Config {
    private static $app;
    public static function get($name){
        if (!Config::$app) {
            Config::$app = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/app.ini');
        }
        
        return Config::$app[$name];
    
    }
}
