<?php
namespace app\util;

class SSO
{
    /**
     * SSO key
     * @var string
     */
   public static $sEyesmart_Private_Key = 'y3YIbxXhpokzN95kxtTf6nxrm1OpIfFzPbEX7uDv';
    
    /**
     *  EYESmart登入
     */
    public static function login(){
        if (!isset($_GET["sso"]))return;
      
        try {
            $sso = $_GET["sso"];
            
          
            
            $decode_sso=SSO::eyesmart_sso_decrypt (SSO::$sEyesmart_Private_Key, $sso);
           
            $arr = explode("_",$decode_sso);
            
            $date = $arr[0];
            $uid =  $arr[1];
              
            date_default_timezone_set("Asia/Taipei");
            
            $origin = new \DateTime();
           
            //$target = new \DateTime($date);
            $target = new \DateTime();
            $target->setTimestamp($date);
           
             
            $interval = $origin->getTimestamp() - $target->getTimestamp();
           // echo $interval;
            //小於30秒內 登入 自動登入
            if ($interval <= 30) {
                $result = Api::fmoApi("getMember", ["m_id"=>$uid]);
               // var_export($interval);
              //  var_export($result);
                if ($result->result =="1") {
                    session_start();
                    $_SESSION['user'] = $result;
                }
            }
        }catch (\Exception $e){
            
        }
       
    }
    
    public static function getEYESmartUrl(){
        session_start();
        
        $EYESmartUrl ="https://www.eyesmart.com.tw/api/official_login.php";
        /*
        $_SESSION['user'] = new \stdClass();
        $_SESSION['user'] ->m_id="1234";
        */
        if (!isset($_SESSION['user'])) {
            return $EYESmartUrl;
        }
        
        date_default_timezone_set("Asia/Taipei");
        
        //$nowdate = (new \DateTime())->format('YmdHis');
        $nUser_Id = $_SESSION['user'] ->m_id; // 會員編號
        $sSSO_Data = time() . '_' . $nUser_Id;
          
        $sso = urlencode(SSO::eyesmart_sso_encrypt (SSO::$sEyesmart_Private_Key, $sSSO_Data));
        
        return  $EYESmartUrl."?sso=".$sso;
    } 
    
    /**
     * 加密
     * @param unknown $key
     * @param unknown $data
     * @return unknown
     */
    public static function eyesmart_sso_encrypt($key, $data)
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $key, 0, $iv);
        return base64_encode($encrypted . '::' . $iv);
    }
    
    /**
     * 解碼
     * @param unknown $key
     * @param unknown $data
     * @return unknown
     */
    public static function eyesmart_sso_decrypt($key, $data)
    {
        list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
    }
    
  
}
//SSO::getEYESmartUrl();
//SSO::login();
 /*
$nUser_Id = '1234'; // 會員編號
$sSSO_Data = time() . '_' . $nUser_Id;

$sso = SSO::eyesmart_sso_encrypt (SSO::$sEyesmart_Private_Key, $sSSO_Data);
 
echo $sso;
 */