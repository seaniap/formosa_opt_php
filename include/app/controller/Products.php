<?php
namespace  app\controller;
use app\controller\BaseController;
use app\util\DB;

include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";

class Products extends BaseController{
    
    /**依據大類別 找出所有子類別及商品*/
    public function getCategoryProducts(){
        $db = new DB();
        $category_id = $_GET["category_id"] ;
        $brand_id = $_GET["brand_id"] ;
       
        //找出大類別
        $sql = "SELECT * from goweb_product_category where category_father_id in($category_id)
        and category_show_search=1 order by category_sort desc ";
        
        $category = $db->query($sql);
        
        
        //找出子類別
        $categoryItmeId = implode(",",array_map( function($row){
            return $row->category_id;
        },$category));
           
        $sql = "SELECT * from goweb_product_category where category_show_search=1 and category_father_id in($categoryItmeId) order by category_sort desc";
        
        $categoryItem = $db->query($sql);
        
        
        //找出子類別
        $categoryItmeId = implode(",",array_map( function($row){
            return $row->category_id;
        },$categoryItem));
            
        //如果有指定商品
        if ($brand_id) {
            $categoryItmeId = $brand_id;
        }
      
        //找出子類別下的商品
        $sql = "select * from goweb_product where 1=1 and product_id in(
        SELECT product_id from goweb_product_category_ids
        where  product_category_id  in ($categoryItmeId) 
        ) order by field(product_image_label , 0,17,16,15,14,13,12,11,10,9,8,7,6,5,4,2,3,1) desc
        ";
        
        $products = $db->query($sql);
        
        $result = [
            "category"=>$category,
            "categoryItem"=>$categoryItem,
            "products"=>$products
        ];
        
        $json  = json_encode($result,JSON_UNESCAPED_UNICODE);
        
        echo $json;
                
    }
    
    /**
     * 取得眼鏡資料
     */
    public function getProducts(){
        $db = new DB();
        
        $sql = "select * from goweb_product where 1=1 ";
        
        if ( isset($_GET["category_ids"]) && $_GET["category_ids"] !="") {
            $category_ids = explode("|",$_GET["category_ids"]);
            
            //子類別，連集處理
            $innSql = "";
            foreach ($category_ids as $val) {
                $innSql.= "SELECT product_id from goweb_product_category_ids
                where  product_category_id  in ($val)
                GROUP BY product_id
                intersect ";
                
            }
            $innSql = rtrim($innSql , "intersect ");
            
            $sql .=" and product_id in($innSql)";
        } else {
         
            $category_id = $_GET["category_id"] ;
           
            //找出大類別
            $sql2 = "SELECT * from goweb_product_category where category_father_id in($category_id)
            and category_show_search=1 order by category_sort desc";
          
            $category = $db->query($sql2);
          
            //找出子類別
            $categoryItmeId = implode(",",array_map( function($row){
                return $row->category_id;
            },$category));
               
            $sql2 = "SELECT * from goweb_product_category where category_show_search=1 and category_father_id in($categoryItmeId) order by category_sort desc";
            
            $categoryItem = $db->query($sql2);
            
             
          
            //找出子類別
            $categoryItmeId = implode(",",array_map( function($row){
                return $row->category_id;
            },$categoryItem));
           
            $sql .=" and product_id in(
                SELECT product_id from goweb_product_category_ids
                where  product_category_id  in ($categoryItmeId)
            )";
        }
        
        if ($_GET["ordreby"] =="hot") { //熱門商品
            $sql .=" order by field(product_image_label , 0,17,16,15,14,13,12,11,10,9,8,7,6,5,4,2,3,1) desc";
        } else {//AR
            $sql .=" order by field(product_ar , 1 ) desc";
        }
        
        $result = $db->query($sql);
        
        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        
    }
    
    
    
    /**
     * 取得眼鏡類別
     
     public function getCategoryItem(){
     $db = new DB();
     
     $category_father_ids =  implode(",",$_GET["category_father_ids"]) ;
     //$category_father_ids =[207,211, 223,232,239 ,248,267 ];
     $sql = "SELECT * from goweb_product_category where category_father_id in($category_father_ids)
     and category_show_search=1 order by category_father_id";
     
     $result = $db->query($sql);
     
     $json =json_encode($result);
     
     echo $json;
     }
     */
    
}

(new Products())->{$_GET["method"]}();