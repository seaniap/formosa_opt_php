<?php
namespace  app\controller;
use app\controller\BaseController;
use app\util\DB;
use app\util\Api;
use app\util\SMS;
use app\util\Email;
use app\util\IdCard;
use app\util\SSO;

include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";

class Member extends BaseController{
   
    public function getEYESmartUrl(){
        echo SSO::getEYESmartUrl();
    }
    
    public function updatePassword(){
        session_start();
        $member = $_SESSION['user'];
        $new_password = $_GET["new_password"];
        $result = Api::fmoApi("updMember", ["m_id"=>$member->m_id, "m_passwd"=>$new_password, "m_is_change_passwd"=>"N"]);
        
        if ($result->result =="1") {
            $_SESSION['user'] =  Api::fmoApi("getMember", ["m_id"=>$member->m_id]);
        }
        
        echo json_encode($result);
    }
    
    public function getMemberSale(){
        session_start();
        $member = $_SESSION['user'];
        $year = $_GET["year"];
        
        $month = "$year-01";
        $start_date = date("Ymd", strtotime(date("Y-m", strtotime($month))));
        
        $month = "$year-12";
        $end_date = date("Ymd", strtotime(date("Y-m", strtotime($month)) . "+1month-1day"));
        
        $result = Api::fmoApi("getMemberSale", [
            "m_id"=>$member->m_id,
            "s_date_start" =>$start_date,
            "s_date_end" =>$end_date,
            
        ]); 
        //for test date
        /*
       $array =[
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
           ["s_date"=> "202101010","s_name"=> "test", "s_amt"=> "1000","s_points1"=> "10","s_points2"=> "20"],
       ];
       
       $result->Sales = $array;
       */
       echo json_encode($result);
    }
    
    /*
     *社群登入
     */
    public function socialLogin(){
        $type = $_GET["type"];
        $social_id = $_GET["social_id"];
        
        switch ($type) {
            case "google" : $result = Api::fmoApi("getMember", ["m_google_id"=>$social_id]); break; 
            case "fb" : $result = Api::fmoApi("getMember", ["m_fb_id"=> $social_id]);break; 
            case "line" : $result = Api::fmoApi("getMember", ["m_line_id"=>$social_id]);break; 
        }
                
        if ($result->result =="1") {
            session_start();
            $_SESSION['user'] =  $result;
        }
        
        echo json_encode($result);
    }
    
    public function updateRecommendationData(){
        session_start();
        $member = $_SESSION['user'];
        if (isset($_POST["data"])) {
            $member->recommendationData = $_POST["data"];
        }
        $_SESSION['user'] = $member;
    }
    
    /**
     * 社群綁定
     */
    public function socialBind(){
        
        session_start();
        $member = $_SESSION['user'];
        
        $type = $_GET["type"];
        $social_id = $_GET["social_id"];
        $m_id= $member->m_id;
        
        $praam =  [
            "m_id"=>  $m_id,
        ];
        
        switch ($type) {
            case "google" :  $praam["m_google_id"] = $social_id; break; 
            case "fb" :      $praam["m_fb_id"] = $social_id; break; 
            case "line" :    $praam["m_line_id"] = $social_id; break; 
        }
         
        $result = Api::fmoApi("updMember", $praam);
        
        if ($result->result =="1") {
            $_SESSION['user'] =  Api::fmoApi("getMember", ["m_id"=>$m_id]);
        }
        echo json_encode($result);
    }
    
    public function toMemberEdit(){
        session_start();
        
        //user session 已存在重新讀取一次 寶島的資料 確保同步
        $_SESSION['user'] =  Api::fmoApi("getMember", ["m_id"=> $_SESSION['user']->m_id]);
         
          
        $member = $_SESSION['user'];
        
        
        $city = Api::fmoApi("getCity");
        
        $town = null;
        $road = null;
        if ($member->m_addr_city) {
            $town = Api::fmoApi("getTown", ["city_name"=>$member->m_addr_city ] );
            
            if ($member->m_addr_town){
                $road = Api::fmoApi("getRoad", [
                    "city_name"=>$member->m_addr_city ,
                    "town_name"=>$member->m_addr_town
                    
                ]);
            }
        }
        
        echo json_encode([
            "user" =>$member,
            "city" =>$city,
            "town" =>$town,
            "road" =>$road
        ]);
        
        
         //15). m_addr_city: string 地址縣市
        //(16). m_addr_town: string 地址區鄉鎮
        //(17). m_addr_road: string 地址路
        //(18). m_addr: string 地址除了縣市區鄉鎮路名其餘部分
    }
     
    public function chkAccountPasswd(){
        /*
        $praam =  [            
            "m_account"=>$_GET["m_account"],
            "m_passwd"=>$_GET["m_passwd"],
        ];
        
        $result = Api::fmoApi("chkAccountPasswd", $praam);
        
        if ($result->result =="1") {
            session_start();
            $_SESSION['user'] =  Api::fmoApi("getMember", ["m_account"=>$_GET["m_account"]]);
            $result->m_id = $_SESSION['user']->m_id;
            $result->m_is_change_passwd = $_SESSION['user']->m_is_change_passwd;
            //var_export($_SESSION['user']);
        }
        
        echo json_encode($result);
        */
        
        
        
        /*
         * 配合寶島錯誤API流程  因有重複帳號問題 ，修改為    抓取會員資料 直接比對 所抓取的會員資料 進行密碼比對
         */
        $result =  Api::fmoApi("getMember", ["m_account"=>$_GET["m_account"]]);
       
        if ($result->result =="1") {
            if ($result->m_passwd == $_GET["m_passwd"]) {
                session_start();                
                $_SESSION['user'] =  $result;               
                //var_export($_SESSION['user']);
            } else {
                $result->result = "0";                
            }
        }
        
        echo json_encode($result);
       
    }
    
    public function idcardCheck() {
        $id_number =  $_GET["id_number"];
        $idcardType = $_GET["idcardType"];
        
        //身分證非必要 不用檢查
        if (!$id_number){
            echo "true";
            return;
        }
       
        switch ($idcardType) {
            case 0 : echo IdCard::validate_resident($id_number) ? "true":"false"; return ;//居留證號 
            case 1 : echo IdCard::validate_chinese_personid($id_number) ? "true":"false"; return ;//中國身分證驗證
            case 2 : echo IdCard::validate_hongkong_personid($id_number) ? "true":"false"; return ;//香港身分證驗證
            case 3 : echo IdCard::validate_macao_personid($id_number) ? "true":"false"; return ;//澳門身分證驗證
            case 4 : echo IdCard::validate_singapore_personid($id_number) ? "true":"false"; return ;//新加坡身分證驗證
        }
     
        
    }
    
    /**
     * 居留證判斷
     * @param	string	$IdNo		要判斷的居留證號
     * @return	boolean			true.格式正確 false.格式錯誤
     */
    public function validate_resident()
    {
        echo IdCard::validate_resident($_GET["IdNo"]);
        
    }
    
    public function editMember() {
        
        
        $praam =  [
            "m_id"=>  $_GET["m_id"],
            "m_name"=>$_GET["m_name"],
            "m_email"=>$_GET["m_email"],
            "m_edm"=>$_GET["m_edm"],
            "m_addr_city" => $_GET["m_addr_city"],
            "m_addr_town" => $_GET["m_addr_town"],
            "m_addr_road" => $_GET["m_addr_road"],
            "m_addr" => $_GET["m_addr"],
            //"m_idcard" =>$_GET["m_idcard"],
        ];
        
        $result = Api::fmoApi("updMember", $praam);
        
        if ($result->result =="1") {
            session_start();
            $_SESSION['user'] =  Api::fmoApi("getMember", ["m_id"=>$_GET["m_id"]]);
        }
        echo json_encode($result);
    }
    
    public  function  updMember(){
       
        session_start();
        
        $opencardMember = $_SESSION['opencardMember'];
         
        $praam =  [
            "m_id"=>  $opencardMember->m_id,
            "m_mobile"=>$_GET["m_mobile"],
            "m_account"=>$_GET["m_account"],
            "m_passwd"=>$_GET["m_passwd"],
            "m_name"=>$_GET["m_name"],
            "m_idcard"=>$_GET["m_idcard"],
            "m_email"=>$_GET["m_email"],
            "m_birth_year"=>$_GET["m_birth_year"],
            "m_birth_month"=>$_GET["m_birth_month"],
            "m_birth_day"=>$_GET["m_birth_day"],
            "m_edm"=>$_GET["m_edm"],
            "m_store"=>$_GET["m_store"],
            "m_gender"=>$_GET["m_gender"],
            "m_google_id"=>$_GET["m_google_id"],
            "m_line_id"=>$_GET["m_line_id"],
            "m_fb_id"=>$_GET["m_fb_id"],
            "m_is_send"=>$_GET["m_is_send"],
            "m_is_change_passwd"=>$_GET["m_is_change_passwd"],
            "m_invite_code"=>$_GET["m_invite_code"],
            
            "m_addr_city" => $_GET["m_addr_city"],
            "m_addr_town" => $_GET["m_addr_town"],
            "m_addr_road" => $_GET["m_addr_road"],
            "m_addr" => $_GET["m_addr"],
        ];
        
        $result = Api::fmoApi("updMember", $praam);
        
        if ($result->result =="1") {
            $_SESSION['user'] =  Api::fmoApi("getMember", ["m_id"=>$opencardMember->m_id]);
            $result->m_id =  $_SESSION['user']->m_id;
        }
        echo json_encode($result);
    }
    
    
    public function addMember(){
       $praam =  [
           "m_mobile"=>$_GET["m_mobile"],
           "m_account"=>$_GET["m_account"],
           "m_passwd"=>$_GET["m_passwd"],
           "m_name"=>$_GET["m_name"],
           "m_idcard"=>$_GET["m_idcard"],
           "m_email"=>$_GET["m_email"],
           "m_birth_year"=>$_GET["m_birth_year"],
           "m_birth_month"=>$_GET["m_birth_month"],
           "m_birth_day"=>$_GET["m_birth_day"],
           "m_edm"=>$_GET["m_edm"],
           "m_store"=>$_GET["m_store"],
           "m_gender"=>$_GET["m_gender"],
           "m_google_id"=>$_GET["m_google_id"],
           "m_line_id"=>$_GET["m_line_id"],
           "m_fb_id"=>$_GET["m_fb_id"],
           "m_is_send"=>$_GET["m_is_send"],
           "m_is_change_passwd"=>$_GET["m_is_change_passwd"],
           "m_invite_code"=>$_GET["m_invite_code"],
       
           "m_addr_city" => $_GET["m_addr_city"],
           "m_addr_town" => $_GET["m_addr_town"],
           "m_addr_road" => $_GET["m_addr_road"],
           "m_addr" => $_GET["m_addr"],
       ];
      
       $result = Api::fmoApi("putMember", $praam);
       
       //test
       //$result =  Api::fmoApi("getMember", ["m_account"=>$_GET["m_account"]]);
       if ($result->result =="1") {
           session_start();
           $_SESSION['user'] =  Api::fmoApi("getMember", ["m_account"=>$_GET["m_account"]]);
           $result->m_id =  $_SESSION['user']->m_id;
       } 
       echo json_encode($result);
    }
    
    public function getOpenCardMember(){
        $praam = [];
        
        if (isset($_GET["m_mobile"]) && $_GET["m_mobile"]) {
            $praam["m_mobile"] = $_GET["m_mobile"];
        }
        
        
        if (isset($_GET["m_idcard"]) && $_GET["m_idcard"]) {
            $praam["m_idcard"] = $_GET["m_idcard"];
        }
        
        if (isset($_GET["m_birth_year"]) && $_GET["m_birth_year"]) {
            $praam["m_birth_year"] = $_GET["m_birth_year"];
        }
        
        if (isset($_GET["m_birth_month"]) && $_GET["m_birth_month"]) {
            $praam["m_birth_month"] = $_GET["m_birth_month"];
        }
        
        if (isset($_GET["m_birth_day"]) && $_GET["m_birth_day"]) {
            $praam["m_birth_day"] = $_GET["m_birth_day"];
        }
        
        if (isset($_GET["m_account"]) && $_GET["m_account"]) {
            $praam["m_account"] = $_GET["m_account"];
        }
        
        $result = Api::fmoApi("getMember", $praam);
        
        if ($result->result =="1") {
            session_start();
            $_SESSION['opencardMember'] = $result;
        }
        
        echo json_encode($result);
        
    }
    
    public function getForgetMember(){
        $praam = [];
        
        if (isset($_GET["m_mobile"]) && $_GET["m_mobile"]) {
            $praam["m_mobile"] = $_GET["m_mobile"];
        }
        
        
        if (isset($_GET["m_idcard"]) && $_GET["m_idcard"]) {
            $praam["m_idcard"] = $_GET["m_idcard"];
        }
        
        if (isset($_GET["m_birth_year"]) && $_GET["m_birth_year"]) {
            $praam["m_birth_year"] = $_GET["m_birth_year"];
        }
        
        if (isset($_GET["m_birth_month"]) && $_GET["m_birth_month"]) {
            $praam["m_birth_month"] = $_GET["m_birth_month"];
        }
        
        if (isset($_GET["m_birth_day"]) && $_GET["m_birth_day"]) {
            $praam["m_birth_day"] = $_GET["m_birth_day"];
        }
        
        if (isset($_GET["m_account"]) && $_GET["m_account"]) {
            $praam["m_account"] = $_GET["m_account"];
        } 
        
        $result = Api::fmoApi("getMember", $praam);
        
        if ($result->result =="1") {
            session_start();
            $_SESSION['forgetMember'] = $result;
        } 
         
        echo json_encode($result);
    }
    
    public function getMember(){
        
        $praam = [];
        
        if (isset($_GET["m_mobile"]) && $_GET["m_mobile"]) {
            $praam["m_mobile"] = $_GET["m_mobile"];
        } 
        
        
        if (isset($_GET["m_idcard"]) && $_GET["m_idcard"]) {
            $praam["m_idcard"] = $_GET["m_idcard"];
        } 
        
        if (isset($_GET["m_birth_year"]) && $_GET["m_birth_year"]) {
            $praam["m_birth_year"] = $_GET["m_birth_year"];
        } 
        
        if (isset($_GET["m_birth_month"]) && $_GET["m_birth_month"]) {
            $praam["m_birth_month"] = $_GET["m_birth_month"];
        } 
        
        if (isset($_GET["m_birth_day"]) && $_GET["m_birth_day"]) {
            $praam["m_birth_day"] = $_GET["m_birth_day"];
        } 
        
        if (isset($_GET["m_account"]) && $_GET["m_account"]) {
            $praam["m_account"] = $_GET["m_account"];
        } 
            
        $result = Api::fmoApiStr("getMember", $praam);
        
        echo $result;
    }
     
   
    
    private function random_str($length)
    {
        //生成一个包含 大写英文字母, 小写英文字母, 数字 的数组
        $arr = array_merge(range(0, 9), range('a', 'z'));
        $str = '';
        $arr_len = count($arr);
        for ($i = 0; $i < $length; $i++)
        {
            $rand = mt_rand(0, $arr_len-1);
            $str.=$arr[$rand];
        }
        return $str;
    }
    
    public function sendEmailForget(){
         
        session_start();
        
        $member = $_SESSION['forgetMember'];
        
        $email = $member-> m_email;
        //$email = "koko0809@gmail.com";
        $name = $member->m_name;
        $account = $member->m_account;
        
        
        $resetPassword =  $this->random_str(8);
        Api::fmoApi("updMember", ["m_id"=>$member->m_id, "m_passwd"=>$resetPassword, "m_is_change_passwd"=>"Y"]);
        
        $text = " 
                                信件內容：
                                親愛的會員 $name 您好, <br>
                                會員帳號：$account <br>
                                會員密碼：$resetPassword <br>
                                請立即登入進行密碼變更<br>
                                提醒您：此郵件是系統自動發送，請勿直接回覆此郵件<br>
        ";
        
        $result = Email::send($email, "【寶島眼鏡】忘記帳號密碼補發通知", $text);
        
        if ($result ==0) {
            unset ($_SESSION["forgetMember"]);
            //unset ($_SESSION["user"]);
        }
        echo $result;
    }
    
    /**
     * 傳送忘記密碼 簡訊
     */
    public function smsSendForget(){
         
        session_start();
        
        $member = $_SESSION['forgetMember'];
        
        $phone =$member-> m_mobile;
        $account = $member->m_account;
       
        $resetPassword =  $this->random_str(8);
        Api::fmoApi("updMember", ["m_id"=>$member->m_id, "m_passwd"=>$resetPassword, "m_is_change_passwd"=>"Y"]);
        
        $name = $member->m_name;
        
        $msg = $name."您好，寶島眼鏡通知您的會員帳號：".$account." 會員密碼：".$resetPassword." ，請立即登入進行密碼變更";
         
        $result = SMS::send("online6",$phone,$msg);
        
        if ($result->ErrorCode ==0) { 
            unset ($_SESSION["forgetMember"]);
            //unset ($_SESSION["user"]);
        }
        echo json_encode($result);
    }
    
    /**
     * 傳送簡訊
     */
    public function smsSendCode(){
        $phone = $_POST["phone"];
        $code = $_POST["code"];
  
        $user =  Api::fmoApi("getMember", ["m_mobile"=> $phone]);
      
        if ($user->result =="1") {
            echo json_encode(["ErrorCode" =>1]);
            return;
        }
        
        $msg = "寶島眼鏡通知：您的行動電話驗證碼為[$code]，請在30分鐘內輸入驗證碼完成會員開卡作業";
        
        $result = SMS::send("online5",$phone,$msg);
        
        echo json_encode($result);
    }
    
    /**
     * 傳送簡訊
     */
    public function smsSendCodeRegister(){
        $phone = $_POST["phone"];
        $code = $_POST["code"];
        
        $user =  Api::fmoApi("getMember", ["m_mobile"=> $phone]);
       
        if ($user->result =="1") {
            echo json_encode(["ErrorCode" =>1]);
            return;
        }
        $msg = "寶島眼鏡通知：您的行動電話驗證碼為[$code]，請在30分鐘內輸入驗證碼完成會員註冊作業";
        
        $result = SMS::send("online4",$phone,$msg);
        
        echo json_encode($result);
    }
    
    public function getCity(){
       echo Api::fmoApiStr("getCity");
    }
    
    public function getTown(){
        echo Api::fmoApiStr("getTown", ["city_name"=>$_GET["city_name"]] );
    }
    
    public function getRoad(){
        echo Api::fmoApiStr("getRoad", ["city_name"=>$_GET["city_name"] , "town_name"=>$_GET["town_name"]] );
    }
    
    /**
     * 取得眼鏡資料
     */
    public function register(){
        $db = new DB();
        
        $sql = "select * from goweb_product where 1=1 ";
        
        if ( isset($_GET["category_ids"]) && $_GET["category_ids"] !="") {
            $category_ids = explode("|",$_GET["category_ids"]);
             
            //$category_ids  =  implode("," , $_GET["category_ids"]);
            $innSql = "";
            
            $i=0;
            foreach ($category_ids as $val) {
                $innSql.= "SELECT product_id from goweb_product_category_ids
                where  product_category_id  in ($val)
                GROUP BY product_id
                intersect ";
                
            }
            $innSql = rtrim($innSql , "intersect ");
            
            $sql .=" and product_id in($innSql)";
        }
        
        $result = $db->query($sql);
        
        $json =json_encode($result);
        
        echo $json;
    }
    
     
    
}

(new Member())->{$_GET["method"]}();



