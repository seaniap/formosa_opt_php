<?php
namespace  app\controller;
use app\controller\BaseController;
use app\util\DB;
use app\util\Api;
include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";

class Store extends BaseController{
    
    public function getCity(){
        $db = new DB();
        
        $sql = "select s_city from goweb_store GROUP BY s_city";
        
        $result = $db->query($sql);
        
        
        foreach ($result as $city) {
            if (isset(Api::CITY[$city->s_city])) {
                $city->order = Api::CITY[$city->s_city];
            } else {
                $city->order = 999; //不存在給最大值
            }
        }
       
        
        usort($result, function($city1, $city2) {
   
            if ( $city1->order < $city2->order)
                    return -1;
                else
                    return 1;
        });
            
           //var_export($result) ;
           
        $json =json_encode($result);
        
         echo $json;
    }
    
    
    public function getTown(){
        $db = new DB();
        $sql ="select s_town from goweb_store where s_city =?
               GROUP BY s_town order by s_id asc";
        $params = [$_GET["s_city"]];
        
        $result = $db->query($sql, $params);
        
        $json =json_encode($result);
        
        echo $json;
    }
    
    public function getName(){
        $db = new DB();
        $sql ="select s_name from goweb_store
               where s_city =? and s_town =?
               GROUP BY s_name order by s_id asc";
        
        $params = [$_GET["s_city"], $_GET["s_town"]];
        
        $result = $db->query($sql, $params);
        
        $json =json_encode($result);
        
        echo $json;
    }
    
    public function getStore(){
        $db = new DB();
        
        $sql = "select * from goweb_store where 1=1 ";
        $params = [];
        
        
        
        if($_GET["s_city"] !="") {
            $params[]= $_GET["s_city"];
            $sql.=" and s_city=?";
        }
        
        if($_GET["s_town"] !="") {
            $params[]= $_GET["s_town"];
            $sql.=" and s_town=?";
        }
        
        if($_GET["s_name"]!="") {
            $params[]= $_GET["s_name"];
            $sql.=" and s_name=?";
        }
        
        if ($_GET["s_is_wam"]=="Y" && $_GET["s_is_slit"] =="Y") {
            $params[]= "Y";
            $params[]= "Y";
            $sql.=" and s_is_wam=? and s_is_slit=?";
        } else if ($_GET["s_is_wam"]=="Y") {
            // $params[]= "Y";
            // $params[]= "Y";
            $sql.=" and ((s_is_wam='Y' and s_is_slit='Y') or (s_is_wam='Y' and s_is_slit='N' ))";
        } else if ($_GET["s_is_slit"]=="Y") {
            // $params[]= "N";
            //     $params[]= "Y";
            $sql.=" and ((s_is_wam='N' and s_is_slit='Y') or (s_is_wam='Y' and s_is_slit='Y' ) or (s_is_wam='Y' and s_is_slit='N' ))";
        }
        
        
        
        if (!($_GET["s_stars_1"]=="N" && $_GET["s_stars_2"]=="N" && $_GET["s_stars_3"]=="N")) {
            $s_stars ="";
            
            if ($_GET["s_stars_1"]=="Y") $s_stars .="1,";
            if ($_GET["s_stars_2"]=="Y") $s_stars .="2,";
            if ($_GET["s_stars_3"]=="Y") $s_stars .="3,";
            
            $s_stars = rtrim($s_stars,",");
            
            $sql.=" and s_stars in($s_stars)";
            
        }
        
        if ($_GET["is_listen"]=="Y") {
            $params[]= $_GET["is_listen"];
            $sql.=" and is_listen=?";
        }
        
        if ($_GET["is_hearing_aids"]=="Y") {
            $params[]= $_GET["is_hearing_aids"];
            $sql.=" and is_hearing_aids=?";
        }
        
        
        
        $sql.=" order by s_id asc";
        
        
        $result = $db->query($sql, $params);
        
        $json =json_encode($result);
        
        echo $json;
    }
}

(new Store())->{$_GET["method"]}();



