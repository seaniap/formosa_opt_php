<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";

use app\util\DB;
use app\util\Api;

$db = new DB();

//更新商品類別
$count = 0;
$result = Api::fmoApi("getStore3");

 

$sql = "INSERT INTO `goweb_store` 
   (`s_id`, `s_name`, `s_city`, `s_town`, `s_address`, `s_phone`, `s_start_time`, 
    `s_end_time`, `s_line`, `s_is_wam`, `s_is_slit`,`s_stars`,`is_listen`,`is_hearing_aids`)
   VALUES 
   (:s_id, :s_name, :s_city, :s_town, :s_address, :s_phone, :s_start_time, :s_end_time, :s_line, 
    :s_is_wam, :s_is_slit,:s_stars,:is_listen,:is_hearing_aids);";
 
$parms = [];

foreach ($result->stores as $store){
   
    $parms[]= (array) $store;
   
}

try{
    $db->getConn()->beginTransaction();
    $db->update("TRUNCATE TABLE goweb_store");
    $count = $db->updateBatch($sql, $parms);
    $db->getConn()->commit();
} catch (Exception $e) {
    $db->getConn()->rollBack();
    echo $e."<br>";
}

date_default_timezone_set('Asia/Taipei');
$date = new DateTime;
 
echo " Store 更新是否成功:[".($count>0?"是":"否")."] ，更新資料數:[".count($result->stores)."]<br>".
    "更新時間".$date->format('Y-m-d H:i:s');;
 
?>

<script>
var today = new Date(); 
var tomorrow = new Date();
tomorrow.setDate(today.getDate()+1); 
tomorrow.setHours(06); 
tomorrow.setMinutes(0);
tomorrow.setSeconds(0);
 
setTimeout(function(){
	location.reload();
}, tomorrow.getTime() - today.getTime()); 
</script>