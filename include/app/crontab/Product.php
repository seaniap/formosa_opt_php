<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";

use app\util\DB;
use app\util\Api;

$db = new DB();

//更新商品類別
$product_category_count = 0;
$productCategorys = Api::getProductCategory();

$sql = "INSERT INTO goweb_product_category
        (category_id , category_father_id,category_name, category_display,category_show_search,category_sort) VALUES
        (:category_id,:category_father_id,:category_name,:category_display,:category_show_search,:category_sort)";

$parms = [];

foreach ($productCategorys as $productCategory){
    $parms[]= (array) $productCategory;
}

try{
    $db->getConn()->beginTransaction();
    $db->update("TRUNCATE TABLE goweb_product_category");
    $product_category_count = $db->updateBatch($sql, $parms);
    $db->getConn()->commit();
} catch (Exception $e) {
    $db->getConn()->rollBack();
    echo $e."<br>";
}

echo "ProductCategory 更新是否成功:[".($product_category_count>0?"是":"否")."] ，更新資料數:[".count($productCategorys)."]<br>";

//更新商品類別
$products = Api::getProductList();
$product_count = 0;
$product_parms = [];
$product_category_ids_parms = [];


try {
    $db->getConn()->beginTransaction();
    
    $db->update("TRUNCATE TABLE goweb_product_category_ids;");
    $sql_1 ="INSERT INTO  `goweb_product_category_ids` (`product_id`, `product_category_id`) VALUES (:product_id, :product_category_id);";
    
    
    $db->update("TRUNCATE TABLE goweb_product;");
    $sql2 = "INSERT INTO  `goweb_product`
    (`product_id`, `product_image`, `product_name`, `product_ar`, `product_image_label`, `product_url`)
    VALUES (:product_id, :product_image, :product_name, :product_ar, :product_image_label, :product_url);";
    
    $i =1;
    foreach ($products as $product){
        $product_parms[]= [
            "product_id"=>  $product->product_id,
            "product_image"=>  $product->product_image,
            "product_name"=>  $product->product_name,
            "product_ar"=>  $product->product_ar,
            "product_image_label"=>  $product->product_image_label,
            "product_url"=>  $product->product_url,
        ] ;
        
        foreach ($product->category_ids as $category_id ){
            $product_category_ids_parms[]=[
                "product_id"=>  $product->product_id,
                "product_category_id"=>  $category_id,
            ];
        }
        
        if ($i++ % 100 ==0 ) {
            $db->updateBatch($sql_1, $product_category_ids_parms);
            $db->updateBatch($sql2, $product_parms);
            
            $product_count += count($product_parms);
            
            $product_parms =[];
            $product_category_ids_parms =[];
        }
        
    }
    
    if (count($product_category_ids_parms) > 0) {
        $db->updateBatch($sql_1, $product_category_ids_parms);
    }
    
    if (count($product_parms) > 0) {
        $product_count += count($product_parms);
        $db->updateBatch($sql2, $product_parms);
    }
    
    
    $db->getConn()->commit();
} catch (Exception $e) {
    $db->getConn()->rollBack();
    echo $e."<br>";
}

date_default_timezone_set('Asia/Taipei');
$date = new DateTime;

echo "Product 更新是否成功:[".($product_count>0?"是":"否")."] ， 更新資料數:[".$product_count."]<br>".
    "更新時間".$date->format('Y-m-d H:i:s');
?>


<script>
var today = new Date(); 
var tomorrow = new Date();
tomorrow.setDate(today.getDate()+1); 
tomorrow.setHours(06); 
tomorrow.setMinutes(0);
tomorrow.setSeconds(0);
 
setTimeout(function(){
	location.reload();
}, tomorrow.getTime() - today.getTime()); 
</script>