<?php require_once('../Connections/MySQL.php'); ?>
<?php $template = "featuredcideos02.php";
$templateS = "featuredcideos02";
// 判斷是預設是A版還是S版
// 預設 A 版
?>
<?php
if ($_GET[$templateS] == "" and $row_template['Extend'] == "Y") {
    //A 版開始
    if ($_GET['demo'] == 'y') { ?>
        <!-- ============= 主要內容區 ============= -->
        <section id="video" class="u-bg-white u-py-200">
            <div class="container">
                <h3 class="c-title-center u-mb-125">精選影片 --A 版Demo</h3>
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-6">
                        <h4 class="text-center u-text-gray-800 u-mb-100 u-mt-100 u-mt-lg-000">精選廣告款</h4>
                        <div class="v-slick c-card-verA p-index-video-slick">
                            <!-- Slides -->
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">
                                        一二三四五六七八九十一二三四五六七八九十一二三四五</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_03.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_04.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_02.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =============end 主要內容區 ============= -->
    <?php } else {  ?>
        <?php $currentPage = $_SERVER["PHP_SELF"];

        $maxRows_content01 = $row_template['MaxRow'];
        $pageNum_content01 = 0;
        if (isset($_GET['pageNum_content01'])) {
            $pageNum_content01 = $_GET['pageNum_content01'];
        }
        $startRow_content01 = $pageNum_content01 * $maxRows_content01;

        $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            //$colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
            $colname_content01 = $_GET['Page'];
        }

        $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        if ($_GET['Cate01'] <> "") {
            $query_content01 = $query_content01 . "AND Cate01='" . $_GET['Cate01'] . "'";
        }

        if ($_GET['h02'] <> "") {
            $query_content01 = $query_content01 . "AND h02='" . $_GET['h02'] . "'";
        }

        //排序 by Sq
        $query_content01 = $query_content01 . " order by Sq desc ";
        //echo $query_content01;
         

        $query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
        //echo $query_limit_content01 ;
        $content01 = mysqli_query($MySQL, $query_limit_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);

        $all_content01 = mysqli_query($MySQL, $query_content01);
        $totalRows_content01 = mysqli_num_rows($all_content01);

        //echo $totalRows_content01;

        $totalPages_content01 = ceil($totalRows_content01 / $maxRows_content01) - 1;

        $queryString_content01 = "";
        if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
                if (
                    stristr($param, "pageNum_content01") == false &&
                    stristr($param, "totalRows_content01") == false &&
                    stristr($param, "Cate01") == false &&
                    stristr($param, "Cate02") == false &&
                    stristr($param, "Cate03") == false
                ) {
                    array_push($newParams, $param);
                }
            }
            if (count($newParams) != 0) {
                $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
            }
        }
        $queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
        $queryString_content01 = $queryString_content01 . "&Cate01=" . $_GET['Cate01'] . "&Cate02=" . $_GET['Cate02'] . "&Cate03=" . $_GET['Cate03'];
        ?>
        <section id="video" class="u-bg-white u-py-200">
            <div class="container">
            <div class="col d-flex justify-content-between u-mb-100 u-mt-100 u-mt-lg-000">
                <h4 class="text-center u-text-gray-800">精選商品<?php  echo $_GET['Cate01']; ?> --精選廣告款</h4>
                <a class="btn btn-outline-secondary" href="index.php?Page=0-2">回目錄</a>
                </div>            
                <div class="row justify-content-between align-items-center">
                    <div class="row">       
                        <!--<div class="col-lg-6"> <div class="v-slick c-card-verA p-index-video-slick"> -->
                            <!-- Slide items -->
                            <?php do {  ?>
                               <?php if($totalRows_content01 <= 3) { ?>
                                <div class="col">
                                <?php } else { ?>
                                <div class="col col-lg-3">
                                <?php } ?>
                                <!--<div>  3 button -->
                               	
                                <?php 
                                if($totalRows_content01==0){  //如果一筆資料都沒有還是可以新增
                                    $row_content01['Page']=$_GET['Page'];
                                    $row_content01['Sq']=1000;
                                    // echo 'if';
                                    SuperM_ins($row_content01,$template,$row_template['Security']);
                                    ShowSqM( $row_content01,$template,$row_template['Security']);
                                    //  index.php?Page=4&h02=迪士尼粉萌派對系列 搜尋結果是空的時候 也要顯示
                                    }else if($totalRows_content01==8){
                                    
                                    SuperM_edit( $row_content01,$template,$row_template['Security']);
                                    SuperM_del($row_content01, $template,$row_template['Security']);
                                    ShowSqM( $row_content01,$template,$row_template['Security']);
                                
                                        
                                    }else {
                                    // echo 'else';	
                                    SuperM_Button_3( $row_content01,$template,$row_template['Security']);
                                    ShowSqM( $row_content01,$template,$row_template['Security']);
                                    } 
                                ?> 									
									
									
									
                                    <a href="<?php echo $row_content01['l01'];  ?>" class="v-slick-slide p-2">
                                        <div class="v-slick-slide-frame c-card-head">
                                            <img src="UploadImages/<?php echo $row_content01['p01'];  ?>" title="<?php echo $row_content01['h01']; ?>" alt="<?php echo $row_content01['h01'];  ?>" class="img-fluid">
                                        </div>
                                        <div class="c-card-body-layout">
                                            <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100"><?php echo $row_content01['h01']; ?></p>

                                        </div>
                                    </a>
                                </div>
                            <?php } while ($row_content01 = mysqli_fetch_assoc($content01)); ?>
                            <!--end  Slide items -->
                            
                        <!-- </div> -->
                    </div>
                </div>
                <div class="col d-flex justify-content-end">
                <a class="btn btn-outline-secondary" href="index.php?Page=0-2">回目錄</a>
                </div>
                
            </div>
        </section>




    <?php mysqli_free_result($content01);
    }

    //A 版結束 
} else {
    //S版開始

    if ($_GET['demo'] == 'y') { ?>
        <section id="video" class="u-bg-white u-py-200">
            <div class="container">
                <h3 class="c-title-center u-mb-125">精選影片 --S版DEMO</h3>
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8 u-mb-100 u-mb-lg-000 u-px-200">
                        <div class="embed-responsive embed-responsive-16by9 u-mb-125">
                            <iframe data-src="https://www.youtube.com/embed/O7pNpR3Py68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" class="embed-responsive-item lozad" allowfullscreen></iframe>
                        </div>
                        <div class="p-index-video-thumbnails">
                            <div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame js-active">
                                <img data-src="assets/img/index/video/03.jpg" alt="" class="img-fluid lozad">
                            </div>
                            <div data-url="https://www.youtube.com/embed/U-8MgOi68ig" class="p-index-video-thumbnails-frame">
                                <img data-src="assets/img/index/video/02.jpg" alt="" class="img-fluid lozad">
                            </div>
                            <div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame">
                                <img data-src="assets/img/index/video/03.jpg" alt="" class="img-fluid lozad">
                            </div>
                            <div data-url="https://www.youtube.com/embed/U-8MgOi68ig" class="p-index-video-thumbnails-frame">
                                <img data-src="assets/img/index/video/04.jpg" alt="" class="img-fluid lozad">
                            </div>
                            <div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame">
                                <img data-src="assets/img/index/video/05.jpg" alt="" class="img-fluid lozad">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <h4 class="text-center u-text-gray-800 u-mb-100 u-mt-100 u-mt-lg-000">精選廣告款</h4>
                        <div class="v-slick c-card-verA p-index-video-slick">
                            <!-- Slides -->
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">一二三四五六七八九十一二三四五六七八九十一二三四五</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_03.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_04.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_02.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } else {  ?>
        <?php $colname_content01 = "-1";
        // if (isset($_GET['Page'])) {
        //     $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        // }

        $maxRows_content01 = $row_template['MaxRow'];
        $KeyID = str_replace($vowels, "**!!!**", substr($_GET[$templateS], 0, 50));
        if ($row_template['Extend'] == 'Y') {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        } else {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        }

        $content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);
        $totalRows_content01 = mysqli_num_rows($content01);
        //echo "S版啟動".$query_content01;
        ?>
        <section id="video" class="u-bg-white u-py-200">
            <div class="container">
                <h3 class="c-title-center u-mb-125">精選影片 --S版</h3>
                <div class="row justify-content-between align-items-center">
                    <div class="col-lg-8 u-mb-100 u-mb-lg-000 u-px-200">
                        <div class="embed-responsive embed-responsive-16by9 u-mb-125">
                            <iframe data-src="https://www.youtube.com/embed/O7pNpR3Py68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" class="embed-responsive-item lozad" allowfullscreen></iframe>
                        </div>
                        <div class="p-index-video-thumbnails">
                            <div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame js-active">
                                <img data-src="assets/img/index/video/03.jpg" alt="" class="img-fluid lozad">
                            </div>
                            <div data-url="https://www.youtube.com/embed/U-8MgOi68ig" class="p-index-video-thumbnails-frame">
                                <img data-src="assets/img/index/video/02.jpg" alt="" class="img-fluid lozad">
                            </div>
                            <div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame">
                                <img data-src="assets/img/index/video/03.jpg" alt="" class="img-fluid lozad">
                            </div>
                            <div data-url="https://www.youtube.com/embed/U-8MgOi68ig" class="p-index-video-thumbnails-frame">
                                <img data-src="assets/img/index/video/04.jpg" alt="" class="img-fluid lozad">
                            </div>
                            <div data-url="https://www.youtube.com/embed/QVV7fMugBuQ" class="p-index-video-thumbnails-frame">
                                <img data-src="assets/img/index/video/05.jpg" alt="" class="img-fluid lozad">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <h4 class="text-center u-text-gray-800 u-mb-100 u-mt-100 u-mt-lg-000">精選廣告款</h4>
                        <div class="v-slick c-card-verA p-index-video-slick">
                            <!-- Slides -->
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">一二三四五六七八九十一二三四五六七八九十一二三四五</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_03.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_04.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                            <a href="" class="v-slick-slide p-2">
                                <div class="v-slick-slide-frame c-card-head">
                                    <img src="assets/img/index/product_02.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="c-card-body-layout">
                                    <p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">SOU・SOU l 微笑日常 多邊框眼鏡</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>





<?php mysqli_free_result($content01);
    }
}  //S版結束
?>