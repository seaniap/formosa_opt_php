<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <h3 class="c-title-center u-mb-125">公司治理</h3>
        </div>
        <!-- regulation tabs -->
        <?php include('formosa_regulation_tabs.php')?>
        <!-- end regulation tabs -->
        <div class="u-pb-100">
            <div class="container">
                <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">獨立董事選任資訊
                </p>
                <p class="u-mb-000 u-font-14 u-text-red-formosa-02 u-font-weight-900">本公司依照證交法所訂資格條件選任獨立董事之相關訊息
                </p>
            </div>
        </div>
        <div class="u-pb-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">一、提名暨選任方式</h4>
                <p class="u-mb-000">
                    證交法第14條之2第1項：「已依本法發行股票之公司，得依章程規定設置獨立董事。但主管機關應視公司規模、股東結構、業務性質及其他必要情況，要求其設置獨立董事，人數不得少於二人，且不得少於董事席次五分之一。」據此，本公司於95年股東常會通過公司章程修正案，董事選舉採候選人提名制、增訂第18條之1依證交法規定設置獨立董事。
                </p>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">二、提名過程</h4>
                <p>本公司依公司法第172條之1、第192條之1及第216條之1規定，於110年4月6日在公開資訊觀測站公告「110年股東常會受理股東提名董監事候選人名單」。持有本公司已發行股數1%以上之股東，得於110年4月16日至26日提名期間內，以書面方式向本公司提出董事(含獨立董事)候選人名單。
                </p>
                <p class="u-mb-000">
                    公告期間屆滿，僅董事會提名文鍾奇、蔡育菁、梁榮輝與吳孟柔為本公司獨立董事候選人，並於110年5月10日董事會通過文鍾奇、蔡育菁、梁榮輝與吳孟柔四人資格審查。
                </p>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">三、候選人資料</h4>
                <div class="row">
                            <div class="col-lg-10">
                                <table class="table table-bordered l-table u-mb-000">
                                    <thead>
                                        <tr>
                                            <th class="text-center">項次</th>
                                            <th class="text-center" style="min-width: 80px;">姓名</th>
                                            <th class="text-center">學歷</th>
                                            <th class="text-center">經歷</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th class="text-center l-table-vertical-align-middle">1</th>
                                            <td class="text-center l-table-vertical-align-middle">文鍾奇</td>
                                            <td class="text-center l-table-vertical-align-middle">文化大學法律學研究所法學碩士</td>
                                            <td class="text-center l-table-vertical-align-middle">文鍾奇律師事務所律師</td>
                                        </tr>
                                        <tr class="u-bg-gray-100">
                                            <th class="text-center l-table-vertical-align-middle">2</th>
                                            <td class="text-center l-table-vertical-align-middle">蔡育菁</td>
                                            <td class="text-center l-table-vertical-align-middle">台灣大學會計學研究所</td>
                                            <td class="text-center l-table-vertical-align-middle">永華聯合會計師事務所合夥會計師</td>
                                        </tr>
                                        <tr>
                                            <th class="text-center l-table-vertical-align-middle">3</th>
                                            <td class="text-center l-table-vertical-align-middle">梁榮輝</td>
                                            <td class="text-center l-table-vertical-align-middle">國立台灣科技大學企管所財金博士</td>
                                            <td class="text-center l-table-vertical-align-middle">中國文化大學財金系兼任教授</td>
                                        </tr>
                                        <tr class="u-bg-gray-100">
                                            <th class="text-center l-table-vertical-align-middle">4</th>
                                            <td class="text-center l-table-vertical-align-middle">吳孟柔</td>
                                            <td class="text-center l-table-vertical-align-middle">密西根州立大學管理學院經濟碩士</td>
                                            <td class="text-center l-table-vertical-align-middle">法赫法律事務所律師</td>
                                        </tr>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
            </div>
        </div>
        <div class="u-py-100 u-pb-300">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">四、選任過程與結果</h4>
                <p>本公司於110年7月27日股東常會改選董事時，選任董事7席，其中獨立董事4席，4席獨立董事得票權數如下：</p>
                <div class="row">
                            <div class="col-lg-10">
                                <table class="table table-bordered l-table u-mb-000">
                                    <thead>
                                        <tr>
                                            <th class="text-center">項次</th>
                                            <th class="text-center">
                                                被候選人姓名</th>
                                            <th class="text-center">
                                                當選權數</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th class="text-center l-table-vertical-align-middle">1</th>
                                            <td class="text-center l-table-vertical-align-middle">文鍾奇</td>
                                            <td class="text-center l-table-vertical-align-middle">38,582,198</td>
                                        </tr>
                                        <tr class="u-bg-gray-100">
                                            <th class="text-center l-table-vertical-align-middle">2</th>
                                            <td class="text-center l-table-vertical-align-middle">蔡育菁</td>
                                            <td class="text-center l-table-vertical-align-middle">38,436,768</td>
                                        </tr>
                                        <tr>
                                            <th class="text-center l-table-vertical-align-middle">3</th>
                                            <td class="text-center l-table-vertical-align-middle">梁榮輝</td>
                                            <td class="text-center l-table-vertical-align-middle">38,397,366</td>
                                        </tr>
                                        <tr class="u-bg-gray-100">
                                            <th class="text-center l-table-vertical-align-middle">4</th>
                                            <td class="text-center l-table-vertical-align-middle">吳孟柔</td>
                                            <td class="text-center l-table-vertical-align-middle">38,573,058</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
            </div>
        </div>
    </section>
</main>