        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="container u-py-200">
                    <h3 class='c-title-center u-mb-225'>歷史沿革</h3>
                    <div class="position-relative">
                        <!-- <button type="button" class="v-tabBtnSlick-prev"><i class="fal fa-angle-left"></i></button> -->
                        <div class="c-tab-verE v-tabBtnSlick">
                            <div class="c-tab-container">
                                <a href="" class="c-tab-linkE">2020-2017</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="" class="c-tab-linkE">2016-2012</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="" class="c-tab-linkE">2011-2007</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="" class="c-tab-linkE">2006-2002</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="" class="c-tab-linkE">2001-1976</a>
                            </div>
                            <div class="c-tab-container">
                                <a href="" class="c-tab-linkE">1975以前</a>
                            </div>
                        </div>
                        <!-- <button type="button" class="v-tabBtnSlick-next"><i class="fal fa-angle-right"></i></button> -->
                    </div>
                </div>
                <div class="u-pb-300">
                    <div class="u-bg-gray-100 u-py-200 p-history-triangle-down-gray">
                        <div class="container">
                            <h4 class="c-title-underline u-font-18 u-md-font-24">西元2020年</h4>
                            <div class="row">
                                <div class="col-md-5 col-lg-3 u-pb-100 u-pb-md-000">
                                    <img src="assets/img/history/history_01.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="col-md-7 col-lg-9">
                                    <ul class="u-list-style--disc u-text-gray-700 u-mb-000">
                                        <li>第一季推出與日本潮牌「SOU‧SOU」聯名款，除了眼鏡的完整配件，還有限量眼鏡隨身包，廣告更是首次採用故事行銷的手法，除了引起
                                            「SOU‧SOU」粉絲的關注，更造成消費市場廣大的迴響。</li>
                                        <li>1月中旬配合年節氣氛，推出「Disney米奇鼠於你」系列商品。</li>
                                        <li>第二季活動商品「NEOS」i鏡片(舒壓、漸進系列)，除了請到不敗女神徐若瑄擔任代言人，廣告更是採用一鏡到底的拍攝手法，令人耳目一新。</li>
                                        <li>第三季主打「K-Design」的K-REATE系列，除了鏡框的特殊烤漆色及1框+2套圈=3造型的訴求，並同步推出獨家銷售的「KANDY」果凍變色片。
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="u-py-200 p-history-triangle-down-white">
                        <div class="container">
                            <h4 class="c-title-underline u-font-18 u-md-font-24">西元2019年</h4>
                            <div class="row">
                                <div class="col-md-5 col-lg-3 u-pb-100 u-pb-md-000">
                                    <img src="assets/img/history/history_02.jpg" alt="" class="img-fluid">
                                </div>
                                <div class="col-md-7 col-lg-9">
                                    <ul class="u-list-style--disc u-text-gray-700 u-mb-000">
                                        <li>第一季『K-Design』好評再升級「K-PLUS」機能/超機能系列，主打「超輕 超彈」除整付配到好只要1980元第二付75折外，再送萌萌貓眼鏡包。
                                        </li>
                                        <li>2月同樣推出『K-Design』x Line Friends系列，整付配到好1980元起，全台獨家販售。</li>
                                        <li>第二季再度請「謝祖武」代言同時與PENTAX合作推出日本專業光學 漸進多焦鏡片，以「豈止好看」為訴求，打造更舒適便利的戴鏡享受。</li>
                                        <li>第三季與「迪士尼 玩具總動員」合作，推出全台獨佔玩具總動員鏡框整付配到好只要1980元起，讓大朋友小朋友一同懷念那些年與兒時夥伴的共同時光。</li>
                                        <li>『寶島有eye 視界美好』讓弱勢的青銀族群能同享這份愛，連續第九年捐贈2000付眼鏡給衛生福利部，這次擴大長者同受惠，一同守護國人的雙眼。</li>
                                        <li><a href="#">寶島眼鏡力拼數位轉型，串聯眼科、醫美診所　極智配鏡體驗 一站滿足全眼服務</a></li>
                                        <li><a href="#">日系快時尚竄起！40年老牌寶島眼鏡砸千萬轉型，要靠什麼贏？</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="u-bg-gray-100 u-py-200 p-history-triangle-down-gray">
                        <div class="container">
                            <h4 class="c-title-underline u-font-18 u-md-font-24">西元2018年</h4>
                            <ul class="u-list-style--disc u-text-gray-700 u-mb-000">
                                <li>第一季好評再推出韓風『K-Design』系列潮框，主打韓國流行即時連線，除整付配到好只要1980元第二付再享75折優惠活動外，下載寶島眼鏡APP鏡片可免升級濾藍光變色片，代言人沿用「鬼鬼」吳映潔。
                                </li>
                                <li>2月推出『K-Design』x Line Friends系列，全台獨家販售。</li>
                                <li>第二季主打日本Nikon光學技術導入的『NEOS』漸進多焦鏡片，以「看遠看近
                                    一付搞定」做為廣告主訴求，活動優惠價2980元，不滿意保證退費，邀請影劇凍齡男星「謝祖武」擔綱代言。</li>
                                <li>第三季推出『K-Design』品牌舒適大升級的「K-PLUS」機能/超機能系列，主打極輕、超彈機能，整付配到好只要1980元，第二付再享75折，下載寶島眼鏡APP再享現折500元優惠三選一，代言人沿用「鬼鬼」吳映潔。
                                </li>
                                <li>第四季再與全球頂尖品牌共同推出漸進式多焦鏡片，主打看遠看近一付可搞定，只需2980配到好，不滿意保證退費，同樣邀請「謝祖武」代言。</li>
                            </ul>
                        </div>
                    </div>
                    <div class="u-py-200 p-history-triangle-down-white">
                        <div class="container">
                            <h4 class="c-title-underline u-font-18 u-md-font-24">西元2017年</h4>
                            <ul class="u-list-style--disc u-text-gray-700 u-mb-000">
                                <li>1月總公司進駐汐止遠雄U-TOWN辦公大樓，同年3月舉辦總公司喬遷慶祝酒會。</li>
                                <li>第一季推出韓國設計的『K-Design』自有品牌，並以優惠價整付配到好只要1980元起第二付75折為訴求，代言人「鬼鬼」。</li>
                                <li>第二季以日本知名品牌『Nikon』漸進多焦鏡片，不滿意保證退費及限折3000元起為訴求，廣告沿用去年「黃子佼」為代言人。</li>
                                <li>【RICHBABY面膜保養系列】
                                    寶島眼鏡複合式經營觸角更為廣泛，從保健食品到保養品，讓品牌經營系列化，106年8月首推RICHBABY面膜基礎保養系列，讓更多愛美的女性，可以有更多的選擇！</li>
                                <li>第三季和「漫威」合作，推出全台獨賣「復仇者聯盟」系列，整付配到好只要1980元起，第二付只要1480元。</li>
                                <li>9月推出全台獨家『K-Design』爽爽貓聯名系列，除整付配到好只要1980元外，滿額再贈送獨家限量爽爽貓托特袋及2018爽爽貓桌曆。</li>
                                <li>第四季推出六大功能一次滿足的「PENTAX」 多焦鏡片只要2980元，好評再推出愛視爾i鏡片
                                    漸進多焦及舒適系列鏡片只要2980元，不滿意保證退費，廣告仍由「黃子佼」代言。</li>
                                <li>『寶島有eye 視界美好』關懷清寒學子，連續第七年捐贈2000付眼鏡給衛生福利部，協助清寒學子免費配鏡。</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->