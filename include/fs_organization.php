        <!-- ============= 主要內容區 ============= -->
        <main>
            <section>
                <div class="container u-py-200">
                    <h3 class='c-title-center u-mb-225'>公司組織</h3>
                    <div class="u-mb-425">
                        <div class="row u-mb-200">
                            <div class="col-12 d-none d-md-block">
                                <img class="w-100" src="./assets/img/organization/organization-PC.svg" alt="">
                            </div>
                            <div class="col-12 d-md-none">
                                <img class="w-100" src="./assets/img/organization/organization-mobile.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->