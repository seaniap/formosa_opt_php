<!-- header -->

<script type="text/javascript">
$(function() {
    $("#link_member").click(function() {
        qg('event', "N_top_clicked", {
            "clicled_name": "會員專區"
        });
    });

    $("#link_member_phone").click(function() {
        qg('event', "N_top_clicked", {
            "clicled_name": "會員專區"
        });
    });


    $("#link_store_query").click(function() {
        qg('event', "N_top_clicked", {
            "clicled_name": "門市查詢"
        });
    });

    $("#link_store_query_phone").click(function() {
        qg('event', "N_top_clicked", {
            "clicled_name": "門市查詢"
        });
    });

    $("#link_tag_id2").click(function() {
        qg('event', "N_top_clicked", {
            "clicled_name": "行動配送"
        });
    });

    $("#link_tag_id4").click(function() {
        qg('event', "N_top_clicked", {
            "clicled_name": "行動配送"
        });
    });

    $("#link_app").click(function() {
        qg('event', "N_top_clicked", {
            "clicled_name": "APP下載"
        });
    });

    $("#link_app_phone").click(function() {
        qg('event', "N_top_clicked", {
            "clicled_name": "APP下載"
        });
    });

    $("[name='link_about']").click(function() {
        var title = this.innerHTML.replace(/ /g, "");
        var url = this.href;
        qg('event', "N_category_viewed", {
            "category_name": title,
            "category_link": url
        });

    });
})
</script>

<div class=" u-bg-white d-none d-xl-block">
    <div class="container">
        <div class="d-flex justify-content-center justify-content-xl-between">
            <div class="d-flex align-items-center">
                <?php 
                     
                     if (isset($_SESSION['user'])) {
                        $m_name =  $_SESSION['user']->m_name;
                        echo  "<a href='index.php?Page=B-5-1'
                        class='u-link-gray-800 u-link__hover--blue-highlight d-inline-block'>
                        <img src='assets/img/index/icons/07.svg' alt='' class='l-header-icon'>
                      <span>Hi! $m_name</span></a>
                              <a href='logout.php' class='c-btn c-btn--contained c-btn-blue-highlight u-px-050 u-ml-050' style='height:30px;'>登出</a>";
                     } else {
                        echo "<a href='index.php?Page=B'
                        class='u-link-gray-800 u-link__hover--blue-highlight d-inline-block'>
                        <img src='assets/img/index/icons/07.svg' alt='' class='l-header-icon'>
                      <span>會員登入</span></a>";
                     }
                 ?>

                <?php  //echo "session:";print_r($_SESSION);  ?>
                <div class="l-header-seperator">
                </div>
                <a id="link_member" href="index.php?Page=B-5-1"
                    class="u-link-gray-800 u-link__hover--blue-highlight d-inline-block">
                    <img src="assets/img/index/icons/13.svg" alt="" class="l-header-icon">
                    <span>會員專區</span>
                </a>
            </div>
            <div class="d-flex">
                <a id="link_store_query" href="index.php?Page=8"
                    class="u-link-gray-800 u-link__hover--blue-highlight d-inline-block u-pt-050 u-pb-025">
                    <img src="assets/img/index/icons/01.svg" alt="" class="l-header-icon">
                    <span>門市查詢</span>
                </a>
                <div class="l-header-seperator">
                </div>
                <a id="link_tag_id2" href="javascript:void(0)"
                    class="u-link-gray-800 u-link__hover--blue-highlight d-inline-block u-p-050"
                    rel="noreferrer noopener">
                    <img src="assets/img/index/icons/03.svg" alt="" class="l-header-icon">
                    <span>行動配送</span>
                </a>
                <div class="l-header-seperator">
                </div>
                <a id="link_app" href="#footerAnchor"
                    class="u-link-gray-800 u-link__hover--blue-highlight d-inline-block u-p-050 js-goToAnchor">
                    <img src="assets/img/index/icons/08.svg" alt="" class="l-header-icon">
                    <span>APP下載</span>
                </a>
                <div class="l-header-seperator">
                </div>
                <a href="en/formosa-investors-financial.php"
                    class="u-link-gray-800 u-link__hover--blue-highlight d-inline-block u-p-050">
                    <span>English</span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="l-header-layout">
        <button id="hamburger" class="l-header-hamburger">
            <div class="l-header-hamburger-bar l-header-hamburger-bar__top"></div>
            <div class="l-header-hamburger-bar l-header-hamburger-bar__middle"></div>
            <div class="l-header-hamburger-bar l-header-hamburger-bar__bottom"></div>
        </button>
        <a href="index.php" class="l-header-logo">
            <img src="assets/img/logo.png" alt="" class="l-header-logo-img">
        </a>
        <button id="sideLinkToggleBtn" type="button" class="c-sideLink-toggleBtn">
            <div class="l-header-hamburger js-menuOpened">
                <div class="l-header-hamburger-bar l-header-hamburger-bar__top"></div>
                <div class="l-header-hamburger-bar l-header-hamburger-bar__middle"></div>
                <div class="l-header-hamburger-bar l-header-hamburger-bar__bottom"></div>
            </div>
        </button>
        <nav id="menu" class="l-header-menu">
            <div class="container-xl u-px-000">
                <ul class="l-header-menu-layout">
                    <li class="l-header-menu-item l-header-menu-item--mobile justify-content-center">

                        <?php 
                           if (isset($_SESSION['user'])) {
                               $m_name =  $_SESSION['user']->m_name;
                               echo  "<a href='index.php?Page=B-5-1'> Hi! $m_name </a>
                               <a href='logout.php' class='c-btn c-btn--contained c-btn-blue-highlight u-ml-050 u-px-050' style='height:30px;'>登出</a>";
                           } else {
                               echo " <a href='index.php?Page=B' class='l-header-menu-link--mobile'>會員登入</a>";
                           }
                       ?>









                        <div class="l-header-seperator"></div>
                        <a id="link_member_phone" href="index.php?Page=B-5-1" class="l-header-menu-link--mobile">
                            會員專區
                        </a>
                    </li>
                    <li class="l-header-menu-item">
                        <a href="#" class="l-header-menu-link l-header-menu-link__hover--fill">
                            關於我們
                            <i class="far fa-angle-down u-ml-025"></i>
                        </a>
                        <!-- [START]下拉Mega menu -->
                        <div
                            class="l-header-submenu l-header-submenu-wrapper l-header-submenu--hidden u-px-150 u-px-xl-000">
                            <div class="container">
                                <ul
                                    class="l-header-submenu-layout row align-items-center justify-content-center u-mb-xl-200 u-mb-100 u-pt-xl-100">
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="index.php?Page=1-1" class="l-header-submenu-link">
                                            大事記
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="index.php?Page=1-2" class="l-header-submenu-link">
                                            經營理念
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="index.php?Page=1-3" class="l-header-submenu-link">
                                            公司組織
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="index.php?Page=1-4" class="l-header-submenu-link">
                                            教育訓練
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="index.php?Page=1-5" class="l-header-submenu-link">
                                            公益活動
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="index.php?Page=1-7" class="l-header-submenu-link">
                                            企業徵才
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- [END]下拉Mega menu  -->
                    </li>
                    <li class="l-header-menu-item">
                        <a name="link_about" href="index.php?Page=2"
                            class="l-header-menu-link l-header-menu-link__hover--fill">
                            最新消息
                        </a>
                    </li>
                    <li class="l-header-menu-item">
                        <a href="#" class="l-header-menu-link l-header-menu-link__hover--fill">
                            商品專區
                            <i class="far fa-angle-down u-ml-025"></i>
                        </a>
                        <!-- [START]下拉Mega menu -->
                        <div
                            class="l-header-submenu l-header-submenu-wrapper l-header-submenu--hidden u-px-150 u-px-xl-000">
                            <div class="container">
                                <ul class="l-header-submenu-layout row align-items-center 
               justify-content-center u-mb-xl-200 u-mb-100 u-pt-xl-100">
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="formosa_products.php?c=206"
                                            class="l-header-submenu-link">
                                            平光眼鏡
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="formosa_products.php?c=80"
                                            class="l-header-submenu-link">
                                            太陽眼鏡
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="formosa_products.php?c=1"
                                            class="l-header-submenu-link">
                                            隱形眼鏡透明片
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="formosa_products.php?c=35"
                                            class="l-header-submenu-link">
                                            隱形眼鏡彩片
                                        </a>
                                    </li>
                                    <!-- <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a href="index.php?Page=3" class="l-header-submenu-link">
                                            複合式專區
                                        </a>
                                    </li> -->
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="index.php?Page=3-3" class="l-header-submenu-link">
                                            複合式專區
                                        </a>
                                    </li>
                                    <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                        <a name="link_about" href="index.php?Page=3-4" class="l-header-submenu-link">
                                            鏡片
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- [END]下拉Mega menu  -->
                    </li>
                    <li class="l-header-menu-item">
                        <a name="link_about" href="index.php?Page=4"
                            class="l-header-menu-link l-header-menu-link__hover--fill">
                            人氣推薦
                        </a>
                    </li>
                    <li class="l-header-menu-item">
                        <a href="#" class="l-header-menu-link l-header-menu-link__hover--fill">
                            保健知識
                            <i class="far fa-angle-down u-ml-025"></i>
                        </a>
                        <!-- [START]下拉Mega menu -->
                        <div
                            class="l-header-submenu l-header-submenu-wrapper l-header-submenu--hidden u-px-150 u-px-xl-000">
                            <div class="container">
                                <div
                                    class="l-header-submenu-layout row align-items-center justify-content-center u-mb-xl-200 u-mb-100 u-pt-xl-100">
                                    <div class="col-xl-10">
                                        <ul class="row">
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-1"
                                                    class="l-header-submenu-link">
                                                    眼睛基礎
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-2"
                                                    class="l-header-submenu-link">
                                                    視力保健
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-3"
                                                    class="l-header-submenu-link">
                                                    鏡框
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-4"
                                                    class="l-header-submenu-link">
                                                    鏡片
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-5"
                                                    class="l-header-submenu-link">
                                                    多焦點
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-6"
                                                    class="l-header-submenu-link">
                                                    隱形眼鏡
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-7"
                                                    class="l-header-submenu-link">
                                                    太陽眼鏡
                                                </a>
                                            </li>
                                            <!-- <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-8"
                                                    class="l-header-submenu-link">
                                                    極智焦距定位儀
                                                </a>
                                            </li> -->
                                        </ul>
                                        <ul class="row">
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a name="link_about" href="index.php?Page=5-8"
                                                    class="l-header-submenu-link">
                                                    極智焦距定位儀
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a href="index.php?Page=5-9" class="l-header-submenu-link">
                                                    法國依視路WAM700+全方位視覺檢測系統
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mx-xl-3 mt-3">
                                                <a href="index.php?Page=5-A" class="l-header-submenu-link">
                                                    德國蔡司 SL 120 數位光學顯像系統
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- [END]下拉Mega menu  -->
                    </li>
                    <li class="l-header-menu-item">
                        <a href="#" class="l-header-menu-link l-header-menu-link__hover--fill">
                            投資人專區
                            <i class="far fa-angle-down u-ml-025"></i>
                        </a>
                        <!-- [START]下拉Mega menu -->
                        <div
                            class="l-header-submenu l-header-submenu-wrapper l-header-submenu--hidden u-px-150 u-px-xl-000">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-xl-10">
                                        <ul
                                            class="l-header-submenu-layout row align-items-center u-mb-xl-200 u-mb-100 u-pt-xl-100">
                                            <li class="l-header-submenu-item col-xl-2 mt-3">
                                                <a name="link_about" href="index.php?Page=6-1"
                                                    class="l-header-submenu-link">
                                                    財務資訊
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-2 mt-3">
                                                <a name="link_about" href="index.php?Page=6-3"
                                                    class="l-header-submenu-link">
                                                    股東會資料
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-2 mt-3">
                                                <a name="link_about" href="index.php?Page=6-4"
                                                    class="l-header-submenu-link">
                                                    公司治理
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-2 mt-3">
                                                <a name="link_about" href="index.php?Page=6-F"
                                                    class="l-header-submenu-link">
                                                    股利及股價資訊
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-auto mt-3">
                                                <a name="link_about" href="index.php?Page=6-H"
                                                    class="l-header-submenu-link">
                                                    履行企業社會責任情形
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-2 mt-3">
                                                <a name="link_about" href="index.php?Page=6-G"
                                                    class="l-header-submenu-link">
                                                    重大訊息
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-2 mt-3">
                                                <a name="link_about" href="index.php?Page=6-I"
                                                    class="l-header-submenu-link">
                                                    股代聯絡資訊
                                                </a>
                                            </li>
                                            <li class="l-header-submenu-item col-xl-2 mt-3">
                                                <a name="link_about" href="index.php?Page=6-J"
                                                    class="l-header-submenu-link">
                                                    法人說明會訊息
                                                </a>
                                            </li>
                                            <!-- <li class="l-header-submenu-item col-xl-3 mt-3">
                    <a href="#" class="l-header-submenu-link">
                      公司履行誠信經營情形及採行措施
                    </a>
                  </li> -->
                                            <li class="l-header-submenu-item col-xl-2 mt-3">
                                                <a name="link_about" href="index.php?Page=6-K"
                                                    class="l-header-submenu-link">
                                                    利害關係人專區
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- [END]下拉Mega menu  -->
                    </li>
                    <!-- <li class="l-header-menu-item">
            <a href="#" class="l-header-menu-link l-header-menu-link__hover--fill">
              寶島眼科
            </a>
          </li> -->
                </ul>
            </div>
            <div class="l-header-bottom container-fluid">
                <div class="l-header-bottom-layout">
                    <a id="link_store_query_phone" href="formosa_store.php" class="l-header-bottom-link">
                        <img src="assets/img/index/icons/01.svg" alt="" class="l-header-icon">
                        門市查詢
                    </a>
                    <div class="l-header-seperator"></div>
                    <a id="link_tag_id4" href="javascript:void(0)" class="l-header-bottom-link"
                        rel="noreferrer noopener">
                        <img src="assets/img/index/icons/03.svg" alt="" class="l-header-icon">
                        行動配送
                    </a>
                    <div id="link_app_phone" class="l-header-seperator"></div>
                    <a href="index.php?Page=B-7#appId" class="l-header-bottom-link">
                        <img src="assets/img/index/icons/08.svg" alt="" class="l-header-icon">
                        APP下載
                    </a>
                </div>
                <div class="l-header-bottom-layout">
                    <div class="d-flex justify-content-center u-mt-100">
                        <a href="index.php"
                            class="c-btn c-btn--outlined c-btn-gray-800 u-py-050 u-px-200 u-mr-100 rounded-pill">繁體中文</a>
                        <a href="en/formosa-investors-financial.php"
                            class="c-btn c-btn--outlined c-btn-gray-800 u-py-050 u-px-200 rounded-pill">English</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- end header -->