<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>會員登入 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="會員登入／註冊 · 使用社群帳號快速登入或註冊！請注意英文大小寫，以免影響輸入。">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />
    
     <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>
    
    <script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>    
    
    <script src="assets/plugins/platform.js"></script>
    <!-- JS Customization -->
    
     
   	<script type="text/javascript">
   
  

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));

     window.fbAsyncInit = function() {
        FB.init({
          appId      : '578978113058811',
          cookie     : true,
          xfbml      : true,
          version    : 'v2.2'
        });
          
        FB.AppEvents.logPageView();   
          
      };
    
       	window.alert = function(msg, icon="info") {
    		Swal.fire({
    		  text: ""+msg,
    		  icon: icon,
    		  confirmButtonColor: '#3085d6',
    		  confirmButtonText: '確定'
    		})
    	};

    	function fbLogin(){
    		FB.login(function(response) {
        		if (response.status === 'connected') {
        			//已登入 Facebook，也登入應用程式
        			 FB.api('/me', {fields: 'name,email,picture.type(large)'},function (response) {
        				 var id =  response.id;
      					 console.log("id=" + id);
       					login(id,"fb");
    	   
                 	 });
        		}  
        	},{scope: 'public_profile,email'}); //選擇性 scope 參數是權用來向應用程式用戶要求權限
 
        }

    	function googleLogin(){
       		 auth2.signIn().then(function() {
    	   	     if (auth2.isSignedIn.get()) {
	      	    	  var profile = auth2.currentUser.get().getBasicProfile();
	      	    	  console.log('ID: ' + profile.getId());
 	      	    	 
       	      	      login(profile.getId(),"google");
    	  	     }
    	   	  });
        }
        
        //for line login
        var win;

    	window.addEventListener('message',function(e){
    	     	if (e.origin.indexOf(window.location.hostname) < 0) return;
    
    			if (win)  win.close();
    			 
    			var id =  e.data;
    			 
    			if (id != "" &&  id !="error") {
    			    login(id, "line");
    			    
    			}
    	   },false);

    	function lineLogin(){
			var dftWidth=400;
			var dftHeight=600;
			var w=window.screen.width/2-(dftWidth/2);
			var t=window.screen.height/2-(dftHeight/2);
			
    		let client_id = '1656601227';
            let redirect_uri = 'https://formosa.ai-ad.net.tw/line_callback.php';
            let link = 'https://access.line.me/oauth2/v2.1/authorize?';
            link += 'response_type=code';
            link += '&client_id=' + client_id;
            link += '&redirect_uri=' + redirect_uri;
            link += '&state=login';
            link += '&scope=openid%20profile';
            //window.location.href = link;

            win = window.open(link, '_blank', 'location=false,height=400,width=500,scrollbars=yes,status=yes,top=' + t + ",left=" + w);
        }

		function login(social_id , type){
			var params ={
				"social_id" : social_id,
				"type" : type,
			};
			$.get("app/controller/Member.php?method=socialLogin",params, function(result){
	       		  result = JSON.parse(result);
	       		  if (result.result =="1") {
	         	       window.location = "index.php";
	         	  } else {
					   alert("登入錯誤或帳號尚未綁定!");
  		          }
	  	   }); 
	    }
        
       	$(function(){

       		gapi.load('auth2', function() {
           	  auth2 = gapi.auth2.init({
               	  client_id: '990851712395-db8pqllp14ogjjil3h5j9moq6c39j4mh.apps.googleusercontent.com',
               	  fetch_basic_profile: true,
               	  scope: 'profile'
                 });
        	 });
          	 
          	 $.validator.methods.reg = function (value, element, params) {
    			 return this.optional(element) || params.test(value);
    		 };

    		 $("#form").validate({
      			  rules:  {
        		     account: {
      					 required: true,
      			    	 reg : /^([a-zA-Z0-9]+)$/,
      			    	 rangelength : [6,12],
      				 },
      				 
      				 password :{
      					 required: true,
      			    	 reg : /^([a-zA-Z0-9]+)$/,
      			    	 rangelength : [6,12],
      				 },
      				  
      		      }, 
      		      messages: {      		    	  
      		    	 account : "請輸入6-12位英數帳號",
      		    	 password : "請輸入6-12位英數帳號",
      			  },
      			 
      			  submitHandler:function(form){
 
      				  var params =  {
     					   "m_account":$("#account").val(),
     					   "m_passwd":$("#password").val(),
     				  };
      		  

      				  $.get("app/controller/Member.php?method=chkAccountPasswd",params, function(result){
    		         		  result = JSON.parse(result);
    		         		  if (result.result =="1") {
        		         		//登入首頁
      		         			  window.location = "index.php";
    	  		         	  } else {
   							 	  alert(result.message)
    	  	  		          }
    		    	   }); 
      	          }
      		 });
       	})

   	</script>
    
    <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="header" class="l-header"></header>
		<div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-100">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">會員登入</h3>
                    </div>
                </div>
                <div class="u-pb-100">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-12">
                                <form id="form">
                                    <div class="u-mb-100">
                                        <label class="d-flex align-items-center" for="required">
                                            <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon4.svg" alt="">
                                            </div>
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-18 u-m-000">帳號</h5>
                                        </label>
                                        <input type="text" id="account" name="account" class="l-form-field" placeholder="請輸入6-15位英數帳號">
                                    </div>

                                    <div class="u-mb-200">
                                        <label class="d-flex align-items-center" for="password">
                                            <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon5.svg" alt="">
                                            </div>
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-18 u-m-000">密碼</h5>
                                        </label>
                                        <input type="password" id="password" name="password" class="l-form-field" placeholder="請輸入6-12位英數密碼">
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6 col-12 u-mb-100">
                                            <button type="button" onclick="location.href='formosa_members-forget-choseCountry.php'" class="c-btn c-btn--outlined c-btn-gray-800 col">
                                                忘記帳號/密碼
                                            </button>
                                        </div>
                                        <div class="col-md-6 col-12 u-mb-200">
                                            <button    type="submit" class="c-btn c-btn--contained c-btn-blue-highlight col">
                                                登入
                                            </button>
                                        </div>
                                    </div>
                                    <div class="u-mb-200">
                                        <p class="p-divider u-text-gray-500 u-font-weight-700 u-font-18">或</p>
                                    </div>
                                    <div class="u-mb-200">
                                        <p class="u-text-gray-800 u-font-weight-700 u-font-18 text-center">您的會員帳戶曾經有綁定才可由此登入</p>
                                    </div>
                                    
                                    <div class="row align-items-start">
                                        <div class="col-4">
                                            <a class="d-flex flex-column align-items-center" href="javascript:fbLogin()">
                                                <div class="col-sm-8 col-12 u-mb-100 p-linkIcon">
                                                    <img src="assets/img/members/facebook.svg" alt="">
                                                </div>
                                                <p class="u-text-gray-800">Facebook登入</p>
                                            </a>
                                        </div>
                                        <div class="col-4">
                                            <a class="d-flex flex-column align-items-center" href="javascript:googleLogin()">
                                                <div class="col-sm-8 col-12 u-mb-100 p-linkIcon">
                                                    <img src="assets/img/members/google.svg" alt="">
                                                </div>
                                                <p class="u-text-gray-800">Google登入</p>
                                            </a>
                                        </div>
                                        <div class="col-4">
                                            <a class="d-flex flex-column align-items-center" href="javascript:lineLogin()">
                                                <div class="col-sm-8 col-12 u-mb-100 p-linkIcon">
                                                    <img src="assets/img/members/line.svg" alt="">
                                                </div>
                                                <p class="u-text-gray-800">LINE登入</p>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php')?></footer>
        <!-- =============end footer ============= -->
    </div>
   
</body>

</html>