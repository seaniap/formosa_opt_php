<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">會員登入</h3>
            </div>
        </div>
        <div class="u-pt-100 u-pb-250">
            <div class="container">
                <h5 class="u-text-gray-800 u-font-weight-700 u-font-24 text-center u-mb-200">親愛的訪客，您好</h5>
                <div class="row justify-content-center">
                    <div class="col-10 col-sm-8 col-md-12 col-lg-10">
                        <div class="row justify-content-center align-items-start">
                            <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                <a href="index.php?Page=B-1">
                                    <div class="p-iconBg u-mb-100">
                                        <img src="assets/img/members/icon7.svg" alt="" class="img-fluid">
                                    </div>
                                    <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">會員登入
                                    </h5>
                                    <p class="u-text-gray-800 u-mb-000">已有帳號密碼</p>
                                </a>
                            </div>
                            <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                <a href="index.php?Page=B-2">
                                    <div class="p-iconBg u-mb-100">
                                        <img src="assets/img/members/icon8.svg" alt="" class="img-fluid">
                                    </div>
                                    <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">立即開卡
                                    </h5>
                                    <p class="u-text-gray-800 u-mb-000">已加入會員<br>但沒有帳號密碼</p>
                                </a>
                            </div>
                            <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                <a href="index.php?Page=B-3">
                                    <div class="p-iconBg u-mb-100">
                                        <img src="assets/img/members/icon9.svg" alt="" class="img-fluid">
                                    </div>
                                    <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">立即註冊
                                    </h5>
                                    <p class="u-text-gray-800 u-mb-000">未曾加入過會員</p>
                                </a>
                            </div>
                            <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                <a href="index.php?Page=B-4">
                                    <div class="p-iconBg u-mb-100">
                                        <img src="assets/img/members/icon10.svg" alt="" class="img-fluid">
                                    </div>
                                    <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">暸解狀態
                                    </h5>
                                    <p class="u-text-gray-800 u-mb-000">不確定是否為會員<br>或有帳號密碼</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- end -->