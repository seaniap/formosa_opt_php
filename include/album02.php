<?php require_once('../Connections/MySQL.php'); ?>
<?php


############ Configuration ##############
$thumb_square_size 		= 200; //Thumbnails will be cropped to 200x200 pixels
$max_image_size 		= 1000; //Maximum image size (height and width)
$thumb_prefix			= "thumb_"; //Normal thumb Prefix
$destination_folder		= 'UploadImages/'; //upload directory ends with / (slash)
$jpeg_quality 			= 100; //jpeg quality
##########################################

//continue only if $_POST is set and it is a Ajax request
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
require_once('../Connections/AdminMySQL.php');
//echo "333";
	// check $_FILES['ImageFile'] not empty
	
$fileCount = count($_FILES['image_file2']['name']);
	//echo $fileCount;
	if($fileCount >10){ echo "證照(請勿超過10張)"; exit; } 		
for ($i = 0; $i < $fileCount; $i++) {
	
	
	
	if(!$_FILES['image_file2']['error'][$i] === UPLOAD_ERR_OK){
			die('Image file is Missing!'); // output error when above checks fail.
	}
	
	//uploaded file info we need to proceed
	$image_name = $_FILES['image_file2']['name'][$i]; //file name
	$image_size = $_FILES['image_file2']['size'][$i]; //file size
	$image_temp = $_FILES['image_file2']['tmp_name'][$i]; //file temp

	$image_size_info 	= getimagesize($image_temp); //get image size
	
	if($image_size_info){
		$image_width 		= $image_size_info[0]; //image width
		$image_height 		= $image_size_info[1]; //image height
		$image_type 		= $image_size_info['mime']; //image type
	}else{
		die("Make sure image file is valid!");
	}

	//switch statement below checks allowed image type 
	//as well as creates new image from given file 
	switch($image_type){
		case 'image/png':
			$image_res =  imagecreatefrompng($image_temp); break;
		case 'image/gif':
			$image_res =  imagecreatefromgif($image_temp); break;			
		case 'image/jpeg': case 'image/pjpeg':
			$image_res = imagecreatefromjpeg($image_temp); break;
		default:
			$image_res = false;
	}

	if($image_res){
		//Get file extension and name to construct new file name 
		$image_info = pathinfo($image_name);
		$image_extension = strtolower($image_info["extension"]); //image extension
		$image_name_only = strtolower($image_info["filename"]);//file name only, no extension
		
		//create a random name for new image (Eg: fileName_293749.jpg) ;
		//$new_file_name = $image_name_only. '_' .  rand(0, 9999999999) . '.' . $image_extension;
		$new_file_name = date("Ymd").uniqid(mt_rand()). '.' . $image_extension;
		//folder path to save resized images and thumbnails
		$thumb_save_folder 	= $destination_folder . $thumb_prefix . $new_file_name; 
		$image_save_folder 	= $destination_folder . $new_file_name;
		
		//call normal_resize_image() function to proportionally resize image
	
		if(normal_resize_image($image_res, $image_save_folder, $image_type, $max_image_size, $image_width, $image_height, $jpeg_quality))
		{

// 插入資料進資料庫


$insertSQL = sprintf("INSERT INTO %s_BigTable (KeyID, Template, Page, Sq, ProductID, Pname, Cate01, Cate02, Cate03, Price01, h01, h02, h03, h04, h05, h06, h07, h08, h09, h10,h11, h12, h13, h14, h15, h16, h17, h18, h19, h20, c01, c02, c03, c04, c05, c06, c07, c08, c09, c10, d01, d02, d03, f01, f02, f03, l01, l02, l03, l04, l05, l06, p01, p02, p03, p04, p05, p06,author) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)",
	                   $Table_ID, 
                       GetSQLValueString(uniqid(mt_rand()), "text"),
                       GetSQLValueString($_POST['Template'], "text"),
                       GetSQLValueString($_POST['Page'], "text"),
                       GetSQLValueString($_POST['Sq'], "int"),
                       GetSQLValueString($_POST['ProductID'], "text"),
                       GetSQLValueString($_POST['Pname'], "text"),
                       GetSQLValueString($_POST['Cate01'], "text"),
                       GetSQLValueString($_POST['Cate02'], "text"),
                       GetSQLValueString($_POST['Cate03'], "text"),
                       GetSQLValueString($_POST['Price01'], "int"),
                       GetSQLValueString($_POST['h01'], "text"),
                       GetSQLValueString($_POST['h02'], "text"),
                       GetSQLValueString($_POST['h03'], "text"),
                       GetSQLValueString($_POST['h04'], "text"),
                       GetSQLValueString($_POST['h05'], "text"),
                       GetSQLValueString($_POST['h06'], "text"),
                       GetSQLValueString($_POST['h07'], "text"),
                       GetSQLValueString($_POST['h08'], "text"),
                       GetSQLValueString($_POST['h09'], "text"),
                       GetSQLValueString($_POST['h10'], "text"),
  				       GetSQLValueString($_POST['h11'], "text"),
                       GetSQLValueString($_POST['h12'], "text"),
                       GetSQLValueString($_POST['h13'], "text"),
                       GetSQLValueString($_POST['h14'], "text"),
                       GetSQLValueString($_POST['h15'], "text"),
                       GetSQLValueString($_POST['h16'], "text"),
                       GetSQLValueString($_POST['h17'], "text"),
                       GetSQLValueString($_POST['h18'], "text"),
                       GetSQLValueString($_POST['h19'], "text"),
                       GetSQLValueString($_POST['h20'], "text"),
					   
                       GetSQLValueString($_POST['c01'], "text"),
                       GetSQLValueString($_POST['c02'], "text"),
					   GetSQLValueString($_POST['c03'], "text"),
					   GetSQLValueString($_POST['c04'], "text"),
					   GetSQLValueString($_POST['c05'], "text"),
					   GetSQLValueString($_POST['c06'], "text"),
				       GetSQLValueString($_POST['c07'], "text"),
					   GetSQLValueString($_POST['c08'], "text"),
					   GetSQLValueString($_POST['c09'], "text"),
					   GetSQLValueString($_POST['c10'], "text"),
                       GetSQLValueString($_POST['d01'], "date"),
                       GetSQLValueString($_POST['d02'], "date"),
                       GetSQLValueString(date("Y-m-d H:i:s"), "date"),
                       GetSQLValueString($_POST['f01'], "text"),
                       GetSQLValueString($_POST['f02'], "text"),
                       GetSQLValueString($_POST['f03'], "text"),
                       GetSQLValueString($_POST['l01'], "text"),
                       GetSQLValueString($_POST['l02'], "text"),
                       GetSQLValueString($_POST['l03'], "text"),
                       GetSQLValueString($_POST['l04'], "text"),
                       GetSQLValueString($_POST['l05'], "text"),
                       GetSQLValueString($_POST['l06'], "text"),
                       GetSQLValueString($new_file_name, "text"),
                       GetSQLValueString($p02, "text"),
                       GetSQLValueString($p03, "text"),
                       GetSQLValueString($p04, "text"),
                       GetSQLValueString($p05, "text"),
                       GetSQLValueString($p06, "text"),
					   GetSQLValueString($_POST['author'], "text"));
					   
					   
					   
					   $Result1 = mysqli_query($MySQL,$insertSQL) or die(mysqli_error($MySQL));
//echo $insertSQL."<br>";


			//call crop_image_square() function to create square thumbnails
		/*	if(!crop_image_square($image_res, $thumb_save_folder, $image_type, $thumb_square_size, $image_width, $image_height, $jpeg_quality))
			{
				die('Error Creating thumbnail');
			}
			*/
			/* We have succesfully resized and created thumbnail image
			We can now output image to user's browser or store information in the database*/
		//	echo '<div align="center">';
		//	echo '<img src="PostImages/'.$thumb_prefix . $new_file_name.'" alt="Thumbnail">';
		//	echo '<br />';
		//	echo '<img src="UploadImages/'. $new_file_name.'" alt="Resized Image"  class="img-responsive">';
		//	echo '</div>';

			
		//	echo'<input type="hidden" name="tp02[]"   value="'.$new_file_name.'">';
			
		}
		
		imagedestroy($image_res); //freeup memory
	}
	
}  // for 多照片
	
	
 $insertGoTo = "index.php?Page=".$_POST['Page']."&album02=".$_POST['l06']; //關閉視窗 
   header(sprintf("Location: %s", $insertGoTo)); //關閉視窗 	
	
	exit;
	
	
}

#####  This function will proportionally resize image ##### 
function normal_resize_image($source, $destination, $image_type, $max_size, $image_width, $image_height, $quality){
	
	if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
	
	//do not resize if image is smaller than max size
	if($image_width <= $max_size && $image_height <= $max_size){
		if(save_image($source, $destination, $image_type, $quality)){
			return true;
		}
	}
	
	//Construct a proportional size of new image
	$image_scale	= min($max_size/$image_width, $max_size/$image_height);
	$new_width		= ceil($image_scale * $image_width);
	$new_height		= ceil($image_scale * $image_height);
	
	$new_canvas		= imagecreatetruecolor( $new_width, $new_height ); //Create a new true color image
	
	//Copy and resize part of an image with resampling
	if(imagecopyresampled($new_canvas, $source, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height)){
		save_image($new_canvas, $destination, $image_type, $quality); //save resized image
	}

	return true;
}

##### This function corps image to create exact square, no matter what its original size! ######
function crop_image_square($source, $destination, $image_type, $square_size, $image_width, $image_height, $quality){
	if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
	
	if( $image_width > $image_height )
	{
		$y_offset = 0;
		$x_offset = ($image_width - $image_height) / 2;
		$s_size 	= $image_width - ($x_offset * 2);
	}else{
		$x_offset = 0;
		$y_offset = ($image_height - $image_width) / 2;
		$s_size = $image_height - ($y_offset * 2);
	}
	$new_canvas	= imagecreatetruecolor( $square_size, $square_size); //Create a new true color image
	
	//Copy and resize part of an image with resampling
	if(imagecopyresampled($new_canvas, $source, 0, 0, $x_offset, $y_offset, $square_size, $square_size, $s_size, $s_size)){
		save_image($new_canvas, $destination, $image_type, $quality);
	}

	return true;
}

##### Saves image resource to file ##### 
function save_image($source, $destination, $image_type, $quality){
	switch(strtolower($image_type)){//determine mime type
		case 'image/png': 
			imagepng($source, $destination); return true; //save png file
			break;
		case 'image/gif': 
			imagegif($source, $destination); return true; //save gif file
			break;          
		case 'image/jpeg': case 'image/pjpeg': 
			imagejpeg($source, $destination, $quality); return true; //save jpeg file
			break;
		default: return false;
	}
}




?>
<?php $template="album02.php";
$templateS="album02";
// 判斷是預設是A版還是S版
// 預設 A 版

if( $_GET[$templateS]=="" and $row_template['Extend']=="Y" ) {
//A 版開始
if( $_GET['demo']=='y'  ) { ?>
<center>
                    <div style="display:none;"></div>
                   
                    <div  style="float:left; background-image:url(ch_images/activity.png); background-repeat:no-repeat; width:225px; height:200px; padding:10px 0px 0px 0px; margin:0px 0px 0px 0px;">
					<a href="index.php?Page=<?php echo $_GET['Page']; ?>&<?php echo $templateS."=".$row_content01['KeyID'];?>"><img src="UploadImages/<?php echo $row_content01['p01'];  ?>" alt="<?php echo $row_content01['h01'];  ?>" width="200" height="125" style="margin:8px 0px 0px 0px; font-size:13px;" /></a>
                      <div><a href="index.php?Page=<?php echo $_GET['Page']; ?>&<?php echo $templateS."=".$row_content01['KeyID'];?>">
					  
					  <div  class="exhtext2"><?php echo $row_content01['h01'];  ?></div>
                     		
                        <div class="exhtext3">日期:<?php echo substr($row_content01['d01'],0,10);  ?></div></a>
                        <div style=""><?php include("3buttonA.php");  ?></div>
                      </div>
                    </div>
                  </center>
 
<?php } else {  ?>
<?php $currentPage = $_SERVER["PHP_SELF"];

$maxRows_content01 = $row_template['MaxRow'];
$pageNum_content01 = 0;
if (isset($_GET['pageNum_content01'])) {
  $pageNum_content01 = $_GET['pageNum_content01'];
}
$startRow_content01 = $pageNum_content01 * $maxRows_content01;

$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ",$Table_ID, GetSQLValueString($colname_content01, "text"));
if($_GET['Cate01']<>""){
$query_content01=$query_content01."AND Cate01='".$_GET['Cate01']."'" ;}

if($_GET['Cate02']<>""){
$query_content01=$query_content01."AND Cate02='".$_GET['Cate02']."'" ;}

$query_limit_content01 = sprintf("%s order by Sq LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
//echo $query_limit_content01 ;
$content01 = mysqli_query($MySQL,$query_limit_content01 ) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);

  $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
$totalPages_content01 = ceil($totalRows_content01/$maxRows_content01)-1;

$queryString_content01 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_content01") == false && 
        stristr($param, "totalRows_content01") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);

//相簿目錄頁 album02


?>















                  <!--重複區域-->
<?php do { ?>                       
                  <center>
                    <div style="display:none;"></div>
                   
                    <div  style="float:left; background-image:url(ch_images/activity.png); background-repeat:no-repeat; width:225px; height:200px; padding:10px 0px 0px 0px; margin:0px 0px 0px 0px;">
					<a href="index.php?Page=<?php echo $_GET['Page']; ?>&<?php echo $templateS."=".$row_content01['KeyID'];?>"><img src="UploadImages/<?php echo $row_content01['p01'];  ?>" alt="<?php echo $row_content01['h01'];  ?>" width="200" height="125" style="margin:8px 0px 0px 0px;" /></a>
                      <div><a href="index.php?Page=<?php echo $_GET['Page']; ?>&<?php echo $templateS."=".$row_content01['KeyID'];?>">
					  
					  <div class="exhtext2"><?php echo $row_content01['h01'];  ?></div>
                     
                        <div class="exhtext3">日期:<?php echo substr($row_content01['d01'],0,10);  ?></div>
                       </a> <div style=""><?php include("3buttonA.php");  ?></div>		
                      </div>
                    </div>
                  </center>
 <?php } while ($row_content01 = mysqli_fetch_assoc($content01)); ?>                  
                  <!--重複區域end-->
<div class="clear_both"></div>                 
</td>
      </tr>
      </table>          		    
  
   
<?php mysqli_free_result($content01);
 } 
 
//A 版結束 
} 
else {  //S版開始
 
if( $_GET['demo']=='y'  ) { ?>



                <!--重複開始-->
       <div class="exh"> <a class="example-image-link" href="UploadImages/<?php echo $row_content01['p02'];  ?>" data-lightbox="example-set" data-title="<?php echo $row_content01['h12'];  ?>"> <img src="UploadImages/<?php echo $row_content01['p02'];  ?>" alt="" width="200" height="175" border="0" class="example-image p_img"/>
    <div class="exhtext"><?php echo $row_content01['h12'];  ?></div></a>
    </div>
<?php } if($row_content01['p03']<>""){ ?>      
      <!--重複結束-->
      


<?php } else {  ?>
<?php $colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$KeyID=str_replace($vowels ,"**!!!**",substr($_GET[$templateS],0,50));

if($row_template['Extend']=='Y'){
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='album02s.php' AND l06= %s order by Sq+0 ",$Table_ID, GetSQLValueString($colname_content01, "text"),GetSQLValueString($_GET['album02'], "text"));
}
else {
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ",$Table_ID, GetSQLValueString($colname_content01, "text")); 	
}



$content01 = mysqli_query($MySQL,$query_content01 ) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
$totalRows_content01 = mysqli_num_rows($content01);
//echo "S版啟動".$query_content01;

//相片內頁 $template="album01s.php";

//相簿資訊
//點擊加一

$query_content02 = sprintf("update %s_BigTable set Price01=IFNULL(Price01+1,1)  WHERE KeyID = %s  ",$Table_ID, GetSQLValueString($_GET['album02'], "text"));
$content02 = mysqli_query($MySQL,$query_content02 ) or die(mysqli_error($MySQL));
//echo $query_content02;

$query_content02 = sprintf("SELECT * FROM %s_BigTable WHERE KeyID = %s  ",$Table_ID, GetSQLValueString($_GET['album02'], "text"));
$content02 = mysqli_query( $MySQL,$query_content02) or die(mysqli_error($MySQL));
$row_content02 = mysqli_fetch_assoc($content02);
//echo $query_content02;


?>


<!--照片放大語法開始-->
<link rel="stylesheet" href="ch_lightbox/css/lightbox.css" />
<script src="ch_lightbox/js/jquery-1.11.0.min.js"></script>
<script src="ch_lightbox/js/lightbox.js"></script>
 <!--照片放大語法結束-->
<?php if($_SESSION['Mode']=="Admin" or $_SESSION['Mode']=="User" ){ ?>     
<div style="background-color: #4ac4d8;border-radius: 15px;padding: 10px">
<form method="post" name="form1" action="album02.php" enctype="multipart/form-data">
  <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" >       
	
	<input name="l06" type="hidden" class="f" id="l06"   value="<?php echo $_GET['album02']; ?>" size="35">    
	       
	<tr valign="baseline">
      <td align="right" nowrap class="style11" valign="top">
       <div style=" margin:10px;font-size: 16px;color: aliceblue"><strong>多張照片上傳：</strong></div>
       </td>
		<td>
		  <div style=" margin-top:10px;"> 
				<!-- width代表預覽圖的寬度--> 
 				<input class="upload-btn form-control" name="image_file2[]" multiple="" id="imageInput2" type="file" />
 				<input class="upload-btn" type="button"  value="點擊上傳" onClick="checkupload(document.form1)">
		  </div>
          <small style="border-bottom: 2px solid #E35456;color: #FFF">限用jpg 格式,請勿使用中文檔名 (請上傳後。再編輯文字說明)</small>
       </td>
    </tr>  

</td>
    </tr>	  	  
  </table></td>
    </tr>	  

  </table></td>
    </tr>
 
  </table>
  
  <input type="hidden" name="KeyID" value="<?php echo uniqid(mt_rand()); ?>">
  <input type="hidden" name="Template" value="album02s.php">
  <input type="hidden" name="Page" value="<?php echo $_GET['Page'] ?>">
  
  <input type="hidden" name="p01" value="<?php echo  $row_MiddleBigTable['p01']; ?>" id="p01" >
  <input type="hidden" name="p02" value="<?php echo  $row_MiddleBigTable['p02']; ?>" id="p02" >
  <input type="hidden" name="p03" value="<?php echo  $row_MiddleBigTable['p03']; ?>" id="p03" >
  <input type="hidden" name="p04" value="<?php echo  $row_MiddleBigTable['p04']; ?>" id="p04" >
  <input type="hidden" name="p05" value="<?php echo  $row_MiddleBigTable['p05']; ?>" id="p05" >
  <input type="hidden" name="p06" value="<?php echo  $row_MiddleBigTable['p06']; ?>" id="p06" >
    
  <input type="hidden" name="tp01" value="" id="tp01" >
  <input type="hidden" name="tp02" value="" id="tp02">
  <input type="hidden" name="tp03" value="" id="tp03">
  <input type="hidden" name="tp04" value="" id="tp04">
  <input type="hidden" name="tp05" value="" id="tp05" >
  <input type="hidden" name="tp06" value="" id="tp06"> 
  
  <input type="hidden" name="author" id="author"  value="<?php echo $_SESSION['UserName']; ?>" >
  <input type="hidden" name="Action" id="Action" value="<?php echo $_GET['Action']; ?>" >
  <input type="hidden" name="d03" value="<?php echo date("Y-m-d H:i:s"); ?>" >
 <input type="hidden" name="MM_update" value="form1">
</form>

<script>

function checkupload(myform){

if( document.getElementById('imageInput2').value==''){
	 alert("請選擇照片!");
     return false;	
}else{
  myform.submit();
}
}
</script>


</div>      
<?php } ?>


      
      

     <?php do { ?>   
      
     <!--重複開始-->
<div class="exh"> <a class="example-image-link" href="UploadImages/<?php echo $row_content01['p01'];  ?>" data-lightbox="example-set" data-title="&nbsp;
"> <img src="UploadImages/<?php echo $row_content01['p01'];  ?>" alt="<?php echo $row_content01['h01'];  ?>" width="200" height="136" border="0" class="example-image spacing02 p_img "/>
<div class="exhtext"><?php echo $row_content01['h01'];  ?>

</div></a><?php  

	 $template="album02s.php";
	  if($totalRows_content01==0){  //如果一筆資料都沒有還是可以新增
	  $row_content01['Page']=$_GET['Page'];
	  $row_content01['Sq']=1000;
	  $row_content01['KeyID']=$_GET['album02'];
	  SuperM_ins($row_content01,$template,$row_template['Security']);
	  }
	  else {
	  SuperM_Button_3( $row_content01,$template,$row_template['Security']);
	  } 
	  ?>
</div>
  <?php } while ($row_content01 = mysqli_fetch_assoc($content01)); ?>  
      <!--重複結束-->
      
      
 
  
   
<div class="clear_both"></div>
<div class="Spacing12">
<center>
<a href=javascript:onclick=history.go(-1)><img src="std_images/back.png" alt="回上一頁" width="65" height="20" border="0" /></a>
</center>
</div>

<script type="text/javascript"> 
 
  $(document).ready(function(){
  	//點選小圖
  	$(".-prod-simg").click(function(){
 
  	  changeIMG($(this));
  	});
  });
  function changeIMG(jqueryObj){
   
    var opath = jqueryObj.attr("data-big-url")
	var idd = jqueryObj.attr("idd")
   //alert(idd)
   
   
  
  //  $("#showmainimg").fadeOut(function(){
   //   $("#showmainimg").attr("src",opath).fadeIn();
   
     $("#"+idd).fadeOut(function(){
      $("#"+idd).attr("src",opath).fadeIn();  
   
   
    });
  }
</script>

<?php mysqli_free_result($content01);
 } 
}  //S版結束
 ?>