<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">公司治理</h3>
            </div>
        </div>
        <!-- regulation tabs -->
        <?php include('formosa_regulation_tabs.php')?>
        <!-- end regulation tabs -->
        <div class="u-pb-100">
            <div class="container">
                <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">寶島光學科技股份有限公司 董事選任程序
                </p>
            </div>
        </div>
        <div class="u-pb-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">一、目的</h4>
                <p class="u-mb-000">
                    為公平、公正、公開選任董事，爰依「上市上櫃公司治理實務守則」第二十一條及第四十一條規定訂定本程序。
                </p>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">二、範圍</h4>
                <p class="u-mb-000">本公司董事之選任，除法令或章程另有規定者外，應依本程序辦理。</p>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">三、權責單位</h4>
                <ul class="u-list-style--custom-level02 u-mb-000">
                    <li class="u-mb-100">1.&nbsp;財會室：負責本辦法之制訂及編修。</li>
                    <li>2.&nbsp;選任單位：股東會。</li>
                </ul>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">四、作業內容</h4>
                <ul class="u-list-style--custom-level02 u-mb-000">
                    <li class="u-mb-100">1.&nbsp;選任資格：
                        <ul class="u-list-style--custom-level03">
                            <li>(1)&nbsp;本公司董事之選任，應考量董事會之整體配置。董事會成員組成應考量多元化，並就本身運作、營運型態及發展需求以擬訂適當之多元化方針，宜包括但不限於以下二大面向之標準：
                                <ul class="u-list-style--disc reset-text-indent">
                                    <li>基本條件與價值：性別、年齡、國籍及文化等。</li>
                                    <li>專業知識技能：專業背景（如法律、會計、產業、財務、行銷或科技）、專業技能及產業經驗等。</li>
                                </ul>
                            </li>
                            <li>(2)&nbsp;董事會成員應普遍具備執行職務所必須之知識、技能及素養，其整體應具備之能力如下：
                                <ul class="u-list-style--disc reset-text-indent">
                                    <li>營運判斷能力。</li>
                                    <li>會計及財務分析能力。</li>
                                    <li>經營管理能力。</li>
                                    <li>危機處理能力。</li>
                                    <li>產業知識。</li>
                                    <li>國際市場觀。</li>
                                    <li>領導能力。</li>
                                    <li>決策能力。</li>
                                </ul>
                            </li>
                            <li>(3)&nbsp;董事間應有超過半數之席次，不得具有配偶或二親等以內之親屬關係。</li>
                            <li>(4)&nbsp;本公司董事會應依據績效評估之結果，考量調整董事會成員組成。</li>
                            <li>(5)&nbsp;本公司獨立董事之資格，應符合「公開發行公司獨立董事設置即應遵循事項辦法」第二條、第三條以及第四條之規定。</li>
                        </ul>
                    </li>
                    <li>2.&nbsp;選任方式：
                        <ul class="u-list-style--custom-level03">
                            <li>(1)&nbsp;本公司董事之選舉，應依照公司法第一百九十二條之一所規定之候選人提名制度程序為之。<br>董事因故解任，致不足五人者，公司應於最近一次股東會補選之。但董事缺額達章程所定席次三分之一者，公司應自事實發生之日起六十日內，召開股東臨時會補選之。獨立董事之人數不足證券交易法第十四條之二第一項但書規定者，應於最近一次股東會補選之；獨立董事均解任時，應自事實發生之日起六十日內，召開股東臨時會補選之。
                            </li>
                            <li>(2)&nbsp;本公司董事之選舉應採用累積投票制，每一股份有與應選出董事人數相同之選舉權，得集中選舉一人，或分配選舉數人。</li>
                            <li>(3)&nbsp;董事會應製備與應選出董事人數相同之選舉票，並加填其權數，分發出席股東會之股東，選舉人之記名，得以在選舉票上所印出席證號碼代之。</li>
                            <li>(4)&nbsp;本公司董事依公司章程所定之名額，分別計算獨立董事、非獨立董事之選舉權，由所得選舉票代表選舉權數較多者分別依次當選，如有二人以上得權數相同而超過規定名額時，由得權數相同者抽籤決定，未出席者由主席代為抽籤。</li>
                            <li>(5)&nbsp;選舉開始前，由主席指定監票員、計票員各若干人，執行各項有關職務。其中監票員應具有股東身分，投票箱由董事會製備之，於投票前由監票員當眾開驗。
                            </li>
                            <li>(6)&nbsp;選舉票有左列情事之一者無效：
                                <ul class="u-list-style--disc reset-text-indent">
                                    <li>不用有召集權人製備之選票者。</li>
                                    <li>以空白之選票投入投票箱者。</li>
                                    <li>字跡模糊無法辨認或經塗改者。</li>
                                    <li>所填被選舉人與董事候選人名單經核對不符者。</li>
                                    <li>除填分配選舉權數外，夾寫其他文字者。</li>
                                </ul>
                            </li>
                            <li>
                                (7)&nbsp;投票完畢後當場開票，開票結果應由主席或指定司儀當場宣布，包含董事當選名單與其當選權數。<br>
                                前項選舉事項之選舉票，應由監票員密封簽字後，妥善保管，並至少保存一年。但經股東依公司法第一百八十九條提起訴訟者，應保存至訴訟終結為止。
                            </li>
                            <li>
                                (8)&nbsp;當選之董事由本公司董事會發給當選通知書。
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">五、施行</h4>
                <p class="u-mb-000">本程序由股東會通過後施行，修正時亦同。</p>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">六、相關文件</h4>
                <ol class="u-list-style--decimal u-mb-000">
                    <li class="u-mb-100">獨立董事設置及應遵循事項辦法</li>
                    <li>公司治理實務守則</li>
                </ol>
            </div>
        </div>
        <div class="u-py-100">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">七、實施及修訂歷程</h4>
                <ol class="u-list-style--decimal u-mb-000">
                    <li class="u-mb-100">中華民國83年11月28日首次制訂並經股東臨時會通過。</li>
                    <li class="u-mb-100">第一次修訂於中華民國98年6月19日，經股東會表決通過。</li>
                    <li class="u-mb-100">第二次修訂於中華民國104年6月22日，經股東會表決通過。</li>
                    <li>第三次修訂於中華民國110年7月27日，經股東會表決通過。</li>
                </ol>
            </div>
        </div>
        <div class="u-py-100 u-pb-300">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">八、檔案下載</h4>
                <div class="col-md-5 col-lg-3 col-auto u-pb-100 u-px-000">
                    <a target="_blank" class="c-btn c-btn--download c-btn-gray-800 u-text-black mb-2 col-auto" href="download/pdf/Others_regulations/董事及監察人簡歷.pdf">
                        <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050" style="width: 17px;">
                        <span>董事簡歷</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
</main>