<?php

namespace app\util;

include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";
 
session_start();

if (!isset($_SESSION['opencardMember']))
    header("Location:index.php?Page=B-2")

?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>立即開卡 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="若您已於寶島眼鏡門市加入會員，但沒有帳號密碼，請按照以下步驟完成開卡，即可享有寶島眼鏡會員獨享好康！">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />


    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript">
        ! function(q, g, r, a, p, h, js) {
            if (q.qg) return;
            js = q.qg = function() {
                js.callmethod ? js.callmethod.call(js, arguments) : js.queue.push(arguments);
            };
            js.queue = [];
            p = g.createElement(r);
            p.async = !0;
            p.src = a;
            h = g.getElementsByTagName(r)[0];
            h.parentNode.insertBefore(p, h);
        }(window, document, 'script', 'https://cdn.qgr.ph/qgraph.<?php echo Config::get("appier_id"); ?>.js');
    </script>
    <script type="text/javascript">
        //for test
        //var tempCode  = "1234";
        var tempCode = "";



        $(function() {
            window.alert = function(msg, icon = "info") {
                Swal.fire({
                    text: "" + msg,
                    icon: icon,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: '確定'
                })
            };

            window.showLoading = function(msg) {
                Swal.fire({
                    title: '請稍待...',
                    html: msg,
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    didOpen: () => {
                        Swal.showLoading()
                    }
                });
            }

            $.validator.methods.matches = function(value, element, params) {
                var re = new RegExp(params);
                return this.optional(element) || re.test(value);
            };

            $.validator.methods.reg = function(value, element, params) {
                return this.optional(element) || params.test(value);
            };


            $.validator.methods.idnumber = function(value, element, params) {
                if (value == "") return true;

                var userid = value;
                var reg = /^[A-Z]{1}[1-2]{1}[0-9]{8}$/;
                if (reg.test(userid)) {
                    var s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    var ct = ["10", "11", "12", "13", "14", "15", "16", "17", "34", "18", "19", "20", "21",
                        "22", "35", "23", "24", "25", "26", "27", "28", "29", "32", "30", "31", "33"
                    ];
                    var i = s.indexOf(userid.charAt(0));
                    var tempuserid = ct[i] + userid.substr(1, 9);
                    var num = tempuserid.charAt(0) * 1;
                    for (i = 1; i <= 9; i++) {
                        num = num + tempuserid.charAt(i) * (10 - i);
                    }
                    num += tempuserid.charAt(10) * 1;
                    return num % 10 == 0;
                } else {
                    return false;
                }
            };

            $("#form").validate({
                rules: {
                    gender: "required",

                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        matches: "^09\\d{8}$",
                    },
                    account: {
                        required: true,
                        reg: /^([a-zA-Z0-9]+)$/,
                        rangelength: [6, 15],
                    },
                    password: {
                        required: true,
                        reg: /^([a-zA-Z0-9]+)$/,
                        rangelength: [6, 12],
                    },
                    re_password: {
                        required: true,
                        reg: /^([a-zA-Z0-9]+)$/,
                        rangelength: [6, 12],
                        equalTo: "#password",
                    },

                    id_number: {
                        remote: "app/controller/Member.php?method=idcardCheck&idcardType=<?php echo $_GET["idcardType"] ?>",
                    },
                },
                messages: {
                    gender: "請選擇性別",
                    email: "請輸入正確的Email",
                    phone: "請輸入正確的手機號碼",

                    account: "請輸入6-12位英數帳號",
                    password: "請輸入6-12位英數帳號",
                    re_password: {
                        required: "請輸入6-12位英數帳號",
                        equalTo: "與設定密碼不一致",
                    },
                    id_number: {
                        remote: "居留證證號錯誤",
                    },
                },

                submitHandler: function(form) {

                    if (!$("#isCheck").prop("checked")) {
                        alert("尚未同意條款。", "error");
                        return;
                    }

                    var date = $("#date").val().split('/');

                    var params = {
                        "m_mobile": $("#phone").val(),
                        "m_account": $("#account").val(),
                        "m_passwd": $("#password").val(),
                        "m_name": $("#name").val(),
                        "m_idcard": $("#id_number").val(),
                        "m_email": $("#email").val(),
                        "m_birth_year": date[0],
                        "m_birth_month": date[1],
                        "m_birth_day": date[2],
                        "m_edm": $("#m_edm").prop("checked") ? "Y" : "N",
                        "m_gender": $("#gender").val(),
                        "m_store": "",
                        "m_google_id": "",
                        "m_line_id": "",
                        "m_fb_id": "",
                        "m_is_send": $("#m_is_send").prop("checked") ? "Y" : "N",
                        "m_is_change_passwd": "N",
                        "m_invite_code": $("#m_invite_code").val(),

                        "m_addr_city": $("#city").val(),
                        "m_addr_town": $("#town").val(),
                        "m_addr_road": $("#road").val(),
                        "m_addr": $("#addr").val(),
                    };




                    $.get("app/controller/Member.php?method=updMember", params, function(result) {
                        result = JSON.parse(result);
                        if (result.result == "1") {
                            qg('registration_completed', {
                                'user_id': result.m_id
                            });
                            qg('event', 'N_registration_completed');
                            window.location = "index.php?Page=B-2-6";
                        } else {
                            alert(result.message)
                        }
                    });
                }
            });


            $.get("app/controller/Member.php?method=getCity", function(result) {
                result = JSON.parse(result);

                $.each(result.Citys, function(i, data) {
                    $('#city').append($('<option>', {
                        value: data.city_name,
                        text: data.city_name
                    }));
                })
            });

            $('#city').change(function() {
                $('#town').children().remove().end().append('<option value="" selected >請選擇鄉鎮區</option>');
                $('#road').children().remove().end().append('<option value="" selected >請選擇道路名</option>')
                genTown();
            });

            $('#town').change(function() {
                $('#road').children().remove().end().append('<option value="" selected >請選擇道路名</option>')
                genRoad();
            });

        })

        function genTown() {
            var params = {
                "city_name": $('#city').val(),
            };
            $.get("app/controller/Member.php?method=getTown", params, function(result) {
                result = JSON.parse(result);

                $('#town').children().remove().end()
                    .append('<option value="" selected>請選擇鄉鎮區</option>');

                $.each(result.Towns, function(i, data) {
                    $('#town').append($('<option>', {
                        value: data.town_name,
                        text: data.town_name
                    }));
                })
            });
        }

        function genRoad() {
            var params = {
                "city_name": $('#city').val(),
                "town_name": $('#town').val(),
            };
            $.get("app/controller/Member.php?method=getRoad", params, function(result) {
                result = JSON.parse(result);

                $('#road').children().remove().end()
                    .append('<option value="" selected>請選擇道路名</option>');

                $.each(result.Roads, function(i, data) {
                    $('#road').append($('<option>', {
                        value: data.road_name,
                        text: data.road_name
                    }));
                })
            });
        }
    </script>
    <!-- JS Customization -->
    <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php') ?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">立即開卡</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-12">
                                <form id="form">
                                    <div class="u-text-gray-800 u-font-weight-700 u-font-18 u-mb-100">
                                        請填寫以下資料（<span class="u-text-red-formosa">＊</span>為必填）
                                    </div>
                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="required">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon11.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>姓名</h5>
                                        </label>
                                        <input type="text" id="name" name="name" class="l-form-field" placeholder="請輸入您的大名" required>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="">
                                                <!-- <div class="p-titleIcon">
                                                    <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon12.svg" alt="">
                                                </div> -->
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>出生日期</h5>
                                            </label>
                                            <input type="text" readonly id="date" class="l-form-field" value="1990/01/01">
                                        </div>
                                        <div class="col-md-6 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="">
                                                <!-- <div class="p-titleIcon">
                                                    <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon13.svg" alt="">
                                                </div> -->
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>性別</h5>
                                            </label>
                                            <select type="select" name="gender" id="gender" class="l-form-field l-form-select">
                                                <option value="" selected disabled>請輸入性別</option>
                                                <option value="M">男性</option>
                                                <option value="F">女性</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="required">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon16.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>E-mail</h5>
                                        </label>
                                        <input type="text" name="email" id="email" class="l-form-field" placeholder="請輸入您常用E-mail">
                                    </div>

                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="required">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon11.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>設定帳號</h5>
                                        </label>
                                        <input type="text" id="account" name="account" class="l-form-field" placeholder="請輸入6-15位英數帳號">
                                    </div>

                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="password">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon17.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>設定密碼</h5>
                                        </label>
                                        <input type="password" id="password" name="password" class="l-form-field" placeholder="請輸入6-12位英數密碼">
                                    </div>

                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="password">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon17.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>確認密碼</h5>
                                        </label>
                                        <input type="password" id="re_password" name="re_password" class="l-form-field" placeholder="請再次輸入設定的密碼">
                                    </div>


                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="required">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon18.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">居留證證號</h5>
                                        </label>
                                        <input type="text" id="id_number" name="id_number" class="l-form-field" placeholder="請輸入您的居留證證號">
                                    </div>
                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon14.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>手機號碼</h5>
                                        </label>
                                        <input type="text" name="phone" id="phone" class="l-form-field" placeholder="請輸入您的手機號碼">
                                    </div>
                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon19.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">聯絡地址</h5>
                                        </label>
                                        <div class="row">
                                            <div class="col-md-3 col-6 u-mb-100">
                                                <select type="select" id="city" class="l-form-field l-form-select">
                                                    <option value="" selected disabled>請選擇縣市</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <select type="select" id="town" class="l-form-field l-form-select">
                                                    <option value="" selected disabled>請選擇鄉鎮區</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 col-12 u-mb-100">
                                                <select type="road" id="road" class="l-form-field l-form-select">
                                                    <option value="" selected disabled>請選擇道路名</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" id="addr" class="l-form-field" placeholder="請輸入地址">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="u-mb-200">
                                        <label class="d-flex align-items-center u-mb-075" for="required">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon20.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">好友推薦碼</h5>
                                        </label>
                                        <input type="text" id="m_invite_code" class="l-form-field" placeholder="請輸入推薦碼">
                                    </div>

                                    <div class="u-mb-200 u-text-gray-800">
                                        <div class="form-check u-mb-100">
                                            <input class="form-check-input" type="checkbox" value="Y" id="m_edm" checked>
                                            <label class="form-check-label" for="">
                                                同意收到 寶島眼鏡會員電子報
                                            </label>
                                        </div>
                                        <div class="form-check u-mb-100">
                                            <input class="form-check-input" type="checkbox" value="Y" id="m_is_send" checked>
                                            <label class="form-check-label" for="">
                                                同意收到 簡訊訊息通知
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="Y" id="isCheck" checked>
                                            <label class="form-check-label" for="">
                                                同意 <span class="u-text-blue-highlight">會員權益</span>及<span class="u-text-blue-highlight">隱私權條款</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div>
                                        <input id="btnSend" class="c-btn c-btn--contained c-btn-blue-highlight col" type="submit" value="確認送出">
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
        <!-- =============end footer ============= -->
    </div>

</body>

</html>