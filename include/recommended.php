<?php require_once('../Connections/MySQL.php'); ?>
<? 
$template="news01.php";
$templateS="news01";
// 判斷是預設是A版還是S版
// 預設 A 版
if( $_GET[$templateS]=="" and $row_template['Extend']=="Y") {
    //A 版開始 demo page
    if( $_GET['demo']=='y'  ) { ?>
       <!-- //============= recommended 主要內容區 ============= -->
        <main>
            <section>
                <div class="container u-py-200">
                    <h3 class='c-title-center u-mb-225'>人氣推薦</h3>
                    <div class="row justify-content-end u-pb-200">
                        <span class="align-self-center d-none d-md-inline-block">系列活動：</span>
                        <div class="col-md-4">
                            <select name="select" id="" class="l-form-field l-form-select u-bg-gray-200">
                                <option value="default" selected disabled>全部活動</option>
                                <option value="">迪士尼幻想視界系列</option>
                                <option value="">迪士尼粉萌派對系列</option>
                                <option value="">寶島眼鏡xSOU‧SOU系列 記者會體驗文</option>
                                <option value="">寶島眼鏡X故宮 大清宮逗系列</option>
                                <option value="">K-DESIGN KREATE系列</option>
                                <option value="">寶島眼鏡十倍振興券</option>
                                <option value="">新年好運鼠不盡 米奇系列鏡框</option>
                                <option value="">SOU・SOU獨家聯名鏡框</option>
                            </select>
                        </div>
                    </div>
                    <!-- context -->
                    <div class="u-pb-200">
                        <div class="row">
                            <!-- loop here -->
                            <div class="col-md-3">
                                <div class="c-card c-card-link">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio">
                                            <img src="assets/img/recommended/recommended_01.jpg" alt=""
                                                class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body c-card-body__height-adjust">
                                            <div class="c-card-body-container">
                                                <div class="c-card-body-layout">
                                                    <div class="d-flex mb-1">
                                                        <h4 class="c-card-body-title mb-0">Kimi</h4>
                                                        <a href="#" class="ml-auto">
                                                            <img src="assets/img/recommended/icon_facebook.svg" alt=""
                                                                class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <span class="u-font-14 u-text-blue-500 u-font-weight-700">迪士尼幻想視界系列</span>
                                                    <hr class="w-100">
                                                    <div class="c-card-body-content u-text-gray-700 mb-0">
                                                        <span
                                                            class="c-card-body-txt">這次寶島眼鏡和迪士尼聯名的粉萌派對系列眼鏡連我臉型很挑都能輕鬆駕馭不僅修飾臉型，邊框設計也很質感😚</span>
                                                        <span><a href="#">more</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end loop -->
                            <div class="col-md-3">
                                <div class="c-card c-card-link">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio">
                                            <img src="assets/img/recommended/recommended_02.jpg" alt=""
                                                class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body c-card-body__height-adjust">
                                            <div class="c-card-body-container">
                                                <div class="c-card-body-layout">
                                                    <div class="d-flex mb-1">
                                                        <h4 class="c-card-body-title mb-0">Miko</h4>
                                                        <a href="#" class="ml-auto">
                                                            <img src="assets/img/recommended/icon_instagram.svg" alt=""
                                                                class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <span class="u-font-14 u-text-blue-500 u-font-weight-700">迪士尼幻想視界系列</span>
                                                    <hr class="w-100">
                                                    <div class="c-card-body-content u-text-gray-700 mb-0">
                                                        <span class="c-card-body-txt">寶島眼鏡迪士尼粉萌季開跑拉🌸整副配到好 $1980
                                                            起質感的金屬細框除了修飾臉型也增添了幾分氣質</span>
                                                        <span><a href="#">more</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="c-card c-card-link">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio">
                                            <img src="assets/img/recommended/recommended_03.jpg" alt=""
                                                class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body c-card-body__height-adjust">
                                            <div class="c-card-body-container">
                                                <div class="c-card-body-layout">
                                                    <div class="d-flex mb-1">
                                                        <h4 class="c-card-body-title mb-0">韻文</h4>
                                                        <a href="#" class="ml-auto">
                                                            <img src="assets/img/recommended/icon_blog.svg" alt=""
                                                                class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <span class="u-font-14 u-text-blue-500 u-font-weight-700">迪士尼幻想視界系列</span>
                                                    <hr class="w-100">
                                                    <div class="c-card-body-content u-text-gray-700 mb-0">
                                                        <span
                                                            class="c-card-body-txt">這次寶島眼鏡和我愛的迪士尼聯名了😍粉萌季真的太萌這陣子試戴了三款眼鏡～雖然是粉萌季但都蠻百搭的搭配</span>
                                                        <span><a href="#">more</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="c-card c-card-link">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio">
                                            <img src="assets/img/recommended/recommended_04.jpg" alt=""
                                                class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body c-card-body__height-adjust">
                                            <div class="c-card-body-container">
                                                <div class="c-card-body-layout">
                                                    <div class="d-flex mb-1">
                                                        <h4 class="c-card-body-title mb-0">Kimi</h4>
                                                        <a href="#" class="ml-auto">
                                                            <img src="assets/img/recommended/icon_instagram.svg" alt=""
                                                                class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <span class="u-font-14 u-text-blue-500 u-font-weight-700">迪士尼幻想視界系列</span>
                                                    <hr class="w-100">
                                                    <div class="c-card-body-content u-text-gray-700 mb-0">
                                                        <span
                                                            class="c-card-body-txt">最近寶島眼鏡推出迪士尼粉萌派對系列眼鏡金屬光澤質感超好的，設計也很修飾臉型圓臉跟長臉都適合，鏡架</span>
                                                        <span><a href="#">more</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            
                        </div>
                    </div>
                <!-- // context -->
                   <!-- pagination -->
                    <nav>
                        <ul class="c-pagination justify-content-center align-items-center">
                            <li class="c-pagination-item  align-self-center">
                                <a href="#" class="py-1 px-2">
                                    <i class="far fa-angle-left"></i>
                                </a>
                            </li>
                            <li class="c-pagination-item"><a href="#">1</a></li>
                            <li class="c-pagination-item">...</li>
                            <li class="c-pagination-item"><a href="#">10</a></li>
                            <li class="c-pagination-item py-1 px-2 align-self-center">
                                <a href="#" class="py-1 px-2">
                                    <i class="far fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                <!-- // pagination -->
                </div>
            </section>
        </main>
        <? } else {  ?> 
            <?
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_content01 = $row_template['MaxRow'];
$pageNum_content01 = 0;
if (isset($_GET['pageNum_content01'])) {
  $pageNum_content01 = $_GET['pageNum_content01'];
}
$startRow_content01 = $pageNum_content01 * $maxRows_content01;

$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ",$Table_ID, GetSQLValueString($colname_content01, "text"));
if($_GET['Cate01']<>""){
$query_content01=$query_content01."AND Cate01='".$_GET['Cate01']."'" ;}

if($_GET['Cate02']<>""){
$query_content01=$query_content01."AND Cate02='".$_GET['Cate02']."'" ;}

//排序
$query_content01=$query_content01." order by d01 desc ";

$query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
//echo $query_limit_content01 ;
$content01 = mysqli_query($MySQL,$query_limit_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
if (isset($_GET['totalRows_content01'])) {
   $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
} else {
  $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
}
$totalPages_content01 = ceil($totalRows_content01/$maxRows_content01)-1;

$queryString_content01 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_content01") == false && 
        stristr($param, "totalRows_content01") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
// A版開始 real page
?>
<!-- //============= recommended 主要內容區 ============= -->
<main>
            <section>
                <div class="container u-py-200">
                    <h3 class='c-title-center u-mb-225'>人氣推薦</h3>
                    <div class="row justify-content-end u-pb-200">
                        <span class="align-self-center d-none d-md-inline-block">系列活動：</span>
                        <div class="col-md-4">
                            <select name="select" id="" class="l-form-field l-form-select u-bg-gray-200">
                                <option value="default" selected disabled>全部活動</option>
                                <option value="">迪士尼幻想視界系列</option>
                                <option value="">迪士尼粉萌派對系列</option>
                                <option value="">寶島眼鏡xSOU‧SOU系列 記者會體驗文</option>
                                <option value="">寶島眼鏡X故宮 大清宮逗系列</option>
                                <option value="">K-DESIGN KREATE系列</option>
                                <option value="">寶島眼鏡十倍振興券</option>
                                <option value="">新年好運鼠不盡 米奇系列鏡框</option>
                                <option value="">SOU・SOU獨家聯名鏡框</option>
                            </select>
                        </div>
                    </div>
                    <!-- context -->
                    <div class="u-pb-200">
                        <div class="row">
                            <!-- loop here -->
                            <div class="col-md-3">
                                <div class="c-card c-card-link">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio">
                                            <img src="assets/img/recommended/recommended_01.jpg" alt=""
                                                class="c-card-head-img">
                                        </div>
                                        <div class="c-card-body c-card-body__height-adjust">
                                            <div class="c-card-body-container">
                                                <div class="c-card-body-layout">
                                                    <div class="d-flex mb-1">
                                                        <h4 class="c-card-body-title mb-0">Kimi</h4>
                                                        <a href="#" class="ml-auto">
                                                            <img src="assets/img/recommended/icon_facebook.svg" alt=""
                                                                class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <span class="u-font-14 u-text-blue-500 u-font-weight-700">迪士尼幻想視界系列</span>
                                                    <hr class="w-100">
                                                    <div class="c-card-body-content u-text-gray-700 mb-0">
                                                        <span
                                                            class="c-card-body-txt">這次寶島眼鏡和迪士尼聯名的粉萌派對系列眼鏡連我臉型很挑都能輕鬆駕馭不僅修飾臉型，邊框設計也很質感😚</span>
                                                        <span><a href="#">more</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end loop -->
                        </div>
                    </div>
                <!-- // context -->
                   <!-- pagination -->
                    <nav>
                        <ul class="c-pagination justify-content-center align-items-center">
                            <li class="c-pagination-item  align-self-center">
                                <a href="#" class="py-1 px-2">
                                    <i class="far fa-angle-left"></i>
                                </a>
                            </li>
                            <li class="c-pagination-item"><a href="#">1</a></li>
                            <li class="c-pagination-item">...</li>
                            <li class="c-pagination-item"><a href="#">10</a></li>
                            <li class="c-pagination-item py-1 px-2 align-self-center">
                                <a href="#" class="py-1 px-2">
                                    <i class="far fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                <!-- // pagination -->
                </div>
            </section>
        </main>

<? 
mysqli_free_result($content01);
 } 
//A 版結束 
}
else {  //S版開始 
    if( $_GET['demo']=='y'  ) { ?>
<div>S版 demo</div>
<? } else {  ?>
<?
$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$KeyID=str_replace($vowels ,"**!!!**",$_GET[$templateS]);
if($row_template['Extend']=='Y'){
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ",$Table_ID, GetSQLValueString($colname_content01, "text")); }
else {
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ",$Table_ID, GetSQLValueString($colname_content01, "text")); 	
}

$content01 = mysqli_query($MySQL,$query_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
$totalRows_content01 = mysqli_num_rows($content01);
//echo "S版啟動".$query_content01;
?>    
<div>S版real</div>
 
 <? mysqli_free_result($content01);
 } 
}  //S版結束
?>
       