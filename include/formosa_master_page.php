<?php

namespace app\util;

include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";
?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $row_template['Title']; ?>| 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="<?php echo $row_template['Topic']; ?>">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    
    
     <!-- JS Global Compulsory -->
        <script src="assets/plugins/jquery@3.5.1.min.js"></script>
        <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
        <script src="assets/plugins/aos/aos.js"></script>
        <script src="assets/plugins/slick@1.90/slick.min.js"></script>
        <script src="assets/plugins/lozad/lozad.min.js"></script>
        <script nomodule
            src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver">
        </script>

        <!-- for page=1-1 jQuery Include -->
        <script src="assets/plugins/zepto.js"></script>
        <!-- Mobiscroll JS-->
        <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
        <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
        <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
        <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

        <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
        <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
        <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

        <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
        <!-- End dateMobiscroll JS for page=1-1-->


        <script src="assets/js/main.js"></script>
    
    <?php require_once '../lib/BBlib.php';?>
    <!-- appier -->
    <script type="text/javascript">
    ! function(q, g, r, a, p, h, js) {
        if (q.qg) return;
        js = q.qg = function() {
            js.callmethod ? js.callmethod.call(js, arguments) : js.queue.push(arguments);
        };
        js.queue = [];
        p = g.createElement(r);
        p.async = !0;
        p.src = a;
        h = g.getElementsByTagName(r)[0];
        h.parentNode.insertBefore(p, h);
    }(window, document, 'script', 'https://cdn.qgr.ph/qgraph.<?php echo Config::get("appier_id"); ?>.js');
    </script>
    <!-- appier -->
    <?php include "ga_codes_header.php"?>
</head>

<body>
    <?php include "ga_codes_body.php"?>
    <?php include "formosa_loading.php";?>  
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <?php if ($_GET['Page'] == "6-G") {?>
    <div id="page" class="h-100">
        <?php } else {?>
        <div id="page">
            <?php }?>
            <!-- ============= 導覽列 ============= -->
            <!-- <header id="header" class="l-header"> -->
            <header class="l-header">
                <?php include 'formosa_header.php'?>
            </header>
            <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
            <!-- =============end 導覽列 ============= -->
            <!-- ============= 主要內容區 ============= -->
                <!-- start main content -->
                <?php if ($row_template['TP1'] != "") {include $row_template['TP1'];}?>
                <?php if ($row_template['TP2'] != "") {include $row_template['TP2'];}?>
                <?php if ($row_template['TP3'] != "") {include $row_template['TP3'];}?>
                <?php if ($row_template['TP4'] != "") {include $row_template['TP4'];}?>
                <!-- end main content -->
            <!-- =============end 主要內容區 ============= -->
            <!-- ============= footer ============= -->
            <!-- <footer id="footer" class="l-footer"> -->
            <footer class="l-footer">
                <?php include 'formosa_footer.php'?>
            </footer>
            <!-- =============end footer ============= -->
        </div>
       
        <script type="text/javascript">
        function productslide01(l01) {
            alert('AAA' + l01);
            $.get("productslide01.php?l01=" + l01, function(data, status) {
                document.getElementById("slick_ad").innerHTML = data;
                alert("Data: " + data + "\nStatus: " + status);
            });
        }
        </script>
        <!-- JS Customization -->
</body>

</html>