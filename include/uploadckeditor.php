<?php  

	$File_Extension = strtolower(pathinfo($_FILES["upload"]["name"], PATHINFO_EXTENSION));

	$newfilename=  uniqid(mt_rand()).".".$File_Extension;	

	move_uploaded_file($_FILES["upload"]["tmp_name"],"img/" . $newfilename);

	$funcNum =isset($_GET['CKEditorFuncNum']) ? $_GET['CKEditorFuncNum'] :1 ;
 
	echo json_encode([
		 "uploaded"=>   $funcNum,
		 "fileName"=>  $newfilename,
		 "url"=>  "img/$newfilename" 
	]);
?>
 