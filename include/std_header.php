<link rel="icon" href="favicon.ico" mce_href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" mce_href="favicon.ico" type="image/x-icon">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $Title.":".$KeyWord; ?></title>
<!-- SEO 專區 -->
<meta http-equiv="content-language" content="zh-tw">
<meta name="robots" content="index,follow,all" />
<meta name="author" content="<?php echo $CustomerName; ?>"  />
<meta name="description" content="<?php echo $description; ?>"  />
<meta name="keywords" content="<?php echo $KeyWord; ?>"  />

<meta property='og:title' content='<?php echo $Title.":".$KeyWord; ?>' />
<meta property="og:type" content="article" />
<meta property="og:url" content="http://<?php echo str_replace($vowels2 ,"",$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']); ?>" />
<meta property='og:description' content='<?php echo $description; ?>' />
<meta property='og:image' content='<?php echo $fbimage; ?>' />
<!-- End SEO -->
<?php require_once('../lib/BBlib.php'); ?>



