	<style>
		/*對左側menu層設置*/
		#LeftMenu {
		  font-family:Arial;
		  font-size:14px;
		  width:120px; 
		  padding:8px; 
		  background:#ccc;
		  margin:0 auto;
		  border:1px solid #ccc;
		  }
		
		/*設置功能表選項*/
		#LeftMenu a, #LeftMenu a:visited {
		  display:block; 
		  background-color:#fff; 
		  padding:4px 8px;
		  color:#000; 
		  text-decoration:none;
		  margin:8px 0; 
		  border-left:8px solid #9ab; 
		  border-right:8px solid #9ab; 
		  
		  }
		#LeftMenu a:hover {
		  color:#f00; 
		  border-left:8px solid #000; 
		  border-right:8px solid #000; 
		  }

 		#LeftMenu a#first, #LeftMenu a#last {
		  margin:0;
		}  
		
#TopMenu {
		font-family:Arial;
		font-size:14px;
		margin:0 auto 0 0;
		background:url(images/under.gif);
	}
		 
	#TopMenu ul {
		display:block;
		width:500px;
		height:35px;
		padding:0 0 0 8px;
		margin:0;
		list-style:none;
	}
		  
	#TopMenu ul li {
		  float:left;
	}

	#TopMenu ul li a{
		  display:block;
		  float:left; 
  		  line-height:35px; 
		  color:#ddd;
		  text-decoration:none;
		  padding:0 0 0 14px;
		  }

	#TopMenu ul li a:hover{
		  color:#fff; 
   		  background: url(images/hover.gif);
   		  }
		  
	#TopMenu ul li a b{
		  display:block;
		  padding:0 14px 0 0;
   		  }
		  
	#TopMenu ul li a:hover b{
		  color:#fff; 
   		  background: url(images/hover.gif) no-repeat right top;
   		  }		

    </style>