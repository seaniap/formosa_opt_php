<?php require_once('../Connections/MySQL.php'); ?>
<? 
$template="news02.php";
$templateS="news02";
// 判斷是預設是A版還是S版
// 預設 A 版

if( $_GET[$templateS]=="" and $row_template['Extend']=="Y") {
//A 版開始
if( $_GET['demo']=='y'  ) { ?>

<section class="container">
            <div class="d-title">
                <h3>最新消息</h3>
            </div>
            <div class="block-list">
                <!-- 180619 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/1600X760.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>上洋進駐2018國際飯店暨餐飲設備用品展 ! 6/27~30，09:00-17:00，在台北世貿展覽館一館1F，B區120號攤位，歡迎參觀 !</h4>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-06-20</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-180619.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 180619 end  -->
                <!-- 0601 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180601.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>買三菱重工空調，抽日本科技二人行！</h4>
                        <p>凡於活動期間購買三菱重工空調室外機任乙台(不限金額)，並於活動期限內至「上洋產品保固網」https://warranty.upyoung.com.tw/ 登錄「室外機機號」(每登錄乙台室外機機號等同乙次抽獎資格)，即有機會獲得「台北-東京來回機票」及「三菱港未來技術館入場券」。</p>
                        <span class="time">
                                <i class="far fa-clock"></i>2018-06-01</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0601.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 0601p2 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180601p2.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>尋找阿部寬大作戰，拍照上傳FB，科技好禮送給你！</h4>
                        <p>到上洋簽約的三菱重工空調經銷商，與阿部寬旗幟、人形立牌合照，就有機會抽到輕薄行動電源</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-06-01</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0601p2.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 0516 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180516.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>阿部寬到你家 得獎公布</h4>
                        <p>感謝熱情粉絲們分享阿部寬，得獎名單公布</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-05-16</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0516.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 0514 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180514.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>上洋堅持做對的事</h4>
                        <p>重視經銷夥伴，提供貼心服務，扮演強大後盾，一直是上洋公司秉持之信念。</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-05-14</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0514.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 0510 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180510.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>阿部寬帥氣廣告影片 PART2 曝光</h4>
                        <p>阿部寬內斂演繹三菱重工，是否覺得意猶未盡呢？</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-05-10</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0510.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 0502p2 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180502p2.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>阿部寬到你家，活動熱烈進行中</h4>
                        <p>1、加入「三菱重工空調」FB粉絲專頁</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-05-02</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0502p2.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 0502 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180502.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>早販最後倒數，好評搶購</h4>
                        <p>上洋產業在空調市場以專業技術、耐心傾聽、熱誠服務，深受消費者肯定。</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-05-02</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0502.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 0501 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180501.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>分享阿部寬，好禮帶回家！得獎公布</h4>
                        <p>感謝熱情粉絲們分享阿部寬，得獎名單公布</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-05-01</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0501.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 0416 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/20180416.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>分享阿部寬，好禮帶回家</h4>
                        <p>分享阿部寬，好禮帶回家</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-04-16</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-0416.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 51 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/5101.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>網路經銷商授權認證</h4>
                        <p>購買前請認明合格的上洋家電授權認證經銷商，以免影響權益。</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-03-31</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-51.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 50 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/ma_0_1521102719.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>三菱重工空調，精選好禮買就送</h4>
                        <p>極致舒適，即早體驗 ! 獨家好禮，只在上洋 !</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-03-15</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-50.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 49 -->
                <!-- <div class="block-item">
                    <div class="news-banner">
                        <img src="./assets/img/news/test.jpg" alt="">
                        <img src="./assets/img/news/ma_0_1521507689.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>三菱重工空調早販 開跑囉</h4>
                        <p>三菱重工空調感謝媒體記者報導!</p>
                        <span class="time">
              <i class="far fa-clock"></i>2018-03-16</span>
                        <div class="more">
                            <a href="news-inpage/news-inpage-49.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div> -->
                <!-- 48 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/ma_0_1521166508.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>【2018/3/16~19台北電器空調3C影音大展！現場優惠獎不完，獨享買就送！】</h4>
                        <p>買一對一家用空調ZMXT系列，現省$5,000元起 !
                            <br>買一對一家用空調ZRT系列，現省$10,000元起 !
                        </p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-03-16</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-48.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 43 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/ma_0_1520214123.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>三菱重工 空調 新品發表 阿部寬代言創新局</h4>
                        <p>阿部寬 多年演藝淬鍊 完美 呈現 三菱重工空調的力與美</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-02-08</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-43.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 42 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/ma_0_1519977093.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>【代言大猜謎，重工抽驚喜！得獎公佈】</h4>
                        <p>恭喜幸運粉絲猜中三菱重工空調新代言人，獲出席記者會近距離一睹巨星風采 !</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-01-23</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-42.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 41 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/ma_0_1515725013.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>代言大猜謎，重工抽驚喜</h4>
                        <p>猜中三菱重工空調新代言人，就有機會近距離一睹巨星風采!</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2018-01-12</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-41.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 39 -->
                <div class="block-item">
                    <div class="news-banner">
                        <!-- <img src="./assets/img/news/test.jpg" alt=""> -->
                        <img src="../../assets/img/news/ma_0_1513821057.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>中視新聞專訪 – 2017年金商獎優良商人獎</h4>
                        <p>中視新聞台，為「2017金商獎優良商人」獲獎者，製作專訪節目，上洋產業獲得此殊榮，特別訪問執行長 !</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2017-12-16</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-39.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!--  170701 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="../../assets/img/news/170701.jpg" alt="">
                    </div>
                    <div class="detail">
                        <h4>登錄保固，送兩年濾網 !</h4>
                        <p>詳細辦法：1.活動期間：自民國106年7月1日起。</p>
                        <span class="time">
                            <i class="far fa-clock"></i>2017-07-01</span>
                        <div class="more">
                            <a href="../../news-inpage/news-inpage-170701.html" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- end 170701 -->                

            </div>
        </section>

<? } else {  ?>
<?
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_content01 = $row_template['MaxRow'];
$pageNum_content01 = 0;
if (isset($_GET['pageNum_content01'])) {
  $pageNum_content01 = $_GET['pageNum_content01'];
}
$startRow_content01 = $pageNum_content01 * $maxRows_content01;

$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ",$Table_ID, GetSQLValueString($colname_content01, "text"));


if($_COOKIE['Mode']==""){
$query_content01=$query_content01."AND Cate01='Y' AND (now()>=d01 or d01 is NULL) and (now()<=d02 or d02 is NULL )" ;}

if($_GET['Cate01']<>""){
$query_content01=$query_content01."AND Cate01='".$_GET['Cate01']."'" ;}

if($_GET['Cate02']<>""){
$query_content01=$query_content01."AND Cate02='".$_GET['Cate02']."'" ;}

//排序
$query_content01=$query_content01." order by d01 desc ";

$query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
//echo $query_limit_content01 ;
$content01 = mysqli_query($MySQL,$query_limit_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
if (isset($_GET['totalRows_content01'])) {
   $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
} else {
  $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
}
$totalPages_content01 = ceil($totalRows_content01/$maxRows_content01)-1;

$queryString_content01 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_content01") == false && 
        stristr($param, "totalRows_content01") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);

?>





<section class="container">
            <div class="d-title">
                <h3>最新消息</h3>
            </div>
            <div class="block-list">
           
           
           
  <?php do { ?>   
  
    <? if ($row_content01['l01']=="") { 
		$newlink="index.php?Page=".$row_content01['Page']."&".$templateS."=".$row_content01['KeyID'];
		$target="";
		}else {
		$ext =" (".end(explode('.', $row_content01['l01'])).")";
		$newlink=$row_content01['l01'];
		$target="_blank";
		}
		?>          
           
                <!-- 180619 -->
                <div class="block-item">
                    <div class="news-banner">
                        <img src="UploadImages/<? echo $row_content01['p01'];  ?>" alt=""  >
                    </div>
                    <div class="detail">
                        <h4><? echo $row_content01['h01']; ?></h4>  <?php   include("3buttonA.php");  ?>  
                         <p><? echo $row_content01['h03']; ?></p>
                         
                        <span class="time">
                            <i class="far fa-clock"></i> <? if ($row_content01['d01']<>"") {echo substr($row_content01['d01'],0,10);}  ?></span>
                        <div class="more">
                            <a href="<? echo $newlink; ?>" class="btn btn-bor-yellow">more</a>
                        </div>
                    </div>
                </div>
                <!-- 180619 end  -->
     <?  } while ($row_content01 = mysqli_fetch_assoc($content01)); ?>                          

            </div>
        </section>
          		    
    <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                      
                      
                      <?php if ($pageNum_content01 > 0) { // Show if not first page ?>
                      <li><a href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, max(0, $pageNum_content01 - 1), $queryString_content01); ?>"><i class="fas fa-arrow-left"></i></a></li>
                       <?php } // Show if not first page ?>
                      
                      
                      <?php if($totalPages_content01>0) { // 如果總頁數大於1	
	for($i=-4;$i<=5;$i++) {   
	
	     if(($_GET['pageNum_content01']+$i)>=0 and ($_GET['pageNum_content01']+$i)<= $totalPages_content01)
    {
	?>
                      
             
                      <li><a href="<?php printf("%s?pageNum_content01=%d%s", $currentPage,$_GET['pageNum_content01']+$i , $queryString_content01); ?>" <? if($i==0){ ?>class="active" <? } ?>><? printf("%d ",$_GET['pageNum_content01']+$i+1); ?></a></li>
                        
          <?php
		   }
		  } 
		} ?>	             
                      
                   
                      
                      
                      
                      <?php if ($pageNum_content01 < $totalPages_content01) { // Show if not last page ?>         
                      <li><a href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, min($totalPages_content01, $pageNum_content01 + 1), $queryString_content01); ?>"><i class="fas fa-arrow-right"></i></a></li>
                       <?php } // Show if not last page ?>
                    </ul>
                  </nav>

<? 
mysqli_free_result($content01);
 } 
 
//A 版結束 
} 
else {  //S版開始
 
if( $_GET['demo']=='y'  ) { ?>

 <section class="container">
      <div class="d-title">
        <h3>最新消息</h3>
      </div>
      <div class="news-jumbotron mb-5">
        <img src="./../assets/img/news/news-default.jpg" alt="" class="img-fluid news-inpage-img">
      </div>
      <div class="news-text mb-5">
        <div class="news-title">
          <h4 class="bar-y-title">news-default</h4>
          <span class="time">
            <i class="far fa-clock"></i>2018-03-31</span>
            <!-- <div class="share-link"><a target="_blank" href="http://www.facebook.com/share.php?u=http://demo.ai-ad.com.tw/2018/Heavyduty/news-inpage.html"><i class="fab fa-facebook-f"></i></a></div> -->
        </div>
        
        <p>
          news-default
        </p>

      </div>
      <a href="/webtest2018/news.html" role="button" class="btn-bor-white btn-back">
        <i class="fas fa-angle-left"></i>返回</a>
    </section>

<? } else {  ?>
<?
$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$KeyID=str_replace($vowels ,"**!!!**",$_GET[$templateS]);
if($row_template['Extend']=='Y'){
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ",$Table_ID, GetSQLValueString($colname_content01, "text")); }
else {
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ",$Table_ID, GetSQLValueString($colname_content01, "text")); 	
}

$content01 = mysqli_query($MySQL,$query_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
$totalRows_content01 = mysqli_num_rows($content01);
//echo "S版啟動".$query_content01;
?>



 <section class="container">
      <div class="d-title">
        <h3>最新消息</h3>
      </div>
      <div class="news-jumbotron mb-5">
        <img src="UploadImages/<? echo $row_content01['p01'];  ?>" alt="" class="img-fluid news-inpage-img"  >
      </div>
      <div class="news-text mb-5">
        <div class="news-title">
          <h4 class="bar-y-title"><? echo $row_content01['h01'];  ?></h4>
          <span class="time">
            <i class="far fa-clock"></i><? if ($row_content01['d01']<>"") {echo substr($row_content01['d01'],0,10);}  ?></span>
            <!-- <div class="share-link"><a target="_blank" href="http://www.facebook.com/share.php?u=http://demo.ai-ad.com.tw/2018/Heavyduty/news-inpage.html"><i class="fab fa-facebook-f"></i></a></div> -->
        </div>
        
        <p>
         <? echo $row_content01['c01'];  ?>
        </p>

      </div>
      <a href="index.php?Page=2" role="button" class="btn-bor-white btn-back">
        <i class="fas fa-angle-left"></i>返回</a>
    </section>






<? mysqli_free_result($content01);
 } 
}  //S版結束
 ?>