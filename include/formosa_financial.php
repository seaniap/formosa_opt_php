<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>網頁名稱</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="描述">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header id="header" class="l-header"></header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main>
            <section>
                <div class="container u-py-200">
                    <h3 class='c-title-center u-mb-225'>財務報告</h3>
                    <div class="c-accordion u-mb-200">
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent1">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">109年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon js-accordionExpended">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent1" class="c-accordion-content js-accordionExpended">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島109_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島109_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島109_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島109_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島109_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島109_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島109_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent2">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">108年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent2" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島108_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島108_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島108_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島108_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島108_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島108_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島108_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent3">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">107年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent3" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島107_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島107_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島107_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島107_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島107_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島107_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島107_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent4">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">106年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent4" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島106_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島106_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島106_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島106_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島106_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島106_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島106_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent5">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">105年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent5" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島105_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島105_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島105_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島105_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島105_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島105_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島105_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent6">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">104年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent6" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島104_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島104_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島104_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島104_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島104_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島104_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島104_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent7">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">103年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent7" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島103_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島103_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島103_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島103_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島103_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島103_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島103_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent8">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">102年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent8" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島102_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島102_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島102_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島102_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島102_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島102_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島102_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent9">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">101年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent9" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島101_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島101_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島101_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島101_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島101_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島101_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島101_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                        <!-- group -->
                        <button class="c-accordion-btn" data-target="#accordionContent10">
                            <div class="c-accordion-btn-wrapper">
                                <div class="c-accordion-btn-layout">

                                    <div class="c-accordion-btn-title">
                                        <p class="u-font-18">100年<span
                                                class="c-accordion-btn-title-seperator"></span>財務報告</p>
                                    </div>
                                    <div class="c-accordion-btn-icon">
                                        <i class="far fa-angle-down u-font-20"></i>
                                    </div>
                                </div>
                            </div>
                        </button>
                        <div id="accordionContent10" class="c-accordion-content">
                            <div class="c-accordion-content-wrapper">
                                <div class="c-accordion-content-layout">
                                    <div class="row">
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島100_Q1C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島100_Q2C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島100_Q3C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島100_Q4</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島100_Q4C</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島100_Q4E</span>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-2">
                                            <a href="#"
                                                class="c-btn c-btn--outlined c-btn-gray-800 u-text-black w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                <img src="assets/img/financial/icon_pdf.svg" alt="" class="u-mr-050">
                                                <span>寶島100_Q4EC</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End group -->
                    </div>
                    <nav>
                        <ul class="c-pagination justify-content-center align-items-center">
                            <li class="c-pagination-item  align-self-center">
                                <a href="#" class="py-1 px-2">
                                    <i class="far fa-angle-left"></i>
                                </a>
                            </li>
                            <li class="c-pagination-item"><a href="#">1</a></li>
                            <li class="c-pagination-item">...</li>
                            <li class="c-pagination-item"><a href="#">10</a></li>
                            <li class="c-pagination-item py-1 px-2 align-self-center">
                                <a href="#" class="py-1 px-2">
                                    <i class="far fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer">
            <?php include('formosa_footer.php') ?>
        </footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>