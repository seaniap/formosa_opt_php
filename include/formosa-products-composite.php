    <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">複合式專區</h3>
                        <p class="u-mb-125 text-center">複合式專區擁有各類商品，隨時滿足您的需求。</p>
                    </div>
                </div>
                <div class="u-pt-100 u-pb-250">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-10 col-sm-8 col-md-12 col-lg-10">
                                <div class="row align-items-start js-cardJustifyContent">
                                    <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                        <a target="_blank" href="https://www.eyesmart.com.tw/link/6270/?utm_source=OfficialWebsite&utm_medium=product&utm_campaign=compound&utm_term=Lutein">
                                            <div class="p-iconBg02 u-mb-100">
                                                <img src="assets/img/products/icon_lutien.svg" alt="" class="img-fluid">
                                            </div>
                                            <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">葉黃素</h5>
                                            <p class="u-text-gray-800 u-mb-000 u-font-12 u-md-font-16">
                                                <span>提供最佳品質的</span><br>
                                                <span>專利葉黃素</span>
                                            </p>
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                        <a target="_blank" href="https://www.eyesmart.com.tw/link/6271/?utm_source=OfficialWebsite&utm_medium=product&utm_campaign=compound&utm_term=Facialmask">
                                            <div class="p-iconBg02 u-mb-100">
                                                <img src="assets/img/products/icon_mask.svg" alt="" class="img-fluid">
                                            </div>
                                            <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">面膜</h5>
                                            <p class="u-text-gray-800 u-mb-000 u-font-12 u-md-font-16">
                                                <span>高浸潤、高保濕、高滲透</span><br>
                                                <span>無多餘添加</span>
                                            </p>
                                        </a>
                                    </div>
                                    <!-- <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                        <a target="_blank" href="members-register-citizen-mobilePhone.html">
                                            <div class="p-iconBg02 u-mb-100">
                                                <img src="assets/img/products/icon_bottle.svg" alt="" class="img-fluid">
                                            </div>
                                            <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">水</h5>
                                            <p class="u-text-gray-800 u-mb-000 u-font-12 u-md-font-16">
                                                <span>萃取一點一滴用好水</span><br>
                                                <span>來儲蓄你每日的健康</span>
                                            </p>
                                        </a>
                                    </div> -->
                                    <!-- <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                        <a target="_blank" href="members-realize.html">
                                            <div class="p-iconBg02 u-mb-100">
                                                <img src="assets/img/products/icon_hearingAid.svg" alt="" class="img-fluid">
                                            </div>
                                            <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">助聽器</h5>
                                            <p class="u-text-gray-800 u-mb-000 u-font-12 u-md-font-16">
                                                <span>增進聆聽品質</span><br>
                                                <span>體驗更清晰舒適的生活</span>
                                            </p>
                                        </a>
                                    </div> -->
                                    <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                        <a target="_blank" href="https://www.eyesmart.com.tw/link/6272/?utm_source=OfficialWebsite&utm_medium=product&utm_campaign=compound&utm_term=Ultrasound">
                                            <div class="p-iconBg02 u-mb-100">
                                                <img src="assets/img/products/icon_ultrasound.svg" alt="" class="img-fluid">
                                            </div>
                                            <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">超音波清洗機</h5>
                                            <p class="u-text-gray-800 u-mb-000 u-font-12 u-md-font-16">
                                                <span>百款超音波清洗機型</span><br>
                                                <span>任你挑選</span>
                                            </p>
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                        <a target="_blank" href="https://www.eyesmart.com.tw/link/6273/?utm_source=OfficialWebsite&utm_medium=product&utm_campaign=compound&utm_term=brandedproducts">
                                            <div class="p-iconBg02 u-mb-100">
                                                <img src="assets/img/products/icon_littleGoods.svg" alt="" class="img-fluid">
                                            </div>
                                            <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">聯名週邊小物</h5>
                                            <p class="u-text-gray-800 u-mb-000 u-font-12 u-md-font-16">
                                                <span>頸枕眼罩及水盒</span><br>
                                                <span>等聯名周邊小物</span>
                                            </p>
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                        <a target="_blank" href="https://www.eyesmart.com.tw/link/6274/?utm_source=OfficialWebsite&utm_medium=product&utm_campaign=compound&utm_term=brandedstorage">
                                            <div class="p-iconBg02 u-mb-100">
                                                <img src="assets/img/products/icon_glassesCase.svg" alt="" class="img-fluid">
                                            </div>
                                            <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">聯名眼鏡收納</h5>
                                            <p class="u-text-gray-800 u-mb-000 u-font-12 u-md-font-16">
                                                <span>EYESmart獨家販售!</span><br>
                                                <span>聯名眼鏡盒、眼鏡包</span>
                                            </p>
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-6 u-mb-150 text-center u-px-075">
                                        <a target="_blank" href="https://www.eyesmart.com.tw/link/6275/?utm_source=OfficialWebsite&utm_medium=product&utm_campaign=compound&utm_term=clean">
                                            <div class="p-iconBg02 u-mb-100">
                                                <img src="assets/img/products/icon_cleaner.svg" alt="" class="img-fluid">
                                            </div>
                                            <h5 class="u-text-blue-highlight u-font-weight-700 u-font-18">清潔收納小物</h5>
                                            <p class="u-text-gray-800 u-mb-000 u-font-12 u-md-font-16">
                                                <span>眼鏡清潔專用噴霧、濕巾</span><br>
                                                <span>及眼鏡收納小物</span>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <!-- =============end 主要內容區 ============= -->