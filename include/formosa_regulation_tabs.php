<!-- START TAB -->
<div class="u-pb-100">
            <div class="container">
                <div class="d-md-flex d-none c-tab-verF row justify-content-center">
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-4" class="c-tab-linkF <?php if($_GET['Page']=="6-4"){ echo 'active';}?>">獨立董事選任資訊</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-5" class="c-tab-linkF <?php if($_GET['Page']=="6-5"){ echo 'active';}?>">董事選任程序</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-D" class="c-tab-linkF <?php if($_GET['Page']=="6-D"){ echo 'active';}?>">董事會</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-6" class="c-tab-linkF <?php if($_GET['Page']=="6-6"){ echo 'active';}?>">公司章程</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-7" class="c-tab-linkF <?php if($_GET['Page']=="6-7"){ echo 'active';}?>">財務規章</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-8" class="c-tab-linkF <?php if($_GET['Page']=="6-8"){ echo 'active';}?>">公司組織架構與經理人之職稱及職權範圍</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-9" class="c-tab-linkF <?php if($_GET['Page']=="6-9"){ echo 'active';}?>">內部稽核之組織及運作</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-A" class="c-tab-linkF <?php if($_GET['Page']=="6-A"){ echo 'active';}?>">其他公司治理規章</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-C" class="c-tab-linkF <?php if($_GET['Page']=="6-C"){ echo 'active';}?>">公司履行誠信經營情形及採行措施</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-B" class="c-tab-linkF <?php if($_GET['Page']=="6-B"){ echo 'active';}?>">公司治理運作情形</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-E" class="c-tab-linkF <?php if($_GET['Page']=="6-E"){ echo 'active';}?>">功能性委員會相關資訊</a>
                    </div>
                </div>
                <div class="row justify-content-center d-block d-md-none">
                    <div class="col-12">
                        <select name="select" id="" class="l-form-field l-form-select u-bg-gray-200 js-selectURL">
                            <option value="default" disabled>治理項目</option>
                            <option value="index.php?Page=6-4" selected>獨立董事選任資訊
                            </option>
                            <option value="index.php?Page=6-5">董事選任程序</option>
                            <option value="index.php?Page=6-D">董事會</option>
                            <option value="index.php?Page=6-6">公司章程</option>
                            <option value="index.php?Page=6-7">財務規章</option>
                            <option value="index.php?Page=6-8">公司組織架構與經理人之職稱及職權範圍</option>
                            <option value="index.php?Page=6-9">內部稽核之組織及運作</option>
                            <option value="index.php?Page=6-A">其他公司治理規章</option>
                            <option value="index.php?Page=6-C">公司履行誠信經營情形及採行措施</option>
                            <option value="index.php?Page=6-B">公司治理運作情形</option>
                            <option value="index.php?Page=6-E">功能性委員會相關資訊</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- END TAB -->