<?php require_once('../Connections/MySQL.php'); ?>
<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   
    <?php 
        $category_id =  isset($_GET['c']) ? $_GET['c'] : 206; 
        switch ($category_id) {
            case "1":            
                print_r(  '<title id="metaTitle">隱形眼鏡透明片 ｜  寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>');
                print_r( '<meta id="description" name="description" content="寶島眼鏡提供嬌生安視優、ACUVUE、愛爾康、酷柏、博士倫、海昌、美若康、SEED、帝康等隱形眼鏡透明片。最優惠的隱形眼鏡價格、最完整的日拋、雙週拋、月拋、季拋、散光、多焦、水膠、矽水膠的隱眼都在寶島眼鏡，滿足您各樣的配戴需求"/>');
                 break;
            case "35":
                print_r( '<title id="metaTitle">隱形眼鏡彩片 ｜  寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>');
                print_r( '<meta id="description" name="description" content="寶島眼鏡提供嬌生安視優、ACUVUE、愛爾康、酷柏、博士倫、海昌、美若康、SEED、帝康、MIMA、Deesse、女神、PLAYBOY、斑比、LENS TOWN、睛靚、亨泰、氧視加等隱形眼鏡彩片。最優惠的隱形眼鏡價格、最完整的日拋、雙週拋、月拋、季拋、散光、多焦的隱眼及放大片、最新的歐美日韓流行趨勢都在寶島眼鏡，滿足您各樣的配戴需求"/>');
                break;
            case "80":
                 print_r( '<title id="metaTitle">太陽眼鏡 ｜  寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>');
                  
                 print_r( '<meta id="description" name="description" content="寶島眼鏡提供時下潮流的Cartier、GUCCI、BURBERRY、COACH、PRADA、VERSACE、PUMA、BMW、K-DESIGN、HORIEN、RayBan雷朋等時尚太陽眼鏡及寶島眼鏡聯名款式墨鏡，除了飛行款太陽眼鏡外，寶島眼鏡也會隨時引進歐美日韓等全球最新的流行趨勢鏡框，也可以透過您的臉型，不論您是圓臉、國字臉、瓜子臉、鵝蛋臉、長形臉、三角形臉，您都可以在這裡挑選到您最適合您的鏡框寶島眼鏡滿足您的個人風格及需求。"/>');
                 break;
            case "206":
                print_r( '<title id="metaTitle">平光眼鏡 ｜  寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>');
                print_r( '<meta id="description" name="description" content="寶島眼鏡提供全球(歐美日韓)時下潮流的Cartier、GUCCI、BURBERRY、COACH、PRADA、VERSACE、PUMA、BMW、K-DESIGN、HORIEN、RayBan雷朋等時尚眼鏡、時尚鏡框及寶島眼鏡聯名款式眼鏡，寶島眼鏡滿足您的個人風格及需求，不論您想要圓框、方框、飛行款、無邊框、多邊框、小圓、大圓、小方、大方，甚至您需要修飾小臉的眼鏡、爆紅的鏡框等，也可以透過您的臉型，不論您是圓臉、國字臉、瓜子臉、鵝蛋臉、長形臉、三角形臉，您都可以在這裡挑選到您最適合您的鏡框，是您配鏡推薦的最佳選擇。"/>');
                break;
        }
    ?>
    
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg"> -->
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="assets/plugins/aos/aos.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <!-- Start dateMobiscroll CSS-->
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />
    <!-- End dateMobiscroll CSS-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!-- <link rel="stylesheet" href="assets/plugins/page/pagination.css" /> -->

    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule
        src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <!-- Start dateMobiscroll JS-->
    <!-- jQuery Include -->
    <script src="assets/plugins/zepto.js"></script>

    <!-- Mobiscroll JS-->
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>

    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <!-- End dateMobiscroll JS-->
    <script src="assets/js/main.js?v=1"></script>

    <script src="assets/plugins/page/pagination.js"></script>
    <script src="assets/plugins/underscore-umd-min.js" type="text/javascript"></script>
    <!-- JS Customization -->

    <!-- JS Customization -->

    <script>
    var category_id = "<?php echo $category_id; ?>";
    var brand_id = "<?php echo isset($_GET['brand']) ? $_GET['brand']:'';?>";
    var category_father_ids = [];

    $(function() {
        init(category_id);

        $("#btnRemove").click(function() {
            category_father_ids.forEach(function(val) {
                $('input[name="category_' + val + '"]').prop('checked', false);
            });

        });

        $("#js-confirm").click(function() {
            getProduct();
        })

        $("#ordreby").change(function() {
            getProduct();
        });

    });

    function init(category_id) {
        var title = "";
        var subtitle = "鏡架款式多樣選擇，提供最適合您的風格";
        var metaTitle = "商品專區 ｜  寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器";
		var description ="";
		
        switch (category_id) {
            case "1":
                title = "隱形眼鏡透明片";
                subtitle = "寶島眼鏡提供各大知名品牌透明隱形眼鏡，滿足您的配戴需求";
                               break;
            case "35":
                title = "隱形眼鏡彩片";
                subtitle = "寶島眼鏡提供各大知名品牌彩色隱形眼鏡，滿足您的配戴需求";
                             break;
            case "80":
                title = "太陽眼鏡";
                            break;
            case "206":
                title = "平光眼鏡";
                              break;
        }

        $("#title").html(title);

        $("#subtitle").html(subtitle);
        
/*
         $("#title").html(title);
        $("#metaTitle").html(metaTitle);
        $("#description").attr("content",metaTitle);
*/
        
        var params = {
            category_id: category_id,
            brand_id: brand_id
        };

        $.get("app/controller/Products.php?method=getCategoryProducts", params, function(result) {
            result = JSON.parse(result);

            //console.log(result);
            var html = "";
            result.category.forEach(function(category) {
                category_father_ids.push(category.category_id);

                var categoryItem = result.categoryItem.filter(function(row) {
                    if (category.category_id == row.category_father_id) {
                        return row;
                    }
                });

                //GEN大類別項目 
                html += `
	            		 <div class="d-flex u-mb-100">
	                     	<h4 class="u-font-18 u-my-025">${category.category_name}</h4>
	            		 	<div class="p-products-search-checkbox col">
                         		<div class="d-flex flex-wrap">
		             `;

                //GEN子類別項目
                categoryItem.forEach(function(categoryItem, index) {
                    html += `
		            		 	<div class="m-1">
                            		<input type="checkbox" name="category_${category.category_id}" id="categoryItme_${categoryItem.category_id}" value="${categoryItem.category_id}" class="l-form-check-input">
                            		<label for="categoryItme_${categoryItem.category_id}" class="l-form-check-label">${categoryItem.category_name}</label>
                     			</div>
		                    `;
                });

                html += `
    	            		  </div>
    	                     </div>
    	                 </div>
		             `;
            });

            $("#category").html(html);
            genProduct(result.products);

            if (brand_id) {
                $("#categoryItme_" + brand_id).prop("checked", true);
            }
        });
    }

    function getProduct() {
        var url = "app/controller/Products.php?method=getProducts";

        //預設類別
        var arr = "";

        category_father_ids.forEach(function(val) {
            var category_ids = $.map($('input[name="category_' + val + '"]:checked'), function(item) {
                return item.value;
            })
            if (category_ids.length > 0) {
                arr += category_ids.join(",") + "|";
            }
        });

        if (arr.length > 0) {
            arr = arr.substring(0, arr.length - 1);
        }


        var params = {
            category_id : category_id,
            category_ids: arr,
            ordreby: $("#ordreby").val(),
        };


        $.get(url, params, function(result) {
            result = JSON.parse(result);
            genProduct(result);
        });
    }

    function genProduct(product) {
        var container = $('#pagination_page');
        container.pagination({
            dataSource: product,
            pageSize: 20,
            callback: function(response, pagination) {
                var compiled = _.template($("#tpl").html());
                var html = compiled({
                    "datas": response
                });
                $("#pagination-data").html(html);
            }
        });
    }
    </script>
    <?php include ("ga_codes_header.php")?>
</head>

<body>
    <?php include ("ga_codes_body.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php')?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper position-relative p-products">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 id="title" class="c-title-center u-mb-125">平光眼鏡</h3>
                        <p id="subtitle" class="text-center">鏡架款式多樣選擇，提供最適合您的風格</p>
                    </div>
                </div>
                <div>
                    <div class="container">
                        <div class="row u-mb-100 justify-content-between">
                            <div class="col-5 col-lg-auto">
                                <button
                                    class="c-btn c-btn--default c-btn--bordered d-block w-100 p-products-search-btn text-left"><i
                                        class="far fa-search mr-2"></i>進階搜尋</button>
                            </div>
                            <div class="col-7 col-lg-auto">
                                <select id="ordreby" class="l-form-field l-form-field-verB l-form-select u-bg-gray-200">
                                    <option value="ar">依AR試戴排序</option>
                                    <option value="hot" selected>依熱門人氣排序</option>
                                </select>
                            </div>
                        </div>
                        <div class="p-products-search">

                            <div class="p-products-search-wrapper u-bg-gray-02">
                                <div
                                    class="p-products-search-header text-center justify-content-center align-items-center">
                                    <button type="button" class="close">
                                        <i class="fal fa-times"></i>
                                    </button>
                                    <p class="mb-0 font-weight-bold u-font-20">進階搜尋</p>
                                </div>
                                <!-- 手機版風琴 電腦版Tab  -->
                                <div id="category" class="p-products-search-content p-3">


                                </div>
                                <!-- End 手機版風琴 電腦版Tab -->
                                <div class="p-products-search-footer my-3 d-flex justify-content-center">
                                    <div class="col-sm-3">
                                        <button class="c-btn c-btn-blue-highlight c-btn--outlined w-100"
                                            id="btnRemove">全部清除</button>
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="c-btn c-btn-blue-highlight c-btn--contained w-100"
                                            id="js-confirm">確定</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="u-pb-100">
                    <div class="container">
                        <div id="pagination-data" class="row"></div>

                        <script type="text/template" id="tpl">
                            <% _.each(datas, function(data){ %>
                             <div class="col-6 col-lg-4 col-xl-3">
                                <div class="c-card c-card-link c-card-verA">
                                    <div class="c-card-layout">
                                        <div class="c-card-head c-card-head-aspect-ratio">
                                            <a target="_blank" href="<%= data.product_url %>" target="_blank"> 
                                                <img src="<%= data.product_image %>" alt="" class="c-card-head-img">
                                            </a>
                                            <% if (data.product_image_label != 0 ) { %>
                                            <img src="assets/img/products/tag_<%= data.product_image_label%>.svg" alt="" class="c-card-head-tag">
                                            <% } %>
                                        </div>
                                        <div class="c-card-body">
                                            <div class="c-card-body-container">
                                                <div class="c-card-body-layout">
                                                <% if(data.product_ar ==1){%>
                                                    <div class="c-card-body-ar u-mb-050">
                                                        <img src="assets/img/products/icon_AR.svg" alt="" class="img-fluid">
                                                    </div>
                                                    <%}%>
                                                    <h5 class="u-font-16 u-font-weight-700 u-mb-075">
                                                        <a target="_blank" href="<%= data.product_url %>" class="u-text-gray-800">
                                                        <%= data.product_name %>
                                                        </a>
                                                    </h5>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <% }) %>
                         </script>

                    </div>
                </div>


                <!-- START 頁籤 -->
                <div class="u-py-100 u-pb-400">
                    <div class="container">
                        <nav>
                            <ul class="c-pagination justify-content-center align-items-center u-mb-000">
                                <div id="pagination_page"></div>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- END 頁籤 -->
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php')?></footer>
        <!-- =============end footer ============= -->
    </div>
</body>

</html>