<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>立即開卡 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="若您已於寶島眼鏡門市加入會員，但沒有帳號密碼，請按照以下步驟完成開卡，即可享有寶島眼鏡會員獨享好康！">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.frame.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/mobiscroll@2.17.3/css/mobiscroll.scroller.css" rel="stylesheet" type="text/css" />

    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.zepto.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.core.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.frame.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.scroller.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.util.datetime.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetimebase.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/i18n/mobiscroll.i18n.zh.js"></script>
    <script src="assets/plugins/mobiscroll@2.17.3/js/mobiscroll.datetime.js"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
    <script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- JS Customization -->

    <script type="text/javascript">
        window.alert = function(msg, icon = "info") {
            Swal.fire({
                text: "" + msg,
                icon: icon,
                confirmButtonColor: '#3085d6',
                confirmButtonText: '確定'
            })
        };

        $(function() {
            $("#form").validate({
                rules: {
                    phoneOrIdCard: "required",
                },
                messages: {
                    phoneOrIdCard: "請輸入您的手機號碼或身分證字號",
                },
                submitHandler: function(form) {

                    var m_mobile = "";
                    var m_idcard = "";
                    if (isNaN($("#phoneOrIdCard").val())) {
                        m_idcard = $("#phoneOrIdCard").val();
                    } else {
                        m_mobile = $("#phoneOrIdCard").val();
                    }

                    var date = $("#date").val().split('/');

                    var params = {
                        "m_mobile": m_mobile,
                        "m_idcard": m_idcard,
                        "m_birth_year": date[0],
                        "m_birth_month": date[1],
                        "m_birth_day": date[2],
                    };


                    $.get("app/controller/Member.php?method=getOpenCardMember", params, function(result) {
                        result = JSON.parse(result);
                        if (result.result == 0) {
                            Swal.fire({
                                html: `
                                     <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
		                             	請確認您輸入的資料是否正確
    		                         </h3>
                                     <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
    		                          	   未曾至門市加入會員，但現在想加入請選擇立即註冊
    		                         </p>
		   	   	                 `,

                                imageUrl: 'assets/img/members/icon23.svg',
                                imageWidth: 70,
                                imageHeight: 70,
                                allowEscapeKey: false,
                                allowOutsideClick: false,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: '我知道了',
                                showCancelButton: true,
                                cancelButtonText: '立即註冊',
                            }).then((result) => {
                                if (result.dismiss == 'cancel') {
                                    window.location = "index.php?Page=B-3";
                                }
                            })
                        } else {
                            //for test
                            //result.m_type=1;

                            if (result.m_account) {
                                Swal.fire({
                                    html: `
                                        <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
			                           	  請檢查資料
			                         </h3>
                                     <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
			                         	    此手機號碼 / 身分證字號及生日已開過卡
			                         </p>
			   	   	                  `,

                                    imageUrl: 'assets/img/members/icon31.svg',
                                    imageWidth: 70,
                                    imageHeight: 70,
                                    allowEscapeKey: false,
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: '我知道了'
                                })
                            } else if (!result.m_card_no) { //沒卡號不能開卡
                               	 Swal.fire({
                                     html: `
                                        <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
    			                           	  請檢查資料
    			                         </h3>
    			                         
                                         <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
    			                         	    無卡號不能開卡
    			                         </p>
    			   	   	                  `,
                                     imageUrl: 'assets/img/members/icon31.svg',
                                     imageWidth: 70,
                                     imageHeight: 70,
                                     allowEscapeKey: false,
                                     allowOutsideClick: false,
                                     confirmButtonColor: '#3085d6',
                                     confirmButtonText: '我知道了'
                                 });
                            } else {
                                window.location = "index.php?Page=B-2-2";
                            }

                        }
                    });
                }
            })
        });
    </script>
   <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>




    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php') ?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">立即開卡</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-12">
                                <form id="form">
                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="required">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon1.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">驗證方式</h5>
                                        </label>
                                        <input type="text" id="phoneOrIdCard" name="phoneOrIdCard" class="l-form-field" placeholder="請輸入您的手機號碼或身分證字號">
                                    </div>

                                    <div class="u-mb-200">
                                        <label class="d-flex align-items-center u-mb-075" for="date">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon2.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">出生日期</h5>
                                        </label>
                                        <input type="text" readonly id="date" class="l-form-field" value="1990/01/01">
                                    </div>

                                    <div>
                                        <button id="btnSend" type="submit" class="c-btn c-btn--contained c-btn-blue-highlight col">
                                            確認送出
                                        </button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
        <!-- =============end footer ============= -->
    </div>

</body>

</html>