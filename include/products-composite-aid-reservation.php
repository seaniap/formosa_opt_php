        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">聽力檢測預約</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8 col-12">
                                <form>
                                    <div class="u-text-gray-800 u-font-weight-700 u-font-18 u-mb-100">
                                        請填寫以下資料（<span class="u-text-red-formosa">＊</span>為必填）
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="required">
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>姓名</h5>
                                            </label>
                                            <input type="text" id="required" class="l-form-field" placeholder="請輸入您的大名">
                                        </div>
                                        <div class="col-md-6 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="titleSelect">
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>稱謂</h5>
                                            </label>
                                            <select type="select" id="titleSelect" class="l-form-field l-form-select">
                                                <option value="default" selected="" disabled="">請選擇稱謂</option>
                                                <option value="mr">先生</option>
                                                <option value="miss">小姐</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="identitySelect">
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>身份</h5>
                                            </label>
                                            <select type="select" id="identitySelect" class="l-form-field l-form-select">
                                                <option value="default" selected="" disabled="">請選擇身份</option>
                                                <option value="myself">聽友本人</option>
                                                <option value="family">聽友家屬</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="phoneNumberInput">
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>聯繫手機</h5>
                                            </label>
                                            <input type="text" id="phoneNumberInput" class="l-form-field" placeholder="請輸入聯繫手機">
                                        </div>
                                    </div>

                                    <div class="u-mb-150">
                                        <label class="d-flex align-items-center u-mb-075" for="emailInput">
                                            <!-- <div class="p-titleIcon">
                                                <img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon16.svg" alt="">
                                            </div> -->
                                            <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">E-mail</h5>
                                        </label>
                                        <input type="text" id="emailInput" class="l-form-field" placeholder="請輸入您常用E-mail">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="reservationItemSelect">
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>預約項目</h5>
                                            </label>
                                            <select type="select" id="reservationItemSelect" class="l-form-field l-form-select">
                                                <option value="default" selected="" disabled="">請選擇預約項目</option>
                                                <option value="test">專業聽力測試</option>
                                                <option value="try">日本理音助聽器機種試戴</option>
                                                <option value="adjust">助聽器調整</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="storeSelect">
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>選擇縣市</h5>
                                            </label>
                                            <select type="select" id="storeSelect" class="l-form-field l-form-select">
                                                <option value="default" selected="" disabled="">請選擇縣市</option>
                                                <option value="store01">縣市01</option>
                                                <option value="store02">縣市02</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row u-mb-200">
                                        <div class="col-md-8 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="storeSelect">
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000"><span class="u-text-red-formosa">＊</span>門市名稱</h5>
                                            </label>
                                            <select type="select" id="storeSelect" class="l-form-field l-form-select">
                                                <option value="default" selected="" disabled="">請選擇門市名稱</option>
                                                <option value="store01">嘉義興業路門市 - 嘉義市西區翠岱里興業西路122-8號1樓</option>
                                                <option value="store02">門市名稱02</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-12 u-mb-150">
                                            <label class="d-flex align-items-center u-mb-075" for="eventCode">
                                                <h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">活動代碼</h5>
                                            </label>
                                            <input type="text" id="eventCode" class="l-form-field" placeholder="請輸入活動代碼">
                                        </div>
                                    </div>
                                    
                                    <div>
                                        <button onclick="location.href='members-openCard-quickLogin.html'" type="button" class="c-btn c-btn--contained c-btn-blue-highlight col">
                                            確認送出
                                        </button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->