<?php require_once('../Connections/MySQL.php'); ?>
<? 
$template="store02.php";
$templateS="store02";
// 判斷是預設是A版還是S版
// 預設 A 版

if( $_GET[$templateS]=="" and $row_template['Extend']=="Y") {
//A 版開始
if( $_GET['demo']=='y'  ) { ?>

 <div class="wrapper">
    <section class="container">
      <div class="d-title">
        <h3>經銷據點</h3>
      </div>
      <div class="row">
        <div class="col-lg-6 form-title">
          <form action="">
            <label for="">尋找距離您最近的據點</label>
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <div class="select-box">
                    <select class="form-control" id="">
                      <option selected="" disabled="">請選擇類型</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <i></i>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="select-box">
                    <select class="form-control" id="">
                      <option selected="" disabled="">請選擇類型</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <i></i>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 mb-5 order-lg-2">
          <div class="loca-list">
            <ul>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>臺北市中山區八德路二段326號</h5>
                  <span>臺北市士林區中山北路六段308號1樓</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>千雅電器有限公司</h5>
                  <span>臺北市士林區中山北路六段308號1樓</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>千雅電器有限公司</h5>
                  <span>臺北市士林區中山北路六段308號1樓</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>千雅電器有限公司</h5>
                  <span>臺北市士林區中山北路六段308號1樓</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
              <li>
                <a href="">
                  <h5>三泰電器(股)公司</h5>
                  <span>臺北市中山區八德路二段326號</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-6 order-lg-1">
          <div class="map-fr">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.611800413124!2d121.54094331500647!3d25.04724498396617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abdbf8a48117%3A0x37e5e3cc07466e57!2zMTA0OTHlj7DljJfluILkuK3lsbHljYDlhavlvrfot6_kuozmrrUzMjbomZ8!5e0!3m2!1szh-TW!2stw!4v1521603850206"
              width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            <div class="information showinto">
              <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <img src="../../assets/img/news/qrcode.jpg" alt="">
              <div class="text">
                <p>三泰電器(股)公司</p>
                <span>臺北市中山區八德路二段326號
                  <br/> 10:00-20:00 (週日公休)</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div><footer id="footer"></footer>

<? } else {  ?>
<?
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_content01 = $row_template['MaxRow'];
$pageNum_content01 = 0;
if (isset($_GET['pageNum_content01'])) {
  $pageNum_content01 = $_GET['pageNum_content01'];
}
$startRow_content01 = $pageNum_content01 * $maxRows_content01;

$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  Template='$template'  ",$Table_ID);
if($_GET['Cate01']<>""){
$query_content01=$query_content01."AND Cate01='".$_GET['Cate01']."'" ;}

if($_GET['Cate02']<>""){
$query_content01=$query_content01."AND Cate02='".$_GET['Cate02']."'" ;}

if($_POST['h10']<>""){
$query_content01=$query_content01."AND h10='".$_POST['h10']."'" ;}

if($_POST['h09']<>""){
$query_content01=$query_content01."AND h09='".$_POST['h09']."'" ;}

if($_POST['h11']<>""){
$query_content01=$query_content01."AND h09='".$_POST['h11']."'" ;}



$query_limit_content01 = sprintf("%s order by CONVERT(`h01` using big5)  LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
//echo $query_limit_content01 ;
$content01 = mysqli_query($MySQL,$query_limit_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
if (isset($_GET['totalRows_content01'])) {
   $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
} else {
  $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
}
$totalPages_content01 = ceil($totalRows_content01/$maxRows_content01)-1;

$queryString_content01 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_content01") == false && 
        stristr($param, "totalRows_content01") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);

?>
<script language="javascript" src="../js/Zip.js"></script>
<script>
function getarea(myarea,zip){
 $.get("getarea.php?h10="+encodeURI(myarea)+"&h09="+encodeURI(zip), function(data, status){
           
                   if(status=="success"){
                           document.getElementById("myarea").innerHTML=data;
                   }else{
                      alert('系統錯誤請重新選擇');                       
                   }
                   
                  //  alert("Data: " + data + "\nStatus: " + status);
        });
}



function mystore(address){
//alert('BBB');


	var mapMarkers = [
				{
			address: address,	
			
			popup: false,
			icon: {
				image: "images/maker.png",
				iconsize: [28, 42],
				iconanchor: [28, 32]
			}
		},
		
				
		];

		// Map Initial Location
		
     var initLatitude = 23.59781;
 	 var initLongitude = 120.960515;


		// Map Extended Settings
		var mapSettings = {
			controls: {
				panControl: true,
				zoomControl: true,
				mapTypeControl: true,
				scaleControl: true,
				streetViewControl: true,
				overviewMapControl: true
			},
			scrollwheel: false,
			markers: mapMarkers,
			latitude: initLatitude,
			longitude: initLongitude,
		
			zoom: 13
		
		
		};





		var map = $("#googlemaps").gMap(mapSettings);




   geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      
	 // alert('AAA');
	 // alert(results[0].geometry.location.lat());
	 // alert(results[0].geometry.location.lng());

	 $("#googlemaps").gMap('centerAt',
	{
		latitude: results[0].geometry.location.lat(),
		longitude: results[0].geometry.location.lng()
	}
);

	 
	 var initLatitude = results[0].geometry.location.lat();
 	 var initLongitude = results[0].geometry.location.lng();
	 
	 
	
		
		
	 
	 
	 
//alert('ccc');	 
	 
	 
    } else {
    //  alert('Geocode was not successful for the following reason: ' + status);
	
	 
    }
  });




		
				











 }

</script>

 <div class="wrapper">
    <section class="container">
      <div class="d-title">
        <h3>經銷據點</h3>
      </div>
      <div class="row">
        <div class="col-lg-6 form-title">
          <form name="form1" id="form1" action="index.php?Page=6-1" method="POST" >
            <label for="">尋找距離您最近的據點</label>
            <div class="row">
              <div class="col-md-5 ">
                <div class="form-group">
                  <div class="select-box">    
                    
                    
    <select name="h10" class="form-control" size="1" id="h10" onchange="form1.submit();" >
      <option value="">請選擇縣市...</option>
      <option value="臺北市" <? if($_POST['h10']=="臺北市"){ echo "selected"; } ?>>臺北市</option>
      <option value="基隆市" <? if($_POST['h10']=="基隆市"){ echo "selected"; } ?>>基隆市</option>
      <option value="新北市" <? if($_POST['h10']=="新北市"){ echo "selected"; } ?>>新北市</option>
      <option value="宜蘭縣" <? if($_POST['h10']=="宜蘭縣"){ echo "selected"; } ?>>宜蘭縣</option>
      <option value="新竹縣" <? if($_POST['h10']=="新竹縣"){ echo "selected"; } ?>>新竹縣</option>
      <option value="新竹市" <? if($_POST['h10']=="新竹市"){ echo "selected"; } ?>>新竹市</option>
      <option value="桃園市" <? if($_POST['h10']=="桃園市"){ echo "selected"; } ?>>桃園市</option>
      <option value="苗栗縣" <? if($_POST['h10']=="苗栗縣"){ echo "selected"; } ?>>苗栗縣</option>
      <option value="臺中市" <? if($_POST['h10']=="臺中市"){ echo "selected"; } ?>>臺中市</option>
      <option value="彰化縣" <? if($_POST['h10']=="彰化縣"){ echo "selected"; } ?>>彰化縣</option>
      <option value="南投縣" <? if($_POST['h10']=="南投縣"){ echo "selected"; } ?>>南投縣</option>
      <option value="嘉義縣" <? if($_POST['h10']=="嘉義縣"){ echo "selected"; } ?>>嘉義縣</option>
      <option value="嘉義市" <? if($_POST['h10']=="嘉義市"){ echo "selected"; } ?>>嘉義市</option>
      <option value="雲林縣" <? if($_POST['h10']=="雲林縣"){ echo "selected"; } ?>>雲林縣</option>
      <option value="臺南市" <? if($_POST['h10']=="臺南市"){ echo "selected"; } ?>>臺南市</option>
      <option value="高雄市" <? if($_POST['h10']=="高雄市"){ echo "selected"; } ?>>高雄市</option>
      <option value="屏東縣" <? if($_POST['h10']=="屏東縣"){ echo "selected"; } ?>>屏東縣</option>
      <option value="臺東縣" <? if($_POST['h10']=="臺東縣"){ echo "selected"; } ?>>臺東縣</option>
      <option value="花蓮縣" <? if($_POST['h10']=="花蓮縣"){ echo "selected"; } ?>>花蓮縣</option>
      <option value="澎湖縣" <? if($_POST['h10']=="澎湖縣"){ echo "selected"; } ?>>澎湖縣</option>
      <option value="金門縣" <? if($_POST['h10']=="金門縣"){ echo "selected"; } ?>>金門縣</option>
     
     
     
    </select>
                                   
                    <i></i>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <div class="select-box" id="myarea">
                    
<select class="form-control" name="h11" id="h11" size="1" onchange="form1.submit();"   >
<option value="">請選擇區域..</option>
</select>
                    
                    
                    <i></i>
                  </div>
                </div>
              </div>
                <div class="col-md-2">
                <div class="form-group">
                <!--
                  <button type="button" class="btn btn-red btn-block text-center px-1" onclick="form1.submit();">查詢</button>
                  -->
                </div>
              </div>
            </div>
  
  <input type="hidden" name="Page" value="6">            
          </form>

<? if($_POST['h10']<>"") { ?>          
<script> 
//getarea(document.getElementById('h10').value,'<? //echo $_POST['h09'] ?>');

Buildkey(document.getElementById('h10').value); 

var len=document.getElementById("h11").length;
var i=0;
for(i=0;i<len;i++){
//alert(document.getElementById("h11").options[i].value);
if(document.getElementById("h11").options[i].value=="<? echo  $_POST['h11'] ?>"){ document.getElementById("h11").options[i].selected=true;     }
}

 </script>
 <? } ?>         
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 order-lg-2 mb-5">
          <div class="loca-list">
            <ul>
            
 <? if($row_content01['KeyID']<>""){ ?>
 <?php do { ?>                  
              <li>
               <form name="form1<? echo $row_content01['KeyID']; ?>" id="form1<? echo $row_content01['KeyID']; ?>" action="index.php?Page=6-1" method="POST" >
                <?php   include("3buttonA.php");  ?>   
                <a onclick="form1<? echo $row_content01['KeyID']; ?>.submit();" >
                  <h5><? echo $row_content01['h01']; ?></h5>  
                  <span><? echo $row_content01['h10'].$row_content01['h11'].$row_content01['h12']; ?></span>
                </a>
                <span style="color:#FF0">
                <? if($row_content01['Cate03']=="1"){ echo "啟用";}else{ echo "不啟用";}  ?>
                </span>
                <a onclick="mystore('<? echo $row_content01['h10'].$row_content01['h11'].$row_content01['h12']; ?>');"></a>
                <input type="hidden" name="h10" value="<? echo $row_content01['h10'] ?>" />
                <input type="hidden" name="h09" id="h09" value="<? echo $row_content01['h09'] ?>" />
                <input type="hidden" name="KeyID" value="<? echo $row_content01['KeyID'] ?>" />
                </form>
              </li>
 <?  } while ($row_content01 = mysqli_fetch_assoc($content01)); ?>                 
<? } else{  ?>
   <li>抱歉，目前此區尚無服務店家！</li>
 <?  } ?>            
            </ul>
          </div>
        </div>
        <div class="col-lg-6 order-lg-1">
          <div class="map-fr">
            
         
         
          <div class="col-md-12" id="googlemaps" style="height:450px;">    </div>
            
            <!--
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.611800413124!2d121.54094331500647!3d25.04724498396617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abdbf8a48117%3A0x37e5e3cc07466e57!2zMTA0OTHlj7DljJfluILkuK3lsbHljYDlhavlvrfot6_kuozmrrUzMjbomZ8!5e0!3m2!1szh-TW!2stw!4v1521603850206"
              width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            -->
            
 <? 
if($_POST['KeyID']<>""){  $where=" and KeyID = '".$_POST['KeyID']."' ";}
$query_contentS1=$query_content01;

$query_contentS1=$query_contentS1.$where;
$query_contentS1=$query_contentS1." limit 0 ,1 ";
$contentS1 = mysqli_query($MySQL,$query_contentS1) or die(mysqli_error());	
//echo $query_contentS1;	
$row_contentS1 = mysqli_fetch_assoc($contentS1);		
?>           
      
            <div class="information <? if($_POST['KeyID']<>""){ ?> showinto <? } ?>" >
              <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            
                           <!-- 00912修改 -->
              <div class="text">
                <p><? echo $row_contentS1['h01']; ?></p>
                <span><? echo $row_contentS1['h10'].$row_contentS1['h11'].$row_contentS1['h12']; ?>
                  <br /> <? echo $row_contentS1['h04']; ?></span>
                   <? if($row_contentS1['h02']=="1"){ ?>
                   <span>電話:<? echo $row_contentS1['h03']; ?></span>
                  <? } ?>
              </div>
              <? if($row_contentS1['p01']<>"" ) { ?>
              <div class="linearea">
                <a href="<? echo $row_contentS1['h07']; ?>" target="_blank" class="btn btn-radius-green addline"><i class="icon-line"></i>加入好友</a>
                 <img src="UploadImages/<? echo $row_contentS1['p01'];  ?>" alt="">
              </div>
              <? } ?>
              <div class="text">
          <? if($row_contentS1['h06']<>""){ ?><span>LINE ID: <? echo $row_contentS1['h06']; ?> </span><? } ?>
<? if($row_contentS1['h05']<>""){ ?><span>email:<? echo $row_contentS1['h05']; ?></span><? } ?>
              </div>
              <!-- 00912修改 -->
             
              
              
              
                  
              
              
              
              
            </div>
          </div>
        </div>
      </div>
    </section>
        <footer id="footer"></footer>
  </div> 		    



<? 
mysqli_free_result($content01);
 } 
 
//A 版結束 
} 
else {  //S版開始
 
if( $_GET['demo']=='y'  ) { ?>

<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="std_images/kl_c02.png" width="699" height="12" /></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">
    <div class="cont">
    <div class="title"><span class="blue">最新消息</span></div>
    <div class="left"><img src="std_images/kl_image02.jpg" width="155" height="138" /></div><div class="news">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="std_images/kl_icon2.gif" width="15" height="18" /> 我的E政府</td>
          <td align="right" class="red">[2011/05/05]</td>
        </tr>
        <tr>
          <td colspan="2"><div class="borderline"></div></td>
          </tr>
        <tr>
          <td colspan="2" class="normal">我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府</td>
        </tr>
        </table>
    </div></div></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td><img src="std_images/kl_c03.png" width="699" height="12" /></td>
  </tr>
</table>

</div>

<? } else {  ?>
<?
$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$KeyID=str_replace($vowels ,"**!!!**",$_GET[$templateS]);
if($row_template['Extend']=='Y'){
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ",$Table_ID, GetSQLValueString($colname_content01, "text")); }
else {
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ",$Table_ID, GetSQLValueString($colname_content01, "text")); 	
}

$content01 = mysqli_query($MySQL,$query_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
$totalRows_content01 = mysqli_num_rows($content01);
//echo "S版啟動".$query_content01;
?>



<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <img src="std_images/kl_c02.png" width="699" height="12" />
    </td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">
    <div class="cont">
    <div class="title"><span class="blue"><? echo $Title; ?></span></div>
    <div class="left">
    <? if($row_content01['p01']<>"") { ?>
   <img src="UploadImages/<? echo $row_content01['p01'];  ?>" width="155"  title="<? echo $row_content01['h01'];  ?>" alt="<? echo $row_content01['h01'];  ?>"/>
    <?  }  ?>
    </div>
    
    <div class="news1">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="std_images/kl_icon2.gif" width="15" height="18" /> 
		  <span style="color:#03F;"> <? echo $row_content01['h01'];  ?> </span>
          </td>
          <td align="right" class="red">
          <? if($row_content01['d01']<>"") {
           echo "[".substr($row_content01['d01'],0,10)."]";  }  ?>
          </td>
        </tr>
        <tr>
          <td colspan="2"><div class="borderline"></div></td>
          </tr>
        <tr>
          <td colspan="2" class="normal">
		  <? echo $row_content01['c01'];  ?> 
          
                   <? if($row_content01['f01']<>""){ 
$query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f01'], "text"));
$content0f = mysqli_query($MySQL,$query_content0f) or die(mysqli_error());
$row_content0f = mysqli_fetch_assoc($content0f);	?>	 		 
		 <a href="GetDBfile.php?KeyID=<? echo $row_content01['f01'];  ?> ">文件下載:<? echo $row_content0f['filename']; ?></a><br>
          <? } ?>
     
          <? if($row_content01['f02']<>""){ 
$query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f02'], "text"));
$content0f = mysqli_query($MySQL,$query_content0f) or die(mysqli_error());
$row_content0f = mysqli_fetch_assoc($content0f);	?>	 		 
		 <a href="GetDBfile.php?KeyID=<? echo $row_content01['f02'];  ?> ">文件下載:<? echo $row_content0f['filename']; ?></a><br>
          <? } ?>
          
           <? if($row_content01['f03']<>""){ 
$query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f03'], "text"));
$content0f = mysqli_query($MySQL,$query_content0f ) or die(mysqli_error());
$row_content0f = mysqli_fetch_assoc($content0f);	?>	 		 
		 <a href="GetDBfile.php?KeyID=<? echo $row_content01['f03'];  ?> ">文件下載:<? echo $row_content0f['filename']; ?></a><br>
          <? } ?>
          
          <?  include("3button.php"); ?>
          </td>
        </tr>
        </table>
    </div></div></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td><img src="std_images/kl_c03.png" width="699" height="12" /></td>
  </tr>
</table>


 <?
$query_sql = "SELECT * FROM GoWeb_BigTable where KeyID='$KeyID'";
//echo $query_sql;
$data = mysqli_query($MySQL,$query_sql) or die(mysqli_error($MySQL));
$row_data = mysqli_fetch_assoc($data);
$Cate01=$row_data['Cate01'];
$h01=$row_data['h01'];

$query_sqlU = "SELECT * FROM GoWeb_BigTable where Template='".$template."' ".$Pname." and d01>'".$row_content01['d01']."' order by d01";
//echo $query_sqlU;
$dataU = mysqli_query($MySQL,$query_sqlU) or die(mysqli_error($MySQL));
$row_dataU = mysqli_fetch_assoc($dataU);


$query_sqlD = "SELECT * FROM GoWeb_BigTable where Template='".$template."' ".$Pname." and d01<'".$row_content01['d01']."' order by d01 desc";
//echo $query_sqlD;
$dataD = mysqli_query($MySQL,$query_sqlD) or die(mysqli_error($MySQL));
$row_dataD = mysqli_fetch_assoc($dataD);
?>            
 
  
  <ul class="pagination page_number">
   <? if($row_dataU['KeyID']<>"") { ?>
    <li><a href="index.php?Page=<? echo $_GET['Page']; ?>&<? echo $templateS."=".$row_dataU['KeyID'];?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
  <? } ?>  
   <? if($row_dataD['KeyID']<>"") { ?>
    <li><a href="index.php?Page=<? echo $_GET['Page']; ?>&<? echo $templateS."=".$row_dataD['KeyID'];?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
  <? } ?>    
  </ul>


</div>







<? mysqli_free_result($content01);
 } 
}  //S版結束
 ?>