<section id="recommandation" class="u-bg-gray-02 u-pb-100">
    <div class="container-md position-relative">
        <h3 class="c-title-center u-mb-125">商品推薦</h3>
        <div class="position-relative">
            <div class="v-slick c-card-verA">
                <!-- Slides -->
                <a href="" class="v-slick-slide p-2">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑
                        </p>
                    </div>
                </a>
                <a href="" class="v-slick-slide p-2">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                    </div>
                </a>
                <a href="" class="v-slick-slide p-2">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="assets/img/index/product_02.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                    </div>
                </a>
                <a href="" class="v-slick-slide p-2">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="assets/img/index/product_03.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                    </div>
                </a>
                <a href="" class="v-slick-slide p-2">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="assets/img/index/product_04.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                    </div>
                </a>
                <a href="" class="v-slick-slide p-2">
                    <div class="v-slick-slide-frame c-card-head c-card-head">
                        <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="c-card-body-layout">
                        <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="u-pt-100 text-md-right text-right">
            <a href="" class="u-link-blue-500 u-link__hover--blue-highlight">More <i class="far fa-angle-right u-mr-025 u-font-14"></i></a>
        </div>
    </div>
</section>