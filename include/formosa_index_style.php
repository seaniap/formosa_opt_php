<script type="text/javascript"> 
	$(function(){
	  
		$("#link_style_213").click(function(){
			qg('event', "N_my_type_clicked", {"clicled_name": "學院風"});
		});

		$("#link_style_214").click(function(){
			qg('event', "N_my_type_clicked", {"clicled_name": "日韓風"});
		});

		$("#link_style_137").click(function(){
			qg('event', "N_my_type_clicked", {"clicled_name": "復古風"});
		});

		$("#link_style_133").click(function(){
			qg('event', "N_my_type_clicked", {"clicled_name": "明星雜誌風"});
		});
        $("#link_tag_id7").click(function(){
			qg('event', "N_icon_clicked", {"clicled_name": "行動配送"});
		});
	})
</script>

<section id="style" class="u-bg-gray-02 u-pb-100">
    <div class="container">
        <h3 class="c-title-center u-mb-125">我的眼鏡風格</h3>
        <div class="row no-gutters" data-aos="fade-left" data-aos-duration="1000">
            <a id="link_style_213" href="https://www.eyesmart.com.tw/link/6249/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=mystyle&utm_term=Preppy" class="col-6 col-md-3 p-index-style-link order-0" target="_blank">
                <div class="p-index-style-frame">
                    <picture>
                        <source srcset="assets/img/index/style_01_pc.png" media="(min-width: 768px)">
                        <source srcset="assets/img/index/style_01_mobile.png" media="(min-width: 320px)">
                        <img src="assets/img/index/style_01_pc.png" alt="" class="img-fluid">
                    </picture>
                </div>
                <div class="u-bg-blue-highlight u-font-16 u-md-font-24 u-font-weight-700 u-text-white text-center u-py-025">
                    學院風</div>
            </a>
            <a id="link_style_214" href="https://www.eyesmart.com.tw/link/6250/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=mystyle&utm_term=JapanandKorea" class="col-6 col-md-3 p-index-style-link" target="_blank">
                <div class="p-index-style-frame">
                    <picture>
                        <source srcset="assets/img/index/style_02_pc.png" media="(min-width: 768px)">
                        <source srcset="assets/img/index/style_02_mobile.png" media="(min-width: 320px)">
                        <img src="assets/img/index/style_02_pc.png" alt="" class="img-fluid">
                    </picture>
                </div>
                <div class="u-bg-blue-500 u-font-16 u-md-font-24 u-font-weight-700 u-text-white text-center u-py-025">
                    日韓風
                </div>
            </a>
            <a id="link_style_137" href="https://www.eyesmart.com.tw/link/6251/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=mystyle&utm_term=Vintage" class="col-6 col-md-3 p-index-style-link order-1 order-md-0" target="_blank">
                <div class="p-index-style-frame">
                    <picture>
                        <source srcset="assets/img/index/style_03_pc.png" media="(min-width: 768px)">
                        <source srcset="assets/img/index/style_03_mobile.png" media="(min-width: 320px)">
                        <img src="assets/img/index/style_03_pc.png" alt="" class="img-fluid">
                    </picture>
                </div>
                <div class="u-bg-blue-highlight u-font-16 u-md-font-24 u-font-weight-700 u-text-white text-center u-py-025">
                    復古風</div>
            </a>
            <a id="link_style_133" href="https://www.eyesmart.com.tw/link/6252/?utm_source=OfficialWebsite&utm_medium=index&utm_campaign=mystyle&utm_term=Magazine" class="col-6 col-md-3 p-index-style-link" target="_blank">
                <div class="p-index-style-frame">
                    <picture>
                        <source srcset="assets/img/index/style_04_pc.png" media="(min-width: 768px)">
                        <source srcset="assets/img/index/style_04_mobile.png" media="(min-width: 320px)">
                        <img src="assets/img/index/style_04_pc.png" alt="" class="img-fluid">
                    </picture>
                </div>
                <div class="u-bg-blue-500 u-font-16 u-md-font-24 u-font-weight-700 u-text-white text-center u-py-025">
                    明星雜誌風</div>
            </a>
        </div>
        <div class="u-pt-050 text-md-right text-right">
            <a id="link_tag_id7" href="javascript:void(0)" class="u-link-blue-500 u-link__hover--blue-highlight">More <i class="far fa-angle-right u-mr-025 u-font-14"></i></a>
        </div>
    </div>
</section>