<?php require_once('../Connections/MySQL.php'); ?>
<?php $template = "suggest01.php";
$templateS = "suggest01";
// 判斷是預設是A版還是S版
// 預設 A 版
?>
<?php
if ($_GET[$templateS] == "" and $row_template['Extend'] == "Y") {
    //A 版開始
    if ($_GET['demo'] == 'y') { ?>
        <!-- ============= 主要內容區 ============= -->
        <p>here is a demo</p>
        <!-- =============end 主要內容區 ============= -->
    <?php } else {  ?>
        <?php $currentPage = $_SERVER["PHP_SELF"];

        $maxRows_content01 = $row_template['MaxRow'];
        $pageNum_content01 = 0;
        if (isset($_GET['pageNum_content01'])) {
            $pageNum_content01 = $_GET['pageNum_content01'];
        }
        $startRow_content01 = $pageNum_content01 * $maxRows_content01;

        $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        if ($_GET['Cate01'] <> "") {
            $query_content01 = $query_content01 . "AND Cate01='" . $_GET['Cate01'] . "'";
        }

        if ($_GET['h02'] <> "") {
            $query_content01 = $query_content01 . "AND h02='" . $_GET['h02'] . "'";
        }

        //排序 by Sq
        $query_content01 = $query_content01 . " order by Sq desc ";

        $query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
        //echo $query_limit_content01 ;
        $content01 = mysqli_query($MySQL, $query_limit_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);

        $all_content01 = mysqli_query($MySQL, $query_content01);
        $totalRows_content01 = mysqli_num_rows($all_content01);

        $totalPages_content01 = ceil($totalRows_content01 / $maxRows_content01) - 1;

        $queryString_content01 = "";
        if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
                if (
                    stristr($param, "pageNum_content01") == false &&
                    stristr($param, "totalRows_content01") == false &&
                    stristr($param, "Cate01") == false &&
                    stristr($param, "Cate02") == false &&
                    stristr($param, "Cate03") == false
                ) {
                    array_push($newParams, $param);
                }
            }
            if (count($newParams) != 0) {
                $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
            }
        }
        $queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
        $queryString_content01 = $queryString_content01 . "&Cate01=" . $_GET['Cate01'] . "&Cate02=" . $_GET['Cate02'] . "&Cate03=" . $_GET['Cate03'];
        ?>

        <p>Here is A</p>

    <?php mysqli_free_result($content01);
    }

    //A 版結束 
} else {
    //S版開始

    if ($_GET['demo'] == 'y') { ?>
        <section id="recommandation" class="u-bg-gray-02 u-pb-100">
            <div class="container-md position-relative">
                <h3 class="c-title-center u-mb-125">商品推薦</h3>
                <div class="position-relative">
                    <div class="v-slick c-card-verA">
                        <!-- Slides -->
                        <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑
                                </p>
                            </div>
                        </a>
                        <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                            </div>
                        </a>
                        <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_02.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                            </div>
                        </a>
                        <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_03.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                            </div>
                        </a>
                        <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_04.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                            </div>
                        </a>
                        <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="u-pt-000 text-md-right text-right">
                    <a href="index.php?Page=2" class="u-link-blue-500 u-link__hover--blue-highlight">More <i class="far fa-angle-right u-mr-025 u-font-14"></i></a>
                </div>
            </div>
        </section>
    <?php } else {  ?>
        <?php $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $maxRows_content01 = $row_template['MaxRow'];
        $KeyID = str_replace($vowels, "**!!!**", substr($_GET[$templateS], 0, 50));
        if ($row_template['Extend'] == 'Y') {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        } else {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        }

        $content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);
        $totalRows_content01 = mysqli_num_rows($content01);
        //echo "S版啟動".$query_content01;
        ?>
        <section id="recommandation" class="u-bg-gray-02 u-pb-100">
            <div class="container-md position-relative">
                <h3 class="c-title-center u-mb-125">商品推薦</h3>
                <?php include("3button.php"); ?>
                <div class="position-relative">
                
                    <div class="v-slick c-card-verA">
                        <!-- Slides -->
                        <a href="<?php echo $row_content01['l01'];  ?>" class="v-slick-slide p-2" title="l01" target="_blank">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="UploadImages/<?php echo $row_content01['p01']; ?>" alt="<?php echo $row_content01['h01']; ?>" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">
                                <?php echo $row_content01['h01'];  ?>
                                </p>
                            </div>
                        </a>
                        <a href="<?php echo $row_content01['l02'];  ?>" class="v-slick-slide p-2" target="_blank">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="UploadImages/<?php echo $row_content01['p02']; ?>" alt="<?php echo $row_content01['h02']; ?>" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000"><?php echo $row_content01['h02']; ?></p>
                            </div>
                        </a>
                        <a href="<?php echo $row_content01['l03'];  ?>" class="v-slick-slide p-2" target="_blank">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="UploadImages/<?php echo $row_content01['p03']; ?>" alt="<?php echo $row_content01['h03']; ?>" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000"><?php echo $row_content01['h03']; ?></p>
                            </div>
                        </a>
                        <!-- <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_03.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                            </div>
                        </a>
                        <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_04.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                            </div>
                        </a>
                        <a href="" class="v-slick-slide p-2">
                            <div class="v-slick-slide-frame c-card-head c-card-head">
                                <img src="assets/img/index/product_01.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="c-card-body-layout">
                                <p class="u-font-14 u-font-md-16 u-link-gray-800 u-mt-100 u-mb-000">HORIEN 金屬復古雅痞款墨鏡 ☀ 時代黑</p>
                            </div>
                        </a> -->
                    </div>
                </div>
                <div class="u-pt-000 text-md-right text-right">
                    <a href="https://www.eyesmart.com.tw/link/5900/" class="u-link-blue-500 u-link__hover--blue-highlight">More <i class="far fa-angle-right u-mr-025 u-font-14" target="_blank"></i></a>
                </div>
            </div>
        </section>





<?php mysqli_free_result($content01);
    }
}  //S版結束
?>