/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src@4.0/assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src@4.0/assets/js/main.js":
/*!***********************************!*\
  !*** ./src@4.0/assets/js/main.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ajax 配合JQ3 引入設置
$(document).ready(function () {
  var el = document.querySelector('html[lang="zh-tw"]');

  if (el) {
    if ($("#header")) {
      $.ajax({
        url: "ajax/_header.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#header").html(data);
        headerFunction();
        goToAnchor();
      });
    }

    if ($("#sideLink")) {
      $.ajax({
        url: "ajax/_sideLink.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#sideLink").html(data);
        toggleSideLink();
      });
    }

    if ($("#footer")) {
      $.ajax({
        url: "ajax/_footer.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#footer").html(data);
        goTop();
        goTopSticky();
      });
    }
  }
}); //英文版的ajax

$(document).ready(function () {
  var el = document.querySelector('html[lang="en"]');

  if (el) {
    if ($("#headerEn")) {
      $.ajax({
        url: "../ajax/_header_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#headerEn").html(data);
        headerFunction();
        goToAnchor();
      });
    }

    if ($("#sideLinkEn")) {
      $.ajax({
        url: "../ajax/_sideLink_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#sideLinkEn").html(data);
        toggleSideLink();
      });
    }

    if ($("#footerEn")) {
      $.ajax({
        url: "../ajax/_footer_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#footerEn").html(data);
        goTop();
        goTopSticky();
      });
    }
  }
});

function toolsListener() {
  window.addEventListener("keydown", function (e) {
    if (e.keyCode === 9) {
      document.body.classList.remove("js-useMouse");
      document.body.classList.add("js-useKeyboard");
    }
  });
  window.addEventListener("mousedown", function (e) {
    document.body.classList.remove("js-useKeyboard");
    document.body.classList.add("js-useMouse");
  });
}

function toggleSideLink() {
  var elTw = document.querySelector('html[lang="zh-tw"]');
  var elEn = document.querySelector('html[lang="en"]');
  document.querySelector("#sideLinkToggleBtn").addEventListener("click", function () {
    this.classList.toggle("js-sideLinkOpened");

    if (elTw) {
      document.querySelector("#sideLink").classList.toggle("js-sideLinkOpened");
    }

    if (elEn) {
      document.querySelector("#sideLinkEn").classList.toggle("js-sideLinkOpened");
    }

    document.querySelector("#page").classList.toggle("js-sideLinkOpened");
    document.querySelector("body").classList.toggle("overflow-hidden"); //打開sideMenu時，把展開的menu關掉

    document.documentElement.classList.remove("js-menuOpened");
    document.querySelector(".l-header-hamburger").classList.remove("js-menuOpened");
    document.querySelector(".l-header-menu").classList.remove("js-menuOpened"); //打開sideMenu時，把gotop推到旁邊去

    document.querySelector("#sideLinkFooter").classList.toggle("js-sideLinkOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= 1200) {
      document.querySelector("#sideLinkToggleBtn").classList.remove("js-sideLinkOpened");

      if (elTw) {
        document.querySelector("#sideLink").classList.remove("js-sideLinkOpened");
      }

      if (elEn) {
        document.querySelector("#sideLinkEn").classList.remove("js-sideLinkOpened");
      }

      document.querySelector("#page").classList.remove("js-sideLinkOpened");
      document.querySelector("body").classList.remove("overflow-hidden"); //[移除]打開sideMenu時，把gotop推到旁邊去

      document.querySelector("#sideLinkFooter").classList.remove("js-sideLinkOpened");
    }
  });
}

function goTop() {
  document.querySelector("#goTop").onclick = function (e) {
    e.preventDefault();
    $("html, body").animate({
      scrollTop: 0
    }, 500);
  };
}

function goTopSticky() {
  var el = document.querySelector("#sideLinkFooter"); //計算footer目前高度

  var footerHeightVar = document.querySelector(".l-footer").clientHeight; // console.log(`footerHeightVar + ${footerHeightVar}`);
  //當下網頁高度(整頁長)

  var scrollHeightVar = document.documentElement.scrollHeight; // console.log(`scrollHeightVar + ${scrollHeightVar}`);
  //當下畫面高度(window height)

  var windowHeightVar = window.innerHeight; // console.log(`windowHeightVar + ${windowHeightVar}`);
  //捲動中的畫面最上端與網頁頂端之間的距離

  var scrollTopVar = document.documentElement.scrollTop; // console.log(`scrollTopVar + ${scrollTopVar}`);
  // console.log(windowHeightVar + scrollTopVar + footerHeightVar);
  //如果整頁長==畫面高度+捲動高度

  if (el) {
    if (scrollHeightVar <= windowHeightVar + scrollTopVar + footerHeightVar) {
      el.classList.remove("js-sticky");
    } else {
      el.classList.add("js-sticky");
    }
  }
}

function goToAnchor() {
  var headerHeight = "";
  window.addEventListener("resize", function () {
    return headerHeight = $(".l-header").height();
  });
  $('.js-goToAnchor').click(function (e) {
    e.preventDefault();
    var target = $(this).attr('href');
    var targetPos = $(target).offset().top;
    var headerHeight = $(".l-header").height();
    $('html,body').animate({
      scrollTop: targetPos - headerHeight
    }, 1000);
    var trigger02 = document.querySelector("#hamburger");
    var target02 = document.querySelector("#menu");
    trigger02.classList.remove("js-menuOpened");
    target02.classList.remove("js-menuOpened");
    document.documentElement.classList.remove("js-menuOpened");
  });
} //風琴用function


function Accordion(el, target, siblings) {
  this.el = el;
  this.target = target;
  this.siblings = siblings;
}

Accordion.prototype.init = function () {
  var triggers = document.querySelectorAll(this.el);
  var target = this.target;
  var siblings = this.siblings;
  Array.prototype.slice.call(triggers).forEach(function (trigger) {
    trigger.addEventListener("click", function () {
      event.preventDefault();

      if (siblings == true) {
        if (this.querySelector(target) !== null) {
          // this.querySelector(target).classList.toggle(
          //   "js-accordionExpended"
          // );
          //為了要能在打開一個時，其他的收合，這邊改用jQuery==>
          $(this).find(target).toggleClass("js-accordionExpended"); // 增加藍線框效果

          $(this).toggleClass("c-accordion-btn-focus");
          $(this).siblings().find(target).removeClass("js-accordionExpended"); // 增加藍線框效果

          $(this).siblings().removeClass("c-accordion-btn-focus");
        } // document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        //為了要能在打開一個時，其他的收合，這邊改用jQuery==>


        var att = $(trigger).attr("data-target");
        $(att).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended"); // $(`${trigger.getAttribute("data-target")}`).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended");
        //用了jQuery之後，這組會被樓上那個綁在一起，所以可以省略
        // trigger.classList.toggle("js-accordionExpended");
      } else {
        if (this.querySelector(target) !== null) {
          this.querySelector(target).classList.toggle("js-accordionExpended");
        }

        document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        trigger.classList.toggle("js-accordionExpended");
      }
    });
  });
};

function nodeListToArray(nodeListCollection) {
  return Array.prototype.slice.call(nodeListCollection);
} // const nodeListToArray = (nodeListCollection) => Array.prototype.slice.call(nodeListCollection);

/**
 * 
 * @param {*} el :class name
 * @param {*} target :被影響到的目標
 * @param {*} mediaQuery :斷點設定
 * @說明 "el"與"target" toggle 一個叫js-active的class要用出什麼效果，端看你怎麼寫js-active的css效果展開或收合什麼東西
 */


function toggleVisiable(el, target, mediaQuery) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);

  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        this.classList.toggle("js-active");
        target.classList.toggle("js-active");
        var hasMediaQuery = mediaQuery;

        if (hasMediaQuery !== "") {
          var isMobile = window.innerWidth < mediaQuery;

          if (isMobile) {
            document.documentElement.classList.toggle("js-functionMenuOpened");
          }
        } else {
          document.documentElement.classList.remove("js-functionMenuOpened");
        }

        window.addEventListener("resize", function () {
          if (window.innerWidth >= mediaQuery) {
            document.documentElement.classList.remove("js-functionMenuOpened");
          }
        });
      });
    });
  }
}
/*el=觸發對象(開關)*/

/*target=被控制的物件(燈)*/


function clickConfirm(el, target) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);

  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        target.classList.remove("js-active");
        document.documentElement.classList.remove("js-functionMenuOpened");
      });
    });
  }
} //↑↑↑通用function---------------------


function createAccordion() {
  if (document.querySelector('.c-accordion-btn')) {
    new Accordion(".c-accordion-btn", ".c-accordion-btn-icon", true).init();
  }
} // TODO: 想辦法這個全域變數要處理一下


var str = 0;

function showAllTab() {
  if (window.innerWidth > 992 && str == 1) {
    $(".tab-pane").removeClass("show active").first().addClass("show active");
    $(".c-tab-linkE").removeClass("active").parent().first().find(".c-tab-linkE").addClass("active");
    str = 0;
  }

  if (window.innerWidth < 992 && str == 0) {
    $(".tab-pane").addClass("show active");
    str = 1;
  } // console.log(str);

}

function togglepFilterSubmenu() {
  // TODO: 有空試試看寫一個手機版可以針對內容高度增加showmore，並且記錄此showmore已經點開過，不會因為rwd就又計算一次
  //手機版顯示全部
  if (document.querySelector('.p-products-search-showMore')) {
    new Accordion(".p-products-search-showMore", undefined, false).init();
  }

  var triggers = document.querySelectorAll(".c-accordion-btn");
  nodeListToArray(triggers).forEach(function (trigger) {
    var el = document.querySelector(trigger.getAttribute("data-target"));
    trigger.addEventListener("click", function () {
      if (el.querySelector(".p-products-search-showMore")) {
        el.querySelector(".p-products-search-showMore").classList.remove("js-accordionExpended");
        el.querySelector(".c-accordion-content-layout").classList.remove("js-accordionExpended");
      }
    });
  }); //手機版開關選單
  //商品專區

  toggleVisiable(".close", ".p-products-search", "");
  toggleVisiable(".p-products-search-btn", ".p-products-search", 992);
  clickConfirm("#js-confirm", ".p-products-search"); //門市查詢

  toggleVisiable(".v-dropdown-btn", ".v-dropdown-menu", 992);
  toggleVisiable(".close", ".v-dropdown-menu", "");
  clickConfirm("#js-confirm", ".v-dropdown-menu"); // if ($(".v-dropdown-btn").hasClass("js-active")) {
  //   document.querySelector("body").addEventListener("click", function () {
  //     document.querySelector(".v-dropdown-btn").classList.remove("js-active");
  //     document.querySelector(".v-dropdown-menu").classList.remove("js-active");
  //   });
  // }
}

function confirmIfDoubleMenuOpened(el, mediaQuery) {
  if (window.innerWidth < mediaQuery && $(el).hasClass("js-active") && document.documentElement.classList == "") {
    document.documentElement.classList.add("js-menuOpened");
  }
} // 輪播相關--------------------------


function Slick(el, slidesToShow, slidesPerRow, rows, responsive) {
  this.el = el;
  this.slidesToShow = slidesToShow;
  this.slidesPerRow = slidesPerRow;
  this.rows = rows;
  this.responsive = responsive;
}

;

Slick.prototype.init = function () {
  $(this.el + " .v-slick").slick({
    arrows: true,
    slidesToShow: this.slidesToShow,
    slidesPerRow: this.slidesPerRow,
    rows: this.rows,
    prevArrow: "<button type=\"button\" class=\"v-slick-arrows-prev\"><i class=\"fal fa-angle-left\"></i></button>",
    nextArrow: "<button type=\"button\" class=\"v-slick-arrows-next\"><i class=\"fal fa-angle-right\"></i></button>",
    // autoplay: true,
    // lazyLoad: "progressive",
    responsive: this.responsive
  });
};

function setStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;

    if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
      trigger.style.height = "".concat(totalNum, "px");
    } else if (window.innerWidth > 1200) {
      trigger.style.height = "";
      trigger.classList.remove('js-active');
      trigger.querySelector("i").classList.remove("js-submenuOpened");
    }

    window.addEventListener("resize", function () {
      if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
        trigger.style.height = "".concat(totalNum, "px");
      } else if (window.innerWidth > 1200) {
        trigger.style.height = "";
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      }
    });
  });
}

function toggleStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;
    trigger.querySelector("td:first-child").addEventListener('click', function () {
      if (window.innerWidth < 1200 && trigger.style.height == "") {
        trigger.style.height = "".concat(totalNum, "px");
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      } else {
        trigger.style.height = "";
        trigger.classList.add("js-active");
        trigger.querySelector("i").classList.add("js-submenuOpened");
      }
    });
  });
} // TODO: 有空看一下為什麼new第三個就出事了


function createSlicks() {
  if (document.querySelector(".v-slick")) {
    var targets = ["#news", "#recommandation"];
    targets.forEach(function (target) {
      new Slick(target, 4, 1, 1, [{
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false
        }
      }]).init();
    });
    new Slick("#video", 1, 2, 2, [{
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1
      }
    }, {
      breakpoint: 576,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1,
        arrows: false
      }
    }]).init();
  }
}

function rionetSlick() {
  var el = ".v-rionetSlick";

  if (document.querySelector(el)) {
    $(el).slick({
      // centerMode: true,
      // centerPadding: '60px',
      slidesToShow: 4,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          dots: true
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false,
          // variableWidth: true,
          dots: true
        }
      }]
    }); //   $('.v-tabBtnSlick-prev').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickPrev');
    //  });
    //   $('.v-tabBtnSlick-next').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickNext');
    //  });
  }
}

function tabBtnSlick() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length;
    var slidesToShowNum = tabLength > 6 ? 6 : tabLength; // console.log("slidesToShowNum = " + slidesToShowNum);

    $(el).slick({
      slidesToShow: slidesToShowNum,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          variableWidth: false // slidesToScroll: 5,
          // infinite: true,
          // centerMode: false,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          variableWidth: false // infinite: true,
          // centerMode: false,

        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          // infinite: false,
          arrows: false
        }
      }]
    });
  }
}

function tabBtnSlickFixed() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length; // var slidesToShowNum = (tabLength > 6) ? 6 : tabLength;
    // console.log(tabLength);

    if (window.innerWidth >= 1200 && tabLength <= 6) {
      $(".slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    }

    if (window.innerWidth >= 768 && window.innerWidth < 1200 && tabLength < 5) {
      $(".slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    } // if (window.innerWidth < 768) {
    // document.querySelector('.slick-track').classList.remove('text-md-center');
    // }

  }
}

function tabBtnSlickMembers() {
  var el = ".v-tabBtnSlick-members";

  if (document.querySelector(el)) {
    $(el).slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
          variableWidth: false
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          arrows: false
        }
      }]
    });
  }
}

function tabBtnSlickFixedMembers() {
  var el = ".v-tabBtnSlick-members";

  if (document.querySelector(el)) {
    if (window.innerWidth >= 768) {
      $(".v-tabBtnSlick-members .slick-track").addClass("text-md-center");
    }
  }
} // End 輪播相關--------------------------


function warrantyLink() {
  if (document.querySelectorAll(".p-index-warranty-link").length) {
    var triggers = document.querySelectorAll(".p-index-warranty-link");
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".c-btn").classList.add("js-btnHover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".c-btn").classList.remove("js-btnHover");
      });
    });
  }
} //手機版漢堡選單


function toggleMobileMenu(mediaQuery) {
  var trigger = document.querySelector("#hamburger");
  var target = document.querySelector("#menu");
  trigger.addEventListener("click", function () {
    this.classList.toggle("js-menuOpened");
    target.classList.toggle("js-menuOpened");
    document.documentElement.classList.toggle("js-menuOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      trigger.classList.remove("js-menuOpened");
      target.classList.remove("js-menuOpened");
      document.documentElement.classList.remove("js-menuOpened");
    }
  });
} //手機版風琴折疊選單


function toggleMobileSubmenu(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-link");
  var targets = document.querySelectorAll(".l-header-submenu--hidden");
  nodeListToArray(triggers).forEach(function (trigger) {
    if (trigger.nextElementSibling !== null) {
      trigger.addEventListener("click", function (e) {
        e.preventDefault();
        var target = trigger.nextElementSibling;

        if (window.innerWidth < mediaQuery) {
          target.classList.toggle("js-submenuOpened");
          trigger.querySelector("i").classList.toggle("js-submenuOpened"); //為了開一個關一個，用jQuery加寫

          $(target).parents().siblings().find(".l-header-submenu , i").removeClass("js-submenuOpened");
        }
      });
    }
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      nodeListToArray(triggers).forEach(function (trigger) {
        var target = trigger.nextElementSibling;
        target.classList.remove("js-submenuOpened");
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      });
    }
  });
}

function sibingMobileSubmenu() {
  $(".l-header-menu-item").on("click", function () {
    $(this).siblings().find(".l-header-submenu").removeClass("js-submenuOpened");
  });
}

function togglePcHoverState(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-item");
  nodeListToArray(triggers).forEach(function (trigger) {
    if (trigger.querySelector(".l-header-submenu") !== null) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".l-header-menu-link").classList.add("js-hover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".l-header-menu-link").classList.remove("js-hover");
      });
    }
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth < mediaQuery) {
      Array.prototype.slice.call(document.querySelectorAll(".l-header-menu-link")).forEach(function (item) {
        item.classList.remove("js-hover");
      });
    }
  });
}

function headerFunction() {
  var breakpoint = 1200;
  toggleMobileMenu(breakpoint);
  toggleMobileSubmenu(breakpoint);
  togglePcHoverState(breakpoint);
}

function lazyLoad() {
  if (document.querySelector("img[data-src]")) {
    var observer = lozad();
    observer.observe();

    if (-1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > 0) {
      var iframe = document.querySelector("iframe");
      iframe.setAttribute("src", "https://www.youtube.com/embed/O7pNpR3Py68");
    }
  }
} //計算「最新消息」、「人氣推薦」這類的卡片列表的標題字數，讓大家的高度一樣


function autoFixHeight(con) {
  var el = document.querySelectorAll(con);
  var thisHeight = -1;
  var maxHeight = -1;
  var breakpoint = 768; //為了ie不支援nodelist的forEach修正

  nodeListToArray(el).forEach(function (item) {
    item.style.height = ""; //清空之前的style

    thisHeight = item.clientHeight; //取得已經減過高度的

    maxHeight = maxHeight > thisHeight ? maxHeight : thisHeight;
  });

  if (document.body.clientWidth > breakpoint) {
    nodeListToArray(el).forEach(function (item) {
      item.style.height = "".concat(maxHeight, "px");
    });
  }
}

function textHide(num, con) {
  var el = document.querySelectorAll(con); //ie才執行這個超過字數增加刪節號，其他瀏覽器靠css語法即可
  // if (
  //   navigator.userAgent.indexOf("MSIE") !== -1 ||
  //   navigator.appVersion.indexOf("Trident/") > 0
  // ) {
  //   Array.prototype.slice.call(el).forEach(function (item) {
  //     var txt = item.innerText;
  //     if (txt.length > num) {
  //       txtContent = txt.substring(0, num - 1) + "...";
  //       item.innerHTML = txtContent;
  //     }
  //   });
  // }

  nodeListToArray(el).forEach(function (item) {
    var txt = item.innerText;

    if (txt.length > num) {
      txtContent = txt.substring(0, num - 1) + "...";
      item.innerHTML = txtContent;
    } else {
      txtContent = txt.substring(0, num) + "...";
      item.innerHTML = txtContent;
    }
  });
  autoFixHeight(con);
}

function clearCheckBox(el, target) {
  if (document.querySelector(el)) {
    var trigger = document.querySelector(el);
    var targets = document.querySelectorAll(target);
    trigger.addEventListener("click", function () {
      event.preventDefault();
      trigger.blur();
      Array.prototype.slice.call(targets).forEach(function (trigger) {
        trigger.checked = false;
      });
    });
  }
}

function cardContentFunction() {
  if (document.querySelector(".c-card-body-title") !== null) {
    autoFixHeight(".c-card-body-title");
  } //人氣推薦內文字數限制


  if (document.querySelector(".c-card-body-txt") !== null) {
    textHide(48, ".c-card-body-txt");
  }
}

function toolTips() {
  $('[data-toggle="tooltip"]').tooltip({
    placement: 'right',
    trigger: 'hover',
    offset: 30
  });
  $('[data-toggle="tooltip"]').on('inserted.bs.tooltip', function () {
    var getTootipsId = "#" + $(this).attr("aria-describedby"); // console.log(getTootipsId);
    // console.log($(this).attr("data-original-title") == "德國蔡司數位影像系統");

    if ($(this).attr("data-original-title") == "德國蔡司數位影像系統") {
      $(getTootipsId).find('.tooltip-inner').html("德國蔡司<br>數位影像系統");
    }

    if ($(this).attr("data-original-title") == "法國依視路全方位視覺檢測系統") {
      $(getTootipsId).find('.tooltip-inner').html("法國依視路<br>全方位視覺檢測系統");
    }
  });
}

function aos() {
  if (document.querySelector("[data-aos]")) {
    AOS.init({
      once: true
    });
  }
}


function ytHandler() {
	  if (document.querySelector("[data-url]")) {
	    var triggers = document.querySelectorAll("[data-url]");
	    var target = document.querySelector("iframe");
	    
	    //對應的ad key 可以自行塞入你要的key 
	    var index= 1;
	    
	    Array.prototype.slice.call(triggers).forEach(function (trigger) {
	      //塞入對應的AD key值
	      trigger.setAttribute("index", index++);
	      
	      //ad被點擊事件
	      trigger.onclick = function () {
	    	 
	    	var triggerIndex = trigger.getAttribute("index");
	    	  
	    	/*=====此處 為抓DB AD廣告圖片資料========================*/
	    	 
	        $.get("productslide01.php",{l01:triggerIndex}, function(result){
	    	     
	            result = JSON.parse(result);
	            
	            let html = "";
	           
	            result.forEach(function(item){
	            	 
	            	html += `
		        		<a href="${item.l01}" class="v-slick-slide p-2">
							<div class="v-slick-slide-frame c-card-head">
								<img src="UploadImages/${item.p01}" alt="" class="img-fluid">
							</div>
							<div class="c-card-body-layout">
								<p class="u-font-14 u-font-md-16 u-text-gray-800 u-mb-000 u-mt-100">${item.h01}</p>
							</div>
						</a>
		        	`;
	            });
	            $('#slick_ad').slick("destroy");
	            
	           
	            $("#slick_ad").html(html);
	             
	             $("#slick_ad").slick({
	                arrows: true,
	                slidesToShow: 1,
	                slidesPerRow: 2,
	                rows: 2,
	                prevArrow: "<button type=\"button\" class=\"v-slick-arrows-prev\"><i class=\"fal fa-angle-left\"></i></button>",
	                nextArrow: "<button type=\"button\" class=\"v-slick-arrows-next\"><i class=\"fal fa-angle-right\"></i></button>",
	                // autoplay: true,
	                // lazyLoad: "progressive",
	                responsive: [{
                    breakpoint: 992,
                    settings: {
                      slidesToShow: 2,
                      slidesPerRow: 1,
                      rows: 1
                    }
                  }, {
                    breakpoint: 576,
                    settings: {
                      slidesToShow: 2,
                      slidesPerRow: 1,
                      rows: 1,
                      arrows: false
                    }
                  }]
	              });
	        })
	    	
	        //$('#slick_ad').slick('slickGoTo', triggerIndex);
	    	
	    	
	    	target.setAttribute("src", trigger.getAttribute("data-url"));
	        //alert(+trigger.getAttribute("data-url"));
	        trigger.classList.add("js-active");
	        Array.prototype.slice.call(triggers).filter(function (item) {
	          return item !== trigger;
	        }).forEach(function (item) {
	          item.classList.remove("js-active");
	        });
	      };
	    });
	  }
}
 


function cardJustifyContent() {
  var el = $(".js-cardJustifyContent");

  if (el) {
    for (var i = 0; i < el.length; i++) {
      var elChildrenLength = el.eq(i).children('div').length; // console.log(elChildrenLength);

      if (elChildrenLength <= 2) {
        el.eq(i).addClass("justify-content-center");
      } else {
        el.eq(i).removeClass("justify-content-center");
      }
    }
  }
}

function storeFilterNotification(inputContainer, targetEl) {
  var el = document.querySelector(inputContainer);
  var target = document.querySelector(targetEl);

  if (el) {
    // console.log(inputContainer + " + " + target);
    var triggers = el.querySelectorAll("input[type='checkbox']"); // console.log(triggers);

    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        var checkedNum = el.querySelectorAll("input[type=checkbox]:checked").length; // console.log(checkedNum);

        if (checkedNum > 0) {
          target.classList.add("js-inputChecked");
        } else {
          target.classList.remove("js-inputChecked");
        }
      });
    });
    var clearAllBtnEl = document.querySelector("#js-clearCheckBoxes");
    clearAllBtnEl.addEventListener("click", function () {
      target.classList.remove("js-inputChecked");
    });
  }
} //滾軸式日期選單


function dateMobiscroll() {
  if ($('#date')) {
    $('#date').mobiscroll().date({
      theme: $.mobiscroll.defaults.theme,
      // Specify theme like: theme: 'ios' or omit setting to use default 
      mode: 'mixed',
      // Specify scroller mode like: mode: 'mixed' or omit setting to use default 
      display: 'modal',
      // Specify display mode like: display: 'bottom' or omit setting to use default 
      lang: 'zh' // Specify language like: lang: 'pl' or omit setting to use default 

    });
  }
} //下拉選單跳轉超連結


function selectURL() {
  var el = document.querySelector(".js-selectURL");

  if (el) {
    el.addEventListener("change", function () {
      // window.open(this.options[this.selectedIndex].value);
      location.href = this.options[this.selectedIndex].value; // console.log(this.options[this.selectedIndex].value);
    });
  }
}

function popupShow(id) {
  $("#" + id).attr("style", "");
}

function popupHide(id) {
  $("#" + id).attr("style", "display: none !important");
} //呼叫function-網頁載入完成後


$(document).ready(function () {
  selectURL();
  toolsListener();
  createSlicks();
  tabBtnSlick();
  tabBtnSlickFixed(); //會員權益專用

  tabBtnSlickMembers();
  tabBtnSlickFixedMembers();
  rionetSlick();
  warrantyLink();
  lazyLoad();
  createAccordion(); // createVideoSwiper();

  aos();
  ytHandler();
  togglepFilterSubmenu();
  clearCheckBox("#js-clearCheckBoxes", "input[type='checkbox']");
  setStoreTableHeightAtMobile(); // $('[data-toggle="popover"]').popover();

  toolTips();
  cardJustifyContent();
  storeFilterNotification(".v-dropdown-menu", ".v-dropdown-btn");
  storeFilterNotification(".p-products-search", ".p-products-search-btn");
  toggleStoreTableHeightAtMobile();
  dateMobiscroll();
  goToAnchor(); // goTopSticky();
});

window.onload = function () {
  cardContentFunction();
  showAllTab();
}; //呼叫function-視窗大小變更


$(window).resize(function () {
  cardContentFunction();
  showAllTab(); // setStoreTableHeightAtMobile();

  confirmIfDoubleMenuOpened(".p-products-search", 992);
  tabBtnSlickFixed(); // goToAnchor();
}); //呼叫function-捲動

$(window).scroll(function () {
  goTopSticky(); // sideLinkSticky();
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjQDQuMC9hc3NldHMvanMvbWFpbi5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImVsIiwicXVlcnlTZWxlY3RvciIsImFqYXgiLCJ1cmwiLCJtZXRob2QiLCJkYXRhVHlwZSIsImRvbmUiLCJkYXRhIiwiaHRtbCIsImhlYWRlckZ1bmN0aW9uIiwiZ29Ub0FuY2hvciIsInRvZ2dsZVNpZGVMaW5rIiwiZ29Ub3AiLCJnb1RvcFN0aWNreSIsInRvb2xzTGlzdGVuZXIiLCJ3aW5kb3ciLCJhZGRFdmVudExpc3RlbmVyIiwiZSIsImtleUNvZGUiLCJib2R5IiwiY2xhc3NMaXN0IiwicmVtb3ZlIiwiYWRkIiwiZWxUdyIsImVsRW4iLCJ0b2dnbGUiLCJkb2N1bWVudEVsZW1lbnQiLCJpbm5lcldpZHRoIiwib25jbGljayIsInByZXZlbnREZWZhdWx0IiwiYW5pbWF0ZSIsInNjcm9sbFRvcCIsImZvb3RlckhlaWdodFZhciIsImNsaWVudEhlaWdodCIsInNjcm9sbEhlaWdodFZhciIsInNjcm9sbEhlaWdodCIsIndpbmRvd0hlaWdodFZhciIsImlubmVySGVpZ2h0Iiwic2Nyb2xsVG9wVmFyIiwiaGVhZGVySGVpZ2h0IiwiaGVpZ2h0IiwiY2xpY2siLCJ0YXJnZXQiLCJhdHRyIiwidGFyZ2V0UG9zIiwib2Zmc2V0IiwidG9wIiwidHJpZ2dlcjAyIiwidGFyZ2V0MDIiLCJBY2NvcmRpb24iLCJzaWJsaW5ncyIsInByb3RvdHlwZSIsImluaXQiLCJ0cmlnZ2VycyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJBcnJheSIsInNsaWNlIiwiY2FsbCIsImZvckVhY2giLCJ0cmlnZ2VyIiwiZXZlbnQiLCJmaW5kIiwidG9nZ2xlQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImF0dCIsImdldEF0dHJpYnV0ZSIsIm5vZGVMaXN0VG9BcnJheSIsIm5vZGVMaXN0Q29sbGVjdGlvbiIsInRvZ2dsZVZpc2lhYmxlIiwibWVkaWFRdWVyeSIsImhhc01lZGlhUXVlcnkiLCJpc01vYmlsZSIsImNsaWNrQ29uZmlybSIsImNyZWF0ZUFjY29yZGlvbiIsInN0ciIsInNob3dBbGxUYWIiLCJmaXJzdCIsImFkZENsYXNzIiwicGFyZW50IiwidG9nZ2xlcEZpbHRlclN1Ym1lbnUiLCJ1bmRlZmluZWQiLCJjb25maXJtSWZEb3VibGVNZW51T3BlbmVkIiwiaGFzQ2xhc3MiLCJTbGljayIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1BlclJvdyIsInJvd3MiLCJyZXNwb25zaXZlIiwic2xpY2siLCJhcnJvd3MiLCJwcmV2QXJyb3ciLCJuZXh0QXJyb3ciLCJzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUiLCJ0cmlnZ2VyVGQiLCJudW0wMSIsIm51bTAyIiwibnVtMDMiLCJ0b3RhbE51bSIsImNvbnRhaW5zIiwic3R5bGUiLCJ0b2dnbGVTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUiLCJjcmVhdGVTbGlja3MiLCJ0YXJnZXRzIiwiYnJlYWtwb2ludCIsInNldHRpbmdzIiwicmlvbmV0U2xpY2siLCJsYXp5TG9hZCIsImRvdHMiLCJ0YWJCdG5TbGljayIsInRhYkxlbmd0aCIsImxlbmd0aCIsInNsaWRlc1RvU2hvd051bSIsInNsaWRlc1RvU2Nyb2xsIiwiaW5maW5pdGUiLCJ2YXJpYWJsZVdpZHRoIiwidGFiQnRuU2xpY2tGaXhlZCIsInRhYkJ0blNsaWNrTWVtYmVycyIsInRhYkJ0blNsaWNrRml4ZWRNZW1iZXJzIiwid2FycmFudHlMaW5rIiwidG9nZ2xlTW9iaWxlTWVudSIsInRvZ2dsZU1vYmlsZVN1Ym1lbnUiLCJuZXh0RWxlbWVudFNpYmxpbmciLCJwYXJlbnRzIiwic2liaW5nTW9iaWxlU3VibWVudSIsIm9uIiwidG9nZ2xlUGNIb3ZlclN0YXRlIiwiaXRlbSIsIm9ic2VydmVyIiwibG96YWQiLCJvYnNlcnZlIiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwiaW5kZXhPZiIsImFwcFZlcnNpb24iLCJpZnJhbWUiLCJzZXRBdHRyaWJ1dGUiLCJhdXRvRml4SGVpZ2h0IiwiY29uIiwidGhpc0hlaWdodCIsIm1heEhlaWdodCIsImNsaWVudFdpZHRoIiwidGV4dEhpZGUiLCJudW0iLCJ0eHQiLCJpbm5lclRleHQiLCJ0eHRDb250ZW50Iiwic3Vic3RyaW5nIiwiaW5uZXJIVE1MIiwiY2xlYXJDaGVja0JveCIsImJsdXIiLCJjaGVja2VkIiwiY2FyZENvbnRlbnRGdW5jdGlvbiIsInRvb2xUaXBzIiwidG9vbHRpcCIsInBsYWNlbWVudCIsImdldFRvb3RpcHNJZCIsImFvcyIsIkFPUyIsIm9uY2UiLCJ5dEhhbmRsZXIiLCJpbmRleCIsInRyaWdnZXJJbmRleCIsImZpbHRlciIsImNhcmRKdXN0aWZ5Q29udGVudCIsImkiLCJlbENoaWxkcmVuTGVuZ3RoIiwiZXEiLCJjaGlsZHJlbiIsInN0b3JlRmlsdGVyTm90aWZpY2F0aW9uIiwiaW5wdXRDb250YWluZXIiLCJ0YXJnZXRFbCIsImNoZWNrZWROdW0iLCJjbGVhckFsbEJ0bkVsIiwiZGF0ZU1vYmlzY3JvbGwiLCJtb2Jpc2Nyb2xsIiwiZGF0ZSIsInRoZW1lIiwiZGVmYXVsdHMiLCJtb2RlIiwiZGlzcGxheSIsImxhbmciLCJzZWxlY3RVUkwiLCJsb2NhdGlvbiIsImhyZWYiLCJvcHRpb25zIiwic2VsZWN0ZWRJbmRleCIsInZhbHVlIiwicG9wdXBTaG93IiwiaWQiLCJwb3B1cEhpZGUiLCJvbmxvYWQiLCJyZXNpemUiLCJzY3JvbGwiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBQSxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVk7QUFDNUIsTUFBSUMsRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsb0JBQXZCLENBQVQ7O0FBQ0EsTUFBSUQsRUFBSixFQUFRO0FBQ04sUUFBSUgsQ0FBQyxDQUFDLFNBQUQsQ0FBTCxFQUFrQjtBQUNoQkEsT0FBQyxDQUFDSyxJQUFGLENBQU87QUFDTEMsV0FBRyxFQUFFLG1CQURBO0FBRUxDLGNBQU0sRUFBRSxLQUZIO0FBR0xDLGdCQUFRLEVBQUU7QUFITCxPQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3RCVixTQUFDLENBQUMsU0FBRCxDQUFELENBQWFXLElBQWIsQ0FBa0JELElBQWxCO0FBQ0FFLHNCQUFjO0FBQ2RDLGtCQUFVO0FBQ1gsT0FSRDtBQVNEOztBQUNELFFBQUliLENBQUMsQ0FBQyxXQUFELENBQUwsRUFBb0I7QUFDbEJBLE9BQUMsQ0FBQ0ssSUFBRixDQUFPO0FBQ0xDLFdBQUcsRUFBRSxxQkFEQTtBQUVMQyxjQUFNLEVBQUUsS0FGSDtBQUdMQyxnQkFBUSxFQUFFO0FBSEwsT0FBUCxFQUlHQyxJQUpILENBSVEsVUFBVUMsSUFBVixFQUFnQjtBQUN0QlYsU0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlVyxJQUFmLENBQW9CRCxJQUFwQjtBQUNBSSxzQkFBYztBQUNmLE9BUEQ7QUFRRDs7QUFDRCxRQUFJZCxDQUFDLENBQUMsU0FBRCxDQUFMLEVBQWtCO0FBQ2hCQSxPQUFDLENBQUNLLElBQUYsQ0FBTztBQUNMQyxXQUFHLEVBQUUsbUJBREE7QUFFTEMsY0FBTSxFQUFFLEtBRkg7QUFHTEMsZ0JBQVEsRUFBRTtBQUhMLE9BQVAsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLElBQVYsRUFBZ0I7QUFDdEJWLFNBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYVcsSUFBYixDQUFrQkQsSUFBbEI7QUFDQUssYUFBSztBQUNMQyxtQkFBVztBQUNaLE9BUkQ7QUFTRDtBQUNGO0FBRUYsQ0FyQ0QsRSxDQXNDQTs7QUFDQWhCLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBWTtBQUM1QixNQUFJQyxFQUFFLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixpQkFBdkIsQ0FBVDs7QUFDQSxNQUFJRCxFQUFKLEVBQVE7QUFDTixRQUFJSCxDQUFDLENBQUMsV0FBRCxDQUFMLEVBQW9CO0FBQ2xCQSxPQUFDLENBQUNLLElBQUYsQ0FBTztBQUNMQyxXQUFHLEVBQUUseUJBREE7QUFFTEMsY0FBTSxFQUFFLEtBRkg7QUFHTEMsZ0JBQVEsRUFBRTtBQUhMLE9BQVAsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLElBQVYsRUFBZ0I7QUFDdEJWLFNBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZVcsSUFBZixDQUFvQkQsSUFBcEI7QUFDQUUsc0JBQWM7QUFDZEMsa0JBQVU7QUFDWCxPQVJEO0FBU0Q7O0FBQ0QsUUFBSWIsQ0FBQyxDQUFDLGFBQUQsQ0FBTCxFQUFzQjtBQUNwQkEsT0FBQyxDQUFDSyxJQUFGLENBQU87QUFDTEMsV0FBRyxFQUFFLDJCQURBO0FBRUxDLGNBQU0sRUFBRSxLQUZIO0FBR0xDLGdCQUFRLEVBQUU7QUFITCxPQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3RCVixTQUFDLENBQUMsYUFBRCxDQUFELENBQWlCVyxJQUFqQixDQUFzQkQsSUFBdEI7QUFDQUksc0JBQWM7QUFDZixPQVBEO0FBUUQ7O0FBQ0QsUUFBSWQsQ0FBQyxDQUFDLFdBQUQsQ0FBTCxFQUFvQjtBQUNsQkEsT0FBQyxDQUFDSyxJQUFGLENBQU87QUFDTEMsV0FBRyxFQUFFLHlCQURBO0FBRUxDLGNBQU0sRUFBRSxLQUZIO0FBR0xDLGdCQUFRLEVBQUU7QUFITCxPQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3RCVixTQUFDLENBQUMsV0FBRCxDQUFELENBQWVXLElBQWYsQ0FBb0JELElBQXBCO0FBQ0FLLGFBQUs7QUFDTEMsbUJBQVc7QUFDWixPQVJEO0FBU0Q7QUFDRjtBQUNGLENBcENEOztBQXNDQSxTQUFTQyxhQUFULEdBQXlCO0FBQ3ZCQyxRQUFNLENBQUNDLGdCQUFQLENBQXdCLFNBQXhCLEVBQW1DLFVBQVVDLENBQVYsRUFBYTtBQUM5QyxRQUFJQSxDQUFDLENBQUNDLE9BQUYsS0FBYyxDQUFsQixFQUFxQjtBQUNuQnBCLGNBQVEsQ0FBQ3FCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkMsTUFBeEIsQ0FBK0IsYUFBL0I7QUFDQXZCLGNBQVEsQ0FBQ3FCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkUsR0FBeEIsQ0FBNEIsZ0JBQTVCO0FBQ0Q7QUFDRixHQUxEO0FBTUFQLFFBQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsV0FBeEIsRUFBcUMsVUFBVUMsQ0FBVixFQUFhO0FBQ2hEbkIsWUFBUSxDQUFDcUIsSUFBVCxDQUFjQyxTQUFkLENBQXdCQyxNQUF4QixDQUErQixnQkFBL0I7QUFDQXZCLFlBQVEsQ0FBQ3FCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkUsR0FBeEIsQ0FBNEIsYUFBNUI7QUFDRCxHQUhEO0FBSUQ7O0FBRUQsU0FBU1gsY0FBVCxHQUEwQjtBQUN4QixNQUFJWSxJQUFJLEdBQUd6QixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsb0JBQXZCLENBQVg7QUFDQSxNQUFJdUIsSUFBSSxHQUFHMUIsUUFBUSxDQUFDRyxhQUFULENBQXVCLGlCQUF2QixDQUFYO0FBQ0FILFVBQVEsQ0FBQ0csYUFBVCxDQUF1QixvQkFBdkIsRUFBNkNlLGdCQUE3QyxDQUE4RCxPQUE5RCxFQUF1RSxZQUFZO0FBQ2pGLFNBQUtJLFNBQUwsQ0FBZUssTUFBZixDQUFzQixtQkFBdEI7O0FBQ0EsUUFBSUYsSUFBSixFQUFVO0FBQ1J6QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsV0FBdkIsRUFBb0NtQixTQUFwQyxDQUE4Q0ssTUFBOUMsQ0FBcUQsbUJBQXJEO0FBQ0Q7O0FBQ0QsUUFBSUQsSUFBSixFQUFVO0FBQ1IxQixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsYUFBdkIsRUFBc0NtQixTQUF0QyxDQUFnREssTUFBaEQsQ0FBdUQsbUJBQXZEO0FBQ0Q7O0FBQ0QzQixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsT0FBdkIsRUFBZ0NtQixTQUFoQyxDQUEwQ0ssTUFBMUMsQ0FBaUQsbUJBQWpEO0FBQ0EzQixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0JtQixTQUEvQixDQUF5Q0ssTUFBekMsQ0FBZ0QsaUJBQWhELEVBVGlGLENBVWpGOztBQUNBM0IsWUFBUSxDQUFDNEIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0F2QixZQUFRLENBQUNHLGFBQVQsQ0FBdUIscUJBQXZCLEVBQThDbUIsU0FBOUMsQ0FBd0RDLE1BQXhELENBQStELGVBQS9EO0FBQ0F2QixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsZ0JBQXZCLEVBQXlDbUIsU0FBekMsQ0FBbURDLE1BQW5ELENBQTBELGVBQTFELEVBYmlGLENBY2pGOztBQUNBdkIsWUFBUSxDQUFDRyxhQUFULENBQXVCLGlCQUF2QixFQUEwQ21CLFNBQTFDLENBQW9ESyxNQUFwRCxDQUEyRCxtQkFBM0Q7QUFDRCxHQWhCRDtBQWlCQVYsUUFBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFFBQUlELE1BQU0sQ0FBQ1ksVUFBUCxJQUFxQixJQUF6QixFQUErQjtBQUM3QjdCLGNBQVEsQ0FBQ0csYUFBVCxDQUF1QixvQkFBdkIsRUFBNkNtQixTQUE3QyxDQUF1REMsTUFBdkQsQ0FBOEQsbUJBQTlEOztBQUNBLFVBQUlFLElBQUosRUFBVTtBQUNSekIsZ0JBQVEsQ0FBQ0csYUFBVCxDQUF1QixXQUF2QixFQUFvQ21CLFNBQXBDLENBQThDQyxNQUE5QyxDQUFxRCxtQkFBckQ7QUFDRDs7QUFDRCxVQUFJRyxJQUFKLEVBQVU7QUFDUjFCLGdCQUFRLENBQUNHLGFBQVQsQ0FBdUIsYUFBdkIsRUFBc0NtQixTQUF0QyxDQUFnREMsTUFBaEQsQ0FBdUQsbUJBQXZEO0FBQ0Q7O0FBQ0R2QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsT0FBdkIsRUFBZ0NtQixTQUFoQyxDQUEwQ0MsTUFBMUMsQ0FBaUQsbUJBQWpEO0FBQ0F2QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0JtQixTQUEvQixDQUF5Q0MsTUFBekMsQ0FBZ0QsaUJBQWhELEVBVDZCLENBVTdCOztBQUNBdkIsY0FBUSxDQUFDRyxhQUFULENBQXVCLGlCQUF2QixFQUEwQ21CLFNBQTFDLENBQW9EQyxNQUFwRCxDQUEyRCxtQkFBM0Q7QUFDRDtBQUNGLEdBZEQ7QUFlRDs7QUFFRCxTQUFTVCxLQUFULEdBQWlCO0FBQ2ZkLFVBQVEsQ0FBQ0csYUFBVCxDQUF1QixRQUF2QixFQUFpQzJCLE9BQWpDLEdBQTJDLFVBQVVYLENBQVYsRUFBYTtBQUN0REEsS0FBQyxDQUFDWSxjQUFGO0FBQ0FoQyxLQUFDLENBQUMsWUFBRCxDQUFELENBQWdCaUMsT0FBaEIsQ0FBd0I7QUFDcEJDLGVBQVMsRUFBRTtBQURTLEtBQXhCLEVBR0UsR0FIRjtBQUtELEdBUEQ7QUFRRDs7QUFFRCxTQUFTbEIsV0FBVCxHQUF1QjtBQUNyQixNQUFJYixFQUFFLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixpQkFBdkIsQ0FBVCxDQURxQixDQUVyQjs7QUFDQSxNQUFJK0IsZUFBZSxHQUFHbEMsUUFBUSxDQUFDRyxhQUFULENBQXVCLFdBQXZCLEVBQW9DZ0MsWUFBMUQsQ0FIcUIsQ0FJckI7QUFDQTs7QUFDQSxNQUFJQyxlQUFlLEdBQUdwQyxRQUFRLENBQUM0QixlQUFULENBQXlCUyxZQUEvQyxDQU5xQixDQU9yQjtBQUNBOztBQUNBLE1BQUlDLGVBQWUsR0FBR3JCLE1BQU0sQ0FBQ3NCLFdBQTdCLENBVHFCLENBVXJCO0FBQ0E7O0FBQ0EsTUFBSUMsWUFBWSxHQUFHeEMsUUFBUSxDQUFDNEIsZUFBVCxDQUF5QkssU0FBNUMsQ0FacUIsQ0FhckI7QUFDQTtBQUNBOztBQUNBLE1BQUkvQixFQUFKLEVBQVE7QUFDTixRQUFJa0MsZUFBZSxJQUFJRSxlQUFlLEdBQUdFLFlBQWxCLEdBQWlDTixlQUF4RCxFQUF5RTtBQUN2RWhDLFFBQUUsQ0FBQ29CLFNBQUgsQ0FBYUMsTUFBYixDQUFvQixXQUFwQjtBQUNELEtBRkQsTUFFTztBQUNMckIsUUFBRSxDQUFDb0IsU0FBSCxDQUFhRSxHQUFiLENBQWlCLFdBQWpCO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFNBQVNaLFVBQVQsR0FBc0I7QUFDcEIsTUFBSTZCLFlBQVksR0FBRyxFQUFuQjtBQUNBeEIsUUFBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFdBQU91QixZQUFZLEdBQUcxQyxDQUFDLENBQUMsV0FBRCxDQUFELENBQWUyQyxNQUFmLEVBQXRCO0FBQ0QsR0FGRDtBQUdBM0MsR0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0I0QyxLQUFwQixDQUEwQixVQUFVeEIsQ0FBVixFQUFhO0FBQ3JDQSxLQUFDLENBQUNZLGNBQUY7QUFDQSxRQUFJYSxNQUFNLEdBQUc3QyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVE4QyxJQUFSLENBQWEsTUFBYixDQUFiO0FBQ0EsUUFBSUMsU0FBUyxHQUFHL0MsQ0FBQyxDQUFDNkMsTUFBRCxDQUFELENBQVVHLE1BQVYsR0FBbUJDLEdBQW5DO0FBQ0EsUUFBSVAsWUFBWSxHQUFHMUMsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlMkMsTUFBZixFQUFuQjtBQUNBM0MsS0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlaUMsT0FBZixDQUF1QjtBQUNyQkMsZUFBUyxFQUFFYSxTQUFTLEdBQUdMO0FBREYsS0FBdkIsRUFFRyxJQUZIO0FBR0EsUUFBSVEsU0FBUyxHQUFHakQsUUFBUSxDQUFDRyxhQUFULENBQXVCLFlBQXZCLENBQWhCO0FBQ0EsUUFBSStDLFFBQVEsR0FBR2xELFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixPQUF2QixDQUFmO0FBQ0E4QyxhQUFTLENBQUMzQixTQUFWLENBQW9CQyxNQUFwQixDQUEyQixlQUEzQjtBQUNBMkIsWUFBUSxDQUFDNUIsU0FBVCxDQUFtQkMsTUFBbkIsQ0FBMEIsZUFBMUI7QUFDQXZCLFlBQVEsQ0FBQzRCLGVBQVQsQ0FBeUJOLFNBQXpCLENBQW1DQyxNQUFuQyxDQUEwQyxlQUExQztBQUNELEdBYkQ7QUFjRCxDLENBQ0Q7OztBQUNBLFNBQVM0QixTQUFULENBQW1CakQsRUFBbkIsRUFBdUIwQyxNQUF2QixFQUErQlEsUUFBL0IsRUFBeUM7QUFDdkMsT0FBS2xELEVBQUwsR0FBVUEsRUFBVjtBQUNBLE9BQUswQyxNQUFMLEdBQWNBLE1BQWQ7QUFDQSxPQUFLUSxRQUFMLEdBQWdCQSxRQUFoQjtBQUNEOztBQUNERCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JDLElBQXBCLEdBQTJCLFlBQVk7QUFDckMsTUFBSUMsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIsS0FBS3RELEVBQS9CLENBQWY7QUFDQSxNQUFJMEMsTUFBTSxHQUFHLEtBQUtBLE1BQWxCO0FBQ0EsTUFBSVEsUUFBUSxHQUFHLEtBQUtBLFFBQXBCO0FBQ0FLLE9BQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCSixRQUEzQixFQUFxQ0ssT0FBckMsQ0FBNkMsVUFBVUMsT0FBVixFQUFtQjtBQUM5REEsV0FBTyxDQUFDM0MsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1QzRDLFdBQUssQ0FBQy9CLGNBQU47O0FBQ0EsVUFBSXFCLFFBQVEsSUFBSSxJQUFoQixFQUFzQjtBQUNwQixZQUFJLEtBQUtqRCxhQUFMLENBQW1CeUMsTUFBbkIsTUFBK0IsSUFBbkMsRUFBeUM7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTdDLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWdFLElBQVIsQ0FBYW5CLE1BQWIsRUFBcUJvQixXQUFyQixDQUFpQyxzQkFBakMsRUFMdUMsQ0FNdkM7O0FBQ0FqRSxXQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRSxXQUFSLENBQW9CLHVCQUFwQjtBQUNBakUsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRcUQsUUFBUixHQUFtQlcsSUFBbkIsQ0FBd0JuQixNQUF4QixFQUFnQ3FCLFdBQWhDLENBQTRDLHNCQUE1QyxFQVJ1QyxDQVN2Qzs7QUFDQWxFLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXFELFFBQVIsR0FBbUJhLFdBQW5CLENBQStCLHVCQUEvQjtBQUNELFNBWm1CLENBYXBCO0FBQ0E7OztBQUNBLFlBQUlDLEdBQUcsR0FBR25FLENBQUMsQ0FBQzhELE9BQUQsQ0FBRCxDQUFXaEIsSUFBWCxDQUFnQixhQUFoQixDQUFWO0FBQ0E5QyxTQUFDLENBQUNtRSxHQUFELENBQUQsQ0FBT0YsV0FBUCxDQUFtQixzQkFBbkIsRUFBMkNaLFFBQTNDLEdBQXNEYSxXQUF0RCxDQUFrRSxzQkFBbEUsRUFoQm9CLENBaUJwQjtBQUNBO0FBQ0E7QUFDRCxPQXBCRCxNQW9CTztBQUNMLFlBQUksS0FBSzlELGFBQUwsQ0FBbUJ5QyxNQUFuQixNQUErQixJQUFuQyxFQUF5QztBQUN2QyxlQUFLekMsYUFBTCxDQUFtQnlDLE1BQW5CLEVBQTJCdEIsU0FBM0IsQ0FBcUNLLE1BQXJDLENBQ0Usc0JBREY7QUFHRDs7QUFDRDNCLGdCQUFRLENBQUNHLGFBQVQsQ0FBdUIwRCxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsYUFBckIsQ0FBdkIsRUFBNEQ3QyxTQUE1RCxDQUFzRUssTUFBdEUsQ0FBNkUsc0JBQTdFO0FBQ0FrQyxlQUFPLENBQUN2QyxTQUFSLENBQWtCSyxNQUFsQixDQUF5QixzQkFBekI7QUFDRDtBQUNGLEtBL0JEO0FBZ0NELEdBakNEO0FBa0NELENBdENEOztBQXdDQSxTQUFTeUMsZUFBVCxDQUF5QkMsa0JBQXpCLEVBQTZDO0FBQzNDLFNBQU9aLEtBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCVSxrQkFBM0IsQ0FBUDtBQUNELEMsQ0FDRDs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUEsU0FBU0MsY0FBVCxDQUF3QnBFLEVBQXhCLEVBQTRCMEMsTUFBNUIsRUFBb0MyQixVQUFwQyxFQUFnRDtBQUM5QyxNQUFJaEIsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEJ0RCxFQUExQixDQUFmO0FBQ0EsTUFBSTBDLE1BQU0sR0FBRzVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QnlDLE1BQXZCLENBQWI7O0FBQ0EsTUFBSUEsTUFBSixFQUFZO0FBQ1Z3QixtQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkRBLGFBQU8sQ0FBQzNDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUM0QyxhQUFLLENBQUMvQixjQUFOO0FBQ0EsYUFBS1QsU0FBTCxDQUFlSyxNQUFmLENBQXNCLFdBQXRCO0FBQ0FpQixjQUFNLENBQUN0QixTQUFQLENBQWlCSyxNQUFqQixDQUF3QixXQUF4QjtBQUNBLFlBQUk2QyxhQUFhLEdBQUdELFVBQXBCOztBQUNBLFlBQUlDLGFBQWEsS0FBSyxFQUF0QixFQUEwQjtBQUN4QixjQUFJQyxRQUFRLEdBQUd4RCxNQUFNLENBQUNZLFVBQVAsR0FBb0IwQyxVQUFuQzs7QUFDQSxjQUFJRSxRQUFKLEVBQWM7QUFDWnpFLG9CQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixDQUFtQ0ssTUFBbkMsQ0FBMEMsdUJBQTFDO0FBQ0Q7QUFDRixTQUxELE1BS087QUFDTDNCLGtCQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixDQUFtQ0MsTUFBbkMsQ0FBMEMsdUJBQTFDO0FBQ0Q7O0FBQ0ROLGNBQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBWTtBQUM1QyxjQUFJRCxNQUFNLENBQUNZLFVBQVAsSUFBcUIwQyxVQUF6QixFQUFxQztBQUNuQ3ZFLG9CQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixDQUFtQ0MsTUFBbkMsQ0FBMEMsdUJBQTFDO0FBQ0Q7QUFDRixTQUpEO0FBS0QsT0FsQkQ7QUFtQkQsS0FwQkQ7QUFxQkQ7QUFDRjtBQUNEOztBQUNBOzs7QUFDQSxTQUFTbUQsWUFBVCxDQUFzQnhFLEVBQXRCLEVBQTBCMEMsTUFBMUIsRUFBa0M7QUFDaEMsTUFBSVcsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEJ0RCxFQUExQixDQUFmO0FBQ0EsTUFBSTBDLE1BQU0sR0FBRzVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QnlDLE1BQXZCLENBQWI7O0FBQ0EsTUFBSUEsTUFBSixFQUFZO0FBQ1Z3QixtQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkRBLGFBQU8sQ0FBQzNDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUM0QyxhQUFLLENBQUMvQixjQUFOO0FBQ0FhLGNBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJDLE1BQWpCLENBQXdCLFdBQXhCO0FBQ0F2QixnQkFBUSxDQUFDNEIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLHVCQUExQztBQUNELE9BSkQ7QUFLRCxLQU5EO0FBT0Q7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVNvRCxlQUFULEdBQTJCO0FBQ3pCLE1BQUkzRSxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsa0JBQXZCLENBQUosRUFBZ0Q7QUFDOUMsUUFBSWdELFNBQUosQ0FBYyxrQkFBZCxFQUFrQyx1QkFBbEMsRUFBMkQsSUFBM0QsRUFBaUVHLElBQWpFO0FBQ0Q7QUFDRixDLENBQ0Q7OztBQUNBLElBQUlzQixHQUFHLEdBQUcsQ0FBVjs7QUFFQSxTQUFTQyxVQUFULEdBQXNCO0FBQ3BCLE1BQUk1RCxNQUFNLENBQUNZLFVBQVAsR0FBb0IsR0FBcEIsSUFBMkIrQyxHQUFHLElBQUksQ0FBdEMsRUFBeUM7QUFDdkM3RSxLQUFDLENBQUMsV0FBRCxDQUFELENBQWVrRSxXQUFmLENBQTJCLGFBQTNCLEVBQTBDYSxLQUExQyxHQUFrREMsUUFBbEQsQ0FBMkQsYUFBM0Q7QUFDQWhGLEtBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JrRSxXQUFsQixDQUE4QixRQUE5QixFQUF3Q2UsTUFBeEMsR0FBaURGLEtBQWpELEdBQXlEZixJQUF6RCxDQUE4RCxjQUE5RCxFQUE4RWdCLFFBQTlFLENBQXVGLFFBQXZGO0FBQ0FILE9BQUcsR0FBRyxDQUFOO0FBQ0Q7O0FBQ0QsTUFBSTNELE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQixHQUFwQixJQUEyQitDLEdBQUcsSUFBSSxDQUF0QyxFQUF5QztBQUN2QzdFLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZWdGLFFBQWYsQ0FBd0IsYUFBeEI7QUFDQUgsT0FBRyxHQUFHLENBQU47QUFDRCxHQVRtQixDQVVwQjs7QUFDRDs7QUFFRCxTQUFTSyxvQkFBVCxHQUFnQztBQUM5QjtBQUNBO0FBQ0EsTUFBSWpGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1Qiw2QkFBdkIsQ0FBSixFQUEyRDtBQUN6RCxRQUFJZ0QsU0FBSixDQUFjLDZCQUFkLEVBQTZDK0IsU0FBN0MsRUFBd0QsS0FBeEQsRUFBK0Q1QixJQUEvRDtBQUNEOztBQUNELE1BQUlDLFFBQVEsR0FBR3ZELFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCLGtCQUExQixDQUFmO0FBQ0FZLGlCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxRQUFJM0QsRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUIwRCxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsYUFBckIsQ0FBdkIsQ0FBVDtBQUNBTixXQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDLFVBQUloQixFQUFFLENBQUNDLGFBQUgsQ0FBaUIsNkJBQWpCLENBQUosRUFBcUQ7QUFDbkRELFVBQUUsQ0FBQ0MsYUFBSCxDQUFpQiw2QkFBakIsRUFBZ0RtQixTQUFoRCxDQUEwREMsTUFBMUQsQ0FBaUUsc0JBQWpFO0FBQ0FyQixVQUFFLENBQUNDLGFBQUgsQ0FBaUIsNkJBQWpCLEVBQWdEbUIsU0FBaEQsQ0FBMERDLE1BQTFELENBQWlFLHNCQUFqRTtBQUNEO0FBQ0YsS0FMRDtBQU1ELEdBUkQsRUFQOEIsQ0FnQjlCO0FBQ0E7O0FBQ0ErQyxnQkFBYyxDQUFDLFFBQUQsRUFBVyxvQkFBWCxFQUFpQyxFQUFqQyxDQUFkO0FBQ0FBLGdCQUFjLENBQUMsd0JBQUQsRUFBMkIsb0JBQTNCLEVBQWlELEdBQWpELENBQWQ7QUFDQUksY0FBWSxDQUFDLGFBQUQsRUFBZ0Isb0JBQWhCLENBQVosQ0FwQjhCLENBcUI5Qjs7QUFDQUosZ0JBQWMsQ0FBQyxpQkFBRCxFQUFvQixrQkFBcEIsRUFBd0MsR0FBeEMsQ0FBZDtBQUNBQSxnQkFBYyxDQUFDLFFBQUQsRUFBVyxrQkFBWCxFQUErQixFQUEvQixDQUFkO0FBQ0FJLGNBQVksQ0FBQyxhQUFELEVBQWdCLGtCQUFoQixDQUFaLENBeEI4QixDQXlCOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUQ7O0FBRUQsU0FBU1MseUJBQVQsQ0FBbUNqRixFQUFuQyxFQUF1Q3FFLFVBQXZDLEVBQW1EO0FBQ2pELE1BQUl0RCxNQUFNLENBQUNZLFVBQVAsR0FBb0IwQyxVQUFwQixJQUFrQ3hFLENBQUMsQ0FBQ0csRUFBRCxDQUFELENBQU1rRixRQUFOLENBQWUsV0FBZixDQUFsQyxJQUFpRXBGLFFBQVEsQ0FBQzRCLGVBQVQsQ0FBeUJOLFNBQXpCLElBQXNDLEVBQTNHLEVBQStHO0FBQzdHdEIsWUFBUSxDQUFDNEIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNFLEdBQW5DLENBQXVDLGVBQXZDO0FBQ0Q7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVM2RCxLQUFULENBQWVuRixFQUFmLEVBQW1Cb0YsWUFBbkIsRUFBaUNDLFlBQWpDLEVBQStDQyxJQUEvQyxFQUFxREMsVUFBckQsRUFBaUU7QUFDL0QsT0FBS3ZGLEVBQUwsR0FBVUEsRUFBVjtBQUNBLE9BQUtvRixZQUFMLEdBQW9CQSxZQUFwQjtBQUNBLE9BQUtDLFlBQUwsR0FBb0JBLFlBQXBCO0FBQ0EsT0FBS0MsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsT0FBS0MsVUFBTCxHQUFrQkEsVUFBbEI7QUFDRDs7QUFBQTs7QUFFREosS0FBSyxDQUFDaEMsU0FBTixDQUFnQkMsSUFBaEIsR0FBdUIsWUFBWTtBQUNqQ3ZELEdBQUMsQ0FBQyxLQUFLRyxFQUFMLEdBQVUsV0FBWCxDQUFELENBQXlCd0YsS0FBekIsQ0FBK0I7QUFDN0JDLFVBQU0sRUFBRSxJQURxQjtBQUU3QkwsZ0JBQVksRUFBRSxLQUFLQSxZQUZVO0FBRzdCQyxnQkFBWSxFQUFFLEtBQUtBLFlBSFU7QUFJN0JDLFFBQUksRUFBRSxLQUFLQSxJQUprQjtBQUs3QkksYUFBUyxzR0FMb0I7QUFNN0JDLGFBQVMsdUdBTm9CO0FBTzdCO0FBQ0E7QUFDQUosY0FBVSxFQUFFLEtBQUtBO0FBVFksR0FBL0I7QUFXRCxDQVpEOztBQWNBLFNBQVNLLDJCQUFULEdBQXVDO0FBQ3JDdkMsVUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIsZ0NBQTFCLENBQVg7QUFDQVksaUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFFBQUlrQyxTQUFTLEdBQUdsQyxPQUFPLENBQUNMLGdCQUFSLENBQXlCLElBQXpCLENBQWhCO0FBQ0EsUUFBSXdDLEtBQUssR0FBR0QsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhNUQsWUFBekI7QUFDQSxRQUFJOEQsS0FBSyxHQUFHRixTQUFTLENBQUMsQ0FBRCxDQUFULENBQWE1RCxZQUF6QjtBQUNBLFFBQUkrRCxLQUFLLEdBQUdILFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTVELFlBQXpCO0FBQ0EsUUFBSWdFLFFBQVEsR0FBR0gsS0FBSyxHQUFHQyxLQUFSLEdBQWdCQyxLQUEvQjs7QUFDQSxRQUFJakYsTUFBTSxDQUFDWSxVQUFQLEdBQW9CLElBQXBCLElBQTRCZ0MsT0FBTyxDQUFDdkMsU0FBUixDQUFrQjhFLFFBQWxCLENBQTJCLFdBQTNCLEtBQTJDLEtBQTNFLEVBQWtGO0FBQ2hGdkMsYUFBTyxDQUFDd0MsS0FBUixDQUFjM0QsTUFBZCxhQUEwQnlELFFBQTFCO0FBQ0QsS0FGRCxNQUVPLElBQUlsRixNQUFNLENBQUNZLFVBQVAsR0FBb0IsSUFBeEIsRUFBOEI7QUFDbkNnQyxhQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLEdBQXVCLEVBQXZCO0FBQ0FtQixhQUFPLENBQUN2QyxTQUFSLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QjtBQUNBc0MsYUFBTyxDQUFDMUQsYUFBUixDQUFzQixHQUF0QixFQUEyQm1CLFNBQTNCLENBQXFDQyxNQUFyQyxDQUE0QyxrQkFBNUM7QUFDRDs7QUFDRE4sVUFBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFVBQUlELE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQixJQUFwQixJQUE0QmdDLE9BQU8sQ0FBQ3ZDLFNBQVIsQ0FBa0I4RSxRQUFsQixDQUEyQixXQUEzQixLQUEyQyxLQUEzRSxFQUFrRjtBQUNoRnZDLGVBQU8sQ0FBQ3dDLEtBQVIsQ0FBYzNELE1BQWQsYUFBMEJ5RCxRQUExQjtBQUNELE9BRkQsTUFFTyxJQUFJbEYsTUFBTSxDQUFDWSxVQUFQLEdBQW9CLElBQXhCLEVBQThCO0FBQ25DZ0MsZUFBTyxDQUFDd0MsS0FBUixDQUFjM0QsTUFBZCxHQUF1QixFQUF2QjtBQUNBbUIsZUFBTyxDQUFDdkMsU0FBUixDQUFrQkMsTUFBbEIsQ0FBeUIsV0FBekI7QUFDQXNDLGVBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsR0FBdEIsRUFBMkJtQixTQUEzQixDQUFxQ0MsTUFBckMsQ0FBNEMsa0JBQTVDO0FBQ0Q7QUFDRixLQVJEO0FBU0QsR0F0QkQ7QUF1QkQ7O0FBRUQsU0FBUytFLDhCQUFULEdBQTBDO0FBQ3hDL0MsVUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIsZ0NBQTFCLENBQVg7QUFDQVksaUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFFBQUlrQyxTQUFTLEdBQUdsQyxPQUFPLENBQUNMLGdCQUFSLENBQXlCLElBQXpCLENBQWhCO0FBQ0EsUUFBSXdDLEtBQUssR0FBR0QsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhNUQsWUFBekI7QUFDQSxRQUFJOEQsS0FBSyxHQUFHRixTQUFTLENBQUMsQ0FBRCxDQUFULENBQWE1RCxZQUF6QjtBQUNBLFFBQUkrRCxLQUFLLEdBQUdILFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTVELFlBQXpCO0FBQ0EsUUFBSWdFLFFBQVEsR0FBR0gsS0FBSyxHQUFHQyxLQUFSLEdBQWdCQyxLQUEvQjtBQUNBckMsV0FBTyxDQUFDMUQsYUFBUixDQUFzQixnQkFBdEIsRUFBd0NlLGdCQUF4QyxDQUF5RCxPQUF6RCxFQUFrRSxZQUFZO0FBQzVFLFVBQUlELE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQixJQUFwQixJQUE0QmdDLE9BQU8sQ0FBQ3dDLEtBQVIsQ0FBYzNELE1BQWQsSUFBd0IsRUFBeEQsRUFBNEQ7QUFDMURtQixlQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLGFBQTBCeUQsUUFBMUI7QUFDQXRDLGVBQU8sQ0FBQ3ZDLFNBQVIsQ0FBa0JDLE1BQWxCLENBQXlCLFdBQXpCO0FBQ0FzQyxlQUFPLENBQUMxRCxhQUFSLENBQXNCLEdBQXRCLEVBQTJCbUIsU0FBM0IsQ0FBcUNDLE1BQXJDLENBQTRDLGtCQUE1QztBQUNELE9BSkQsTUFJTztBQUNMc0MsZUFBTyxDQUFDd0MsS0FBUixDQUFjM0QsTUFBZCxHQUF1QixFQUF2QjtBQUNBbUIsZUFBTyxDQUFDdkMsU0FBUixDQUFrQkUsR0FBbEIsQ0FBc0IsV0FBdEI7QUFDQXFDLGVBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsR0FBdEIsRUFBMkJtQixTQUEzQixDQUFxQ0UsR0FBckMsQ0FBeUMsa0JBQXpDO0FBQ0Q7QUFDRixLQVZEO0FBV0QsR0FqQkQ7QUFrQkQsQyxDQUVEOzs7QUFDQSxTQUFTK0UsWUFBVCxHQUF3QjtBQUN0QixNQUFJdkcsUUFBUSxDQUFDRyxhQUFULENBQXVCLFVBQXZCLENBQUosRUFBd0M7QUFDdEMsUUFBSXFHLE9BQU8sR0FBRyxDQUFDLE9BQUQsRUFBVSxpQkFBVixDQUFkO0FBQ0FBLFdBQU8sQ0FBQzVDLE9BQVIsQ0FBZ0IsVUFBVWhCLE1BQVYsRUFBa0I7QUFDaEMsVUFBSXlDLEtBQUosQ0FBVXpDLE1BQVYsRUFBa0IsQ0FBbEIsRUFBcUIsQ0FBckIsRUFBd0IsQ0FBeEIsRUFBMkIsQ0FBQztBQUN4QjZELGtCQUFVLEVBQUUsR0FEWTtBQUV4QkMsZ0JBQVEsRUFBRTtBQUNScEIsc0JBQVksRUFBRTtBQUROO0FBRmMsT0FBRCxFQU16QjtBQUNFbUIsa0JBQVUsRUFBRSxHQURkO0FBRUVDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUUsQ0FETjtBQUVSSyxnQkFBTSxFQUFFO0FBRkE7QUFGWixPQU55QixDQUEzQixFQWFHckMsSUFiSDtBQWNELEtBZkQ7QUFnQkEsUUFBSStCLEtBQUosQ0FBVSxRQUFWLEVBQW9CLENBQXBCLEVBQXVCLENBQXZCLEVBQTBCLENBQTFCLEVBQTZCLENBQUM7QUFDMUJvQixnQkFBVSxFQUFFLEdBRGM7QUFFMUJDLGNBQVEsRUFBRTtBQUNScEIsb0JBQVksRUFBRSxDQUROO0FBRVJDLG9CQUFZLEVBQUUsQ0FGTjtBQUdSQyxZQUFJLEVBQUU7QUFIRTtBQUZnQixLQUFELEVBUTNCO0FBQ0VpQixnQkFBVSxFQUFFLEdBRGQ7QUFFRUMsY0FBUSxFQUFFO0FBQ1JwQixvQkFBWSxFQUFFLENBRE47QUFFUkMsb0JBQVksRUFBRSxDQUZOO0FBR1JDLFlBQUksRUFBRSxDQUhFO0FBSVJHLGNBQU0sRUFBRTtBQUpBO0FBRlosS0FSMkIsQ0FBN0IsRUFpQkdyQyxJQWpCSDtBQWtCRDtBQUNGOztBQUVELFNBQVNxRCxXQUFULEdBQXVCO0FBQ3JCLE1BQUl6RyxFQUFFLEdBQUcsZ0JBQVQ7O0FBQ0EsTUFBSUYsUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixDQUFKLEVBQWdDO0FBQzlCSCxLQUFDLENBQUNHLEVBQUQsQ0FBRCxDQUFNd0YsS0FBTixDQUFZO0FBQ1Y7QUFDQTtBQUNBSixrQkFBWSxFQUFFLENBSEo7QUFJVkssWUFBTSxFQUFFLElBSkU7QUFLVkMsZUFBUyxxR0FMQztBQU1WQyxlQUFTLHNHQU5DO0FBT1ZlLGNBQVEsRUFBRSxhQVBBO0FBUVZuQixnQkFBVSxFQUFFLENBQUM7QUFDVGdCLGtCQUFVLEVBQUUsR0FESDtBQUVUQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUnVCLGNBQUksRUFBRTtBQUZFO0FBRkQsT0FBRCxFQU9WO0FBQ0VKLGtCQUFVLEVBQUUsR0FEZDtBQUVFQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUkssZ0JBQU0sRUFBRSxLQUZBO0FBR1I7QUFDQWtCLGNBQUksRUFBRTtBQUpFO0FBRlosT0FQVTtBQVJGLEtBQVosRUFEOEIsQ0EyQjlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNEO0FBQ0Y7O0FBRUQsU0FBU0MsV0FBVCxHQUF1QjtBQUNyQixNQUFJNUcsRUFBRSxHQUFHLGdCQUFUOztBQUNBLE1BQUlGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBSixFQUFnQztBQUM5QixRQUFJNkcsU0FBUyxHQUFHL0csUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixFQUEyQnNELGdCQUEzQixDQUE0QyxrQkFBNUMsRUFBZ0V3RCxNQUFoRjtBQUNBLFFBQUlDLGVBQWUsR0FBSUYsU0FBUyxHQUFHLENBQWIsR0FBa0IsQ0FBbEIsR0FBc0JBLFNBQTVDLENBRjhCLENBRzlCOztBQUNBaEgsS0FBQyxDQUFDRyxFQUFELENBQUQsQ0FBTXdGLEtBQU4sQ0FBWTtBQUNWSixrQkFBWSxFQUFFMkIsZUFESjtBQUVWQyxvQkFBYyxFQUFFLENBRk47QUFHVnZCLFlBQU0sRUFBRSxJQUhFO0FBSVZDLGVBQVMscUdBSkM7QUFLVkMsZUFBUyxzR0FMQztBQU1WZSxjQUFRLEVBQUUsYUFOQTtBQU9WTyxjQUFRLEVBQUUsS0FQQTtBQVFWMUIsZ0JBQVUsRUFBRSxDQUFDO0FBQ1RnQixrQkFBVSxFQUFFLElBREg7QUFFVEMsZ0JBQVEsRUFBRTtBQUNScEIsc0JBQVksRUFBRSxDQUROO0FBRVI4Qix1QkFBYSxFQUFFLEtBRlAsQ0FHUjtBQUNBO0FBQ0E7O0FBTFE7QUFGRCxPQUFELEVBU1A7QUFDRFgsa0JBQVUsRUFBRSxHQURYO0FBRURDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUUsQ0FETjtBQUVSOEIsdUJBQWEsRUFBRSxLQUZQLENBR1I7QUFDQTs7QUFKUTtBQUZULE9BVE8sRUFrQlY7QUFDRVgsa0JBQVUsRUFBRSxHQURkO0FBRUVDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUUsQ0FETjtBQUVSOEIsdUJBQWEsRUFBRSxJQUZQO0FBR1I7QUFDQXpCLGdCQUFNLEVBQUU7QUFKQTtBQUZaLE9BbEJVO0FBUkYsS0FBWjtBQXFDRDtBQUNGOztBQUVELFNBQVMwQixnQkFBVCxHQUE0QjtBQUMxQixNQUFJbkgsRUFBRSxHQUFHLGdCQUFUOztBQUNBLE1BQUlGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBSixFQUFnQztBQUM5QixRQUFJNkcsU0FBUyxHQUFHL0csUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixFQUEyQnNELGdCQUEzQixDQUE0QyxrQkFBNUMsRUFBZ0V3RCxNQUFoRixDQUQ4QixDQUU5QjtBQUNBOztBQUNBLFFBQUkvRixNQUFNLENBQUNZLFVBQVAsSUFBcUIsSUFBckIsSUFBNkJrRixTQUFTLElBQUksQ0FBOUMsRUFBaUQ7QUFDL0NoSCxPQUFDLENBQUMsY0FBRCxDQUFELENBQWtCZ0YsUUFBbEIsQ0FBMkIsZ0JBQTNCLEVBRCtDLENBRS9DO0FBQ0Q7O0FBQ0QsUUFBSTlELE1BQU0sQ0FBQ1ksVUFBUCxJQUFxQixHQUFyQixJQUE0QlosTUFBTSxDQUFDWSxVQUFQLEdBQW9CLElBQWhELElBQXdEa0YsU0FBUyxHQUFHLENBQXhFLEVBQTJFO0FBQ3pFaEgsT0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQmdGLFFBQWxCLENBQTJCLGdCQUEzQixFQUR5RSxDQUV6RTtBQUNELEtBWDZCLENBWTlCO0FBQ0E7QUFDQTs7QUFDRDtBQUNGOztBQUVELFNBQVN1QyxrQkFBVCxHQUE4QjtBQUM1QixNQUFJcEgsRUFBRSxHQUFHLHdCQUFUOztBQUNBLE1BQUlGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBSixFQUFnQztBQUM5QkgsS0FBQyxDQUFDRyxFQUFELENBQUQsQ0FBTXdGLEtBQU4sQ0FBWTtBQUNWSixrQkFBWSxFQUFFLENBREo7QUFFVjRCLG9CQUFjLEVBQUUsQ0FGTjtBQUdWdkIsWUFBTSxFQUFFLElBSEU7QUFJVkMsZUFBUyxxR0FKQztBQUtWQyxlQUFTLHNHQUxDO0FBTVZlLGNBQVEsRUFBRSxhQU5BO0FBT1ZPLGNBQVEsRUFBRSxLQVBBO0FBUVYxQixnQkFBVSxFQUFFLENBQUM7QUFDVGdCLGtCQUFVLEVBQUUsR0FESDtBQUVUQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUjhCLHVCQUFhLEVBQUU7QUFGUDtBQUZELE9BQUQsRUFPVjtBQUNFWCxrQkFBVSxFQUFFLEdBRGQ7QUFFRUMsZ0JBQVEsRUFBRTtBQUNScEIsc0JBQVksRUFBRSxDQUROO0FBRVI4Qix1QkFBYSxFQUFFLElBRlA7QUFHUnpCLGdCQUFNLEVBQUU7QUFIQTtBQUZaLE9BUFU7QUFSRixLQUFaO0FBeUJEO0FBQ0Y7O0FBRUQsU0FBUzRCLHVCQUFULEdBQW1DO0FBQ2pDLE1BQUlySCxFQUFFLEdBQUcsd0JBQVQ7O0FBQ0EsTUFBSUYsUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixDQUFKLEVBQWdDO0FBQzlCLFFBQUllLE1BQU0sQ0FBQ1ksVUFBUCxJQUFxQixHQUF6QixFQUE4QjtBQUM1QjlCLE9BQUMsQ0FBQyxxQ0FBRCxDQUFELENBQXlDZ0YsUUFBekMsQ0FBa0QsZ0JBQWxEO0FBQ0Q7QUFDRjtBQUNGLEMsQ0FDRDs7O0FBQ0EsU0FBU3lDLFlBQVQsR0FBd0I7QUFDdEIsTUFBSXhILFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCLHdCQUExQixFQUFvRHdELE1BQXhELEVBQWdFO0FBQzlELFFBQUl6RCxRQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQix3QkFBMUIsQ0FBZjtBQUNBWSxtQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkRBLGFBQU8sQ0FBQzNDLGdCQUFSLENBQXlCLFdBQXpCLEVBQXNDLFlBQVk7QUFDaEQyQyxlQUFPLENBQUMxRCxhQUFSLENBQXNCLFFBQXRCLEVBQWdDbUIsU0FBaEMsQ0FBMENFLEdBQTFDLENBQThDLGFBQTlDO0FBQ0QsT0FGRDtBQUdBcUMsYUFBTyxDQUFDM0MsZ0JBQVIsQ0FBeUIsVUFBekIsRUFBcUMsWUFBWTtBQUMvQzJDLGVBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsUUFBdEIsRUFBZ0NtQixTQUFoQyxDQUEwQ0MsTUFBMUMsQ0FBaUQsYUFBakQ7QUFDRCxPQUZEO0FBR0QsS0FQRDtBQVFEO0FBQ0YsQyxDQUNEOzs7QUFDQSxTQUFTa0csZ0JBQVQsQ0FBMEJsRCxVQUExQixFQUFzQztBQUNwQyxNQUFJVixPQUFPLEdBQUc3RCxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBZDtBQUNBLE1BQUl5QyxNQUFNLEdBQUc1QyxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBYjtBQUVBMEQsU0FBTyxDQUFDM0MsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1QyxTQUFLSSxTQUFMLENBQWVLLE1BQWYsQ0FBc0IsZUFBdEI7QUFDQWlCLFVBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJLLE1BQWpCLENBQXdCLGVBQXhCO0FBQ0EzQixZQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixDQUFtQ0ssTUFBbkMsQ0FBMEMsZUFBMUM7QUFDRCxHQUpEO0FBTUFWLFFBQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBWTtBQUM1QyxRQUFJRCxNQUFNLENBQUNZLFVBQVAsSUFBcUIwQyxVQUF6QixFQUFxQztBQUNuQ1YsYUFBTyxDQUFDdkMsU0FBUixDQUFrQkMsTUFBbEIsQ0FBeUIsZUFBekI7QUFDQXFCLFlBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJDLE1BQWpCLENBQXdCLGVBQXhCO0FBQ0F2QixjQUFRLENBQUM0QixlQUFULENBQXlCTixTQUF6QixDQUFtQ0MsTUFBbkMsQ0FBMEMsZUFBMUM7QUFDRDtBQUNGLEdBTkQ7QUFPRCxDLENBQ0Q7OztBQUNBLFNBQVNtRyxtQkFBVCxDQUE2Qm5ELFVBQTdCLEVBQXlDO0FBQ3ZDLE1BQUloQixRQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQixxQkFBMUIsQ0FBZjtBQUNBLE1BQUlnRCxPQUFPLEdBQUd4RyxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQiwyQkFBMUIsQ0FBZDtBQUNBWSxpQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkQsUUFBSUEsT0FBTyxDQUFDOEQsa0JBQVIsS0FBK0IsSUFBbkMsRUFBeUM7QUFDdkM5RCxhQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxVQUFVQyxDQUFWLEVBQWE7QUFDN0NBLFNBQUMsQ0FBQ1ksY0FBRjtBQUNBLFlBQUlhLE1BQU0sR0FBR2lCLE9BQU8sQ0FBQzhELGtCQUFyQjs7QUFDQSxZQUFJMUcsTUFBTSxDQUFDWSxVQUFQLEdBQW9CMEMsVUFBeEIsRUFBb0M7QUFDbEMzQixnQkFBTSxDQUFDdEIsU0FBUCxDQUFpQkssTUFBakIsQ0FBd0Isa0JBQXhCO0FBQ0FrQyxpQkFBTyxDQUFDMUQsYUFBUixDQUFzQixHQUF0QixFQUEyQm1CLFNBQTNCLENBQXFDSyxNQUFyQyxDQUE0QyxrQkFBNUMsRUFGa0MsQ0FHbEM7O0FBQ0E1QixXQUFDLENBQUM2QyxNQUFELENBQUQsQ0FBVWdGLE9BQVYsR0FBb0J4RSxRQUFwQixHQUErQlcsSUFBL0IsQ0FBb0MsdUJBQXBDLEVBQTZERSxXQUE3RCxDQUF5RSxrQkFBekU7QUFDRDtBQUNGLE9BVEQ7QUFVRDtBQUNGLEdBYkQ7QUFlQWhELFFBQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBWTtBQUM1QyxRQUFJRCxNQUFNLENBQUNZLFVBQVAsSUFBcUIwQyxVQUF6QixFQUFxQztBQUNuQ0gscUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFlBQUlqQixNQUFNLEdBQUdpQixPQUFPLENBQUM4RCxrQkFBckI7QUFDQS9FLGNBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJDLE1BQWpCLENBQXdCLGtCQUF4QjtBQUNBc0MsZUFBTyxDQUFDMUQsYUFBUixDQUFzQixHQUF0QixFQUEyQm1CLFNBQTNCLENBQXFDQyxNQUFyQyxDQUE0QyxrQkFBNUM7QUFDRCxPQUpEO0FBS0Q7QUFDRixHQVJEO0FBU0Q7O0FBRUQsU0FBU3NHLG1CQUFULEdBQStCO0FBQzdCOUgsR0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUIrSCxFQUF6QixDQUE0QixPQUE1QixFQUFxQyxZQUFZO0FBQy9DL0gsS0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRcUQsUUFBUixHQUFtQlcsSUFBbkIsQ0FBd0IsbUJBQXhCLEVBQTZDRSxXQUE3QyxDQUF5RCxrQkFBekQ7QUFDRCxHQUZEO0FBR0Q7O0FBRUQsU0FBUzhELGtCQUFULENBQTRCeEQsVUFBNUIsRUFBd0M7QUFDdEMsTUFBSWhCLFFBQVEsR0FBR3ZELFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCLHFCQUExQixDQUFmO0FBQ0FZLGlCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxRQUFJQSxPQUFPLENBQUMxRCxhQUFSLENBQXNCLG1CQUF0QixNQUErQyxJQUFuRCxFQUF5RDtBQUN2RDBELGFBQU8sQ0FBQzNDLGdCQUFSLENBQXlCLFdBQXpCLEVBQXNDLFlBQVk7QUFDaEQyQyxlQUFPLENBQUMxRCxhQUFSLENBQXNCLHFCQUF0QixFQUE2Q21CLFNBQTdDLENBQXVERSxHQUF2RCxDQUEyRCxVQUEzRDtBQUNELE9BRkQ7QUFHQXFDLGFBQU8sQ0FBQzNDLGdCQUFSLENBQXlCLFVBQXpCLEVBQXFDLFlBQVk7QUFDL0MyQyxlQUFPLENBQ0oxRCxhQURILENBQ2lCLHFCQURqQixFQUVHbUIsU0FGSCxDQUVhQyxNQUZiLENBRW9CLFVBRnBCO0FBR0QsT0FKRDtBQUtEO0FBQ0YsR0FYRDtBQWFBTixRQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsUUFBSUQsTUFBTSxDQUFDWSxVQUFQLEdBQW9CMEMsVUFBeEIsRUFBb0M7QUFDbENkLFdBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FDR0MsSUFESCxDQUNRM0QsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIscUJBQTFCLENBRFIsRUFFR0ksT0FGSCxDQUVXLFVBQVVvRSxJQUFWLEVBQWdCO0FBQ3ZCQSxZQUFJLENBQUMxRyxTQUFMLENBQWVDLE1BQWYsQ0FBc0IsVUFBdEI7QUFDRCxPQUpIO0FBS0Q7QUFDRixHQVJEO0FBU0Q7O0FBRUQsU0FBU1osY0FBVCxHQUEwQjtBQUN4QixNQUFJOEYsVUFBVSxHQUFHLElBQWpCO0FBQ0FnQixrQkFBZ0IsQ0FBQ2hCLFVBQUQsQ0FBaEI7QUFDQWlCLHFCQUFtQixDQUFDakIsVUFBRCxDQUFuQjtBQUNBc0Isb0JBQWtCLENBQUN0QixVQUFELENBQWxCO0FBQ0Q7O0FBRUQsU0FBU0csUUFBVCxHQUFvQjtBQUNsQixNQUFJNUcsUUFBUSxDQUFDRyxhQUFULENBQXVCLGVBQXZCLENBQUosRUFBNkM7QUFDM0MsUUFBSThILFFBQVEsR0FBR0MsS0FBSyxFQUFwQjtBQUNBRCxZQUFRLENBQUNFLE9BQVQ7O0FBQ0EsUUFDRSxDQUFDLENBQUQsS0FBT0MsU0FBUyxDQUFDQyxTQUFWLENBQW9CQyxPQUFwQixDQUE0QixNQUE1QixDQUFQLElBQ0FGLFNBQVMsQ0FBQ0csVUFBVixDQUFxQkQsT0FBckIsQ0FBNkIsVUFBN0IsSUFBMkMsQ0FGN0MsRUFHRTtBQUNBLFVBQUlFLE1BQU0sR0FBR3hJLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixRQUF2QixDQUFiO0FBQ0FxSSxZQUFNLENBQUNDLFlBQVAsQ0FBb0IsS0FBcEIsRUFBMkIsMkNBQTNCO0FBQ0Q7QUFDRjtBQUNGLEMsQ0FDRDs7O0FBQ0EsU0FBU0MsYUFBVCxDQUF1QkMsR0FBdkIsRUFBNEI7QUFDMUIsTUFBSXpJLEVBQUUsR0FBR0YsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEJtRixHQUExQixDQUFUO0FBQ0EsTUFBSUMsVUFBVSxHQUFHLENBQUMsQ0FBbEI7QUFDQSxNQUFJQyxTQUFTLEdBQUcsQ0FBQyxDQUFqQjtBQUNBLE1BQUlwQyxVQUFVLEdBQUcsR0FBakIsQ0FKMEIsQ0FLMUI7O0FBQ0FyQyxpQkFBZSxDQUFDbEUsRUFBRCxDQUFmLENBQW9CMEQsT0FBcEIsQ0FBNEIsVUFBVW9FLElBQVYsRUFBZ0I7QUFDMUNBLFFBQUksQ0FBQzNCLEtBQUwsQ0FBVzNELE1BQVgsR0FBb0IsRUFBcEIsQ0FEMEMsQ0FDbEI7O0FBQ3hCa0csY0FBVSxHQUFHWixJQUFJLENBQUM3RixZQUFsQixDQUYwQyxDQUVWOztBQUNoQzBHLGFBQVMsR0FBR0EsU0FBUyxHQUFHRCxVQUFaLEdBQXlCQyxTQUF6QixHQUFxQ0QsVUFBakQ7QUFDRCxHQUpEOztBQUtBLE1BQUk1SSxRQUFRLENBQUNxQixJQUFULENBQWN5SCxXQUFkLEdBQTRCckMsVUFBaEMsRUFBNEM7QUFDMUNyQyxtQkFBZSxDQUFDbEUsRUFBRCxDQUFmLENBQW9CMEQsT0FBcEIsQ0FBNEIsVUFBVW9FLElBQVYsRUFBZ0I7QUFDMUNBLFVBQUksQ0FBQzNCLEtBQUwsQ0FBVzNELE1BQVgsYUFBdUJtRyxTQUF2QjtBQUNELEtBRkQ7QUFHRDtBQUNGOztBQUVELFNBQVNFLFFBQVQsQ0FBa0JDLEdBQWxCLEVBQXVCTCxHQUF2QixFQUE0QjtBQUMxQixNQUFJekksRUFBRSxHQUFHRixRQUFRLENBQUN3RCxnQkFBVCxDQUEwQm1GLEdBQTFCLENBQVQsQ0FEMEIsQ0FFMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0F2RSxpQkFBZSxDQUFDbEUsRUFBRCxDQUFmLENBQW9CMEQsT0FBcEIsQ0FBNEIsVUFBVW9FLElBQVYsRUFBZ0I7QUFDMUMsUUFBSWlCLEdBQUcsR0FBR2pCLElBQUksQ0FBQ2tCLFNBQWY7O0FBQ0EsUUFBSUQsR0FBRyxDQUFDakMsTUFBSixHQUFhZ0MsR0FBakIsRUFBc0I7QUFDcEJHLGdCQUFVLEdBQUdGLEdBQUcsQ0FBQ0csU0FBSixDQUFjLENBQWQsRUFBaUJKLEdBQUcsR0FBRyxDQUF2QixJQUE0QixLQUF6QztBQUNBaEIsVUFBSSxDQUFDcUIsU0FBTCxHQUFpQkYsVUFBakI7QUFDRCxLQUhELE1BR087QUFDTEEsZ0JBQVUsR0FBR0YsR0FBRyxDQUFDRyxTQUFKLENBQWMsQ0FBZCxFQUFpQkosR0FBakIsSUFBd0IsS0FBckM7QUFDQWhCLFVBQUksQ0FBQ3FCLFNBQUwsR0FBaUJGLFVBQWpCO0FBQ0Q7QUFDRixHQVREO0FBVUFULGVBQWEsQ0FBQ0MsR0FBRCxDQUFiO0FBQ0Q7O0FBRUQsU0FBU1csYUFBVCxDQUF1QnBKLEVBQXZCLEVBQTJCMEMsTUFBM0IsRUFBbUM7QUFDakMsTUFBSTVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBSixFQUFnQztBQUM5QixRQUFJMkQsT0FBTyxHQUFHN0QsUUFBUSxDQUFDRyxhQUFULENBQXVCRCxFQUF2QixDQUFkO0FBQ0EsUUFBSXNHLE9BQU8sR0FBR3hHLFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCWixNQUExQixDQUFkO0FBQ0FpQixXQUFPLENBQUMzQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDNEMsV0FBSyxDQUFDL0IsY0FBTjtBQUNBOEIsYUFBTyxDQUFDMEYsSUFBUjtBQUNBOUYsV0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkI2QyxPQUEzQixFQUFvQzVDLE9BQXBDLENBQTRDLFVBQVVDLE9BQVYsRUFBbUI7QUFDN0RBLGVBQU8sQ0FBQzJGLE9BQVIsR0FBa0IsS0FBbEI7QUFDRCxPQUZEO0FBR0QsS0FORDtBQU9EO0FBQ0Y7O0FBRUQsU0FBU0MsbUJBQVQsR0FBK0I7QUFDN0IsTUFBSXpKLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixvQkFBdkIsTUFBaUQsSUFBckQsRUFBMkQ7QUFDekR1SSxpQkFBYSxDQUFDLG9CQUFELENBQWI7QUFDRCxHQUg0QixDQUk3Qjs7O0FBQ0EsTUFBSTFJLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixrQkFBdkIsTUFBK0MsSUFBbkQsRUFBeUQ7QUFDdkQ0SSxZQUFRLENBQUMsRUFBRCxFQUFLLGtCQUFMLENBQVI7QUFDRDtBQUNGOztBQUVELFNBQVNXLFFBQVQsR0FBb0I7QUFDbEIzSixHQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QjRKLE9BQTdCLENBQXFDO0FBQ25DQyxhQUFTLEVBQUUsT0FEd0I7QUFFbkMvRixXQUFPLEVBQUUsT0FGMEI7QUFHbkNkLFVBQU0sRUFBRTtBQUgyQixHQUFyQztBQUtBaEQsR0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkIrSCxFQUE3QixDQUFnQyxxQkFBaEMsRUFBdUQsWUFBWTtBQUNqRSxRQUFJK0IsWUFBWSxHQUFHLE1BQU05SixDQUFDLENBQUMsSUFBRCxDQUFELENBQVE4QyxJQUFSLENBQWEsa0JBQWIsQ0FBekIsQ0FEaUUsQ0FFakU7QUFDQTs7QUFDQSxRQUFJOUMsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFROEMsSUFBUixDQUFhLHFCQUFiLEtBQXVDLFlBQTNDLEVBQXlEO0FBQ3ZEOUMsT0FBQyxDQUFDOEosWUFBRCxDQUFELENBQWdCOUYsSUFBaEIsQ0FBcUIsZ0JBQXJCLEVBQXVDckQsSUFBdkMsQ0FBNEMsZ0JBQTVDO0FBQ0Q7O0FBQ0QsUUFBSVgsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFROEMsSUFBUixDQUFhLHFCQUFiLEtBQXVDLGdCQUEzQyxFQUE2RDtBQUMzRDlDLE9BQUMsQ0FBQzhKLFlBQUQsQ0FBRCxDQUFnQjlGLElBQWhCLENBQXFCLGdCQUFyQixFQUF1Q3JELElBQXZDLENBQTRDLG9CQUE1QztBQUNEO0FBQ0YsR0FWRDtBQVdEOztBQUVELFNBQVNvSixHQUFULEdBQWU7QUFDYixNQUFJOUosUUFBUSxDQUFDRyxhQUFULENBQXVCLFlBQXZCLENBQUosRUFBMEM7QUFDeEM0SixPQUFHLENBQUN6RyxJQUFKLENBQVM7QUFDUDBHLFVBQUksRUFBRTtBQURDLEtBQVQ7QUFHRDtBQUNGOztBQUVELFNBQVNDLFNBQVQsR0FBcUI7QUFDbkIsTUFBSWpLLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixZQUF2QixDQUFKLEVBQTBDO0FBQ3hDLFFBQUlvRCxRQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQixZQUExQixDQUFmO0FBQ0EsUUFBSVosTUFBTSxHQUFHNUMsUUFBUSxDQUFDRyxhQUFULENBQXVCLFFBQXZCLENBQWI7QUFDQSxRQUFJK0osS0FBSyxHQUFHLENBQVo7QUFDQXpHLFNBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCSixRQUEzQixFQUFxQ0ssT0FBckMsQ0FBNkMsVUFBVUMsT0FBVixFQUFtQjtBQUM5REEsYUFBTyxDQUFDNEUsWUFBUixDQUFxQixPQUFyQixFQUE4QnlCLEtBQUssRUFBbkM7O0FBRUFyRyxhQUFPLENBQUMvQixPQUFSLEdBQWtCLFlBQVk7QUFDNUIsWUFBSXFJLFlBQVksR0FBR3RHLE9BQU8sQ0FBQ00sWUFBUixDQUFxQixPQUFyQixDQUFuQjtBQUVBcEUsU0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlMkYsS0FBZixDQUFxQixXQUFyQixFQUFrQ3lFLFlBQWxDO0FBRUF2SCxjQUFNLENBQUM2RixZQUFQLENBQW9CLEtBQXBCLEVBQTJCNUUsT0FBTyxDQUFDTSxZQUFSLENBQXFCLFVBQXJCLENBQTNCLEVBTDRCLENBTTVCOztBQUNBTixlQUFPLENBQUN2QyxTQUFSLENBQWtCRSxHQUFsQixDQUFzQixXQUF0QjtBQUNBaUMsYUFBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkJKLFFBQTNCLEVBQXFDNkcsTUFBckMsQ0FBNEMsVUFBVXBDLElBQVYsRUFBZ0I7QUFDMUQsaUJBQU9BLElBQUksS0FBS25FLE9BQWhCO0FBQ0QsU0FGRCxFQUVHRCxPQUZILENBRVcsVUFBVW9FLElBQVYsRUFBZ0I7QUFDekJBLGNBQUksQ0FBQzFHLFNBQUwsQ0FBZUMsTUFBZixDQUFzQixXQUF0QjtBQUNELFNBSkQ7QUFLRCxPQWJEO0FBY0QsS0FqQkQ7QUFrQkQ7QUFDRjs7QUFFRCxTQUFTOEksa0JBQVQsR0FBOEI7QUFDNUIsTUFBSW5LLEVBQUUsR0FBR0gsQ0FBQyxDQUFDLHdCQUFELENBQVY7O0FBQ0EsTUFBSUcsRUFBSixFQUFRO0FBQ04sU0FBSyxJQUFJb0ssQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR3BLLEVBQUUsQ0FBQzhHLE1BQXZCLEVBQStCc0QsQ0FBQyxFQUFoQyxFQUFvQztBQUNsQyxVQUFJQyxnQkFBZ0IsR0FBR3JLLEVBQUUsQ0FBQ3NLLEVBQUgsQ0FBTUYsQ0FBTixFQUFTRyxRQUFULENBQWtCLEtBQWxCLEVBQXlCekQsTUFBaEQsQ0FEa0MsQ0FFbEM7O0FBQ0EsVUFBSXVELGdCQUFnQixJQUFJLENBQXhCLEVBQTJCO0FBQ3pCckssVUFBRSxDQUFDc0ssRUFBSCxDQUFNRixDQUFOLEVBQVN2RixRQUFULENBQWtCLHdCQUFsQjtBQUNELE9BRkQsTUFFTztBQUNMN0UsVUFBRSxDQUFDc0ssRUFBSCxDQUFNRixDQUFOLEVBQVNyRyxXQUFULENBQXFCLHdCQUFyQjtBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUVELFNBQVN5Ryx1QkFBVCxDQUFpQ0MsY0FBakMsRUFBaURDLFFBQWpELEVBQTJEO0FBQ3pELE1BQUkxSyxFQUFFLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QndLLGNBQXZCLENBQVQ7QUFDQSxNQUFJL0gsTUFBTSxHQUFHNUMsUUFBUSxDQUFDRyxhQUFULENBQXVCeUssUUFBdkIsQ0FBYjs7QUFDQSxNQUFJMUssRUFBSixFQUFRO0FBQ047QUFDQSxRQUFJcUQsUUFBUSxHQUFHckQsRUFBRSxDQUFDc0QsZ0JBQUgsQ0FBb0Isd0JBQXBCLENBQWYsQ0FGTSxDQUdOOztBQUNBQyxTQUFLLENBQUNKLFNBQU4sQ0FBZ0JLLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQkosUUFBM0IsRUFBcUNLLE9BQXJDLENBQTZDLFVBQVVDLE9BQVYsRUFBbUI7QUFDOURBLGFBQU8sQ0FBQzNDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUMsWUFBSTJKLFVBQVUsR0FBRzNLLEVBQUUsQ0FBQ3NELGdCQUFILENBQW9CLDhCQUFwQixFQUFvRHdELE1BQXJFLENBRDRDLENBRTVDOztBQUNBLFlBQUk2RCxVQUFVLEdBQUcsQ0FBakIsRUFBb0I7QUFDbEJqSSxnQkFBTSxDQUFDdEIsU0FBUCxDQUFpQkUsR0FBakIsQ0FBcUIsaUJBQXJCO0FBQ0QsU0FGRCxNQUVPO0FBQ0xvQixnQkFBTSxDQUFDdEIsU0FBUCxDQUFpQkMsTUFBakIsQ0FBd0IsaUJBQXhCO0FBQ0Q7QUFDRixPQVJEO0FBU0QsS0FWRDtBQVdBLFFBQUl1SixhQUFhLEdBQUc5SyxRQUFRLENBQUNHLGFBQVQsQ0FBdUIscUJBQXZCLENBQXBCO0FBQ0EySyxpQkFBYSxDQUFDNUosZ0JBQWQsQ0FBK0IsT0FBL0IsRUFBd0MsWUFBWTtBQUNsRDBCLFlBQU0sQ0FBQ3RCLFNBQVAsQ0FBaUJDLE1BQWpCLENBQXdCLGlCQUF4QjtBQUNELEtBRkQ7QUFHRDtBQUNGLEMsQ0FDRDs7O0FBQ0EsU0FBU3dKLGNBQVQsR0FBMEI7QUFDeEIsTUFBSWhMLENBQUMsQ0FBQyxPQUFELENBQUwsRUFBZ0I7QUFDZEEsS0FBQyxDQUFDLE9BQUQsQ0FBRCxDQUFXaUwsVUFBWCxHQUF3QkMsSUFBeEIsQ0FBNkI7QUFDM0JDLFdBQUssRUFBRW5MLENBQUMsQ0FBQ2lMLFVBQUYsQ0FBYUcsUUFBYixDQUFzQkQsS0FERjtBQUNTO0FBQ3BDRSxVQUFJLEVBQUUsT0FGcUI7QUFFWjtBQUNmQyxhQUFPLEVBQUUsT0FIa0I7QUFHVDtBQUNsQkMsVUFBSSxFQUFFLElBSnFCLENBSWhCOztBQUpnQixLQUE3QjtBQU1EO0FBRUYsQyxDQUNEOzs7QUFDQSxTQUFTQyxTQUFULEdBQXFCO0FBQ25CLE1BQUlyTCxFQUFFLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixlQUF2QixDQUFUOztBQUNBLE1BQUlELEVBQUosRUFBUTtBQUNOQSxNQUFFLENBQUNnQixnQkFBSCxDQUFvQixRQUFwQixFQUE4QixZQUFZO0FBQ3hDO0FBQ0FzSyxjQUFRLENBQUNDLElBQVQsR0FBZ0IsS0FBS0MsT0FBTCxDQUFhLEtBQUtDLGFBQWxCLEVBQWlDQyxLQUFqRCxDQUZ3QyxDQUd4QztBQUNELEtBSkQ7QUFLRDtBQUNGOztBQUNELFNBQVNDLFNBQVQsQ0FBbUJDLEVBQW5CLEVBQXVCO0FBQ3JCL0wsR0FBQyxDQUFDLE1BQU0rTCxFQUFQLENBQUQsQ0FBWWpKLElBQVosQ0FBaUIsT0FBakIsRUFBMEIsRUFBMUI7QUFDRDs7QUFFRCxTQUFTa0osU0FBVCxDQUFtQkQsRUFBbkIsRUFBdUI7QUFDckIvTCxHQUFDLENBQUMsTUFBTStMLEVBQVAsQ0FBRCxDQUFZakosSUFBWixDQUFpQixPQUFqQixFQUEwQiwwQkFBMUI7QUFDRCxDLENBQ0Q7OztBQUNBOUMsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzVCc0wsV0FBUztBQUNUdkssZUFBYTtBQUNidUYsY0FBWTtBQUNaTyxhQUFXO0FBQ1hPLGtCQUFnQixHQUxZLENBTTVCOztBQUNBQyxvQkFBa0I7QUFDbEJDLHlCQUF1QjtBQUN2QlosYUFBVztBQUNYYSxjQUFZO0FBQ1paLFVBQVE7QUFDUmpDLGlCQUFlLEdBWmEsQ0FhNUI7O0FBQ0FtRixLQUFHO0FBQ0hHLFdBQVM7QUFDVGhGLHNCQUFvQjtBQUNwQnFFLGVBQWEsQ0FBQyxxQkFBRCxFQUF3Qix3QkFBeEIsQ0FBYjtBQUNBeEQsNkJBQTJCLEdBbEJDLENBbUI1Qjs7QUFDQTRELFVBQVE7QUFDUlcsb0JBQWtCO0FBQ2xCSyx5QkFBdUIsQ0FBQyxrQkFBRCxFQUFxQixpQkFBckIsQ0FBdkI7QUFDQUEseUJBQXVCLENBQUMsb0JBQUQsRUFBdUIsd0JBQXZCLENBQXZCO0FBQ0FwRSxnQ0FBOEI7QUFDOUJ5RSxnQkFBYztBQUNkbkssWUFBVSxHQTFCa0IsQ0EyQjVCO0FBQ0QsQ0E1QkQ7O0FBNkJBSyxNQUFNLENBQUMrSyxNQUFQLEdBQWdCLFlBQVk7QUFDMUJ2QyxxQkFBbUI7QUFDbkI1RSxZQUFVO0FBQ1gsQ0FIRCxDLENBSUE7OztBQUNBOUUsQ0FBQyxDQUFDa0IsTUFBRCxDQUFELENBQVVnTCxNQUFWLENBQWlCLFlBQVk7QUFDM0J4QyxxQkFBbUI7QUFDbkI1RSxZQUFVLEdBRmlCLENBRzNCOztBQUNBTSwyQkFBeUIsQ0FBQyxvQkFBRCxFQUF1QixHQUF2QixDQUF6QjtBQUNBa0Msa0JBQWdCLEdBTFcsQ0FNM0I7QUFDRCxDQVBELEUsQ0FRQTs7QUFDQXRILENBQUMsQ0FBQ2tCLE1BQUQsQ0FBRCxDQUFVaUwsTUFBVixDQUFpQixZQUFZO0FBQzNCbkwsYUFBVyxHQURnQixDQUUzQjtBQUNELENBSEQsRSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmNANC4wL2Fzc2V0cy9qcy9tYWluLmpzXCIpO1xuIiwiLy8gYWpheCDphY3lkIhKUTMg5byV5YWl6Kit572uXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcclxuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdodG1sW2xhbmc9XCJ6aC10d1wiXScpO1xyXG4gIGlmIChlbCkge1xyXG4gICAgaWYgKCQoXCIjaGVhZGVyXCIpKSB7XHJcbiAgICAgICQuYWpheCh7XHJcbiAgICAgICAgdXJsOiBcImFqYXgvX2hlYWRlci5odG1sXCIsXHJcbiAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxyXG4gICAgICAgIGRhdGFUeXBlOiBcImh0bWxcIixcclxuICAgICAgfSkuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICQoXCIjaGVhZGVyXCIpLmh0bWwoZGF0YSk7XHJcbiAgICAgICAgaGVhZGVyRnVuY3Rpb24oKTtcclxuICAgICAgICBnb1RvQW5jaG9yKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKCQoXCIjc2lkZUxpbmtcIikpIHtcclxuICAgICAgJC5hamF4KHtcclxuICAgICAgICB1cmw6IFwiYWpheC9fc2lkZUxpbmsuaHRtbFwiLFxyXG4gICAgICAgIG1ldGhvZDogXCJHRVRcIixcclxuICAgICAgICBkYXRhVHlwZTogXCJodG1sXCIsXHJcbiAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAkKFwiI3NpZGVMaW5rXCIpLmh0bWwoZGF0YSk7XHJcbiAgICAgICAgdG9nZ2xlU2lkZUxpbmsoKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAoJChcIiNmb290ZXJcIikpIHtcclxuICAgICAgJC5hamF4KHtcclxuICAgICAgICB1cmw6IFwiYWpheC9fZm9vdGVyLmh0bWxcIixcclxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXHJcbiAgICAgICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxyXG4gICAgICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgJChcIiNmb290ZXJcIikuaHRtbChkYXRhKTtcclxuICAgICAgICBnb1RvcCgpO1xyXG4gICAgICAgIGdvVG9wU3RpY2t5KCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn0pO1xyXG4vL+iLseaWh+eJiOeahGFqYXhcclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2h0bWxbbGFuZz1cImVuXCJdJyk7XHJcbiAgaWYgKGVsKSB7XHJcbiAgICBpZiAoJChcIiNoZWFkZXJFblwiKSkge1xyXG4gICAgICAkLmFqYXgoe1xyXG4gICAgICAgIHVybDogXCIuLi9hamF4L19oZWFkZXJfZW4uaHRtbFwiLFxyXG4gICAgICAgIG1ldGhvZDogXCJHRVRcIixcclxuICAgICAgICBkYXRhVHlwZTogXCJodG1sXCIsXHJcbiAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAkKFwiI2hlYWRlckVuXCIpLmh0bWwoZGF0YSk7XHJcbiAgICAgICAgaGVhZGVyRnVuY3Rpb24oKTtcclxuICAgICAgICBnb1RvQW5jaG9yKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKCQoXCIjc2lkZUxpbmtFblwiKSkge1xyXG4gICAgICAkLmFqYXgoe1xyXG4gICAgICAgIHVybDogXCIuLi9hamF4L19zaWRlTGlua19lbi5odG1sXCIsXHJcbiAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxyXG4gICAgICAgIGRhdGFUeXBlOiBcImh0bWxcIixcclxuICAgICAgfSkuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICQoXCIjc2lkZUxpbmtFblwiKS5odG1sKGRhdGEpO1xyXG4gICAgICAgIHRvZ2dsZVNpZGVMaW5rKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKCQoXCIjZm9vdGVyRW5cIikpIHtcclxuICAgICAgJC5hamF4KHtcclxuICAgICAgICB1cmw6IFwiLi4vYWpheC9fZm9vdGVyX2VuLmh0bWxcIixcclxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXHJcbiAgICAgICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxyXG4gICAgICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgJChcIiNmb290ZXJFblwiKS5odG1sKGRhdGEpO1xyXG4gICAgICAgIGdvVG9wKCk7XHJcbiAgICAgICAgZ29Ub3BTdGlja3koKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG59KTtcclxuXHJcbmZ1bmN0aW9uIHRvb2xzTGlzdGVuZXIoKSB7XHJcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICBpZiAoZS5rZXlDb2RlID09PSA5KSB7XHJcbiAgICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LnJlbW92ZShcImpzLXVzZU1vdXNlXCIpO1xyXG4gICAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5hZGQoXCJqcy11c2VLZXlib2FyZFwiKTtcclxuICAgIH1cclxuICB9KTtcclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKFwianMtdXNlS2V5Ym9hcmRcIik7XHJcbiAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5hZGQoXCJqcy11c2VNb3VzZVwiKTtcclxuICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gdG9nZ2xlU2lkZUxpbmsoKSB7XHJcbiAgdmFyIGVsVHcgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdodG1sW2xhbmc9XCJ6aC10d1wiXScpO1xyXG4gIHZhciBlbEVuID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaHRtbFtsYW5nPVwiZW5cIl0nKTtcclxuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rVG9nZ2xlQnRuXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICB0aGlzLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICAgIGlmIChlbFR3KSB7XHJcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtcIikuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xyXG4gICAgfVxyXG4gICAgaWYgKGVsRW4pIHtcclxuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua0VuXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICAgIH1cclxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcGFnZVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XHJcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiYm9keVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwib3ZlcmZsb3ctaGlkZGVuXCIpO1xyXG4gICAgLy/miZPplotzaWRlTWVudeaZgu+8jOaKiuWxlemWi+eahG1lbnXpl5zmjolcclxuICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcclxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubC1oZWFkZXItaGFtYnVyZ2VyXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5sLWhlYWRlci1tZW51XCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgLy/miZPplotzaWRlTWVudeaZgu+8jOaKimdvdG9w5o6o5Yiw5peB6YKK5Y67XHJcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rRm9vdGVyXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICB9KTtcclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gMTIwMCkge1xyXG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rVG9nZ2xlQnRuXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICAgICAgaWYgKGVsVHcpIHtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoZWxFbikge1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtFblwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XHJcbiAgICAgIH1cclxuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNwYWdlXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcclxuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImJvZHlcIikuY2xhc3NMaXN0LnJlbW92ZShcIm92ZXJmbG93LWhpZGRlblwiKTtcclxuICAgICAgLy9b56e76ZmkXeaJk+mWi3NpZGVNZW515pmC77yM5oqKZ290b3DmjqjliLDml4HpgorljrtcclxuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua0Zvb3RlclwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XHJcbiAgICB9XHJcbiAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdvVG9wKCkge1xyXG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjZ29Ub3BcIikub25jbGljayA9IGZ1bmN0aW9uIChlKSB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAkKFwiaHRtbCwgYm9keVwiKS5hbmltYXRlKHtcclxuICAgICAgICBzY3JvbGxUb3A6IDAsXHJcbiAgICAgIH0sXHJcbiAgICAgIDUwMFxyXG4gICAgKTtcclxuICB9O1xyXG59XHJcblxyXG5mdW5jdGlvbiBnb1RvcFN0aWNreSgpIHtcclxuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rRm9vdGVyXCIpO1xyXG4gIC8v6KiI566XZm9vdGVy55uu5YmN6auY5bqmXHJcbiAgdmFyIGZvb3RlckhlaWdodFZhciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubC1mb290ZXJcIikuY2xpZW50SGVpZ2h0O1xyXG4gIC8vIGNvbnNvbGUubG9nKGBmb290ZXJIZWlnaHRWYXIgKyAke2Zvb3RlckhlaWdodFZhcn1gKTtcclxuICAvL+eVtuS4i+e2sumggemrmOW6pijmlbTpoIHplbcpXHJcbiAgdmFyIHNjcm9sbEhlaWdodFZhciA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxIZWlnaHQ7XHJcbiAgLy8gY29uc29sZS5sb2coYHNjcm9sbEhlaWdodFZhciArICR7c2Nyb2xsSGVpZ2h0VmFyfWApO1xyXG4gIC8v55W25LiL55Wr6Z2i6auY5bqmKHdpbmRvdyBoZWlnaHQpXHJcbiAgdmFyIHdpbmRvd0hlaWdodFZhciA9IHdpbmRvdy5pbm5lckhlaWdodDtcclxuICAvLyBjb25zb2xlLmxvZyhgd2luZG93SGVpZ2h0VmFyICsgJHt3aW5kb3dIZWlnaHRWYXJ9YCk7XHJcbiAgLy/mjbLli5XkuK3nmoTnlavpnaLmnIDkuIrnq6/oiIfntrLpoIHpoILnq6/kuYvplpPnmoTot53pm6JcclxuICB2YXIgc2Nyb2xsVG9wVmFyID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDtcclxuICAvLyBjb25zb2xlLmxvZyhgc2Nyb2xsVG9wVmFyICsgJHtzY3JvbGxUb3BWYXJ9YCk7XHJcbiAgLy8gY29uc29sZS5sb2cod2luZG93SGVpZ2h0VmFyICsgc2Nyb2xsVG9wVmFyICsgZm9vdGVySGVpZ2h0VmFyKTtcclxuICAvL+WmguaenOaVtOmggemVtz0955Wr6Z2i6auY5bqmK+aNsuWLlemrmOW6plxyXG4gIGlmIChlbCkge1xyXG4gICAgaWYgKHNjcm9sbEhlaWdodFZhciA8PSB3aW5kb3dIZWlnaHRWYXIgKyBzY3JvbGxUb3BWYXIgKyBmb290ZXJIZWlnaHRWYXIpIHtcclxuICAgICAgZWwuY2xhc3NMaXN0LnJlbW92ZShcImpzLXN0aWNreVwiKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGVsLmNsYXNzTGlzdC5hZGQoXCJqcy1zdGlja3lcIik7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBnb1RvQW5jaG9yKCkge1xyXG4gIHZhciBoZWFkZXJIZWlnaHQgPSBcIlwiO1xyXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgIHJldHVybiBoZWFkZXJIZWlnaHQgPSAkKFwiLmwtaGVhZGVyXCIpLmhlaWdodCgpO1xyXG4gIH0pO1xyXG4gICQoJy5qcy1nb1RvQW5jaG9yJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIHZhciB0YXJnZXQgPSAkKHRoaXMpLmF0dHIoJ2hyZWYnKTtcclxuICAgIHZhciB0YXJnZXRQb3MgPSAkKHRhcmdldCkub2Zmc2V0KCkudG9wO1xyXG4gICAgdmFyIGhlYWRlckhlaWdodCA9ICQoXCIubC1oZWFkZXJcIikuaGVpZ2h0KCk7XHJcbiAgICAkKCdodG1sLGJvZHknKS5hbmltYXRlKHtcclxuICAgICAgc2Nyb2xsVG9wOiB0YXJnZXRQb3MgLSBoZWFkZXJIZWlnaHRcclxuICAgIH0sIDEwMDApO1xyXG4gICAgdmFyIHRyaWdnZXIwMiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjaGFtYnVyZ2VyXCIpO1xyXG4gICAgdmFyIHRhcmdldDAyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNtZW51XCIpO1xyXG4gICAgdHJpZ2dlcjAyLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgdGFyZ2V0MDIuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XHJcbiAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XHJcbiAgfSk7XHJcbn1cclxuLy/poqjnkLTnlKhmdW5jdGlvblxyXG5mdW5jdGlvbiBBY2NvcmRpb24oZWwsIHRhcmdldCwgc2libGluZ3MpIHtcclxuICB0aGlzLmVsID0gZWw7XHJcbiAgdGhpcy50YXJnZXQgPSB0YXJnZXQ7XHJcbiAgdGhpcy5zaWJsaW5ncyA9IHNpYmxpbmdzO1xyXG59XHJcbkFjY29yZGlvbi5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcclxuICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHRoaXMuZWwpO1xyXG4gIHZhciB0YXJnZXQgPSB0aGlzLnRhcmdldDtcclxuICB2YXIgc2libGluZ3MgPSB0aGlzLnNpYmxpbmdzO1xyXG4gIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgIGlmIChzaWJsaW5ncyA9PSB0cnVlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucXVlcnlTZWxlY3Rvcih0YXJnZXQpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAvLyB0aGlzLnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KS5jbGFzc0xpc3QudG9nZ2xlKFxyXG4gICAgICAgICAgLy8gICBcImpzLWFjY29yZGlvbkV4cGVuZGVkXCJcclxuICAgICAgICAgIC8vICk7XHJcbiAgICAgICAgICAvL+eCuuS6huimgeiDveWcqOaJk+mWi+S4gOWAi+aZgu+8jOWFtuS7lueahOaUtuWQiO+8jOmAmemCiuaUueeUqGpRdWVyeT09PlxyXG4gICAgICAgICAgJCh0aGlzKS5maW5kKHRhcmdldCkudG9nZ2xlQ2xhc3MoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcclxuICAgICAgICAgIC8vIOWinuWKoOiXjee3muahhuaViOaenFxyXG4gICAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcyhcImMtYWNjb3JkaW9uLWJ0bi1mb2N1c1wiKTtcclxuICAgICAgICAgICQodGhpcykuc2libGluZ3MoKS5maW5kKHRhcmdldCkucmVtb3ZlQ2xhc3MoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcclxuICAgICAgICAgIC8vIOWinuWKoOiXjee3muahhuaViOaenFxyXG4gICAgICAgICAgJCh0aGlzKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwiYy1hY2NvcmRpb24tYnRuLWZvY3VzXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS10YXJnZXRcIikpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcclxuICAgICAgICAvL+eCuuS6huimgeiDveWcqOaJk+mWi+S4gOWAi+aZgu+8jOWFtuS7lueahOaUtuWQiO+8jOmAmemCiuaUueeUqGpRdWVyeT09PlxyXG4gICAgICAgIHZhciBhdHQgPSAkKHRyaWdnZXIpLmF0dHIoXCJkYXRhLXRhcmdldFwiKTtcclxuICAgICAgICAkKGF0dCkudG9nZ2xlQ2xhc3MoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XHJcbiAgICAgICAgLy8gJChgJHt0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdGFyZ2V0XCIpfWApLnRvZ2dsZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIikuc2libGluZ3MoKS5yZW1vdmVDbGFzcyhcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xyXG4gICAgICAgIC8v55So5LqGalF1ZXJ55LmL5b6M77yM6YCZ57WE5pyD6KKr5qiT5LiK6YKj5YCL57aB5Zyo5LiA6LW377yM5omA5Lul5Y+v5Lul55yB55WlXHJcbiAgICAgICAgLy8gdHJpZ2dlci5jbGFzc0xpc3QudG9nZ2xlKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKHRoaXMucXVlcnlTZWxlY3Rvcih0YXJnZXQpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICB0aGlzLnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KS5jbGFzc0xpc3QudG9nZ2xlKFxyXG4gICAgICAgICAgICBcImpzLWFjY29yZGlvbkV4cGVuZGVkXCJcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodHJpZ2dlci5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKSkuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xyXG4gICAgICAgIHRyaWdnZXIuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gbm9kZUxpc3RUb0FycmF5KG5vZGVMaXN0Q29sbGVjdGlvbikge1xyXG4gIHJldHVybiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChub2RlTGlzdENvbGxlY3Rpb24pO1xyXG59XHJcbi8vIGNvbnN0IG5vZGVMaXN0VG9BcnJheSA9IChub2RlTGlzdENvbGxlY3Rpb24pID0+IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKG5vZGVMaXN0Q29sbGVjdGlvbik7XHJcbi8qKlxyXG4gKiBcclxuICogQHBhcmFtIHsqfSBlbCA6Y2xhc3MgbmFtZVxyXG4gKiBAcGFyYW0geyp9IHRhcmdldCA66KKr5b2x6Z+/5Yiw55qE55uu5qiZXHJcbiAqIEBwYXJhbSB7Kn0gbWVkaWFRdWVyeSA65pa36bue6Kit5a6aXHJcbiAqIEDoqqrmmI4gXCJlbFwi6IiHXCJ0YXJnZXRcIiB0b2dnbGUg5LiA5YCL5Y+ranMtYWN0aXZl55qEY2xhc3PopoHnlKjlh7rku4DpurzmlYjmnpzvvIznq6/nnIvkvaDmgI7purzlr6tqcy1hY3RpdmXnmoRjc3PmlYjmnpzlsZXplovmiJbmlLblkIjku4DpurzmnbHopb9cclxuICovXHJcblxyXG5mdW5jdGlvbiB0b2dnbGVWaXNpYWJsZShlbCwgdGFyZ2V0LCBtZWRpYVF1ZXJ5KSB7XHJcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChlbCk7XHJcbiAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KTtcclxuICBpZiAodGFyZ2V0KSB7XHJcbiAgICBub2RlTGlzdFRvQXJyYXkodHJpZ2dlcnMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcclxuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdGhpcy5jbGFzc0xpc3QudG9nZ2xlKFwianMtYWN0aXZlXCIpO1xyXG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QudG9nZ2xlKFwianMtYWN0aXZlXCIpO1xyXG4gICAgICAgIHZhciBoYXNNZWRpYVF1ZXJ5ID0gbWVkaWFRdWVyeTtcclxuICAgICAgICBpZiAoaGFzTWVkaWFRdWVyeSAhPT0gXCJcIikge1xyXG4gICAgICAgICAgdmFyIGlzTW9iaWxlID0gd2luZG93LmlubmVyV2lkdGggPCBtZWRpYVF1ZXJ5O1xyXG4gICAgICAgICAgaWYgKGlzTW9iaWxlKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QudG9nZ2xlKFwianMtZnVuY3Rpb25NZW51T3BlbmVkXCIpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWZ1bmN0aW9uTWVudU9wZW5lZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IG1lZGlhUXVlcnkpIHtcclxuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1mdW5jdGlvbk1lbnVPcGVuZWRcIik7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbi8qZWw96Ke455m85bCN6LGhKOmWi+mXnCkqL1xyXG4vKnRhcmdldD3ooqvmjqfliLbnmoTnianku7Yo54eIKSovXHJcbmZ1bmN0aW9uIGNsaWNrQ29uZmlybShlbCwgdGFyZ2V0KSB7XHJcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChlbCk7XHJcbiAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KTtcclxuICBpZiAodGFyZ2V0KSB7XHJcbiAgICBub2RlTGlzdFRvQXJyYXkodHJpZ2dlcnMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcclxuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY3RpdmVcIik7XHJcbiAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1mdW5jdGlvbk1lbnVPcGVuZWRcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbi8v4oaR4oaR4oaR6YCa55SoZnVuY3Rpb24tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuZnVuY3Rpb24gY3JlYXRlQWNjb3JkaW9uKCkge1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1hY2NvcmRpb24tYnRuJykpIHtcclxuICAgIG5ldyBBY2NvcmRpb24oXCIuYy1hY2NvcmRpb24tYnRuXCIsIFwiLmMtYWNjb3JkaW9uLWJ0bi1pY29uXCIsIHRydWUpLmluaXQoKTtcclxuICB9XHJcbn1cclxuLy8gVE9ETzog5oOz6L6m5rOV6YCZ5YCL5YWo5Z+f6K6K5pW46KaB6JmV55CG5LiA5LiLXHJcbnZhciBzdHIgPSAwO1xyXG5cclxuZnVuY3Rpb24gc2hvd0FsbFRhYigpIHtcclxuICBpZiAod2luZG93LmlubmVyV2lkdGggPiA5OTIgJiYgc3RyID09IDEpIHtcclxuICAgICQoXCIudGFiLXBhbmVcIikucmVtb3ZlQ2xhc3MoXCJzaG93IGFjdGl2ZVwiKS5maXJzdCgpLmFkZENsYXNzKFwic2hvdyBhY3RpdmVcIik7XHJcbiAgICAkKFwiLmMtdGFiLWxpbmtFXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpLnBhcmVudCgpLmZpcnN0KCkuZmluZChcIi5jLXRhYi1saW5rRVwiKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgIHN0ciA9IDA7XHJcbiAgfVxyXG4gIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDk5MiAmJiBzdHIgPT0gMCkge1xyXG4gICAgJChcIi50YWItcGFuZVwiKS5hZGRDbGFzcyhcInNob3cgYWN0aXZlXCIpO1xyXG4gICAgc3RyID0gMTtcclxuICB9XHJcbiAgLy8gY29uc29sZS5sb2coc3RyKTtcclxufVxyXG5cclxuZnVuY3Rpb24gdG9nZ2xlcEZpbHRlclN1Ym1lbnUoKSB7XHJcbiAgLy8gVE9ETzog5pyJ56m66Kmm6Kmm55yL5a+r5LiA5YCL5omL5qmf54mI5Y+v5Lul6Yed5bCN5YWn5a656auY5bqm5aKe5Yqgc2hvd21vcmXvvIzkuKbkuJToqJjpjITmraRzaG93bW9yZeW3sue2k+m7numWi+mBju+8jOS4jeacg+WboOeCunJ3ZOWwseWPiOioiOeul+S4gOasoVxyXG4gIC8v5omL5qmf54mI6aGv56S65YWo6YOoXHJcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wLXByb2R1Y3RzLXNlYXJjaC1zaG93TW9yZScpKSB7XHJcbiAgICBuZXcgQWNjb3JkaW9uKFwiLnAtcHJvZHVjdHMtc2VhcmNoLXNob3dNb3JlXCIsIHVuZGVmaW5lZCwgZmFsc2UpLmluaXQoKTtcclxuICB9XHJcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5jLWFjY29yZGlvbi1idG5cIik7XHJcbiAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS10YXJnZXRcIikpO1xyXG4gICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAoZWwucXVlcnlTZWxlY3RvcihcIi5wLXByb2R1Y3RzLXNlYXJjaC1zaG93TW9yZVwiKSkge1xyXG4gICAgICAgIGVsLnF1ZXJ5U2VsZWN0b3IoXCIucC1wcm9kdWN0cy1zZWFyY2gtc2hvd01vcmVcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xyXG4gICAgICAgIGVsLnF1ZXJ5U2VsZWN0b3IoXCIuYy1hY2NvcmRpb24tY29udGVudC1sYXlvdXRcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9KTtcclxuICAvL+aJi+apn+eJiOmWi+mXnOmBuOWWrlxyXG4gIC8v5ZWG5ZOB5bCI5Y2AXHJcbiAgdG9nZ2xlVmlzaWFibGUoXCIuY2xvc2VcIiwgXCIucC1wcm9kdWN0cy1zZWFyY2hcIiwgXCJcIik7XHJcbiAgdG9nZ2xlVmlzaWFibGUoXCIucC1wcm9kdWN0cy1zZWFyY2gtYnRuXCIsIFwiLnAtcHJvZHVjdHMtc2VhcmNoXCIsIDk5Mik7XHJcbiAgY2xpY2tDb25maXJtKFwiI2pzLWNvbmZpcm1cIiwgXCIucC1wcm9kdWN0cy1zZWFyY2hcIik7XHJcbiAgLy/ploDluILmn6XoqaJcclxuICB0b2dnbGVWaXNpYWJsZShcIi52LWRyb3Bkb3duLWJ0blwiLCBcIi52LWRyb3Bkb3duLW1lbnVcIiwgOTkyKTtcclxuICB0b2dnbGVWaXNpYWJsZShcIi5jbG9zZVwiLCBcIi52LWRyb3Bkb3duLW1lbnVcIiwgXCJcIik7XHJcbiAgY2xpY2tDb25maXJtKFwiI2pzLWNvbmZpcm1cIiwgXCIudi1kcm9wZG93bi1tZW51XCIpO1xyXG4gIC8vIGlmICgkKFwiLnYtZHJvcGRvd24tYnRuXCIpLmhhc0NsYXNzKFwianMtYWN0aXZlXCIpKSB7XHJcbiAgLy8gICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiYm9keVwiKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gIC8vICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnYtZHJvcGRvd24tYnRuXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY3RpdmVcIik7XHJcbiAgLy8gICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudi1kcm9wZG93bi1tZW51XCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY3RpdmVcIik7XHJcbiAgLy8gICB9KTtcclxuICAvLyB9XHJcblxyXG59XHJcblxyXG5mdW5jdGlvbiBjb25maXJtSWZEb3VibGVNZW51T3BlbmVkKGVsLCBtZWRpYVF1ZXJ5KSB7XHJcbiAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgbWVkaWFRdWVyeSAmJiAkKGVsKS5oYXNDbGFzcyhcImpzLWFjdGl2ZVwiKSAmJiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0ID09IFwiXCIpIHtcclxuICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QuYWRkKFwianMtbWVudU9wZW5lZFwiKTtcclxuICB9XHJcbn1cclxuLy8g6Lyq5pKt55u46ZecLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuZnVuY3Rpb24gU2xpY2soZWwsIHNsaWRlc1RvU2hvdywgc2xpZGVzUGVyUm93LCByb3dzLCByZXNwb25zaXZlKSB7XHJcbiAgdGhpcy5lbCA9IGVsO1xyXG4gIHRoaXMuc2xpZGVzVG9TaG93ID0gc2xpZGVzVG9TaG93O1xyXG4gIHRoaXMuc2xpZGVzUGVyUm93ID0gc2xpZGVzUGVyUm93O1xyXG4gIHRoaXMucm93cyA9IHJvd3M7XHJcbiAgdGhpcy5yZXNwb25zaXZlID0gcmVzcG9uc2l2ZTtcclxufTtcclxuXHJcblNsaWNrLnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xyXG4gICQodGhpcy5lbCArIFwiIC52LXNsaWNrXCIpLnNsaWNrKHtcclxuICAgIGFycm93czogdHJ1ZSxcclxuICAgIHNsaWRlc1RvU2hvdzogdGhpcy5zbGlkZXNUb1Nob3csXHJcbiAgICBzbGlkZXNQZXJSb3c6IHRoaXMuc2xpZGVzUGVyUm93LFxyXG4gICAgcm93czogdGhpcy5yb3dzLFxyXG4gICAgcHJldkFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXNsaWNrLWFycm93cy1wcmV2XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtbGVmdFwiPjwvaT48L2J1dHRvbj5gLFxyXG4gICAgbmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXNsaWNrLWFycm93cy1uZXh0XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtcmlnaHRcIj48L2k+PC9idXR0b24+YCxcclxuICAgIC8vIGF1dG9wbGF5OiB0cnVlLFxyXG4gICAgLy8gbGF6eUxvYWQ6IFwicHJvZ3Jlc3NpdmVcIixcclxuICAgIHJlc3BvbnNpdmU6IHRoaXMucmVzcG9uc2l2ZSxcclxuICB9KTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIHNldFN0b3JlVGFibGVIZWlnaHRBdE1vYmlsZSgpIHtcclxuICB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jLXNob3BUYWJsZS1yd2QtdmVyQSB0Ym9keSB0cicpO1xyXG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4gICAgdmFyIHRyaWdnZXJUZCA9IHRyaWdnZXIucXVlcnlTZWxlY3RvckFsbCgndGQnKTtcclxuICAgIHZhciBudW0wMSA9IHRyaWdnZXJUZFswXS5jbGllbnRIZWlnaHQ7XHJcbiAgICB2YXIgbnVtMDIgPSB0cmlnZ2VyVGRbMV0uY2xpZW50SGVpZ2h0O1xyXG4gICAgdmFyIG51bTAzID0gdHJpZ2dlclRkWzJdLmNsaWVudEhlaWdodDtcclxuICAgIHZhciB0b3RhbE51bSA9IG51bTAxICsgbnVtMDIgKyBudW0wMztcclxuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEyMDAgJiYgdHJpZ2dlci5jbGFzc0xpc3QuY29udGFpbnMoJ2pzLWFjdGl2ZScpID09IGZhbHNlKSB7XHJcbiAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gYCR7dG90YWxOdW19cHhgO1xyXG4gICAgfSBlbHNlIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDEyMDApIHtcclxuICAgICAgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPSBcIlwiO1xyXG4gICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5yZW1vdmUoJ2pzLWFjdGl2ZScpO1xyXG4gICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xyXG4gICAgfVxyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCAxMjAwICYmIHRyaWdnZXIuY2xhc3NMaXN0LmNvbnRhaW5zKCdqcy1hY3RpdmUnKSA9PSBmYWxzZSkge1xyXG4gICAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gYCR7dG90YWxOdW19cHhgO1xyXG4gICAgICB9IGVsc2UgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gMTIwMCkge1xyXG4gICAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gXCJcIjtcclxuICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5yZW1vdmUoJ2pzLWFjdGl2ZScpO1xyXG4gICAgICAgIHRyaWdnZXIucXVlcnlTZWxlY3RvcihcImlcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiB0b2dnbGVTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKSB7XHJcbiAgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuYy1zaG9wVGFibGUtcndkLXZlckEgdGJvZHkgdHInKTtcclxuICBub2RlTGlzdFRvQXJyYXkodHJpZ2dlcnMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcclxuICAgIHZhciB0cmlnZ2VyVGQgPSB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3JBbGwoJ3RkJyk7XHJcbiAgICB2YXIgbnVtMDEgPSB0cmlnZ2VyVGRbMF0uY2xpZW50SGVpZ2h0O1xyXG4gICAgdmFyIG51bTAyID0gdHJpZ2dlclRkWzFdLmNsaWVudEhlaWdodDtcclxuICAgIHZhciBudW0wMyA9IHRyaWdnZXJUZFsyXS5jbGllbnRIZWlnaHQ7XHJcbiAgICB2YXIgdG90YWxOdW0gPSBudW0wMSArIG51bTAyICsgbnVtMDM7XHJcbiAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJ0ZDpmaXJzdC1jaGlsZFwiKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgMTIwMCAmJiB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9PSBcIlwiKSB7XHJcbiAgICAgICAgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPSBgJHt0b3RhbE51bX1weGA7XHJcbiAgICAgICAgdHJpZ2dlci5jbGFzc0xpc3QucmVtb3ZlKCdqcy1hY3RpdmUnKTtcclxuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gXCJcIjtcclxuICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5hZGQoXCJqcy1hY3RpdmVcIik7XHJcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QuYWRkKFwianMtc3VibWVudU9wZW5lZFwiKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfSk7XHJcbn1cclxuXHJcbi8vIFRPRE86IOacieepuueci+S4gOS4i+eCuuS7gOm6vG5ld+esrOS4ieWAi+WwseWHuuS6i+S6hlxyXG5mdW5jdGlvbiBjcmVhdGVTbGlja3MoKSB7XHJcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudi1zbGlja1wiKSkge1xyXG4gICAgdmFyIHRhcmdldHMgPSBbXCIjbmV3c1wiLCBcIiNyZWNvbW1hbmRhdGlvblwiXTtcclxuICAgIHRhcmdldHMuZm9yRWFjaChmdW5jdGlvbiAodGFyZ2V0KSB7XHJcbiAgICAgIG5ldyBTbGljayh0YXJnZXQsIDQsIDEsIDEsIFt7XHJcbiAgICAgICAgICBicmVha3BvaW50OiA5OTIsXHJcbiAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDJcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIGJyZWFrcG9pbnQ6IDU3NixcclxuICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMixcclxuICAgICAgICAgICAgYXJyb3dzOiBmYWxzZSxcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICBdKS5pbml0KCk7XHJcbiAgICB9KTtcclxuICAgIG5ldyBTbGljayhcIiN2aWRlb1wiLCAxLCAyLCAyLCBbe1xyXG4gICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcclxuICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxyXG4gICAgICAgICAgc2xpZGVzUGVyUm93OiAxLFxyXG4gICAgICAgICAgcm93czogMSxcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBicmVha3BvaW50OiA1NzYsXHJcbiAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgIHNsaWRlc1RvU2hvdzogMixcclxuICAgICAgICAgIHNsaWRlc1BlclJvdzogMSxcclxuICAgICAgICAgIHJvd3M6IDEsXHJcbiAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgIF0pLmluaXQoKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJpb25ldFNsaWNrKCkge1xyXG4gIHZhciBlbCA9IFwiLnYtcmlvbmV0U2xpY2tcIjtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcclxuICAgICQoZWwpLnNsaWNrKHtcclxuICAgICAgLy8gY2VudGVyTW9kZTogdHJ1ZSxcclxuICAgICAgLy8gY2VudGVyUGFkZGluZzogJzYwcHgnLFxyXG4gICAgICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgcHJldkFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLXByZXZcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1sZWZ0XCI+PC9pPjwvYnV0dG9uPmAsXHJcbiAgICAgIG5leHRBcnJvdzogYDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwidi10YWJCdG5TbGljay1uZXh0XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtcmlnaHRcIj48L2k+PC9idXR0b24+YCxcclxuICAgICAgbGF6eUxvYWQ6IFwicHJvZ3Jlc3NpdmVcIixcclxuICAgICAgcmVzcG9uc2l2ZTogW3tcclxuICAgICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcclxuICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMyxcclxuICAgICAgICAgICAgZG90czogdHJ1ZSxcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIGJyZWFrcG9pbnQ6IDU3NixcclxuICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMixcclxuICAgICAgICAgICAgYXJyb3dzOiBmYWxzZSxcclxuICAgICAgICAgICAgLy8gdmFyaWFibGVXaWR0aDogdHJ1ZSxcclxuICAgICAgICAgICAgZG90czogdHJ1ZSxcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICBdLFxyXG4gICAgfSk7XHJcbiAgICAvLyAgICQoJy52LXRhYkJ0blNsaWNrLXByZXYnKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xyXG4gICAgLy8gICAgICQoJy52LXRhYkJ0blNsaWNrJykuc2xpY2soJ3NsaWNrUHJldicpO1xyXG4gICAgLy8gIH0pO1xyXG4gICAgLy8gICAkKCcudi10YWJCdG5TbGljay1uZXh0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcclxuICAgIC8vICAgICAkKCcudi10YWJCdG5TbGljaycpLnNsaWNrKCdzbGlja05leHQnKTtcclxuICAgIC8vICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRhYkJ0blNsaWNrKCkge1xyXG4gIHZhciBlbCA9IFwiLnYtdGFiQnRuU2xpY2tcIjtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcclxuICAgIHZhciB0YWJMZW5ndGggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKS5xdWVyeVNlbGVjdG9yQWxsKCcuYy10YWItY29udGFpbmVyJykubGVuZ3RoO1xyXG4gICAgdmFyIHNsaWRlc1RvU2hvd051bSA9ICh0YWJMZW5ndGggPiA2KSA/IDYgOiB0YWJMZW5ndGg7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhcInNsaWRlc1RvU2hvd051bSA9IFwiICsgc2xpZGVzVG9TaG93TnVtKTtcclxuICAgICQoZWwpLnNsaWNrKHtcclxuICAgICAgc2xpZGVzVG9TaG93OiBzbGlkZXNUb1Nob3dOdW0sXHJcbiAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxyXG4gICAgICBhcnJvd3M6IHRydWUsXHJcbiAgICAgIHByZXZBcnJvdzogYDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwidi10YWJCdG5TbGljay1wcmV2XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtbGVmdFwiPjwvaT48L2J1dHRvbj5gLFxyXG4gICAgICBuZXh0QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtdGFiQnRuU2xpY2stbmV4dFwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPjwvYnV0dG9uPmAsXHJcbiAgICAgIGxhenlMb2FkOiBcInByb2dyZXNzaXZlXCIsXHJcbiAgICAgIGluZmluaXRlOiBmYWxzZSxcclxuICAgICAgcmVzcG9uc2l2ZTogW3tcclxuICAgICAgICAgIGJyZWFrcG9pbnQ6IDEyMDAsXHJcbiAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXHJcbiAgICAgICAgICAgIHZhcmlhYmxlV2lkdGg6IGZhbHNlLFxyXG4gICAgICAgICAgICAvLyBzbGlkZXNUb1Njcm9sbDogNSxcclxuICAgICAgICAgICAgLy8gaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgICAgIC8vIGNlbnRlck1vZGU6IGZhbHNlLFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcclxuICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogNCxcclxuICAgICAgICAgICAgdmFyaWFibGVXaWR0aDogZmFsc2UsXHJcbiAgICAgICAgICAgIC8vIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAvLyBjZW50ZXJNb2RlOiBmYWxzZSxcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIGJyZWFrcG9pbnQ6IDc2OCxcclxuICAgICAgICAgIHNldHRpbmdzOiB7XHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcclxuICAgICAgICAgICAgdmFyaWFibGVXaWR0aDogdHJ1ZSxcclxuICAgICAgICAgICAgLy8gaW5maW5pdGU6IGZhbHNlLFxyXG4gICAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgIF0sXHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRhYkJ0blNsaWNrRml4ZWQoKSB7XHJcbiAgdmFyIGVsID0gXCIudi10YWJCdG5TbGlja1wiO1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xyXG4gICAgdmFyIHRhYkxlbmd0aCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpLnF1ZXJ5U2VsZWN0b3JBbGwoJy5jLXRhYi1jb250YWluZXInKS5sZW5ndGg7XHJcbiAgICAvLyB2YXIgc2xpZGVzVG9TaG93TnVtID0gKHRhYkxlbmd0aCA+IDYpID8gNiA6IHRhYkxlbmd0aDtcclxuICAgIC8vIGNvbnNvbGUubG9nKHRhYkxlbmd0aCk7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gMTIwMCAmJiB0YWJMZW5ndGggPD0gNikge1xyXG4gICAgICAkKFwiLnNsaWNrLXRyYWNrXCIpLmFkZENsYXNzKFwidGV4dC1tZC1jZW50ZXJcIik7XHJcbiAgICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGljay10cmFjaycpLmNsYXNzTGlzdC5hZGQoJ3RleHQtbWQtY2VudGVyJyk7XHJcbiAgICB9XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gNzY4ICYmIHdpbmRvdy5pbm5lcldpZHRoIDwgMTIwMCAmJiB0YWJMZW5ndGggPCA1KSB7XHJcbiAgICAgICQoXCIuc2xpY2stdHJhY2tcIikuYWRkQ2xhc3MoXCJ0ZXh0LW1kLWNlbnRlclwiKTtcclxuICAgICAgLy8gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNsaWNrLXRyYWNrJykuY2xhc3NMaXN0LmFkZCgndGV4dC1tZC1jZW50ZXInKTtcclxuICAgIH1cclxuICAgIC8vIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDc2OCkge1xyXG4gICAgLy8gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNsaWNrLXRyYWNrJykuY2xhc3NMaXN0LnJlbW92ZSgndGV4dC1tZC1jZW50ZXInKTtcclxuICAgIC8vIH1cclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRhYkJ0blNsaWNrTWVtYmVycygpIHtcclxuICB2YXIgZWwgPSBcIi52LXRhYkJ0blNsaWNrLW1lbWJlcnNcIjtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcclxuICAgICQoZWwpLnNsaWNrKHtcclxuICAgICAgc2xpZGVzVG9TaG93OiA2LFxyXG4gICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgYXJyb3dzOiB0cnVlLFxyXG4gICAgICBwcmV2QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtdGFiQnRuU2xpY2stcHJldlwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLWxlZnRcIj48L2k+PC9idXR0b24+YCxcclxuICAgICAgbmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLW5leHRcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1yaWdodFwiPjwvaT48L2J1dHRvbj5gLFxyXG4gICAgICBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxyXG4gICAgICBpbmZpbml0ZTogZmFsc2UsXHJcbiAgICAgIHJlc3BvbnNpdmU6IFt7XHJcbiAgICAgICAgICBicmVha3BvaW50OiA5OTIsXHJcbiAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDUsXHJcbiAgICAgICAgICAgIHZhcmlhYmxlV2lkdGg6IGZhbHNlLFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgYnJlYWtwb2ludDogNzY4LFxyXG4gICAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgICAgICB2YXJpYWJsZVdpZHRoOiB0cnVlLFxyXG4gICAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgIF0sXHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRhYkJ0blNsaWNrRml4ZWRNZW1iZXJzKCkge1xyXG4gIHZhciBlbCA9IFwiLnYtdGFiQnRuU2xpY2stbWVtYmVyc1wiO1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xyXG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDc2OCkge1xyXG4gICAgICAkKFwiLnYtdGFiQnRuU2xpY2stbWVtYmVycyAuc2xpY2stdHJhY2tcIikuYWRkQ2xhc3MoXCJ0ZXh0LW1kLWNlbnRlclwiKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLy8gRW5kIOi8quaSreebuOmXnC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbmZ1bmN0aW9uIHdhcnJhbnR5TGluaygpIHtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5wLWluZGV4LXdhcnJhbnR5LWxpbmtcIikubGVuZ3RoKSB7XHJcbiAgICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLnAtaW5kZXgtd2FycmFudHktbGlua1wiKTtcclxuICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4gICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW92ZXJcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRyaWdnZXIucXVlcnlTZWxlY3RvcihcIi5jLWJ0blwiKS5jbGFzc0xpc3QuYWRkKFwianMtYnRuSG92ZXJcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW91dFwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiLmMtYnRuXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1idG5Ib3ZlclwiKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuLy/miYvmqZ/niYjmvKLloKHpgbjllq5cclxuZnVuY3Rpb24gdG9nZ2xlTW9iaWxlTWVudShtZWRpYVF1ZXJ5KSB7XHJcbiAgdmFyIHRyaWdnZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhbWJ1cmdlclwiKTtcclxuICB2YXIgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNtZW51XCIpO1xyXG5cclxuICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICB0aGlzLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgdGFyZ2V0LmNsYXNzTGlzdC50b2dnbGUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC50b2dnbGUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gIH0pO1xyXG5cclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gbWVkaWFRdWVyeSkge1xyXG4gICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xyXG4gICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XHJcbiAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcclxuICAgIH1cclxuICB9KTtcclxufVxyXG4vL+aJi+apn+eJiOmiqOeQtOaKmOeWiumBuOWWrlxyXG5mdW5jdGlvbiB0b2dnbGVNb2JpbGVTdWJtZW51KG1lZGlhUXVlcnkpIHtcclxuICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmwtaGVhZGVyLW1lbnUtbGlua1wiKTtcclxuICB2YXIgdGFyZ2V0cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIubC1oZWFkZXItc3VibWVudS0taGlkZGVuXCIpO1xyXG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xyXG4gICAgaWYgKHRyaWdnZXIubmV4dEVsZW1lbnRTaWJsaW5nICE9PSBudWxsKSB7XHJcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHZhciB0YXJnZXQgPSB0cmlnZ2VyLm5leHRFbGVtZW50U2libGluZztcclxuICAgICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCBtZWRpYVF1ZXJ5KSB7XHJcbiAgICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XHJcbiAgICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xyXG4gICAgICAgICAgLy/ngrrkuobplovkuIDlgIvpl5zkuIDlgIvvvIznlKhqUXVlcnnliqDlr6tcclxuICAgICAgICAgICQodGFyZ2V0KS5wYXJlbnRzKCkuc2libGluZ3MoKS5maW5kKFwiLmwtaGVhZGVyLXN1Ym1lbnUgLCBpXCIpLnJlbW92ZUNsYXNzKFwianMtc3VibWVudU9wZW5lZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gbWVkaWFRdWVyeSkge1xyXG4gICAgICBub2RlTGlzdFRvQXJyYXkodHJpZ2dlcnMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcclxuICAgICAgICB2YXIgdGFyZ2V0ID0gdHJpZ2dlci5uZXh0RWxlbWVudFNpYmxpbmc7XHJcbiAgICAgICAgdGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xyXG4gICAgICAgIHRyaWdnZXIucXVlcnlTZWxlY3RvcihcImlcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBzaWJpbmdNb2JpbGVTdWJtZW51KCkge1xyXG4gICQoXCIubC1oZWFkZXItbWVudS1pdGVtXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgJCh0aGlzKS5zaWJsaW5ncygpLmZpbmQoXCIubC1oZWFkZXItc3VibWVudVwiKS5yZW1vdmVDbGFzcyhcImpzLXN1Ym1lbnVPcGVuZWRcIik7XHJcbiAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHRvZ2dsZVBjSG92ZXJTdGF0ZShtZWRpYVF1ZXJ5KSB7XHJcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1tZW51LWl0ZW1cIik7XHJcbiAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICBpZiAodHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiLmwtaGVhZGVyLXN1Ym1lbnVcIikgIT09IG51bGwpIHtcclxuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdmVyXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCIubC1oZWFkZXItbWVudS1saW5rXCIpLmNsYXNzTGlzdC5hZGQoXCJqcy1ob3ZlclwiKTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlb3V0XCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0cmlnZ2VyXHJcbiAgICAgICAgICAucXVlcnlTZWxlY3RvcihcIi5sLWhlYWRlci1tZW51LWxpbmtcIilcclxuICAgICAgICAgIC5jbGFzc0xpc3QucmVtb3ZlKFwianMtaG92ZXJcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCBtZWRpYVF1ZXJ5KSB7XHJcbiAgICAgIEFycmF5LnByb3RvdHlwZS5zbGljZVxyXG4gICAgICAgIC5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIubC1oZWFkZXItbWVudS1saW5rXCIpKVxyXG4gICAgICAgIC5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICBpdGVtLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1ob3ZlclwiKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gaGVhZGVyRnVuY3Rpb24oKSB7XHJcbiAgdmFyIGJyZWFrcG9pbnQgPSAxMjAwO1xyXG4gIHRvZ2dsZU1vYmlsZU1lbnUoYnJlYWtwb2ludCk7XHJcbiAgdG9nZ2xlTW9iaWxlU3VibWVudShicmVha3BvaW50KTtcclxuICB0b2dnbGVQY0hvdmVyU3RhdGUoYnJlYWtwb2ludCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGxhenlMb2FkKCkge1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiaW1nW2RhdGEtc3JjXVwiKSkge1xyXG4gICAgdmFyIG9ic2VydmVyID0gbG96YWQoKTtcclxuICAgIG9ic2VydmVyLm9ic2VydmUoKTtcclxuICAgIGlmIChcclxuICAgICAgLTEgIT09IG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZihcIk1TSUVcIikgfHxcclxuICAgICAgbmF2aWdhdG9yLmFwcFZlcnNpb24uaW5kZXhPZihcIlRyaWRlbnQvXCIpID4gMFxyXG4gICAgKSB7XHJcbiAgICAgIHZhciBpZnJhbWUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiaWZyYW1lXCIpO1xyXG4gICAgICBpZnJhbWUuc2V0QXR0cmlidXRlKFwic3JjXCIsIFwiaHR0cHM6Ly93d3cueW91dHViZS5jb20vZW1iZWQvTzdwTnBSM1B5NjhcIik7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi8v6KiI566X44CM5pyA5paw5raI5oGv44CN44CB44CM5Lq65rCj5o6o6Jam44CN6YCZ6aGe55qE5Y2h54mH5YiX6KGo55qE5qiZ6aGM5a2X5pW477yM6K6T5aSn5a6255qE6auY5bqm5LiA5qijXHJcbmZ1bmN0aW9uIGF1dG9GaXhIZWlnaHQoY29uKSB7XHJcbiAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChjb24pO1xyXG4gIHZhciB0aGlzSGVpZ2h0ID0gLTE7XHJcbiAgdmFyIG1heEhlaWdodCA9IC0xO1xyXG4gIHZhciBicmVha3BvaW50ID0gNzY4O1xyXG4gIC8v54K65LqGaWXkuI3mlK/mj7Rub2RlbGlzdOeahGZvckVhY2jkv67mraNcclxuICBub2RlTGlzdFRvQXJyYXkoZWwpLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgIGl0ZW0uc3R5bGUuaGVpZ2h0ID0gXCJcIjsgLy/muIXnqbrkuYvliY3nmoRzdHlsZVxyXG4gICAgdGhpc0hlaWdodCA9IGl0ZW0uY2xpZW50SGVpZ2h0OyAvL+WPluW+l+W3sue2k+a4m+mBjumrmOW6pueahFxyXG4gICAgbWF4SGVpZ2h0ID0gbWF4SGVpZ2h0ID4gdGhpc0hlaWdodCA/IG1heEhlaWdodCA6IHRoaXNIZWlnaHQ7XHJcbiAgfSk7XHJcbiAgaWYgKGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggPiBicmVha3BvaW50KSB7XHJcbiAgICBub2RlTGlzdFRvQXJyYXkoZWwpLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgaXRlbS5zdHlsZS5oZWlnaHQgPSBgJHttYXhIZWlnaHR9cHhgO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiB0ZXh0SGlkZShudW0sIGNvbikge1xyXG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoY29uKTtcclxuICAvL2ll5omN5Z+36KGM6YCZ5YCL6LaF6YGO5a2X5pW45aKe5Yqg5Yiq56+A6Jmf77yM5YW25LuW54CP6Ka95Zmo6Z2gY3Nz6Kqe5rOV5Y2z5Y+vXHJcbiAgLy8gaWYgKFxyXG4gIC8vICAgbmF2aWdhdG9yLnVzZXJBZ2VudC5pbmRleE9mKFwiTVNJRVwiKSAhPT0gLTEgfHxcclxuICAvLyAgIG5hdmlnYXRvci5hcHBWZXJzaW9uLmluZGV4T2YoXCJUcmlkZW50L1wiKSA+IDBcclxuICAvLyApIHtcclxuICAvLyAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGVsKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgLy8gICAgIHZhciB0eHQgPSBpdGVtLmlubmVyVGV4dDtcclxuICAvLyAgICAgaWYgKHR4dC5sZW5ndGggPiBudW0pIHtcclxuICAvLyAgICAgICB0eHRDb250ZW50ID0gdHh0LnN1YnN0cmluZygwLCBudW0gLSAxKSArIFwiLi4uXCI7XHJcbiAgLy8gICAgICAgaXRlbS5pbm5lckhUTUwgPSB0eHRDb250ZW50O1xyXG4gIC8vICAgICB9XHJcbiAgLy8gICB9KTtcclxuICAvLyB9XHJcbiAgbm9kZUxpc3RUb0FycmF5KGVsKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICB2YXIgdHh0ID0gaXRlbS5pbm5lclRleHQ7XHJcbiAgICBpZiAodHh0Lmxlbmd0aCA+IG51bSkge1xyXG4gICAgICB0eHRDb250ZW50ID0gdHh0LnN1YnN0cmluZygwLCBudW0gLSAxKSArIFwiLi4uXCI7XHJcbiAgICAgIGl0ZW0uaW5uZXJIVE1MID0gdHh0Q29udGVudDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHR4dENvbnRlbnQgPSB0eHQuc3Vic3RyaW5nKDAsIG51bSkgKyBcIi4uLlwiO1xyXG4gICAgICBpdGVtLmlubmVySFRNTCA9IHR4dENvbnRlbnQ7XHJcbiAgICB9XHJcbiAgfSk7XHJcbiAgYXV0b0ZpeEhlaWdodChjb24pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjbGVhckNoZWNrQm94KGVsLCB0YXJnZXQpIHtcclxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcclxuICAgIHZhciB0cmlnZ2VyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCk7XHJcbiAgICB2YXIgdGFyZ2V0cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwodGFyZ2V0KTtcclxuICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgdHJpZ2dlci5ibHVyKCk7XHJcbiAgICAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRhcmdldHMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcclxuICAgICAgICB0cmlnZ2VyLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhcmRDb250ZW50RnVuY3Rpb24oKSB7XHJcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuYy1jYXJkLWJvZHktdGl0bGVcIikgIT09IG51bGwpIHtcclxuICAgIGF1dG9GaXhIZWlnaHQoXCIuYy1jYXJkLWJvZHktdGl0bGVcIik7XHJcbiAgfVxyXG4gIC8v5Lq65rCj5o6o6Jam5YWn5paH5a2X5pW46ZmQ5Yi2XHJcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuYy1jYXJkLWJvZHktdHh0XCIpICE9PSBudWxsKSB7XHJcbiAgICB0ZXh0SGlkZSg0OCwgXCIuYy1jYXJkLWJvZHktdHh0XCIpO1xyXG4gIH1cclxufVxyXG5cclxuZnVuY3Rpb24gdG9vbFRpcHMoKSB7XHJcbiAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoe1xyXG4gICAgcGxhY2VtZW50OiAncmlnaHQnLFxyXG4gICAgdHJpZ2dlcjogJ2hvdmVyJyxcclxuICAgIG9mZnNldDogMzAsXHJcbiAgfSk7XHJcbiAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLm9uKCdpbnNlcnRlZC5icy50b29sdGlwJywgZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIGdldFRvb3RpcHNJZCA9IFwiI1wiICsgJCh0aGlzKS5hdHRyKFwiYXJpYS1kZXNjcmliZWRieVwiKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKGdldFRvb3RpcHNJZCk7XHJcbiAgICAvLyBjb25zb2xlLmxvZygkKHRoaXMpLmF0dHIoXCJkYXRhLW9yaWdpbmFsLXRpdGxlXCIpID09IFwi5b635ZyL6JSh5Y+45pW45L2N5b2x5YOP57O757WxXCIpO1xyXG4gICAgaWYgKCQodGhpcykuYXR0cihcImRhdGEtb3JpZ2luYWwtdGl0bGVcIikgPT0gXCLlvrflnIvolKHlj7jmlbjkvY3lvbHlg4/ns7vntbFcIikge1xyXG4gICAgICAkKGdldFRvb3RpcHNJZCkuZmluZCgnLnRvb2x0aXAtaW5uZXInKS5odG1sKFwi5b635ZyL6JSh5Y+4PGJyPuaVuOS9jeW9seWDj+ezu+e1sVwiKTtcclxuICAgIH1cclxuICAgIGlmICgkKHRoaXMpLmF0dHIoXCJkYXRhLW9yaWdpbmFsLXRpdGxlXCIpID09IFwi5rOV5ZyL5L6d6KaW6Lev5YWo5pa55L2N6KaW6Ka65qqi5ris57O757WxXCIpIHtcclxuICAgICAgJChnZXRUb290aXBzSWQpLmZpbmQoJy50b29sdGlwLWlubmVyJykuaHRtbChcIuazleWci+S+neimlui3rzxicj7lhajmlrnkvY3oppboprrmqqLmuKzns7vntbFcIik7XHJcbiAgICB9XHJcbiAgfSlcclxufVxyXG5cclxuZnVuY3Rpb24gYW9zKCkge1xyXG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiW2RhdGEtYW9zXVwiKSkge1xyXG4gICAgQU9TLmluaXQoe1xyXG4gICAgICBvbmNlOiB0cnVlLFxyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiB5dEhhbmRsZXIoKSB7XHJcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJbZGF0YS11cmxdXCIpKSB7XHJcbiAgICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtdXJsXVwiKTtcclxuICAgIHZhciB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiaWZyYW1lXCIpO1xyXG4gICAgdmFyIGluZGV4ID0gMDtcclxuICAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgIHRyaWdnZXIuc2V0QXR0cmlidXRlKFwiaW5kZXhcIiwgaW5kZXgrKyk7XHJcblxyXG4gICAgICB0cmlnZ2VyLm9uY2xpY2sgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHRyaWdnZXJJbmRleCA9IHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiaW5kZXhcIik7XHJcblxyXG4gICAgICAgICQoJyNzbGlja19hZCcpLnNsaWNrKCdzbGlja0dvVG8nLCB0cmlnZ2VySW5kZXgpO1xyXG5cclxuICAgICAgICB0YXJnZXQuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS11cmxcIikpO1xyXG4gICAgICAgIC8vYWxlcnQoK3RyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS11cmxcIikpO1xyXG4gICAgICAgIHRyaWdnZXIuY2xhc3NMaXN0LmFkZChcImpzLWFjdGl2ZVwiKTtcclxuICAgICAgICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0cmlnZ2VycykuZmlsdGVyKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICByZXR1cm4gaXRlbSAhPT0gdHJpZ2dlcjtcclxuICAgICAgICB9KS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICBpdGVtLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY3RpdmVcIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH07XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNhcmRKdXN0aWZ5Q29udGVudCgpIHtcclxuICB2YXIgZWwgPSAkKFwiLmpzLWNhcmRKdXN0aWZ5Q29udGVudFwiKTtcclxuICBpZiAoZWwpIHtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZWwubGVuZ3RoOyBpKyspIHtcclxuICAgICAgdmFyIGVsQ2hpbGRyZW5MZW5ndGggPSBlbC5lcShpKS5jaGlsZHJlbignZGl2JykubGVuZ3RoO1xyXG4gICAgICAvLyBjb25zb2xlLmxvZyhlbENoaWxkcmVuTGVuZ3RoKTtcclxuICAgICAgaWYgKGVsQ2hpbGRyZW5MZW5ndGggPD0gMikge1xyXG4gICAgICAgIGVsLmVxKGkpLmFkZENsYXNzKFwianVzdGlmeS1jb250ZW50LWNlbnRlclwiKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBlbC5lcShpKS5yZW1vdmVDbGFzcyhcImp1c3RpZnktY29udGVudC1jZW50ZXJcIik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHN0b3JlRmlsdGVyTm90aWZpY2F0aW9uKGlucHV0Q29udGFpbmVyLCB0YXJnZXRFbCkge1xyXG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5wdXRDb250YWluZXIpO1xyXG4gIHZhciB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRhcmdldEVsKTtcclxuICBpZiAoZWwpIHtcclxuICAgIC8vIGNvbnNvbGUubG9nKGlucHV0Q29udGFpbmVyICsgXCIgKyBcIiArIHRhcmdldCk7XHJcbiAgICB2YXIgdHJpZ2dlcnMgPSBlbC5xdWVyeVNlbGVjdG9yQWxsKFwiaW5wdXRbdHlwZT0nY2hlY2tib3gnXVwiKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKHRyaWdnZXJzKTtcclxuICAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgY2hlY2tlZE51bSA9IGVsLnF1ZXJ5U2VsZWN0b3JBbGwoXCJpbnB1dFt0eXBlPWNoZWNrYm94XTpjaGVja2VkXCIpLmxlbmd0aDtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhjaGVja2VkTnVtKTtcclxuICAgICAgICBpZiAoY2hlY2tlZE51bSA+IDApIHtcclxuICAgICAgICAgIHRhcmdldC5jbGFzc0xpc3QuYWRkKFwianMtaW5wdXRDaGVja2VkXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWlucHV0Q2hlY2tlZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgICB2YXIgY2xlYXJBbGxCdG5FbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjanMtY2xlYXJDaGVja0JveGVzXCIpO1xyXG4gICAgY2xlYXJBbGxCdG5FbC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWlucHV0Q2hlY2tlZFwiKTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4vL+a7vui7uOW8j+aXpeacn+mBuOWWrlxyXG5mdW5jdGlvbiBkYXRlTW9iaXNjcm9sbCgpIHtcclxuICBpZiAoJCgnI2RhdGUnKSkge1xyXG4gICAgJCgnI2RhdGUnKS5tb2Jpc2Nyb2xsKCkuZGF0ZSh7XHJcbiAgICAgIHRoZW1lOiAkLm1vYmlzY3JvbGwuZGVmYXVsdHMudGhlbWUsIC8vIFNwZWNpZnkgdGhlbWUgbGlrZTogdGhlbWU6ICdpb3MnIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcclxuICAgICAgbW9kZTogJ21peGVkJywgLy8gU3BlY2lmeSBzY3JvbGxlciBtb2RlIGxpa2U6IG1vZGU6ICdtaXhlZCcgb3Igb21pdCBzZXR0aW5nIHRvIHVzZSBkZWZhdWx0IFxyXG4gICAgICBkaXNwbGF5OiAnbW9kYWwnLCAvLyBTcGVjaWZ5IGRpc3BsYXkgbW9kZSBsaWtlOiBkaXNwbGF5OiAnYm90dG9tJyBvciBvbWl0IHNldHRpbmcgdG8gdXNlIGRlZmF1bHQgXHJcbiAgICAgIGxhbmc6ICd6aCcgLy8gU3BlY2lmeSBsYW5ndWFnZSBsaWtlOiBsYW5nOiAncGwnIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbn1cclxuLy/kuIvmi4npgbjllq7ot7PovYnotoXpgKPntZBcclxuZnVuY3Rpb24gc2VsZWN0VVJMKCkge1xyXG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuanMtc2VsZWN0VVJMXCIpO1xyXG4gIGlmIChlbCkge1xyXG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIC8vIHdpbmRvdy5vcGVuKHRoaXMub3B0aW9uc1t0aGlzLnNlbGVjdGVkSW5kZXhdLnZhbHVlKTtcclxuICAgICAgbG9jYXRpb24uaHJlZiA9IHRoaXMub3B0aW9uc1t0aGlzLnNlbGVjdGVkSW5kZXhdLnZhbHVlO1xyXG4gICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLm9wdGlvbnNbdGhpcy5zZWxlY3RlZEluZGV4XS52YWx1ZSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuZnVuY3Rpb24gcG9wdXBTaG93KGlkKSB7XHJcbiAgJChcIiNcIiArIGlkKS5hdHRyKFwic3R5bGVcIiwgXCJcIik7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHBvcHVwSGlkZShpZCkge1xyXG4gICQoXCIjXCIgKyBpZCkuYXR0cihcInN0eWxlXCIsIFwiZGlzcGxheTogbm9uZSAhaW1wb3J0YW50XCIpO1xyXG59XHJcbi8v5ZG85Y+rZnVuY3Rpb24t57ay6aCB6LyJ5YWl5a6M5oiQ5b6MXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcclxuICBzZWxlY3RVUkwoKTtcclxuICB0b29sc0xpc3RlbmVyKCk7XHJcbiAgY3JlYXRlU2xpY2tzKCk7XHJcbiAgdGFiQnRuU2xpY2soKTtcclxuICB0YWJCdG5TbGlja0ZpeGVkKCk7XHJcbiAgLy/mnIPlk6HmrIrnm4rlsIjnlKhcclxuICB0YWJCdG5TbGlja01lbWJlcnMoKTtcclxuICB0YWJCdG5TbGlja0ZpeGVkTWVtYmVycygpO1xyXG4gIHJpb25ldFNsaWNrKCk7XHJcbiAgd2FycmFudHlMaW5rKCk7XHJcbiAgbGF6eUxvYWQoKTtcclxuICBjcmVhdGVBY2NvcmRpb24oKTtcclxuICAvLyBjcmVhdGVWaWRlb1N3aXBlcigpO1xyXG4gIGFvcygpO1xyXG4gIHl0SGFuZGxlcigpO1xyXG4gIHRvZ2dsZXBGaWx0ZXJTdWJtZW51KCk7XHJcbiAgY2xlYXJDaGVja0JveChcIiNqcy1jbGVhckNoZWNrQm94ZXNcIiwgXCJpbnB1dFt0eXBlPSdjaGVja2JveCddXCIpO1xyXG4gIHNldFN0b3JlVGFibGVIZWlnaHRBdE1vYmlsZSgpO1xyXG4gIC8vICQoJ1tkYXRhLXRvZ2dsZT1cInBvcG92ZXJcIl0nKS5wb3BvdmVyKCk7XHJcbiAgdG9vbFRpcHMoKTtcclxuICBjYXJkSnVzdGlmeUNvbnRlbnQoKTtcclxuICBzdG9yZUZpbHRlck5vdGlmaWNhdGlvbihcIi52LWRyb3Bkb3duLW1lbnVcIiwgXCIudi1kcm9wZG93bi1idG5cIik7XHJcbiAgc3RvcmVGaWx0ZXJOb3RpZmljYXRpb24oXCIucC1wcm9kdWN0cy1zZWFyY2hcIiwgXCIucC1wcm9kdWN0cy1zZWFyY2gtYnRuXCIpO1xyXG4gIHRvZ2dsZVN0b3JlVGFibGVIZWlnaHRBdE1vYmlsZSgpO1xyXG4gIGRhdGVNb2Jpc2Nyb2xsKCk7XHJcbiAgZ29Ub0FuY2hvcigpO1xyXG4gIC8vIGdvVG9wU3RpY2t5KCk7XHJcbn0pO1xyXG53aW5kb3cub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG4gIGNhcmRDb250ZW50RnVuY3Rpb24oKTtcclxuICBzaG93QWxsVGFiKCk7XHJcbn07XHJcbi8v5ZG85Y+rZnVuY3Rpb24t6KaW56qX5aSn5bCP6K6K5pu0XHJcbiQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKCkge1xyXG4gIGNhcmRDb250ZW50RnVuY3Rpb24oKTtcclxuICBzaG93QWxsVGFiKCk7XHJcbiAgLy8gc2V0U3RvcmVUYWJsZUhlaWdodEF0TW9iaWxlKCk7XHJcbiAgY29uZmlybUlmRG91YmxlTWVudU9wZW5lZChcIi5wLXByb2R1Y3RzLXNlYXJjaFwiLCA5OTIpO1xyXG4gIHRhYkJ0blNsaWNrRml4ZWQoKTtcclxuICAvLyBnb1RvQW5jaG9yKCk7XHJcbn0pO1xyXG4vL+WRvOWPq2Z1bmN0aW9uLeaNsuWLlVxyXG4kKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uICgpIHtcclxuICBnb1RvcFN0aWNreSgpO1xyXG4gIC8vIHNpZGVMaW5rU3RpY2t5KCk7XHJcbn0pOyJdLCJzb3VyY2VSb290IjoiIn0=