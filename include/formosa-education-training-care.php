        <main class="wrapper">
            <section class="p-knowledge">
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">視力保健</h3>
                    </div>
                </div>
                <!-- banner -->
                <div class="u-py-100 u-mb-200">
                    <div class="container">
                        <img src="./assets/img/education/education_25.jpg" alt="" class="w-100 img-fluid">
                    </div>
                </div>
                <!-- end banner -->
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">眼睛日常保健</h4>
                        <div class="row">
                            <!-- <div class="col-xl-4 u-pb-100 u-pb-xl-000 order-xl-1 text-center">
                                <img src="assets/img/education/education_06.jpg" alt="" class="img-fluid">
                            </div> -->
                            <div class="col-xl-12 order-xl-0">
                                <ol class="u-list-style--decimal u-mb-000">
                                    <li class="font-weight-bold">飲食</li>
                                    <ul class="u-list-style--custom u-mb-100">
                                        <li>(1)&nbsp;均衡營養、不偏食，可依照營養師建議蔬果五七九</li>
                                        <li>(2)&nbsp;小孩２份水果３份蔬菜</li>
                                        <li>(3)&nbsp;成年女性３份水果４份蔬菜</li>
                                        <li>(4)&nbsp;成年男性４份水果５份蔬菜</li>
                                        <li>(5)&nbsp;水果以自身拳頭大小為一份</li>
                                        <li>(6)&nbsp;蔬菜生的100g為一份，熟的半碗為一份(一般的瓷碗)</li>
                                        <li>(7)&nbsp;除此之外還可注意眼底防曬，黃斑部佔我們的視覺80%，葉黃素就如同眼底的防曬乳幫我們吸收強光以及抗氧化，依據行政院國民健康局資料，葉黃素「除了眼睛之外，葉黃素已經證實是一個全身性的重要抗氧化劑，在抑制脂肪過氧化作用上，是最有效的類胡蘿蔔素，對於體內傷害方面，葉黃素與其他抗氧化劑，是抗氧化機轉中的關鍵」
                                        </li>
                                    </ul>
                                    </li>
                                    <li class="font-weight-bold">閱讀姿勢</li>
                                    <ul class="u-list-style--custom u-mb-100">
                                        <li>(1)&nbsp;看書保持30公分以上的距離</li>
                                        <li>(2)&nbsp;不可躺著看書、不可在搖晃的車內看書</li>
                                        <li>(3)&nbsp;看書或工作每小時休息五至十分鐘</li>
                                        <li>(4)&nbsp;避免閱讀字體細小、印刷不良之讀物</li>
                                        <li>(5)&nbsp;閱讀光線盡量照度在500~1000lux(晴天正午戶外照度約20000lux)</li>
                                    </ul>
                                    <li class="font-weight-bold">3C產品的使用</li>
                                    <ul class="u-list-style--custom u-mb-100">
                                        <li>(1)&nbsp;看電視應距離三公尺，或螢幕對角線的六到八倍距離</li>
                                        <li>(2)&nbsp;電視機的中心畫面，最好於眼睛水平略低的位置</li>
                                        <li>(3)&nbsp;房間亮度以照度300~500lux為宜</li>
                                        <li>(4)&nbsp;看電視、電腦、手機每小時休息五至十分鐘</li>
                                        <li>(5)&nbsp;螢幕可以調到最暗，再往回調亮至您覺得舒適的亮度</li>
                                    </ul>
                                    <li class="font-weight-bold">其他注意事項</li>
                                    <ul class="u-list-style--custom">
                                        <li>(1)&nbsp;多看遠距離物體，多至戶外活動</li>
                                        <li>(2)&nbsp;生活作息正常，睡眠充足</li>
                                        <li>(3)&nbsp;每三到六個月定期接受視力檢測、眼底檢查</li>
                                    </ul>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">孩童及青少年視力保健</h4>
                        <div class="row">
                            <!-- <div class="col-xl-4 u-pb-100 u-pb-xl-000 order-xl-1 text-center">
                                <img src="assets/img/education/education_07.jpg" alt="" class="img-fluid">
                            </div> -->
                            <div class="col-xl-12 order-xl-0">
                                <p class="u-mb-000">
                                    新生兒擁有所有的眼睛結構，但是他們還沒有學會如何使用，嬰兒的視覺從出生後開始發展，嬰兒必須花上幾個禮拜與幾個月發展注視、雙眼協同動作、深度知覺、手眼協調、空間感，學齡期時，發展更為複雜的視覺技巧像是視覺認知、視覺運動整合，視覺發展使他逐漸看見世界並詮釋世界的樣子。
                                </p>
                                <ul class="u-list-style--custom-level02 u-mb-000">
                                    <li class="u-mb-100">
                                        <span class="font-weight-bold">1.&nbsp;出生至四個月</span><br>
                                        剛出生的寶寶只能看到黑色、白色、灰色的陰影，因為新生兒只能看到8~12英尺(2.44m~3.66m)，他們看到的影像大部分是模糊的，寶寶一開始學習注視人臉，然後漸漸變成注視身旁明亮的東西，新生兒可以固視物體短暫幾秒鐘，8~12週（二至三個月）他們的眼睛開始隨著人或物體移動，嬰兒一開始頭會跟著注視的方向移動，2~4個月時，他們必須在少許移動頭部的情況下獨立地移動眼睛。<br><br>
                                        寶寶開始發展眼睛追視與雙眼運動時，眼睛會跟著物體移動，新生兒的肌肉神經還未發育完全，所以無法雙眼一同注視物體，眼珠有時會有交錯的情形。<br><br>
                                        所以當他們四到五個月時，請注意寶寶雙眼交錯的情形是不是已經消失了，因為這時通常已經可以正常的注視物體，如果過了五個月還有這樣的情況，就要盡快就醫並且定期追蹤。
                                    </li>
                                    <li class="u-mb-100">
                                        <span class="font-weight-bold">2.&nbsp;四至六個月</span><br>
                                        四至六個月時寶寶已經可以撐起自己的身體、坐立、打滾，手眼協調功能在這個階段開始發育，學習控制身體的活動，像是拿東西的準確度、把奶瓶放到嘴裡的流暢度…都會慢慢進步，也開始學習記住他們看見的東西。<br><br>
                                        在四或五個月時，寶寶的大腦已經可以將左右眼的影像融合，並發育深度知覺，像是空間感以及物體大小的感知，這些知覺的發育使他們可以準確的拿到物品，雙眼協同與遠近之間的注視技巧更加迅速準確，在六歲左右一般小孩會有1.0的視力。
                                    </li>
                                    <li class="u-mb-100">
                                        <sapn class="font-weight-bold">3.&nbsp;六到八個月</sapn><br>
                                        大部分的小孩這時開始爬行，進一步發展眼睛跟身體的協調性，學習距離判斷與豎立注視物標，然後爬過去拿取，新的感知經驗使他們的視覺知覺快速發育，像是注意到大小、形狀、位置的差異性，六個月的寶寶要有相當地眼睛活動控制能力，有些專家警告太早走路的小孩的雙眼運動可能沒有爬行比較多的小孩好，因為爬行較多的小孩會不斷的運用雙眼注視達到訓練雙眼功能的目的。
                                    </li>
                                    <li class="u-mb-100">
                                        <span class="font-weight-bold">4.&nbsp;八至十二個月</span><br>
                                        八至十二個月時寶寶可以判斷物品的距離，因為身、手、眼協調的發育，他們可以準確地搶取、丟拿物品，視覺記憶與辨別讓他們對事物好奇，視覺整合與身體協調讓寶寶可以巧妙的操控小物體，像是用手吃東西，一旦小孩開始走路，便開始學習運用眼睛辨別方向還有與身體主要肌肉群的協調，以達到身體移動的目的。
                                    </li>
                                    <li class="u-mb-100">
                                        <span class="font-weight-bold">5.&nbsp;剛學步的孩子與學齡期前兒童</span><br>
                                        對剛學步的小孩來說，手、眼、身體協調、眼睛協同作用、深度知覺的發育非常重要，而堆積木、滾球、上色、畫畫、剪裁、組裝玩具這些活動都可以幫助孩子增強這些重要的發展，當然閱讀對孩子一樣的重要，藉由閱讀可以培養他們的想像力。
                                    </li>
                                    <li class="u-mb-100">
                                        <span class="font-weight-bold">6.&nbsp;學齡期孩童</span><br>
                                        建議在入學前做一次完整的視力檢測，視光師必須確定視覺系統已發育完善，並且準備好應付近距離閱讀、寫字、工作，學校的學習會造成眼睛許多壓力，學齡前小孩主要注視著遠距離，而在學校孩童必須注視著近距離的書本，一天好幾個小時，這樣的壓力可能促使眼睛問題的增加，大多的孩子無法發現自己眼睛問題，因此也很少反應眼睛問題，以為他們看到的影像就是正常的，大家看起來都是這樣，所以必須持續觀察以及帶小朋友做定期的眼睛檢查，以下列幾點項目，如果有這些情況請盡速就醫。
                                        <ul class="u-list-style--custom-level03">
                                            <li>(1)&nbsp;閉上一眼看東西</li>
                                            <li>(2)&nbsp;經常揉擦眼睛</li>
                                            <li>(3)&nbsp;經常瞇著眼睛看東西</li>
                                            <li>(4)&nbsp;看不清楚遠方的景物</li>
                                            <li>(5)&nbsp;眼睛酸澀、流淚</li>
                                            <li>(6)&nbsp;閱讀時容易疲勞</li>
                                            <li>(7)&nbsp;頭歪著看書或看電視</li>
                                            <li>此外，研究顯示戶外活動有助於降低學齡期孩童得近視的機率，因此建議一個禮拜至少有三次的戶外活動。</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="font-weight-bold">7.&nbsp;青少年</span>
                                        <ul class="u-list-style--custom-level03">
                                            <li>如果有發現以下問題也請早做眼睛檢查</li>
                                            <li>(1)&nbsp;在黑暗的環境中，您經常會因看不清楚而撞到東西</li>
                                            <li>(2)&nbsp;觀看遠方景物的能力明顯變差</li>
                                            <li>(3)&nbsp;看到雙重影像或影像不全</li>
                                            <li>(4)&nbsp;對光線的感覺較以前敏感、不舒服</li>
                                            <li>(5)&nbsp;影像模糊不清並伴有光暈或虹彩投影</li>
                                            <li>(6)&nbsp;容易感到疲勞</li>
                                        </ul>
                                    </li>
                                </ul>
                                <p class="u-mb-000">
                                    不管您有沒有以上特徵，我們還是建議您至少每半年檢查一次。
                                    若你患有糖尿病、高血壓等一些會影響眼睛及視力的疾病時，更要比常人更注意以上所述之的視力障礙特徵!
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-pt-100 u-pb-400">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">中老年人視力保健</h4>
                        <div class="row">
                            <!-- <div class="col-xl-4 u-pb-100 u-pb-xl-000 order-xl-1 text-center">
                                <img src="assets/img/education/education_08.jpg" alt="" class="img-fluid">
                            </div> -->
                            <div class="col-12">
                                <p class="u-mb-000">
                                    人類的身體機能在中年以後，會慢慢步入老化的狀態。尤其是在四十歲以後，大部分的人都或多或少會有老花眼及視力減弱的情況發生，此時對於眼睛的保養與定期檢測更是重要，以下是常見的老化情況
                                </p>
                                <ul class="u-list-style--custom-level03">
                                    <li>(1)&nbsp;眼前影像有一些小暗點隨著眼球的移動而移動</li>
                                    <li>(2)&nbsp;眼睛過於濕潤或者乾癢</li>
                                    <li>(3)&nbsp;晚上的視力較差</li>
                                    <li>(4)&nbsp;對比敏感度下降</li>
                                    <li>(5)&nbsp;畏光</li>
                                    <li>(6)&nbsp;閱讀時需要較多的光線</li>
                                    <li>(7)&nbsp;從明室到暗室需要較長的時間適應</li>
                                    <li>(8)&nbsp;對於色彩的辨識下降，尤其是藍色與綠色</li>
                                    <li>(9)&nbsp;因為白內障、糖尿病、青光眼造成視覺功能下降</li>
                                </ul>
                            </div>
                            <div class="col-12 order-2">
                                <p class="u-mb-000">
                                    隨著老化眼底對於光線的耐受力下降，因此適當的補充葉黃素以及出門配戴太陽眼鏡是必要的，葉黃素就像是眼底的防曬乳，幫我們對抗自由基，除此之外，若您有以下情形
                                </p>
                                <ul class="u-list-style--custom-level03">
                                    <li>(1)&nbsp;閱讀書報時會不經意的將刊物往後延伸</li>
                                    <li>(2)&nbsp;對於黑暗及閃光的調適力明顯變差</li>
                                </ul>
                                <p class="u-mb-000">此時，一付經由驗光師所驗配出來的老花眼鏡將是您護眼的必備工具。
                                    特別提醒您，一般路邊或夜市所販賣的老花眼鏡通常都未經過嚴格的光學測試，千萬不可因為便宜或使用率不高而購買或配戴喔！眼鏡還是必須經過眼科醫師或專業驗光師，經由精密的儀器及專業的技術搭配，並考量您日常生活的用眼習慣，確實做到量身定做，才是一付適合您使用的眼鏡。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>