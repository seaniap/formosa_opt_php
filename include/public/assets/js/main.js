/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src@4.0/assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src@4.0/assets/js/main.js":
/*!***********************************!*\
  !*** ./src@4.0/assets/js/main.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ajax 配合JQ3 引入設置
$(document).ready(function () {
  $.ajax({
    url: "ajax/_header.html",
    method: "GET",
    dataType: "html"
  }).done(function (data) {
    $("#header").html(data);
    headerFunction(); // sibingMobileSubmenu();
  });
  $.ajax({
    url: "ajax/_sideLink.html",
    method: "GET",
    dataType: "html"
  }).done(function (data) {
    $("#sideLink").html(data); // goTop();

    toggleSideLink();
  });
  $.ajax({
    url: "ajax/_footer.html",
    method: "GET",
    dataType: "html"
  }).done(function (data) {
    $("#footer").html(data);
    goTop();
    goTopSticky(); // toggleSideLink();
  }); // $("#footer").load("ajax/_footer.html");
}); //英文版的ajax

$(document).ready(function () {
  $.ajax({
    url: "../ajax/_header_en.html",
    method: "GET",
    dataType: "html"
  }).done(function (data) {
    $("#headerEn").html(data);
    headerFunction(); // sibingMobileSubmenu();
  });
  $.ajax({
    url: "../ajax/_sideLink_en.html",
    method: "GET",
    dataType: "html"
  }).done(function (data) {
    $("#sideLinkEn").html(data); // goTop();

    toggleSideLink();
  });
  $.ajax({
    url: "../ajax/_footer_en.html",
    method: "GET",
    dataType: "html"
  }).done(function (data) {
    $("#footerEn").html(data);
    goTop();
    goTopSticky(); // toggleSideLink();
  }); // $("#footer").load("ajax/_footer.html");
});

function toolsListener() {
  window.addEventListener("keydown", function (e) {
    if (e.keyCode === 9) {
      document.body.classList.remove("js-useMouse");
      document.body.classList.add("js-useKeyboard");
    }
  });
  window.addEventListener("mousedown", function (e) {
    document.body.classList.remove("js-useKeyboard");
    document.body.classList.add("js-useMouse");
  });
}

function toggleSideLink() {
  document.querySelector("#sideLinkToggleBtn").addEventListener("click", function () {
    this.classList.toggle("js-sideLinkOpened");
    document.querySelector("#sideLink").classList.toggle("js-sideLinkOpened");
    document.querySelector("#page").classList.toggle("js-sideLinkOpened");
    document.querySelector("body").classList.toggle("overflow-hidden"); //打開sideMenu時，把展開的menu關掉

    document.documentElement.classList.remove("js-menuOpened");
    document.querySelector(".l-header-hamburger").classList.remove("js-menuOpened");
    document.querySelector(".l-header-menu").classList.remove("js-menuOpened"); //打開sideMenu時，把gotop推到旁邊去

    document.querySelector("#sideLinkFooter").classList.toggle("js-sideLinkOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= 1200) {
      document.querySelector("#sideLinkToggleBtn").classList.remove("js-sideLinkOpened");
      document.querySelector("#sideLink").classList.remove("js-sideLinkOpened");
      document.querySelector("#page").classList.remove("js-sideLinkOpened");
      document.querySelector("body").classList.remove("overflow-hidden"); //[移除]打開sideMenu時，把gotop推到旁邊去

      document.querySelector("#sideLinkFooter").classList.remove("js-sideLinkOpened");
    }
  });
}

function goTop() {
  document.querySelector("#goTop").onclick = function (e) {
    e.preventDefault();
    $("html, body").animate({
      scrollTop: 0
    }, 500);
  };
}

function goTopSticky() {
  var el = document.querySelector("#sideLinkFooter"); //計算footer目前高度

  var footerHeightVar = document.querySelector(".l-footer").clientHeight; // console.log(`footerHeightVar + ${footerHeightVar}`);
  //當下網頁高度(整頁長)

  var scrollHeightVar = document.documentElement.scrollHeight; // console.log(`scrollHeightVar + ${scrollHeightVar}`);
  //當下畫面高度(window height)

  var windowHeightVar = window.innerHeight; // console.log(`windowHeightVar + ${windowHeightVar}`);
  //捲動中的畫面最上端與網頁頂端之間的距離

  var scrollTopVar = document.documentElement.scrollTop; // console.log(`scrollTopVar + ${scrollTopVar}`);
  // console.log(windowHeightVar + scrollTopVar + footerHeightVar);
  //如果整頁長==畫面高度+捲動高度

  if (scrollHeightVar <= windowHeightVar + scrollTopVar + footerHeightVar) {
    el.classList.remove("js-sticky");
  } else {
    el.classList.add("js-sticky");
  }
}

function goToAnchor() {
  $('.js-goToAnchor').click(function (e) {
    e.preventDefault();
    var target = $(this).attr('href');
    var targetPos = $(target).offset().top;
    $('html,body').animate({
      scrollTop: targetPos - 50
    }, 1000);
    var trigger02 = document.querySelector("#hamburger");
    var target02 = document.querySelector("#menu");
    trigger02.classList.remove("js-menuOpened");
    target02.classList.remove("js-menuOpened");
    document.documentElement.classList.remove("js-menuOpened");
  });
} //風琴用function


function Accordion(el, target, siblings) {
  this.el = el;
  this.target = target;
  this.siblings = siblings;
}

Accordion.prototype.init = function () {
  var triggers = document.querySelectorAll(this.el);
  var target = this.target;
  var siblings = this.siblings;
  Array.prototype.slice.call(triggers).forEach(function (trigger) {
    trigger.addEventListener("click", function () {
      event.preventDefault();

      if (siblings == true) {
        if (this.querySelector(target) !== null) {
          // this.querySelector(target).classList.toggle(
          //   "js-accordionExpended"
          // );
          //為了要能在打開一個時，其他的收合，這邊改用jQuery==>
          $(this).find(target).toggleClass("js-accordionExpended"); // 增加藍線框效果

          $(this).toggleClass("c-accordion-btn-focus");
          $(this).siblings().find(target).removeClass("js-accordionExpended"); // 增加藍線框效果

          $(this).siblings().removeClass("c-accordion-btn-focus");
        } // document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        //為了要能在打開一個時，其他的收合，這邊改用jQuery==>


        var att = $(trigger).attr("data-target");
        $(att).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended"); // $(`${trigger.getAttribute("data-target")}`).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended");
        //用了jQuery之後，這組會被樓上那個綁在一起，所以可以省略
        // trigger.classList.toggle("js-accordionExpended");
      } else {
        if (this.querySelector(target) !== null) {
          this.querySelector(target).classList.toggle("js-accordionExpended");
        }

        document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        trigger.classList.toggle("js-accordionExpended");
      }
    });
  });
};

function nodeListToArray(nodeListCollection) {
  return Array.prototype.slice.call(nodeListCollection);
} // const nodeListToArray = (nodeListCollection) => Array.prototype.slice.call(nodeListCollection);

/**
 * 
 * @param {*} el :class name
 * @param {*} target :被影響到的目標
 * @param {*} mediaQuery :斷點設定
 * @說明 "el"與"target" toggle 一個叫js-active的class要用出什麼效果，端看你怎麼寫js-active的css效果展開或收合什麼東西
 */


function toggleVisiable(el, target, mediaQuery) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);

  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        this.classList.toggle("js-active");
        target.classList.toggle("js-active");
        var hasMediaQuery = mediaQuery;

        if (hasMediaQuery !== "") {
          var isMobile = window.innerWidth < mediaQuery;

          if (isMobile) {
            document.documentElement.classList.toggle("js-functionMenuOpened");
          }
        } else {
          document.documentElement.classList.remove("js-functionMenuOpened");
        }

        window.addEventListener("resize", function () {
          if (window.innerWidth >= mediaQuery) {
            document.documentElement.classList.remove("js-functionMenuOpened");
          }
        });
      });
    });
  }
}
/*el=觸發對象(開關)*/

/*target=被控制的物件(燈)*/


function clickConfirm(el, target) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);

  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        target.classList.remove("js-active");
        document.documentElement.classList.remove("js-functionMenuOpened");
      });
    });
  }
} //↑↑↑通用function---------------------


function createAccordion() {
  if (document.querySelector('.c-accordion-btn')) {
    new Accordion(".c-accordion-btn", ".c-accordion-btn-icon", true).init();
  }
} // TODO: 想辦法這個全域變數要處理一下


var str = 0;

function showAllTab() {
  if (window.innerWidth > 992 && str == 1) {
    $(".tab-pane").removeClass("show active").first().addClass("show active");
    $(".c-tab-linkE").removeClass("active").parent().first().find(".c-tab-linkE").addClass("active");
    str = 0;
  }

  if (window.innerWidth < 992 && str == 0) {
    $(".tab-pane").addClass("show active");
    str = 1;
  } // console.log(str);

}

function togglepFilterSubmenu() {
  // TODO: 有空試試看寫一個手機版可以針對內容高度增加showmore，並且記錄此showmore已經點開過，不會因為rwd就又計算一次
  //手機版顯示全部
  if (document.querySelector('.p-products-search-showMore')) {
    new Accordion(".p-products-search-showMore", undefined, false).init();
  }

  var triggers = document.querySelectorAll(".c-accordion-btn");
  nodeListToArray(triggers).forEach(function (trigger) {
    var el = document.querySelector(trigger.getAttribute("data-target"));
    trigger.addEventListener("click", function () {
      if (el.querySelector(".p-products-search-showMore")) {
        el.querySelector(".p-products-search-showMore").classList.remove("js-accordionExpended");
        el.querySelector(".c-accordion-content-layout").classList.remove("js-accordionExpended");
      }
    });
  }); //手機版開關選單
  //商品專區

  toggleVisiable(".close", ".p-products-search", "");
  toggleVisiable(".p-products-search-btn", ".p-products-search", 992);
  clickConfirm("#js-confirm", ".p-products-search"); //門市查詢

  toggleVisiable(".v-dropdown-btn", ".v-dropdown-menu", 992);
  toggleVisiable(".close", ".v-dropdown-menu", "");
  clickConfirm("#js-confirm", ".v-dropdown-menu"); // if ($(".v-dropdown-btn").hasClass("js-active")) {
  //   document.querySelector("body").addEventListener("click", function () {
  //     document.querySelector(".v-dropdown-btn").classList.remove("js-active");
  //     document.querySelector(".v-dropdown-menu").classList.remove("js-active");
  //   });
  // }
}

function confirmIfDoubleMenuOpened(el, mediaQuery) {
  if (window.innerWidth < mediaQuery && $(el).hasClass("js-active") && document.documentElement.classList == "") {
    document.documentElement.classList.add("js-menuOpened");
  }
} // 輪播相關--------------------------


function Slick(el, slidesToShow, slidesPerRow, rows, responsive) {
  this.el = el;
  this.slidesToShow = slidesToShow;
  this.slidesPerRow = slidesPerRow;
  this.rows = rows;
  this.responsive = responsive;
}

;

Slick.prototype.init = function () {
  $(this.el + " .v-slick").slick({
    arrows: true,
    slidesToShow: this.slidesToShow,
    slidesPerRow: this.slidesPerRow,
    rows: this.rows,
    prevArrow: "<button type=\"button\" class=\"v-slick-arrows-prev\"><i class=\"fal fa-angle-left\"></i></button>",
    nextArrow: "<button type=\"button\" class=\"v-slick-arrows-next\"><i class=\"fal fa-angle-right\"></i></button>",
    // autoplay: true,
    // lazyLoad: "progressive",
    responsive: this.responsive
  });
};

function setStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;

    if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
      trigger.style.height = "".concat(totalNum, "px");
    } else if (window.innerWidth > 1200) {
      trigger.style.height = "";
      trigger.classList.remove('js-active');
      trigger.querySelector("i").classList.remove("js-submenuOpened");
    }

    window.addEventListener("resize", function () {
      if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
        trigger.style.height = "".concat(totalNum, "px");
      } else if (window.innerWidth > 1200) {
        trigger.style.height = "";
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      }
    });
  });
}

function toggleStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;
    trigger.querySelector("td:first-child").addEventListener('click', function () {
      if (window.innerWidth < 1200 && trigger.style.height == "") {
        trigger.style.height = "".concat(totalNum, "px");
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      } else {
        trigger.style.height = "";
        trigger.classList.add("js-active");
        trigger.querySelector("i").classList.add("js-submenuOpened");
      }
    });
  });
} // TODO: 有空看一下為什麼new第三個就出事了


function createSlicks() {
  if (document.querySelector(".v-slick")) {
    var targets = ["#news", "#recommandation"];
    targets.forEach(function (target) {
      new Slick(target, 4, 1, 1, [{
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false
        }
      }]).init();
    });
    new Slick("#video", 1, 2, 2, [{
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1
      }
    }, {
      breakpoint: 576,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1,
        arrows: false
      }
    }]).init();
  }
}

function rionetSlick() {
  var el = ".v-rionetSlick";

  if (document.querySelector(el)) {
    $(el).slick({
      // centerMode: true,
      // centerPadding: '60px',
      slidesToShow: 4,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          dots: true
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false,
          // variableWidth: true,
          dots: true
        }
      }]
    }); //   $('.v-tabBtnSlick-prev').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickPrev');
    //  });
    //   $('.v-tabBtnSlick-next').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickNext');
    //  });
  }
}

function tabBtnSlick() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length;
    var slidesToShowNum = tabLength > 6 ? 6 : tabLength; // console.log("slidesToShowNum = " + slidesToShowNum);

    $(el).slick({
      slidesToShow: slidesToShowNum,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          variableWidth: false // slidesToScroll: 5,
          // infinite: true,
          // centerMode: false,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          variableWidth: false // infinite: true,
          // centerMode: false,

        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          // infinite: false,
          arrows: false
        }
      }]
    });
  }
}

function tabBtnSlickFixed() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length; // var slidesToShowNum = (tabLength > 6) ? 6 : tabLength;
    // console.log(tabLength);

    if (window.innerWidth >= 1200 && tabLength <= 6) {
      $(".v-tabBtnSlick .slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    }

    if (window.innerWidth >= 768 && window.innerWidth < 1200 && tabLength < 5) {
      $(".v-tabBtnSlick .slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    } // if (window.innerWidth < 768) {
    // document.querySelector('.slick-track').classList.remove('text-md-center');
    // }

  }
}

function tabBtnSlickMembers() {
  var el = ".v-tabBtnSlick-members";

  if (document.querySelector(el)) {
    $(el).slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
          variableWidth: false
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          arrows: false
        }
      }]
    });
  }
}

function tabBtnSlickFixedMembers() {
  var el = ".v-tabBtnSlick-members";

  if (document.querySelector(el)) {
    if (window.innerWidth >= 768) {
      $(".v-tabBtnSlick-members .slick-track").addClass("text-md-center");
    }
  }
} // End 輪播相關--------------------------


function warrantyLink() {
  if (document.querySelectorAll(".p-index-warranty-link").length) {
    var triggers = document.querySelectorAll(".p-index-warranty-link");
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".c-btn").classList.add("js-btnHover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".c-btn").classList.remove("js-btnHover");
      });
    });
  }
} //手機版漢堡選單


function toggleMobileMenu(mediaQuery) {
  var trigger = document.querySelector("#hamburger");
  var target = document.querySelector("#menu");
  trigger.addEventListener("click", function () {
    this.classList.toggle("js-menuOpened");
    target.classList.toggle("js-menuOpened");
    document.documentElement.classList.toggle("js-menuOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      trigger.classList.remove("js-menuOpened");
      target.classList.remove("js-menuOpened");
      document.documentElement.classList.remove("js-menuOpened");
    }
  });
} //手機版風琴折疊選單


function toggleMobileSubmenu(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-link");
  var targets = document.querySelectorAll(".l-header-submenu--hidden");
  nodeListToArray(triggers).forEach(function (trigger) {
    if (trigger.nextElementSibling !== null) {
      trigger.addEventListener("click", function (e) {
        e.preventDefault();
        var target = trigger.nextElementSibling;

        if (window.innerWidth < mediaQuery) {
          target.classList.toggle("js-submenuOpened");
          trigger.querySelector("i").classList.toggle("js-submenuOpened"); //為了開一個關一個，用jQuery加寫

          $(target).parents().siblings().find(".l-header-submenu , i").removeClass("js-submenuOpened");
        }
      });
    }
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      nodeListToArray(triggers).forEach(function (trigger) {
        var target = trigger.nextElementSibling;
        target.classList.remove("js-submenuOpened");
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      });
    }
  });
}

function sibingMobileSubmenu() {
  $(".l-header-menu-item").on("click", function () {
    $(this).siblings().find(".l-header-submenu").removeClass("js-submenuOpened");
  });
}

function togglePcHoverState(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-item");
  nodeListToArray(triggers).forEach(function (trigger) {
    if (trigger.querySelector(".l-header-submenu") !== null) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".l-header-menu-link").classList.add("js-hover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".l-header-menu-link").classList.remove("js-hover");
      });
    }
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth < mediaQuery) {
      Array.prototype.slice.call(document.querySelectorAll(".l-header-menu-link")).forEach(function (item) {
        item.classList.remove("js-hover");
      });
    }
  });
}

function headerFunction() {
  var breakpoint = 1200;
  toggleMobileMenu(breakpoint);
  toggleMobileSubmenu(breakpoint);
  togglePcHoverState(breakpoint);
}

function lazyLoad() {
  if (document.querySelector("img[data-src]")) {
    var observer = lozad();
    observer.observe();

    if (-1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > 0) {
      var iframe = document.querySelector("iframe");
      iframe.setAttribute("src", "https://www.youtube.com/embed/O7pNpR3Py68");
    }
  }
} //計算「最新消息」、「人氣推薦」這類的卡片列表的標題字數，讓大家的高度一樣


function autoFixHeight(con) {
  var el = document.querySelectorAll(con);
  var thisHeight = -1;
  var maxHeight = -1;
  var breakpoint = 768; //為了ie不支援nodelist的forEach修正

  nodeListToArray(el).forEach(function (item) {
    item.style.height = ""; //清空之前的style

    thisHeight = item.clientHeight; //取得已經減過高度的

    maxHeight = maxHeight > thisHeight ? maxHeight : thisHeight;
  });

  if (document.body.clientWidth > breakpoint) {
    nodeListToArray(el).forEach(function (item) {
      item.style.height = "".concat(maxHeight, "px");
    });
  }
}

function textHide(num, con) {
  var el = document.querySelectorAll(con); //ie才執行這個超過字數增加刪節號，其他瀏覽器靠css語法即可
  // if (
  //   navigator.userAgent.indexOf("MSIE") !== -1 ||
  //   navigator.appVersion.indexOf("Trident/") > 0
  // ) {
  //   Array.prototype.slice.call(el).forEach(function (item) {
  //     var txt = item.innerText;
  //     if (txt.length > num) {
  //       txtContent = txt.substring(0, num - 1) + "...";
  //       item.innerHTML = txtContent;
  //     }
  //   });
  // }

  nodeListToArray(el).forEach(function (item) {
    var txt = item.innerText;

    if (txt.length > num) {
      txtContent = txt.substring(0, num - 1) + "...";
      item.innerHTML = txtContent;
    } else {
      txtContent = txt.substring(0, num) + "...";
      item.innerHTML = txtContent;
    }
  });
  autoFixHeight(con);
}

function clearCheckBox(el, target) {
  if (document.querySelector(el)) {
    var trigger = document.querySelector(el);
    var targets = document.querySelectorAll(target);
    trigger.addEventListener("click", function () {
      event.preventDefault();
      trigger.blur();
      Array.prototype.slice.call(targets).forEach(function (trigger) {
        trigger.checked = false;
      });
    });
  }
}

function cardContentFunction() {
  if (document.querySelector(".c-card-body-title") !== null) {
    autoFixHeight(".c-card-body-title");
  } //人氣推薦內文字數限制


  if (document.querySelector(".c-card-body-txt") !== null) {
    textHide(48, ".c-card-body-txt");
  }
}

function toolTips() {
  $('[data-toggle="tooltip"]').tooltip({
    placement: 'right',
    trigger: 'hover',
    offset: 30
  });
  $('[data-toggle="tooltip"]').on('inserted.bs.tooltip', function () {
    var getTootipsId = "#" + $(this).attr("aria-describedby"); // console.log(getTootipsId);
    // console.log($(this).attr("data-original-title") == "德國蔡司數位影像系統");

    if ($(this).attr("data-original-title") == "德國蔡司數位影像系統") {
      $(getTootipsId).find('.tooltip-inner').html("德國蔡司<br>數位影像系統");
    }

    if ($(this).attr("data-original-title") == "法國依視路全方位視覺檢測系統") {
      $(getTootipsId).find('.tooltip-inner').html("法國依視路<br>全方位視覺檢測系統");
    }
  });
}

function aos() {
  if (document.querySelector("[data-aos]")) {
    AOS.init({
      once: true
    });
  }
}

function ytHandler() {
  if (document.querySelector("[data-url]")) {
    var triggers = document.querySelectorAll("[data-url]");
    var target = document.querySelector("iframe");
    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      trigger.onclick = function () {
        target.setAttribute("src", trigger.getAttribute("data-url"));
        trigger.classList.add("js-active");
        Array.prototype.slice.call(triggers).filter(function (item) {
          return item !== trigger;
        }).forEach(function (item) {
          item.classList.remove("js-active");
        });
      };
    });
  }
}

function cardJustifyContent() {
  var el = $(".js-cardJustifyContent");

  if (el) {
    for (var i = 0; i < el.length; i++) {
      var elChildrenLength = el.eq(i).children('div').length; // console.log(elChildrenLength);

      if (elChildrenLength <= 2) {
        el.eq(i).addClass("justify-content-center");
      } else {
        el.eq(i).removeClass("justify-content-center");
      }
    }
  }
}

function storeFilterNotification(inputContainer, targetEl) {
  var el = document.querySelector(inputContainer);
  var target = document.querySelector(targetEl);

  if (el) {
    // console.log(inputContainer + " + " + target);
    var triggers = el.querySelectorAll("input[type='checkbox']"); // console.log(triggers);

    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        var checkedNum = el.querySelectorAll("input[type=checkbox]:checked").length; // console.log(checkedNum);

        if (checkedNum > 0) {
          target.classList.add("js-inputChecked");
        } else {
          target.classList.remove("js-inputChecked");
        }
      });
    });
    var clearAllBtnEl = document.querySelector("#js-clearCheckBoxes");
    clearAllBtnEl.addEventListener("click", function () {
      target.classList.remove("js-inputChecked");
    });
  }
}

function dateMobiscroll() {
  if ($('#dateMobiscrollId')) {
    $('#dateMobiscrollId').mobiscroll().date({
      theme: $.mobiscroll.defaults.theme,
      // Specify theme like: theme: 'ios' or omit setting to use default 
      mode: 'mixed',
      // Specify scroller mode like: mode: 'mixed' or omit setting to use default 
      display: 'modal',
      // Specify display mode like: display: 'bottom' or omit setting to use default 
      lang: 'zh' // Specify language like: lang: 'pl' or omit setting to use default 

    });
  }
} //下拉選單跳轉超連結


function selectURL() {
  var el = document.querySelector(".js-selectURL");

  if (el) {
    el.addEventListener("change", function () {
      window.open(this.options[this.selectedIndex].value);
    });
  }
} //呼叫function-網頁載入完成後


$(document).ready(function () {
  selectURL();
  toolsListener();
  createSlicks();
  tabBtnSlick();
  tabBtnSlickFixed(); //會員權益專用

  tabBtnSlickMembers();
  tabBtnSlickFixedMembers();
  rionetSlick();
  warrantyLink();
  lazyLoad();
  createAccordion(); // createVideoSwiper();

  aos();
  ytHandler();
  togglepFilterSubmenu();
  clearCheckBox("#js-clearCheckBoxes", "input[type='checkbox']");
  setStoreTableHeightAtMobile(); // $('[data-toggle="popover"]').popover();

  toolTips();
  cardJustifyContent();
  storeFilterNotification(".v-dropdown-menu", ".v-dropdown-btn");
  storeFilterNotification(".p-products-search", ".p-products-search-btn");
  toggleStoreTableHeightAtMobile();
  dateMobiscroll();
  goToAnchor(); // goTopSticky();
});

window.onload = function () {
  cardContentFunction();
  showAllTab();
}; //呼叫function-視窗大小變更


$(window).resize(function () {
  cardContentFunction();
  showAllTab(); // setStoreTableHeightAtMobile();

  confirmIfDoubleMenuOpened(".p-products-search", 992);
  tabBtnSlickFixed();
}); //呼叫function-捲動

$(window).scroll(function () {
  goTopSticky(); // sideLinkSticky();
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjQDQuMC9hc3NldHMvanMvbWFpbi5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImFqYXgiLCJ1cmwiLCJtZXRob2QiLCJkYXRhVHlwZSIsImRvbmUiLCJkYXRhIiwiaHRtbCIsImhlYWRlckZ1bmN0aW9uIiwidG9nZ2xlU2lkZUxpbmsiLCJnb1RvcCIsImdvVG9wU3RpY2t5IiwidG9vbHNMaXN0ZW5lciIsIndpbmRvdyIsImFkZEV2ZW50TGlzdGVuZXIiLCJlIiwia2V5Q29kZSIsImJvZHkiLCJjbGFzc0xpc3QiLCJyZW1vdmUiLCJhZGQiLCJxdWVyeVNlbGVjdG9yIiwidG9nZ2xlIiwiZG9jdW1lbnRFbGVtZW50IiwiaW5uZXJXaWR0aCIsIm9uY2xpY2siLCJwcmV2ZW50RGVmYXVsdCIsImFuaW1hdGUiLCJzY3JvbGxUb3AiLCJlbCIsImZvb3RlckhlaWdodFZhciIsImNsaWVudEhlaWdodCIsInNjcm9sbEhlaWdodFZhciIsInNjcm9sbEhlaWdodCIsIndpbmRvd0hlaWdodFZhciIsImlubmVySGVpZ2h0Iiwic2Nyb2xsVG9wVmFyIiwiZ29Ub0FuY2hvciIsImNsaWNrIiwidGFyZ2V0IiwiYXR0ciIsInRhcmdldFBvcyIsIm9mZnNldCIsInRvcCIsInRyaWdnZXIwMiIsInRhcmdldDAyIiwiQWNjb3JkaW9uIiwic2libGluZ3MiLCJwcm90b3R5cGUiLCJpbml0IiwidHJpZ2dlcnMiLCJxdWVyeVNlbGVjdG9yQWxsIiwiQXJyYXkiLCJzbGljZSIsImNhbGwiLCJmb3JFYWNoIiwidHJpZ2dlciIsImV2ZW50IiwiZmluZCIsInRvZ2dsZUNsYXNzIiwicmVtb3ZlQ2xhc3MiLCJhdHQiLCJnZXRBdHRyaWJ1dGUiLCJub2RlTGlzdFRvQXJyYXkiLCJub2RlTGlzdENvbGxlY3Rpb24iLCJ0b2dnbGVWaXNpYWJsZSIsIm1lZGlhUXVlcnkiLCJoYXNNZWRpYVF1ZXJ5IiwiaXNNb2JpbGUiLCJjbGlja0NvbmZpcm0iLCJjcmVhdGVBY2NvcmRpb24iLCJzdHIiLCJzaG93QWxsVGFiIiwiZmlyc3QiLCJhZGRDbGFzcyIsInBhcmVudCIsInRvZ2dsZXBGaWx0ZXJTdWJtZW51IiwidW5kZWZpbmVkIiwiY29uZmlybUlmRG91YmxlTWVudU9wZW5lZCIsImhhc0NsYXNzIiwiU2xpY2siLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNQZXJSb3ciLCJyb3dzIiwicmVzcG9uc2l2ZSIsInNsaWNrIiwiYXJyb3dzIiwicHJldkFycm93IiwibmV4dEFycm93Iiwic2V0U3RvcmVUYWJsZUhlaWdodEF0TW9iaWxlIiwidHJpZ2dlclRkIiwibnVtMDEiLCJudW0wMiIsIm51bTAzIiwidG90YWxOdW0iLCJjb250YWlucyIsInN0eWxlIiwiaGVpZ2h0IiwidG9nZ2xlU3RvcmVUYWJsZUhlaWdodEF0TW9iaWxlIiwiY3JlYXRlU2xpY2tzIiwidGFyZ2V0cyIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsInJpb25ldFNsaWNrIiwibGF6eUxvYWQiLCJkb3RzIiwidGFiQnRuU2xpY2siLCJ0YWJMZW5ndGgiLCJsZW5ndGgiLCJzbGlkZXNUb1Nob3dOdW0iLCJzbGlkZXNUb1Njcm9sbCIsImluZmluaXRlIiwidmFyaWFibGVXaWR0aCIsInRhYkJ0blNsaWNrRml4ZWQiLCJ0YWJCdG5TbGlja01lbWJlcnMiLCJ0YWJCdG5TbGlja0ZpeGVkTWVtYmVycyIsIndhcnJhbnR5TGluayIsInRvZ2dsZU1vYmlsZU1lbnUiLCJ0b2dnbGVNb2JpbGVTdWJtZW51IiwibmV4dEVsZW1lbnRTaWJsaW5nIiwicGFyZW50cyIsInNpYmluZ01vYmlsZVN1Ym1lbnUiLCJvbiIsInRvZ2dsZVBjSG92ZXJTdGF0ZSIsIml0ZW0iLCJvYnNlcnZlciIsImxvemFkIiwib2JzZXJ2ZSIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsImluZGV4T2YiLCJhcHBWZXJzaW9uIiwiaWZyYW1lIiwic2V0QXR0cmlidXRlIiwiYXV0b0ZpeEhlaWdodCIsImNvbiIsInRoaXNIZWlnaHQiLCJtYXhIZWlnaHQiLCJjbGllbnRXaWR0aCIsInRleHRIaWRlIiwibnVtIiwidHh0IiwiaW5uZXJUZXh0IiwidHh0Q29udGVudCIsInN1YnN0cmluZyIsImlubmVySFRNTCIsImNsZWFyQ2hlY2tCb3giLCJibHVyIiwiY2hlY2tlZCIsImNhcmRDb250ZW50RnVuY3Rpb24iLCJ0b29sVGlwcyIsInRvb2x0aXAiLCJwbGFjZW1lbnQiLCJnZXRUb290aXBzSWQiLCJhb3MiLCJBT1MiLCJvbmNlIiwieXRIYW5kbGVyIiwiZmlsdGVyIiwiY2FyZEp1c3RpZnlDb250ZW50IiwiaSIsImVsQ2hpbGRyZW5MZW5ndGgiLCJlcSIsImNoaWxkcmVuIiwic3RvcmVGaWx0ZXJOb3RpZmljYXRpb24iLCJpbnB1dENvbnRhaW5lciIsInRhcmdldEVsIiwiY2hlY2tlZE51bSIsImNsZWFyQWxsQnRuRWwiLCJkYXRlTW9iaXNjcm9sbCIsIm1vYmlzY3JvbGwiLCJkYXRlIiwidGhlbWUiLCJkZWZhdWx0cyIsIm1vZGUiLCJkaXNwbGF5IiwibGFuZyIsInNlbGVjdFVSTCIsIm9wZW4iLCJvcHRpb25zIiwic2VsZWN0ZWRJbmRleCIsInZhbHVlIiwib25sb2FkIiwicmVzaXplIiwic2Nyb2xsIl0sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7QUNsRkE7QUFDQUEsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzVCRixHQUFDLENBQUNHLElBQUYsQ0FBTztBQUNMQyxPQUFHLEVBQUUsbUJBREE7QUFFTEMsVUFBTSxFQUFFLEtBRkg7QUFHTEMsWUFBUSxFQUFFO0FBSEwsR0FBUCxFQUlHQyxJQUpILENBSVEsVUFBVUMsSUFBVixFQUFnQjtBQUN0QlIsS0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFhUyxJQUFiLENBQWtCRCxJQUFsQjtBQUNBRSxrQkFBYyxHQUZRLENBR3RCO0FBQ0QsR0FSRDtBQVNBVixHQUFDLENBQUNHLElBQUYsQ0FBTztBQUNMQyxPQUFHLEVBQUUscUJBREE7QUFFTEMsVUFBTSxFQUFFLEtBRkg7QUFHTEMsWUFBUSxFQUFFO0FBSEwsR0FBUCxFQUlHQyxJQUpILENBSVEsVUFBVUMsSUFBVixFQUFnQjtBQUN0QlIsS0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlUyxJQUFmLENBQW9CRCxJQUFwQixFQURzQixDQUV0Qjs7QUFDQUcsa0JBQWM7QUFDZixHQVJEO0FBU0FYLEdBQUMsQ0FBQ0csSUFBRixDQUFPO0FBQ0xDLE9BQUcsRUFBRSxtQkFEQTtBQUVMQyxVQUFNLEVBQUUsS0FGSDtBQUdMQyxZQUFRLEVBQUU7QUFITCxHQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3RCUixLQUFDLENBQUMsU0FBRCxDQUFELENBQWFTLElBQWIsQ0FBa0JELElBQWxCO0FBQ0FJLFNBQUs7QUFDTEMsZUFBVyxHQUhXLENBSXRCO0FBQ0QsR0FURCxFQW5CNEIsQ0E2QjVCO0FBQ0QsQ0E5QkQsRSxDQStCQTs7QUFDQWIsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzVCRixHQUFDLENBQUNHLElBQUYsQ0FBTztBQUNMQyxPQUFHLEVBQUUseUJBREE7QUFFTEMsVUFBTSxFQUFFLEtBRkg7QUFHTEMsWUFBUSxFQUFFO0FBSEwsR0FBUCxFQUlHQyxJQUpILENBSVEsVUFBVUMsSUFBVixFQUFnQjtBQUN0QlIsS0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlUyxJQUFmLENBQW9CRCxJQUFwQjtBQUNBRSxrQkFBYyxHQUZRLENBR3RCO0FBQ0QsR0FSRDtBQVNBVixHQUFDLENBQUNHLElBQUYsQ0FBTztBQUNMQyxPQUFHLEVBQUUsMkJBREE7QUFFTEMsVUFBTSxFQUFFLEtBRkg7QUFHTEMsWUFBUSxFQUFFO0FBSEwsR0FBUCxFQUlHQyxJQUpILENBSVEsVUFBVUMsSUFBVixFQUFnQjtBQUN0QlIsS0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQlMsSUFBakIsQ0FBc0JELElBQXRCLEVBRHNCLENBRXRCOztBQUNBRyxrQkFBYztBQUNmLEdBUkQ7QUFTQVgsR0FBQyxDQUFDRyxJQUFGLENBQU87QUFDTEMsT0FBRyxFQUFFLHlCQURBO0FBRUxDLFVBQU0sRUFBRSxLQUZIO0FBR0xDLFlBQVEsRUFBRTtBQUhMLEdBQVAsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLElBQVYsRUFBZ0I7QUFDdEJSLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZVMsSUFBZixDQUFvQkQsSUFBcEI7QUFDQUksU0FBSztBQUNMQyxlQUFXLEdBSFcsQ0FJdEI7QUFDRCxHQVRELEVBbkI0QixDQTZCNUI7QUFDRCxDQTlCRDs7QUErQkEsU0FBU0MsYUFBVCxHQUF5QjtBQUN2QkMsUUFBTSxDQUFDQyxnQkFBUCxDQUF3QixTQUF4QixFQUFtQyxVQUFVQyxDQUFWLEVBQWE7QUFDOUMsUUFBSUEsQ0FBQyxDQUFDQyxPQUFGLEtBQWMsQ0FBbEIsRUFBcUI7QUFDbkJqQixjQUFRLENBQUNrQixJQUFULENBQWNDLFNBQWQsQ0FBd0JDLE1BQXhCLENBQStCLGFBQS9CO0FBQ0FwQixjQUFRLENBQUNrQixJQUFULENBQWNDLFNBQWQsQ0FBd0JFLEdBQXhCLENBQTRCLGdCQUE1QjtBQUNEO0FBQ0YsR0FMRDtBQU1BUCxRQUFNLENBQUNDLGdCQUFQLENBQXdCLFdBQXhCLEVBQXFDLFVBQVVDLENBQVYsRUFBYTtBQUNoRGhCLFlBQVEsQ0FBQ2tCLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkMsTUFBeEIsQ0FBK0IsZ0JBQS9CO0FBQ0FwQixZQUFRLENBQUNrQixJQUFULENBQWNDLFNBQWQsQ0FBd0JFLEdBQXhCLENBQTRCLGFBQTVCO0FBQ0QsR0FIRDtBQUlEOztBQUVELFNBQVNYLGNBQVQsR0FBMEI7QUFDeEJWLFVBQVEsQ0FDTHNCLGFBREgsQ0FDaUIsb0JBRGpCLEVBRUdQLGdCQUZILENBRW9CLE9BRnBCLEVBRTZCLFlBQVk7QUFDckMsU0FBS0ksU0FBTCxDQUFlSSxNQUFmLENBQXNCLG1CQUF0QjtBQUNBdkIsWUFBUSxDQUFDc0IsYUFBVCxDQUF1QixXQUF2QixFQUFvQ0gsU0FBcEMsQ0FBOENJLE1BQTlDLENBQXFELG1CQUFyRDtBQUNBdkIsWUFBUSxDQUFDc0IsYUFBVCxDQUF1QixPQUF2QixFQUFnQ0gsU0FBaEMsQ0FBMENJLE1BQTFDLENBQWlELG1CQUFqRDtBQUNBdkIsWUFBUSxDQUFDc0IsYUFBVCxDQUF1QixNQUF2QixFQUErQkgsU0FBL0IsQ0FBeUNJLE1BQXpDLENBQWdELGlCQUFoRCxFQUpxQyxDQUtyQzs7QUFDQXZCLFlBQVEsQ0FBQ3dCLGVBQVQsQ0FBeUJMLFNBQXpCLENBQW1DQyxNQUFuQyxDQUEwQyxlQUExQztBQUNBcEIsWUFBUSxDQUFDc0IsYUFBVCxDQUF1QixxQkFBdkIsRUFBOENILFNBQTlDLENBQXdEQyxNQUF4RCxDQUErRCxlQUEvRDtBQUNBcEIsWUFBUSxDQUFDc0IsYUFBVCxDQUF1QixnQkFBdkIsRUFBeUNILFNBQXpDLENBQW1EQyxNQUFuRCxDQUEwRCxlQUExRCxFQVJxQyxDQVNyQzs7QUFDQXBCLFlBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsaUJBQXZCLEVBQTBDSCxTQUExQyxDQUFvREksTUFBcEQsQ0FBMkQsbUJBQTNEO0FBQ0QsR0FiSDtBQWNBVCxRQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsUUFBSUQsTUFBTSxDQUFDVyxVQUFQLElBQXFCLElBQXpCLEVBQStCO0FBQzdCekIsY0FBUSxDQUFDc0IsYUFBVCxDQUF1QixvQkFBdkIsRUFBNkNILFNBQTdDLENBQXVEQyxNQUF2RCxDQUE4RCxtQkFBOUQ7QUFDQXBCLGNBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsV0FBdkIsRUFBb0NILFNBQXBDLENBQThDQyxNQUE5QyxDQUFxRCxtQkFBckQ7QUFDQXBCLGNBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsT0FBdkIsRUFBZ0NILFNBQWhDLENBQTBDQyxNQUExQyxDQUFpRCxtQkFBakQ7QUFDQXBCLGNBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0JILFNBQS9CLENBQXlDQyxNQUF6QyxDQUFnRCxpQkFBaEQsRUFKNkIsQ0FLN0I7O0FBQ0FwQixjQUFRLENBQUNzQixhQUFULENBQXVCLGlCQUF2QixFQUEwQ0gsU0FBMUMsQ0FBb0RDLE1BQXBELENBQTJELG1CQUEzRDtBQUNEO0FBQ0YsR0FURDtBQVVEOztBQUVELFNBQVNULEtBQVQsR0FBaUI7QUFDZlgsVUFBUSxDQUFDc0IsYUFBVCxDQUF1QixRQUF2QixFQUFpQ0ksT0FBakMsR0FBMkMsVUFBVVYsQ0FBVixFQUFhO0FBQ3REQSxLQUFDLENBQUNXLGNBQUY7QUFDQTVCLEtBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0I2QixPQUFoQixDQUF3QjtBQUN0QkMsZUFBUyxFQUFFO0FBRFcsS0FBeEIsRUFHRSxHQUhGO0FBS0QsR0FQRDtBQVFEOztBQUVELFNBQVNqQixXQUFULEdBQXVCO0FBQ3JCLE1BQUlrQixFQUFFLEdBQUc5QixRQUFRLENBQUNzQixhQUFULENBQXVCLGlCQUF2QixDQUFULENBRHFCLENBRXJCOztBQUNBLE1BQUlTLGVBQWUsR0FBRy9CLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsV0FBdkIsRUFBb0NVLFlBQTFELENBSHFCLENBSXJCO0FBQ0E7O0FBQ0EsTUFBSUMsZUFBZSxHQUFHakMsUUFBUSxDQUFDd0IsZUFBVCxDQUF5QlUsWUFBL0MsQ0FOcUIsQ0FPckI7QUFDQTs7QUFDQSxNQUFJQyxlQUFlLEdBQUdyQixNQUFNLENBQUNzQixXQUE3QixDQVRxQixDQVVyQjtBQUNBOztBQUNBLE1BQUlDLFlBQVksR0FBR3JDLFFBQVEsQ0FBQ3dCLGVBQVQsQ0FBeUJLLFNBQTVDLENBWnFCLENBYXJCO0FBQ0E7QUFDQTs7QUFDQSxNQUFJSSxlQUFlLElBQUlFLGVBQWUsR0FBR0UsWUFBbEIsR0FBaUNOLGVBQXhELEVBQXlFO0FBQ3ZFRCxNQUFFLENBQUNYLFNBQUgsQ0FBYUMsTUFBYixDQUFvQixXQUFwQjtBQUNELEdBRkQsTUFFTztBQUNMVSxNQUFFLENBQUNYLFNBQUgsQ0FBYUUsR0FBYixDQUFpQixXQUFqQjtBQUNEO0FBQ0Y7O0FBRUQsU0FBU2lCLFVBQVQsR0FBc0I7QUFDcEJ2QyxHQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQndDLEtBQXBCLENBQTBCLFVBQVV2QixDQUFWLEVBQWE7QUFDckNBLEtBQUMsQ0FBQ1csY0FBRjtBQUNBLFFBQUlhLE1BQU0sR0FBR3pDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBDLElBQVIsQ0FBYSxNQUFiLENBQWI7QUFDQSxRQUFJQyxTQUFTLEdBQUczQyxDQUFDLENBQUN5QyxNQUFELENBQUQsQ0FBVUcsTUFBVixHQUFtQkMsR0FBbkM7QUFDQTdDLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZTZCLE9BQWYsQ0FBdUI7QUFDckJDLGVBQVMsRUFBRWEsU0FBUyxHQUFHO0FBREYsS0FBdkIsRUFFRyxJQUZIO0FBR0EsUUFBSUcsU0FBUyxHQUFHN0MsUUFBUSxDQUFDc0IsYUFBVCxDQUF1QixZQUF2QixDQUFoQjtBQUNBLFFBQUl3QixRQUFRLEdBQUc5QyxRQUFRLENBQUNzQixhQUFULENBQXVCLE9BQXZCLENBQWY7QUFDQXVCLGFBQVMsQ0FBQzFCLFNBQVYsQ0FBb0JDLE1BQXBCLENBQTJCLGVBQTNCO0FBQ0EwQixZQUFRLENBQUMzQixTQUFULENBQW1CQyxNQUFuQixDQUEwQixlQUExQjtBQUNBcEIsWUFBUSxDQUFDd0IsZUFBVCxDQUF5QkwsU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0QsR0FaRDtBQWFELEMsQ0FDRDs7O0FBQ0EsU0FBUzJCLFNBQVQsQ0FBbUJqQixFQUFuQixFQUF1QlUsTUFBdkIsRUFBK0JRLFFBQS9CLEVBQXlDO0FBQ3ZDLE9BQUtsQixFQUFMLEdBQVVBLEVBQVY7QUFDQSxPQUFLVSxNQUFMLEdBQWNBLE1BQWQ7QUFDQSxPQUFLUSxRQUFMLEdBQWdCQSxRQUFoQjtBQUNEOztBQUNERCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JDLElBQXBCLEdBQTJCLFlBQVk7QUFDckMsTUFBSUMsUUFBUSxHQUFHbkQsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEIsS0FBS3RCLEVBQS9CLENBQWY7QUFDQSxNQUFJVSxNQUFNLEdBQUcsS0FBS0EsTUFBbEI7QUFDQSxNQUFJUSxRQUFRLEdBQUcsS0FBS0EsUUFBcEI7QUFDQUssT0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkJKLFFBQTNCLEVBQXFDSyxPQUFyQyxDQUE2QyxVQUFVQyxPQUFWLEVBQW1CO0FBQzlEQSxXQUFPLENBQUMxQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDMkMsV0FBSyxDQUFDL0IsY0FBTjs7QUFDQSxVQUFJcUIsUUFBUSxJQUFJLElBQWhCLEVBQXNCO0FBQ3BCLFlBQUksS0FBSzFCLGFBQUwsQ0FBbUJrQixNQUFuQixNQUErQixJQUFuQyxFQUF5QztBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBekMsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNEQsSUFBUixDQUFhbkIsTUFBYixFQUFxQm9CLFdBQXJCLENBQWlDLHNCQUFqQyxFQUx1QyxDQU12Qzs7QUFDQTdELFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTZELFdBQVIsQ0FBb0IsdUJBQXBCO0FBQ0E3RCxXQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxRQUFSLEdBQW1CVyxJQUFuQixDQUF3Qm5CLE1BQXhCLEVBQWdDcUIsV0FBaEMsQ0FBNEMsc0JBQTVDLEVBUnVDLENBU3ZDOztBQUNBOUQsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUQsUUFBUixHQUFtQmEsV0FBbkIsQ0FBK0IsdUJBQS9CO0FBQ0QsU0FabUIsQ0FhcEI7QUFDQTs7O0FBQ0EsWUFBSUMsR0FBRyxHQUFHL0QsQ0FBQyxDQUFDMEQsT0FBRCxDQUFELENBQVdoQixJQUFYLENBQWdCLGFBQWhCLENBQVY7QUFDQTFDLFNBQUMsQ0FBQytELEdBQUQsQ0FBRCxDQUFPRixXQUFQLENBQW1CLHNCQUFuQixFQUEyQ1osUUFBM0MsR0FBc0RhLFdBQXRELENBQWtFLHNCQUFsRSxFQWhCb0IsQ0FpQnBCO0FBQ0E7QUFDQTtBQUNELE9BcEJELE1Bb0JPO0FBQ0wsWUFBSSxLQUFLdkMsYUFBTCxDQUFtQmtCLE1BQW5CLE1BQStCLElBQW5DLEVBQXlDO0FBQ3ZDLGVBQUtsQixhQUFMLENBQW1Ca0IsTUFBbkIsRUFBMkJyQixTQUEzQixDQUFxQ0ksTUFBckMsQ0FDRSxzQkFERjtBQUdEOztBQUNEdkIsZ0JBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUJtQyxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsYUFBckIsQ0FBdkIsRUFBNEQ1QyxTQUE1RCxDQUFzRUksTUFBdEUsQ0FBNkUsc0JBQTdFO0FBQ0FrQyxlQUFPLENBQUN0QyxTQUFSLENBQWtCSSxNQUFsQixDQUF5QixzQkFBekI7QUFDRDtBQUNGLEtBL0JEO0FBZ0NELEdBakNEO0FBa0NELENBdENEOztBQXdDQSxTQUFTeUMsZUFBVCxDQUF5QkMsa0JBQXpCLEVBQTZDO0FBQzNDLFNBQU9aLEtBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCVSxrQkFBM0IsQ0FBUDtBQUNELEMsQ0FDRDs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUEsU0FBU0MsY0FBVCxDQUF3QnBDLEVBQXhCLEVBQTRCVSxNQUE1QixFQUFvQzJCLFVBQXBDLEVBQWdEO0FBQzlDLE1BQUloQixRQUFRLEdBQUduRCxRQUFRLENBQUNvRCxnQkFBVCxDQUEwQnRCLEVBQTFCLENBQWY7QUFDQSxNQUFJVSxNQUFNLEdBQUd4QyxRQUFRLENBQUNzQixhQUFULENBQXVCa0IsTUFBdkIsQ0FBYjs7QUFDQSxNQUFJQSxNQUFKLEVBQVk7QUFDVndCLG1CQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuREEsYUFBTyxDQUFDMUMsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1QzJDLGFBQUssQ0FBQy9CLGNBQU47QUFDQSxhQUFLUixTQUFMLENBQWVJLE1BQWYsQ0FBc0IsV0FBdEI7QUFDQWlCLGNBQU0sQ0FBQ3JCLFNBQVAsQ0FBaUJJLE1BQWpCLENBQXdCLFdBQXhCO0FBQ0EsWUFBSTZDLGFBQWEsR0FBR0QsVUFBcEI7O0FBQ0EsWUFBSUMsYUFBYSxLQUFLLEVBQXRCLEVBQTBCO0FBQ3hCLGNBQUlDLFFBQVEsR0FBR3ZELE1BQU0sQ0FBQ1csVUFBUCxHQUFvQjBDLFVBQW5DOztBQUNBLGNBQUlFLFFBQUosRUFBYztBQUNackUsb0JBQVEsQ0FBQ3dCLGVBQVQsQ0FBeUJMLFNBQXpCLENBQW1DSSxNQUFuQyxDQUEwQyx1QkFBMUM7QUFDRDtBQUNGLFNBTEQsTUFLTztBQUNMdkIsa0JBQVEsQ0FBQ3dCLGVBQVQsQ0FBeUJMLFNBQXpCLENBQW1DQyxNQUFuQyxDQUEwQyx1QkFBMUM7QUFDRDs7QUFDRE4sY0FBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLGNBQUlELE1BQU0sQ0FBQ1csVUFBUCxJQUFxQjBDLFVBQXpCLEVBQXFDO0FBQ25DbkUsb0JBQVEsQ0FBQ3dCLGVBQVQsQ0FBeUJMLFNBQXpCLENBQW1DQyxNQUFuQyxDQUEwQyx1QkFBMUM7QUFDRDtBQUNGLFNBSkQ7QUFLRCxPQWxCRDtBQW1CRCxLQXBCRDtBQXFCRDtBQUNGO0FBQ0Q7O0FBQ0E7OztBQUNBLFNBQVNrRCxZQUFULENBQXNCeEMsRUFBdEIsRUFBMEJVLE1BQTFCLEVBQWtDO0FBQ2hDLE1BQUlXLFFBQVEsR0FBR25ELFFBQVEsQ0FBQ29ELGdCQUFULENBQTBCdEIsRUFBMUIsQ0FBZjtBQUNBLE1BQUlVLE1BQU0sR0FBR3hDLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUJrQixNQUF2QixDQUFiOztBQUNBLE1BQUlBLE1BQUosRUFBWTtBQUNWd0IsbUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25EQSxhQUFPLENBQUMxQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDMkMsYUFBSyxDQUFDL0IsY0FBTjtBQUNBYSxjQUFNLENBQUNyQixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixXQUF4QjtBQUNBcEIsZ0JBQVEsQ0FBQ3dCLGVBQVQsQ0FBeUJMLFNBQXpCLENBQW1DQyxNQUFuQyxDQUEwQyx1QkFBMUM7QUFDRCxPQUpEO0FBS0QsS0FORDtBQU9EO0FBQ0YsQyxDQUNEOzs7QUFDQSxTQUFTbUQsZUFBVCxHQUEyQjtBQUN6QixNQUFJdkUsUUFBUSxDQUFDc0IsYUFBVCxDQUF1QixrQkFBdkIsQ0FBSixFQUFnRDtBQUM5QyxRQUFJeUIsU0FBSixDQUFjLGtCQUFkLEVBQWtDLHVCQUFsQyxFQUEyRCxJQUEzRCxFQUFpRUcsSUFBakU7QUFDRDtBQUNGLEMsQ0FDRDs7O0FBQ0EsSUFBSXNCLEdBQUcsR0FBRyxDQUFWOztBQUVBLFNBQVNDLFVBQVQsR0FBc0I7QUFDcEIsTUFBSTNELE1BQU0sQ0FBQ1csVUFBUCxHQUFvQixHQUFwQixJQUEyQitDLEdBQUcsSUFBSSxDQUF0QyxFQUF5QztBQUN2Q3pFLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZThELFdBQWYsQ0FBMkIsYUFBM0IsRUFBMENhLEtBQTFDLEdBQWtEQyxRQUFsRCxDQUEyRCxhQUEzRDtBQUNBNUUsS0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQjhELFdBQWxCLENBQThCLFFBQTlCLEVBQXdDZSxNQUF4QyxHQUFpREYsS0FBakQsR0FBeURmLElBQXpELENBQThELGNBQTlELEVBQThFZ0IsUUFBOUUsQ0FBdUYsUUFBdkY7QUFDQUgsT0FBRyxHQUFHLENBQU47QUFDRDs7QUFDRCxNQUFJMUQsTUFBTSxDQUFDVyxVQUFQLEdBQW9CLEdBQXBCLElBQTJCK0MsR0FBRyxJQUFJLENBQXRDLEVBQXlDO0FBQ3ZDekUsS0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlNEUsUUFBZixDQUF3QixhQUF4QjtBQUNBSCxPQUFHLEdBQUcsQ0FBTjtBQUNELEdBVG1CLENBVXBCOztBQUNEOztBQUVELFNBQVNLLG9CQUFULEdBQWdDO0FBQzlCO0FBQ0E7QUFDQSxNQUFJN0UsUUFBUSxDQUFDc0IsYUFBVCxDQUF1Qiw2QkFBdkIsQ0FBSixFQUEyRDtBQUN6RCxRQUFJeUIsU0FBSixDQUFjLDZCQUFkLEVBQTZDK0IsU0FBN0MsRUFBd0QsS0FBeEQsRUFBK0Q1QixJQUEvRDtBQUNEOztBQUNELE1BQUlDLFFBQVEsR0FBR25ELFFBQVEsQ0FBQ29ELGdCQUFULENBQTBCLGtCQUExQixDQUFmO0FBQ0FZLGlCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxRQUFJM0IsRUFBRSxHQUFHOUIsUUFBUSxDQUFDc0IsYUFBVCxDQUF1Qm1DLE9BQU8sQ0FBQ00sWUFBUixDQUFxQixhQUFyQixDQUF2QixDQUFUO0FBQ0FOLFdBQU8sQ0FBQzFDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUMsVUFBSWUsRUFBRSxDQUFDUixhQUFILENBQWlCLDZCQUFqQixDQUFKLEVBQXFEO0FBQ25EUSxVQUFFLENBQUNSLGFBQUgsQ0FBaUIsNkJBQWpCLEVBQWdESCxTQUFoRCxDQUEwREMsTUFBMUQsQ0FBaUUsc0JBQWpFO0FBQ0FVLFVBQUUsQ0FBQ1IsYUFBSCxDQUFpQiw2QkFBakIsRUFBZ0RILFNBQWhELENBQTBEQyxNQUExRCxDQUFpRSxzQkFBakU7QUFDRDtBQUNGLEtBTEQ7QUFNRCxHQVJELEVBUDhCLENBZ0I5QjtBQUNBOztBQUNBOEMsZ0JBQWMsQ0FBQyxRQUFELEVBQVcsb0JBQVgsRUFBaUMsRUFBakMsQ0FBZDtBQUNBQSxnQkFBYyxDQUFDLHdCQUFELEVBQTJCLG9CQUEzQixFQUFpRCxHQUFqRCxDQUFkO0FBQ0FJLGNBQVksQ0FBQyxhQUFELEVBQWdCLG9CQUFoQixDQUFaLENBcEI4QixDQXFCOUI7O0FBQ0FKLGdCQUFjLENBQUMsaUJBQUQsRUFBb0Isa0JBQXBCLEVBQXdDLEdBQXhDLENBQWQ7QUFDQUEsZ0JBQWMsQ0FBQyxRQUFELEVBQVcsa0JBQVgsRUFBK0IsRUFBL0IsQ0FBZDtBQUNBSSxjQUFZLENBQUMsYUFBRCxFQUFnQixrQkFBaEIsQ0FBWixDQXhCOEIsQ0F5QjlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVEOztBQUVELFNBQVNTLHlCQUFULENBQW1DakQsRUFBbkMsRUFBdUNxQyxVQUF2QyxFQUFtRDtBQUNqRCxNQUFJckQsTUFBTSxDQUFDVyxVQUFQLEdBQW9CMEMsVUFBcEIsSUFBa0NwRSxDQUFDLENBQUMrQixFQUFELENBQUQsQ0FBTWtELFFBQU4sQ0FBZSxXQUFmLENBQWxDLElBQWlFaEYsUUFBUSxDQUFDd0IsZUFBVCxDQUF5QkwsU0FBekIsSUFBc0MsRUFBM0csRUFBK0c7QUFDN0duQixZQUFRLENBQUN3QixlQUFULENBQXlCTCxTQUF6QixDQUFtQ0UsR0FBbkMsQ0FBdUMsZUFBdkM7QUFDRDtBQUNGLEMsQ0FDRDs7O0FBQ0EsU0FBUzRELEtBQVQsQ0FBZW5ELEVBQWYsRUFBbUJvRCxZQUFuQixFQUFpQ0MsWUFBakMsRUFBK0NDLElBQS9DLEVBQXFEQyxVQUFyRCxFQUFpRTtBQUMvRCxPQUFLdkQsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsT0FBS29ELFlBQUwsR0FBb0JBLFlBQXBCO0FBQ0EsT0FBS0MsWUFBTCxHQUFvQkEsWUFBcEI7QUFDQSxPQUFLQyxJQUFMLEdBQVlBLElBQVo7QUFDQSxPQUFLQyxVQUFMLEdBQWtCQSxVQUFsQjtBQUNEOztBQUFBOztBQUVESixLQUFLLENBQUNoQyxTQUFOLENBQWdCQyxJQUFoQixHQUF1QixZQUFZO0FBQ2pDbkQsR0FBQyxDQUFDLEtBQUsrQixFQUFMLEdBQVUsV0FBWCxDQUFELENBQXlCd0QsS0FBekIsQ0FBK0I7QUFDN0JDLFVBQU0sRUFBRSxJQURxQjtBQUU3QkwsZ0JBQVksRUFBRSxLQUFLQSxZQUZVO0FBRzdCQyxnQkFBWSxFQUFFLEtBQUtBLFlBSFU7QUFJN0JDLFFBQUksRUFBRSxLQUFLQSxJQUprQjtBQUs3QkksYUFBUyxzR0FMb0I7QUFNN0JDLGFBQVMsdUdBTm9CO0FBTzdCO0FBQ0E7QUFDQUosY0FBVSxFQUFFLEtBQUtBO0FBVFksR0FBL0I7QUFXRCxDQVpEOztBQWNBLFNBQVNLLDJCQUFULEdBQXVDO0FBQ3JDdkMsVUFBUSxHQUFHbkQsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEIsZ0NBQTFCLENBQVg7QUFDQVksaUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFFBQUlrQyxTQUFTLEdBQUdsQyxPQUFPLENBQUNMLGdCQUFSLENBQXlCLElBQXpCLENBQWhCO0FBQ0EsUUFBSXdDLEtBQUssR0FBR0QsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhM0QsWUFBekI7QUFDQSxRQUFJNkQsS0FBSyxHQUFHRixTQUFTLENBQUMsQ0FBRCxDQUFULENBQWEzRCxZQUF6QjtBQUNBLFFBQUk4RCxLQUFLLEdBQUdILFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTNELFlBQXpCO0FBQ0EsUUFBSStELFFBQVEsR0FBR0gsS0FBSyxHQUFHQyxLQUFSLEdBQWdCQyxLQUEvQjs7QUFDQSxRQUFJaEYsTUFBTSxDQUFDVyxVQUFQLEdBQW9CLElBQXBCLElBQTRCZ0MsT0FBTyxDQUFDdEMsU0FBUixDQUFrQjZFLFFBQWxCLENBQTJCLFdBQTNCLEtBQTJDLEtBQTNFLEVBQWtGO0FBQ2hGdkMsYUFBTyxDQUFDd0MsS0FBUixDQUFjQyxNQUFkLGFBQTBCSCxRQUExQjtBQUNELEtBRkQsTUFFTyxJQUFJakYsTUFBTSxDQUFDVyxVQUFQLEdBQW9CLElBQXhCLEVBQThCO0FBQ25DZ0MsYUFBTyxDQUFDd0MsS0FBUixDQUFjQyxNQUFkLEdBQXVCLEVBQXZCO0FBQ0F6QyxhQUFPLENBQUN0QyxTQUFSLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QjtBQUNBcUMsYUFBTyxDQUFDbkMsYUFBUixDQUFzQixHQUF0QixFQUEyQkgsU0FBM0IsQ0FBcUNDLE1BQXJDLENBQTRDLGtCQUE1QztBQUNEOztBQUNETixVQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsVUFBSUQsTUFBTSxDQUFDVyxVQUFQLEdBQW9CLElBQXBCLElBQTRCZ0MsT0FBTyxDQUFDdEMsU0FBUixDQUFrQjZFLFFBQWxCLENBQTJCLFdBQTNCLEtBQTJDLEtBQTNFLEVBQWtGO0FBQ2hGdkMsZUFBTyxDQUFDd0MsS0FBUixDQUFjQyxNQUFkLGFBQTBCSCxRQUExQjtBQUNELE9BRkQsTUFFTyxJQUFJakYsTUFBTSxDQUFDVyxVQUFQLEdBQW9CLElBQXhCLEVBQThCO0FBQ25DZ0MsZUFBTyxDQUFDd0MsS0FBUixDQUFjQyxNQUFkLEdBQXVCLEVBQXZCO0FBQ0F6QyxlQUFPLENBQUN0QyxTQUFSLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QjtBQUNBcUMsZUFBTyxDQUFDbkMsYUFBUixDQUFzQixHQUF0QixFQUEyQkgsU0FBM0IsQ0FBcUNDLE1BQXJDLENBQTRDLGtCQUE1QztBQUNEO0FBQ0YsS0FSRDtBQVNELEdBdEJEO0FBdUJEOztBQUVELFNBQVMrRSw4QkFBVCxHQUEwQztBQUN4Q2hELFVBQVEsR0FBR25ELFFBQVEsQ0FBQ29ELGdCQUFULENBQTBCLGdDQUExQixDQUFYO0FBQ0FZLGlCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxRQUFJa0MsU0FBUyxHQUFHbEMsT0FBTyxDQUFDTCxnQkFBUixDQUF5QixJQUF6QixDQUFoQjtBQUNBLFFBQUl3QyxLQUFLLEdBQUdELFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTNELFlBQXpCO0FBQ0EsUUFBSTZELEtBQUssR0FBR0YsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhM0QsWUFBekI7QUFDQSxRQUFJOEQsS0FBSyxHQUFHSCxTQUFTLENBQUMsQ0FBRCxDQUFULENBQWEzRCxZQUF6QjtBQUNBLFFBQUkrRCxRQUFRLEdBQUdILEtBQUssR0FBR0MsS0FBUixHQUFnQkMsS0FBL0I7QUFDQXJDLFdBQU8sQ0FBQ25DLGFBQVIsQ0FBc0IsZ0JBQXRCLEVBQXdDUCxnQkFBeEMsQ0FBeUQsT0FBekQsRUFBa0UsWUFBWTtBQUM1RSxVQUFJRCxNQUFNLENBQUNXLFVBQVAsR0FBb0IsSUFBcEIsSUFBNEJnQyxPQUFPLENBQUN3QyxLQUFSLENBQWNDLE1BQWQsSUFBd0IsRUFBeEQsRUFBNEQ7QUFDMUR6QyxlQUFPLENBQUN3QyxLQUFSLENBQWNDLE1BQWQsYUFBMEJILFFBQTFCO0FBQ0F0QyxlQUFPLENBQUN0QyxTQUFSLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QjtBQUNBcUMsZUFBTyxDQUFDbkMsYUFBUixDQUFzQixHQUF0QixFQUEyQkgsU0FBM0IsQ0FBcUNDLE1BQXJDLENBQTRDLGtCQUE1QztBQUNELE9BSkQsTUFJTztBQUNMcUMsZUFBTyxDQUFDd0MsS0FBUixDQUFjQyxNQUFkLEdBQXVCLEVBQXZCO0FBQ0F6QyxlQUFPLENBQUN0QyxTQUFSLENBQWtCRSxHQUFsQixDQUFzQixXQUF0QjtBQUNBb0MsZUFBTyxDQUFDbkMsYUFBUixDQUFzQixHQUF0QixFQUEyQkgsU0FBM0IsQ0FBcUNFLEdBQXJDLENBQXlDLGtCQUF6QztBQUNEO0FBQ0YsS0FWRDtBQVdELEdBakJEO0FBa0JELEMsQ0FFRDs7O0FBQ0EsU0FBUytFLFlBQVQsR0FBd0I7QUFDdEIsTUFBSXBHLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsVUFBdkIsQ0FBSixFQUF3QztBQUN0QyxRQUFJK0UsT0FBTyxHQUFHLENBQUMsT0FBRCxFQUFVLGlCQUFWLENBQWQ7QUFDQUEsV0FBTyxDQUFDN0MsT0FBUixDQUFnQixVQUFVaEIsTUFBVixFQUFrQjtBQUNoQyxVQUFJeUMsS0FBSixDQUFVekMsTUFBVixFQUFrQixDQUFsQixFQUFxQixDQUFyQixFQUF3QixDQUF4QixFQUEyQixDQUFDO0FBQzFCOEQsa0JBQVUsRUFBRSxHQURjO0FBRTFCQyxnQkFBUSxFQUFFO0FBQ1JyQixzQkFBWSxFQUFFO0FBRE47QUFGZ0IsT0FBRCxFQU0zQjtBQUNFb0Isa0JBQVUsRUFBRSxHQURkO0FBRUVDLGdCQUFRLEVBQUU7QUFDUnJCLHNCQUFZLEVBQUUsQ0FETjtBQUVSSyxnQkFBTSxFQUFFO0FBRkE7QUFGWixPQU4yQixDQUEzQixFQWFHckMsSUFiSDtBQWNELEtBZkQ7QUFnQkEsUUFBSStCLEtBQUosQ0FBVSxRQUFWLEVBQW9CLENBQXBCLEVBQXVCLENBQXZCLEVBQTBCLENBQTFCLEVBQTZCLENBQUM7QUFDNUJxQixnQkFBVSxFQUFFLEdBRGdCO0FBRTVCQyxjQUFRLEVBQUU7QUFDUnJCLG9CQUFZLEVBQUUsQ0FETjtBQUVSQyxvQkFBWSxFQUFFLENBRk47QUFHUkMsWUFBSSxFQUFFO0FBSEU7QUFGa0IsS0FBRCxFQVE3QjtBQUNFa0IsZ0JBQVUsRUFBRSxHQURkO0FBRUVDLGNBQVEsRUFBRTtBQUNSckIsb0JBQVksRUFBRSxDQUROO0FBRVJDLG9CQUFZLEVBQUUsQ0FGTjtBQUdSQyxZQUFJLEVBQUUsQ0FIRTtBQUlSRyxjQUFNLEVBQUU7QUFKQTtBQUZaLEtBUjZCLENBQTdCLEVBaUJHckMsSUFqQkg7QUFrQkQ7QUFDRjs7QUFFRCxTQUFTc0QsV0FBVCxHQUF1QjtBQUNyQixNQUFJMUUsRUFBRSxHQUFHLGdCQUFUOztBQUNBLE1BQUk5QixRQUFRLENBQUNzQixhQUFULENBQXVCUSxFQUF2QixDQUFKLEVBQWdDO0FBQzlCL0IsS0FBQyxDQUFDK0IsRUFBRCxDQUFELENBQU13RCxLQUFOLENBQVk7QUFDVjtBQUNBO0FBQ0FKLGtCQUFZLEVBQUUsQ0FISjtBQUlWSyxZQUFNLEVBQUUsSUFKRTtBQUtWQyxlQUFTLHFHQUxDO0FBTVZDLGVBQVMsc0dBTkM7QUFPVmdCLGNBQVEsRUFBRSxhQVBBO0FBUVZwQixnQkFBVSxFQUFFLENBQUM7QUFDWGlCLGtCQUFVLEVBQUUsR0FERDtBQUVYQyxnQkFBUSxFQUFFO0FBQ1JyQixzQkFBWSxFQUFFLENBRE47QUFFUndCLGNBQUksRUFBRTtBQUZFO0FBRkMsT0FBRCxFQU9aO0FBQ0VKLGtCQUFVLEVBQUUsR0FEZDtBQUVFQyxnQkFBUSxFQUFFO0FBQ1JyQixzQkFBWSxFQUFFLENBRE47QUFFUkssZ0JBQU0sRUFBRSxLQUZBO0FBR1I7QUFDQW1CLGNBQUksRUFBRTtBQUpFO0FBRlosT0FQWTtBQVJGLEtBQVosRUFEOEIsQ0EyQjlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNEO0FBQ0Y7O0FBRUQsU0FBU0MsV0FBVCxHQUF1QjtBQUNyQixNQUFJN0UsRUFBRSxHQUFHLGdCQUFUOztBQUNBLE1BQUk5QixRQUFRLENBQUNzQixhQUFULENBQXVCUSxFQUF2QixDQUFKLEVBQWdDO0FBQzlCLFFBQUk4RSxTQUFTLEdBQUc1RyxRQUFRLENBQUNzQixhQUFULENBQXVCUSxFQUF2QixFQUEyQnNCLGdCQUEzQixDQUE0QyxrQkFBNUMsRUFBZ0V5RCxNQUFoRjtBQUNBLFFBQUlDLGVBQWUsR0FBSUYsU0FBUyxHQUFHLENBQWIsR0FBa0IsQ0FBbEIsR0FBc0JBLFNBQTVDLENBRjhCLENBRzlCOztBQUNBN0csS0FBQyxDQUFDK0IsRUFBRCxDQUFELENBQU13RCxLQUFOLENBQVk7QUFDVkosa0JBQVksRUFBRTRCLGVBREo7QUFFVkMsb0JBQWMsRUFBRSxDQUZOO0FBR1Z4QixZQUFNLEVBQUUsSUFIRTtBQUlWQyxlQUFTLHFHQUpDO0FBS1ZDLGVBQVMsc0dBTEM7QUFNVmdCLGNBQVEsRUFBRSxhQU5BO0FBT1ZPLGNBQVEsRUFBRSxLQVBBO0FBUVYzQixnQkFBVSxFQUFFLENBQUM7QUFDWGlCLGtCQUFVLEVBQUUsSUFERDtBQUVYQyxnQkFBUSxFQUFFO0FBQ1JyQixzQkFBWSxFQUFFLENBRE47QUFFUitCLHVCQUFhLEVBQUUsS0FGUCxDQUdSO0FBQ0E7QUFDQTs7QUFMUTtBQUZDLE9BQUQsRUFTVDtBQUNEWCxrQkFBVSxFQUFFLEdBRFg7QUFFREMsZ0JBQVEsRUFBRTtBQUNSckIsc0JBQVksRUFBRSxDQUROO0FBRVIrQix1QkFBYSxFQUFFLEtBRlAsQ0FHUjtBQUNBOztBQUpRO0FBRlQsT0FUUyxFQWtCWjtBQUNFWCxrQkFBVSxFQUFFLEdBRGQ7QUFFRUMsZ0JBQVEsRUFBRTtBQUNSckIsc0JBQVksRUFBRSxDQUROO0FBRVIrQix1QkFBYSxFQUFFLElBRlA7QUFHUjtBQUNBMUIsZ0JBQU0sRUFBRTtBQUpBO0FBRlosT0FsQlk7QUFSRixLQUFaO0FBcUNEO0FBQ0Y7O0FBRUQsU0FBUzJCLGdCQUFULEdBQTRCO0FBQzFCLE1BQUlwRixFQUFFLEdBQUcsZ0JBQVQ7O0FBQ0EsTUFBSTlCLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUJRLEVBQXZCLENBQUosRUFBZ0M7QUFDOUIsUUFBSThFLFNBQVMsR0FBRzVHLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUJRLEVBQXZCLEVBQTJCc0IsZ0JBQTNCLENBQTRDLGtCQUE1QyxFQUFnRXlELE1BQWhGLENBRDhCLENBRTlCO0FBQ0E7O0FBQ0EsUUFBSS9GLE1BQU0sQ0FBQ1csVUFBUCxJQUFxQixJQUFyQixJQUE2Qm1GLFNBQVMsSUFBSSxDQUE5QyxFQUFpRDtBQUMvQzdHLE9BQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0I0RSxRQUFsQixDQUEyQixnQkFBM0IsRUFEK0MsQ0FFL0M7QUFDRDs7QUFDRCxRQUFJN0QsTUFBTSxDQUFDVyxVQUFQLElBQXFCLEdBQXJCLElBQTRCWCxNQUFNLENBQUNXLFVBQVAsR0FBb0IsSUFBaEQsSUFBd0RtRixTQUFTLEdBQUcsQ0FBeEUsRUFBMkU7QUFDekU3RyxPQUFDLENBQUMsY0FBRCxDQUFELENBQWtCNEUsUUFBbEIsQ0FBMkIsZ0JBQTNCLEVBRHlFLENBRXpFO0FBQ0QsS0FYNkIsQ0FZOUI7QUFDQTtBQUNBOztBQUNEO0FBQ0Y7O0FBRUQsU0FBU3dDLGtCQUFULEdBQThCO0FBQzVCLE1BQUlyRixFQUFFLEdBQUcsd0JBQVQ7O0FBQ0EsTUFBSTlCLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUJRLEVBQXZCLENBQUosRUFBZ0M7QUFDOUIvQixLQUFDLENBQUMrQixFQUFELENBQUQsQ0FBTXdELEtBQU4sQ0FBWTtBQUNWSixrQkFBWSxFQUFFLENBREo7QUFFVjZCLG9CQUFjLEVBQUUsQ0FGTjtBQUdWeEIsWUFBTSxFQUFFLElBSEU7QUFJVkMsZUFBUyxxR0FKQztBQUtWQyxlQUFTLHNHQUxDO0FBTVZnQixjQUFRLEVBQUUsYUFOQTtBQU9WTyxjQUFRLEVBQUUsS0FQQTtBQVFWM0IsZ0JBQVUsRUFBRSxDQUFDO0FBQ1hpQixrQkFBVSxFQUFFLEdBREQ7QUFFWEMsZ0JBQVEsRUFBRTtBQUNSckIsc0JBQVksRUFBRSxDQUROO0FBRVIrQix1QkFBYSxFQUFFO0FBRlA7QUFGQyxPQUFELEVBT1o7QUFDRVgsa0JBQVUsRUFBRSxHQURkO0FBRUVDLGdCQUFRLEVBQUU7QUFDUnJCLHNCQUFZLEVBQUUsQ0FETjtBQUVSK0IsdUJBQWEsRUFBRSxJQUZQO0FBR1IxQixnQkFBTSxFQUFFO0FBSEE7QUFGWixPQVBZO0FBUkYsS0FBWjtBQXlCRDtBQUNGOztBQUVELFNBQVM2Qix1QkFBVCxHQUFtQztBQUNqQyxNQUFJdEYsRUFBRSxHQUFHLHdCQUFUOztBQUNBLE1BQUk5QixRQUFRLENBQUNzQixhQUFULENBQXVCUSxFQUF2QixDQUFKLEVBQWdDO0FBQzlCLFFBQUloQixNQUFNLENBQUNXLFVBQVAsSUFBcUIsR0FBekIsRUFBOEI7QUFDNUIxQixPQUFDLENBQUMscUNBQUQsQ0FBRCxDQUF5QzRFLFFBQXpDLENBQWtELGdCQUFsRDtBQUNEO0FBQ0Y7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVMwQyxZQUFULEdBQXdCO0FBQ3RCLE1BQUlySCxRQUFRLENBQUNvRCxnQkFBVCxDQUEwQix3QkFBMUIsRUFBb0R5RCxNQUF4RCxFQUFnRTtBQUM5RCxRQUFJMUQsUUFBUSxHQUFHbkQsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEIsd0JBQTFCLENBQWY7QUFDQVksbUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25EQSxhQUFPLENBQUMxQyxnQkFBUixDQUF5QixXQUF6QixFQUFzQyxZQUFZO0FBQ2hEMEMsZUFBTyxDQUFDbkMsYUFBUixDQUFzQixRQUF0QixFQUFnQ0gsU0FBaEMsQ0FBMENFLEdBQTFDLENBQThDLGFBQTlDO0FBQ0QsT0FGRDtBQUdBb0MsYUFBTyxDQUFDMUMsZ0JBQVIsQ0FBeUIsVUFBekIsRUFBcUMsWUFBWTtBQUMvQzBDLGVBQU8sQ0FBQ25DLGFBQVIsQ0FBc0IsUUFBdEIsRUFBZ0NILFNBQWhDLENBQTBDQyxNQUExQyxDQUFpRCxhQUFqRDtBQUNELE9BRkQ7QUFHRCxLQVBEO0FBUUQ7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVNrRyxnQkFBVCxDQUEwQm5ELFVBQTFCLEVBQXNDO0FBQ3BDLE1BQUlWLE9BQU8sR0FBR3pELFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBZDtBQUNBLE1BQUlrQixNQUFNLEdBQUd4QyxRQUFRLENBQUNzQixhQUFULENBQXVCLE9BQXZCLENBQWI7QUFFQW1DLFNBQU8sQ0FBQzFDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUMsU0FBS0ksU0FBTCxDQUFlSSxNQUFmLENBQXNCLGVBQXRCO0FBQ0FpQixVQUFNLENBQUNyQixTQUFQLENBQWlCSSxNQUFqQixDQUF3QixlQUF4QjtBQUNBdkIsWUFBUSxDQUFDd0IsZUFBVCxDQUF5QkwsU0FBekIsQ0FBbUNJLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0QsR0FKRDtBQU1BVCxRQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsUUFBSUQsTUFBTSxDQUFDVyxVQUFQLElBQXFCMEMsVUFBekIsRUFBcUM7QUFDbkNWLGFBQU8sQ0FBQ3RDLFNBQVIsQ0FBa0JDLE1BQWxCLENBQXlCLGVBQXpCO0FBQ0FvQixZQUFNLENBQUNyQixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixlQUF4QjtBQUNBcEIsY0FBUSxDQUFDd0IsZUFBVCxDQUF5QkwsU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0Q7QUFDRixHQU5EO0FBT0QsQyxDQUNEOzs7QUFDQSxTQUFTbUcsbUJBQVQsQ0FBNkJwRCxVQUE3QixFQUF5QztBQUN2QyxNQUFJaEIsUUFBUSxHQUFHbkQsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEIscUJBQTFCLENBQWY7QUFDQSxNQUFJaUQsT0FBTyxHQUFHckcsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEIsMkJBQTFCLENBQWQ7QUFDQVksaUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFFBQUlBLE9BQU8sQ0FBQytELGtCQUFSLEtBQStCLElBQW5DLEVBQXlDO0FBQ3ZDL0QsYUFBTyxDQUFDMUMsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsVUFBVUMsQ0FBVixFQUFhO0FBQzdDQSxTQUFDLENBQUNXLGNBQUY7QUFDQSxZQUFJYSxNQUFNLEdBQUdpQixPQUFPLENBQUMrRCxrQkFBckI7O0FBQ0EsWUFBSTFHLE1BQU0sQ0FBQ1csVUFBUCxHQUFvQjBDLFVBQXhCLEVBQW9DO0FBQ2xDM0IsZ0JBQU0sQ0FBQ3JCLFNBQVAsQ0FBaUJJLE1BQWpCLENBQXdCLGtCQUF4QjtBQUNBa0MsaUJBQU8sQ0FBQ25DLGFBQVIsQ0FBc0IsR0FBdEIsRUFBMkJILFNBQTNCLENBQXFDSSxNQUFyQyxDQUE0QyxrQkFBNUMsRUFGa0MsQ0FHbEM7O0FBQ0F4QixXQUFDLENBQUN5QyxNQUFELENBQUQsQ0FBVWlGLE9BQVYsR0FBb0J6RSxRQUFwQixHQUErQlcsSUFBL0IsQ0FBb0MsdUJBQXBDLEVBQTZERSxXQUE3RCxDQUF5RSxrQkFBekU7QUFDRDtBQUNGLE9BVEQ7QUFVRDtBQUNGLEdBYkQ7QUFlQS9DLFFBQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBWTtBQUM1QyxRQUFJRCxNQUFNLENBQUNXLFVBQVAsSUFBcUIwQyxVQUF6QixFQUFxQztBQUNuQ0gscUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFlBQUlqQixNQUFNLEdBQUdpQixPQUFPLENBQUMrRCxrQkFBckI7QUFDQWhGLGNBQU0sQ0FBQ3JCLFNBQVAsQ0FBaUJDLE1BQWpCLENBQXdCLGtCQUF4QjtBQUNBcUMsZUFBTyxDQUFDbkMsYUFBUixDQUFzQixHQUF0QixFQUEyQkgsU0FBM0IsQ0FBcUNDLE1BQXJDLENBQTRDLGtCQUE1QztBQUNELE9BSkQ7QUFLRDtBQUNGLEdBUkQ7QUFTRDs7QUFFRCxTQUFTc0csbUJBQVQsR0FBK0I7QUFDN0IzSCxHQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QjRILEVBQXpCLENBQTRCLE9BQTVCLEVBQXFDLFlBQVk7QUFDL0M1SCxLQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRCxRQUFSLEdBQW1CVyxJQUFuQixDQUF3QixtQkFBeEIsRUFBNkNFLFdBQTdDLENBQXlELGtCQUF6RDtBQUNELEdBRkQ7QUFHRDs7QUFFRCxTQUFTK0Qsa0JBQVQsQ0FBNEJ6RCxVQUE1QixFQUF3QztBQUN0QyxNQUFJaEIsUUFBUSxHQUFHbkQsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEIscUJBQTFCLENBQWY7QUFDQVksaUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFFBQUlBLE9BQU8sQ0FBQ25DLGFBQVIsQ0FBc0IsbUJBQXRCLE1BQStDLElBQW5ELEVBQXlEO0FBQ3ZEbUMsYUFBTyxDQUFDMUMsZ0JBQVIsQ0FBeUIsV0FBekIsRUFBc0MsWUFBWTtBQUNoRDBDLGVBQU8sQ0FBQ25DLGFBQVIsQ0FBc0IscUJBQXRCLEVBQTZDSCxTQUE3QyxDQUF1REUsR0FBdkQsQ0FBMkQsVUFBM0Q7QUFDRCxPQUZEO0FBR0FvQyxhQUFPLENBQUMxQyxnQkFBUixDQUF5QixVQUF6QixFQUFxQyxZQUFZO0FBQy9DMEMsZUFBTyxDQUNKbkMsYUFESCxDQUNpQixxQkFEakIsRUFFR0gsU0FGSCxDQUVhQyxNQUZiLENBRW9CLFVBRnBCO0FBR0QsT0FKRDtBQUtEO0FBQ0YsR0FYRDtBQWFBTixRQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsUUFBSUQsTUFBTSxDQUFDVyxVQUFQLEdBQW9CMEMsVUFBeEIsRUFBb0M7QUFDbENkLFdBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FDR0MsSUFESCxDQUNRdkQsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEIscUJBQTFCLENBRFIsRUFFR0ksT0FGSCxDQUVXLFVBQVVxRSxJQUFWLEVBQWdCO0FBQ3ZCQSxZQUFJLENBQUMxRyxTQUFMLENBQWVDLE1BQWYsQ0FBc0IsVUFBdEI7QUFDRCxPQUpIO0FBS0Q7QUFDRixHQVJEO0FBU0Q7O0FBRUQsU0FBU1gsY0FBVCxHQUEwQjtBQUN4QixNQUFJNkYsVUFBVSxHQUFHLElBQWpCO0FBQ0FnQixrQkFBZ0IsQ0FBQ2hCLFVBQUQsQ0FBaEI7QUFDQWlCLHFCQUFtQixDQUFDakIsVUFBRCxDQUFuQjtBQUNBc0Isb0JBQWtCLENBQUN0QixVQUFELENBQWxCO0FBQ0Q7O0FBRUQsU0FBU0csUUFBVCxHQUFvQjtBQUNsQixNQUFJekcsUUFBUSxDQUFDc0IsYUFBVCxDQUF1QixlQUF2QixDQUFKLEVBQTZDO0FBQzNDLFFBQUl3RyxRQUFRLEdBQUdDLEtBQUssRUFBcEI7QUFDQUQsWUFBUSxDQUFDRSxPQUFUOztBQUNBLFFBQ0UsQ0FBQyxDQUFELEtBQU9DLFNBQVMsQ0FBQ0MsU0FBVixDQUFvQkMsT0FBcEIsQ0FBNEIsTUFBNUIsQ0FBUCxJQUNBRixTQUFTLENBQUNHLFVBQVYsQ0FBcUJELE9BQXJCLENBQTZCLFVBQTdCLElBQTJDLENBRjdDLEVBR0U7QUFDQSxVQUFJRSxNQUFNLEdBQUdySSxRQUFRLENBQUNzQixhQUFULENBQXVCLFFBQXZCLENBQWI7QUFDQStHLFlBQU0sQ0FBQ0MsWUFBUCxDQUFvQixLQUFwQixFQUEyQiwyQ0FBM0I7QUFDRDtBQUNGO0FBQ0YsQyxDQUNEOzs7QUFDQSxTQUFTQyxhQUFULENBQXVCQyxHQUF2QixFQUE0QjtBQUMxQixNQUFJMUcsRUFBRSxHQUFHOUIsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEJvRixHQUExQixDQUFUO0FBQ0EsTUFBSUMsVUFBVSxHQUFHLENBQUMsQ0FBbEI7QUFDQSxNQUFJQyxTQUFTLEdBQUcsQ0FBQyxDQUFqQjtBQUNBLE1BQUlwQyxVQUFVLEdBQUcsR0FBakIsQ0FKMEIsQ0FLMUI7O0FBQ0F0QyxpQkFBZSxDQUFDbEMsRUFBRCxDQUFmLENBQW9CMEIsT0FBcEIsQ0FBNEIsVUFBVXFFLElBQVYsRUFBZ0I7QUFDMUNBLFFBQUksQ0FBQzVCLEtBQUwsQ0FBV0MsTUFBWCxHQUFvQixFQUFwQixDQUQwQyxDQUNsQjs7QUFDeEJ1QyxjQUFVLEdBQUdaLElBQUksQ0FBQzdGLFlBQWxCLENBRjBDLENBRVY7O0FBQ2hDMEcsYUFBUyxHQUFHQSxTQUFTLEdBQUdELFVBQVosR0FBeUJDLFNBQXpCLEdBQXFDRCxVQUFqRDtBQUNELEdBSkQ7O0FBS0EsTUFBSXpJLFFBQVEsQ0FBQ2tCLElBQVQsQ0FBY3lILFdBQWQsR0FBNEJyQyxVQUFoQyxFQUE0QztBQUMxQ3RDLG1CQUFlLENBQUNsQyxFQUFELENBQWYsQ0FBb0IwQixPQUFwQixDQUE0QixVQUFVcUUsSUFBVixFQUFnQjtBQUMxQ0EsVUFBSSxDQUFDNUIsS0FBTCxDQUFXQyxNQUFYLGFBQXVCd0MsU0FBdkI7QUFDRCxLQUZEO0FBR0Q7QUFDRjs7QUFFRCxTQUFTRSxRQUFULENBQWtCQyxHQUFsQixFQUF1QkwsR0FBdkIsRUFBNEI7QUFDMUIsTUFBSTFHLEVBQUUsR0FBRzlCLFFBQVEsQ0FBQ29ELGdCQUFULENBQTBCb0YsR0FBMUIsQ0FBVCxDQUQwQixDQUUxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQXhFLGlCQUFlLENBQUNsQyxFQUFELENBQWYsQ0FBb0IwQixPQUFwQixDQUE0QixVQUFVcUUsSUFBVixFQUFnQjtBQUMxQyxRQUFJaUIsR0FBRyxHQUFHakIsSUFBSSxDQUFDa0IsU0FBZjs7QUFDQSxRQUFJRCxHQUFHLENBQUNqQyxNQUFKLEdBQWFnQyxHQUFqQixFQUFzQjtBQUNwQkcsZ0JBQVUsR0FBR0YsR0FBRyxDQUFDRyxTQUFKLENBQWMsQ0FBZCxFQUFpQkosR0FBRyxHQUFHLENBQXZCLElBQTRCLEtBQXpDO0FBQ0FoQixVQUFJLENBQUNxQixTQUFMLEdBQWlCRixVQUFqQjtBQUNELEtBSEQsTUFHTztBQUNMQSxnQkFBVSxHQUFHRixHQUFHLENBQUNHLFNBQUosQ0FBYyxDQUFkLEVBQWlCSixHQUFqQixJQUF3QixLQUFyQztBQUNBaEIsVUFBSSxDQUFDcUIsU0FBTCxHQUFpQkYsVUFBakI7QUFDRDtBQUNGLEdBVEQ7QUFVQVQsZUFBYSxDQUFDQyxHQUFELENBQWI7QUFDRDs7QUFFRCxTQUFTVyxhQUFULENBQXVCckgsRUFBdkIsRUFBMkJVLE1BQTNCLEVBQW1DO0FBQ2pDLE1BQUl4QyxRQUFRLENBQUNzQixhQUFULENBQXVCUSxFQUF2QixDQUFKLEVBQWdDO0FBQzlCLFFBQUkyQixPQUFPLEdBQUd6RCxRQUFRLENBQUNzQixhQUFULENBQXVCUSxFQUF2QixDQUFkO0FBQ0EsUUFBSXVFLE9BQU8sR0FBR3JHLFFBQVEsQ0FBQ29ELGdCQUFULENBQTBCWixNQUExQixDQUFkO0FBQ0FpQixXQUFPLENBQUMxQyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDMkMsV0FBSyxDQUFDL0IsY0FBTjtBQUNBOEIsYUFBTyxDQUFDMkYsSUFBUjtBQUNBL0YsV0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkI4QyxPQUEzQixFQUFvQzdDLE9BQXBDLENBQTRDLFVBQVVDLE9BQVYsRUFBbUI7QUFDN0RBLGVBQU8sQ0FBQzRGLE9BQVIsR0FBa0IsS0FBbEI7QUFDRCxPQUZEO0FBR0QsS0FORDtBQU9EO0FBQ0Y7O0FBRUQsU0FBU0MsbUJBQVQsR0FBK0I7QUFDN0IsTUFBSXRKLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsb0JBQXZCLE1BQWlELElBQXJELEVBQTJEO0FBQ3pEaUgsaUJBQWEsQ0FBQyxvQkFBRCxDQUFiO0FBQ0QsR0FINEIsQ0FJN0I7OztBQUNBLE1BQUl2SSxRQUFRLENBQUNzQixhQUFULENBQXVCLGtCQUF2QixNQUErQyxJQUFuRCxFQUF5RDtBQUN2RHNILFlBQVEsQ0FBQyxFQUFELEVBQUssa0JBQUwsQ0FBUjtBQUNEO0FBQ0Y7O0FBRUQsU0FBU1csUUFBVCxHQUFvQjtBQUNsQnhKLEdBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCeUosT0FBN0IsQ0FBcUM7QUFDbkNDLGFBQVMsRUFBRSxPQUR3QjtBQUVuQ2hHLFdBQU8sRUFBRSxPQUYwQjtBQUduQ2QsVUFBTSxFQUFFO0FBSDJCLEdBQXJDO0FBS0E1QyxHQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QjRILEVBQTdCLENBQWdDLHFCQUFoQyxFQUF1RCxZQUFZO0FBQ2pFLFFBQUkrQixZQUFZLEdBQUcsTUFBTTNKLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBDLElBQVIsQ0FBYSxrQkFBYixDQUF6QixDQURpRSxDQUVqRTtBQUNBOztBQUNBLFFBQUkxQyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVEwQyxJQUFSLENBQWEscUJBQWIsS0FBdUMsWUFBM0MsRUFBeUQ7QUFDdkQxQyxPQUFDLENBQUMySixZQUFELENBQUQsQ0FBZ0IvRixJQUFoQixDQUFxQixnQkFBckIsRUFBdUNuRCxJQUF2QyxDQUE0QyxnQkFBNUM7QUFDRDs7QUFDRCxRQUFJVCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVEwQyxJQUFSLENBQWEscUJBQWIsS0FBdUMsZ0JBQTNDLEVBQTZEO0FBQzNEMUMsT0FBQyxDQUFDMkosWUFBRCxDQUFELENBQWdCL0YsSUFBaEIsQ0FBcUIsZ0JBQXJCLEVBQXVDbkQsSUFBdkMsQ0FBNEMsb0JBQTVDO0FBQ0Q7QUFDRixHQVZEO0FBV0Q7O0FBRUQsU0FBU21KLEdBQVQsR0FBZTtBQUNiLE1BQUkzSixRQUFRLENBQUNzQixhQUFULENBQXVCLFlBQXZCLENBQUosRUFBMEM7QUFDeENzSSxPQUFHLENBQUMxRyxJQUFKLENBQVM7QUFDUDJHLFVBQUksRUFBRTtBQURDLEtBQVQ7QUFHRDtBQUNGOztBQUVELFNBQVNDLFNBQVQsR0FBcUI7QUFDbkIsTUFBSTlKLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBSixFQUEwQztBQUN4QyxRQUFJNkIsUUFBUSxHQUFHbkQsUUFBUSxDQUFDb0QsZ0JBQVQsQ0FBMEIsWUFBMUIsQ0FBZjtBQUNBLFFBQUlaLE1BQU0sR0FBR3hDLFFBQVEsQ0FBQ3NCLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYjtBQUVBK0IsU0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkJKLFFBQTNCLEVBQXFDSyxPQUFyQyxDQUE2QyxVQUFVQyxPQUFWLEVBQW1CO0FBQzlEQSxhQUFPLENBQUMvQixPQUFSLEdBQWtCLFlBQVk7QUFDNUJjLGNBQU0sQ0FBQzhGLFlBQVAsQ0FBb0IsS0FBcEIsRUFBMkI3RSxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsVUFBckIsQ0FBM0I7QUFDQU4sZUFBTyxDQUFDdEMsU0FBUixDQUFrQkUsR0FBbEIsQ0FBc0IsV0FBdEI7QUFFQWdDLGFBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FDR0MsSUFESCxDQUNRSixRQURSLEVBRUc0RyxNQUZILENBRVUsVUFBVWxDLElBQVYsRUFBZ0I7QUFDdEIsaUJBQU9BLElBQUksS0FBS3BFLE9BQWhCO0FBQ0QsU0FKSCxFQUtHRCxPQUxILENBS1csVUFBVXFFLElBQVYsRUFBZ0I7QUFDdkJBLGNBQUksQ0FBQzFHLFNBQUwsQ0FBZUMsTUFBZixDQUFzQixXQUF0QjtBQUNELFNBUEg7QUFRRCxPQVpEO0FBYUQsS0FkRDtBQWVEO0FBQ0Y7O0FBRUQsU0FBUzRJLGtCQUFULEdBQThCO0FBQzVCLE1BQUlsSSxFQUFFLEdBQUcvQixDQUFDLENBQUMsd0JBQUQsQ0FBVjs7QUFDQSxNQUFJK0IsRUFBSixFQUFRO0FBQ04sU0FBSyxJQUFJbUksQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR25JLEVBQUUsQ0FBQytFLE1BQXZCLEVBQStCb0QsQ0FBQyxFQUFoQyxFQUFvQztBQUNsQyxVQUFJQyxnQkFBZ0IsR0FBR3BJLEVBQUUsQ0FBQ3FJLEVBQUgsQ0FBTUYsQ0FBTixFQUFTRyxRQUFULENBQWtCLEtBQWxCLEVBQXlCdkQsTUFBaEQsQ0FEa0MsQ0FFbEM7O0FBQ0EsVUFBSXFELGdCQUFnQixJQUFJLENBQXhCLEVBQTJCO0FBQ3pCcEksVUFBRSxDQUFDcUksRUFBSCxDQUFNRixDQUFOLEVBQVN0RixRQUFULENBQWtCLHdCQUFsQjtBQUNELE9BRkQsTUFFTztBQUNMN0MsVUFBRSxDQUFDcUksRUFBSCxDQUFNRixDQUFOLEVBQVNwRyxXQUFULENBQXFCLHdCQUFyQjtBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUVELFNBQVN3Ryx1QkFBVCxDQUFpQ0MsY0FBakMsRUFBaURDLFFBQWpELEVBQTJEO0FBQ3pELE1BQUl6SSxFQUFFLEdBQUc5QixRQUFRLENBQUNzQixhQUFULENBQXVCZ0osY0FBdkIsQ0FBVDtBQUNBLE1BQUk5SCxNQUFNLEdBQUd4QyxRQUFRLENBQUNzQixhQUFULENBQXVCaUosUUFBdkIsQ0FBYjs7QUFDQSxNQUFJekksRUFBSixFQUFRO0FBQ047QUFDQSxRQUFJcUIsUUFBUSxHQUFHckIsRUFBRSxDQUFDc0IsZ0JBQUgsQ0FBb0Isd0JBQXBCLENBQWYsQ0FGTSxDQUdOOztBQUNBQyxTQUFLLENBQUNKLFNBQU4sQ0FBZ0JLLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQkosUUFBM0IsRUFBcUNLLE9BQXJDLENBQTZDLFVBQVVDLE9BQVYsRUFBbUI7QUFDOURBLGFBQU8sQ0FBQzFDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUMsWUFBSXlKLFVBQVUsR0FBRzFJLEVBQUUsQ0FBQ3NCLGdCQUFILENBQW9CLDhCQUFwQixFQUFvRHlELE1BQXJFLENBRDRDLENBRTVDOztBQUNBLFlBQUkyRCxVQUFVLEdBQUcsQ0FBakIsRUFBb0I7QUFDbEJoSSxnQkFBTSxDQUFDckIsU0FBUCxDQUFpQkUsR0FBakIsQ0FBcUIsaUJBQXJCO0FBQ0QsU0FGRCxNQUVPO0FBQ0xtQixnQkFBTSxDQUFDckIsU0FBUCxDQUFpQkMsTUFBakIsQ0FBd0IsaUJBQXhCO0FBQ0Q7QUFDRixPQVJEO0FBU0QsS0FWRDtBQVdBLFFBQUlxSixhQUFhLEdBQUd6SyxRQUFRLENBQUNzQixhQUFULENBQXVCLHFCQUF2QixDQUFwQjtBQUNBbUosaUJBQWEsQ0FBQzFKLGdCQUFkLENBQStCLE9BQS9CLEVBQXdDLFlBQVk7QUFDbER5QixZQUFNLENBQUNyQixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixpQkFBeEI7QUFDRCxLQUZEO0FBR0Q7QUFDRjs7QUFFRCxTQUFTc0osY0FBVCxHQUEwQjtBQUN4QixNQUFJM0ssQ0FBQyxDQUFDLG1CQUFELENBQUwsRUFBNEI7QUFDMUJBLEtBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCNEssVUFBdkIsR0FBb0NDLElBQXBDLENBQXlDO0FBQ3ZDQyxXQUFLLEVBQUU5SyxDQUFDLENBQUM0SyxVQUFGLENBQWFHLFFBQWIsQ0FBc0JELEtBRFU7QUFDSDtBQUNwQ0UsVUFBSSxFQUFFLE9BRmlDO0FBRXhCO0FBQ2ZDLGFBQU8sRUFBRSxPQUg4QjtBQUdyQjtBQUNsQkMsVUFBSSxFQUFFLElBSmlDLENBSTVCOztBQUo0QixLQUF6QztBQU1EO0FBRUYsQyxDQUNEOzs7QUFDQSxTQUFTQyxTQUFULEdBQXFCO0FBQ25CLE1BQUlwSixFQUFFLEdBQUc5QixRQUFRLENBQUNzQixhQUFULENBQXVCLGVBQXZCLENBQVQ7O0FBQ0EsTUFBSVEsRUFBSixFQUFRO0FBQ05BLE1BQUUsQ0FBQ2YsZ0JBQUgsQ0FBb0IsUUFBcEIsRUFBOEIsWUFBWTtBQUN4Q0QsWUFBTSxDQUFDcUssSUFBUCxDQUFZLEtBQUtDLE9BQUwsQ0FBYSxLQUFLQyxhQUFsQixFQUFpQ0MsS0FBN0M7QUFDRCxLQUZEO0FBR0Q7QUFDRixDLENBQ0Q7OztBQUNBdkwsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzVCaUwsV0FBUztBQUNUckssZUFBYTtBQUNidUYsY0FBWTtBQUNaTyxhQUFXO0FBQ1hPLGtCQUFnQixHQUxZLENBTTVCOztBQUNBQyxvQkFBa0I7QUFDbEJDLHlCQUF1QjtBQUN2QlosYUFBVztBQUNYYSxjQUFZO0FBQ1paLFVBQVE7QUFDUmxDLGlCQUFlLEdBWmEsQ0FhNUI7O0FBQ0FvRixLQUFHO0FBQ0hHLFdBQVM7QUFDVGpGLHNCQUFvQjtBQUNwQnNFLGVBQWEsQ0FBQyxxQkFBRCxFQUF3Qix3QkFBeEIsQ0FBYjtBQUNBekQsNkJBQTJCLEdBbEJDLENBbUI1Qjs7QUFDQTZELFVBQVE7QUFDUlMsb0JBQWtCO0FBQ2xCSyx5QkFBdUIsQ0FBQyxrQkFBRCxFQUFxQixpQkFBckIsQ0FBdkI7QUFDQUEseUJBQXVCLENBQUMsb0JBQUQsRUFBdUIsd0JBQXZCLENBQXZCO0FBQ0FsRSxnQ0FBOEI7QUFDOUJ1RSxnQkFBYztBQUNkcEksWUFBVSxHQTFCa0IsQ0EyQjVCO0FBQ0QsQ0E1QkQ7O0FBNkJBeEIsTUFBTSxDQUFDeUssTUFBUCxHQUFnQixZQUFZO0FBQzFCakMscUJBQW1CO0FBQ25CN0UsWUFBVTtBQUNYLENBSEQsQyxDQUlBOzs7QUFDQTFFLENBQUMsQ0FBQ2UsTUFBRCxDQUFELENBQVUwSyxNQUFWLENBQWlCLFlBQVk7QUFDM0JsQyxxQkFBbUI7QUFDbkI3RSxZQUFVLEdBRmlCLENBRzNCOztBQUNBTSwyQkFBeUIsQ0FBQyxvQkFBRCxFQUF1QixHQUF2QixDQUF6QjtBQUNBbUMsa0JBQWdCO0FBQ2pCLENBTkQsRSxDQU9BOztBQUNBbkgsQ0FBQyxDQUFDZSxNQUFELENBQUQsQ0FBVTJLLE1BQVYsQ0FBaUIsWUFBWTtBQUMzQjdLLGFBQVcsR0FEZ0IsQ0FFM0I7QUFDRCxDQUhELEUiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjQDQuMC9hc3NldHMvanMvbWFpbi5qc1wiKTtcbiIsIi8vIGFqYXgg6YWN5ZCISlEzIOW8leWFpeioree9rlxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICAkLmFqYXgoe1xuICAgIHVybDogXCJhamF4L19oZWFkZXIuaHRtbFwiLFxuICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICBkYXRhVHlwZTogXCJodG1sXCIsXG4gIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAkKFwiI2hlYWRlclwiKS5odG1sKGRhdGEpO1xuICAgIGhlYWRlckZ1bmN0aW9uKCk7XG4gICAgLy8gc2liaW5nTW9iaWxlU3VibWVudSgpO1xuICB9KTtcbiAgJC5hamF4KHtcbiAgICB1cmw6IFwiYWpheC9fc2lkZUxpbmsuaHRtbFwiLFxuICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICBkYXRhVHlwZTogXCJodG1sXCIsXG4gIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAkKFwiI3NpZGVMaW5rXCIpLmh0bWwoZGF0YSk7XG4gICAgLy8gZ29Ub3AoKTtcbiAgICB0b2dnbGVTaWRlTGluaygpO1xuICB9KTtcbiAgJC5hamF4KHtcbiAgICB1cmw6IFwiYWpheC9fZm9vdGVyLmh0bWxcIixcbiAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxuICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgJChcIiNmb290ZXJcIikuaHRtbChkYXRhKTtcbiAgICBnb1RvcCgpO1xuICAgIGdvVG9wU3RpY2t5KCk7XG4gICAgLy8gdG9nZ2xlU2lkZUxpbmsoKTtcbiAgfSk7XG4gIC8vICQoXCIjZm9vdGVyXCIpLmxvYWQoXCJhamF4L19mb290ZXIuaHRtbFwiKTtcbn0pO1xuLy/oi7HmlofniYjnmoRhamF4XG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gICQuYWpheCh7XG4gICAgdXJsOiBcIi4uL2FqYXgvX2hlYWRlcl9lbi5odG1sXCIsXG4gICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgIGRhdGFUeXBlOiBcImh0bWxcIixcbiAgfSkuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuICAgICQoXCIjaGVhZGVyRW5cIikuaHRtbChkYXRhKTtcbiAgICBoZWFkZXJGdW5jdGlvbigpO1xuICAgIC8vIHNpYmluZ01vYmlsZVN1Ym1lbnUoKTtcbiAgfSk7XG4gICQuYWpheCh7XG4gICAgdXJsOiBcIi4uL2FqYXgvX3NpZGVMaW5rX2VuLmh0bWxcIixcbiAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxuICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgJChcIiNzaWRlTGlua0VuXCIpLmh0bWwoZGF0YSk7XG4gICAgLy8gZ29Ub3AoKTtcbiAgICB0b2dnbGVTaWRlTGluaygpO1xuICB9KTtcbiAgJC5hamF4KHtcbiAgICB1cmw6IFwiLi4vYWpheC9fZm9vdGVyX2VuLmh0bWxcIixcbiAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxuICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgJChcIiNmb290ZXJFblwiKS5odG1sKGRhdGEpO1xuICAgIGdvVG9wKCk7XG4gICAgZ29Ub3BTdGlja3koKTtcbiAgICAvLyB0b2dnbGVTaWRlTGluaygpO1xuICB9KTtcbiAgLy8gJChcIiNmb290ZXJcIikubG9hZChcImFqYXgvX2Zvb3Rlci5odG1sXCIpO1xufSk7XG5mdW5jdGlvbiB0b29sc0xpc3RlbmVyKCkge1xuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgZnVuY3Rpb24gKGUpIHtcbiAgICBpZiAoZS5rZXlDb2RlID09PSA5KSB7XG4gICAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy11c2VNb3VzZVwiKTtcbiAgICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmFkZChcImpzLXVzZUtleWJvYXJkXCIpO1xuICAgIH1cbiAgfSk7XG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKFwianMtdXNlS2V5Ym9hcmRcIik7XG4gICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuYWRkKFwianMtdXNlTW91c2VcIik7XG4gIH0pO1xufVxuXG5mdW5jdGlvbiB0b2dnbGVTaWRlTGluaygpIHtcbiAgZG9jdW1lbnRcbiAgICAucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua1RvZ2dsZUJ0blwiKVxuICAgIC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy5jbGFzc0xpc3QudG9nZ2xlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcGFnZVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiYm9keVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwib3ZlcmZsb3ctaGlkZGVuXCIpO1xuICAgICAgLy/miZPplotzaWRlTWVudeaZgu+8jOaKiuWxlemWi+eahG1lbnXpl5zmjolcbiAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubC1oZWFkZXItaGFtYnVyZ2VyXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5sLWhlYWRlci1tZW51XCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xuICAgICAgLy/miZPplotzaWRlTWVudeaZgu+8jOaKimdvdG9w5o6o5Yiw5peB6YKK5Y67XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rRm9vdGVyXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgICB9KTtcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgZnVuY3Rpb24gKCkge1xuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+PSAxMjAwKSB7XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rVG9nZ2xlQnRuXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNwYWdlXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJib2R5XCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJvdmVyZmxvdy1oaWRkZW5cIik7XG4gICAgICAvL1vnp7vpmaRd5omT6ZaLc2lkZU1lbnXmmYLvvIzmiopnb3RvcOaOqOWIsOaXgemCiuWOu1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua0Zvb3RlclwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XG4gICAgfVxuICB9KTtcbn1cblxuZnVuY3Rpb24gZ29Ub3AoKSB7XG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjZ29Ub3BcIikub25jbGljayA9IGZ1bmN0aW9uIChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICQoXCJodG1sLCBib2R5XCIpLmFuaW1hdGUoe1xuICAgICAgc2Nyb2xsVG9wOiAwLFxuICAgIH0sXG4gICAgICA1MDBcbiAgICApO1xuICB9O1xufVxuXG5mdW5jdGlvbiBnb1RvcFN0aWNreSgpIHtcbiAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua0Zvb3RlclwiKTtcbiAgLy/oqIjnrpdmb290ZXLnm67liY3pq5jluqZcbiAgdmFyIGZvb3RlckhlaWdodFZhciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubC1mb290ZXJcIikuY2xpZW50SGVpZ2h0O1xuICAvLyBjb25zb2xlLmxvZyhgZm9vdGVySGVpZ2h0VmFyICsgJHtmb290ZXJIZWlnaHRWYXJ9YCk7XG4gIC8v55W25LiL57ay6aCB6auY5bqmKOaVtOmggemVtylcbiAgdmFyIHNjcm9sbEhlaWdodFZhciA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxIZWlnaHQ7XG4gIC8vIGNvbnNvbGUubG9nKGBzY3JvbGxIZWlnaHRWYXIgKyAke3Njcm9sbEhlaWdodFZhcn1gKTtcbiAgLy/nlbbkuIvnlavpnaLpq5jluqYod2luZG93IGhlaWdodClcbiAgdmFyIHdpbmRvd0hlaWdodFZhciA9IHdpbmRvdy5pbm5lckhlaWdodDtcbiAgLy8gY29uc29sZS5sb2coYHdpbmRvd0hlaWdodFZhciArICR7d2luZG93SGVpZ2h0VmFyfWApO1xuICAvL+aNsuWLleS4reeahOeVq+mdouacgOS4iuerr+iIh+e2sumggemgguerr+S5i+mWk+eahOi3nembolxuICB2YXIgc2Nyb2xsVG9wVmFyID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDtcbiAgLy8gY29uc29sZS5sb2coYHNjcm9sbFRvcFZhciArICR7c2Nyb2xsVG9wVmFyfWApO1xuICAvLyBjb25zb2xlLmxvZyh3aW5kb3dIZWlnaHRWYXIgKyBzY3JvbGxUb3BWYXIgKyBmb290ZXJIZWlnaHRWYXIpO1xuICAvL+WmguaenOaVtOmggemVtz0955Wr6Z2i6auY5bqmK+aNsuWLlemrmOW6plxuICBpZiAoc2Nyb2xsSGVpZ2h0VmFyIDw9IHdpbmRvd0hlaWdodFZhciArIHNjcm9sbFRvcFZhciArIGZvb3RlckhlaWdodFZhcikge1xuICAgIGVsLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdGlja3lcIik7XG4gIH0gZWxzZSB7XG4gICAgZWwuY2xhc3NMaXN0LmFkZChcImpzLXN0aWNreVwiKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBnb1RvQW5jaG9yKCkge1xuICAkKCcuanMtZ29Ub0FuY2hvcicpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIHZhciB0YXJnZXQgPSAkKHRoaXMpLmF0dHIoJ2hyZWYnKTtcbiAgICB2YXIgdGFyZ2V0UG9zID0gJCh0YXJnZXQpLm9mZnNldCgpLnRvcDtcbiAgICAkKCdodG1sLGJvZHknKS5hbmltYXRlKHtcbiAgICAgIHNjcm9sbFRvcDogdGFyZ2V0UG9zIC0gNTBcbiAgICB9LCAxMDAwKTtcbiAgICB2YXIgdHJpZ2dlcjAyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNoYW1idXJnZXJcIik7XG4gICAgdmFyIHRhcmdldDAyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNtZW51XCIpO1xuICAgIHRyaWdnZXIwMi5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcbiAgICB0YXJnZXQwMi5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcbiAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XG4gIH0pO1xufVxuLy/poqjnkLTnlKhmdW5jdGlvblxuZnVuY3Rpb24gQWNjb3JkaW9uKGVsLCB0YXJnZXQsIHNpYmxpbmdzKSB7XG4gIHRoaXMuZWwgPSBlbDtcbiAgdGhpcy50YXJnZXQgPSB0YXJnZXQ7XG4gIHRoaXMuc2libGluZ3MgPSBzaWJsaW5ncztcbn1cbkFjY29yZGlvbi5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLmVsKTtcbiAgdmFyIHRhcmdldCA9IHRoaXMudGFyZ2V0O1xuICB2YXIgc2libGluZ3MgPSB0aGlzLnNpYmxpbmdzO1xuICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBpZiAoc2libGluZ3MgPT0gdHJ1ZSkge1xuICAgICAgICBpZiAodGhpcy5xdWVyeVNlbGVjdG9yKHRhcmdldCkgIT09IG51bGwpIHtcbiAgICAgICAgICAvLyB0aGlzLnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KS5jbGFzc0xpc3QudG9nZ2xlKFxuICAgICAgICAgIC8vICAgXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiXG4gICAgICAgICAgLy8gKTtcbiAgICAgICAgICAvL+eCuuS6huimgeiDveWcqOaJk+mWi+S4gOWAi+aZgu+8jOWFtuS7lueahOaUtuWQiO+8jOmAmemCiuaUueeUqGpRdWVyeT09PlxuICAgICAgICAgICQodGhpcykuZmluZCh0YXJnZXQpLnRvZ2dsZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XG4gICAgICAgICAgLy8g5aKe5Yqg6JeN57ea5qGG5pWI5p6cXG4gICAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcyhcImMtYWNjb3JkaW9uLWJ0bi1mb2N1c1wiKTtcbiAgICAgICAgICAkKHRoaXMpLnNpYmxpbmdzKCkuZmluZCh0YXJnZXQpLnJlbW92ZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XG4gICAgICAgICAgLy8g5aKe5Yqg6JeN57ea5qGG5pWI5p6cXG4gICAgICAgICAgJCh0aGlzKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwiYy1hY2NvcmRpb24tYnRuLWZvY3VzXCIpO1xuICAgICAgICB9XG4gICAgICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodHJpZ2dlci5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKSkuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xuICAgICAgICAvL+eCuuS6huimgeiDveWcqOaJk+mWi+S4gOWAi+aZgu+8jOWFtuS7lueahOaUtuWQiO+8jOmAmemCiuaUueeUqGpRdWVyeT09PlxuICAgICAgICB2YXIgYXR0ID0gJCh0cmlnZ2VyKS5hdHRyKFwiZGF0YS10YXJnZXRcIik7XG4gICAgICAgICQoYXR0KS50b2dnbGVDbGFzcyhcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcbiAgICAgICAgLy8gJChgJHt0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdGFyZ2V0XCIpfWApLnRvZ2dsZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIikuc2libGluZ3MoKS5yZW1vdmVDbGFzcyhcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xuICAgICAgICAvL+eUqOS6hmpRdWVyeeS5i+W+jO+8jOmAmee1hOacg+iiq+aok+S4iumCo+WAi+e2geWcqOS4gOi1t++8jOaJgOS7peWPr+S7peecgeeVpVxuICAgICAgICAvLyB0cmlnZ2VyLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICh0aGlzLnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KSAhPT0gbnVsbCkge1xuICAgICAgICAgIHRoaXMucXVlcnlTZWxlY3Rvcih0YXJnZXQpLmNsYXNzTGlzdC50b2dnbGUoXG4gICAgICAgICAgICBcImpzLWFjY29yZGlvbkV4cGVuZGVkXCJcbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodHJpZ2dlci5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKSkuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xuICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIG5vZGVMaXN0VG9BcnJheShub2RlTGlzdENvbGxlY3Rpb24pIHtcbiAgcmV0dXJuIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKG5vZGVMaXN0Q29sbGVjdGlvbik7XG59XG4vLyBjb25zdCBub2RlTGlzdFRvQXJyYXkgPSAobm9kZUxpc3RDb2xsZWN0aW9uKSA9PiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChub2RlTGlzdENvbGxlY3Rpb24pO1xuLyoqXG4gKiBcbiAqIEBwYXJhbSB7Kn0gZWwgOmNsYXNzIG5hbWVcbiAqIEBwYXJhbSB7Kn0gdGFyZ2V0IDrooqvlvbHpn7/liLDnmoTnm67mqJlcbiAqIEBwYXJhbSB7Kn0gbWVkaWFRdWVyeSA65pa36bue6Kit5a6aXG4gKiBA6Kqq5piOIFwiZWxcIuiIh1widGFyZ2V0XCIgdG9nZ2xlIOS4gOWAi+WPq2pzLWFjdGl2ZeeahGNsYXNz6KaB55So5Ye65LuA6bq85pWI5p6c77yM56uv55yL5L2g5oCO6bq85a+ranMtYWN0aXZl55qEY3Nz5pWI5p6c5bGV6ZaL5oiW5pS25ZCI5LuA6bq85p2x6KW/XG4gKi9cblxuZnVuY3Rpb24gdG9nZ2xlVmlzaWFibGUoZWwsIHRhcmdldCwgbWVkaWFRdWVyeSkge1xuICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGVsKTtcbiAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KTtcbiAgaWYgKHRhcmdldCkge1xuICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB0aGlzLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY3RpdmVcIik7XG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QudG9nZ2xlKFwianMtYWN0aXZlXCIpO1xuICAgICAgICB2YXIgaGFzTWVkaWFRdWVyeSA9IG1lZGlhUXVlcnk7XG4gICAgICAgIGlmIChoYXNNZWRpYVF1ZXJ5ICE9PSBcIlwiKSB7XG4gICAgICAgICAgdmFyIGlzTW9iaWxlID0gd2luZG93LmlubmVyV2lkdGggPCBtZWRpYVF1ZXJ5O1xuICAgICAgICAgIGlmIChpc01vYmlsZSkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC50b2dnbGUoXCJqcy1mdW5jdGlvbk1lbnVPcGVuZWRcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwianMtZnVuY3Rpb25NZW51T3BlbmVkXCIpO1xuICAgICAgICB9XG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gbWVkaWFRdWVyeSkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1mdW5jdGlvbk1lbnVPcGVuZWRcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59XG4vKmVsPeinuOeZvOWwjeixoSjplovpl5wpKi9cbi8qdGFyZ2V0Peiiq+aOp+WItueahOeJqeS7tijnh4gpKi9cbmZ1bmN0aW9uIGNsaWNrQ29uZmlybShlbCwgdGFyZ2V0KSB7XG4gIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoZWwpO1xuICB2YXIgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0YXJnZXQpO1xuICBpZiAodGFyZ2V0KSB7XG4gICAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XG4gICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWN0aXZlXCIpO1xuICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWZ1bmN0aW9uTWVudU9wZW5lZFwiKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59XG4vL+KGkeKGkeKGkemAmueUqGZ1bmN0aW9uLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5mdW5jdGlvbiBjcmVhdGVBY2NvcmRpb24oKSB7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1hY2NvcmRpb24tYnRuJykpIHtcbiAgICBuZXcgQWNjb3JkaW9uKFwiLmMtYWNjb3JkaW9uLWJ0blwiLCBcIi5jLWFjY29yZGlvbi1idG4taWNvblwiLCB0cnVlKS5pbml0KCk7XG4gIH1cbn1cbi8vIFRPRE86IOaDs+i+puazlemAmeWAi+WFqOWfn+iuiuaVuOimgeiZleeQhuS4gOS4i1xudmFyIHN0ciA9IDA7XG5cbmZ1bmN0aW9uIHNob3dBbGxUYWIoKSB7XG4gIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDk5MiAmJiBzdHIgPT0gMSkge1xuICAgICQoXCIudGFiLXBhbmVcIikucmVtb3ZlQ2xhc3MoXCJzaG93IGFjdGl2ZVwiKS5maXJzdCgpLmFkZENsYXNzKFwic2hvdyBhY3RpdmVcIik7XG4gICAgJChcIi5jLXRhYi1saW5rRVwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKS5wYXJlbnQoKS5maXJzdCgpLmZpbmQoXCIuYy10YWItbGlua0VcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgc3RyID0gMDtcbiAgfVxuICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5OTIgJiYgc3RyID09IDApIHtcbiAgICAkKFwiLnRhYi1wYW5lXCIpLmFkZENsYXNzKFwic2hvdyBhY3RpdmVcIik7XG4gICAgc3RyID0gMTtcbiAgfVxuICAvLyBjb25zb2xlLmxvZyhzdHIpO1xufVxuXG5mdW5jdGlvbiB0b2dnbGVwRmlsdGVyU3VibWVudSgpIHtcbiAgLy8gVE9ETzog5pyJ56m66Kmm6Kmm55yL5a+r5LiA5YCL5omL5qmf54mI5Y+v5Lul6Yed5bCN5YWn5a656auY5bqm5aKe5Yqgc2hvd21vcmXvvIzkuKbkuJToqJjpjITmraRzaG93bW9yZeW3sue2k+m7numWi+mBju+8jOS4jeacg+WboOeCunJ3ZOWwseWPiOioiOeul+S4gOasoVxuICAvL+aJi+apn+eJiOmhr+ekuuWFqOmDqFxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnAtcHJvZHVjdHMtc2VhcmNoLXNob3dNb3JlJykpIHtcbiAgICBuZXcgQWNjb3JkaW9uKFwiLnAtcHJvZHVjdHMtc2VhcmNoLXNob3dNb3JlXCIsIHVuZGVmaW5lZCwgZmFsc2UpLmluaXQoKTtcbiAgfVxuICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmMtYWNjb3JkaW9uLWJ0blwiKTtcbiAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XG4gICAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdGFyZ2V0XCIpKTtcbiAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoZWwucXVlcnlTZWxlY3RvcihcIi5wLXByb2R1Y3RzLXNlYXJjaC1zaG93TW9yZVwiKSkge1xuICAgICAgICBlbC5xdWVyeVNlbGVjdG9yKFwiLnAtcHJvZHVjdHMtc2VhcmNoLXNob3dNb3JlXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcbiAgICAgICAgZWwucXVlcnlTZWxlY3RvcihcIi5jLWFjY29yZGlvbi1jb250ZW50LWxheW91dFwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xuICAvL+aJi+apn+eJiOmWi+mXnOmBuOWWrlxuICAvL+WVhuWTgeWwiOWNgFxuICB0b2dnbGVWaXNpYWJsZShcIi5jbG9zZVwiLCBcIi5wLXByb2R1Y3RzLXNlYXJjaFwiLCBcIlwiKTtcbiAgdG9nZ2xlVmlzaWFibGUoXCIucC1wcm9kdWN0cy1zZWFyY2gtYnRuXCIsIFwiLnAtcHJvZHVjdHMtc2VhcmNoXCIsIDk5Mik7XG4gIGNsaWNrQ29uZmlybShcIiNqcy1jb25maXJtXCIsIFwiLnAtcHJvZHVjdHMtc2VhcmNoXCIpO1xuICAvL+mWgOW4guafpeipolxuICB0b2dnbGVWaXNpYWJsZShcIi52LWRyb3Bkb3duLWJ0blwiLCBcIi52LWRyb3Bkb3duLW1lbnVcIiwgOTkyKTtcbiAgdG9nZ2xlVmlzaWFibGUoXCIuY2xvc2VcIiwgXCIudi1kcm9wZG93bi1tZW51XCIsIFwiXCIpO1xuICBjbGlja0NvbmZpcm0oXCIjanMtY29uZmlybVwiLCBcIi52LWRyb3Bkb3duLW1lbnVcIik7XG4gIC8vIGlmICgkKFwiLnYtZHJvcGRvd24tYnRuXCIpLmhhc0NsYXNzKFwianMtYWN0aXZlXCIpKSB7XG4gIC8vICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImJvZHlcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgLy8gICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudi1kcm9wZG93bi1idG5cIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLWFjdGl2ZVwiKTtcbiAgLy8gICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudi1kcm9wZG93bi1tZW51XCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY3RpdmVcIik7XG4gIC8vICAgfSk7XG4gIC8vIH1cblxufVxuXG5mdW5jdGlvbiBjb25maXJtSWZEb3VibGVNZW51T3BlbmVkKGVsLCBtZWRpYVF1ZXJ5KSB7XG4gIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IG1lZGlhUXVlcnkgJiYgJChlbCkuaGFzQ2xhc3MoXCJqcy1hY3RpdmVcIikgJiYgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdCA9PSBcIlwiKSB7XG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJqcy1tZW51T3BlbmVkXCIpO1xuICB9XG59XG4vLyDovKrmkq3nm7jpl5wtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuZnVuY3Rpb24gU2xpY2soZWwsIHNsaWRlc1RvU2hvdywgc2xpZGVzUGVyUm93LCByb3dzLCByZXNwb25zaXZlKSB7XG4gIHRoaXMuZWwgPSBlbDtcbiAgdGhpcy5zbGlkZXNUb1Nob3cgPSBzbGlkZXNUb1Nob3c7XG4gIHRoaXMuc2xpZGVzUGVyUm93ID0gc2xpZGVzUGVyUm93O1xuICB0aGlzLnJvd3MgPSByb3dzO1xuICB0aGlzLnJlc3BvbnNpdmUgPSByZXNwb25zaXZlO1xufTtcblxuU2xpY2sucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICQodGhpcy5lbCArIFwiIC52LXNsaWNrXCIpLnNsaWNrKHtcbiAgICBhcnJvd3M6IHRydWUsXG4gICAgc2xpZGVzVG9TaG93OiB0aGlzLnNsaWRlc1RvU2hvdyxcbiAgICBzbGlkZXNQZXJSb3c6IHRoaXMuc2xpZGVzUGVyUm93LFxuICAgIHJvd3M6IHRoaXMucm93cyxcbiAgICBwcmV2QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtc2xpY2stYXJyb3dzLXByZXZcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1sZWZ0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgbmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXNsaWNrLWFycm93cy1uZXh0XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtcmlnaHRcIj48L2k+PC9idXR0b24+YCxcbiAgICAvLyBhdXRvcGxheTogdHJ1ZSxcbiAgICAvLyBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxuICAgIHJlc3BvbnNpdmU6IHRoaXMucmVzcG9uc2l2ZSxcbiAgfSk7XG59O1xuXG5mdW5jdGlvbiBzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKSB7XG4gIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmMtc2hvcFRhYmxlLXJ3ZC12ZXJBIHRib2R5IHRyJyk7XG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgIHZhciB0cmlnZ2VyVGQgPSB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3JBbGwoJ3RkJyk7XG4gICAgdmFyIG51bTAxID0gdHJpZ2dlclRkWzBdLmNsaWVudEhlaWdodDtcbiAgICB2YXIgbnVtMDIgPSB0cmlnZ2VyVGRbMV0uY2xpZW50SGVpZ2h0O1xuICAgIHZhciBudW0wMyA9IHRyaWdnZXJUZFsyXS5jbGllbnRIZWlnaHQ7XG4gICAgdmFyIHRvdGFsTnVtID0gbnVtMDEgKyBudW0wMiArIG51bTAzO1xuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEyMDAgJiYgdHJpZ2dlci5jbGFzc0xpc3QuY29udGFpbnMoJ2pzLWFjdGl2ZScpID09IGZhbHNlKSB7XG4gICAgICB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9IGAke3RvdGFsTnVtfXB4YDtcbiAgICB9IGVsc2UgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gMTIwMCkge1xuICAgICAgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPSBcIlwiO1xuICAgICAgdHJpZ2dlci5jbGFzc0xpc3QucmVtb3ZlKCdqcy1hY3RpdmUnKTtcbiAgICAgIHRyaWdnZXIucXVlcnlTZWxlY3RvcihcImlcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XG4gICAgfVxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEyMDAgJiYgdHJpZ2dlci5jbGFzc0xpc3QuY29udGFpbnMoJ2pzLWFjdGl2ZScpID09IGZhbHNlKSB7XG4gICAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gYCR7dG90YWxOdW19cHhgO1xuICAgICAgfSBlbHNlIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDEyMDApIHtcbiAgICAgICAgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPSBcIlwiO1xuICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5yZW1vdmUoJ2pzLWFjdGl2ZScpO1xuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gdG9nZ2xlU3RvcmVUYWJsZUhlaWdodEF0TW9iaWxlKCkge1xuICB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jLXNob3BUYWJsZS1yd2QtdmVyQSB0Ym9keSB0cicpO1xuICBub2RlTGlzdFRvQXJyYXkodHJpZ2dlcnMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcbiAgICB2YXIgdHJpZ2dlclRkID0gdHJpZ2dlci5xdWVyeVNlbGVjdG9yQWxsKCd0ZCcpO1xuICAgIHZhciBudW0wMSA9IHRyaWdnZXJUZFswXS5jbGllbnRIZWlnaHQ7XG4gICAgdmFyIG51bTAyID0gdHJpZ2dlclRkWzFdLmNsaWVudEhlaWdodDtcbiAgICB2YXIgbnVtMDMgPSB0cmlnZ2VyVGRbMl0uY2xpZW50SGVpZ2h0O1xuICAgIHZhciB0b3RhbE51bSA9IG51bTAxICsgbnVtMDIgKyBudW0wMztcbiAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJ0ZDpmaXJzdC1jaGlsZFwiKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEyMDAgJiYgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPT0gXCJcIikge1xuICAgICAgICB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9IGAke3RvdGFsTnVtfXB4YDtcbiAgICAgICAgdHJpZ2dlci5jbGFzc0xpc3QucmVtb3ZlKCdqcy1hY3RpdmUnKTtcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gXCJcIjtcbiAgICAgICAgdHJpZ2dlci5jbGFzc0xpc3QuYWRkKFwianMtYWN0aXZlXCIpO1xuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC5hZGQoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTtcbn1cblxuLy8gVE9ETzog5pyJ56m655yL5LiA5LiL54K65LuA6bq8bmV356ys5LiJ5YCL5bCx5Ye65LqL5LqGXG5mdW5jdGlvbiBjcmVhdGVTbGlja3MoKSB7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnYtc2xpY2tcIikpIHtcbiAgICB2YXIgdGFyZ2V0cyA9IFtcIiNuZXdzXCIsIFwiI3JlY29tbWFuZGF0aW9uXCJdO1xuICAgIHRhcmdldHMuZm9yRWFjaChmdW5jdGlvbiAodGFyZ2V0KSB7XG4gICAgICBuZXcgU2xpY2sodGFyZ2V0LCA0LCAxLCAxLCBbe1xuICAgICAgICBicmVha3BvaW50OiA5OTIsXG4gICAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgICAgc2xpZGVzVG9TaG93OiAyXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGJyZWFrcG9pbnQ6IDU3NixcbiAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDIsXG4gICAgICAgICAgYXJyb3dzOiBmYWxzZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIF0pLmluaXQoKTtcbiAgICB9KTtcbiAgICBuZXcgU2xpY2soXCIjdmlkZW9cIiwgMSwgMiwgMiwgW3tcbiAgICAgIGJyZWFrcG9pbnQ6IDk5MixcbiAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgIHNsaWRlc1RvU2hvdzogMixcbiAgICAgICAgc2xpZGVzUGVyUm93OiAxLFxuICAgICAgICByb3dzOiAxLFxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogNTc2LFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxuICAgICAgICBzbGlkZXNQZXJSb3c6IDEsXG4gICAgICAgIHJvd3M6IDEsXG4gICAgICAgIGFycm93czogZmFsc2UsXG4gICAgICB9XG4gICAgfSxcbiAgICBdKS5pbml0KCk7XG4gIH1cbn1cblxuZnVuY3Rpb24gcmlvbmV0U2xpY2soKSB7XG4gIHZhciBlbCA9IFwiLnYtcmlvbmV0U2xpY2tcIjtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpKSB7XG4gICAgJChlbCkuc2xpY2soe1xuICAgICAgLy8gY2VudGVyTW9kZTogdHJ1ZSxcbiAgICAgIC8vIGNlbnRlclBhZGRpbmc6ICc2MHB4JyxcbiAgICAgIHNsaWRlc1RvU2hvdzogNCxcbiAgICAgIGFycm93czogdHJ1ZSxcbiAgICAgIHByZXZBcnJvdzogYDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwidi10YWJCdG5TbGljay1wcmV2XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtbGVmdFwiPjwvaT48L2J1dHRvbj5gLFxuICAgICAgbmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLW5leHRcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1yaWdodFwiPjwvaT48L2J1dHRvbj5gLFxuICAgICAgbGF6eUxvYWQ6IFwicHJvZ3Jlc3NpdmVcIixcbiAgICAgIHJlc3BvbnNpdmU6IFt7XG4gICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcbiAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDMsXG4gICAgICAgICAgZG90czogdHJ1ZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgYnJlYWtwb2ludDogNTc2LFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogMixcbiAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxuICAgICAgICAgIC8vIHZhcmlhYmxlV2lkdGg6IHRydWUsXG4gICAgICAgICAgZG90czogdHJ1ZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIF0sXG4gICAgfSk7XG4gICAgLy8gICAkKCcudi10YWJCdG5TbGljay1wcmV2Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcbiAgICAvLyAgICAgJCgnLnYtdGFiQnRuU2xpY2snKS5zbGljaygnc2xpY2tQcmV2Jyk7XG4gICAgLy8gIH0pO1xuICAgIC8vICAgJCgnLnYtdGFiQnRuU2xpY2stbmV4dCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgLy8gICAgICQoJy52LXRhYkJ0blNsaWNrJykuc2xpY2soJ3NsaWNrTmV4dCcpO1xuICAgIC8vICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiB0YWJCdG5TbGljaygpIHtcbiAgdmFyIGVsID0gXCIudi10YWJCdG5TbGlja1wiO1xuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcbiAgICB2YXIgdGFiTGVuZ3RoID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkucXVlcnlTZWxlY3RvckFsbCgnLmMtdGFiLWNvbnRhaW5lcicpLmxlbmd0aDtcbiAgICB2YXIgc2xpZGVzVG9TaG93TnVtID0gKHRhYkxlbmd0aCA+IDYpID8gNiA6IHRhYkxlbmd0aDtcbiAgICAvLyBjb25zb2xlLmxvZyhcInNsaWRlc1RvU2hvd051bSA9IFwiICsgc2xpZGVzVG9TaG93TnVtKTtcbiAgICAkKGVsKS5zbGljayh7XG4gICAgICBzbGlkZXNUb1Nob3c6IHNsaWRlc1RvU2hvd051bSxcbiAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgYXJyb3dzOiB0cnVlLFxuICAgICAgcHJldkFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLXByZXZcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1sZWZ0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgICBuZXh0QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtdGFiQnRuU2xpY2stbmV4dFwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgICBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxuICAgICAgaW5maW5pdGU6IGZhbHNlLFxuICAgICAgcmVzcG9uc2l2ZTogW3tcbiAgICAgICAgYnJlYWtwb2ludDogMTIwMCxcbiAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXG4gICAgICAgICAgdmFyaWFibGVXaWR0aDogZmFsc2UsXG4gICAgICAgICAgLy8gc2xpZGVzVG9TY3JvbGw6IDUsXG4gICAgICAgICAgLy8gaW5maW5pdGU6IHRydWUsXG4gICAgICAgICAgLy8gY2VudGVyTW9kZTogZmFsc2UsXG4gICAgICAgIH1cbiAgICAgIH0sIHtcbiAgICAgICAgYnJlYWtwb2ludDogOTkyLFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogNCxcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcbiAgICAgICAgICAvLyBpbmZpbml0ZTogdHJ1ZSxcbiAgICAgICAgICAvLyBjZW50ZXJNb2RlOiBmYWxzZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgYnJlYWtwb2ludDogNzY4LFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiB0cnVlLFxuICAgICAgICAgIC8vIGluZmluaXRlOiBmYWxzZSxcbiAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgXSxcbiAgICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiB0YWJCdG5TbGlja0ZpeGVkKCkge1xuICB2YXIgZWwgPSBcIi52LXRhYkJ0blNsaWNrXCI7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xuICAgIHZhciB0YWJMZW5ndGggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKS5xdWVyeVNlbGVjdG9yQWxsKCcuYy10YWItY29udGFpbmVyJykubGVuZ3RoO1xuICAgIC8vIHZhciBzbGlkZXNUb1Nob3dOdW0gPSAodGFiTGVuZ3RoID4gNikgPyA2IDogdGFiTGVuZ3RoO1xuICAgIC8vIGNvbnNvbGUubG9nKHRhYkxlbmd0aCk7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDEyMDAgJiYgdGFiTGVuZ3RoIDw9IDYpIHtcbiAgICAgICQoXCIuc2xpY2stdHJhY2tcIikuYWRkQ2xhc3MoXCJ0ZXh0LW1kLWNlbnRlclwiKTtcbiAgICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGljay10cmFjaycpLmNsYXNzTGlzdC5hZGQoJ3RleHQtbWQtY2VudGVyJyk7XG4gICAgfVxuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+PSA3NjggJiYgd2luZG93LmlubmVyV2lkdGggPCAxMjAwICYmIHRhYkxlbmd0aCA8IDUpIHtcbiAgICAgICQoXCIuc2xpY2stdHJhY2tcIikuYWRkQ2xhc3MoXCJ0ZXh0LW1kLWNlbnRlclwiKTtcbiAgICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGljay10cmFjaycpLmNsYXNzTGlzdC5hZGQoJ3RleHQtbWQtY2VudGVyJyk7XG4gICAgfVxuICAgIC8vIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDc2OCkge1xuICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGljay10cmFjaycpLmNsYXNzTGlzdC5yZW1vdmUoJ3RleHQtbWQtY2VudGVyJyk7XG4gICAgLy8gfVxuICB9XG59XG5cbmZ1bmN0aW9uIHRhYkJ0blNsaWNrTWVtYmVycygpIHtcbiAgdmFyIGVsID0gXCIudi10YWJCdG5TbGljay1tZW1iZXJzXCI7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xuICAgICQoZWwpLnNsaWNrKHtcbiAgICAgIHNsaWRlc1RvU2hvdzogNixcbiAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgYXJyb3dzOiB0cnVlLFxuICAgICAgcHJldkFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLXByZXZcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1sZWZ0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgICBuZXh0QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtdGFiQnRuU2xpY2stbmV4dFwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgICBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxuICAgICAgaW5maW5pdGU6IGZhbHNlLFxuICAgICAgcmVzcG9uc2l2ZTogW3tcbiAgICAgICAgYnJlYWtwb2ludDogOTkyLFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogNSxcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgYnJlYWtwb2ludDogNzY4LFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiB0cnVlLFxuICAgICAgICAgIGFycm93czogZmFsc2UsXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBdLFxuICAgIH0pO1xuICB9XG59XG5cbmZ1bmN0aW9uIHRhYkJ0blNsaWNrRml4ZWRNZW1iZXJzKCkge1xuICB2YXIgZWwgPSBcIi52LXRhYkJ0blNsaWNrLW1lbWJlcnNcIjtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpKSB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDc2OCkge1xuICAgICAgJChcIi52LXRhYkJ0blNsaWNrLW1lbWJlcnMgLnNsaWNrLXRyYWNrXCIpLmFkZENsYXNzKFwidGV4dC1tZC1jZW50ZXJcIik7XG4gICAgfVxuICB9XG59XG4vLyBFbmQg6Lyq5pKt55u46ZecLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbmZ1bmN0aW9uIHdhcnJhbnR5TGluaygpIHtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIucC1pbmRleC13YXJyYW50eS1saW5rXCIpLmxlbmd0aCkge1xuICAgIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIucC1pbmRleC13YXJyYW50eS1saW5rXCIpO1xuICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdmVyXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiLmMtYnRuXCIpLmNsYXNzTGlzdC5hZGQoXCJqcy1idG5Ib3ZlclwiKTtcbiAgICAgIH0pO1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdXRcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCIuYy1idG5cIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLWJ0bkhvdmVyXCIpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbn1cbi8v5omL5qmf54mI5ryi5aCh6YG45ZauXG5mdW5jdGlvbiB0b2dnbGVNb2JpbGVNZW51KG1lZGlhUXVlcnkpIHtcbiAgdmFyIHRyaWdnZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhbWJ1cmdlclwiKTtcbiAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbWVudVwiKTtcblxuICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5jbGFzc0xpc3QudG9nZ2xlKFwianMtbWVudU9wZW5lZFwiKTtcbiAgICB0YXJnZXQuY2xhc3NMaXN0LnRvZ2dsZShcImpzLW1lbnVPcGVuZWRcIik7XG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC50b2dnbGUoXCJqcy1tZW51T3BlbmVkXCIpO1xuICB9KTtcblxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IG1lZGlhUXVlcnkpIHtcbiAgICAgIHRyaWdnZXIuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XG4gICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XG4gICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XG4gICAgfVxuICB9KTtcbn1cbi8v5omL5qmf54mI6aKo55C05oqY55aK6YG45ZauXG5mdW5jdGlvbiB0b2dnbGVNb2JpbGVTdWJtZW51KG1lZGlhUXVlcnkpIHtcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1tZW51LWxpbmtcIik7XG4gIHZhciB0YXJnZXRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1zdWJtZW51LS1oaWRkZW5cIik7XG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgIGlmICh0cmlnZ2VyLm5leHRFbGVtZW50U2libGluZyAhPT0gbnVsbCkge1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB2YXIgdGFyZ2V0ID0gdHJpZ2dlci5uZXh0RWxlbWVudFNpYmxpbmc7XG4gICAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IG1lZGlhUXVlcnkpIHtcbiAgICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XG4gICAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgICAgICAvL+eCuuS6humWi+S4gOWAi+mXnOS4gOWAi++8jOeUqGpRdWVyeeWKoOWvq1xuICAgICAgICAgICQodGFyZ2V0KS5wYXJlbnRzKCkuc2libGluZ3MoKS5maW5kKFwiLmwtaGVhZGVyLXN1Ym1lbnUgLCBpXCIpLnJlbW92ZUNsYXNzKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9KTtcblxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IG1lZGlhUXVlcnkpIHtcbiAgICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgICAgICB2YXIgdGFyZ2V0ID0gdHJpZ2dlci5uZXh0RWxlbWVudFNpYmxpbmc7XG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIHNpYmluZ01vYmlsZVN1Ym1lbnUoKSB7XG4gICQoXCIubC1oZWFkZXItbWVudS1pdGVtXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICQodGhpcykuc2libGluZ3MoKS5maW5kKFwiLmwtaGVhZGVyLXN1Ym1lbnVcIikucmVtb3ZlQ2xhc3MoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gdG9nZ2xlUGNIb3ZlclN0YXRlKG1lZGlhUXVlcnkpIHtcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1tZW51LWl0ZW1cIik7XG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgIGlmICh0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCIubC1oZWFkZXItc3VibWVudVwiKSAhPT0gbnVsbCkge1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdmVyXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiLmwtaGVhZGVyLW1lbnUtbGlua1wiKS5jbGFzc0xpc3QuYWRkKFwianMtaG92ZXJcIik7XG4gICAgICB9KTtcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlb3V0XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdHJpZ2dlclxuICAgICAgICAgIC5xdWVyeVNlbGVjdG9yKFwiLmwtaGVhZGVyLW1lbnUtbGlua1wiKVxuICAgICAgICAgIC5jbGFzc0xpc3QucmVtb3ZlKFwianMtaG92ZXJcIik7XG4gICAgICB9KTtcbiAgICB9XG4gIH0pO1xuXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCBtZWRpYVF1ZXJ5KSB7XG4gICAgICBBcnJheS5wcm90b3R5cGUuc2xpY2VcbiAgICAgICAgLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1tZW51LWxpbmtcIikpXG4gICAgICAgIC5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgaXRlbS5jbGFzc0xpc3QucmVtb3ZlKFwianMtaG92ZXJcIik7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGhlYWRlckZ1bmN0aW9uKCkge1xuICB2YXIgYnJlYWtwb2ludCA9IDEyMDA7XG4gIHRvZ2dsZU1vYmlsZU1lbnUoYnJlYWtwb2ludCk7XG4gIHRvZ2dsZU1vYmlsZVN1Ym1lbnUoYnJlYWtwb2ludCk7XG4gIHRvZ2dsZVBjSG92ZXJTdGF0ZShicmVha3BvaW50KTtcbn1cblxuZnVuY3Rpb24gbGF6eUxvYWQoKSB7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiaW1nW2RhdGEtc3JjXVwiKSkge1xuICAgIHZhciBvYnNlcnZlciA9IGxvemFkKCk7XG4gICAgb2JzZXJ2ZXIub2JzZXJ2ZSgpO1xuICAgIGlmIChcbiAgICAgIC0xICE9PSBuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoXCJNU0lFXCIpIHx8XG4gICAgICBuYXZpZ2F0b3IuYXBwVmVyc2lvbi5pbmRleE9mKFwiVHJpZGVudC9cIikgPiAwXG4gICAgKSB7XG4gICAgICB2YXIgaWZyYW1lID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImlmcmFtZVwiKTtcbiAgICAgIGlmcmFtZS5zZXRBdHRyaWJ1dGUoXCJzcmNcIiwgXCJodHRwczovL3d3dy55b3V0dWJlLmNvbS9lbWJlZC9PN3BOcFIzUHk2OFwiKTtcbiAgICB9XG4gIH1cbn1cbi8v6KiI566X44CM5pyA5paw5raI5oGv44CN44CB44CM5Lq65rCj5o6o6Jam44CN6YCZ6aGe55qE5Y2h54mH5YiX6KGo55qE5qiZ6aGM5a2X5pW477yM6K6T5aSn5a6255qE6auY5bqm5LiA5qijXG5mdW5jdGlvbiBhdXRvRml4SGVpZ2h0KGNvbikge1xuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGNvbik7XG4gIHZhciB0aGlzSGVpZ2h0ID0gLTE7XG4gIHZhciBtYXhIZWlnaHQgPSAtMTtcbiAgdmFyIGJyZWFrcG9pbnQgPSA3Njg7XG4gIC8v54K65LqGaWXkuI3mlK/mj7Rub2RlbGlzdOeahGZvckVhY2jkv67mraNcbiAgbm9kZUxpc3RUb0FycmF5KGVsKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgaXRlbS5zdHlsZS5oZWlnaHQgPSBcIlwiOyAvL+a4heepuuS5i+WJjeeahHN0eWxlXG4gICAgdGhpc0hlaWdodCA9IGl0ZW0uY2xpZW50SGVpZ2h0OyAvL+WPluW+l+W3sue2k+a4m+mBjumrmOW6pueahFxuICAgIG1heEhlaWdodCA9IG1heEhlaWdodCA+IHRoaXNIZWlnaHQgPyBtYXhIZWlnaHQgOiB0aGlzSGVpZ2h0O1xuICB9KTtcbiAgaWYgKGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggPiBicmVha3BvaW50KSB7XG4gICAgbm9kZUxpc3RUb0FycmF5KGVsKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICBpdGVtLnN0eWxlLmhlaWdodCA9IGAke21heEhlaWdodH1weGA7XG4gICAgfSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gdGV4dEhpZGUobnVtLCBjb24pIHtcbiAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChjb24pO1xuICAvL2ll5omN5Z+36KGM6YCZ5YCL6LaF6YGO5a2X5pW45aKe5Yqg5Yiq56+A6Jmf77yM5YW25LuW54CP6Ka95Zmo6Z2gY3Nz6Kqe5rOV5Y2z5Y+vXG4gIC8vIGlmIChcbiAgLy8gICBuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoXCJNU0lFXCIpICE9PSAtMSB8fFxuICAvLyAgIG5hdmlnYXRvci5hcHBWZXJzaW9uLmluZGV4T2YoXCJUcmlkZW50L1wiKSA+IDBcbiAgLy8gKSB7XG4gIC8vICAgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoZWwpLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcbiAgLy8gICAgIHZhciB0eHQgPSBpdGVtLmlubmVyVGV4dDtcbiAgLy8gICAgIGlmICh0eHQubGVuZ3RoID4gbnVtKSB7XG4gIC8vICAgICAgIHR4dENvbnRlbnQgPSB0eHQuc3Vic3RyaW5nKDAsIG51bSAtIDEpICsgXCIuLi5cIjtcbiAgLy8gICAgICAgaXRlbS5pbm5lckhUTUwgPSB0eHRDb250ZW50O1xuICAvLyAgICAgfVxuICAvLyAgIH0pO1xuICAvLyB9XG4gIG5vZGVMaXN0VG9BcnJheShlbCkuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xuICAgIHZhciB0eHQgPSBpdGVtLmlubmVyVGV4dDtcbiAgICBpZiAodHh0Lmxlbmd0aCA+IG51bSkge1xuICAgICAgdHh0Q29udGVudCA9IHR4dC5zdWJzdHJpbmcoMCwgbnVtIC0gMSkgKyBcIi4uLlwiO1xuICAgICAgaXRlbS5pbm5lckhUTUwgPSB0eHRDb250ZW50O1xuICAgIH0gZWxzZSB7XG4gICAgICB0eHRDb250ZW50ID0gdHh0LnN1YnN0cmluZygwLCBudW0pICsgXCIuLi5cIjtcbiAgICAgIGl0ZW0uaW5uZXJIVE1MID0gdHh0Q29udGVudDtcbiAgICB9XG4gIH0pO1xuICBhdXRvRml4SGVpZ2h0KGNvbik7XG59XG5cbmZ1bmN0aW9uIGNsZWFyQ2hlY2tCb3goZWwsIHRhcmdldCkge1xuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcbiAgICB2YXIgdHJpZ2dlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpO1xuICAgIHZhciB0YXJnZXRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0YXJnZXQpO1xuICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB0cmlnZ2VyLmJsdXIoKTtcbiAgICAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRhcmdldHMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcbiAgICAgICAgdHJpZ2dlci5jaGVja2VkID0gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiBjYXJkQ29udGVudEZ1bmN0aW9uKCkge1xuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5jLWNhcmQtYm9keS10aXRsZVwiKSAhPT0gbnVsbCkge1xuICAgIGF1dG9GaXhIZWlnaHQoXCIuYy1jYXJkLWJvZHktdGl0bGVcIik7XG4gIH1cbiAgLy/kurrmsKPmjqjolqblhafmloflrZfmlbjpmZDliLZcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuYy1jYXJkLWJvZHktdHh0XCIpICE9PSBudWxsKSB7XG4gICAgdGV4dEhpZGUoNDgsIFwiLmMtY2FyZC1ib2R5LXR4dFwiKTtcbiAgfVxufVxuXG5mdW5jdGlvbiB0b29sVGlwcygpIHtcbiAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoe1xuICAgIHBsYWNlbWVudDogJ3JpZ2h0JyxcbiAgICB0cmlnZ2VyOiAnaG92ZXInLFxuICAgIG9mZnNldDogMzAsXG4gIH0pO1xuICAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykub24oJ2luc2VydGVkLmJzLnRvb2x0aXAnLCBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGdldFRvb3RpcHNJZCA9IFwiI1wiICsgJCh0aGlzKS5hdHRyKFwiYXJpYS1kZXNjcmliZWRieVwiKTtcbiAgICAvLyBjb25zb2xlLmxvZyhnZXRUb290aXBzSWQpO1xuICAgIC8vIGNvbnNvbGUubG9nKCQodGhpcykuYXR0cihcImRhdGEtb3JpZ2luYWwtdGl0bGVcIikgPT0gXCLlvrflnIvolKHlj7jmlbjkvY3lvbHlg4/ns7vntbFcIik7XG4gICAgaWYgKCQodGhpcykuYXR0cihcImRhdGEtb3JpZ2luYWwtdGl0bGVcIikgPT0gXCLlvrflnIvolKHlj7jmlbjkvY3lvbHlg4/ns7vntbFcIikge1xuICAgICAgJChnZXRUb290aXBzSWQpLmZpbmQoJy50b29sdGlwLWlubmVyJykuaHRtbChcIuW+t+Wci+iUoeWPuDxicj7mlbjkvY3lvbHlg4/ns7vntbFcIik7XG4gICAgfVxuICAgIGlmICgkKHRoaXMpLmF0dHIoXCJkYXRhLW9yaWdpbmFsLXRpdGxlXCIpID09IFwi5rOV5ZyL5L6d6KaW6Lev5YWo5pa55L2N6KaW6Ka65qqi5ris57O757WxXCIpIHtcbiAgICAgICQoZ2V0VG9vdGlwc0lkKS5maW5kKCcudG9vbHRpcC1pbm5lcicpLmh0bWwoXCLms5XlnIvkvp3oppbot688YnI+5YWo5pa55L2N6KaW6Ka65qqi5ris57O757WxXCIpO1xuICAgIH1cbiAgfSlcbn1cblxuZnVuY3Rpb24gYW9zKCkge1xuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIltkYXRhLWFvc11cIikpIHtcbiAgICBBT1MuaW5pdCh7XG4gICAgICBvbmNlOiB0cnVlLFxuICAgIH0pO1xuICB9XG59XG5cbmZ1bmN0aW9uIHl0SGFuZGxlcigpIHtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJbZGF0YS11cmxdXCIpKSB7XG4gICAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIltkYXRhLXVybF1cIik7XG4gICAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJpZnJhbWVcIik7XG5cbiAgICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgICAgdHJpZ2dlci5vbmNsaWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0YXJnZXQuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS11cmxcIikpO1xuICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5hZGQoXCJqcy1hY3RpdmVcIik7XG5cbiAgICAgICAgQXJyYXkucHJvdG90eXBlLnNsaWNlXG4gICAgICAgICAgLmNhbGwodHJpZ2dlcnMpXG4gICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgcmV0dXJuIGl0ZW0gIT09IHRyaWdnZXI7XG4gICAgICAgICAgfSlcbiAgICAgICAgICAuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgaXRlbS5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWN0aXZlXCIpO1xuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiBjYXJkSnVzdGlmeUNvbnRlbnQoKSB7XG4gIHZhciBlbCA9ICQoXCIuanMtY2FyZEp1c3RpZnlDb250ZW50XCIpO1xuICBpZiAoZWwpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGVsLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZWxDaGlsZHJlbkxlbmd0aCA9IGVsLmVxKGkpLmNoaWxkcmVuKCdkaXYnKS5sZW5ndGg7XG4gICAgICAvLyBjb25zb2xlLmxvZyhlbENoaWxkcmVuTGVuZ3RoKTtcbiAgICAgIGlmIChlbENoaWxkcmVuTGVuZ3RoIDw9IDIpIHtcbiAgICAgICAgZWwuZXEoaSkuYWRkQ2xhc3MoXCJqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCIpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZWwuZXEoaSkucmVtb3ZlQ2xhc3MoXCJqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCIpO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBzdG9yZUZpbHRlck5vdGlmaWNhdGlvbihpbnB1dENvbnRhaW5lciwgdGFyZ2V0RWwpIHtcbiAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihpbnB1dENvbnRhaW5lcik7XG4gIHZhciB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRhcmdldEVsKTtcbiAgaWYgKGVsKSB7XG4gICAgLy8gY29uc29sZS5sb2coaW5wdXRDb250YWluZXIgKyBcIiArIFwiICsgdGFyZ2V0KTtcbiAgICB2YXIgdHJpZ2dlcnMgPSBlbC5xdWVyeVNlbGVjdG9yQWxsKFwiaW5wdXRbdHlwZT0nY2hlY2tib3gnXVwiKTtcbiAgICAvLyBjb25zb2xlLmxvZyh0cmlnZ2Vycyk7XG4gICAgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwodHJpZ2dlcnMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGNoZWNrZWROdW0gPSBlbC5xdWVyeVNlbGVjdG9yQWxsKFwiaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZFwiKS5sZW5ndGg7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGNoZWNrZWROdW0pO1xuICAgICAgICBpZiAoY2hlY2tlZE51bSA+IDApIHtcbiAgICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LmFkZChcImpzLWlucHV0Q2hlY2tlZFwiKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWlucHV0Q2hlY2tlZFwiKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG4gICAgdmFyIGNsZWFyQWxsQnRuRWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2pzLWNsZWFyQ2hlY2tCb3hlc1wiKTtcbiAgICBjbGVhckFsbEJ0bkVsLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWlucHV0Q2hlY2tlZFwiKTtcbiAgICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiBkYXRlTW9iaXNjcm9sbCgpIHtcbiAgaWYgKCQoJyNkYXRlTW9iaXNjcm9sbElkJykpIHtcbiAgICAkKCcjZGF0ZU1vYmlzY3JvbGxJZCcpLm1vYmlzY3JvbGwoKS5kYXRlKHtcbiAgICAgIHRoZW1lOiAkLm1vYmlzY3JvbGwuZGVmYXVsdHMudGhlbWUsIC8vIFNwZWNpZnkgdGhlbWUgbGlrZTogdGhlbWU6ICdpb3MnIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcbiAgICAgIG1vZGU6ICdtaXhlZCcsIC8vIFNwZWNpZnkgc2Nyb2xsZXIgbW9kZSBsaWtlOiBtb2RlOiAnbWl4ZWQnIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcbiAgICAgIGRpc3BsYXk6ICdtb2RhbCcsIC8vIFNwZWNpZnkgZGlzcGxheSBtb2RlIGxpa2U6IGRpc3BsYXk6ICdib3R0b20nIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcbiAgICAgIGxhbmc6ICd6aCcgLy8gU3BlY2lmeSBsYW5ndWFnZSBsaWtlOiBsYW5nOiAncGwnIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcbiAgICB9KTtcbiAgfVxuXG59XG4vL+S4i+aLiemBuOWWrui3s+i9iei2hemAo+e1kFxuZnVuY3Rpb24gc2VsZWN0VVJMKCkge1xuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmpzLXNlbGVjdFVSTFwiKTtcbiAgaWYgKGVsKSB7XG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICB3aW5kb3cub3Blbih0aGlzLm9wdGlvbnNbdGhpcy5zZWxlY3RlZEluZGV4XS52YWx1ZSk7XG4gICAgfSk7XG4gIH1cbn1cbi8v5ZG85Y+rZnVuY3Rpb24t57ay6aCB6LyJ5YWl5a6M5oiQ5b6MXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gIHNlbGVjdFVSTCgpO1xuICB0b29sc0xpc3RlbmVyKCk7XG4gIGNyZWF0ZVNsaWNrcygpO1xuICB0YWJCdG5TbGljaygpO1xuICB0YWJCdG5TbGlja0ZpeGVkKCk7XG4gIC8v5pyD5ZOh5qyK55uK5bCI55SoXG4gIHRhYkJ0blNsaWNrTWVtYmVycygpO1xuICB0YWJCdG5TbGlja0ZpeGVkTWVtYmVycygpO1xuICByaW9uZXRTbGljaygpO1xuICB3YXJyYW50eUxpbmsoKTtcbiAgbGF6eUxvYWQoKTtcbiAgY3JlYXRlQWNjb3JkaW9uKCk7XG4gIC8vIGNyZWF0ZVZpZGVvU3dpcGVyKCk7XG4gIGFvcygpO1xuICB5dEhhbmRsZXIoKTtcbiAgdG9nZ2xlcEZpbHRlclN1Ym1lbnUoKTtcbiAgY2xlYXJDaGVja0JveChcIiNqcy1jbGVhckNoZWNrQm94ZXNcIiwgXCJpbnB1dFt0eXBlPSdjaGVja2JveCddXCIpO1xuICBzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKTtcbiAgLy8gJCgnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXScpLnBvcG92ZXIoKTtcbiAgdG9vbFRpcHMoKTtcbiAgY2FyZEp1c3RpZnlDb250ZW50KCk7XG4gIHN0b3JlRmlsdGVyTm90aWZpY2F0aW9uKFwiLnYtZHJvcGRvd24tbWVudVwiLCBcIi52LWRyb3Bkb3duLWJ0blwiKTtcbiAgc3RvcmVGaWx0ZXJOb3RpZmljYXRpb24oXCIucC1wcm9kdWN0cy1zZWFyY2hcIiwgXCIucC1wcm9kdWN0cy1zZWFyY2gtYnRuXCIpO1xuICB0b2dnbGVTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKTtcbiAgZGF0ZU1vYmlzY3JvbGwoKTtcbiAgZ29Ub0FuY2hvcigpO1xuICAvLyBnb1RvcFN0aWNreSgpO1xufSk7XG53aW5kb3cub25sb2FkID0gZnVuY3Rpb24gKCkge1xuICBjYXJkQ29udGVudEZ1bmN0aW9uKCk7XG4gIHNob3dBbGxUYWIoKTtcbn07XG4vL+WRvOWPq2Z1bmN0aW9uLeimlueql+Wkp+Wwj+iuiuabtFxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbiAoKSB7XG4gIGNhcmRDb250ZW50RnVuY3Rpb24oKTtcbiAgc2hvd0FsbFRhYigpO1xuICAvLyBzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKTtcbiAgY29uZmlybUlmRG91YmxlTWVudU9wZW5lZChcIi5wLXByb2R1Y3RzLXNlYXJjaFwiLCA5OTIpO1xuICB0YWJCdG5TbGlja0ZpeGVkKCk7XG59KTtcbi8v5ZG85Y+rZnVuY3Rpb24t5o2y5YuVXG4kKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uICgpIHtcbiAgZ29Ub3BTdGlja3koKTtcbiAgLy8gc2lkZUxpbmtTdGlja3koKTtcbn0pOyJdLCJzb3VyY2VSb290IjoiIn0=