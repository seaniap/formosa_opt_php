<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>門市查詢 ｜  寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="提供寶島眼鏡全台門市查詢服務，可查詢門市地址、電話、營業時間、Lina@、門市服務等門市資訊。">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/icomoon/style.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <!-- <link rel="stylesheet" href="assets/plugins/page/pagination.css" /> -->
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/plugins/page/pagination.js"></script>
    <script src="assets/plugins/underscore-umd-min.js" type="text/javascript"></script>

    <script>
        function genStore() {
            var url = "app/controller/Store.php?method=getStore";


            var params = {
                "s_city": $('#city').val(),
                "s_town": $('#town').val(),
                "s_name": $('#name').val(),
                "s_is_wam": $("#s_is_wam").prop("checked") ? "Y" : "N",
                "s_is_slit": $("#s_is_slit").prop("checked") ? "Y" : "N",
                "s_stars_1": $("#s_stars_1").prop("checked") ? "Y" : "N",
                "s_stars_2": $("#s_stars_2").prop("checked") ? "Y" : "N",
                "s_stars_3": $("#s_stars_3").prop("checked") ? "Y" : "N",
                "is_listen": $("#is_listen").prop("checked") ? "Y" : "N",
                "is_hearing_aids": $("#is_hearing_aids").prop("checked") ? "Y" : "N",
            };


            $.get(url, params, function(result) {
                result = JSON.parse(result);

                var container = $('#pagination_page');
                container.pagination({
                    dataSource: result,
                    //locator: 'stores',
                    pageSize: 5,
                    callback: function(response, pagination) {
                        console.log(response);
                        var compiled = _.template($("#tpl").html());
                        var html = compiled({
                            "datas": response
                        });
                        $("#pagination-data").html(html);
                    }
                });
            });
        }

        function genTown() {
            var params = {
                "s_city": $('#city').val(),
            };
            $.get("app/controller/Store.php?method=getTown", params, function(result) {
                result = JSON.parse(result);

                $('#town').children().remove().end()
                    .append('<option value="" selected  >選擇區域</option>');

                $.each(result, function(i, data) {
                    $('#town').append($('<option>', {
                        value: data.s_town,
                        text: data.s_town
                    }));
                })
            });
        }

        function genNeme() {
            var params = {
                "s_city": $('#city').val(),
                "s_town": $('#town').val(),
            };

            $.get("app/controller/Store.php?method=getName", params, function(result) {
                result = JSON.parse(result);
                $('#name').children().remove().end()
                    .append('<option value="" selected >所有門市</option');


                $.each(result, function(i, data) {
                    $('#name').append($('<option>', {
                        value: data.s_name,
                        text: data.s_name
                    }));
                });
            });
        }



        $(function() {
            var init = true;
            //add city
            $.get("app/controller/Store.php?method=getCity", function(result) {
                result = JSON.parse(result);
 
                 $.each(result, function(i, data) {
                    $('#city').append($('<option>', {
                        value: data.s_city,
                        text: data.s_city
                    }));
                })

                genStore();
                /*預設取消
                $("#city").val("新北市");
                var params = {
                    "s_city": $('#city').val(),
                };

                $.get("app/controller/Store.php?method=getTown", params, function(result) {
                    result = JSON.parse(result);

                    $('#town').children().remove().end()
                        .append('<option value="" selected  >選擇區域</option>');

                    $.each(result, function(i, data) {
                        $('#town').append($('<option>', {
                            value: data.s_town,
                            text: data.s_town
                        }));
                    })

                    $("#town").val("汐止區");
                    genNeme();
                    genStore();
                });
                */
            });



            $('#city').change(function() {
                $('#town').children().remove().end().append('<option value="" selected >選擇區域</option>');
                $('#name').children().remove().end().append('<option value="" selected >所有門市</option');
                genTown();
                genStore();
            });

            $('#town').change(function() {
                $('#name').children().remove().end().append('<option value="" selected >所有門市</option');
                genNeme();
                genStore();
            });

            $('#name').change(function() {
                genStore();
            });

            $("#s_is_wam").click(function() {
                genStore();
            });

            $("#s_is_slit").click(function() {
                genStore();
            });


            $("#s_stars_1").click(function() {
                genStore();
            });

            $("#s_stars_2").click(function() {
                genStore();
            });

            $("#s_stars_3").click(function() {
                genStore();
            });

            $("#is_listen").click(function() {
                genStore();
            });

            $("#is_hearing_aids").click(function() {
                genStore();
            });
        });
    </script>

    <!-- JS Customization -->
    <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php') ?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section style="min-height: 600px;">
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">門市查詢</h3>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <div class="row">
                            <div class="col-6 col-lg-3 mb-3 mb-lg-0">
                                <select name="select" id="city" class="l-form-field l-form-field-verB l-form-select u-bg-gray-200">
                                    <option value="" selected>選擇縣市</option>
                                </select>
                            </div>
                            <div class="col-6 col-lg-3 mb-3 mb-lg-0">
                                <select name="select" id="town" class="l-form-field l-form-field-verB l-form-select u-bg-gray-200">
                                    <option value="" selected>選擇區域</option>
                                </select>
                            </div>
                            <div class="col-6 col-lg-3">
                                <select name="select" id="name" class="l-form-field l-form-field-verB l-form-select u-bg-gray-200">
                                    <option value="" selected>所有門市</option>
                                </select>
                            </div>
                            <div class="col-6 col-lg-3 v-dropdown-father">
                                <div class="dropdown v-dropdown">
                                    <button class="btn dropdown-toggle w-100 text-left v-dropdown-btn v-dropdown-btn d-flex align-items-center" type="button" id="dropdownMenuButton">
                                        <i class="fal fa-filter mr-2"></i><span class="v-dropdown-btn-overflow-text">篩選門市服務</span>
                                    </button>
                                    <div class="dropdown-menu v-dropdown-menu w-100">
                                        <form action="">
                                            <div class="v-dropdown-header text-center justify-content-center align-items-center">
                                                <button type="button" class="close">
                                                    <i class="fal fa-times"></i>
                                                </button>
                                                <p class="mb-0 font-weight-bold u-font-20">篩選門市服務</p>
                                            </div>
                                            <div class="v-dropdown-content">
                                                <p class="u-mb-000">驗光儀器：</p>
                                                <div class="d-flex align-items-center mb-2">
                                                    <input type="checkbox" name="s_is_slit" id="s_is_slit" class="l-form-check-input mr-2" value="Y">
                                                    <label for="s_is_slit" class="l-form-check-label u-font-12 mb-0">
                                                        <div class="d-flex align-items-center">
                                                            <img src="assets/img/store/system_01.png" alt="" class="c-shopTable-img-system mr-2">
                                                            <div>德國蔡司<br>數位影像系統</div>
                                                        </div>
                                                    </label>
                                                </div>

                                                <div class="d-flex align-items-center">
                                                    <input type="checkbox" name="s_is_wam" id="s_is_wam" class="l-form-check-input mr-2 order-1" value="Y">
                                                    <label for="s_is_wam" class="l-form-check-label u-font-12 mb-0">
                                                        <div class="d-flex align-items-center">
                                                            <img src="assets/img/store/system_02.png" alt="" class="c-shopTable-img-system mr-2">
                                                            <div>
                                                                法國依視路<br>數位影像系統
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <hr class="u-my-100">
                                                <p class="u-mb-000">隱形眼鏡星級驗光所：</p>

                                                <div class="d-flex align-items-center">
                                                    <input type="checkbox" name="s_stars_1" id="s_stars_1" class="l-form-check-input mr-2" value="1">
                                                    <label for="s_stars_1" class="l-form-check-label">
                                                        <img src="assets/img/store/star_01.png" alt="1星" class="c-shopTable-img-star">
                                                    </label>
                                                </div>

                                                <div class="d-flex align-items-center">
                                                    <input type="checkbox" name="s_stars_2" id="s_stars_2" class="l-form-check-input mr-2" value="2">
                                                    <label for="s_stars_2" class="l-form-check-label">
                                                        <img src="assets/img/store/star_02.png" alt="2星" class="c-shopTable-img-star">
                                                    </label>
                                                </div>
                                                <div class="d-flex align-items-center">
                                                    <input type="checkbox" name="s_stars_3" id="s_stars_3" class="l-form-check-input mr-2" value="3">
                                                    <label for="s_stars_3" class="l-form-check-label">
                                                        <img src="assets/img/store/star_03.png" alt="3星" class="c-shopTable-img-star">
                                                    </label>
                                                </div>
                                                <hr class="u-my-100">
                                                <p class="u-mb-000">助聽器驗配：</p>
                                                
 												<div class="d-sm-flex d-xl-block align-items-sm-start">
                                                     <input type="checkbox" name="is_listen" id="is_listen" value="Y" class="l-form-check-input mt-sm-1 mr-sm-1 mt-xl-0 mr-xl-0">
                                                    <label for="is_listen" class="l-form-check-label">聽力測試</label>
                                                </div>
                                            
                                                <div class="d-sm-flex d-xl-block align-items-sm-start">
                                                    <input type="checkbox" name="is_hearing_aids" id="is_hearing_aids" value="Y" class="l-form-check-input mt-sm-1 mr-sm-1 mt-xl-0 mr-xl-0">
                                                    <label for="is_hearing_aids" class="l-form-check-label">助聽器試戴</label>
                                                </div>
                                            </div>

                                            <div class="v-dropdown-footer mb-3 justify-content-center">
                                                <div class="col-sm-3">
                                                    <button class="c-btn c-btn-blue-highlight c-btn--outlined w-100" id="js-clearCheckBoxes">全部清除</button>
                                                </div>
                                                <div class="col-sm-3">
                                                    <button class="c-btn c-btn-blue-highlight c-btn--contained w-100" id="js-confirm">確定</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="u-py-100">
                    <div class="container">
                        <div class="l-tablesSroller-wrapper">
                           <div class="l-tablesSroller-wrapper u-pb-200" id="pagination-data"></div>
                            
                            <script type="text/template" id="tpl">
                                <table class="c-shopTable c-shopTable-rwd c-shopTable-rwd-verA c-shopTable--bordered__bottom">
                                <thead>
                                    <tr>
                                        <th scope="col">
                                            <span>門市名稱</span>
                                        </th>
                                        <th>
                                            <span>門市電話</span>
                                        </th>
                                        <th>
                                            <span>門市地址</span>
                                        </th>
                                        <th>
                                            <span>營業時間</span>
                                        </th>
                                        <th>
                                            <span>Line</span>
                                        </th>
                                        <th>
                                            <span>驗光儀器</span>
                                        </th>
                                        <th>
                                            <span>星級<br>驗光所</span>
                                        </th>
                                        <th>
                                            <span>助聽器<br>驗配</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% _.each(datas, function(data){ %>
                                    
                                    <tr>
                                        <td data-th="門市名稱">
                                            <span><%= data.s_name %> <i class="far fa-angle-down u-ml-025 d-xl-none"></i></span>
                                        </td>
                                        <td data-th="門市電話">
                                            <a href="tel:%= data.s_phone %>">
                                                <span><%= data.s_phone %></span>
                                            </a>
                                        </td>
                                        <td data-th="門市地址">
                                            <a href="https://www.google.com.tw/maps/dir/<%= data.s_city %><%- data.s_address %>" target="_blank">
                                                <span><%= data.s_city %><%- data.s_address %>   </span>
                                            </a>
                                        </td>
                                        <td data-th="營業時間">
                                            <span><%= data.s_start_time %>~<%= data.s_end_time %></span>
                                        </td>
                                        <td data-th="Line">
                                            <a href="https://line.me/R/ti/p/@<%= data.s_line %>">
                                                <span>@<%= data.s_line %></span>
                                            </a>
                                        </td>
                                        <td data-th="驗光儀器">
                                            <% if (data.s_is_wam =='Y') { %>
                                               <span>
                                                <img src="assets/img/store/system_02.png" alt=""
                                                    class="c-shopTable-img-system" data-toggle="tooltip"
                                                    title="法國依視路全方位視覺檢測系統">
                                                </span>
                                            <% } %>

                                            <% if (data.s_is_slit  =='Y') { %>
                                                <span>
                                                 <img src="assets/img/store/system_01.png" alt=""
                                                    class="c-shopTable-img-system" data-toggle="tooltip" title="德國蔡司數位影像系統">
                                                </span>
                                            <% } %>
                                        </td>
                                        <td data-th="星級驗光所">
                                            <span>
                                                <img src="assets/img/store/star_0<%= data.s_stars %>.png" alt="1星"
                                                    class="c-shopTable-img-star">
                                            </span>
                                        </td>
                                        <td data-th="助聽器驗配">
                                            <span>
                                                 <% if (data.is_listen =='Y' && data.is_hearing_aids =='Y') { %>
                                                                                                                                                                         助聽器試戴<br class="d-none d-xl-inline">聽力測試
                                                <%} else if (data.is_hearing_aids =='Y'){%>
                                                                                                                                                                        助聽器試戴
                                                <%} else if (data.is_listen =='Y'){%>
                                                                                                                                                                        聽力測試
                                                <%}%>
                                            </span>
                                        </td>
                                    </tr>
                                    <% }) %>
                                </tbody>
                            </table>
                        </script>

                        </div>

                    </div>
                </div>
                <!-- START 頁籤 -->
                <div class="u-py-100 u-pb-400">
                    <div class="container">
                        <nav>
                            <ul class="c-pagination justify-content-center align-items-center u-mb-000">
                                <li class="c-pagination-item">

                                </li>
                                <div id="pagination_page"></div>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- END 頁籤 -->
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
        <!-- =============end footer ============= -->
    </div>
</body>

</html>