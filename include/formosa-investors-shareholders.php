<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">股東會資料</h3>
            </div>
        </div>
        <div class="u-pb-100">
                    <div class="container">
                        <div class="c-accordion">
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent11">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">111年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon js-accordionExpended">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent11" class="c-accordion-content js-accordionExpended">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/111/111年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>111年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/111/111年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>111年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/111/111年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>111年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/111/111年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>111年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/111/111年開會通知_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>111年 開會通知(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/111/111年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>111年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/111/111年議事手冊及會議補充資料_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>111年 議事手冊及會議補充資料(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/111/111年取得或處分資產處理程序_訂定或修正.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>111年 取得或處分資產處理程序(訂定或修正)</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent10">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">110年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent10" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年開會通知_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 開會通知(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年議事手冊及會議補充資料_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 議事手冊及會議補充資料(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年取得或處分資產處理程序_訂定或修正.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 取得或處分資產處理程序(訂定或修正)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/110/110年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>110年 股東會議事錄</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent9">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">109年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent9" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/109/109年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/109/109年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/109/109年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/109/109年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/109/109年開會通知_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年 開會通知(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/109/109年會議議事手冊_英文版.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年 會議議事手冊(英文版)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/109/109年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/109/109年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>109年 股東會議事錄</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent8">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">108年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent8" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/108/108年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/108/108年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/108/108年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/108/108年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/108/108年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/108/108年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/108/108年取得或處分資產處理程序修正條文對照表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年 取得或處分資產處理程序修正條文對照表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/108/108年資金貸與他人或背書保證作業程序修正條文對照表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>108年 資金貸與他人或背書保證作業程序修正條文對照表</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent7">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">107年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent7" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/107/107年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/107/107年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/107/107年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/107/107年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/107/107年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/107/107年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>107年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent6">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">106年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent6" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/106/106年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/106/106年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/106/106年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/106/106年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/106/106年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/106/106年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>106年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent5">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">105年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent5" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/105/105年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/105/105年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/105/105年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/105/105年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/105/105年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/105/105年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>105年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent4">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">104年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent4" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/104/104年年報前十大股東相互間關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/104/104年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/104/104年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/104/104年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/104/104年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/104/104年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>104年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent3">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">103年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent3" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/103/103年年報前十大股東相互關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/103/103年股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/103/103年股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/103/103年股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/103/103年背書保證作業程序修正對照表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年 背書保證作業程序修正對照表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/103/103年開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/103/103年議事手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>103年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent2">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">102年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent2" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/102/102開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/102/102股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102年 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/102/102股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/102/102年報前十大股東相互關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/102/102事議手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/102/102資金貸與他人程序.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102年 資金貸與他人程序</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/102/102股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>102年 股東會年報</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                            <!-- group -->
                            <button class="c-accordion-btn" data-target="#accordionContent1">
                                <div class="c-accordion-btn-wrapper">
                                    <div class="c-accordion-btn-layout">
                                        <div class="c-accordion-btn-title">
                                            <p class="u-font-18">101年</p>
                                        </div>
                                        <div class="c-accordion-btn-icon">
                                            <i class="far fa-angle-down u-font-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </button>
                            <div id="accordionContent1" class="c-accordion-content">
                                <div class="c-accordion-content-wrapper">
                                    <div class="c-accordion-content-layout">
                                        <div class="row">
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/101/101開會通知.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101年 開會通知</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/101/101股東會議事錄.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101年 股東會議事錄</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/101/101股東會各項議案參考資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101年 股東會各項議案參考資料</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/101/101年報前十大股東相互關係表.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101年 年報前十大股東相互間關係表</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/101/101取得或處分資產處理程序(訂定或修訂).pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101年 取得或處分資產處理程序(訂定或修正)</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/101/101股東會年報.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101年 股東會年報</span>
                                                </a>
                                            </div>
                                            <div class="col-auto">
                                                <a href="download/pdf/investors/shareholder/101/101事議手冊及會議補充資料.pdf"
                                                    target="_blank"
                                                    class="c-btn c-btn--download c-btn-gray-800 w-100 px-2 my-2 my-2 d-flex align-items-center justify-content-center">
                                                    <img src="assets/img/financial/icon_pdf.svg" alt=""
                                                        class="u-mr-050">
                                                    <span>101年 議事手冊及會議補充資料</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End group -->
                        </div>
                    </div>
                </div>
        <!-- START 頁籤 -->
        <div class="u-py-100 u-pb-400">
            <div class="container">
                <!-- <nav>
                            <ul class="c-pagination justify-content-center align-items-center u-mb-000">
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_to_left"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_left"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item active"><a href="#">1</a></li>
                                <li class="c-pagination-item"><a href="#">2</a></li>
                                <li class="c-pagination-item"><a href="#">3</a></li>
                                <li class="c-pagination-item"><a href="#">4</a></li>
                                <li class="c-pagination-item"><a href="#">5</a></li>
                                <li class="c-pagination-item">...</li>
                                <li class="c-pagination-item"><a href="#">10</a></li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_right"></i>
                                    </a>
                                </li>
                                <li class="c-pagination-item">
                                    <a href="#">
                                        <i class="icon-arrow_to_right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>-->
            </div>
        </div>
        <!-- END 頁籤 -->
    </section>
</main>