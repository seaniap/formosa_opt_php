$(document).ready(function() {

    jQuery.ajaxSetup({
      async: false
    }); //if order matters
  
    $.get("ajax/_header.html", '', function(data) {
      $("#header").append(data);
    });
    $.get("ajax/_footer.html", '', function(data) {
      $("#footer").append(data);
    });
    jQuery.ajaxSetup({
      async: true
    }); //if order matters
  
  });

  $(document).ready(function(){
    var i = document.location.href.lastIndexOf("/");
    var currentHTML = document.location.href.substr(i+1);
    $("#mainmenu li a").removeClass('active');
    $("#mainmenu li a[href^='"+currentHTML+"']").addClass('active');
});

$("button.close").click(function(){
  $(".information").removeClass("showinto");
});

/* product menu btn */
$(".pro-menu-btn").click(function(){
  $(".pro-menu-btn").toggleClass("open");
  $(".frist-list").toggleClass("openmenu");
});
/* 錨點 */
// $('.pro-anchor ul li').click(function() {
//   $('.pro-anchor ul li').removeClass('active');
//   $(this).addClass('active');
// });
$('a[href *=#]:not([href =#])').click(function () {
  var target = $(this.hash);
  $('html, body').animate({
    scrollTop: target.offset().top
  }, 1000);
  return false;
});
