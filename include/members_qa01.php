<?php require_once('../Connections/MySQL.php'); ?>
<?php 
$template="membersqa01.php";
$templateS="membersqa01.php";
// 判斷是預設是A版還是S版
// 預設 A 版
?>
<!-- to be marked -->
<!DOCTYPE html>
<html lang="zh-tw">
<meta charset="UTF-8">
<!-- to be marked -->
<style>
        .formosaResetUl ul {
            list-style: disc;
            padding-left: 40px;
            }
        .formosaResetUl ul ul{
            list-style: circle;
        }
        .formosaResetUl ul ul ul {
            list-style-type: square;
        }
    </style>
<?php
if( $_GET[$templateS]=="" and $row_template['Extend']=="Y") {
    //A 版開始 demo page
    if( $_GET['demo']=='y'  ) { ?>  
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section class="u-py-200">
                <div class="container">
                    <h3 class='c-title-center u-mb-125'>會員專區</h3>
                    <div class="position-relative mb-3">
                        <!-- <button type="button" class="v-tabBtnSlick-prev"><i class="fal fa-angle-left"></i></button> -->
                        <div class="c-tab-verE v-tabBtnSlick">
                            <div class="c-tab-container"><a href="members-edit.html" class="c-tab-linkE">會員資料修改</a>
                            </div>
                            <div class="c-tab-container"><a href="members-point.html" class="c-tab-linkE">點數查詢</a></div>
                            <div class="c-tab-container"><a href="members-benefit.html" class="c-tab-linkE">會員權益</a>
                            </div>
                            <div class="c-tab-container"><a href="members-qa.html" class="c-tab-linkE active">會員Q&A</a>
                            </div>
                            <div class="c-tab-container"><a href="members-privacyPolicy.html"
                                    class="c-tab-linkE">隱私權條款</a></div>
                            <div class="c-tab-container"><a href="concept.html" class="c-tab-linkE">四大保固</a></div>
                        </div>
                        <!-- <button type="button" class="v-tabBtnSlick-next"><i class="fal fa-angle-right"></i></button> -->
                    </div>
                    <div class="l-form">
                        <div class="form-row justify-content-end u-pb-075">
                            <div class="col-md-6 u-pb-100">
                                <div class="form-row flex-nowrap align-items-center">
                                    <div class="col">
                                        <select name="select" id="" class="l-form-field l-form-select u-bg-gray-200">
                                            <option value="加入會員" selected >加入會員</option>                                        
                                            <option value="會員申請規則" >會員申請規則</option>
                                            <option value="會員點數規則" >會員點數規則</option>
                                            <option value="會員等級規則" >會員等級規則</option>
                                            <option value="會員退換貨作業" >會員退換貨作業</option>
                                            <option value="會員活動相關" >會員活動相關</option>
                                            <option value="寶島眼鏡APP操作問題" >寶島眼鏡APP操作問題</option>
                                            <option value="EYESmart配送相關" >EYESmart配送相關</option>
                                            <option value="寶島眼鏡EYE+Pay支付相關" >寶島眼鏡EYE+Pay支付相關</option>
                                            <option value="會員終止申請" >會員終止申請</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 u-pb-100">
                                <div class="form-row flex-nowrap align-items-center">
                                    <div class="col">
                                        <input name="search" type="text" value="請輸入關鍵字" class="l-form-field">
                                    </div>
                                    <div class="col-auto">
                                        <button type="button" class="c-btn c-btn--contained c-btn-blue-highlight"><i
                                                class="fal fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container u-pb-200">
                    <!-- group -->
                    <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent1">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">
                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q1</span><span
                                            class="c-accordion-btn-title-seperator"></span>加入EYE-CATCH實體會員？
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent1" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>當您填寫完EYE-CATCH會員申請表，並經手機回傳有效開卡簡訊， 即成為EYE-CATCH會員。EYE-CATCH
                                    會從申請表中獲得您的個人資訊，包括會員卡號、姓名、性別、生日、地址、E-mail、手機號碼。您填寫的資料愈詳細，我們愈能提供您所需求的相關訊息。成為會員後，即可充分使EYE-CATCH提供的所會員服務。
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn" data-target="#accordionContent2">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q2</span><span
                                            class="c-accordion-btn-title-seperator"></span>加入EYE-CATCH網路會員？
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent2" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>當您填寫完EYE-CATCH會員申請表，並經手機回傳有效開卡簡訊， 即成為EYE-CATCH會員。EYE-CATCH
                                    會從申請表中獲得您的個人資訊，包括會員卡號、姓名、性別、生日、地址、E-mail、手機號碼。您填寫的資料愈詳細，我們愈能提供您所需求的相關訊息。成為會員後，即可充分使EYE-CATCH提供的所會員服務。
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent3">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q3</span><span class="c-accordion-btn-title-seperator"></span>訂閱電子報？
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent3" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>訂閱電子報？</p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn" data-target="#accordionContent4">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q4</span><span
                                            class="c-accordion-btn-title-seperator"></span>凡購買鏡架及鏡片類產品，即可享四大保固及專業連鎖服務？
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent4" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>凡購買鏡架及鏡片類產品，即可享四大保固及專業連鎖服務？</p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent5">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q5</span><span
                                            class="c-accordion-btn-title-seperator"></span>EYE-CATCH隱私權條款？
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent5" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>EYE-CATCH隱私權條款？</p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn" data-target="#accordionContent6">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q6</span><span
                                            class="c-accordion-btn-title-seperator"></span>EYE-CATCH如何使用您的資料？
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent6" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>EYE-CATCH如何使用您的資料？</p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent7">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q7</span><span
                                            class="c-accordion-btn-title-seperator"></span>Cookies的運用政策?
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent7" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>Cookies的運用政策?</p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn" data-target="#accordionContent8">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q8</span><span
                                            class="c-accordion-btn-title-seperator"></span>EYE-CATCH如何保護您的資料？
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent8" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>EYE-CATCH如何保護您的資料？</p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn u-bg-gray-100" data-target="#accordionContent9">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q9</span><span class="c-accordion-btn-title-seperator"></span>Facebook
                                        隱私權條款？
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent9" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>Facebook 隱私權條款？</p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                    <!-- group -->
                    <button class="c-accordion-btn" data-target="#accordionContent10">
                        <div class="c-accordion-btn-wrapper">
                            <div class="c-accordion-btn-layout">

                                <div class="c-accordion-btn-title">
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q10</span><span
                                            class="c-accordion-btn-title-seperator"></span>測試會員QA標題文字內容增加到兩行字測試會員QA標題文字內容增加到兩行字測試會員QA標題文字內容增加到兩行字測試會員QA標題文字內容增加到兩行字測試會員QA標題文字內容增加到兩行字
                                    </p>
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent10" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p>測試會員QA標題文字內容增加到兩行字測試會員QA標題文字內容增加到兩行字測試會員QA標題文字內容增加到兩行字測試會員QA標題文字內容增加到兩行字測試會員QA標題文字內容增加到兩行字
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- End group -->
                </div>
                <nav>
                    <ul class="c-pagination justify-content-center align-items-center">
                        <li class="c-pagination-item  align-self-center">
                            <a href="#" class="py-1 px-2">
                                <i class="far fa-angle-left"></i>
                            </a>
                        </li>
                        <li class="c-pagination-item"><a href="#">1</a></li>
                        <li class="c-pagination-item"><a href="#">2</a></li>
                        <li class="c-pagination-item">...</li>
                        <li class="c-pagination-item"><a href="#">10</a></li>
                        <li class="c-pagination-item py-1 px-2 align-self-center">
                            <a href="#" class="py-1 px-2">
                                <i class="far fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
    <?php } else {  ?> 
    <?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_content01 = $row_template['MaxRow'];
$pageNum_content01 = 0;
if (isset($_GET['pageNum_content01'])) {
  $pageNum_content01 = $_GET['pageNum_content01'];
}
$startRow_content01 = $pageNum_content01 * $maxRows_content01;

$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ",$Table_ID, GetSQLValueString($colname_content01, "text"));
if($_GET['Cate01']<>""){
$query_content01=$query_content01."AND Cate01='".$_GET['Cate01']."'" ;}

if($_GET['Cate02']<>""){
$query_content01=$query_content01."AND Cate02='".$_GET['Cate02']."'" ;}

if($_GET['search']<>""){
$query_content01=$query_content01."AND (h01 like '%".$_GET['search']."%' or c01 like '%".$_GET['search']."%' ) " ;}		

		
		
		
		
//排序
$query_content01=$query_content01." order by Sq asc ";

$query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
//echo $query_limit_content01 ;
$content01 = mysqli_query($MySQL,$query_limit_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
if (isset($_GET['totalRows_content01'])) {
   $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
} else {
  $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);
}
$totalPages_content01 = ceil($totalRows_content01/$maxRows_content01)-1;

$queryString_content01 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_content01") == false && 
        stristr($param, "totalRows_content01") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
// A版開始 real page
?>
<!-- ============= 主要內容區 ============= -->
<main class="wrapper formosaResetUl">
    <section class="u-py-200">
        <div class="container">
            <h3 class='c-title-center u-mb-225'>會員專區</h3>
            <!--member sub menu -->
            <?php include('member_sub_menu.php'); ?>
            
            <!--end member sub menu -->  
            </div>        
                <div class="container u-pb-200">
                    <!-- qa group -->
                    <!--start loop -->
                    <?php 
	$n=0;	
		do {
	$n++;			?>
                    <!-- 3 button -->
                    
                    <button class="c-accordion-btn <?php if($n%2==1){  ?>u-bg-gray-100<?php } ?>" data-target="#accordionContent<?php echo $n; ?>">
                        <div class="c-accordion-btn-wrapper">
                        <?php include("3buttonA.php"); ?>
                            <div class="c-accordion-btn-layout">    
                                <div class="c-accordion-btn-title">             
                                    <p class="u-font-18">
                                        <span class="c-accordion-btn-title-number">Q<?php if($_GET['search']==""){echo $n;} ?></span><span
                                            class="c-accordion-btn-title-seperator"></span><?php echo $row_content01['h01']; ?> 
                                    </p>
                                    <!-- 3 button -->
                                    
                                </div>
                                <div class="c-accordion-btn-icon">
                                    <i class="far fa-angle-down u-font-20"></i>
                                </div>
                            </div>
                        </div>
                    </button>
                    <div id="accordionContent<?php echo $n; ?>" class="c-accordion-content">
                        <div class="c-accordion-content-wrapper u-pl-md-500">
                            <div class="c-accordion-content-layout">
                                <p><?php echo $row_content01['c01']; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php } while($row_content01 = mysqli_fetch_assoc($content01)); ?>
                    <!--end loop -->
                    <!-- End qa group -->
                     
                 </div>
                 <!-- paginations -->
                 <div class="u-py-100">
                    <div class="container">
                        <?php include('formosa_paginations.php'); ?>
                    </div>
                 </div>
                    <!--end paginations -->
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->
        <?php 
mysqli_free_result($content01);
 } 
//A 版結束 
}
else {  //S版開始 
    if( $_GET['demo']=='y'  ) { ?>
    <div>S版 demo</div>
    <?php } else {  ?>
<?php
$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$KeyID=str_replace($vowels ,"**!!!**",$_GET[$templateS]);
if($row_template['Extend']=='Y'){
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ",$Table_ID, GetSQLValueString($colname_content01, "text")); }
else {
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ",$Table_ID, GetSQLValueString($colname_content01, "text")); 	
}

$content01 = mysqli_query($MySQL,$query_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
$totalRows_content01 = mysqli_num_rows($content01);
//echo "S版啟動".$query_content01;
?>  
<div>S版real</div>
 
 <?php mysqli_free_result($content01);
 } 
}  //S版結束
?>