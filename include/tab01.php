<?php require_once '../Connections/MySQL.php';?>
<?php $template = "tab01.php";
$templateS = "tab01";
// 判斷是預設是A版還是S版
// 預設 A 版

if ($_GET[$templateS] == "" and $row_template['Extend'] == "Y") {
//A 版開始
    if ($_GET['demo'] == 'y') {?>

<p>TAB 01 A 版</p>

<?php } else {?>
<?php $currentPage = $_SERVER["PHP_SELF"];

        $maxRows_content01 = 7;
        $pageNum_content01 = 0;
        if (isset($_GET['pageNum_content01'])) {
            $pageNum_content01 = $_GET['pageNum_content01'];
        }
        $startRow_content01 = $pageNum_content01 * $maxRows_content01;

        $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s AND Template='$template' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        if ($_GET['Cate01'] != "") {
            $query_content01 = $query_content01 . "AND Cate01='" . $_GET['Cate01'] . "'";}

        if ($_GET['Cate02'] != "") {
            $query_content01 = $query_content01 . "AND Cate02='" . $_GET['Cate02'] . "'";}

//排序
        $query_content01 = $query_content01 . " order by Sq ";

        $query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
//echo $query_limit_content01 ;
        $content01 = mysqli_query($MySQL, $query_limit_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);

        $all_content01 = mysqli_query($MySQL, $query_content01);
        $totalRows_content01 = mysqli_num_rows($all_content01);

        $totalPages_content01 = ceil($totalRows_content01 / $maxRows_content01) - 1;

        $queryString_content01 = "";
        if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
                if (stristr($param, "pageNum_content01") == false &&
                    stristr($param, "totalRows_content01") == false &&
                    stristr($param, "Cate01") == false &&
                    stristr($param, "Cate02") == false &&
                    stristr($param, "Cate03") == false) {
                    array_push($newParams, $param);
                }
            }
            if (count($newParams) != 0) {
                $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
            }
        }
        $queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
        $queryString_content01 = $queryString_content01 . "&Cate01=" . $_GET['Cate01'] . "&Cate02=" . $_GET['Cate02'] . "&Cate03=" . $_GET['Cate03'];
        ?>


<main class="wrapper">
    <section>
        <div class="u-pt-100">
            <div class="container">
                <h3 class="c-title-center u-mb-125">最新消息</h3>
            </div>
        </div>
              
              
<?php if($_SESSION['Mode']!=""){ ?>              
                <!-- START 後台用搜尋bar -->
                <div class="container u-pt-100 u-pb-250">
            <div class="row justify-content-center">
                <div class="col-md-6">
                   <form name="" method="" action="">
                    <div class="form-row flex-nowrap align-items-center">
                        <div class="col">
                            <input name="search" type="text" placeholder="請輸入關鍵字" class="l-form-field" value="">
                        </div>
                        <div class="col-auto">
                            <button type="button" class="c-btn c-btn--contained c-btn-blue-highlight" style="
                                    width: 50px;" onclick="this.form.submit();"><i class="fal fa-search"></i></button>
                        </div>
                    </div>
                    <input type="hidden" name="Page" value="2"  />
                     </form>
                    
                    
                    
                </div>
            </div>
        </div>
        <!-- END 後台用搜尋bar -->
<?php } ?>        
        <!-- START TAB -->
        <div class="u-pb-100">
            <div class="container">
                <div class="position-relative">
                    <!-- <button type="button" class="v-tabBtnSlick-prev"><i class="fal fa-angle-left"></i></button> -->
                    <div class="c-tab-verE v-tabBtnSlick">

                        <?php do {?>
                        <?php if ($_SESSION['Mode'] == "Admin" or $_SESSION['Mode'] == "User"  or $row_content01['h01'] != "") {?>
                        <div class="c-tab-container"><a href="index.php?Page=2&h10=<?php echo $row_content01['h01']; ?>"
                                class="c-tab-linkE <?php if($_GET['h10']==$row_content01['h01']){ echo "active";} ?> "><?php echo $row_content01['h01']; ?></a>
                            <?php //include("3buttonA.php");
            SuperM_edit($row_content01, $template, $row_template['Security']);
            ?>
                        </div>
                        <?php }?>

                        <?php } while ($row_content01 = mysqli_fetch_assoc($content01));?>
                        <!--
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會員日</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會員日</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會員日</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會0505</a></div>
                                <div class="c-tab-container"><a href="" class="c-tab-linkE">寶島眼鏡會0606</a></div>
                                -->
                    </div>
                    <!-- <button type="button" class="v-tabBtnSlick-next"><i class="fal fa-angle-right"></i></button> -->
                </div>
            </div>
        </div>
    </section>
</main>


<?php mysqli_free_result($content01);
    }

//A 版結束
} else { //S版開始

    if ($_GET['demo'] == 'y') {?>

<p>TAB 01 S 版</p>

<?php } else {?>
<?php $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $maxRows_content01 = $row_template['MaxRow'];
        $KeyID = str_replace($vowels, "**!!!**", substr($_GET[$templateS], 0, 50));
        if ($row_template['Extend'] == 'Y') {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ", $Table_ID, GetSQLValueString($colname_content01, "text"));} else {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        }

        $content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);
        $totalRows_content01 = mysqli_num_rows($content01);
//echo "S版啟動".$query_content01;
        ?>



<div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <img src="std_images/kl_c02.png" width="699" height="12" />
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF">
                <div class="cont">
                    <div class="title"><span class="blue"><?php echo $Title; ?></span></div>
                    <div class="left">
                        <?php if ($row_content01['p01'] != "") {?>
                        <img src="UploadImages/<?php echo $row_content01['p01']; ?>" width="155"
                            title="<?php echo $row_content01['h01']; ?>" alt="<?php echo $row_content01['h01']; ?>" />
                        <?php }?>
                    </div>

                    <div class="news1">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><img src="std_images/kl_icon2.gif" width="15" height="18" />
                                    <span style="color:#03F;"> <?php echo $row_content01['h01']; ?> </span>
                                </td>
                                <td align="right" class="red">
                                    <?php if ($row_content01['d01'] != "") {
            echo "[" . substr($row_content01['d01'], 0, 10) . "]";}?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="borderline"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="normal">
                                    <?php echo $row_content01['c01']; ?>

                                    <?php if ($row_content01['h11'] != "") {
            $myyoutube = strstr($row_content01['h11'], "/watch?v=");
            $youtube = str_replace("/embed/", $myyoutube, "https://www.youtube.com/embed/?rel=0");
            $youtube = str_replace("/watch?v=", "/embed/", $youtube);
            ?>
                                    <iframe width="560" height="315" src="<?php echo $youtube; ?>" frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                    <?php }?>
                                    <?php if ($row_content01['h12'] != "") {
            $myyoutube = strstr($row_content01['h12'], "/watch?v=");
            $youtube = str_replace("/embed/", $myyoutube, "https://www.youtube.com/embed/?rel=0");
            $youtube = str_replace("/watch?v=", "/embed/", $youtube);
            ?>
                                    <iframe width="560" height="315" src="<?php echo $youtube; ?>" frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                    <?php }?>
                                    <?php if ($row_content01['h13'] != "") {
            $myyoutube = strstr($row_content01['h13'], "/watch?v=");
            $youtube = str_replace("/embed/", $myyoutube, "https://www.youtube.com/embed/?rel=0");
            $youtube = str_replace("/watch?v=", "/embed/", $youtube);
            ?>
                                    <iframe width="560" height="315" src="<?php echo $youtube; ?>" frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                    <?php }?>



                                    <?php if ($row_content01['f01'] != "") {
            $query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f01'], "text"));
            $content0f = mysqli_query($MySQL, $query_content0f) or die(mysqli_error());
            $row_content0f = mysqli_fetch_assoc($content0f);?>
                                    <a
                                        href="GetDBfile.php?KeyID=<?php echo $row_content01['f01']; ?> ">文件下載:<?php echo $row_content0f['filename']; ?></a><br>
                                    <?php }?>

                                    <?php if ($row_content01['f02'] != "") {
            $query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f02'], "text"));
            $content0f = mysqli_query($MySQL, $query_content0f) or die(mysqli_error());
            $row_content0f = mysqli_fetch_assoc($content0f);?>
                                    <a
                                        href="GetDBfile.php?KeyID=<?php echo $row_content01['f02']; ?> ">文件下載:<?php echo $row_content0f['filename']; ?></a><br>
                                    <?php }?>

                                    <?php if ($row_content01['f03'] != "") {
            $query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f03'], "text"));
            $content0f = mysqli_query($MySQL, $query_content0f) or die(mysqli_error());
            $row_content0f = mysqli_fetch_assoc($content0f);?>
                                    <a
                                        href="GetDBfile.php?KeyID=<?php echo $row_content01['f03']; ?> ">文件下載:<?php echo $row_content0f['filename']; ?></a><br>
                                    <?php }?>

                                    <?php //include("3button.php");
        SuperM_edit($row_content01, $template, $row_template['Security']);
        ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
            <td><img src="std_images/kl_c03.png" width="699" height="12" /></td>
        </tr>
    </table>


    <?php //上一篇 下一篇功能
        $query_sql = "SELECT * FROM GoWeb_BigTable where KeyID='$KeyID'";
//echo $query_sql;
        $data = mysqli_query($MySQL, $query_sql) or die(mysqli_error($MySQL));
        $row_data = mysqli_fetch_assoc($data);
        $Cate01 = $row_data['Cate01'];
        $h01 = $row_data['h01'];

        $query_sqlU = "SELECT * FROM GoWeb_BigTable where Template='" . $template . "' " . $Pname . " and d01>'" . $row_content01['d01'] . "' order by d01";
//echo $query_sqlU;
        $dataU = mysqli_query($MySQL, $query_sqlU) or die(mysqli_error($MySQL));
        $row_dataU = mysqli_fetch_assoc($dataU);

        $query_sqlD = "SELECT * FROM GoWeb_BigTable where Template='" . $template . "' " . $Pname . " and d01<'" . $row_content01['d01'] . "' order by d01 desc";
//echo $query_sqlD;
        $dataD = mysqli_query($MySQL, $query_sqlD) or die(mysqli_error($MySQL));
        $row_dataD = mysqli_fetch_assoc($dataD);
        ?>


    <ul class="pagination page_number">
        <?php if ($row_dataU['KeyID'] != "") {?>
        <li><a href="index.php?Page=<?php echo $_GET['Page']; ?>&<?php echo $templateS . "=" . $row_dataU['KeyID']; ?>"
                aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
        <?php }?>
        <?php if ($row_dataD['KeyID'] != "") {?>
        <li><a href="index.php?Page=<?php echo $_GET['Page']; ?>&<?php echo $templateS . "=" . $row_dataD['KeyID']; ?>"
                aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
        <?php }?>
    </ul>


</div>







<?php mysqli_free_result($content01);
    }
} //S版結束
?>