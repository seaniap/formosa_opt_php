<?php

namespace app\util;

include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";
?>

<!DOCTYPE html>
<html lang="zh-tw">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>會員登入 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
	<meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
	<meta name="description" content="提供寶島眼鏡會員登入.忘記密碼.立即開卡.立即註冊.瞭解狀態等相關功能。 ">
	<meta name="author" content="寶島眼鏡">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
	<meta property="og:image" content="assets/img/share_1200x630.jpg" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
	<link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
	<link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
	<link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
	<link rel="stylesheet" href="assets/css/main.css" />
	<link rel="stylesheet" href="assets/plugins/jquery-validation-1.19.3/css/screen.css" />
	<style>
		.swal2-popup{
			width: 37rem!important;
		}
		.swal2-styled.swal2-confirm{
			border:none;
			border-radius: 0;
			background-color:transparent;
		}
	</style>

	<!-- JS Global Compulsory -->
	<script src="assets/plugins/jquery@3.5.1.min.js"></script>
	<script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
	<script src="assets/plugins/slick@1.90/slick.min.js"></script>
	<script src="assets/plugins/lozad/lozad.min.js"></script>
	<script nomodule src="https://polyfill.io/v3/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
	<script src="assets/js/main.js"></script>

	<script src="assets/plugins/jquery-validation-1.19.3/dist/jquery.validate.min.js"></script>
	<script src="assets/plugins/jquery-validation-1.19.3/dist/localization/messages_zh_TW.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<script src="https://apis.google.com/js/platform.js"></script>
	<!-- JS Customization -->

	<script type="text/javascript">
		! function(q, g, r, a, p, h, js) {
			if (q.qg) return;
			js = q.qg = function() {
				js.callmethod ? js.callmethod.call(js, arguments) : js.queue.push(arguments);
			};
			js.queue = [];
			p = g.createElement(r);
			p.async = !0;
			p.src = a;
			h = g.getElementsByTagName(r)[0];
			h.parentNode.insertBefore(p, h);
		}(window, document, 'script', 'https://cdn.qgr.ph/qgraph.<?php echo Config::get("appier_id"); ?>.js');
	</script>

	<script type="text/javascript">
	 
		function popupShow(id){
			$("#"+id).attr("style", "");
		}

		function popupHide(id){
			$("#"+id).attr("style", "display: none !important");
		}
	 
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement(s);
			js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		window.fbAsyncInit = function() {
			FB.init({
				appId: '<?php echo Config::get("fb_app_id"); ?>',
				cookie: true,
				xfbml: true,
				version: 'v2.2'
			});

			FB.AppEvents.logPageView();

		};

		function fbLogin() {
			FB.login(function(response) {
				if (response.status === 'connected') {
					//已登入 Facebook，也登入應用程式
					FB.api('/me', {
						fields: 'name,email,picture.type(large)'
					}, function(response) {
						var id = response.id;
						console.log("id=" + id);
						login(id, "fb");

					});
				}
			}, {
				scope: 'public_profile,email'
			}); //選擇性 scope 參數是權用來向應用程式用戶要求權限

		}

		//for line login
		var win;
		window.addEventListener('message', function(e) {
			if (window.location.hostname != "localhost" && e.origin.indexOf(window.location.hostname) < 0) return;

			if (win) {
				win.close();
				win = null;

				var id = e.data;
				if (typeof id === "string") {
					if (id.indexOf("line") < 0) return;

					id = id.replace("line ","");

					if (id != "" && id != "error") {
						login(id, "line");

					}
				}
			}
		}, false);

		function lineLogin() {
			var dftWidth = 400;
			var dftHeight = 600;
			var w = window.screen.width / 2 - (dftWidth / 2);
			var t = window.screen.height / 2 - (dftHeight / 2);

			let client_id = '<?php echo Config::get("line_client_id"); ?>';
			let redirect_uri = '<?php echo Config::get("line_redirect_uri"); ?>';
			let link = 'https://access.line.me/oauth2/v2.1/authorize?';
			link += 'response_type=code';
			link += '&client_id=' + client_id;
			link += '&redirect_uri=' + redirect_uri;
			link += '&state=login';
			link += '&scope=openid%20profile';
			//window.location.href = link;

			win = window.open(link, '_blank', 'location=false,height=400,width=500,scrollbars=yes,status=yes,top=' + t + ",left=" + w);
		}

		function login(social_id, type) {
			var params = {
				"social_id": social_id,
				"type": type,
			};
			$.get("app/controller/Member.php?method=socialLogin", params, function(result) {
				result = JSON.parse(result);
				if (result.result == "1") {
					qg('login', {
						'user_id': result.m_id
					});
					qg('event', 'N_login');

					qg('getRecommendationByScenario', {
						    scenarioId: '<?php echo Config::get("scenarioId"); ?>',
							num: 7,
						},
						function(err, recommendationData) {

							$.get("app/controller/Member.php?method=updateRecommendationData", {
								data: recommendationData
							}, function(result) {

								window.location = "index.php";
							});
						}
					);

					window.location = "index.php";
				} else {
					if (type == "fb") {
						popupShow("popup_fb");
					} else if (type == "line") {
						popupShow("popup_line");
					} else {
						popupShow("popup_google");
					}
				}
			});
		}

		function googleLogin() {
			auth2.signIn().then(function() {
				if (auth2.isSignedIn.get()) {
					var profile = auth2.currentUser.get().getBasicProfile();
					console.log('ID: ' + profile.getId());

					login(profile.getId(), "google");
				}
			});
		}

		$(function() {
			
			
			window.alert = function(msg, icon = "info") {
				Swal.fire({
					text: "" + msg,
					icon: icon,
					confirmButtonColor: '#3085d6',
					confirmButtonText: '確定'
				})
			};

			window.showLoading = function(msg) {
				Swal.fire({
					title: '請稍待...',
					html: msg,
					allowEscapeKey: false,
					allowOutsideClick: false,
					didOpen: () => {
						Swal.showLoading()
					}
				});
			}

			gapi.load('auth2', function() {
				auth2 = gapi.auth2.init({
					client_id: '<?php echo Config::get("google_client_id"); ?>',
					fetch_basic_profile: true,
					scope: 'profile'
				});
			});

			$.validator.methods.reg = function(value, element, params) {
				return this.optional(element) || params.test(value);
			};


			$("#btnLogin").click(function() {
				if ($("#account").val() == "" || $("#password").val() == "") {
					popupShow("popup_data_not");
				} else {
					$("#form").submit();
				}
			});

			$("#form").validate({
				rules: {
					account: {
						required: true,
						//reg: /^([a-zA-Z0-9]+)$/,
						//rangelength: [6, 12],
					},

					password: {
						required: true,
						//reg: /^([a-zA-Z0-9]+)$/,
						//rangelength: [6, 12],
					},

				},
				messages: {
					account: "請輸入帳號",
					password: "請輸入帳號",
				},

				submitHandler: function(form) {

					var params = {
						"m_account": $("#account").val(),
						"m_passwd": $("#password").val(),
					};

					showLoading("登入中...");
					$.get("app/controller/Member.php?method=chkAccountPasswd", params, function(result) {
						result = JSON.parse(result);
						if (result && result.result == "1") {
							console.log(result);
							qg('login', {
								'user_id': result.m_id
							});
							qg('event', 'N_login');

							qg('getRecommendationByScenario', {
									scenarioId: '<?php echo Config::get("scenarioId"); ?>',
									num: 7,
								},
								function(err, recommendationData) {
									//recommendationData = [1, 2];
									$.post("app/controller/Member.php?method=updateRecommendationData", {
										data: recommendationData
									}, function(recommendationDataResult) {
										Swal.close();
										if (result.m_is_change_passwd == "Y") {
											window.location = "index.php?Page=B-1-1-4";
										} else {
											window.location = "index.php";
										}
									});


								}
							);
						} else {
							Swal.close();
							alert("帳號或密碼錯誤", "error");
						}
					});
				}
			});
		});
	</script>

<?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
	<noscript>
		<span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
			您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
		</span>
	</noscript>
	 
	 <div id="popup_data_not" class="p-pop_up_wrap d-flex align-items-center justify-content-center" style="display: none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="row p-pop_up justify-content-center">
                        <div class="col-lg-3 col-4 u-mb-125">
                            <img src="assets/img/members/icon23.svg" alt="">
                        </div>
                        <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
                            資料未填寫完成
                        </h3>
                        <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
                            請輸入帳號密碼後再按登入
                        </p>
                        <button onclick="popupHide('popup_data_not')" type="button" class="c-btn c-btn--contained c-btn-blue-highlight col-md-10 col-6">
                            我知道了
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
     <div id="popup_fb" class="p-pop_up_wrap d-flex align-items-center justify-content-center" style="display: none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="row p-pop_up justify-content-center">
                        <div class="col-lg-3 col-4 u-mb-125">
                            <img src="assets/img/members/facebook.svg" alt="">
                        </div>
                        <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
                            目前尚未綁定<br class="d-xl-none">Facebook
                        </h3>
                        <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
                            請登入後綁定，下次登入後即<br class="d-xl-none">可使用Facebook登入
                        </p>
                        <button onclick="popupHide('popup_fb')" type="button"
                            class="c-btn c-btn--contained c-btn-blue-highlight col-md-10 col-6">
                            我知道了
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="popup_google" class="p-pop_up_wrap d-flex align-items-center justify-content-center" style="display: none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="row p-pop_up justify-content-center">
                        <div class="col-lg-3 col-4 u-mb-125">
                            <img src="assets/img/members/google.svg" alt="">
                        </div>
                        <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
                            目前尚未綁定<br class="d-xl-none">Google
                        </h3>
                        <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
                            請登入後綁定，下次登入後即<br class="d-xl-none">可使用Google登入
                        </p>
                        <button  onclick="popupHide('popup_google')" type="button" class="c-btn c-btn--contained c-btn-blue-highlight col-md-10 col-6">
                            我知道了
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div   id="popup_line" class="p-pop_up_wrap d-flex align-items-center justify-content-center" style="display: none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="row p-pop_up justify-content-center">
                        <div class="col-lg-3 col-4 u-mb-125">
                            <img src="assets/img/members/line.svg" alt="">
                        </div>
                        <h3 class="col-12 u-text-blue-highlight u-font-weight-700 u-font-28 text-center u-mb-050">
                            目前尚未綁定<br class="d-xl-none">LINE
                        </h3>
                        <p class="col-12 u-text-gray-800 u-font-weight-700 u-font-18 text-center u-mb-250">
                            請登入後綁定，下次登入後即<br class="d-xl-none">可使用LINE登入
                        </p>
                        <button onclick="popupHide('popup_line')" type="button"  class="c-btn c-btn--contained c-btn-blue-highlight col-md-10 col-6">
                            我知道了
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
 
	
	<div id="page">
		<!-- ============= 導覽列 ============= -->
		<header class="l-header">
			<?php include('formosa_header.php') ?>
		</header>
		<div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
		<!-- =============end 導覽列 ============= -->
		<!-- ============= 主要內容區 ============= -->
		<main class="wrapper">
			<section>
				<div class="u-pt-250">
					<div class="container">
						<h3 class="c-title-center u-mb-125">會員登入</h3>
					</div>
				</div>
				<div class="u-pb-400">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-md-6 col-12">
								<form id="form" name="form">
									<div class="u-mb-150">
										<label class="d-flex align-items-center u-mb-075" for="required">
											<!-- <div class="p-titleIcon">
												<img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon4.svg" alt="">
											</div> -->
											<h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">帳號</h5>
										</label>
										<input type="text" id="account" name="account" class="l-form-field" placeholder="請輸入帳號">
									</div>

									<div class="u-mb-200">
										<label class="d-flex align-items-center u-mb-075" for="password">
											<!-- <div class="p-titleIcon">
												<img class="w-100 img-fluid u-pr-025 u-py-025" src="assets/img/members/icon5.svg" alt="">
											</div> -->
											<h5 class="u-text-gray-800 u-font-weight-700 u-font-12 u-sm-font-16 u-m-000">密碼</h5>
										</label>
										<input type="password" id="password" name="password" class="l-form-field" placeholder="請輸入密碼">
									</div>

									<div class="row">
										<div class="col-md-6 col-12 u-mb-100">
											<button type="button" onclick="location.href='formosa_members-forget-choseCountry.php'" class="c-btn c-btn--outlined c-btn-gray-800 col">
												忘記帳號/密碼
											</button>
										</div>
										<div class="col-md-6 col-12 u-mb-200">
											<button id="btnLogin" type="button" class="c-btn c-btn--contained c-btn-blue-highlight col">
												登入
											</button>

										</div>
									</div>
									<div class="u-mb-200">
										<p class="p-divider u-text-gray-500 u-font-weight-700 u-font-18">或</p>
									</div>
									<div class="u-mb-200">
										<p class="u-text-gray-800 u-font-weight-700 u-font-18 text-center">您的會員帳戶曾經有綁定才可由此登入</p>
									</div>

									<div class="row justify-content-center">
										<div class="col-10 col-sm-12">
											<div class="row align-items-start">
												<div class="col-4 px-3">
													<a class="text-center" href="javascript:fbLogin()">
														<div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
															<img src="assets/img/members/facebook.svg" alt="">
														</div>
														<p class="u-text-gray-800 u-mb-000">Facebook<br class="d-sm-none d-md-inline d-xl-none">登入</p>
													</a>
												</div>
												<div class="col-4 px-3">
													<a class="text-center" href="javascript:googleLogin()">
														<div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
															<img src="assets/img/members/google.svg" alt="">
														</div>
														<p class="u-text-gray-800 u-mb-000">Google<br class="d-sm-none d-md-inline d-xl-none">登入</p>
													</a>
												</div>
												<div class="col-4 px-3">
													<a class="text-center" href="javascript:lineLogin()">
														<div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
															<img src="assets/img/members/line.svg" alt="">
														</div>
														<p class="u-text-gray-800 u-mb-000">LINE<br class="d-sm-none d-md-inline d-xl-none">登入</p>
													</a>
												</div>
											</div>

										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>

		<!-- =============end 主要內容區 ============= -->
		<!-- ============= footer ============= -->
		<footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
		<!-- =============end footer ============= -->
	</div>
</body>

</html>