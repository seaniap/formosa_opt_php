<?php require_once('../Connections/MySQL.php'); ?>
<? 
$template="store01.php";
$templateS="store01";
// 判斷是預設是A版還是S版
// 預設 A 版

if( $_GET[$templateS]=="" and $row_template['Extend']=="Y") {
//A 版開始
if( $_GET['demo']=='y'  ) { ?>

  <div class="wrapper">
    <section class="container">
      <div class="d-title">
        <h3>經銷據點</h3>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <form action="">
            <label for="" class="form-title">尋找距離您最近的據點</label>
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <div class="select-box">
                    <select class="form-control" id="">
                      <option selected="" disabled="">請選擇類型</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <i></i>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <div class="select-box">
                    <select class="form-control" id="">
                      <option selected="" disabled="">請選擇類型</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <i></i>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button type="button" class="btn btn-bor-yellow-2 btn-block text-center px-1">查詢</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="default-table store-table">
            <table class="table table-rwd">
              <thead>
                <tr class="store-th">
                  <th><i class="icon-shop mr-1"></i>門市據點</th>
                  <th><i class="icon-location1 mr-1"></i>地址</th>
                  <th><i class="icon-time mr-1"></i>營業時間</th>
                  <th><i class="icon-message mr-1"></i>聯絡資訊</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                        一太電氣有限公司
                      </div>
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="#" target="_blank">
                          臺北市信義區林口街180號1樓
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                    <div class="table-rwd-content">
                      <div>08:00-20:00</div>
                      <div>(周日公休)</div>
                    </div>
                  </td>
                  <td data-th="聯絡資訊">
                    <div class="table-rwd-content">
                      <div>
                        <a href="tel:+886-2-12345678">
                          <i class="icon-phone mr-1"></i>(02)1234-5678
                        </a>
                      </div>
                      <div>
                        <a href="#" data-toggle="modal" data-target="#line-modal">
                          <i class="icon-line-bordered mr-1"></i>12345
                        </a>
                      </div>
                      <div>
                        <a href="mailto:etai168@hotmail.com">
                          <i class="icon-mail mr-1"></i>etai168@hotmail.com
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                        上友電器行
                      </div>
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="#" target="_blank">
                          臺北市士林區士東路234號
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                    <div class="table-rwd-content">
                      <div>08:00-20:00</div>
                      <div>(周日公休)</div>
                    </div>
                  </td>
                  <td data-th="Line & Email">
                    <div class="table-rwd-content">
                      <div>
                        <a href="tel:+886-2-12345678">
                          <i class="icon-phone mr-1"></i>(02)1234-5678
                        </a>
                      </div>
                      <!-- <div>
                        <a href="#" data-toggle="modal" data-target="#line-modal">
                          <i class="icon-line-bordered mr-1"></i>12345
                        </a>
                      </div> -->
                      <div>
                        <a href="mailto:etai168@hotmail.com">
                          <i class="icon-mail mr-1"></i>shangyou80@gmail.com
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                        千允電器有限公司
                      </div>
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="#" target="_blank">
                          臺北市士林區士東路234號
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                    <div class="table-rwd-content">
                      <div>08:00-20:00</div>
                      <div>(周日公休)</div>
                    </div>
                  </td>
                  <td data-th="Line & Email">
                    <div class="table-rwd-content">
                      <div>
                        <a href="tel:+886-2-12345678">
                          <i class="icon-phone mr-1"></i>(02)1234-5678
                        </a>
                      </div>
                      <div>
                        <a href="#" data-toggle="modal" data-target="#line-modal">
                          <i class="icon-line-bordered mr-1"></i>12345
                        </a>
                      </div>
                      <div>
                        <a href="mailto:etai168@hotmail.com">
                          <i class="icon-mail mr-1"></i>etai168@hotmail.com
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                        大翼科技工程股份有限公司
                      </div>
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="#" target="_blank">
                          臺北市士林區中山北路六段308號1樓
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                    <div class="table-rwd-content">
                      <div>10:00~22:00</div>
                      <div>(周日公休)</div>
                    </div>
                  </td>
                  <td data-th="Line & Email">
                    <div class="table-rwd-content">
                      <div>
                        <a href="tel:+886-2-12345678">
                          <i class="icon-phone mr-1"></i>(02)1234-5678
                        </a>
                      </div>
                      <!-- <div>
                        <a href="#" data-toggle="modal" data-target="#line-modal">
                          <i class="icon-line-bordered mr-1"></i>0983698998
                        </a>
                      </div> -->
                      <div>
                        <a href="mailto:etai168@hotmail.com">
                          <i class="icon-mail mr-1"></i>suntec308@gmail.com
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                        三泰電器(股)公司
                      </div>
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="#" target="_blank">
                          臺北市中山區八德路二段326號
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                    <div class="table-rwd-content">
                      <div>10:00~22:00</div>
                      <div>(周日公休)</div>
                    </div>
                  </td>
                  <td data-th="Line & Email">
                    <div class="table-rwd-content">
                      <div>
                        <a href="tel:+886-2-12345678">
                          <i class="icon-phone mr-1"></i>(02)1234-5678
                        </a>
                      </div>
                      <div>
                        <a href="#" data-toggle="modal" data-target="#line-modal">
                          <i class="icon-line-bordered mr-1"></i>karoter
                        </a>
                      </div>
                      <!-- <div>
                        <a href="mailto:etai168@hotmail.com">
                          <i class="icon-mail mr-1"></i>etai168@hotmail.com
                        </a>
                      </div> -->
                    </div>
                  </td>
                </tr>
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                        大揚電器行
                      </div>
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="#" target="_blank">
                          臺北市大安區通化街166號1樓
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                    <div class="table-rwd-content">
                      <div>08:00-20:00</div>
                      <div>(周日公休)</div>
                    </div>
                  </td>
                  <td data-th="Line & Email">
                    <div class="table-rwd-content">
                      <div>
                        <a href="tel:+886-2-12345678">
                          <i class="icon-phone mr-1"></i>(02)1234-5678
                        </a>
                      </div>
                      <div>
                        <a href="#" data-toggle="modal" data-target="#line-modal">
                          <i class="icon-line-bordered mr-1"></i>12345
                        </a>
                      </div>
                      <div>
                        <a href="mailto:etai168@hotmail.com">
                          <i class="icon-mail mr-1"></i>etai168@hotmail.com
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                        千雅電器有限公司
                      </div>
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="#" target="_blank">
                          臺北市北投區自強街5巷165號
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                    <div class="table-rwd-content">
                      <div>10:00~22:00</div>
                      <div>(周日公休)</div>
                    </div>
                  </td>
                  <td data-th="Line & Email">
                    <div class="table-rwd-content">
                      <!-- <div>
                        <a href="tel:+886-2-12345678">
                          <i class="icon-phone mr-1"></i>(02)1234-5678
                        </a>
                      </div> -->
                      <div>
                        <a href="#" data-toggle="modal" data-target="#line-modal">
                          <i class="icon-line-bordered mr-1"></i>12345
                        </a>
                      </div>
                      <div>
                        <a href="mailto:etai168@hotmail.com">
                          <i class="icon-mail mr-1"></i>111111111111@gmail.com
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
              <li><a href="#" class="active">1 </a></li>
              <li><a href="#">2 </a>
              </li>
              <li><a href="#">3 </a>
              </li>
              <li><a href="#">4 </a>
              </li>
              <li><a href="#"><i class="fas fa-arrow-right"></i></a></li>
            </ul>
          </nav>
        </div>
      </div>
    </section>
  </div>
  <footer id="footer"></footer>
  <!-- /container -->
  <!-- Modal -->
  <div id="line-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <div class="modal-body">
          <img src="assets/img/store/line-qrcode-demo.jpg" alt="">
        </div>
      </div>
    </div>
  </div>

<? } else {  ?>
<?
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_content01 = $row_template['MaxRow'];
$pageNum_content01 = 0;
if (isset($_GET['pageNum_content01'])) {
  $pageNum_content01 = $_GET['pageNum_content01'];
}
$startRow_content01 = $pageNum_content01 * $maxRows_content01;

$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = '5' AND Template='$template' ",$Table_ID);



if($_GET['Cate01']<>""){
$query_content01=$query_content01."AND Cate01='".$_GET['Cate01']."'" ;}

if($_GET['Cate02']<>""){
$query_content01=$query_content01."AND Cate02='".$_GET['Cate02']."'" ;}

//排序
$query_content01=$query_content01." order by CONVERT(`h01` using big5) asc ";

$query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
//echo $query_limit_content01 ;
$content01 = mysqli_query($MySQL,$query_limit_content01) or die(mysqli_error($MySQL));


  $all_content01 = mysqli_query($MySQL,$query_content01);
  $totalRows_content01 = mysqli_num_rows($all_content01);

$totalPages_content01 = ceil($totalRows_content01/$maxRows_content01)-1;

$queryString_content01 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_content01") == false && 
        stristr($param, "totalRows_content01") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);


//echo $totalRows_content01;


?>
<script language="javascript" src="../js/Zip.js"></script>
<script>

function getStore(area, zip) {

//alert('AAA');
  $.get("getStore031.php?area="+area+"&zip="+zip, function(data, status){
           
                   if(data=="ok"){
                           document.getElementById("msg01").innerHTML="可以使用";
                   }else{
                            document.getElementById("locaList").innerHTML=data;
                                     
                   }
                   
                    //alert("Data: " + data + "\nStatus: " + status);
        });

}


 $(function() {
		  
		  //getStore();
 	 
     	
     	$("#h10").change(function(){
     			getStore(this.value,"");  			
     			Buildkey(this.value);    
      })
      
      $("#h11").change(function(){
      	   getStore($("#h10").val(),this.value); 		
		  // alert('BBB');
      })
      
      
    
 })

//getStore("","");
</script>


 <div class="wrapper">
    <section class="container">
      <div class="d-title">
        <h3>經銷據點</h3>
      </div>
      <div class="row">
        <div class="col-lg-6">
       	 <form name="form1" id="form1" action="index.php?Page=5-2" method="POST" >
            <label for="" class="form-title">尋找距離您最近的據點</label>
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <div class="select-box">
       <select name="h10"  class="form-control" size="1" id="h10" >
      <option value="">請選擇縣市...</option>
      <option value="臺北市" >臺北市</option>
      <option value="基隆市" >基隆市</option>
      <option value="新北市" >新北市</option>
      <option value="宜蘭縣" >宜蘭縣</option>
      <option value="新竹縣" >新竹縣</option>
      <option value="新竹市" >新竹市</option>
      <option value="桃園市" >桃園市</option>
      <option value="苗栗縣" >苗栗縣</option>
      <option value="臺中市" >臺中市</option>
      <option value="彰化縣" >彰化縣</option>
      <option value="南投縣" >南投縣</option>
      <option value="嘉義縣" >嘉義縣</option>
      <option value="嘉義市" >嘉義市</option>
      <option value="雲林縣" >雲林縣</option>
      <option value="臺南市" >臺南市</option>
      <option value="高雄市" >高雄市</option>
      <option value="屏東縣" >屏東縣</option>
      <option value="臺東縣" >臺東縣</option>
      <option value="花蓮縣" >花蓮縣</option>
      <option value="澎湖縣" >澎湖縣</option>
      <option value="金門縣" >金門縣</option>
    </select>
                    <i></i>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <div class="select-box">
 <select class="form-control" name="h11" id="h11" size="1"    >
<option value="">請選擇區域..</option>
</select>
                    <i></i>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
<!--                
       <button type="button" class="btn btn-bor-yellow-2 btn-block text-center px-1">查詢</button> -->
                </div>
              </div>
            </div>
            	<input type="hidden" name="Page" value="5-2">     
          </form>
        </div>
      </div>
<?php
//$query_content01 = sprintf("SELECT * FROM GoWeb_BigTable WHERE Page = '5' AND Template='store01.php' order by CONVERT(`h01` using big5) asc limit 0,10 ");
//$content01 = mysqli_query($MySQL,$query_content01) or die(mysqli_error($MySQL));
//$row_content01 = mysqli_fetch_assoc($content01);
?>       
      <div class="row" id="locaList">
<div class="col-12">
          <div class="default-table store-table">
            <table class="table table-rwd">
              <thead>
                <tr class="store-th">
                  <th><i class="icon-shop mr-1"></i>門市據點</th>
                  <th><i class="icon-location1 mr-1"></i>地址</th>
                  <th><i class="icon-time mr-1"></i>營業時間</th>
                  <th><i class="icon-message mr-1"></i>聯絡資訊</th>
                </tr>
              </thead>
              <tbody>              
   <?php while($row_content01 = mysqli_fetch_assoc($content01)){ ?>                 
                <tr>
                  <td data-th="門市據點">
                    <div class="table-rwd-content">
                      <div>
                       <?php echo $row_content01['h01']; ?>   
                      </div><?php   include("3buttonA.php");  ?>   
                    </div>
                  </td>
                  <td data-th="地址">
                    <div class="table-rwd-content">
                      <div>
                        <a href="https://www.google.com.tw/maps/place/<?php echo $row_content01['h10'].$row_content01['h11'].$row_content01['h12']; ?>" target="_blank">
                        <?php echo $row_content01['h10'].$row_content01['h11'].$row_content01['h12']; ?>
                          <img src="assets/img/store/googleMap.svg" alt="" class="map-icon">
                        </a>
                      </div>
                    </div>
                  </td>
                  <td data-th="營業時間">
                  <!--  <div class="table-rwd-content">
                      <div>08:00-20:00</div>
                      <div>(周日公休)</div>
                    </div>
                    -->
                     <?php echo str_replace("(","<br>(",$row_content01['h04']); ?>
                  </td>
                  <td data-th="聯絡資訊">
                    <div class="table-rwd-content">
                      <?php if($row_content01['h03']<>"" and $row_content01['h02']=="1" ){ ?>
                      <div>
                        <a href="tel:+886-<?php echo $row_content01['h03']; ?>">
                          <i class="icon-phone mr-1"></i> <?php echo $row_content01['h03']; ?>
                        </a>
                      </div>
                      <?php }　 ?>
                     <?php if($row_content01['h06']<>""){ ?> 
                      <div>
                        <a  data-toggle="modal" data-target="#line-modal" onclick="document.getElementById('linepic').src='UploadImages/<? echo $row_content01['p01'];  ?>'">
                          <i class="icon-line-bordered mr-1"></i><?php echo $row_content01['h06']; ?>
                        </a>
                      </div>
                      <?php }　 ?>
                     <?php if($row_content01['h05']<>""){ ?>
                      <div>
                        <a href="mailto:<?php echo $row_content01['h05']; ?>">
                          <i class="icon-mail mr-1"></i> <?php echo $row_content01['h05']; ?>
                        </a>
                      </div>
                      <?php }　 ?>
                    </div>
                  </td>
                </tr>
                
    <?php } ?>            
                
              
              </tbody>
            </table>
          </div>
        
          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
             
             <?php if ($pageNum_content01 > 0) { // Show if not first page ?>
             <li><a href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, max(0, $pageNum_content01 - 1), $queryString_content01); ?>"><i class="fas fa-arrow-left"></i></a></li> <?php } // Show if not first page ?>
             
              
              <?php if($totalPages_content01>0) { // 如果總頁數大於1	
	for($i=-4;$i<=5;$i++) {   
	
	     if(($_GET['pageNum_content01']+$i)>=0 and ($_GET['pageNum_content01']+$i)<= $totalPages_content01)
    {
	?>
              <li><a href="<?php printf("%s?pageNum_content01=%d%s", $currentPage,$_GET['pageNum_content01']+$i , $queryString_content01); ?>" class="<?php if($i==0){ ?>active<?php } ?>"><? printf("%d ",$_GET['pageNum_content01']+$i+1); ?> </a></li>
               <?php
		   }
		  } 
		} ?>	
              
              
              <?php if ($pageNum_content01 < $totalPages_content01) { // Show if not last page ?>
              <li><a href="<?php printf("%s?pageNum_content01=%d%s", $currentPage, min($totalPages_content01, $pageNum_content01 + 1), $queryString_content01); ?>"><i class="fas fa-arrow-right"></i></a></li>
                <?php } // Show if not last page ?>
            </ul>
          </nav>
         
        </div>

           
      </div>
    </section>
  </div>
  <footer id="footer"></footer>
  <!-- /container -->
  <!-- Modal -->
  <div id="line-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <div class="modal-body">
          <img id='linepic' src="UploadImages/<? echo $row_content01['p01'];  ?>" alt="">
        </div>
      </div>
    </div>
  </div>    



<? 
 } 
 
//A 版結束 
} 
else {  //S版開始
 
if( $_GET['demo']=='y'  ) { ?>

<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="std_images/kl_c02.png" width="699" height="12" /></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">
    <div class="cont">
    <div class="title"><span class="blue">最新消息</span></div>
    <div class="left"><img src="std_images/kl_image02.jpg" width="155" height="138" /></div><div class="news">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="std_images/kl_icon2.gif" width="15" height="18" /> 我的E政府</td>
          <td align="right" class="red">[2011/05/05]</td>
        </tr>
        <tr>
          <td colspan="2"><div class="borderline"></div></td>
          </tr>
        <tr>
          <td colspan="2" class="normal">我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府 我的E政府</td>
        </tr>
        </table>
    </div></div></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td><img src="std_images/kl_c03.png" width="699" height="12" /></td>
  </tr>
</table>

</div>

<? } else {  ?>
<?
$colname_content01 = "-1";
if (isset($_GET['Page'])) {
  $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
}

$maxRows_content01 = $row_template['MaxRow'];
$KeyID=str_replace($vowels ,"**!!!**",$_GET[$templateS]);
if($row_template['Extend']=='Y'){
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ",$Table_ID, GetSQLValueString($colname_content01, "text")); }
else {
$query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ",$Table_ID, GetSQLValueString($colname_content01, "text")); 	
}

$content01 = mysqli_query($MySQL,$query_content01) or die(mysqli_error($MySQL));
$row_content01 = mysqli_fetch_assoc($content01);
$totalRows_content01 = mysqli_num_rows($content01);
//echo "S版啟動".$query_content01;
?>



<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <img src="std_images/kl_c02.png" width="699" height="12" />
    </td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">
    <div class="cont">
    <div class="title"><span class="blue"><? echo $Title; ?></span></div>
    <div class="left">
    <? if($row_content01['p01']<>"") { ?>
   <img src="UploadImages/<? echo $row_content01['p01'];  ?>" width="155"  title="<? echo $row_content01['h01'];  ?>" alt="<? echo $row_content01['h01'];  ?>"/>
    <?  }  ?>
    </div>
    
    <div class="news1">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="std_images/kl_icon2.gif" width="15" height="18" /> 
		  <span style="color:#03F;"> <? echo $row_content01['h01'];  ?> </span>
          </td>
          <td align="right" class="red">
          <? if($row_content01['d01']<>"") {
           echo "[".substr($row_content01['d01'],0,10)."]";  }  ?>
          </td>
        </tr>
        <tr>
          <td colspan="2"><div class="borderline"></div></td>
          </tr>
        <tr>
          <td colspan="2" class="normal">
		  <? echo $row_content01['c01'];  ?> 
          
                   <? if($row_content01['f01']<>""){ 
$query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f01'], "text"));
$content0f = mysqli_query($MySQL,$query_content0f) or die(mysqli_error());
$row_content0f = mysqli_fetch_assoc($content0f);	?>	 		 
		 <a href="GetDBfile.php?KeyID=<? echo $row_content01['f01'];  ?> ">文件下載:<? echo $row_content0f['filename']; ?></a><br>
          <? } ?>
     
          <? if($row_content01['f02']<>""){ 
$query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f02'], "text"));
$content0f = mysqli_query($MySQL,$query_content0f) or die(mysqli_error());
$row_content0f = mysqli_fetch_assoc($content0f);	?>	 		 
		 <a href="GetDBfile.php?KeyID=<? echo $row_content01['f02'];  ?> ">文件下載:<? echo $row_content0f['filename']; ?></a><br>
          <? } ?>
          
           <? if($row_content01['f03']<>""){ 
$query_content0f = sprintf("SELECT * FROM GoWeb_FileDB WHERE KeyID = %s ", GetSQLValueString($row_content01['f03'], "text"));
$content0f = mysqli_query($MySQL,$query_content0f ) or die(mysqli_error());
$row_content0f = mysqli_fetch_assoc($content0f);	?>	 		 
		 <a href="GetDBfile.php?KeyID=<? echo $row_content01['f03'];  ?> ">文件下載:<? echo $row_content0f['filename']; ?></a><br>
          <? } ?>
          
          <?  include("3button.php"); ?>
          </td>
        </tr>
        </table>
    </div></div></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td><img src="std_images/kl_c03.png" width="699" height="12" /></td>
  </tr>
</table>


 <?
$query_sql = "SELECT * FROM GoWeb_BigTable where KeyID='$KeyID'";
//echo $query_sql;
$data = mysqli_query($MySQL,$query_sql) or die(mysqli_error($MySQL));
$row_data = mysqli_fetch_assoc($data);
$Cate01=$row_data['Cate01'];
$h01=$row_data['h01'];

$query_sqlU = "SELECT * FROM GoWeb_BigTable where Template='".$template."' ".$Pname." and d01>'".$row_content01['d01']."' order by d01";
//echo $query_sqlU;
$dataU = mysqli_query($MySQL,$query_sqlU) or die(mysqli_error($MySQL));
$row_dataU = mysqli_fetch_assoc($dataU);


$query_sqlD = "SELECT * FROM GoWeb_BigTable where Template='".$template."' ".$Pname." and d01<'".$row_content01['d01']."' order by d01 desc";
//echo $query_sqlD;
$dataD = mysqli_query($MySQL,$query_sqlD) or die(mysqli_error($MySQL));
$row_dataD = mysqli_fetch_assoc($dataD);
?>            
 
  
  <ul class="pagination page_number">
   <? if($row_dataU['KeyID']<>"") { ?>
    <li><a href="index.php?Page=<? echo $_GET['Page']; ?>&<? echo $templateS."=".$row_dataU['KeyID'];?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
  <? } ?>  
   <? if($row_dataD['KeyID']<>"") { ?>
    <li><a href="index.php?Page=<? echo $_GET['Page']; ?>&<? echo $templateS."=".$row_dataD['KeyID'];?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
  <? } ?>    
  </ul>


</div>







<? mysqli_free_result($content01);
 } 
}  //S版結束
 ?>