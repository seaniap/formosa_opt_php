<script type="text/javascript"> 
	$(function(){
		$("#link_store_icon").click(function(){
			qg('event', "N_icon_clicked", {"clicled_name": "門市查詢"});
		});

		$("#link_tag_id").click(function(){
			qg('event', "N_icon_clicked", {"clicled_name": "行動配送"});
		});

		$("#link_15_icon").click(function(){
			qg('event', "N_icon_clicked", {"clicled_name": "人氣推薦"});
		});

		$("#link_05_icon").click(function(){
			qg('event', "N_icon_clicked", {"clicled_name": "助聽器預約"});
		});

		$("#link_member_icon").click(function(){
			qg('event', "N_icon_clicked", {"clicled_name": "會員登入"});
		});

		$("#link_app_icon").click(function(){
			qg('event', "N_icon_clicked", {"clicled_name": "APP下載"});
		});
	})
</script>

<section id="link">
    <div class="container px-0 px-md-3">
        <ul class="d-flex flex-wrap flex-lg-nowrap justify-content-between w-100 mb-0 u-py-025 u-py-md-150">
            <li class="col-3 col-lg-auto u-py-025 rounded">
                <a id="link_store_icon"  href="index.php?Page=8" class="p-index-link-link">
                    <img src="assets/img/index/icons/01.svg" alt="">
                    <span>門市查詢</span>
                </a>
            </li>
            <li class="col-3 col-lg-auto u-py-025 rounded">
                <a href="formosa_products.php?c=206" class="p-index-link-link">
                    <img src="assets/img/index/icons/02.svg" alt="">
                    <span>商品專區</span>
                </a>
            </li>
            <li class="col-3 col-lg-auto u-py-025 rounded">
                <a  id="link_tag_id" href="javascript:void(0)" class="p-index-link-link">
                    <img src="assets/img/index/icons/03.svg" alt="">
                    <span>行動配送</span>
                </a>
            </li>
            <!-- <li class="col-3 col-lg-auto u-py-025 rounded">
							<a href="" class="p-index-link-link">
								<img src="assets/img/index/icons/04.svg" alt="">
								<span>寶島眼科</span>
							</a>
						</li> -->
            <li class="col-3 col-lg-auto u-py-025 rounded">
                <a id="link_15_icon" href="index.php?Page=4" class="p-index-link-link">
                    <img src="assets/img/index/icons/15.svg" alt="">
                    <span>人氣推薦</span>
                </a>
            </li>
            <li class="col-3 col-lg-auto u-py-025 rounded">
                <a id="link_05_icon" href="https://www.formosa-optical.com.tw/include/index.php?Page=3-5" class="p-index-link-link" target="_blank">
                    <img src="assets/img/index/icons/05.svg" alt="">
                    <span>助聽器預約</span>
                </a>
            </li>
            <li class="col-3 col-lg-auto u-py-025 rounded">
                <a href="#video" class="p-index-link-link js-goToAnchor">
                    <img src="assets/img/index/icons/06.svg" alt="">
                    <span>精選影片</span>
                </a>
            </li>
            <li class="col-3 col-lg-auto u-py-025 rounded">
                <a id="link_member_icon" href="index.php?Page=B-1" class="p-index-link-link">
                    <img src="assets/img/index/icons/07.svg" alt="">
                    <span>會員登入</span>
                </a>
            </li>
            <li class="col-3 col-lg-auto u-py-025 rounded">
                <a id="link_app_icon" href="index.php?Page=B-7#appId" class="p-index-link-link">
                    <img src="assets/img/index/icons/08.svg" alt="">
                    <span>APP下載</span>
                </a>
            </li>
        </ul>
    </div>
</section>
<!-- <section id="clinic" class="u-bg-gray-02 u-pb-100 u-pt-100 u-pt-xl-300">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 d-flex flex-column justify-content-between">
                <h3 class="c-title-center c-title-lg-left u-pt-100 u-pt-xl-200">寶島眼科</h3>
                <p class="u-font-weight-700 d-none d-lg-block u-mb-050">全新登場<br><span class="u-font-weight-400">為您提供全方位的眼科醫療服務</span>
                </p>
                <div class="u-pb-200 d-none d-lg-block text-right">
                    <a href="" class="u-link-blue-500 u-link__hover--blue-highlight">More <i class="far fa-angle-right u-mr-025 u-font-14"></i></a>
                </div>
            </div>
            <div class="col-lg-9">
                <img src="assets/img/index/clinic.jpg" alt="" class="img-fluid u-my-150 u-my-lg-000">
                <p class="u-font-weight-700 d-lg-none text-center u-mb-050">全新登場<br><span class="u-font-weight-400">為您提供全方位的眼科醫療服務</span></p>
                <div class="d-lg-none text-right">
                    <a href="" class="u-link-blue-500 u-link__hover--blue-highlight">More <i class="far fa-angle-right u-mr-025 u-font-14"></i></a>
                </div>
            </div>
        </div>
    </div>
</section> -->