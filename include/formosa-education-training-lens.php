
        <main class="wrapper">
            <section class="p-knowledge">
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">鏡片</h3>
                    </div>
                </div>
                <!-- banner -->
                <div class="u-py-100 u-mb-200">
                    <div class="container">
                        <img src="./assets/img/education/education_27.jpg" alt="" class="w-100 img-fluid">
                    </div>
                </div>
                <!-- end banner -->
                <div class="u-pb-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">認識鏡片</h4>
                        <div class="row">
                            <!-- <div class="col-md-6 col-lg-5 col-xl-4 u-pb-100 u-pb-md-000">
                                <img src="assets/img/education/education_11.jpg" alt="" class="img-fluid">
                            </div> <div class="col-md-6 col-lg-7 col-xl-8">-->
                            <div class="col-md-12">
                                <ol class="u-list-style--decimal u-mb-000">
                                    <li class="font-weight-bold">凹透鏡</li>
                                    <ul class="u-mb-100">
                                        <li>對光有發散光線的功能，用於矯正近視，外型中間薄邊緣厚成像因此較小，視野較廣分為雙凹、平凹及凸凹透鏡三種。
                                        </li>
                                    </ul>
                                    <li class="font-weight-bold">凸透鏡</li>
                                    <ul class="u-mb-100">
                                        <li>對光線有聚合的功能，用於矯正遠視以及老花，外型中間厚邊緣薄，分為雙凸、平凸及凹凸透鏡三種。
                                        </li>
                                    </ul>
                                    <li class="font-weight-bold">柱面透鏡</li>
                                    <ul class="u-mb-100">
                                        <li>對光線有單一方向放大的功能，用於矯正散光，外型為長條柱狀，中心可為厚可為薄，分為平凸柱面鏡、平凹柱面鏡、雙凹柱面鏡，彎月柱面鏡。
                                        </li>
                                    </ul>
                                    <li class="font-weight-bold">折射率</li>
                                    <ul>
                                        <li>在物理上的定義是：
                                            某介質的折射率( n)亦等於光在真空中的速度(c)跟光在介質中的相速度(v)之比
                                            折射率越高，同度數相較鏡片越薄，密度較高
                                            折射率越低，同度數相較鏡片越厚，質地折射率1.56較折射率1.61的鏡片容易脆裂，不建議無邊框及半邊框使用
                                        </li>
                                    </ul>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">認識鏡片種類</h4>
                        <p>大約可分為：「玻璃鏡片」、「樹脂鏡片」、「太空PC鏡片」其特點比較如下：</p>
                        <table class="table table-bordered l-table u-mb-000">
                            <thead>
                                <tr>
                                    <th class="align-middle">鏡片種類</th>
                                    <th class="align-middle">玻璃鏡片</th>
                                    <th class="align-middle">樹酯鏡片</th>
                                    <th class="align-middle">PC鏡片</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <th class="u-bg-blue-highlight u-text-white u-bdr-top-white align-middle">硬度</th>
                                    <td class="align-middle">高，不容易刮傷</td>
                                    <td class="align-middle">低，容易刮</td>
                                    <td class="align-middle">表面加防刮硬膜</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-bg-blue-highlight u-text-white u-bdr-top-white align-middle">價位</th>
                                    <td class="align-middle">便宜</td>
                                    <td class="align-middle">中等</td>
                                    <td class="align-middle">高</td>
                                </tr>
                                <tr>
                                    <th class="u-bg-blue-highlight u-text-white u-bdr-top-white align-middle">安全性</th>
                                    <td class="align-middle">破碎時易傷害眼睛</td>
                                    <td class="align-middle">耐撞擊力，可抵鏡片破碎時對眼睛的衝擊</td>
                                    <td class="align-middle">比樹脂鏡片強十倍</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-bg-blue-highlight u-text-white u-bdr-top-white align-middle">重量</th>
                                    <td class="align-middle">重</td>
                                    <td class="align-middle">玻璃的1/2</td>
                                    <td class="align-middle">比玻璃鏡片輕58%</td>
                                </tr>
                                <tr>
                                    <th class="u-bg-blue-highlight u-text-white u-bdr-top-white align-middle">鏡片厚度</th>
                                    <td class="align-middle">薄</td>
                                    <td class="align-middle">比玻璃鏡片厚</td>
                                    <td class="align-middle">比樹脂鏡片薄26%</td>
                                </tr>
                                <tr class="u-bg-gray-100">
                                    <th class="u-bg-blue-highlight u-text-white u-bdr-top-white">適用者</th>
                                    <td class="align-middle">一般成年人或高度近視者</td>
                                    <td class="align-middle">適合學生或一般喜愛運動者</td>
                                    <td class="align-middle">各階層人士均適合</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">鏡片厚度</h4>
                        <p class="u-mb-000">為減輕負擔及提升外觀美感，廠商以更高折射率的材質製作鏡片，统稱為"超薄鏡片"，一般分為1.56巧薄，1.61超薄，1.67超超薄，1.74極超薄等數種，其中1.56巧薄因同度數邊緣厚度較厚，影響美觀以及鏡片維護，一般大品牌已不建議使用，較高度數者建議選擇1.61折射率以上的鏡片較佳。</p>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">鏡片設計</h4>
                        <p class="u-mb-000">傳統鏡片以球面方式研磨，高度數時會有明顯的週邊影像失真，稱為"球面像差"，為提升光學品質，由中心向外以不同弧度研磨鏡片，稱為"非球面鏡片"，由其研磨位置可分為單面非球面，內面非球面及雙面非球面等，其中，雙面非球面擁有最佳的光學品質。</p>
                    </div>
                </div>
                <div class="u-pt-100 u-pb-400">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">表面處理</h4>
                        <p class="u-mb-000">鏡片研磨完成後，表面清澈光亮，可是卻會有嚴重反光，一般先作耐磨處理，再以特殊物質塗佈其上，利用其干射及繞射原理，有效的減少反光，稱為"多層鍍膜"，依其所用的原料不同，表面會有輕微的不同顏色反光，常見的是綠色膜，其他尚有紫膜，藍膜及黄金膜等等，除減少反光外，一般鍍膜同時具有防水及防霧等功能，而為了更有效提升鍍膜的防污性，最新的方式是將塗佈物質先奈米化處理再鍍於鏡片上，稱為"奈米鍍膜"，不同品牌又有不同如"蓮花"，"鑽潔"，"超潔淨"等不同的名稱。 奈米鍍膜可以使鏡片有更好的防水及防污功能，更光滑的表面可減少灰塵沾附，有效減少鏡面的刮損。</p>
                    </div>
                </div>
            </section>
        </main>
      