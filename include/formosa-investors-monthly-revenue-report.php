<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class="c-title-center u-mb-125">財務資訊</h3>
            </div>
        </div>
        <!-- START TAB -->
        <div class="u-pb-100">
            <div class="container">
                <div class="d-md-flex d-none c-tab-verF row justify-content-center">
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-1" class="c-tab-linkF">財務報告</a>
                    </div>
                    <div class="c-tab-container">
                        <a href="index.php?Page=6-2" class="c-tab-linkF active">每月營業額報告</a>
                    </div>
                </div>
                <div class="row justify-content-center d-block d-md-none">
                    <div class="col-12">
                        <select name="select" id="" class="l-form-field l-form-select u-bg-gray-200 js-selectURL">
                            <option value="index.php?Page=6-1">財務報告</option>
                            <option value="index.php?Page=6-2" selected="" disabled="">每月營業額報告</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- END TAB -->
        <div class="u-pb-100">
            <div class="container">
                <p class="u-mb-000 u-text-blue-500 u-font-weight-900 u-font-22 u-md-font-28">每月營業額報告</p>
                <!-- <p class="u-mb-000">本公司依照證交法所訂資格條件選任獨立董事之相關訊息</p> -->
            </div>
        </div>
        <div class="u-pb-300">
            <div class="container">
                <h4 class="c-title-underline u-font-16 u-md-font-22">一、相關連結</h4>
                <div class="col-md-3 col-lg-2 u-p-000">
                    <a href="http://mops.twse.com.tw/mops/web/index" target="_blank" class="c-btn c-btn--contained c-btn-gray-200 u-text-black border border-secondary u-mb-100">公開資訊觀測站</a>
                </div>
                <p class="u-mb-000">營運概況 \ 每月營收 \ 採用IFRS後之月營業收入資訊<br>公司代號 5312</p>
            </div>
        </div>
    </section>
</main>