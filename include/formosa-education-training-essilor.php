        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section class="p-knowledge">
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">法國依視路WAM700+全方位視覺檢測系統</h3>
                    </div>
                </div>
                <div class="u-py-100">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">專業提升儀器升級</h4>
                        <div class="row">
                            <div class="col-lg-6 mb-3 mb-lg-0">
                                <p>相較於以往配鏡體驗只能告訴消費者的是，配鏡度數與視力值多少，欠缺視覺照護與預防保健的概念，高階儀器設備導入帶給您不同於以往的配鏡體驗，透過數位即時影像、眼數據分析，讓您更深入了解個人眼生理與眼機能狀況，提供相關衛教訊息及事先的預防措施，提供個人化視覺照護需求的配鏡解決方案，如發現異常現象則轉介眼科醫師做進一步檢查。</p>
                                <p class="u-mb-000">
                                    從問診過程詳細了解個人需求及視覺症狀並記錄完整資料，透過法國依視路WAM700+全方位視覺檢測系統，在驗光之前先進行重要的眼健康篩檢，經驗光人員解析後能預先了解眼睛現狀，再進行自覺式驗光流程，最後彙整檢查數據進行演算，分析推薦適合的鏡片或隱形眼鏡，符合消費者在工作及生活上用眼所需的配鏡方案。
                                </p>
                            </div>
                            <div class="col-lg-6">
                                <div class="d-flex">
                                    <div class="mx-3">
                                        <img src="assets/img/education/education_30.jpg" alt="" class="img-fluid">
                                        <p>WAM700+側面照</p>
                                    </div>
                                    <div class="mx-3">
                                        <img src="assets/img/education/education_31.jpg" alt="" class="img-fluid">
                                        <p>分析總表畫面</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-pb-300">
                    <div class="container">
                        <h4 class="c-title-underline u-font-16 u-md-font-22">法國依視路Wave Analyzer Medica 700+</h4>
                        <div class="row">
                            <div class="col-lg-6 mb-3 mb-lg-0">
                                <p>
                                    WAM700+全方位視覺檢測系統，結合了電腦驗光機、角膜地圖儀、前導波像差儀、眼前部攝影、非接觸式眼壓計，能在約90秒完成7項雙眼檢查與評估，一次性完整彙出7項數據： </p>
                                <ul class="u-list-style--custom-level03">
                                    <li>(1)&nbsp;日夜間瞳孔&屈光度分析--前導波分析日夜間的度數差異</li>
                                    <li>(2)&nbsp;近距離工作度調節測量--客觀評估眼睛是否因調節因素而容易疲勞</li>
                                    <li>(3)&nbsp;視覺像差分析--視覺模擬高低階像差的影響，及矯正前後差異</li>
                                    <li>(4)&nbsp;角膜弧度--角膜散光分析，及驗配隱形眼鏡所需的參數</li>
                                    <li>(5)&nbsp;角膜地形圖分析</li>
                                    <li>(6)&nbsp;水晶體透徹度</li>
                                    <li>(7)&nbsp;前房斷層掃描</li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <div class="d-flex">
                                    <div class="mx-3">
                                        <img src="assets/img/education/education_36.jpg" alt="" class="img-fluid">
                                        <p>WAM700+背面照</p>
                                    </div>
                                    <div class="mx-3">
                                        <img src="assets/img/education/education_32.jpg" alt="" class="img-fluid">
                                        <p>視覺模擬畫面</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <!-- =============end 主要內容區 ============= -->