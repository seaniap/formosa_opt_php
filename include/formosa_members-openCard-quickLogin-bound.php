<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>立即開卡 ｜ 寶島眼鏡官網、配鏡推薦、眼鏡行推薦、鏡框、時尚鏡框、太陽眼鏡、墨鏡、鏡片、隱形眼鏡、彩片、葉黃素、面膜、助聽器、周邊配件、5度C、助聽器</title>
    <meta name="keyword" content="寶島眼鏡,寶島,眼鏡">
    <meta name="description" content="若您已於寶島眼鏡門市加入會員，但沒有帳號密碼，請按照以下步驟完成開卡，即可享有寶島眼鏡會員獨享好康！">
    <meta name="author" content="寶島眼鏡">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" charset="UTF-8">
    <meta property="og:image" content="assets/img/share_1200x630.jpg" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="preload" href="assets/img/index/kv_pc_01.jpg" as="image" type="image/jpg">
    <link rel="stylesheet" href="assets/plugins/bootstrap@4.5.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/plugins/fontawesome@5/css/all.min.css" />
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick-theme.css">
    <link rel="stylesheet" href="assets/plugins/slick@1.90/slick.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <?php include ("ga_codes_header.php")?>   
</head>

<body>
<?php include ("ga_codes_body.php")?>
<?php include ("formosa_loading.php")?>
    <noscript>
        <span class="h2 d-block text-center text-light bg-danger p-3 mb-0">
            您的瀏覽器不支援JavaScript功能，若網頁功能無法正常使用時，請開啟瀏覽器JavaScript狀態。
        </span>
    </noscript>
    <div id="page">
        <!-- ============= 導覽列 ============= -->
        <header class="l-header">
            <?php include('formosa_header.php') ?>
        </header>
        <div id="sideLink" class="c-sideLink c-sideLink--hide-mobile"></div>
        <!-- =============end 導覽列 ============= -->
        <!-- ============= 主要內容區 ============= -->
        <main class="wrapper">
            <section>
                <div class="u-pt-250">
                    <div class="container">
                        <h3 class="c-title-center u-mb-125">立即開卡</h3>
                    </div>
                </div>
                <div class="u-pb-400">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-12">
                                <div class="u-mb-200">
                                    <p class="u-text-gray-800 u-font-weight-700 u-font-18 text-center">綁定社群帳號後，下次登入更為便利</p>
                                </div>

                                <div class="row justify-content-center u-mb-200">
                                    <div class="col-10 col-sm-12">
                                        <div class="row align-items-start">
                                            <div class="col-4 px-2 px-md-3">
                                                <a class="text-center" href="">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/facebook.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">已綁定Facebook</p>
                                                </a>
                                            </div>
                                            <div class="col-4 px-2 px-md-3">
                                                <a class="text-center" href="">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/google.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">已綁定Gooogle</p>
                                                </a>
                                            </div>
                                            <div class="col-4 px-2 px-md-3">
                                                <a class="text-center" href="">
                                                    <div class="u-mb-100 p-linkIcon col-lg-6 mx-auto px-0">
                                                        <img src="assets/img/members/line.svg" alt="">
                                                    </div>
                                                    <p class="u-text-gray-800 u-mb-000">已綁定LINE</p>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>



                                <button onclick="location.href='members-openCard-success.html'" type="button" class="c-btn c-btn--contained c-btn-blue-highlight col">略過</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <!-- =============end 主要內容區 ============= -->
        <!-- ============= footer ============= -->
        <footer class="l-footer"><?php include('formosa_footer.php') ?></footer>
        <!-- =============end footer ============= -->
    </div>
    <!-- JS Global Compulsory -->
    <script src="assets/plugins/jquery@3.5.1.min.js"></script>
    <script src="assets/plugins/bootstrap@4.5.3/js/bootstrap.min.js"></script>
    <script src="assets/plugins/slick@1.90/slick.min.js"></script>
    <script src="assets/plugins/lozad/lozad.min.js"></script>
    <script nomodule src="assets/plugins/polyfill.min.js?flags=gated&features=Object.assign%2CIntersectionObserver"></script>
    <script src="assets/js/main.js"></script>
    <!-- JS Customization -->
</body>

</html>