<?php require_once('../Connections/MySQL.php'); ?>
<?php $template = "recommended01.php";
$templateS = "recommended01";
// 判斷是預設是A版還是S版
// 預設 A 版
?>
<!-- to be marked -->
<!DOCTYPE html>
<html lang="zh-tw">
<meta charset="UTF-8">
<!-- to be marked -->
<?php
if ($_GET[$templateS] == "" and $row_template['Extend'] == "Y") {
    //A 版開始
    if ($_GET['demo'] == 'y') { ?>
<!-- ============= 主要內容區 ============= -->
<main>
    <section>
        <div class="container u-py-200">
            <h3 class='c-title-center u-mb-225'>人氣推薦</h3>
            <div class="row justify-content-end u-pb-200">
                <span class="align-self-center d-none d-md-inline-block">系列活動：</span>
                <div class="col-md-4">
                    <select name="select" id="" class="l-form-field l-form-select u-bg-gray-200">
                        <option value="default" selected disabled>全部活動</option>
                        <option value="">迪士尼幻想視界系列</option>
                        <option value="">迪士尼粉萌派對系列</option>
                        <option value="">寶島眼鏡xSOU‧SOU系列 記者會體驗文</option>
                        <option value="">寶島眼鏡X故宮 大清宮逗系列</option>
                        <option value="">K-DESIGN KREATE系列</option>
                        <option value="">寶島眼鏡十倍振興券</option>
                        <option value="">新年好運鼠不盡 米奇系列鏡框</option>
                        <option value="">SOU・SOU獨家聯名鏡框</option>
                    </select>
                </div>
            </div>
            <div class="u-pb-200">
                <div class="row">



                    <div class="col-md-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <img src="assets/img/recommended/recommended_01.jpg" alt="" class="c-card-head-img">
                                </div>
                                <div class="c-card-body c-card-body__height-adjust">
                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <div class="d-flex mb-1">
                                                <h4 class="c-card-body-title mb-0">Kimi</h4>
                                                <a href="#" class="ml-auto">
                                                    <img src="assets/img/recommended/icon_facebook.svg" alt=""
                                                        class="img-fluid">
                                                </a>
                                            </div>
                                            <span class="u-font-14 u-text-blue-500 u-font-weight-700">迪士尼幻想視界系列</span>
                                            <hr class="w-100">
                                            <div class="c-card-body-content u-text-gray-700 mb-0">
                                                <span
                                                    class="c-card-body-txt">這次寶島眼鏡和迪士尼聯名的粉萌派對系列眼鏡連我臉型很挑都能輕鬆駕馭不僅修飾臉型，邊框設計也很質感😚</span>
                                                <span><a href="#">more</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>












                </div>
            </div>
            <nav>
                <ul class="c-pagination justify-content-center align-items-center">
                    <li class="c-pagination-item  align-self-center">
                        <a href="#" class="py-1 px-2">
                            <i class="far fa-angle-left"></i>
                        </a>
                    </li>
                    <li class="c-pagination-item"><a href="#">1</a></li>
                    <li class="c-pagination-item">...</li>
                    <li class="c-pagination-item"><a href="#">10</a></li>
                    <li class="c-pagination-item py-1 px-2 align-self-center">
                        <a href="#" class="py-1 px-2">
                            <i class="far fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>
</main>
<!-- =============end 主要內容區 ============= -->


<?php } else {  ?>
<?php $currentPage = $_SERVER["PHP_SELF"];

        $maxRows_content01 = $row_template['MaxRow'];
        $pageNum_content01 = 0;
        if (isset($_GET['pageNum_content01'])) {
            $pageNum_content01 = $_GET['pageNum_content01'];
        }
        $startRow_content01 = $pageNum_content01 * $maxRows_content01;

        $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
           // $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
            $colname_content01 = $_GET['Page'];
        }


if($_SESSION['Mode']=="Admin"){
   $query_content01 = sprintf("SELECT a.* FROM %s_BigTable a  WHERE a.Page = %s AND a.Template='$template' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
		
}else{		 
     $query_content01 = sprintf("SELECT a.* FROM %s_BigTable a , 
	 ( select h01 from `GoWeb_BigTable` where Page = '4-1' AND Template='news41.php' 
	 and d01<=now() and d02>=now() 	   ) b  
	 WHERE a.h02=b.h01 and  a.Page = %s AND a.Template='$template' ", $Table_ID, GetSQLValueString($colname_content01, "text"));			 
}
		
		
		
		
        if ($_GET['Cate01'] <> "") {
            $query_content01 = $query_content01 . "AND a.Cate01='" . $_GET['Cate01'] . "'";
        }

        if ($_GET['h02'] <> "") {
            $query_content01 = $query_content01 . "AND a.h02='" . $_GET['h02'] . "'";
        }

        //排序 by Sq
        $query_content01 = $query_content01 . " order by a.Sq desc ";

        $query_limit_content01 = sprintf("%s LIMIT %d, %d", $query_content01, $startRow_content01, $maxRows_content01);
        //echo $query_limit_content01 ;
        $content01 = mysqli_query($MySQL, $query_limit_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);

        $all_content01 = mysqli_query($MySQL, $query_content01);
        $totalRows_content01 = mysqli_num_rows($all_content01);

        $totalPages_content01 = ceil($totalRows_content01 / $maxRows_content01) - 1;

        $queryString_content01 = "";
        if (!empty($_SERVER['QUERY_STRING'])) {
            $params = explode("&", $_SERVER['QUERY_STRING']);
            $newParams = array();
            foreach ($params as $param) {
                if (
                    stristr($param, "pageNum_content01") == false &&
                    stristr($param, "totalRows_content01") == false &&
                    stristr($param, "Cate01") == false &&
                    stristr($param, "Cate02") == false &&
                    stristr($param, "Cate03") == false
                ) {
                    array_push($newParams, $param);
                }
            }
            if (count($newParams) != 0) {
                $queryString_content01 = "&" . htmlentities(implode("&", $newParams));
            }
        }
        $queryString_content01 = sprintf("&totalRows_content01=%d%s", $totalRows_content01, $queryString_content01);
        $queryString_content01 = $queryString_content01 . "&Cate01=" . $_GET['Cate01'] . "&Cate02=" . $_GET['Cate02'] . "&Cate03=" . $_GET['Cate03'];
        ?>




<!-- ============= 主要內容區 ============= -->
<main class="wrapper">
    <section>
        <div class="u-pt-250">
            <div class="container">
                <h3 class='c-title-center u-mb-225'>人氣推薦</h3>
            </div>
        </div>
        <div class="js-tabPosition"></div>
        <div class="u-py-100 u-bg-white js-tabFixed">
            <div class="container">
                <div class="row justify-content-end">
                    <span class="align-self-center d-none d-md-inline-block">系列活動：</span>
                    <?php include("select_opt_button.php"); ?>
                    <div class="col-md-4">
                        <?php
                                $query_content41 = sprintf("SELECT * FROM GoWeb_BigTable WHERE Page = '4-1' AND Template='news41.php' and d01<=now() and d02>=now() order by Sq+0 desc ");
                                $content41 = mysqli_query($MySQL, $query_content41) or die(mysqli_error($MySQL));
                                ?>
                        <select name="h02" id="h02" class="l-form-field l-form-select u-bg-gray-200"
                            onChange="location.href='index.php?Page=4&h02='+this.value;">
                            <option value="">全部活動</option>
                            <?php while ($row_content41 = mysqli_fetch_assoc($content41)) {  ?>
                            <option value="<?php echo $row_content41['h01'];  ?>" <?php if ($_GET['h02'] == $row_content41['h01']) {
                                                                                                    echo "selected";
                                                                                                } ?>>
                                <?php echo $row_content41['h01'];  ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="u-pt-200 u-pb-100">
            <div class="container"><?php  SuperM_ins($row_content01,$template,$row_template['Security']); ?></div>
            <div class="container">
                <div class="row">
                    <!--start loop -->
                    <?php do {  ?>

                    <?php if ($row_content01['l01'] == "") {
                                    $newlink = "index.php?Page=" . $row_content01['Page'] . "&" . $templateS . "=" . $row_content01['KeyID'];
                                    $target = "";
                                } else {
                                    $ext = " (" . end(explode('.', $row_content01['l01'])) . ")";
                                    $newlink = $row_content01['l01'];
                                    $target = "_blank";
                                }
                                ?>
                    <div class="col-md-3">
                        <div class="c-card c-card-link">
                            <div class="c-card-layout">
                                <!-- 3 button -->
                                <?php include("3buttonA.php"); ?>
                                <div class="c-card-head c-card-head-aspect-ratio">
                                    <?php if ($row_content01['p01'] <> "") { ?>
                                    <a href="<?php echo $newlink; ?>" target="_blank">
                                        <img src="UploadImages/<?php echo $row_content01['p01'];  ?>"
                                            class="c-card-head-img" title="<?php echo $row_content01['h01'];  ?>"
                                            alt="<?php echo $row_content01['h01'];  ?>" /></a>
                                    <?php } ?>
                                </div>
                                <?php        if($row_content01['KeyID']!=""){ ?>
                                <div class="c-card-body c-card-body__height-adjust">

                                    <div class="c-card-body-container">
                                        <div class="c-card-body-layout">
                                            <div class="d-flex mb-1">
                                                <h4 class="c-card-body-title mb-0">
                                                    <?php echo $row_content01['h01'];  ?>
                                                </h4>
                                                <a href="<?php echo $row_content01['l01'];  ?>" class="ml-auto" target="_blank">
                                                    <!-- SNS Icons-->
                                                    <?php  	include("SNS_icons.php");     ?>
                                                </a>
                                            </div>



                                            <span
                                                class="u-font-14 u-text-blue-500 u-font-weight-700"><?php echo $row_content01['h02'];  ?></span>
                                            <hr class="w-100">


                                            <div class="c-card-body-content u-text-gray-700 mb-0">
                                                <span class="c-card-body-txt"><a href="<?php echo $newlink; ?>"
                                                        target="_blank"><?php echo $row_content01['c01'];  ?></a></span>

                                                <span><a href="<?php echo $newlink; ?>" target="_blank">more</a></span>


                                            </div>



                                        </div>
                                    </div>
                                </div>

                                <?php } else{  echo "相關資訊正在準備中";} ?>


                            </div>
                        </div>

                    </div>
                    <?php } while ($row_content01 = mysqli_fetch_assoc($content01)); ?>
                    <!--end loop -->
                </div>
            </div>

        </div>
        <!-- paginations -->
        <div class="u-py-100 u-pb-400">
            <div class="container">
                <?php include('formosa_paginations.php'); ?>
            </div>
        </div>
        <!--end paginations -->
    </section>
</main>
<!-- =============end 主要內容區 ============= -->



<?php mysqli_free_result($content01);
    }

    //A 版結束 
} else {  //S版開始

    if ($_GET['demo'] == 'y') { ?>



<?php } else {  ?>
<?php $colname_content01 = "-1";
        if (isset($_GET['Page'])) {
            $colname_content01 = (get_magic_quotes_gpc()) ? $_GET['Page'] : addslashes($_GET['Page']);
        }

        $maxRows_content01 = $row_template['MaxRow'];
        $KeyID = str_replace($vowels, "**!!!**", substr($_GET[$templateS], 0, 50));
        if ($row_template['Extend'] == 'Y') {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE  KeyID='$KeyID' ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        } else {
            $query_content01 = sprintf("SELECT * FROM %s_BigTable WHERE Page = %s  ", $Table_ID, GetSQLValueString($colname_content01, "text"));
        }

        $content01 = mysqli_query($MySQL, $query_content01) or die(mysqli_error($MySQL));
        $row_content01 = mysqli_fetch_assoc($content01);
        $totalRows_content01 = mysqli_num_rows($content01);
        //echo "S版啟動".$query_content01;
        ?>



here is S





<?php mysqli_free_result($content01);
    }
}  //S版結束
?>