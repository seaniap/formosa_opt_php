<?php
use app\util\Config;
use app\util\SSO;
include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";
SSO::login(); 
?>
<?php
error_reporting(0);
# FileName="Connection_php_mysql.htm"
# Type="MYSQL"
# HTTP="true"
session_start();
date_default_timezone_set('Asia/Taipei');
//檔案深度
$offset=1;
$level=0;
while (strpos($_SERVER['PHP_SELF'],'/',$offset)<>strrpos($_SERVER['PHP_SELF'],'/'))
{
$offset=strpos($_SERVER['PHP_SELF'],'/',$offset)+1;
$level=$level+1;
}
for ($i=0;$i<$level;$i++){
$RootLevel=$RootLevel."../";
}
//檢查權限

$RootPath=substr($_SERVER['PHP_SELF'],0,strpos($_SERVER['PHP_SELF'],'aiadmin'));
if($_SESSION['Mode']=="") {
$FF_authFailedURL=$RootPath."Logout.php";
//echo $FF_authFailedURL;
header("Location: $FF_authFailedURL");
exit;
} 

global $MySQL,$Table_ID;
//dev
//  $hostname_MySQL = "localhost";
//  $database_MySQL = "formosaa_GoWeb";
//  $username_MySQL = "formosaaiadnet";
//  $password_MySQL = "~EalDDX?!=Ww";
//prod
$hostname_MySQL = "localhost";
$database_MySQL = "formosaa_goweb";
$username_MySQL = "formosaaiadnet";
$password_MySQL = "~EalDDX?!=Ww";


$hostname_MySQL = Config::get("db_host");
$database_MySQL = Config::get("db_name");
$username_MySQL = Config::get("db_user");
$password_MySQL = Config::get("db_password");

// $username_MySQL = "root";
// $password_MySQL = "root";
$MySQL =mysqli_connect($hostname_MySQL, $username_MySQL, $password_MySQL, $database_MySQL);
mysqli_query($MySQL, 'SET NAMES utf8mb4'); 

$Table_ID="GoWeb";
//$Table_ID=$_SESSION['ClientID'];
//$Table_ID="AI2";
//if (isset($_GET['ClientID'])) {
//$Table_ID=$_GET['ClientID'];
//}
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  global  $MySQL;
   $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($MySQL,$theValue) : mysqli_escape_string($MySQL,$theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$vowels = array("+","=","#","'","\"","?","*","_","%","&","||","\\","/",":",";","(",")","{","}","[","]","<", ">","script","SCRIPT");
$_GET['Template']=str_replace($vowels ,"**!!!**",$_GET['Template']);	
$_GET['Page']=str_replace($vowels ,"**!!!**",$_GET['Page']);
$_GET['Where']=str_replace($vowels ,"**!!!**",$_GET['Where']);
$_GET['Submit']=str_replace($vowels ,"**!!!**",$_GET['Submit']);
$_GET['MKeyID']=str_replace($vowels ,"**!!!**",$_GET['MKeyID']);
$_GET['Sort']=str_replace($vowels ,"**!!!**",$_GET['Sort']);
$_GET['Action']=str_replace($vowels ,"**!!!**",$_GET['Action']);

?>