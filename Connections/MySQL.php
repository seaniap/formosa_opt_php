<?php
use app\util\Config;
use app\util\SSO;
include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";
SSO::login(); 
?>
<?php
error_reporting(0);
# FileName="Connection_php_mysql.htm"
# Type="MYSQL"
# HTTP="true"
ini_set("session.cookie_httponly", 1);
ini_set("session.cookie_secure", 1);
session_start();
global $MySQL,$Table_ID;
date_default_timezone_set('Asia/Taipei');

/*
if($_SERVER['HTTP_HOST']=="ietw.cityweb.com.tw"){
	 $ss="http://ietw.moe.gov.tw".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];
	//echo $ss;
header(sprintf("Location: %s", $ss));  	}
*/
//dev
$hostname_MySQL = Config::get("db_host");
$database_MySQL = Config::get("db_name");
$username_MySQL = Config::get("db_user");
$password_MySQL = Config::get("db_password");
//$password_MySQL = "~EalDDX?!=Ww";
//local
// $username_MySQL = "root";
// $password_MySQL = "root";
//prod
// $hostname_MySQL = "localhost";
// $database_MySQL = "formosaa_goweb";
// $username_MySQL = "formosaaiadnet";
//$password_MySQL = "~EalDDX?!=Ww";



$MySQL =mysqli_connect($hostname_MySQL, $username_MySQL, $password_MySQL, $database_MySQL);

mysqli_query($MySQL, 'SET NAMES utf8mb4'); 

try
{
	$pdo  = new PDO("mysql:host=$hostname_MySQL;dbname=$database_MySQL", $username_MySQL, $password_MySQL);
	$pdo->query("set names utf8");
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}
catch(Exception $e)
{
    echo $e->getMessage();
	var_dump($e->getMessage());
}


header("Content-Type:text/html;charset=utf-8");

$Table_ID="GoWeb";
/*
if (isset($_GET['ClientID'])) {
$Table_ID=$_GET['ClientID'];
}
if (isset($_SESSION['ClientID'])) {
$Table_ID=$_SESSION['ClientID'];
}
*/

//echo "KKK".$_SESSION['ClientID'];



if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

   global  $MySQL;
   $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($MySQL,$theValue) : mysqli_escape_string($MySQL,$theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$vowels0 = array("+","#","'","\"","?","*","||","\\","/",":",";","(",")","{","}","[","]","<", ">","script","SCRIPT");
$vowels = array("+","=","#","'","\"","?","*","_","%","&","||","\\","/",":",";","(",")","{","}","[","]","<", ">","script","SCRIPT");
$vowels2 = array("+","#","'","\"","*","%","||","\\",":",";","(",")","{","}","[","]","<", ">","script","SCRIPT");

foreach ( array_keys ($_GET) as $value) {
   // echo "Value: $value<br>";
        $_GET[$value]=str_replace($vowels ,"**!!!**",$_GET[$value]);        
}
$_SERVER['QUERY_STRING']=str_replace($vowels0 ,"**!!!**",$_SERVER['QUERY_STRING']);	
$_SERVER["PHP_SELF"]=str_replace($vowels2 ,"",$_SERVER["PHP_SELF"]);	


$_GET['Template']=str_replace($vowels ,"**!!!**",$_GET['Template']);	
$_GET['Page']=str_replace($vowels ,"**!!!**",$_GET['Page']);
$_GET['Where']=str_replace($vowels ,"**!!!**",$_GET['Where']);
$_GET['Submit']=str_replace($vowels ,"**!!!**",$_GET['Submit']);
$_GET['MKeyID']=str_replace($vowels ,"**!!!**",$_GET['MKeyID']);
$_GET['Sort']=str_replace($vowels ,"**!!!**",$_GET['Sort']);
$_GET['Cate01']=str_replace($vowels ,"**!!!**",$_GET['Cate01']);
$_GET['Cate02']=str_replace($vowels ,"**!!!**",$_GET['Cate02']);
$_GET['Cate03']=str_replace($vowels ,"**!!!**",$_GET['Cate03']);
$_GET['KeyID']=str_replace($vowels ,"**!!!**",$_GET['KeyID']);
$_GET['news01']=str_replace($vowels ,"**!!!**",$_GET['news01']);
$_GET['news02']=str_replace($vowels ,"**!!!**",$_GET['news02']);
$_GET['news03']=str_replace($vowels ,"**!!!**",$_GET['news03']);

$_GET['totalRows_content01']=str_replace($vowels ,"**!!!**",$_GET['totalRows_content01']);

if(strlen($_GET['pageNum_content01'])>7){$_GET['pageNum_content01']="";}



header("X-XSS-Protection: 1; mode=block "); 
header('X-Content-Type-Options: nosniff');
//header("X-Frame-Options: DENY");
header("X-Frame-Options: SAMEORIGIN");

//安全掃描要求 B 級
//header("Content-Security-Policy: default-src *");
//header("Access-Control-Allow-Origin: https://insp.eshs.ntnu.edu.tw,http://iportal.ntnu.edu.tw");
//header("Access-Control-Allow-Credentials: true ");
//header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
//header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size,X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");

$headerCSP = "Content-Security-Policy:".
        "connect-src 'self' ;". // XMLHttpRequest (AJAX request), WebSocket or EventSource.
        "default-src 'self';". // Default policy for loading html elements
        "frame-ancestors 'self' ;". //allow parent framing - this one blocks click jacking and ui redress
        "frame-src 'none';". // vaid sources for frames
        "media-src 'self';". // vaid sources for media (audio and video html tags src)
        "object-src 'none'; ". // valid object embed and applet tags src
        //"report-uri https://example.com/violationReportForCSP.php;". //A URL that will get raw json data in post that lets you know what was violated and blocked
        "script-src 'self' 'unsafe-inline' // example.com code.jquery.com https://ssl.google-analytics.com ;". // allows js from self, jquery and google analytics.  Inline allows inline js
        "style-src 'self' 'unsafe-inline';";// allows css from self and inline allows inline css
//Sends the Header in the HTTP response to instruct the Browser how it should handle content and what is whitelisted
//Its up to the browser to follow the policy which each browser has varying support
header($contentSecurityPolicy);



header("Strict-Transport-Security:max-age=63072000");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
//header('Set-Cookie: cross-site-cookie=name; SameSite=lax; Secure');
//setcookie('cross-site-cookie', 'name', ['samesite' => 'lax', 'secure' => true]);
?>