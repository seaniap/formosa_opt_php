<?php
namespace app\util;
include_once $_SERVER['DOCUMENT_ROOT'] . "/autoload.php";


function httpRequest($api, $data_string) {
    
    $ch = curl_init($api);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/x-www-form-urlencoded',
        'Content-Length: ' . strlen($data_string))
        );
    $result = curl_exec($ch);
    curl_close($ch);
    
    return $result;
}

$client_id = Config::get("line_client_id");
$client_secret = Config::get("line_client_secret");
$redirect_uri = Config::get("line_redirect_uri");


$url = 'https://api.line.me/oauth2/v2.1/token';
$params = 'code=' . $_GET['code'] . "&client_id=$client_id&client_secret=$client_secret&grant_type=authorization_code&redirect_uri=$redirect_uri";

$response = httpRequest($url,$params);

$result = json_decode($response);

if (!$result->id_token) {
    echo  "<script> window.opener.postMessage('line error','*'); </script>";
    return;
}


$url = "https://api.line.me/oauth2/v2.1/verify";
$params = 'id_token=' . $result->id_token . "&client_id=$client_id";

$response = httpRequest($url,$params);
$result = json_decode($response);

if (!$result->sub) {
    echo  "<script> window.opener.postMessage('line error','*'); </script>";
    return;
}

echo  "<script> window.opener.postMessage('line $result->sub','*'); </script>";